struct VSIn {
	float2 ndc_position : NdcPosition0;
	float2 texcoord : TexCoord0;
};

struct VSOut {
	float4 position : SV_Position;
	float2 texcoord : TexCoord0;
};

Texture2D mat_tex_0 : register(t0);

SamplerState mat_smp_0 : register(s0);

VSOut vs_main(VSIn input) {
    VSOut output = (VSOut)0;
	output.position = float4(input.ndc_position, 0.0, 1.0);
    output.texcoord = input.texcoord;
    return output;
}

float4 ps_main(VSOut input) : SV_Target {
    float4 color = mat_tex_0.Sample(mat_smp_0, input.texcoord);
    return color;
}