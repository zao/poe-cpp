struct VSIn {
	float3 position : Position0;
	float3 color : Color0;
};

struct VSOut {
	float4 position : SV_Position;
	float3 color : Color0;
};

cbuffer CBPass : register(b0) {
    float4x4 view_projection_transform;
}

cbuffer CBPipe : register(b1) {
}

cbuffer CBObject : register(b2) {
    float4x4 world_transform;
}

VSOut vs_main(VSIn input) {
    VSOut output = (VSOut)0;
	float4 position = float4(input.position, 1.0);
    output.position = mul(mul(position, world_transform), view_projection_transform);
    output.color = input.color;
    return output;
}

float4 ps_main(VSOut input) : SV_Target {
    float4 color = float4(input.color, 1.0);
    return color;
}