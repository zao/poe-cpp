struct VSIn {
	float3 position : Position;
	float4 normal : Normal;
    float4 tangent : Tangent;
    float2 tc0 : Texcoord0;
    // float2 tc1 : Texcoord1;
    uint4 bone_indices : BLENDINDICES;
    float4 blend_weights : BLENDWEIGHT;
};

struct VSOut {
	float4 position : SV_Position;
    float4 worldPos : WorldPosition;
    float3 camPos : CamPosition;
    float3 normal : Normal;
    float3 tangent : Tangent;
    float arg0 : Arg0;
    float arg1 : Arg1;
    float2 tc0 : Texcoord0;
    // float2 tc1 : Texcoord1;
};

cbuffer CBPass : register(b0) {
    float4x4 view_projection_transform;
    float4x4 view_transform;
    float4x4 inv_view_transform;
}

cbuffer CBPipe : register(b1) {
    bool hasTex0, hasTex1;
    bool hasLegacyNormals;
    bool hasFlippedNormals;
}

cbuffer CBObject : register(b2) {
    float4x4 world_transform;
    float4x4 inv_world_transform;
    float4 animation_matrices_indices;
}

struct Light {
    float4 pos;
    float3 color;
    float intensity;
};

StructuredBuffer<Light> lights : register(t0);
Texture2D tex_AlbedoTransparency_TEX : register(t1);
Texture2D tex_NormalGlossAO_TEX : register(t2);
Texture2D tex_SpecularColour_TEX : register(t3);

SamplerState smp_AlbedoTransparency_TEX : register(s0);
SamplerState smp_NormalGlossAO_TEX: register(s1);
SamplerState smp_SpecularColour_TEX : register(s2);

float3 NormalTexToTbn(float x, float y) {
    float scale = 1.0f;
	float3 tbn_normal;
    tbn_normal.xy = (2.0f * float2(x, y) - 1.0f) * scale;
	tbn_normal.z = sqrt( max(1.0f - dot(tbn_normal.xy, tbn_normal.xy), 0.0f) );
    return tbn_normal;
}

#define STRUCTURED_BUFFER_DECL(Name, Type) StructuredBuffer<Type> Name
STRUCTURED_BUFFER_DECL(animation_matrices, float4x4);

// DECLARATIONS texture_animation_data
float4x4 ReadAnimationMatrix(int bone_index) {
    return animation_matrices[(int)animation_matrices_indices.x + bone_index];
}

float4 ReadUVAlphaVector(int bone_index) {
    return animation_matrices[(int)animation_matrices_indices.y + bone_index][0];
}

// DECLARATIONS animation_palette
#define ReadAnimationPalette(bone_index) (ReadAnimationMatrix(bone_index))
#define ReadUVAlphaPalette(bone_index) (ReadUVAlphaVector(bone_index))

void Code__SkinVectors(in float4 in_position, in float3 in_normal, in float4 in_tangent, in uint4 bone_indices
                       : BLENDINDICES, in float4 blend_weights
                       : BLENDWEIGHT, out float4 out_position, out float3 out_normal, out float4 out_tangent) {
    out_position = 0.0f;
    out_normal = 0.0f;
    out_tangent = float4(0.0f, 0.0f, 0.0f, in_tangent.w);

    float last_weight = 0.0f;

    // code would have been way more compact with for()-loop but d3d9 compiler generates really poor code for it

    float4x4 matrix0 = ReadAnimationPalette(bone_indices.x);
    out_position += mul(in_position, matrix0) * blend_weights.x;
    out_normal += mul(in_normal, (float3x3)matrix0) * blend_weights.x;
    out_tangent.xyz += mul(in_tangent.xyz, (float3x3)matrix0) * blend_weights.x;

    float4x4 matrix1 = ReadAnimationPalette(bone_indices.y);
    out_position += mul(in_position, matrix1) * blend_weights.y;
    out_normal += mul(in_normal, (float3x3)matrix1) * blend_weights.y;
    out_tangent.xyz += mul(in_tangent.xyz, (float3x3)matrix1) * blend_weights.y;

    float4x4 matrix2 = ReadAnimationPalette(bone_indices.z);
    out_position += mul(in_position, matrix2) * blend_weights.z;
    out_normal += mul(in_normal, (float3x3)matrix2) * blend_weights.z;
    out_tangent.xyz += mul(in_tangent.xyz, (float3x3)matrix2) * blend_weights.z;

    // Work out the weighting of the last bone
    last_weight = 1.0f - blend_weights.x - blend_weights.y - blend_weights.z;

    // Add contribution of the last bone
    float4x4 matrix3 = ReadAnimationPalette(bone_indices.w);
    out_position += mul(in_position, matrix3) * last_weight;
    out_normal += mul(in_normal, (float3x3)matrix3) * last_weight;
    out_tangent.xyz += mul(in_tangent.xyz, (float3x3)matrix3) * last_weight;

    out_normal = normalize(out_normal);
    out_tangent.xyz = normalize(out_tangent.xyz);
}

VSOut vs_main(VSIn input) {
    VSOut output = (VSOut)0;
	float4 position = float4(input.position, 1.0);
    output.camPos = mul(float4(0, 0, 0, 1), inv_view_transform).xyz;

#define SKINNING 1

#if SKINNING
    Code__SkinVectors(position, input.normal.xyz, input.tangent, input.bone_indices, input.blend_weights, position,
                      input.normal.xyz, input.tangent);
#endif

    output.worldPos = mul(position, world_transform);
    output.position = mul(output.worldPos, view_projection_transform);
    output.normal = mul(input.normal.xyz, (float3x3)inv_world_transform);
    output.tangent = mul(input.tangent.xyz, (float3x3)inv_world_transform);
    output.arg0 = input.normal.w;
    output.arg1 = input.tangent.w;
    output.tc0 = input.tc0;
    // output.tc1 = input.tc1;
    return output;
}

float3x3 ComputeTBN(float3 N, float3 T, float argT) {
    N = normalize(N);
    T = normalize(T);
    // flip based on mirror factor
    float3 B = normalize(cross(N, T));
    B *= argT;
    return float3x3(T, B, N);
}

float4 ps_main(VSOut input) : SV_Target {
    // return float4(input.arg1 * 0.5 + 0.5, 0, 0, 1);
    // return float4(normalize(input.normal) * 0.5 + 0.5, 1);
    float3 camVec = (input.camPos - input.worldPos.xyz);
    float3 camDir = normalize(camVec);
    // T, B, N
    float3 N = normalize(input.normal);
    {
        float3 T = normalize(input.tangent);
        float3x3 TBN = ComputeTBN(N, T, input.arg1);
        float2 tbn_encoded = tex_NormalGlossAO_TEX.Sample(smp_NormalGlossAO_TEX, input.tc0).rg;
        N = NormalTexToTbn(tbn_encoded.x, tbn_encoded.y);
        N = mul(N, TBN);
        // return float4(N, 1.0);
    }
    float3 V = camDir;

    // float3 albedo = float3(1.0, 1.0, 1.0);
    float3 colorAcc = float3(0.0, 0.0, 0.0);

    uint lightCount, lightSize;
    lights.GetDimensions(lightCount, lightSize);
        
    for (uint lightIdx = 0; lightIdx < lightCount; ++lightIdx) {
        Light light = lights[lightIdx];
        float3 lightDir;
        float att = 1.0;
        if (light.pos.w) {
            // point light
            float3 lightPos = light.pos.xyz;
            float3 lightVec = lightPos - input.worldPos.xyz;
            lightDir = normalize(lightVec);
            
            float lightDist = length(lightVec);
            float att1 = pow(2, -15.3);
            float att2 = pow(4.4, -11.2);
            att = 1.0 / (1.0 + att1 * lightDist + att2 * lightDist*lightDist);
        } else {
            lightDir = -light.pos.xyz;
        }
        float3 L = lightDir;
        float3 R = reflect(-L, N);

        float NdotL = saturate(dot(N, L));
        colorAcc += light.color * light.intensity * att * NdotL;

        float RdotV = saturate(dot(R, V));
        if (RdotV > 0) {
            colorAcc += light.color * light.intensity * att * pow(RdotV, 50.0);
        }
    }

    // return float4(colorAcc, 1);

    float4 albedo = tex_AlbedoTransparency_TEX.Sample(smp_AlbedoTransparency_TEX, input.tc0);
    // float4 color = float4(colorAcc, 1.0) * pow(abs(albedo), 0.25);
    float4 color = float4(colorAcc, 1.0) * albedo;
    return color;
}