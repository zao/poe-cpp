#define VERTEX_OUTPUT_TEXCOORD1
#define VERTEX_OUTPUT_POSITION
#define VERTEX_OUTPUT_TBN
#define VERTEX_OUTPUT_COLOR0
#define VERTEX_OUTPUT_COLOR1
#define VERTEX_OUTPUT_TEXCOORD2
#define VERTEX_OUTPUT_TEXCOORD3
#define VERTEX_OUTPUT_TEXCOORD4
#define VERTEX_OUTPUT_TEXCOORD8
#define VERTEX_OUTPUT_TEXCOORD9
#define VERTEX_OUTPUT_SEED
#define VERTEX_OUTPUT_UV_SET_2
#define VERTEX_OUTPUT_MODEL_ORIGIN
#define GGX
#define PBR
#define SUBSURFACE_SCATTERING

#define TEXTURE2D_DECL(Name) Texture2D Name
#define STRUCTURED_BUFFER_DECL(Name, Type) StructuredBuffer<Type> Name
#define SAMPLER_DECL(Name) SamplerState Name
#define SAMPLER_CMPDECL(Name) SamplerComparisonState Name
#define SAMPLE_TEX2D(Tex, Smp, Loc) Tex.Sample(Smp, Loc)
#define SAMPLE_TEX2DLOD(Tex, Smp, Loc) Tex.SampleLevel(Smp, (Loc).xy, (Loc).z, (Loc).w)

#define smp_Glow_TEX smp_AlbedoTransparency_TEX
#define SamplerDynamicWrap smp_AlbedoTransparency_TEX

cbuffer CBPass : register(b0) {
    float4x4 view_projection_transform;
    float4x4 view_transform;
    float4x4 inv_view_transform;
    float3 camera_position;
}

cbuffer CBPipe : register(b1) {
    bool hasTex0, hasTex1;
    bool hasLegacyNormals;
    bool hasFlippedNormals;
}

cbuffer CBObject : register(b2) {
    float4x4 world_transform;
    float4x4 inv_world_transform;
    float4 material_specular_color;
    float material_glossiness;
    float material_ambient_occlusion;
    float material_fresnel;
    float material_emission;
    float material_energy_conservation;
    float subsurface_scattering_intensity;
    float glossiness_exponent;
    float anisotropic_glossiness_roughness;
    float anisotropic_glossiness_contrast;
    float anisotropic_glossiness_fade;
    float4 subsurface_color;
    float4 animation_matrices_indices;
}

struct VertexData {
    float2 uv;
    float4 position;
    float4 projected_position;
    float3 normal;
    float4 tangent;
    float3 binormal;
    float4 color;
};

void InitVertexData(inout VertexData vertex_data) {
    vertex_data.uv = float2(0.f, 0.f);
    vertex_data.position = float4(0.f, 0.f, 0.f, 1.f);
    vertex_data.projected_position = float4(0.0f, 0.0f, 0.0f, 1.0f);
    vertex_data.normal = float3(0.f, 0.f, 1.f);
    vertex_data.tangent = float4(1.f, 0.f, 0.f, 1.f);
    vertex_data.binormal = float3(0.f, 1.f, 0.f);
    vertex_data.color = float4(0.f, 0.f, 0.f, 0.f);
}

struct MaterialData {
    float glossiness;
    float3 specular_color;
    float4 albedo_color;
    float3 emissive_color;
    float3 subsurface_color;
    float subsurface_translucency;
    float subsurface_front_scattering;
    float4 indirect_color;
};

void InitMaterial(inout MaterialData material_data) {
    material_data.glossiness = material_glossiness;
    material_data.specular_color = material_specular_color.rgb;
    material_data.albedo_color = float4(0.0f, 1.0f, 0.0f, 1.0f);
    material_data.emissive_color = material_emission;
    material_data.subsurface_color = float3(0.0f, 0.0f, 0.0f);
    material_data.subsurface_translucency = 1.0f;
    material_data.subsurface_front_scattering = 1.0f;
    material_data.indirect_color = float4(0.0f, 0.0f, 0.0f, material_ambient_occlusion);
}

struct LightData {
    float3 direction;
    float3 intensity;
};

struct TBN {
    float3 tangent;
    float3 binormal;
    float3 normal;
};

struct EmissionData {
    float3 diffuse;
    float3 specular;
    float3 emission;
    float3 subsurface;
    float3 ambient;
    float3 total;
    float4 pixel_color;
    float3 direct_light;
};

struct SurfaceData {
    float3 world_pos;
    float3 subsurface_world_pos;
    float3 tbn_normal;
    float3 world_normal;
    float3 world_bent_normal;
    float3 view_dir;
    float3 reflected_dir;
    float2 uv;
    float4 screenspace_shadow_value;
    float bent_normal_shadow_sharpness;
    float subsurface_seed;
    TBN tbn_basis;
    MaterialData material;
};

struct SemanticsData {
    float4 world_pos;    // TEXCOORD0
    float3 normal;       // TEXCOORD5
    float3 tangent;      // TEXCOORD6
    float3 binormal;     // TEXCOORD7
    float2 uv1;          // TEXCOORD1
    float4 uv2;          // TEXCOORD2
    float4 uv3;          // TEXCOORD3
    float4 uv4;          // TEXCOORD4
    float4 uv8;          // TEXCOORD8
    float4 uv9;          // TEXCOORD9
    float4 color0;       // COLOR0
    float4 color1;       // COLOR1
    float seed;          // COLOR2
    float2 uv_set_2;     // COLOR3
    float3 model_origin; // TEXCOORD1 on Doodads
};

void InitEmission(inout EmissionData emission_data) {
    emission_data.diffuse = 0.0f;
    emission_data.specular = 0.0f;
    emission_data.emission = 0.0f;
    emission_data.subsurface = 0.0f;
    emission_data.ambient = 0.0f;
    emission_data.total = 0.0f;
    emission_data.pixel_color = 0.0f;
    emission_data.direct_light = 0.0f;
}

void InitSurface(inout SurfaceData surface_data) {
    surface_data.world_pos = float3(0.0f, 0.0f, 0.0f);
    surface_data.subsurface_world_pos = float3(0.0f, 0.0f, 0.0f);
    surface_data.tbn_normal = float3(0.0f, 0.0f, 1.0f);
    surface_data.world_normal = float3(0.0f, 1.0f, 0.0f);
    surface_data.uv = float2(0.0f, 0.0f);
    surface_data.reflected_dir = float3(0.0f, 0.0f, 0.0f);
    surface_data.view_dir = float3(0.0f, 0.0f, 0.0f);
    surface_data.screenspace_shadow_value = 1.0f;
    surface_data.tbn_basis.normal = float3(0.0f, 1.0f, 0.0f);
    surface_data.tbn_basis.tangent = float3(1.0f, 0.0f, 0.0f);
    surface_data.tbn_basis.binormal = float3(0.0f, 0.0f, 1.0f);
    surface_data.world_bent_normal = float3(0.0f, 0.0f, 0.0f);
    surface_data.bent_normal_shadow_sharpness = 3.0f;
    surface_data.subsurface_seed = 0.0f;

    InitMaterial(surface_data.material);
}

void InitSemanticsData(inout SemanticsData semantics_data) {
    semantics_data.world_pos = float4(0.f, 0.f, 0.f, 1.f);
    semantics_data.normal = float3(0.f, 0.f, 1.f);
    semantics_data.tangent = float3(1.f, 0.f, 0.f);
    semantics_data.binormal = float3(0.f, 1.f, 0.f);
    semantics_data.color0 = float4(1.f, 1.f, 1.f, 1.f);
    semantics_data.color1 = float4(0.f, 0.f, 0.f, 1.f);
    semantics_data.uv1 = float2(0.0f, 0.0f);
    semantics_data.uv2 = float4(0.f, 0.f, 0.f, 0.f);
    semantics_data.uv3 = float4(0.f, 0.f, 0.f, 0.f);
    semantics_data.uv4 = float4(0.f, 0.f, 0.f, 0.f);
    semantics_data.uv8 = float4(0.f, 0.f, 0.f, 0.f);
    semantics_data.uv9 = float4(0.f, 0.f, 0.f, 0.f);
    semantics_data.uv_set_2 = float2(0.0f, 0.0f);
    semantics_data.seed = 0.0f;
    semantics_data.model_origin = float3(0.f, 0.f, 0.f);
}

struct VSIn {
    float4 position : Position;
    float4 normal : Normal;
    float4 tangent : Tangent;
    float2 tc0 : Texcoord0;
    // float2 tc1 : Texcoord1;
    uint4 bone_indices : BLENDINDICES;
    float4 blend_weights : BLENDWEIGHT;
};

struct Light {
    float4 pos;
    float3 color;
    float intensity;
};

STRUCTURED_BUFFER_DECL(animation_matrices, float4x4);

StructuredBuffer<Light> lights : register(t0);
Texture2D tex_AlbedoTransparency_TEX : register(t1);
Texture2D tex_NormalGlossAO_TEX : register(t2);
Texture2D tex_SpecularColour_TEX : register(t3);
Texture2D tex_Glow_TEX : register(t4);

SamplerState smp_AlbedoTransparency_TEX : register(s0);
SamplerState smp_NormalGlossAO_TEX : register(s1);
SamplerState smp_SpecularColour_TEX : register(s2);
#define smp_Glow_TEX smp_AlbedoTransparency_TEX

// DECLARATIONS texture_animation_data
float4x4 ReadAnimationMatrix(int bone_index) {
    return animation_matrices[(int)animation_matrices_indices.x + bone_index];
}

float4 ReadUVAlphaVector(int bone_index) {
    return animation_matrices[(int)animation_matrices_indices.y + bone_index][0];
}

// DECLARATIONS animation_palette
#define ReadAnimationPalette(bone_index) (ReadAnimationMatrix(bone_index))
#define ReadUVAlphaPalette(bone_index) (ReadUVAlphaVector(bone_index))

float3 NormalTexToTbn(float x, float y) {
    float scale = 1.0f;
    float3 tbn_normal;
    tbn_normal.xy = (2.0f * float2(x, y) - 1.0f) * scale;
    tbn_normal.z = sqrt(max(1.0f - dot(tbn_normal.xy, tbn_normal.xy), 0.0f));
    return tbn_normal;
}

float3x3 ComputeTBN(float3 N, float3 T, float argT) {
    N = normalize(N);
    T = normalize(T);
    // flip based on mirror factor
    float3 B = normalize(cross(N, T));
    B *= argT;
    return float3x3(T, B, N);
}

// DECLARATIONS lighting_models
float PhongSpecularLegacy(float3 light_dir, float3 reflection_dir, float specular_power) {
    return pow(saturate(dot(light_dir, reflection_dir)), specular_power) * ((specular_power + 2.f) / (2.f * 3.14159f));

    // float3 view_dir = reflection_dir - dot(surface_normal, reflection_dir) * surface_normal * 2.0f;
    // specular_power = 0.01f;
    // return pow( saturate( dot( light_dir, reflection_dir ) ), specular_power ) * ( (specular_power + 2.f) / 2.0f ) *
    // max(dot( light_dir, surface_normal ), 0.0f); return pow( saturate( dot( light_dir, reflection_dir ) ),
    // specular_power ) * ( (specular_power + 1.f) / 2.0f ); specular_power = 1.0f; float3 half_vec =
    // normalize(-view_dir + light_dir); return pow(saturate(dot(half_vec, surface_normal)), specular_power) *
    // (specular_power + 4.0f) / (8.0f);
}

float BlinnPhongSpecular(float3 light_dir, float3 surface_normal, float3 view_dir, float specular_power) {
    float3 half_vec = normalize(-view_dir + light_dir);
    return pow(saturate(dot(half_vec, surface_normal)), specular_power) * (specular_power + 4.0f) / (8.0f);
}

float Fresnel(float VdotH) { return exp2((-5.55473 * VdotH - 6.98316) * VdotH); }

float GlossinessToRoughness(float glossiness) { return saturate(1.0f - glossiness); }

float GeometryOcclusionSmithTerm(float alpha, float VdotN, float LdotN) // * VdotN * LdotN
{
    // geometric term
    float k = alpha * 0.5;
    float G_SmithI = (VdotN * (1.0 - k) + k);
    float G_SmithO = (LdotN * (1.0 - k) + k);
    return 1.0f / (G_SmithI * G_SmithO);
}

float3 FresnelTerm(float VdotH, float3 specular_color) {
    float3 fresnel_amount = Fresnel(VdotH);
    return lerp(fresnel_amount, float3(1.0f, 1.0f, 1.0f), specular_color);
}

float GGXMicrofacetDistribution(float NdotH, float alpha2) // / pi
{
    return alpha2 / pow((alpha2 - 1.0f) * NdotH * NdotH + 1.0f, 2.0f);
}

// GGX specular is from here : https://www.graphics.cornell.edu/~bjw/microfacetbsdf.pdf
//  "Microfacet Models for Refraction through Rough Surfaces Bruce Walter et al"
float3 GGXSpecular(float3 light_dir, float3 surface_normal, float3 view_dir, float glossiness, float3 specular_color) {
    float roughness = GlossinessToRoughness(glossiness);
    float alpha = max(roughness * roughness, 2e-3); // \alpha_g
    float alpha2 = alpha * alpha;                   // {\alpha_g}^2

    float3 I = -view_dir;        // in vector //same as V
    float3 O = light_dir;        // out vector //same as L
    float3 H = normalize(I + O); // half vector
    float3 N = surface_normal;   // macro normal
    float NdotH = saturate(dot(N, H));
    float OdotN = saturate(dot(O, N)); // LdotH
    float IdotN = saturate(dot(I, N)); // VdotN
    float IdotH = saturate(dot(I, H)); // VdotH

    float3 incident_light = OdotN;

    const float pi = 3.141592f;

    float D = GGXMicrofacetDistribution(NdotH, alpha2) /*/ pi*/;
    float G = GeometryOcclusionSmithTerm(alpha, IdotN, OdotN) /* * IdotN * OdotN*/;
    float3 F = FresnelTerm(IdotH, specular_color);

    // * IdotN * OdotN are eliminated terms from Smith multiplier
    // pi technically should be here, but we instead account for it in the light color to make sure that white surfaces
    // look white in (1, 1, 1) light :
    // https://seblagarde.wordpress.com/2012/01/08/pi-or-not-to-pi-in-game-lighting-equation/
    float3 res = D * F * G * OdotN / (4.0f /* * IdotN * OdotN*/) /* * pi*/;

    return res;
}

float WardSpecular(float3 light_dir, float3 view_dir, float3 normal, float3 fiber_dir, float fiber_roughness,
                   float tangent_roughness) {
    float eps = 1e-5f;
    view_dir = -view_dir;
    float3 fiber_tangent = cross(normal, fiber_dir);
    fiber_tangent /= length(fiber_tangent) + eps;
    const float PI = 3.1415f;
    float NdotL = dot(normal, light_dir);
    float NdotR = dot(normal, view_dir);

    if (NdotL <= 0.0f || NdotR <= 0.0f) {
        return 0.0f;
    }

    float3 H = normalize(light_dir + view_dir);

    float NdotH = dot(normal, H);
    float XdotH = dot(fiber_dir, H);
    float YdotH = dot(fiber_tangent, H);

    // float coeff = sqrt(NdotL / NdotR) / (4.0 * PI * fiber_roughness * tangent_roughness);
    fiber_roughness = max(eps, fiber_roughness);
    tangent_roughness = max(eps, tangent_roughness);
    /*float mult = 1.0f / (4.0f * PI * fiber_roughness * tangent_roughness * sqrt(NdotL * NdotR) + 1e-5f);
    float exp_power = -2.0f * (pow(XdotH / fiber_roughness, 2.0f) + pow(YdotH / tangent_roughness, 2.0f)) / (1.0f +
    NdotH);

    return mult * exp(exp_power);*/

    float coeff =
        sqrt(max(0.0f, NdotL / max(eps, NdotR))) / max(eps, /*4.0 * PI * */ 4.0f * fiber_roughness * tangent_roughness);
    float2 v = float2(XdotH / fiber_roughness, YdotH / tangent_roughness);
    float theta = dot(v, v) / max(eps, 1.0 + NdotH);

    return coeff * exp(-2.0 * theta);
}

void ComputeParticleLight(float3 incident_light, out float3 diffuse, out float3 subsurface_light, out float3 specular) {
    diffuse = incident_light;
    subsurface_light = 0.0f;
    specular = float3(0.0f, 0.0f, 0.0f);
}

void ComputeDiffuse(float3 light_direction, float3 incident_light, float3 surface_normal, float translucency,
                    float front_scattering, out float3 diffuse, out float3 subsurface_light) {
    float ndotl = dot(light_direction, surface_normal);
    diffuse = saturate(ndotl) * incident_light;

#if defined(SUBSURFACE_SCATTERING)
    subsurface_light = lerp(saturate(1.0f - ndotl), 1.0f, front_scattering) *
                       saturate((ndotl + translucency) / (1.0f + translucency)) * incident_light;
#else
    subsurface_light = 0.0f;
#endif
}

float ComputeBentNormalShadow(float3 light_dir, float3 bent_normal, float sharpness) {
    float3 bent_normal_dir = normalize(bent_normal);
    float bent_cone_angle = length(bent_normal);

    return saturate((dot(-bent_normal_dir, light_dir) - (1.0f - bent_cone_angle)) * sharpness);
}

void ComputeLight(in LightData light_data, in SurfaceData surface_data, inout EmissionData emission_data) {
#if (defined(PARTICLE) || defined(FORCE_OMNILIGHT)) && !defined(FORCE_ACCURATE_LIGHTING)
    emission_data.diffuse += light_data.intensity * surface_data.material.albedo_color.rgb;
    emission_data.total += light_data.intensity;
#else
    float3 diffuse_light = 0.0f;
    float3 subsurface_light = 0.0f;
    ComputeDiffuse(light_data.direction, light_data.intensity, surface_data.world_normal,
                   surface_data.material.subsurface_translucency, surface_data.material.subsurface_front_scattering,
                   diffuse_light, subsurface_light);
    float3 specular_light = 0.0f;
#if defined(GGX)
    specular_light = GGXSpecular(light_data.direction, surface_data.world_normal, surface_data.view_dir,
                                 surface_data.material.glossiness, surface_data.material.specular_color) *
                     light_data.intensity;
#elif defined(BLINN_PHONG)
    specular_light = BlinnPhongSpecular(light_data.direction, surface_data.world_normal, surface_data.view_dir,
                                        surface_data.material.specular_exponent) *
                     surface_data.material.specular_color * light_data.intensity;
#elif defined(ANISOTROPY)
    specular_light = WardSpecular(light_data.direction, surface_data.view_dir, surface_data.world_normal,
                                  surface_data.material.world_fiber_dir, surface_data.material.fiber_roughness,
                                  surface_data.material.tangent_roughness) *
                     surface_data.material.specular_color * light_data.intensity;
#elif defined(PHONG)
    specular_light =
        PhongSpecularLegacy(light_data.direction, surface_data.reflected_dir, surface_data.material.specular_exponent) *
        surface_data.material.specular_color * light_data.intensity;
#endif

    emission_data.total += light_data.intensity;
    emission_data.diffuse += diffuse_light * surface_data.material.albedo_color.rgb;
    emission_data.specular += specular_light;
#if defined(SUBSURFACE_SCATTERING)
    emission_data.subsurface += subsurface_light * surface_data.material.subsurface_color;
#endif
#endif
}

// DECLARATIONS directional_light
void ComputeDirectionalLightParams(float4 light_direction_data, float4 light_colour_data, out LightData light_data) {
    light_data.direction = -light_direction_data.xyz;
    light_data.intensity = light_colour_data.rgb;
}

// DECLARATIONS point_light
float PointCutoffFunc(float ratio) {
    float d = saturate(1.0f - ratio * ratio * ratio * ratio);
    return d * d;
}
void ComputePointLightParams(float4 light_position_data, float4 light_colour_data, float3 fragment_pos,
                             out LightData light_data) {
    float3 to_light_vector = light_position_data.xyz - fragment_pos.xyz;
    float distance = length(to_light_vector);
    light_data.direction = to_light_vector / distance;

    /*float cutoff_radius = light_colour_data.a;
    float source_radius = cutoff_radius * 0.2f;
    float zero_intensity = 15.0f;

    float mult = zero_intensity / (1.0f / pow(source_radius, 2.0f) - 1.0f / pow(cutoff_radius +
    source_radius, 2.0f));

    float attenuation = max(0.0f, mult * (1.0f / pow(distance + source_radius, 2.0f) - 1.0f / pow(cutoff_radius
    + source_radius, 2.0f))); light_data.intensity = light_colour_data.rgb * attenuation;*/

    /*float radius_mult = 1.0f;
    float falloff_mult = 1.0f;
    float cutoff_radius = light_colour_data.a * radius_mult;
    float ratio = saturate(distance / cutoff_radius);
    float smooth_ratio = 0.02f / falloff_mult / radius_mult / radius_mult;
    float zero_intensity = 10.0f * falloff_mult;

    float mult = zero_intensity / (1.0f / smooth_ratio - 1.0f / (1.0f + smooth_ratio));

    float attenuation = max(0.0f, mult * (1.0f / (ratio * ratio + smooth_ratio) - 1.0f / (1.0f +
    smooth_ratio)));*/

    float cutoff_radius = light_colour_data.a;
    float light_radius_mult = lerp(100.0f, 1.0f / sqrt(0.02), saturate(light_position_data.a));
    float zero_intensity = 10.0f * 0.02f * light_radius_mult * light_radius_mult;
    float light_radius = cutoff_radius / light_radius_mult;
    float divisor = (distance / light_radius + 1.0f);
    float cutoff_mult = PointCutoffFunc(distance / cutoff_radius);
    float attenuation = min(10.0f, zero_intensity * cutoff_mult / (divisor * divisor));

    light_data.intensity = light_colour_data.rgb * attenuation;
}

float4 GetChannelMask(float channel_index) {
    float4 output = 0.f;
    if (channel_index > -0.5f) {
        if (channel_index >= 2.5f)
            output.w = 1.f;
        else if (channel_index >= 1.5f)
            output.z = 1.f;
        else if (channel_index >= 0.5f)
            output.y = 1.f;
        else
            output.x = 1.f;
    }
    return output;
}

// # Fragment support types and logic

#define GRAPH_STATE_STRUCT_NAME(Shader, Name) GraphState__##Shader##__##Name

#define GRAPH_IO(Name, Index) GRAPH_IO_TYPE(Name) GRAPH_IO_NAME(Name, Index)
#define GRAPH_IO_TYPE(Name) GraphNode__##Name
#define GRAPH_IO_NAME(Name, Index) Node__##Name##__##Index
#define GRAPH_IO_FIELD(Name, Index) (self.GRAPH_IO_NAME(Name, Index))

struct GraphNode__Zero {
    float output;
};

struct GraphNode__SpecularColor {};

struct GraphNode__ConstantPixel {
    float output;
};

struct GraphNode__SpecularPower {};

struct GraphNode__InputUV {
    float2 output;
};

struct GraphNode__SampleTexture {
    float4 rgba;
    float r;
    float g;
    float b;
    float a;
};

struct GraphNode__FromVertexColor {
    float4 output;
};

struct GraphNode__Multiply4 {
    float4 output;
};

struct GraphNode__AlbedoColor {};

struct GraphNode__InputIndirectColor {
    float4 output;
};

struct GraphNode__Glossiness {};

struct GraphNode__NormalTexToTbn {
    float3 tbn_normal;
};

struct GraphNode__IndirectColor {};

struct GraphNode__TbnNormal {};

struct GraphNode__Multiply3 {
    float3 output;
};

struct GraphNode__InputEmissiveColor {
    float3 output;
};

struct GraphNode__ConstantPixel3 {
    float3 output;
};

struct GraphNode__Add3 {
    float3 output;
};

struct GraphNode__EmissiveColor {};

struct GraphNode__InputGlossiness {
    float output;
};

struct GraphNode__InputVertexNormal {
    float3 output;
};

struct GraphNode__InputVertexPosition {
    float4 output;
};

struct GraphNode__InputVertexTangent {
    float4 output;
};

struct GraphNode__VertexNormal {};
struct GraphNode__VertexPosition {};
struct GraphNode__VertexTangent {};

struct GraphNode__Multiply {
    float output;
};

struct GraphNode__Saturate {
    float output;
};

struct GraphNode__SkinVectors {
    float4 out_position;
    float3 out_normal;
    float4 out_tangent;
};

struct GRAPH_STATE_STRUCT_NAME(VS, Art__Textures__NPC__Octavius__Octaviusc) {
    GRAPH_IO(InputVertexPosition, 0);
    GRAPH_IO(InputVertexNormal, 0);
    GRAPH_IO(InputVertexTangent, 0);
    GRAPH_IO(SkinVectors, 0);
    GRAPH_IO(VertexPosition, 0);
    GRAPH_IO(VertexNormal, 0);
    GRAPH_IO(VertexTangent, 0);
};

struct GRAPH_STATE_STRUCT_NAME(PS, Art__Textures__NPC__Octavius__Octaviusc) {
    GRAPH_IO(Zero, 0);
    GRAPH_IO(SpecularColor, 0);
    GRAPH_IO(ConstantPixel, 0);
    GRAPH_IO(SpecularPower, 0);
    GRAPH_IO(InputUV, 0);
    GRAPH_IO(SampleTexture, 0);
    GRAPH_IO(FromVertexColor, 0);
    GRAPH_IO(Multiply4, 0);
    GRAPH_IO(AlbedoColor, 0);
};

struct GRAPH_STATE_STRUCT_NAME(PS, Metadata__Materials__SpecGloss) {
    GRAPH_IO(InputUV, 0);

    GRAPH_IO(SampleTexture, 0);
    GRAPH_IO(SampleTexture, 1);
    GRAPH_IO(InputIndirectColor, 0);
    GRAPH_IO(SampleTexture, 2);

    GRAPH_IO(AlbedoColor, 0);
    GRAPH_IO(Glossiness, 0);
    GRAPH_IO(NormalTexToTbn, 0);
    GRAPH_IO(IndirectColor, 0);
    GRAPH_IO(SpecularColor, 0);

    GRAPH_IO(TbnNormal, 0);
};

struct GRAPH_STATE_STRUCT_NAME(PS, Metadata__Materials__GlowAdd) {
    GRAPH_IO(InputUV, 0);
    GRAPH_IO(SampleTexture, 0);
    GRAPH_IO(ConstantPixel3, 0);
    GRAPH_IO(Multiply3, 0);
    GRAPH_IO(ConstantPixel, 0);
    GRAPH_IO(InputEmissiveColor, 0);
    GRAPH_IO(Multiply3, 1);
    GRAPH_IO(Add3, 0);
    GRAPH_IO(EmissiveColor, 0);
};

struct GRAPH_STATE_STRUCT_NAME(PS, Metadata__Materials__Utility__TWEAK_Gloss) {
    GRAPH_IO(InputGlossiness, 0);
    GRAPH_IO(ConstantPixel, 0);
    GRAPH_IO(Multiply, 0);
    GRAPH_IO(Saturate, 0);
    GRAPH_IO(Glossiness, 0);
};

#define RUN_GRAPH_STATE_NAME(Shader, Name, Stage) RunGraphState__##Shader##__##Name##__##Stage
#define RUN_GRAPH_STATE_DECLARATION_VS(Name, Stage)                                                                    \
    void RUN_GRAPH_STATE_NAME(VS, Name, Stage)(inout GRAPH_STATE_STRUCT_NAME(VS, Name) self, in VSIn input,            \
                                               inout VertexData vertexData, inout SemanticsData semanticsData)

#define RUN_GRAPH_STATE_DECLARATION_PS(Name, Stage)                                                                    \
    void RUN_GRAPH_STATE_NAME(PS, Name, Stage)(inout GRAPH_STATE_STRUCT_NAME(PS, Name) self,                           \
                                               inout EmissionData emissionData, inout SurfaceData surfaceData,         \
                                               inout SemanticsData semanticsData)

#define INIT_GRAPH_STATE_NAME(Shader, Name) InitGraphState__##Shader##__##Name

#define INIT_GRAPH_STATE_DECLARATION_VS(Name)                                                                          \
    void INIT_GRAPH_STATE_NAME(VS, Name)(inout GRAPH_STATE_STRUCT_NAME(VS, Name) self, in VSIn input,                  \
                                         inout VertexData vertexData, inout SemanticsData semanticsData)

#define INIT_GRAPH_STATE_DECLARATION_PS(Name)                                                                          \
    void INIT_GRAPH_STATE_NAME(PS, Name)(inout GRAPH_STATE_STRUCT_NAME(PS, Name) self,                                 \
                                         inout EmissionData emissionData, inout SurfaceData surfaceData,               \
                                         inout SemanticsData semanticsData)

// TODO(LV): Make these actual uniforms
#define cp__Art__Textures__NPC__Octavius__Octaviusc___SampleTexture__0_P0 tex_AlbedoTransparency_TEX
#define cp__Art__Textures__NPC__Octavius__Octaviusc___ConstantPixel__0_P0 5.0

#define cp__Metadata__Materials__SpecGloss___NormalTexToTbn__0_P0 1
#define cp__Metadata__Materials__SpecGloss___SampleTexture__0_P0 tex_AlbedoTransparency_TEX
#define cp__Metadata__Materials__SpecGloss___SampleTexture__0_P1 SamplerDynamicWrap
#define cp__Metadata__Materials__SpecGloss___SampleTexture__1_P0 tex_AlbedoTransparency_TEX
#define cp__Metadata__Materials__SpecGloss___SampleTexture__1_P1 SamplerDynamicWrap
#define cp__Metadata__Materials__SpecGloss___SampleTexture__2_P0 tex_AlbedoTransparency_TEX
#define cp__Metadata__Materials__SpecGloss___SampleTexture__2_P1 SamplerDynamicWrap

#define cp__Metadata__Materials__Utility__TWEAK_Gloss___ConstantPixel__0_P0 1.0499999523162842

#define cp__Metadata__Materials__GlowAdd___ConstantPixel__0_P0 1.0
#define cp__Metadata__Materials__GlowAdd___ConstantPixel3__0_P0 float3(1.0, 1.0, 1.0)
#define cp__Metadata__Materials__GlowAdd___SampleTexture__0_P0 tex_Glow_TEX
#define cp__Metadata__Materials__GlowAdd___SampleTexture__0_P1 SamplerDynamicWrap

#define GRAPH_CP_VALUE(GraphName, Node, Index, Param) cp__##GraphName##___##Node##__##Index##_P##Param

void Code__Add3(in float3 a, in float3 b, out float3 output) { output = a + b; }

void Code__AlbedoColor(in float4 input, inout SurfaceData surfaceData : surface_data) {
    surfaceData.material.albedo_color = input;
}

void Code__ConstantPixel(float constant_pixel, out float output) { output = constant_pixel; }

void Code__ConstantPixel3(float3 constant_pixel3, out float3 output) { output = constant_pixel3; }

void Code__EmissiveColor(in float3 input, inout SurfaceData surfaceData : surface_data) {
    surfaceData.material.emissive_color = input;
}

void Code__FromVertexColor(inout SemanticsData semanticsData : surface_data, out float4 output) {
    output = semanticsData.color0;
}

void Code__Glossiness(in float input, inout SurfaceData surfaceData : surface_data) {
#ifdef PBR
    surfaceData.material.glossiness = input;
#endif
}

void Code__IndirectColor(in float4 input, inout SurfaceData surfaceData : surface_data) {
    surfaceData.material.indirect_color = input;
}

void Code__InputEmissiveColor(inout SurfaceData surfaceData : surface_data, out float3 output) {
    output = surfaceData.material.emissive_color;
}

void Code__InputGlossiness(inout SurfaceData surfaceData : surface_data, out float output) {
#ifdef PBR
    output = surfaceData.material.glossiness;
#endif
}

void Code__InputIndirectColor(inout SurfaceData surfaceData : surface_data, out float4 output) {
    output = surfaceData.material.indirect_color;
}

void Code__InputUV(inout SurfaceData surfaceData : surface_data, out float2 output) { output = surfaceData.uv; }

void Code__InputVertexNormal(inout VertexData vertexData : vertex_data, out float3 value) { value = vertexData.normal; }

void Code__InputVertexPosition(inout VertexData vertexData : vertex_data, out float4 value) {
    value = vertexData.position;
}

void Code__InputVertexTangent(inout VertexData vertexData : vertex_data, out float4 value) {
    value = vertexData.tangent;
}

void Code__Multiply(in float a, in float b, out float output) { output = a * b; }

void Code__Multiply3(in float3 a, in float3 b, out float3 output) { output = a * b; }

void Code__Multiply4(in float4 a, in float4 b, out float4 output) { output = a * b; }

void Code__NormalTexToTbn(float scale, in float x, in float y, out float3 tbn_normal) {
    tbn_normal.xy = (2.0f * float2(x, y) - 1.0f) * scale;
    tbn_normal.z = sqrt(max(1.0f - dot(tbn_normal.xy, tbn_normal.xy), 0.0f));
}

void Code__SampleTexture(Texture2D input_texture, SamplerState input_sampler, in float2 uv, out float4 rgba,
                         out float r, out float g, out float b, out float a) {
    rgba = SAMPLE_TEX2D(input_texture, input_sampler, uv);
    r = rgba.r;
    g = rgba.g;
    b = rgba.b;
    a = rgba.a;
}

void Code__Saturate(in float input, out float output) { output = saturate(input); }

void Code__SkinVectors(in float4 in_position, in float3 in_normal, in float4 in_tangent, in uint4 bone_indices
                       : BLENDINDICES, in float4 blend_weights
                       : BLENDWEIGHT, out float4 out_position, out float3 out_normal, out float4 out_tangent) {
    out_position = 0.0f;
    out_normal = 0.0f;
    out_tangent = float4(0.0f, 0.0f, 0.0f, in_tangent.w);

    float last_weight = 0.0f;

    // code would have been way more compact with for()-loop but d3d9 compiler generates really poor code for it

    float4x4 matrix0 = ReadAnimationPalette(bone_indices.x);
    out_position += mul(in_position, matrix0) * blend_weights.x;
    out_normal += mul(in_normal, (float3x3)matrix0) * blend_weights.x;
    out_tangent.xyz += mul(in_tangent.xyz, (float3x3)matrix0) * blend_weights.x;

    float4x4 matrix1 = ReadAnimationPalette(bone_indices.y);
    out_position += mul(in_position, matrix1) * blend_weights.y;
    out_normal += mul(in_normal, (float3x3)matrix1) * blend_weights.y;
    out_tangent.xyz += mul(in_tangent.xyz, (float3x3)matrix1) * blend_weights.y;

    float4x4 matrix2 = ReadAnimationPalette(bone_indices.z);
    out_position += mul(in_position, matrix2) * blend_weights.z;
    out_normal += mul(in_normal, (float3x3)matrix2) * blend_weights.z;
    out_tangent.xyz += mul(in_tangent.xyz, (float3x3)matrix2) * blend_weights.z;

    // Work out the weighting of the last bone
    last_weight = 1.0f - blend_weights.x - blend_weights.y - blend_weights.z;

    // Add contribution of the last bone
    float4x4 matrix3 = ReadAnimationPalette(bone_indices.w);
    out_position += mul(in_position, matrix3) * last_weight;
    out_normal += mul(in_normal, (float3x3)matrix3) * last_weight;
    out_tangent.xyz += mul(in_tangent.xyz, (float3x3)matrix3) * last_weight;

    out_normal = normalize(out_normal);
    out_tangent.xyz = normalize(out_tangent.xyz);
}

void Code__SpecularColor(in float3 input, inout SurfaceData surfaceData : surface_data) {
    surfaceData.material.specular_color = input;
}

void Code__SpecularPower(in float input, inout SurfaceData surfaceData : surface_data) {
#if defined(PHONG)
    surfaceData.material.specular_exponent = input;
#endif
}

void Code__TbnNormal(in float3 input, inout SurfaceData surfaceData : surface_data) { surfaceData.tbn_normal = input; }

void Code__VertexNormal(in float3 value, inout VertexData vertexData : vertex_data) { vertexData.normal = value; }

void Code__VertexPosition(in float4 value, inout VertexData vertexData : vertex_data) { vertexData.position = value; }

void Code__VertexTangent(in float4 value, inout VertexData vertexData : vertex_data) { vertexData.tangent = value; }

void Code__Zero(out float output) { output = 0.f; }

#define VERTEX_ATTRIBUTE(Name, Semantic) input.Name

#define CURRENT_GRAPH(X) Art__Textures__NPC__Octavius__Octaviusc
INIT_GRAPH_STATE_DECLARATION_VS(CURRENT_GRAPH(0)) { self = (GRAPH_STATE_STRUCT_NAME(VS, CURRENT_GRAPH(0)))0; }

RUN_GRAPH_STATE_DECLARATION_VS(CURRENT_GRAPH(0), WorldTransform) {
    Code__InputVertexPosition(vertexData, GRAPH_IO_FIELD(InputVertexPosition, 0).output);
    Code__InputVertexNormal(vertexData, GRAPH_IO_FIELD(InputVertexNormal, 0).output);
    Code__InputVertexTangent(vertexData, GRAPH_IO_FIELD(InputVertexTangent, 0).output);

    Code__SkinVectors(GRAPH_IO_FIELD(InputVertexPosition, 0).output, GRAPH_IO_FIELD(InputVertexNormal, 0).output,
                      GRAPH_IO_FIELD(InputVertexTangent, 0).output, VERTEX_ATTRIBUTE(bone_indices, BLENDINDICES),
                      VERTEX_ATTRIBUTE(blend_weights, BLENDWEIGHT), GRAPH_IO_FIELD(SkinVectors, 0).out_position,
                      GRAPH_IO_FIELD(SkinVectors, 0).out_normal, GRAPH_IO_FIELD(SkinVectors, 0).out_tangent);

    Code__VertexPosition(GRAPH_IO_FIELD(SkinVectors, 0).out_position, vertexData);
    Code__VertexNormal(GRAPH_IO_FIELD(SkinVectors, 0).out_normal, vertexData);
    Code__VertexTangent(GRAPH_IO_FIELD(SkinVectors, 0).out_tangent, vertexData);
}

INIT_GRAPH_STATE_DECLARATION_PS(CURRENT_GRAPH(0)) {
    self = (GRAPH_STATE_STRUCT_NAME(PS, CURRENT_GRAPH(0)))0;

    Code__Zero(GRAPH_IO_FIELD(Zero, 0).output);

    Code__ConstantPixel(GRAPH_CP_VALUE(CURRENT_GRAPH(0), ConstantPixel, 0, 0), GRAPH_IO_FIELD(ConstantPixel, 0).output);

    Code__FromVertexColor(semanticsData, GRAPH_IO_FIELD(FromVertexColor, 0).output);
}

RUN_GRAPH_STATE_DECLARATION_PS(Art__Textures__NPC__Octavius__Octaviusc, Texturing_Init) {
    Code__SpecularColor(GRAPH_IO_FIELD(Zero, 0).output, surfaceData);

    Code__SpecularPower(GRAPH_IO_FIELD(ConstantPixel, 0).output, surfaceData);

    Code__InputUV(surfaceData, GRAPH_IO_FIELD(InputUV, 0).output);

    Code__SampleTexture(tex_AlbedoTransparency_TEX, smp_AlbedoTransparency_TEX, GRAPH_IO_FIELD(InputUV, 0).output,
                        GRAPH_IO_FIELD(SampleTexture, 0).rgba, GRAPH_IO_FIELD(SampleTexture, 0).r,
                        GRAPH_IO_FIELD(SampleTexture, 0).g, GRAPH_IO_FIELD(SampleTexture, 0).b,
                        GRAPH_IO_FIELD(SampleTexture, 0).a);

    Code__Multiply4(GRAPH_IO_FIELD(SampleTexture, 0).rgba, GRAPH_IO_FIELD(FromVertexColor, 0).output,
                    GRAPH_IO_FIELD(Multiply4, 0).output);

    Code__AlbedoColor(GRAPH_IO_FIELD(Multiply4, 0).output, surfaceData);
}
#undef CURRENT_GRAPH

#define CURRENT_GRAPH(X) Metadata__Materials__SpecGloss
INIT_GRAPH_STATE_DECLARATION_PS(Metadata__Materials__SpecGloss) {
    self = (GRAPH_STATE_STRUCT_NAME(PS, CURRENT_GRAPH(0)))0;
}

RUN_GRAPH_STATE_DECLARATION_PS(Metadata__Materials__SpecGloss, Texturing_Init) {
    Code__InputUV(surfaceData, GRAPH_IO_FIELD(InputUV, 0).output);

    Code__SampleTexture(tex_AlbedoTransparency_TEX, smp_AlbedoTransparency_TEX, GRAPH_IO_FIELD(InputUV, 0).output,
                        GRAPH_IO_FIELD(SampleTexture, 0).rgba, GRAPH_IO_FIELD(SampleTexture, 0).r,
                        GRAPH_IO_FIELD(SampleTexture, 0).g, GRAPH_IO_FIELD(SampleTexture, 0).b,
                        GRAPH_IO_FIELD(SampleTexture, 0).a);

    Code__SampleTexture(tex_NormalGlossAO_TEX, smp_NormalGlossAO_TEX, GRAPH_IO_FIELD(InputUV, 0).output,
                        GRAPH_IO_FIELD(SampleTexture, 1).rgba, GRAPH_IO_FIELD(SampleTexture, 1).r,
                        GRAPH_IO_FIELD(SampleTexture, 1).g, GRAPH_IO_FIELD(SampleTexture, 1).b,
                        GRAPH_IO_FIELD(SampleTexture, 1).a);

    Code__InputIndirectColor(surfaceData, GRAPH_IO_FIELD(InputIndirectColor, 0).output);

    Code__SampleTexture(tex_SpecularColour_TEX, SamplerDynamicWrap, GRAPH_IO_FIELD(InputUV, 0).output,
                        GRAPH_IO_FIELD(SampleTexture, 2).rgba, GRAPH_IO_FIELD(SampleTexture, 2).r,
                        GRAPH_IO_FIELD(SampleTexture, 2).g, GRAPH_IO_FIELD(SampleTexture, 2).b,
                        GRAPH_IO_FIELD(SampleTexture, 2).a);

    float4 Temp__Albedo__0__input;
    Temp__Albedo__0__input.xyz = GRAPH_IO_FIELD(SampleTexture, 0).rgba.xyz;
    Temp__Albedo__0__input.w = GRAPH_IO_FIELD(SampleTexture, 0).rgba.w;
    Code__AlbedoColor(Temp__Albedo__0__input, surfaceData);

    Code__Glossiness(GRAPH_IO_FIELD(SampleTexture, 1).rgba.w, surfaceData);

    Code__NormalTexToTbn(GRAPH_CP_VALUE(CURRENT_GRAPH(0), NormalTexToTbn, 0, 0),
                         GRAPH_IO_FIELD(SampleTexture, 1).rgba.x, GRAPH_IO_FIELD(SampleTexture, 1).rgba.y,
                         GRAPH_IO_FIELD(NormalTexToTbn, 0).tbn_normal);

    float4 Temp__IndirectColor__0__input;
    Temp__IndirectColor__0__input.xyz = GRAPH_IO_FIELD(InputIndirectColor, 0).output.xyz;
    Temp__IndirectColor__0__input.w = GRAPH_IO_FIELD(SampleTexture, 1).rgba.z;
    Code__IndirectColor(Temp__IndirectColor__0__input, surfaceData);

    Code__SpecularColor(GRAPH_IO_FIELD(SampleTexture, 2).rgba.xyz, surfaceData);

    Code__TbnNormal(GRAPH_IO_FIELD(NormalTexToTbn, 0).tbn_normal, surfaceData);
}
#undef CURRENT_GRAPH

#define CURRENT_GRAPH(X) Metadata__Materials__GlowAdd
INIT_GRAPH_STATE_DECLARATION_PS(CURRENT_GRAPH(0)) {
    self = (GRAPH_STATE_STRUCT_NAME(PS, CURRENT_GRAPH(0)))0;

    Code__ConstantPixel3(GRAPH_CP_VALUE(CURRENT_GRAPH(0), ConstantPixel3, 0, 0),
                         GRAPH_IO_FIELD(ConstantPixel3, 0).output);

    Code__ConstantPixel(GRAPH_CP_VALUE(CURRENT_GRAPH(0), ConstantPixel, 0, 0), GRAPH_IO_FIELD(ConstantPixel, 0).output);
}

RUN_GRAPH_STATE_DECLARATION_PS(Metadata__Materials__GlowAdd, Texturing_Init) {
    Code__InputUV(surfaceData, GRAPH_IO_FIELD(InputUV, 0).output);

    Code__SampleTexture(
        GRAPH_CP_VALUE(CURRENT_GRAPH(0), SampleTexture, 0, 0), GRAPH_CP_VALUE(CURRENT_GRAPH(0), SampleTexture, 0, 1),
        GRAPH_IO_FIELD(InputUV, 0).output, GRAPH_IO_FIELD(SampleTexture, 0).rgba, GRAPH_IO_FIELD(SampleTexture, 0).r,
        GRAPH_IO_FIELD(SampleTexture, 0).g, GRAPH_IO_FIELD(SampleTexture, 0).b, GRAPH_IO_FIELD(SampleTexture, 0).a);

    Code__Multiply3(GRAPH_IO_FIELD(SampleTexture, 0).rgba.xyz, GRAPH_IO_FIELD(ConstantPixel3, 0).output,
                    GRAPH_IO_FIELD(Multiply3, 0).output);

    Code__Multiply3(GRAPH_IO_FIELD(Multiply3, 0).output, GRAPH_IO_FIELD(ConstantPixel, 0).output,
                    GRAPH_IO_FIELD(Multiply3, 1).output);

    Code__InputEmissiveColor(surfaceData, GRAPH_IO_FIELD(InputEmissiveColor, 0).output);

    Code__Add3(GRAPH_IO_FIELD(InputEmissiveColor, 0).output, GRAPH_IO_FIELD(Multiply3, 1).output,
               GRAPH_IO_FIELD(Add3, 0).output);

    Code__EmissiveColor(GRAPH_IO_FIELD(Add3, 0).output, surfaceData);
}
#undef CURRENT_GRAPH

#define CURRENT_GRAPH(X) Metadata__Materials__Utility__TWEAK_Gloss
INIT_GRAPH_STATE_DECLARATION_PS(CURRENT_GRAPH(0)) {
    self = (GRAPH_STATE_STRUCT_NAME(PS, CURRENT_GRAPH(0)))0;

    Code__ConstantPixel(GRAPH_CP_VALUE(CURRENT_GRAPH(0), ConstantPixel, 0, 0), GRAPH_IO_FIELD(ConstantPixel, 0).output);
}

RUN_GRAPH_STATE_DECLARATION_PS(CURRENT_GRAPH(0), Texturing) {
    Code__InputGlossiness(surfaceData, GRAPH_IO_FIELD(InputGlossiness, 0).output);

    Code__Multiply(GRAPH_IO_FIELD(InputGlossiness, 0).output, GRAPH_IO_FIELD(ConstantPixel, 0).output,
                   GRAPH_IO_FIELD(Multiply, 0).output);

    Code__Saturate(GRAPH_IO_FIELD(Multiply, 0).output, GRAPH_IO_FIELD(Saturate, 0).output);

    Code__Glossiness(GRAPH_IO_FIELD(Saturate, 0).output, surfaceData);
}

// # Vertex and pixel shader entrypoints
#define GRAPH_STATE_VAR_NAME(Name) graphState__##Name

// ## Vertex shader

#define GRAPH_STATE_VAR(Name) GRAPH_STATE_STRUCT_NAME(VS, Name) GRAPH_STATE_VAR_NAME(Name)

#define INIT_GRAPH_STATE(Name)                                                                                         \
    INIT_GRAPH_STATE_NAME(VS, Name)(GRAPH_STATE_VAR_NAME(Name), input, vertexData, semanticsData)

#define RUN_GRAPH_STATE(Name, Stage)                                                                                   \
    RUN_GRAPH_STATE_NAME(VS, Name, Stage)                                                                              \
    (GRAPH_STATE_VAR_NAME(Name), input, vertexData, semanticsData)

void vs_main(
#ifdef VERTEX_OUTPUT_POSITION
    out float4 projected_position
    : POSITION, out float4 world_position
    : TEXCOORD0,
#endif
#ifdef VERTEX_OUTPUT_TBN
      out float3 normal
    : TEXCOORD5, out float3 tangent
    : TEXCOORD6, out float3 binormal
    : TEXCOORD7,
#endif
#ifdef VERTEX_OUTPUT_COLOR0
      out float4 color0
    : COLOR0,
#endif
#ifdef VERTEX_OUTPUT_COLOR1
      out float4 color1
    : COLOR1,
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD1
      out float2 uv1
    : TEXCOORD1,
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD2
      out float4 uv2
    : TEXCOORD2,
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD3
      out float4 uv3
    : TEXCOORD3,
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD4
      out float4 uv4
    : TEXCOORD4,
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD8
      out float4 uv8
    : TEXCOORD8,
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD9
      out float4 uv9
    : TEXCOORD9,
#endif
#ifdef VERTEX_OUTPUT_SEED
      out float random_seed
    : COLOR2,
#endif
#ifdef VERTEX_OUTPUT_UV_SET_2
      out float2 uv_set_2
    : COLOR3,
#endif
#ifdef VERTEX_OUTPUT_MODEL_ORIGIN
      out float3 model_origin
    : COLOR4,
#endif
      out float4 sv_position
    : SV_Position, in VSIn input) {
    VertexData vertexData;
    SemanticsData semanticsData;

    InitVertexData(vertexData);
    InitSemanticsData(semanticsData);

    GRAPH_STATE_VAR(Art__Textures__NPC__Octavius__Octaviusc);
    INIT_GRAPH_STATE(Art__Textures__NPC__Octavius__Octaviusc);

    vertexData.uv = input.tc0;
    vertexData.normal = input.normal.xyz;
    vertexData.tangent = input.tangent;
    vertexData.position = input.position;

    RUN_GRAPH_STATE(Art__Textures__NPC__Octavius__Octaviusc, WorldTransform);

#define IGNORE_SKINNING 0

#if IGNORE_SKINNING
    vertexData.normal = mul(input.normal.xyz, (float3x3)inv_world_transform);
    vertexData.tangent = float4(mul(input.tangent.xyz, (float3x3)inv_world_transform), input.tangent.w);
    vertexData.binormal = cross(vertexData.normal, vertexData.tangent.xyz) * input.tangent.w;

    vertexData.position = mul(input.position, world_transform);
#else
    vertexData.normal = mul(vertexData.normal, (float3x3)inv_world_transform);
    vertexData.tangent = float4(mul(vertexData.tangent.xyz, (float3x3)inv_world_transform), vertexData.tangent.w);
    vertexData.binormal = cross(vertexData.normal, vertexData.tangent.xyz) * vertexData.tangent.w;

    vertexData.position = mul(vertexData.position, world_transform);
#endif

    vertexData.projected_position = mul(vertexData.position, view_projection_transform);
    sv_position = vertexData.projected_position;
    // from effects
    // Position, TBN and UV should always output their last state so we get the values from vertexData
#ifdef VERTEX_OUTPUT_TEXCOORD1
    uv1 = vertexData.uv;
#endif
#ifdef VERTEX_OUTPUT_POSITION
    world_position = vertexData.position;
    projected_position = vertexData.projected_position;
#endif
#ifdef VERTEX_OUTPUT_TBN
    normal = vertexData.normal;
    tangent = vertexData.tangent.xyz;
    binormal = vertexData.binormal;
#endif

    // These ones are not exposed and can be set anywhere, so we get the values from semanticsData
#ifdef VERTEX_OUTPUT_COLOR0
    color0 = semanticsData.color0;
#endif
#ifdef VERTEX_OUTPUT_COLOR1
    color1 = semanticsData.color1;
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD2
    uv2 = semanticsData.uv2;
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD3
    uv3 = semanticsData.uv3;
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD4
    uv4 = semanticsData.uv4;
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD8
    uv8 = semanticsData.uv8;
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD9
    uv9 = semanticsData.uv9;
#endif
#ifdef VERTEX_OUTPUT_SEED
    random_seed = semanticsData.seed;
#endif
#ifdef VERTEX_OUTPUT_UV_SET_2
    uv_set_2 = semanticsData.uv_set_2;
#endif
#ifdef VERTEX_OUTPUT_MODEL_ORIGIN
    model_origin = semanticsData.model_origin;
#endif
}

#undef INIT_GRAPH_STATE
#undef GRAPH_STATE_VAR
#undef RUN_GRAPH_STATE

// ## Pixel shader

#define GRAPH_STATE_VAR(Name) GRAPH_STATE_STRUCT_NAME(PS, Name) GRAPH_STATE_VAR_NAME(Name)

#define INIT_GRAPH_STATE(Name)                                                                                         \
    INIT_GRAPH_STATE_NAME(PS, Name)(GRAPH_STATE_VAR_NAME(Name), emissionData, surfaceData, semanticsData)

#define RUN_GRAPH_STATE(Name, Stage)                                                                                   \
    RUN_GRAPH_STATE_NAME(PS, Name, Stage)                                                                              \
    (GRAPH_STATE_VAR_NAME(Name), emissionData, surfaceData, semanticsData)

float4 ps_main(
#ifdef VERTEX_OUTPUT_POSITION
    in float4 projected_position
    : POSITION, in float4 world_position
    : TEXCOORD0,
#endif
#ifdef VERTEX_OUTPUT_TBN
      in float3 normal
    : TEXCOORD5, in float3 tangent
    : TEXCOORD6, in float3 binormal
    : TEXCOORD7,
#endif
#ifdef VERTEX_OUTPUT_COLOR0
      in float4 color0
    : COLOR0,
#endif
#ifdef VERTEX_OUTPUT_COLOR1
      in float4 color1
    : COLOR1,
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD1
      in float2 uv1
    : TEXCOORD1,
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD2
      in float4 uv2
    : TEXCOORD2,
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD3
      in float4 uv3
    : TEXCOORD3,
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD4
      in float4 uv4
    : TEXCOORD4,
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD8
      in float4 uv8
    : TEXCOORD8,
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD9
      in float4 uv9
    : TEXCOORD9,
#endif
#ifdef VERTEX_OUTPUT_SEED
      in float random_seed
    : COLOR2,
#endif
#ifdef VERTEX_OUTPUT_UV_SET_2
      in float2 uv_set_2
    : COLOR3,
#endif
#ifdef VERTEX_OUTPUT_MODEL_ORIGIN
      in float3 model_origin
    : COLOR4
#endif
    )
    : SV_Target {
    // PixelInput
    EmissionData emissionData = (EmissionData)0;
    SurfaceData surfaceData = (SurfaceData)0;
    SemanticsData semanticsData = (SemanticsData)0;
    {
        InitEmission(emissionData);
        InitSurface(surfaceData);
        InitSemanticsData(semanticsData);

#ifdef VERTEX_OUTPUT_TEXCOORD1
        semanticsData.uv1 = uv1;
#endif
#ifdef VERTEX_OUTPUT_POSITION
        semanticsData.world_pos = world_position;
#endif
#ifdef VERTEX_OUTPUT_TBN
        semanticsData.normal = normal;
        semanticsData.tangent = tangent;
        semanticsData.binormal = binormal;
#endif
#ifdef VERTEX_OUTPUT_COLOR0
        semanticsData.color0 = color0;
#endif
#ifdef VERTEX_OUTPUT_COLOR1
        semanticsData.color1 = color1;
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD2
        semanticsData.uv2 = uv2;
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD3
        semanticsData.uv3 = uv3;
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD4
        semanticsData.uv4 = uv4;
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD8
        semanticsData.uv8 = uv8;
#endif
#ifdef VERTEX_OUTPUT_TEXCOORD9
        semanticsData.uv9 = uv9;
#endif
#ifdef VERTEX_OUTPUT_SEED
        semanticsData.seed = random_seed;
#endif
#ifdef VERTEX_OUTPUT_UV_SET_2
        semanticsData.uv_set_2 = uv_set_2;
#endif
#ifdef VERTEX_OUTPUT_MODEL_ORIGIN
        semanticsData.model_origin = model_origin;
#endif
    }

    GRAPH_STATE_VAR(Art__Textures__NPC__Octavius__Octaviusc);
    INIT_GRAPH_STATE(Art__Textures__NPC__Octavius__Octaviusc);

    GRAPH_STATE_VAR(Metadata__Materials__SpecGloss);
    INIT_GRAPH_STATE(Metadata__Materials__SpecGloss);

    GRAPH_STATE_VAR(Metadata__Materials__GlowAdd);
    INIT_GRAPH_STATE(Metadata__Materials__GlowAdd);

    GRAPH_STATE_VAR(Metadata__Materials__Utility__TWEAK_Gloss);
    INIT_GRAPH_STATE(Metadata__Materials__Utility__TWEAK_Gloss);

    // ???
    surfaceData.world_pos = semanticsData.world_pos.xyz;
    surfaceData.tbn_basis.tangent = semanticsData.tangent;
    surfaceData.tbn_basis.binormal = semanticsData.binormal;
    surfaceData.tbn_basis.normal = semanticsData.normal;

    // UVSetup
    surfaceData.uv = semanticsData.uv1;

    // Texturing_Init
    RUN_GRAPH_STATE(Art__Textures__NPC__Octavius__Octaviusc, Texturing_Init);
    RUN_GRAPH_STATE(Metadata__Materials__SpecGloss, Texturing_Init);
    RUN_GRAPH_STATE(Metadata__Materials__GlowAdd, Texturing_Init);

    // Texturing
    RUN_GRAPH_STATE(Metadata__Materials__Utility__TWEAK_Gloss, Texturing);

    // Lighting stage?
    float3x3 TBN = {surfaceData.tbn_basis.tangent, surfaceData.tbn_basis.binormal, surfaceData.tbn_basis.normal};

    uint lightCount, lightSize;
    lights.GetDimensions(lightCount, lightSize);

    surfaceData.world_normal = mul(surfaceData.tbn_normal, TBN);

    {
        surfaceData.view_dir = normalize(surfaceData.world_pos.xyz - camera_position);
        surfaceData.reflected_dir = reflect(surfaceData.view_dir, surfaceData.world_normal);
        emissionData.emission += surfaceData.material.emissive_color;
    }

    for (uint lightIdx = 0; lightIdx < lightCount; ++lightIdx) {
        Light light = lights[lightIdx];
        LightData lightData;
        if (light.pos.w) {
            float arbitraryCutoff = 4000.0;
            ComputePointLightParams(light.pos, float4(light.color * light.intensity, arbitraryCutoff),
                                    surfaceData.world_pos, lightData);
        } else {
            ComputeDirectionalLightParams(float4(-light.pos.xyz, 0.0), float4(light.color * light.intensity, 0.0),
                                          lightData);
        }
        lightData.intensity = light.color * light.intensity;
        ComputeLight(lightData, surfaceData, emissionData);
    }

    return float4(emissionData.diffuse + emissionData.specular + emissionData.emission, 1.0);
}

#undef INIT_GRAPH_STATE
#undef GRAPH_STATE_VAR
#undef RUN_GRAPH_STATE