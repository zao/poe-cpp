#define MAX_NO_POINT_LIGHTS_IN_FRAME 1

float3x3 Inverse3x3(float3x3 m)
{
	float det = 
	m[0][0] * (m[1][1] * m[2][2] - m[2][1] * m[1][2]) -
	m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0]) +
	m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]);

	float invdet = 1.0f / det;

	float3x3 res; // inverse of matrix m
	res[0][0] = (m[1][1] * m[2][2] - m[2][1] * m[1][2]) * invdet;
	res[0][1] = (m[0][2] * m[2][1] - m[0][1] * m[2][2]) * invdet;
	res[0][2] = (m[0][1] * m[1][2] - m[0][2] * m[1][1]) * invdet;
	res[1][0] = (m[1][2] * m[2][0] - m[1][0] * m[2][2]) * invdet;
	res[1][1] = (m[0][0] * m[2][2] - m[0][2] * m[2][0]) * invdet;
	res[1][2] = (m[1][0] * m[0][2] - m[0][0] * m[1][2]) * invdet;
	res[2][0] = (m[1][0] * m[2][1] - m[2][0] * m[1][1]) * invdet;
	res[2][1] = (m[2][0] * m[0][1] - m[0][0] * m[2][1]) * invdet;
	res[2][2] = (m[0][0] * m[1][1] - m[1][0] * m[0][1]) * invdet;
	
	return res;
}

cbuffer CBPass : register(b0)
{
	float4 light_positions[MAX_NO_POINT_LIGHTS_IN_FRAME]; // With Theta
	float4 light_directions[MAX_NO_POINT_LIGHTS_IN_FRAME]; // With Phi
	float4 light_colors[MAX_NO_POINT_LIGHTS_IN_FRAME]; // With radius
	float4x4 light_matrices[MAX_NO_POINT_LIGHTS_IN_FRAME];
	float4 light_types[MAX_NO_POINT_LIGHTS_IN_FRAME];
	float4x4 view_projection_transform;
}

cbuffer CBPipe : register(b1)
{
	float4 pipe_padding;
};

cbuffer CBObject : register(b2)
{
	float4x4 world_transform;
};

cbuffer CBTwiddle : register(b3)
{
	uint twiddle_source;
	uint twiddle_comp;
	uint use_inverse_transpose;
};

struct VSIn {
	float3 position : POSITION0;
	float2 texcoord : TEXCOORD0;
	float4 mystery_floatb0 : HFLOAT0;
	float4 mystery_snormb0 : BSNORM0;
	float4 mystery_snormb1 : BSNORM1;
	float4 mystery_unormb0 : BUNORM0;
	float4 mystery_unormb1 : BUNORM1;
};

struct VSOut {
	float4 position : SV_Position;
	float2 texcoord : TEXCOORD0;
	float3 world_normal : NORMAL0;
	float3 mystery : MYSTERY;
};

Texture2D mat_tex_0 : register(t0);

SamplerState mat_smp_0 : register(s0);

VSOut vs_main(VSIn input) {
    VSOut output = (VSOut)0;
    output.position = mul(mul(float4(input.position, 1.0), world_transform), view_projection_transform);
	// always do full inverse-transpose for now
	float3x3 gradient_matrix = Inverse3x3(transpose((float3x3)world_transform));
	float3 obj_normal = input.mystery_unormb0.xyz * 2.0 - 1.0;
	output.world_normal = normalize(mul(obj_normal, gradient_matrix));
    output.texcoord = input.texcoord;
	{
		float4 m;
		if (twiddle_source == 0) m = input.mystery_floatb0;
		if (twiddle_source == 1) m = input.mystery_snormb0;
		if (twiddle_source == 2) m = input.mystery_snormb1;
		if (twiddle_source == 3) m = input.mystery_unormb0;
		if (twiddle_source == 4) m = input.mystery_unormb1;
		if (twiddle_comp == 0) output.mystery = m.xxx;
		if (twiddle_comp == 1) output.mystery = m.yyy;
		if (twiddle_comp == 2) output.mystery = m.zzz;
		if (twiddle_comp == 3) output.mystery = m.www;
	}
    return output;
}

struct DirectionalLight {
	float3 dir;
	float3 color;
};

float4 ps_main(VSOut input) : SV_Target {
    float4 color = mat_tex_0.Sample(mat_smp_0, input.texcoord);
	float2 wrap_tc = fmod(input.texcoord, float2(1.0, 1.0));

	DirectionalLight lights[2];
	lights[0].dir = -float3(0.0, -1.0, -1.0);
	lights[0].dir /= length(lights[0].dir);
	lights[0].color = float3(0.6, 0.396, 0.082);

	lights[1].dir = -float3(-1.0, 0.0, 0.0);
	lights[1].color = float3(0.0, 0.5, 0.5);
	float3 diffuse = float3(0, 0, 0);
	for (int i = 0; i < 2; ++i) {
		diffuse += lights[i].color * saturate(dot(input.world_normal, -lights[i].dir));
	}
	// color = float4(wrap_tc, 0.0, 1.0);
	color = float4(diffuse, 1.0);
	color = float4(input.world_normal.xyz * 0.5 + 0.5, 1.0);
	color = float4(input.mystery, 1.0);
    return color;
}