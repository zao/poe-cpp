#include <poe/format/ggpk.hpp>

#include <loguru/loguru.hpp>

#include <cstdint>
#include <queue>
#include <set>

namespace ggpk = poe::format::ggpk;

int main(int argc, char **argv) {
    loguru::init(argc, argv);
    if (argc != 2) {
        LOG_F(FATAL, "usage: ensure_ggpk_is_tree CONTENT_GGPK");
    }

    auto pack = ggpk::index_ggpk(argv[1]);

    bool is_not_tree = false;
    std::set<uint64_t> found_offsets;
    {
        std::queue<ggpk::parsed_entry const *> q;
        q.push(pack->root_);
        while (!q.empty()) {
            auto *entry = q.front();
            auto *dir = dynamic_cast<ggpk::parsed_directory const *>(entry);
            q.pop();
            if (!found_offsets.insert(entry->offset_).second) {
                LOG_F(WARNING, "Offset {} referenced multiple times", entry->offset_);
                is_not_tree = true;
            }
            if (dir) {
                for (auto &entry : dir->entries_) {
                    q.push(entry.get());
                }
            }
        }
    }

    return is_not_tree ? 1 : 0;
}