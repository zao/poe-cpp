#include <loguru/loguru.hpp>
#include <fmt/format.h>

#include <poe/format/ggpk.hpp>

#include <filesystem>
#include <iostream>
#include <map>

void print_usage() { std::cerr << "adjust_bugged_index INDEX_GGPK\n"; }

namespace ggpk = poe::format::ggpk;

int main(int argc, char **argv) {
    loguru::init(argc, argv);
    if (argc != 2) {
        print_usage();
        return 1;
    }

    std::filesystem::path krangled_path = argv[1];

    if (!std::filesystem::exists(krangled_path)) {
        LOG_F(FATAL, "Input GGPK {} could not be opened.", krangled_path.string());
    }

    using mmap_sink = mio::basic_mmap_sink<std::byte>;
    mmap_sink mem(krangled_path.string(), 0, mio::map_entire_file);

    size_t pdirs_encountered = 0;
    std::map<uint64_t, size_t> correct_offsets;

    enum {
        GGPK_SIZE = 4 + 4 + 4 + 8 * 2,
        FREE_SIZE = 4 + 4 + 8,
    };

    {
        uint64_t offset = 0;
        while (offset < mem.size()) {
            uint32_t rec_len{};
            ggpk::chunk_tag tag{};
            memcpy(&rec_len, mem.data() + offset, 4);
            memcpy(&tag, mem.data() + offset + 4, 4);
            uint64_t bogus_offset = offset - pdirs_encountered - GGPK_SIZE - FREE_SIZE;
            correct_offsets[bogus_offset] = offset;
            if (tag == ggpk::PDIR_TAG) {
                pdirs_encountered += 1;
                offset += rec_len + 1;
            } else {
                offset += rec_len;
            }
        }
    }
    {
        uint64_t offset = 0;
        while (offset < mem.size()) {
            uint32_t rec_len{};
            ggpk::chunk_tag tag{};
            memcpy(&rec_len, mem.data() + offset, 4);
            memcpy(&tag, mem.data() + offset + 4, 4);
            if (tag == ggpk::PDIR_TAG) {
                rec_len += 1;
                memcpy(mem.data() + offset, &rec_len, 4);

                uint32_t child_count{};
                memcpy(&child_count, mem.data() + offset + 12, 4);

                uint64_t child_start = offset + rec_len - child_count * 12;
                for (size_t i = 0; i < child_count; ++i) {
                    uint64_t offset_offset = child_start + i * 12 + 4;
                    uint64_t offset{};
                    memcpy(&offset, mem.data() + offset_offset, 8);
                    auto correction = correct_offsets.find(offset);
                    if (correction == correct_offsets.end()) {
                        LOG_F(ERROR, "Offset {} file does not correspond to a bad offset", offset);
                    } else {
                        offset = correction->second;
                    }
                    memcpy(mem.data() + offset_offset, &offset, 8);
                }
            }
            offset += rec_len;
        }
    }

    return 0;
}