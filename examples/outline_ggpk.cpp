#include <loguru/loguru.hpp>
#include <fmt/format.h>

#include <poe/format/ggpk.hpp>
#include <poe/util/utf.hpp>

#include <asio.hpp>
#include <sqlite3.h>

#include <cassert>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <queue>
#include <set>
#include <thread>
#include <unordered_set>

void print_usage() { std::cerr << "outline_ggpk OUTLINE_DB CONTENT_GGPK OUTLINED_GGPK\n"; }

namespace ggpk = poe::format::ggpk;

uint64_t measure_outlined_entry(ggpk::parsed_entry const *entry) {
    if (auto *file = dynamic_cast<ggpk::parsed_file const *>(entry); file) {
        return file->data_offset_ - file->offset_;
    } else if (auto *dir = dynamic_cast<ggpk::parsed_directory const *>(entry); dir) {
        return 4 + 4 + 4 + 4 + 32 + (dir->name_.size() * 2 + 2) + dir->entries_.size() * 12;
    }
    return 0;
}

auto digest_to_string(poe::util::sha256_digest const &digest) -> std::string {
    fmt::memory_buffer buf;
    for (auto b : digest) {
        format_to(buf, "{:02x}", static_cast<uint8_t>(b));
    }
    return std::string(buf.data(), buf.data() + 64);
};

std::shared_ptr<sqlite3_stmt> prepare_sqlite_statement(sqlite3 *db, std::string_view sql) {
    sqlite3_stmt *stmt{};
    if (SQLITE_OK == sqlite3_prepare(db, sql.data(), static_cast<int>(sql.size()), &stmt, nullptr)) {
        return std::shared_ptr<sqlite3_stmt>(stmt, &sqlite3_finalize);
    } else {
        return {};
    }
}

enum path_fopen_mode {
    PATH_FOPEN_READ = 1,
    PATH_FOPEN_WRITE = 2,
};

auto path_fopen_binary(std::filesystem::path const &path, path_fopen_mode mode) {
#if _WIN32
    return _wfopen(path.c_str(), mode == PATH_FOPEN_READ ? L"rb" : L"wb");
#else
    return fopen(path.c_str(), mode == PATH_FOPEN_READ ? "rb", "wb");
#endif
}

enum {
    GGPK_SIZE = 4 + 4 + 4 + 8 * 2,
    FREE_SIZE = 4 + 4 + 8,
};

int main(int argc, char **argv) {
    loguru::init(argc, argv);
    if (argc != 4) {
        print_usage();
        return 1;
    }

    std::atomic<bool> error_occurred = false;

    std::filesystem::path outline_db = argv[1];
    std::filesystem::path content_path = argv[2];
    std::filesystem::path outlined_path = argv[3];

    if (!std::filesystem::exists(content_path)) {
        LOG_F(FATAL, "Input GGPK {} could not be opened.", content_path.string());
    }

    sqlite3 *db{};
    {
        if (int err = sqlite3_open(outline_db.string().c_str(), &db); err) {
            LOG_F(FATAL, "Could not open outline database \"{}\" - {}.", outline_db.string().c_str(),
                  sqlite3_errstr(err));
        }
        if (int err = sqlite3_exec(
                db, "CREATE TABLE IF NOT EXISTS files (digest BLOB UNIQUE NOT NULL, payload BLOB NOT NULL)", nullptr,
                nullptr, nullptr);
            err) {
            sqlite3_close(db);
            LOG_F(FATAL, "Could not create \"files\" table in outline database");
        }
    }

    auto ggpk_index = ggpk::index_ggpk(content_path);

    struct output_item {
        uint64_t dest_offset_;
        ggpk::parsed_entry const *source_;
    };

    std::vector<output_item> output_sequence;
    std::unordered_map<uint64_t, uint64_t> source_to_dest_offsets;
    {
        uint64_t offset_base = GGPK_SIZE + FREE_SIZE;
        std::queue<ggpk::parsed_entry const *> q;
        q.push(ggpk_index->root_);
        while (!q.empty()) {
            auto *entry = q.front();
            q.pop();
            output_item item;
            item.dest_offset_ = offset_base;
            item.source_ = entry;
            source_to_dest_offsets[entry->offset_] = offset_base;
            offset_base += measure_outlined_entry(entry);
            output_sequence.push_back(item);
            if (auto *dir = dynamic_cast<ggpk::parsed_directory const *>(entry); dir) {
                for (auto &entry : dir->entries_) {
                    q.push(entry.get());
                }
            }
        }
    }
    LOG_F(INFO, "Gathered {} entries", output_sequence.size());

    auto content_write_thread = std::thread([&] {
        std::map<uint64_t, output_item> read_list;
        for (auto &item : output_sequence) {
            if (auto *file = dynamic_cast<ggpk::parsed_file const *>(item.source_); file) {
                read_list[item.source_->offset_] = item;
            }
        }

        uint64_t bytes_in_txn = 0;
        enum { BYTES_PER_TXN = 20ull << 20 };
        sqlite3_exec(db, "BEGIN IMMEDIATE TRANSACTION", nullptr, nullptr, nullptr);
        for (auto &[offset, item] : read_list) {
            if (auto *file = dynamic_cast<ggpk::parsed_file const *>(item.source_)) {
                auto &digest = file->stored_digest_;
                auto digest_str = digest_to_string(digest);

                if (bytes_in_txn > BYTES_PER_TXN) {
                    sqlite3_exec(db, "COMMIT TRANSACTION; BEGIN IMMEDIATE TRANSACTION", nullptr, nullptr, nullptr);
                    bytes_in_txn = 0;
                }
                auto stmt = prepare_sqlite_statement(db, "INSERT OR IGNORE INTO files VALUES (:digest, :payload)");
                sqlite3_bind_blob(stmt.get(), sqlite3_bind_parameter_index(stmt.get(), ":digest"), digest.data(),
                                  static_cast<int>(digest.size()), SQLITE_STATIC);
                std::byte const *p = ggpk_index->mapping_.data() + file->data_offset_;
                uint64_t n = file->data_size_;
                sqlite3_bind_blob64(stmt.get(), sqlite3_bind_parameter_index(stmt.get(), ":payload"), p, n,
                                    SQLITE_STATIC);
                if (int err = sqlite3_step(stmt.get()); err != SQLITE_DONE) {
                    LOG_F(ERROR, "Could not insert file at offset {} with digest {} into outline database: {}",
                          file->offset_, digest_str, sqlite3_errstr(err));
                    error_occurred = true;
                    return;
                }
                bytes_in_txn += n;
            }
        }
        if (bytes_in_txn) {
            sqlite3_exec(db, "COMMIT TRANSACTION", nullptr, nullptr, nullptr);
        }
    });

    auto outline_index_write_thread = std::thread([&] {
        std::filesystem::create_directories(outlined_path.parent_path());
        std::ofstream os(outlined_path, std::ios::binary);
        // write GGPK and FREE entries first, then the root PDIR and the rest of the entries
        auto write_one = [&os](auto x) { os.write(reinterpret_cast<char const *>(&x), sizeof(x)); };
        auto write_many = [&os]<typename T>(T *p, size_t n) {
            os.write(reinterpret_cast<char const *>(p), sizeof(T) * n);
        };
        {
            // write GGPK entry
            uint32_t rec_len = GGPK_SIZE;
            write_one(rec_len);

            auto tag = ggpk::GGPK_TAG;
            write_one(tag);

            uint32_t child_count = 2;
            write_one(child_count);

            uint64_t offsets[] = {GGPK_SIZE, GGPK_SIZE + FREE_SIZE};
            write_many(offsets, 2);

            // write token FREE entry
            rec_len = FREE_SIZE;
            write_one(rec_len);

            tag = ggpk::FREE_TAG;
            write_one(tag);

            uint64_t free_next = 0;
            write_one(free_next);
        }
        for (auto &item : output_sequence) {
            auto *dir = dynamic_cast<ggpk::parsed_directory const *>(item.source_);
            auto *file = dynamic_cast<ggpk::parsed_file const *>(item.source_);
            if (!dir && !file) {
                continue;
            }

            uint32_t rec_len = static_cast<uint32_t>(measure_outlined_entry(item.source_));
            write_one(rec_len);

            auto tag = dir ? ggpk::PDIR_TAG : ggpk::FILE_TAG;
            write_one(tag);

            uint32_t name_len = static_cast<uint32_t>(item.source_->name_.size() + 1);
            write_one(name_len);

            if (dir) {
                uint32_t child_count = static_cast<uint32_t>(dir->entries_.size());
                write_one(child_count);
            }

            auto digest = item.source_->stored_digest_;
            write_one(digest);

            auto &name = item.source_->name_;
            write_many(name.c_str(), name_len);

            // write directory, adjusting offsets
            if (dir) {
                for (auto &child : dir->entries_) {
                    auto hash = child->name_hash_;
                    write_one(hash);
                    auto offset = source_to_dest_offsets[child->offset_];
                    write_one(offset);
                }
            }
        }
    });

    content_write_thread.join();
    outline_index_write_thread.join();

    {
        auto stmt = prepare_sqlite_statement(db, "SELECT COUNT(1) FROM files");
        auto res = sqlite3_step(stmt.get());
        if (res == SQLITE_ROW) {
            auto db_entries = sqlite3_column_int64(stmt.get(), 0);
            LOG_F(INFO, "Database now contains {} entries.", db_entries);
        }
    }

    sqlite3_close(db);
    return error_occurred ? 1 : 0;
}