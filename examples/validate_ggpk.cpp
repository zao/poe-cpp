#include <poe/util/random_access_file.hpp>
#include <poe/util/sha256.hpp>
#include <poe/util/utf.hpp>
#include <poe/format/ggpk.hpp>

#include <fmt/format.h>

#include <iostream>
#include <memory>
#include <mutex>
#include <map>
#include <queue>
#include <set>
#include <thread>
#include <unordered_set>

void print_usage() { std::cerr << "usage: validate_ggpk GGPK" << std::endl; }

struct scan_item {
    uint64_t data_offset_;
    uint64_t data_size_;
};

struct scan_queue {
    std::map<uint64_t, scan_item> incoming_;
    std::map<uint64_t, poe::util::sha256_digest> result_;
};

struct index_state {
    scan_queue queue_;
};

size_t files_processed = 0;
size_t directories_processed = 0;

std::optional<poe::util::sha256_digest> process_scan_item(mio::basic_mmap_source<std::byte> &pack_file,
                                                          scan_item const &item) {
    auto n = item.data_size_;
    auto reader = poe::util::make_stream_reader(pack_file, item.data_offset_);
    auto hasher = poe::util::incremental_sha256();
    std::vector<std::byte> read_buf((std::min<size_t>)(n, 8 << 20));
    while (n) {
        uint64_t amount = (std::min<uint64_t>)(n, read_buf.size());
        if (!reader->read_many(read_buf.data(), amount)) {
            return {};
        }
        hasher->feed(read_buf.data(), amount);
        n -= amount;
    }
    return hasher->finish();
}

std::optional<poe::util::sha256_digest> validate_entry(index_state const &state,
                                                       poe::format::ggpk::parsed_entry const &entry);

std::optional<poe::util::sha256_digest> validate_file(index_state const &state,
                                                      poe::format::ggpk::parsed_file const &file) {
    // Lookup guaranteed to succeed as all entries have been touched by traversal prior.
    auto computed_digest = state.queue_.result_.find(file.offset_)->second;
    if (computed_digest == file.stored_digest_) {
        return computed_digest;
    }
    return {};
}

std::optional<poe::util::sha256_digest> validate_directory(index_state const &state,
                                                           poe::format::ggpk::parsed_directory const &dir) {
    auto name_lower = poe::util::lowercase(dir.name_);
    if (dir.name_hash_ != poe::util::oneshot_murmur2_32(reinterpret_cast<std::byte *>(name_lower.data()),
                                                        sizeof(char16_t) * name_lower.size())) {
        return {};
    }
    auto hasher = poe::util::incremental_sha256();
    for (auto &child : dir.entries_) {
        auto child_digest = validate_entry(state, *child);
        if (!child_digest) {
            return {};
        }
        hasher->feed(child_digest->data(), child_digest->size());
    }
    auto computed_digest = hasher->finish();
    if (computed_digest != dir.stored_digest_) {
        return {};
    }

    return computed_digest;
}

std::optional<poe::util::sha256_digest> validate_entry(index_state const &state,
                                                       poe::format::ggpk::parsed_entry const &entry) {
    auto dir = dynamic_cast<poe::format::ggpk::parsed_directory const *>(&entry);
    if (dir) {
        return validate_directory(state, *dir);
    }
    auto file = dynamic_cast<poe::format::ggpk::parsed_file const *>(&entry);
    if (file) {
        return validate_file(state, *file);
    }
    return {};
}

bool validate_free(poe::util::mmap_source &mapping_, uint64_t offset) {
    uint64_t free_size = 0;
    std::unordered_set<uint64_t> previous_offsets;
    while (true) {
        auto reader = poe::util::make_stream_reader(mapping_, offset);

        struct free_header {
            uint32_t rec_len;
            poe::format::ggpk::chunk_tag tag;
            uint64_t next;
        } free_h;

        if (!reader->read_one(free_h.rec_len) || offset + free_h.rec_len > mapping_.size() ||
            !reader->read_one(free_h.tag) || free_h.tag != poe::format::ggpk::FREE_TAG ||
            !reader->read_one(free_h.next) || previous_offsets.contains(free_h.next)) {
            return false;
        }
        free_size += free_h.rec_len - 16;

        previous_offsets.insert(offset);
        offset = free_h.next;
        if (offset == 0) {
            std::cout << "Free size: " << free_size << std::endl;
            return true;
        }
    }
}

bool validate_ggpk(std::filesystem::path pack_path) {
    auto pack = poe::format::ggpk::index_ggpk(pack_path);
    if (!pack) {
        return false;
    }

    // validate free chain
    if (!validate_free(pack->mapping_, pack->free_offset_)) {
        return false;
    }

    // gather file entries for validation
    fmt::print(stderr, "Gathering file entries\n");
    index_state state;
    {
        std::queue<poe::format::ggpk::parsed_directory const *> q;
        q.push(pack->root_);
        while (!q.empty()) {
            auto *dir = q.front();
            q.pop();
            for (auto &entry : dir->entries_) {
                if (auto *file = dynamic_cast<poe::format::ggpk::parsed_file const *>(entry.get()); file) {
                    scan_item item;
                    item.data_offset_ = file->data_offset_;
                    item.data_size_ = file->data_size_;
                    state.queue_.incoming_[file->offset_] = item;
                } else if (auto *dir = dynamic_cast<poe::format::ggpk::parsed_directory const *>(entry.get()); dir) {
                    q.push(dir);
                }
            }
        }
    }

    // compute hashes for file entry data in storage order
    fmt::print(stderr, "Scanning file contents\n");
    for (auto &incoming : state.queue_.incoming_) {
        auto digest = process_scan_item(pack->mapping_, incoming.second);
        if (!digest) {
            std::cerr << "SHA256 scan failed." << std::endl;
            return false;
        }
        state.queue_.result_[incoming.first] = *digest;
    }

    // validate directory tree against those hashes
    fmt::print(stderr, "Validating directory tree\n");
    if (!validate_directory(state, *pack->root_)) {
        std::cerr << "PDIR validate pass failed." << std::endl;
        return false;
    }

    return true;
}

int main(int argc, char **argv) {
    if (argc != 2) {
        print_usage();
        return 1;
    }

    std::chrono::steady_clock clock;
    auto tic = clock.now();
    std::filesystem::path pack_path = argv[1];

    bool valid = validate_ggpk(pack_path);
    auto toc = clock.now();
    std::chrono::duration<float> time_delta = toc - tic;
    fmt::print(stderr, "{} is {}\n", argv[1], valid ? "valid" : "not valid");
    fmt::print(stderr, "Time taken: {}\n", time_delta.count());
    return valid ? 0 : 1;
}