#include <poe/poe.hpp>
#include <poe/format/ggpk.hpp>

#include <poe/util/path.hpp>
#include <poe/util/random_access_file.hpp>
#include <poe/util/utf.hpp>

#include <loguru/loguru.hpp>

#include <filesystem>
#include <iostream>
#include <memory>
#include <optional>
#include <queue>

namespace ggpk = poe::format::ggpk;

namespace poe::format::smd {
struct smd_file {};

std::unique_ptr<smd_file> parse(std::byte const *p, size_t n) {
    auto reader = poe::util::make_stream_reader(p, n, 0);
    uint8_t version{};
    reader->read_one(version);
    return {}; }
} // namespace poe::format::smd

int main(int argc, char **argv) {
    loguru::init(argc, argv);
    if (argc < 2) {
        std::cerr << "usage: probe_smd Index.ggpk [contents.db]";
        return 1;
    }
    std::filesystem::path pack_path = argv[1];

    auto pack = ggpk::index_ggpk(pack_path);
    if (!pack) {
        LOG_F(FATAL, "Could not load GGPK");
    }

    size_t smd_count{};
    std::queue<ggpk::parsed_directory const *> q;
    q.push(pack->root_);
    while (!q.empty()) {
        auto parent = q.front();
        q.pop();
        for (auto &e : parent->entries_) {
            if (auto *dir = dynamic_cast<ggpk::parsed_directory const *>(e.get()); dir) {
                q.push(dir);
            } else {
                auto *file = dynamic_cast<ggpk::parsed_file const *>(e.get());
                auto name_lower = poe::util::lowercase(file->name_);
                if (name_lower.ends_with(u".smd")) {
                    smd_count += 1;
                }
            }
        }
    }
    LOG_F(INFO, "{} SMD files found in tree.", smd_count);

    return 0;
}