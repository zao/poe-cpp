#include <poe/io/vfs.hpp>

#include <iostream>

using namespace poe::util::string_literals;

int main() {
    using shared_vfs = std::shared_ptr<poe::io::vfs>;
    shared_vfs steam_vfs;
    shared_vfs standalone_vfs;

    auto sources = poe::util::install_locations();
    for (auto& [kind, loc] : sources) {
        try {
        if (kind == poe::util::install_kind::Steam) {
            steam_vfs = std::make_shared<poe::io::vfs>(loc);
        }
        else if (kind == poe::util::install_kind::Standalone) {
            standalone_vfs = std::make_shared<poe::io::vfs>(loc);
        }
        } catch (std::exception &e) {
            std::cerr << e.what() << std::endl;
        }
    }
    
    shared_vfs fake_legacy_vfs;
    try {
        std::filesystem::path loc = "C:/Temp/fake-poe";
        fake_legacy_vfs = std::make_shared<poe::io::vfs>(loc);
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    std::vector<shared_vfs> filesystems = {steam_vfs, standalone_vfs, fake_legacy_vfs};
    std::vector<poe::util::path> locs = { "Audio/Dialogue/Tolman_Scream.ogg"_poe, "Data/AbyssRegions.dat"_poe };
    for (auto &fs : filesystems) {
        for (auto& loc : locs) {
            auto file = fs->open_file(loc);
            if (file) {
                auto data = file->read_all();
                auto digest = poe::util::oneshot_sha256(data.data(), data.size());
                std::cout << poe::util::digest_to_string(digest) << " - " << std::quoted(loc.path_) << std::endl;
            } else {
                std::cout << std::quoted(loc.path_) << " could not be opened." << std::endl;
            }
        }
    }
}