add_executable(adjust_bugged_index adjust_bugged_index.cpp)
GroupSourcesByFolder(adjust_bugged_index)
target_link_libraries(adjust_bugged_index PRIVATE
    libpoe 
    fmt::fmt
    loguru
)

add_executable(ensure_ggpk_is_tree ensure_ggpk_is_tree.cpp)
GroupSourcesByFolder(ensure_ggpk_is_tree)
target_link_libraries(ensure_ggpk_is_tree PRIVATE
    libpoe
    asio
    docopt 
    fmt::fmt
    loguru
)

add_executable(outline_ggpk outline_ggpk.cpp)
GroupSourcesByFolder(outline_ggpk)
target_link_libraries(outline_ggpk PRIVATE
    libpoe
    asio
    fmt::fmt
    loguru
    unofficial::sqlite3::sqlite3
)

add_executable(probe_smd "probe_smd.cpp")
GroupSourcesByFolder(probe_smd)
target_link_libraries(probe_smd PRIVATE
    libpoe
    loguru
)

add_executable(validate_ggpk validate_ggpk.cpp)
GroupSourcesByFolder(validate_ggpk)
target_link_libraries(validate_ggpk PRIVATE
    libpoe
    fmt::fmt
)
