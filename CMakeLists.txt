cmake_minimum_required(VERSION 3.11)
cmake_policy(SET CMP0003 NEW)
cmake_policy(SET CMP0048 NEW)
cmake_policy(SET CMP0074 NEW)
cmake_policy(SET CMP0091 NEW)
project(poe-cpp
	VERSION 1.0
	LANGUAGES C CXX
)
set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreadedDLL")
# set_property(TARGET libpoe PROPERTY MSVC_RUNTIME_LIBRARY "MultiThreaded")

set(POE_WITH_GUI ON CACHE BOOL "Build GUI parts of ${project}")
set(POE_WITH_EXAMPLES OFF CACHE BOOL "Build examples of ${project}")

include(cmake/GroupSourcesByFolder.cmake)

if(MSVC)
    add_compile_definitions(
	    "_SILENCE_CLANG_COROUTINE_MESSAGE"
	    "_UNICODE" "UNICODE"
	    "_CRT_SECURE_NO_DEPRECATE" "_CRT_SECURE_NO_WARNINGS"
	    "_SCL_SECURE_NO_DEPRECATE" "_SCL_SECURE_NO_WARNINGS"
	    "WINVER=0xA000" "_WIN32_WINNT=0xA000"
    )
    if (NOT (${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang" AND "x${CMAKE_CXX_SIMULATE_ID}" STREQUAL "xMSVC"))
        add_compile_options("-MP")
    endif()
endif()

if (UNIX)
	find_package(PkgConfig REQUIRED)
	pkg_check_modules(libsodium REQUIRED IMPORTED_TARGET libsodium)
endif()


### Find external packages from places like vcpkg or system

find_package(OpenGL)

find_package(asio CONFIG REQUIRED)
# target_link_libraries(main PRIVATE asio asio::asio)

# find_package(Boost REQUIRED)
# target_link_libraries(main PRIVATE ${Boost_LIBRARIES})
# target_include_directories(main PRIVATE ${Boost_INCLUDE_DIRS})

find_package(bustache CONFIG REQUIRED)
# target_link_libraries(main PRIVATE bustache::bustache)

find_package(cpr CONFIG REQUIRED)
# target_link_libraries(main PRIVATE cpr::cpr)

find_package(crossguid CONFIG REQUIRED)
# target_link_libraries(main PRIVATE crossguid)

find_path(CTRE_INCLUDE_DIRS "ctll.hpp")

find_package(directxmath CONFIG REQUIRED)
# target_link_libraries(main PRIVATE Microsoft::DirectXMath)

find_package(directxtk CONFIG REQUIRED)
# target_link_libraries(foo Microsoft::DirectXTK)

find_package(docopt CONFIG REQUIRED)
# target_link_libraries(main PRIVATE docopt)

find_package(fmt CONFIG REQUIRED)
# target_link_libraries(main PRIVATE fmt::fmt)

# Or use the header-only version
# target_link_libraries(main PRIVATE fmt::fmt-header-only)

find_package(GLEW REQUIRED)
# target_link_libraries(main PRIVATE GLEW::GLEW)

find_package(glfw3 CONFIG REQUIRED)
# target_link_libraries(main PRIVATE glfw)

find_package(glm CONFIG REQUIRED)
# target_link_libraries(main PRIVATE glm::glm)

find_package(gli CONFIG REQUIRED)
# target_link_libraries(main PRIVATE gli)

find_package(imgui CONFIG REQUIRED)
# target_link_libraries(main PRIVATE imgui::imgui)

find_package(jansson CONFIG REQUIRED)
# target_link_libraries(main PRIVATE jansson::jansson)

find_package(leveldb CONFIG REQUIRED)
# target_link_libraries(main PRIVATE leveldb::leveldb)
find_path(LOGURU_INCLUDE_DIRS "loguru/loguru.cpp")
# target_include_directories(main PRIVATE ${LOGURU_INCLUDE_DIRS})

add_library(loguru STATIC ${LOGURU_INCLUDE_DIRS}/loguru/loguru.cpp ${LOGURU_INCLUDE_DIRS}/loguru/loguru.hpp)
target_compile_definitions(loguru PUBLIC "LOGURU_USE_FMTLIB")
target_include_directories(loguru PUBLIC ${LOGURU_INCLUDE_DIRS})
target_link_libraries(loguru PUBLIC fmt::fmt)

find_package(Microsoft.GSL CONFIG REQUIRED)
# target_link_libraries(main PRIVATE Microsoft.GSL::GSL)

find_package(mio CONFIG REQUIRED)
# target_link_libraries(main PRIVATE mio::mio mio::mio_base mio::mio_full_winapi)

find_package(nlohmann_json CONFIG REQUIRED)
# target_link_libraries(main PRIVATE nlohmann_json nlohmann_json::nlohmann_json)

find_package(TBB CONFIG REQUIRED)
# target_link_libraries(main PRIVATE TBB::tbb)

find_package(Threads REQUIRED)

find_package(unofficial-sqlite3 CONFIG REQUIRED)
# target_link_libraries(main PRIVATE unofficial::sqlite3::sqlite3)

find_package(zstd CONFIG REQUIRED)
# target_link_libraries(main PRIVATE zstd::libzstd_shared)

### Add all our subdirectories

find_package(Boost REQUIRED)

add_subdirectory(vendor)
add_subdirectory(libpoe)
if (POE_WITH_GUI AND WIN32)
	add_subdirectory(libpog)
endif()
add_subdirectory(libs)
if (POE_WITH_EXAMPLES)
	add_subdirectory(examples)
endif()
add_subdirectory(apps)