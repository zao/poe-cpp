function(GroupSourcesByFolder target)

  get_target_property(sources ${target} SOURCES)
  source_group(TREE "${CMAKE_CURRENT_SOURCE_DIR}"
    PREFIX ${target}
    FILES ${sources}
  )
endfunction()