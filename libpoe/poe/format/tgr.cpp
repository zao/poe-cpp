#include <poe/format/tgr.hpp>

#include <poe/util/parse_ops.hpp>
#include <poe/util/utf.hpp>

#include <loguru/loguru.hpp>

namespace poe::format::tgr {
std::shared_ptr<TGRFile> parse_tgr(std::span<std::byte const> data) {
    auto wide_sv = poe::util::to_u16string_view(data);
    if (!wide_sv) {
        return {};
    }
    auto input_text = poe::util::to_string(*wide_sv);
    auto view = std::string_view(input_text);

    auto ret = std::make_shared<TGRFile>();
    poe::util::ParseOps po(view);
    if (auto err = po.reset().string_lit("version").spaces().integer(ret->version_).eol().err()) {
        LOG_F(WARNING, "version line error: {}", *err);
        return {};
    }

    if (ret->version_ > 18) {
        // LOG_F(WARNING, "Version not supported: {}", ret->version_);
        // return {};
    }

    if (auto err = po.reset()
                       .string_lit("Size:")
                       .spaces()
                       .integer(ret->size_.first)
                       .spaces()
                       .integer(ret->size_.second)
                       .eol()
                       .err()) {
        LOG_F(WARNING, "Size line error: {}", *err);
        return {};
    }

    if (ret->version_ <= 18) {
        Coord2 resolution{};
        if (auto err = po.reset()
                           .string_lit("Resolution:")
                           .spaces()
                           .integer(resolution.first)
                           .spaces()
                           .integer(resolution.second)
                           .eol()
                           .err()) {
            LOG_F(WARNING, "Resolution line error: {}", *err);
            return {};
        }
        ret->resolution_ = resolution;
    }

    if (auto err = po.reset().string_lit("MasterFile:").spaces().quoted_string(ret->master_file_).eol().err()) {
        LOG_F(WARNING, "MasterFile line error: {}", *err);
        return {};
    }

    size_t node_count;
    if (auto err = po.reset().string_lit("Nodes:").spaces().integer(node_count).eol().err()) {
        LOG_F(WARNING, "Nodes line error: {}", *err);
        return {};
    }

    size_t edge_count;
    if (auto err = po.reset().string_lit("Edges:").spaces().integer(edge_count).eol().err()) {
        LOG_F(WARNING, "Edges line error: {}", *err);
        return {};
    }

    if (auto err = po.reset()
                       .quoted_string(ret->gt_top_)
                       .eol()
                       .quoted_string(ret->gt_mid_)
                       .eol()
                       .quoted_string(ret->gt_bot_)
                       .eol()
                       .err()) {
        LOG_F(WARNING, "GT lines error: {}", *err);
        return {};
    }

    for (size_t node_idx = 0; node_idx < node_count; ++node_idx) {
        TGRNode node;
        size_t num_count;
        if (auto err = po.reset().integer(node.x_).spaces().integer(node.y_).spaces().integer(num_count).err()) {
            LOG_F(WARNING, "node line error in part 1: {}", *err);
            return {};
        }
        node.nums_.resize(num_count);
        for (size_t num_idx = 0; num_idx < num_count; ++num_idx) {
            if (auto err = po.reset().spaces().integer(node.nums_[num_idx]).err()) {
                LOG_F(WARNING, "node line error in part 2: {}", *err);
                return {};
            }
        }
        size_t note_count;
        if (auto err = po.reset()
                           .spaces()
                           .quoted_string(node.tag_)
                           .spaces()
                           .string(node.orientation_)
                           .spaces()
                           .integer(note_count)
                           .err()) {
            LOG_F(WARNING, "node line error in part 3: {}", *err);
            return {};
        }
        node.notes_.resize(note_count);
        for (size_t note_idx = 0; note_idx < note_count; ++note_idx) {
            if (auto err = po.reset().spaces().quoted_string(node.notes_[note_idx]).err()) {
                LOG_F(WARNING, "node line error in part 4: {}", *err);
                return {};
            }
        }
        if (auto err = po.reset()
                           .spaces()
                           .integer(node.tails_[0])
                           .spaces()
                           .integer(node.tails_[1])
                           .spaces()
                           .integer(node.tails_[2])
                           .spaces()
                           .integer(node.tails_[3])
                           .eol()
                           .err()) {
            LOG_F(WARNING, "node line error in part 5: {}", *err);
            return {};
        }
        ret->nodes_.push_back(std::move(node));
    }

    for (size_t edge_idx = 0; edge_idx < edge_count; ++edge_idx) {
        TGREdge edge;
        size_t coord_count;
        if (auto err = po.reset()
                           .integer(edge.from_node_)
                           .spaces()
                           .integer(edge.to_node_)
                           .spaces()
                           .integer(coord_count)
                           .err()) {
            LOG_F(WARNING, "Edge line error in part 1: {}", *err);
            return {};
        }
        edge.coords_.resize(coord_count);
        for (size_t coord_idx = 0; coord_idx < coord_count; ++coord_idx) {
            auto &coord = edge.coords_[coord_idx];
            if (auto err = po.reset().spaces().integer(coord.first).spaces().integer(coord.second).err()) {
                LOG_F(WARNING, "Edge line error in part 2: {}", *err);
                return {};
            }
        }

        if (auto err = po.reset().spaces().integer(edge.big_).err()) {
            LOG_F(WARNING, "Edge line error in part 3: {}", *err);
            return {};
        }
        if (ret->version_ >= 15) {
            if (auto err = po.reset().spaces().integer(edge.b_).err()) {
                LOG_F(WARNING, "Edge line error in part 4: {}", *err);
                return {};
            }
        }
        if (auto err = po.reset()
                           .spaces()
                           .quoted_string(edge.et_)
                           .spaces()
                           .integer(edge.tails_[0])
                           .spaces()
                           .integer(edge.tails_[1])
                           .err()) {
            LOG_F(WARNING, "Edge line error in part 5: {}", *err);
            return {};
        }
        if (ret->version_ < 15) {
            if (auto err = po.reset().spaces().rest_of_line(edge.tail_word_).err()) {
                LOG_F(WARNING, "Edge line error in part 6: {}", *err);
                return {};
            }
        }
        if (auto err = po.reset().eol().err()) {
            LOG_F(WARNING, "Edge line error in part 7: {}", *err);
            return {};
        }
        ret->edges_.push_back(std::move(edge));
    }

    return ret;
}
} // namespace poe::format::tgr