#pragma once

#include <absl/container/flat_hash_map.h>
#include <memory>
#include <optional>
#include <span>
#include <string>

namespace poe::format::gft {
struct GftEntry {
    int val;
    std::string path;
    std::string mut; // mutation like R90, F, etc.
};

struct GftSection {
    std::optional<int> keyTailVal;
    std::vector<GftEntry> entries;
};

struct GftFile {
    int version;
    absl::flat_hash_map<std::string, GftSection> sections;
};

std::shared_ptr<GftFile> parse_gft(std::span<std::byte const> data);
} // namespace poe::format::gft