#pragma once

#include <gsl/span>
#include <memory>
#include <optional>
#include <string>
#include <utility>
#include <vector>

namespace poe::format::dgr {
using Coord2 = std::pair<size_t, size_t>;

struct DGRNode {
    size_t x_;
    size_t y_;
    std::vector<size_t> vals_;
    std::string key_;
    std::string rotate_flip_;
    std::vector<std::string> strs_;
    size_t unk_num1_;
    size_t unk_num2_;
    std::string unk_last_;
};

struct DGREdge {
    size_t from_node_;
    size_t to_node_;
    std::vector<Coord2> coords_;
    size_t unk2_num_;
    size_t unk3_num_;
    std::string et_;
    size_t unk4_num_;
    size_t unk5_num_;
    size_t unk5a_num_;
    std::optional<std::string> unk5b_str_;
    size_t unk6_num_;
    std::optional<std::string> unk6a_str_;
    size_t unk7_num_;
    size_t unk8_num_;
    std::string unk9_letter_;
    std::string unk10_letter_;
    size_t unk11_num_;
};

struct DGRFile {
    size_t version_;
    Coord2 size_;
    std::optional<Coord2> resolution_;
    std::string master_file_;
    std::string gt_top_;
    std::string gt_mid_;
    std::string gt_bot_;
    std::tuple<size_t, size_t, size_t> default_percent_;
    std::vector<DGRNode> nodes_;
    std::vector<DGREdge> edges_;
};

std::shared_ptr<DGRFile> parse_dgr(std::span<std::byte const> data);
} // namespace poe::format::dgr