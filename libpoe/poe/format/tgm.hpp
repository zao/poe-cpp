#pragma once

#include <gsl/span>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include <poe/format/dolm.hpp>

#include <tbb/scalable_allocator.h>

#include <array>
#include <memory>
#include <optional>
#include <span>
#include <vector>

namespace poe::format::tgm {
using poe::format::dolm::BBox;
using poe::format::dolm::VertexComponent;
using poe::format::dolm::VertexWidth;

using ByteVec = std::vector<std::byte, tbb::scalable_allocator<std::byte>>;

struct TgmGeometry {
    poe::format::dolm::DolmGeometry dolm;

    struct ShapeBounds {
        int ordinal;
        BBox bbox;
    };

    std::vector<ShapeBounds> shapeBounds;
};

struct TgmTailEntry {
    ByteVec data;
};

struct TgmTailEntryV9 {
    uint32_t u00hZero;
    float f04h[12];
    uint32_t u34hOne;
    uint8_t u38h[31];
};

struct TgmFile {
    int version;
    BBox bbox;
    int shapeCount;

    TgmGeometry mainGeometry, groundGeometry;

    std::vector<TgmTailEntry> tailEntries;
    std::vector<TgmTailEntryV9> tailEntriesV9; // only in v9?
};

std::shared_ptr<TgmFile> parse_tgm(std::span<std::byte const> data);
} // namespace poe::format::tgm