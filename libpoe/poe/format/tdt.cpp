#include <poe/format/tdt.hpp>

#include <poe/util/random_access_file.hpp>
#include <poe/util/utf.hpp>

namespace poe::format::tdt {

std::optional<std::string> Tdt::resolveString(uint32_t ref) const {
    if (auto I = stringTable_.find(ref); I != stringTable_.end()) {
        return I->second;
    }
    return std::nullopt;
}

std::optional<Tdt> parse_tdt(std::span<std::byte const> data) {
    auto r = poe::util::make_stream_reader(data, 0);

    Tdt ret;
    if (!r->read_one(ret.version)) {
        return std::nullopt;
    }

    if (ret.version < 3 || ret.version > 7) {
        return std::nullopt;
    }

    // String table
    {
        uint32_t stringTableCch;
        if (!r->read_one(stringTableCch)) {
            return std::nullopt;
        }
        for (uint32_t cchOff = 0; cchOff < stringTableCch;) {
            std::u16string s;
            if (!r->read_terminated_u16string(s)) {
                return std::nullopt;
            }
            ret.stringTable_[cchOff] = poe::util::to_string(s);
            cchOff += (uint32_t)s.size() + 1;
        }
    }

    auto readStringRef = [&ret, &r]() -> std::optional<std::string> {
        uint32_t ref;
        if (!r->read_one(ref)) {
            return std::nullopt;
        }
        return ret.resolveString(ref);
    };

    auto readStringRefOpt = [&ret, &r]() -> std::optional<std::optional<std::string>> {
        uint32_t ref;
        if (!r->read_one(ref)) {
            return std::nullopt;
        }
        return ret.resolveString(ref);
    };

    // Leading refs
    if (ret.version >= 7) {
        if (auto ref = readStringRef()) {
            ret.unkLeadingRef = *ref;
        } else {
            return std::nullopt;
        }
    }
    if (ret.version > 4) {
        if (auto tgt = readStringRef()) {
            ret.commonTgt = *tgt;
        } else {
            return std::nullopt;
        }
    }

    if (auto tag = readStringRef()) {
        ret.tag = *tag;
    } else {
        return std::nullopt;
    }

    // Header
    auto &hdr = ret.header;
    {
        for (auto &et : hdr.sideEts) {
            if (auto str = readStringRefOpt()) {
                et = *str;
            } else {
                return std::nullopt;
            }
        }

        if (ret.version == 3) {
            // v3 has 32-bit dimensions
            if (!r->read_one(hdr.dim)) {
                return std::nullopt;
            }
        } else {
            // v4+ has 8-bit dimensions
            glm::i8vec2 dim;
            if (!r->read_one(dim)) {
                return std::nullopt;
            }
            hdr.dim = dim;
        }

        for (auto &gt : hdr.sideGts) {
            if (auto str = readStringRefOpt()) {
                gt = *str;
            } else {
                return std::nullopt;
            }
        }

        if (ret.version == 3) {
            uint32_t zero1;
            if (!r->read_one(zero1)) {
                return {};
            }
            hdr.zero1 = zero1;
        } else {
            if (!r->read_one(hdr.zero1)) {
                return std::nullopt;
            }
        }

        if (ret.version > 3) {
            if (!r->read_one(hdr.dim2)) {
                return std::nullopt;
            }
        }

        if (ret.version == 3) {
            std::array<uint32_t, 8> sideOffsets;
            if (!r->read_one(sideOffsets)) {
                return std::nullopt;
            }
            std::copy_n(sideOffsets.data(), sideOffsets.size(), hdr.sideOffsets.data());
        } else {
            if (!r->read_one(hdr.sideOffsets)) {
                return std::nullopt;
            }
        }
    }

    // Sub-tile data
    if (ret.version == 3) {
        Tdt::SubTileV3 tile;
        uint8_t seven1;
        if (!r->read_one(tile.preFixed, tile.fixed, tile.preDist, tile.dist, seven1)) {
            return std::nullopt;
        }
        ret.subTileV3 = tile;
        if (auto tgt = readStringRef()) {
            ret.commonTgt = tgt;
        } else {
            return std::nullopt;
        }
    } else {
        uint16_t subCount;
        if (!r->read_one(ret.flags, subCount)) {
            return std::nullopt;
        }
        auto &subs = ret.subTiles;
        subs.resize(subCount);

        for (auto &sub : subs) {
            auto readFixed = [&r]() -> std::optional<Tdt::FixedBlock> {
                Tdt::FixedBlock block;
                if (!r->read_one(block)) {
                    return std::nullopt;
                }
                return block;
            };

            auto readVarying = [&r]() -> std::optional<Tdt::VaryingBlock> {
                uint16_t len;
                Tdt::VaryingBlock block;
                if (!r->read_one(len)) {
                    return std::nullopt;
                }
                block.resize(len);
                if (!r->read_exact((std::byte *)block.data(), block.size())) {
                    return std::nullopt;
                }
                return block;
            };

            if (!r->read_one(sub.type, sub.val)) {
                return std::nullopt;
            }
            switch (sub.type) {
            case 0x00:
            case 0x02:
            case 0x09:
                if (sub.fixed = readFixed(); !sub.fixed) {
                    return std::nullopt;
                }
                if (sub.varying = readVarying(); !sub.varying) {
                    return std::nullopt;
                }
                break;
            case 0x03:
            case 0x0B:
            case 0x1B:
                if (sub.fixed = readFixed(); !sub.fixed) {
                    return std::nullopt;
                }
                break;
            case 0x04:
            case 0x06:
            case 0x0D:
                if (sub.varying = readVarying(); !sub.varying) {
                    return std::nullopt;
                }
                break;
            case 0x07:
            case 0x0F:
            case 0x1F:
                break;
            default:
                return std::nullopt;
            }
            if (ret.version == 4) {
                if (auto tgt = readStringRef()) {
                    sub.tgt = tgt;
                } else {
                    return std::nullopt;
                }
            }
        }
    }

    if (ret.flags & Tdt::FlagsHasUnkBit4) {
        uint32_t u0h;
        std::array<float, 6> f4h;
        if (!r->read_one(u0h, f4h)) {
            return std::nullopt;
        }
    }

    if (ret.version == 3 || ret.flags & Tdt::FlagsHasGtGrid) {
        ret.gtGrid = ret.makeGrid<std::optional<std::string>>(hdr.dim.x + 1, hdr.dim.y + 1);
        for (auto &row : ret.gtGrid) {
            for (auto &gt : row) {
                if (auto str = readStringRefOpt()) {
                    gt = *str;
                } else {
                    return std::nullopt;
                }
            }
        }
    }

    if (ret.version == 3) {
        std::array<uint32_t, 4> zeros;
        if (!r->read_one(zeros)) {
            return std::nullopt;
        }
    }

    if (ret.flags & Tdt::FlagsHasTailBytes) {
        int count = hdr.dim[0] + 1;
        count *= hdr.dim[1] + 1;
        ret.tailBytes.resize(count);
        if (!r->read_exact((std::byte *)ret.tailBytes.data(), ret.tailBytes.size())) {
            return std::nullopt;
        }
    }

    if (ret.version >= 6) {
        if (auto str = readStringRefOpt()) {
            ret.tmd = *str;
        } else {
            return std::nullopt;
        }
    }

    if (ret.version >= 7) {
        Tdt::TailV7 t;
        if (auto str = readStringRefOpt()) {
            t.str0 = *str;
            if (!r->read_one(t.num1)) {
                return std::nullopt;
            }
            ret.tailV7 = t;
        } else {
            return std::nullopt;
        }
    }

    // check for EOF
    uint8_t dummy{};
    if (r->read_one(dummy)) {
        return std::nullopt;
    }

    return ret;
}
} // namespace poe::format::tdt