#include <poe/format/ao.hpp>

#include <poe/util/parse_info.hpp>
#include <poe/util/utf.hpp>

#include <fmt/format.h>
#include <re2/re2.h>

#include <optional>
#include <string>
#include <variant>

namespace poe::format::ao {
ao_component const *ao_parse::find_component(std::string_view name) const {
    for (auto const &comp : components_) {
        if (comp.first == name) {
            return &comp;
        }
    }
    return nullptr;
}

namespace ast {
struct animation_entry {
    float t0;
    std::optional<float> t1;
    std::string value;
};

using poly_value = std::variant<std::string, float, bool>;

using key_value = std::pair<std::string, poly_value>;

struct animation {
    std::string name;
    std::vector<animation_entry> entries;
};

struct continuous_effect {
    std::string name;
    std::vector<key_value> members;
};

using member = std::variant<key_value, animation, continuous_effect>;

using component = std::vector<member>;

struct ao {
    size_t version;
    std::vector<std::string> extends;
    std::vector<std::pair<std::string, component>> components;
};
} // namespace ast

struct ao_parser {
    ao_parser(std::string_view text) : text_(text), full_text_(text) {}

    std::shared_ptr<ao_parse> run();

    std::optional<std::string_view> peek_line();
    void consume_line();
    void skip_empty_lines();

    std::optional<size_t> read_version();
    std::optional<std::string> read_extends();
    std::optional<ao_component> read_component();

    std::string_view text_, full_text_;
};

std::shared_ptr<ao_parse> parse_ao(std::span<std::byte const> data) {
    if (data.size() % 2 != 0) {
        return {};
    }

    std::u16string_view wide_view(reinterpret_cast<char16_t const *>(data.data()), data.size() / 2);
    if (wide_view.size() && wide_view[0] == u'\uFEFF') {
        wide_view = wide_view.substr(1);
    }
    auto text = poe::util::to_string(wide_view);
    auto parser = ao_parser(text);
    return parser.run();
}

std::optional<std::string_view> ao_parser::peek_line() {
    if (text_.empty()) {
        return {};
    }
    auto end = text_.find_first_of("\r\n");
    if (end != std::string_view::npos) {
        return text_.substr(0, end);
    }
    return text_;
}

void ao_parser::consume_line() {
    if (!text_.empty()) {
        auto end = text_.find('\n');
        if (end != std::string_view::npos) {
            text_ = text_.substr(end + 1);
        } else {
            text_ = std::string_view();
        }
    }
}

static RE2 const comment_open_pattern{R"#(\s*/[*].*)#"};
static RE2 const comment_close_pattern{R"#(.*[*]/\s*)#"};
static RE2 const comment_oneline_pattern{R"#(\s*//.*)#"};
static RE2 const blank_line_pattern{R"#(\s*)#"};

void ao_parser::skip_empty_lines() {
    while (auto next_line = peek_line()) {
        if (RE2::FullMatch(*next_line, comment_open_pattern)) {
            consume_line();
            while (auto next_line = peek_line()) {
                consume_line();
                if (RE2::FullMatch(*next_line, comment_close_pattern)) {
                    break;
                }
            }
        } else if (RE2::FullMatch(*next_line, comment_oneline_pattern)) {
            consume_line();
        } else if (RE2::FullMatch(*next_line, blank_line_pattern)) {
            consume_line();
        } else {
            break;
        }
    }
}

static RE2 const version_pattern{R"#(\s*version\s+(\d+)\s*)#"};

std::optional<size_t> ao_parser::read_version() {
    skip_empty_lines();
    auto line = peek_line();
    int version;
    if (RE2::FullMatch(*line, version_pattern, &version)) {
        consume_line();
        return version;
    }
    return {};
}

static RE2 const extends_pattern{R"#(\s*extends\s+"([^"]+)"\s*)#"};

std::optional<std::string> ao_parser::read_extends() {
    skip_empty_lines();
    auto peeked = peek_line();
    if (!peeked) {
        return {};
    }
    std::string extendee;
    if (RE2::FullMatch(*peeked, extends_pattern, &extendee)) {
        consume_line();
        return extendee;
    }
    return {};
}

static RE2 const component_oneline_pattern{R"#(\s*([A-Za-z0-9]+)\s*\{\s*\}\s*)#"};
static RE2 const component_name_open_pattern{R"#(\s*([A-Za-z0-9]+)\s*\{\s*)#"};
static RE2 const component_name_pattern{R"#(\s*([A-Za-z0-9]+)\s*)#"};
static RE2 const component_open_pattern{R"#(\s*\{\s*)#"};
static RE2 const component_close_pattern{R"#(\s*\}\s*)#"};
static RE2 const component_open_close_pattern{R"#(\s*\{\s*\}\s*)#"};
// static constexpr auto component_entry_pattern = ctll::fixed_string{R"#(\t(\S+) = (.*))#"};
// static constexpr auto component_entry_generic_pattern = ctll::fixed_string{R"#(\t\t(\S+) = (.*))#"};
// static constexpr auto component_entry_animation_keyframe_pattern = ctll::fixed_string{R"#(\t\t([0-9.,]+) = (.*))#"};
// static constexpr auto component_entry_animation_faulty_keyframe_pattern =
//    ctll::fixed_string{R"#(\t([0-9.,]+) = (.*))#"};

std::optional<ao_component> ao_parser::read_component() {
    auto pi = poe::util::get_parse_info_service().current_parse();

    std::string name;
    skip_empty_lines();
    auto peeked = peek_line();
    if (!peeked) {
        return {};
    }
    if (RE2::FullMatch(*peeked, component_oneline_pattern, &name)) {
        consume_line();
        return ao_component(name, unknown_component_body{});
    } else if (RE2::FullMatch(*peeked, component_name_open_pattern, &name)) {
        consume_line();
    } else if (RE2::FullMatch(*peeked, component_name_pattern, &name)) {
        consume_line();

        skip_empty_lines();
        peeked = peek_line();
        if (!peeked) {
            if (pi)
                pi->record_fault("Expected component open brace, got end of file");
            return {};
        }
        if (RE2::FullMatch(*peeked, component_open_close_pattern)) {
            consume_line();
            return ao_component(name, unknown_component_body{});
        } else if (RE2::FullMatch(*peeked, component_open_pattern)) {
            consume_line();
        } else {
            if (pi)
                pi->record_fault(fmt::format("Expected component open brace, got \"{}\"", *peeked));
            return {};
        }
    } else {
        if (pi)
            pi->record_fault(fmt::format("Expected component name, got \"{}\"", *peeked));
        return {};
    }

    {
        unknown_component_body body;
        bool found_close = false;
        skip_empty_lines();
        while ((peeked = peek_line())) {
            consume_line();
            if (RE2::FullMatch(*peeked, component_close_pattern)) {
                found_close = true;
                break;
            } else {
                body.lines.push_back(std::string(*peeked));
            }
        }
        if (!found_close) {
            if (pi)
                pi->record_fault(fmt::format("Expected component close brace for \"{}\", got end of file", name));
            return {};
        }
        return ao_component(name, body);
    }

#if 0
    skip_empty_lines();
    peeked = peek_line();
    if (!peeked) {
        if (pi)
            pi->record_fault("Expected component open brace, got end of file");
        return {};
    }
    if (auto m = ctre::match<component_open_pattern>(*peeked)) {
        consume_line();
    } else {
        if (pi)
            pi->record_fault(fmt::format("Expected component open brace, got \"{}\"", *peeked));
        return {};
    }

    bool found_close = false;
    while (true) {
        skip_empty_lines();
        peeked = peek_line();
        if (!peeked) {
        }
        if (auto m = ctre::match<component_close_pattern>(*peeked)) {
            consume_line();
            break;
        } else if (auto m = ctre::match<component_entry_pattern>(*peeked)) {
            consume_line();
            auto name = m.get<1>();
            kv_component_entry entry(name.to_string(), m.get<2>().to_string());
            if (name == "animation" || name == "graphical") {
                while (true) {
                    skip_empty_lines();
                    peeked = peek_line();
                    if (!peeked) {
                        if (pi)
                            pi->record_fault(fmt::format("Expected animation frame, got end of file"));
                        return {};
                    }
                    if (auto m = ctre::match<component_entry_animation_keyframe_pattern>(*peeked)) {
                        consume_line();
                        ao_component_entry keyframe(m.get<1>().to_string(), m.get<2>().to_string());
                        entry.children_.push_back(keyframe);
                    } else {
                        break;
                    }
                }
            } else if (name == "socket" || name == "continuous_effect" || name == "view_of") {
                while (true) {
                    skip_empty_lines();
                    peeked = peek_line();
                    if (!peeked) {
                        if (pi)
                            pi->record_fault(fmt::format("Expected entry child, got end of file"));
                        return {};
                    }
                    if (auto m = ctre::match<component_entry_generic_pattern>(*peeked)) {
                        consume_line();
                        ao_component_entry child(m.get<1>().to_string(), m.get<2>().to_string());
                        entry.children_.push_back(child);
                    } else {
                        break;
                    }
                }
            }
            entries.push_back(entry);
        } else {
            if (pi)
                pi->record_fault(fmt::format("Expected component entry, got \"{}\"", *peeked));
            return {};
        }
    }
#endif

    return {};
}

std::shared_ptr<ao_parse> ao_parser::run() {
    auto pi = poe::util::get_parse_info_service().current_parse();
    auto ret = std::make_shared<ao_parse>();

    if (pi)
        pi->at_stage("reading version");
    ret->version_ = *read_version();

    if (pi)
        pi->at_stage("reading extents");
    while (auto base = read_extends()) {
        ret->extends_.push_back(*base);
    }

    if (pi)
        pi->at_stage("reading component");

    while (auto component = read_component()) {
        ret->components_.push_back(*component);
    }

    if (pi)
        pi->at_stage("finish");

    auto next_line = peek_line();
    if (next_line) {
        if (pi)
            pi->record_fault(fmt::format("Unexpected input: \"{}\"", *next_line));
        return {};
    }

    return ret;
#if 0
    {
        std::string test = "\tfoo = true\n";
        auto from = test.begin(), to = test.end();
        ast::key_value val;
        if (!parse(from, to, parser::other_member, val) || from != to) {
            LOG_F(INFO, "Failed to parse, tailing data is: {}", std::string(from, to).substr(0, 100));
            auto &k = val.first;
            auto *ph = boost::get<ast::placeholder>(&val.second);
            auto *s = boost::get<std::string>(&val.second);
            auto *f = boost::get<float>(&val.second);
            auto *b = boost::get<bool>(&val.second);
            return {};
        } else {
            return {};
        }
    }
#endif
#if 0
    {

        std::string test = R"!!(bing_bong {
    x = "hi"
}
)!!";
        auto from = test.begin(), to = test.end();
        error_handler_type error_handler(from, to, std::cerr);
        auto const p = x3::with<x3::error_handler_tag>(std::ref(error_handler))[*parser::component];

        std::vector<std::pair<std::string, ast::component>> v;
        if (!parse(from, to, p, v) || from != to) {
            LOG_F(INFO, "Failed to parse component, tailing data is: {}", std::string(from, to).substr(0, 100));
            return {};
        } else {
            return {};
        }
    }
#endif
#if 0
    {
        std::string test = R"!!(version 2
extends "beep"
extends "boop"

bing_bong {}
foo {
}

bar {
    x = "hi, there!"
    y = 42.3
    z = true
}")!!";
        auto from = test.begin(), to = test.end();
        error_handler_type error_handler(from, to, std::cerr);
        auto const p = x3::with<x3::error_handler_tag>(std::ref(error_handler))[parser::ao];

        ast::ao parsed_ao;
        if (!parse(from, to, p, parsed_ao) || from != to) {
            LOG_F(INFO, "Failed to parse AO file, tailing data is: {}", std::string(from, to).substr(0, 100));
            return {};
        } else {
            return {};
        }
    }
#endif
#if 0
    {
        ast::ao parsed_ao;
        auto from = text_.begin(), to = text_.end();
        if (!parse(from, to, parser::ao, parsed_ao) || from != to) {
            LOG_F(INFO, "Failed to parse AO file, tailing data is: {}", std::string(from, to).substr(0, 100));
            return {};
        }
    }
#endif
    return {};
}

} // namespace poe::format::ao
