#include <poe/format/tgt.hpp>

#include <poe/util/parse_ops.hpp>
#include <poe/util/utf.hpp>

#include <loguru/loguru.hpp>
#include <Windows.h>

#include <regex>
#include <sstream>

namespace poe::format::tgt {
#define TGT_BAIL(msg)                                                                                                  \
    LOG_F(WARNING, "TGT: {}", (msg));                                                                                  \
    if (IsDebuggerPresent()) {                                                                                         \
        DebugBreak();                                                                                                  \
    }                                                                                                                  \
    return std::nullopt

std::optional<Tgt> parse_tgt(std::span<std::byte const> data) {
    Tgt ret;
    auto wideText = poe::util::to_u16string_view(data);
    if (!wideText) {
        TGT_BAIL("Could not interpret as UTF-16LE");
    }
    auto text = poe::util::to_string(*wideText);

    poe::util::ParseOps po(text);
    if (auto err = po.reset().string_lit("version").spaces().integer(ret.version).eol().err()) {
        TGT_BAIL("Could not parse version");
    }

    if (ret.version > 3) {
        TGT_BAIL("Unsupported version");
    }

    while (!po.at_end()) {
        while (po.is_next_newline()) {
            po.eol();
        }
        std::string key;
        if (auto err = po.reset().string(key).err()) {
            TGT_BAIL("Could not read key");
            return std::nullopt;
        }

        // Common keys to all formats first, then branch for distinct formats
        if (key == "NormalMaterials") {
            size_t matCount;
            if (po.reset().spaces().integer(matCount).eol().err()) {
                return std::nullopt;
            }
            ret.normalMaterials.resize(matCount);
            for (auto &mat : ret.normalMaterials) {
                if (ret.version == 1) {
                    int _;
                    if (po.reset().whitespace().quoted_string(mat).spaces().integer(_).eol().err()) {
                        TGT_BAIL("Could not read v1 normal material");
                    }
                } else if (ret.version == 2) {
                    int idx;
                    int _;
                    if (po.reset()
                            .whitespace()
                            .integer(idx)
                            .whitespace()
                            .quoted_string(mat)
                            .spaces()
                            .integer(_)
                            .eol()
                            .err()) {
                        TGT_BAIL("Could not read v2 normal material");
                    }
                } else {
                    if (po.reset().whitespace().quoted_string(mat).eol().err()) {
                        TGT_BAIL("Could not read v3+ normal material");
                    }
                }
            }
        } else if (key == "TileMesh") {
            if (po.reset().spaces().quoted_string(ret.tileMesh).eol().err()) {
                TGT_BAIL("Could not read tile mesh");
            }
        } else if (key == "Size") {
            if (po.reset().spaces().integer(ret.size.x).spaces().integer(ret.size.y).eol().err()) {
                TGT_BAIL("Could not read size");
            }
        } else if (key == "TileMeshRoot") {
            if (po.reset().spaces().quoted_string(ret.tileMeshRoot).eol().err()) {
                TGT_BAIL("Could not read tile mesh root");
            }
        } else if (key == "GroundMask") {
            std::string groundMask;
            if (po.reset().spaces().quoted_string(groundMask).eol().err()) {
                TGT_BAIL("Could not read ground mask");
            }
            ret.groundMask = groundMask;
        } else if (key == "SubTileMaterialIndices") {
            ret.subTileMaterialIndices.resize(ret.size.x * ret.size.y);
            if (po.reset().eol().err()) {
                TGT_BAIL("Sub tile material indices list too short");
            }
            for (auto &stmi : ret.subTileMaterialIndices) {
                while (!po.is_next_newline()) {
                    size_t matIdx;
                    if (po.whitespace().integer(matIdx).err()) {
                        TGT_BAIL("Unexpected non-integer sub tile material index");
                    }
                    stmi.push_back(matIdx);
                }
                po.eol();
            }
        } else {
            TGT_BAIL(fmt::format("Unexpected key {}", key));
        }
    }

    return ret;
}
} // namespace poe::format::tgt
