#pragma once

#include <gsl/span>

#include <array>
#include <memory>
#include <optional>
#include <string>
#include <vector>

namespace poe::format::sm {
using BBox = std::array<float, 6>;
using Vec3 = std::array<float, 3>;

struct SMMaterial {
    std::string path_;
    std::optional<size_t> value_;
};

struct BoneGroup {
};

struct SM {
    uint8_t version_{};
    std::string skinned_mesh_data_;
    std::vector<SMMaterial> materials_;
    std::optional<BBox> bbox_{};
    std::vector<BoneGroup> boneGroups_;
};

std::shared_ptr<SM> parse_sm(std::span<std::byte const> data);
} // namespace poe::format::sm