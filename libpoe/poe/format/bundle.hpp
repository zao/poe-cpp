#pragma once

#include <optional>
#include <memory>
#include <vector>

#include <span>

namespace poe::format::bundle {
struct parsed_bundle {
  public:
    std::optional<std::vector<std::byte>> decompress_range(uint64_t range_offset, uint64_t range_size) const;
    std::optional<std::vector<std::byte>> decompress_all() const;

    std::vector<uint32_t> block_sizes_;
    uint64_t uncompressed_size_;
    std::span<std::byte const> compressed_storage_;
};

std::shared_ptr<parsed_bundle> parse_bundle(std::span<std::byte const> data);
} // namespace poe::format::bundle