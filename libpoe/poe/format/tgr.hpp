#pragma once

#include <gsl/span>
#include <memory>
#include <optional>
#include <string>
#include <utility>
#include <vector>

namespace poe::format::tgr {
using Coord2 = std::pair<size_t, size_t>;

struct TGRNode {
    size_t x_;
    size_t y_;
    std::vector<size_t> nums_;
    std::string tag_;
    std::string orientation_;
    std::vector<std::string> notes_;
    std::array<size_t, 4> tails_;
};

struct TGREdge {
    size_t from_node_;
    size_t to_node_;
    std::vector<Coord2> coords_;
    size_t big_;
    std::optional<size_t> b_;
    std::string et_;
    std::array<size_t, 2> tails_;
    std::optional<std::string> tail_word_;
};

struct TGRFile {
    size_t version_;
    Coord2 size_;
    std::optional<Coord2> resolution_;
    std::string master_file_;
    std::string gt_top_;
    std::string gt_mid_;
    std::string gt_bot_;
    std::vector<TGRNode> nodes_;
    std::vector<TGREdge> edges_;
};

std::shared_ptr<TGRFile> parse_tgr(std::span<std::byte const> data);
} // namespace poe::format::tgr