#include <poe/format/bundle.hpp>

#include <poe/util/decompress.hpp>
#include <poe/util/reader.hpp>

#include <algorithm>

namespace {
struct bundle_fixed_header {
    uint32_t uncompressed_size_;
    uint32_t total_payload_size_;
    uint32_t head_payload_size_;
    enum encoding_schemes { Kraken_6 = 8, Mermaid_A = 9, Leviathan_C = 13 };
    uint32_t first_file_encode_;
    uint32_t unk10_;
    uint64_t uncompressed_size2_;
    uint64_t total_payload_size2_;
    uint32_t block_count_;
    uint32_t unk28_[5];
};
} // namespace

namespace poe::format::bundle {
std::optional<std::vector<std::byte>> parsed_bundle::decompress_range(uint64_t range_offset,
                                                                      uint64_t range_size) const {
    static uint32_t const BLOCK_SIZE = (256 << 10);
    std::vector<std::byte> ret;
    ret.reserve(range_size);

    auto first_block = range_offset / BLOCK_SIZE;
    auto sub_offset = range_offset % BLOCK_SIZE;

    auto tail = std::span<std::byte const>(compressed_storage_);
    std::vector<std::byte> compressed_block;
    auto remaining = range_size;
    for (size_t i = 0; i < first_block; ++i) {
        tail = tail.subspan(block_sizes_[i]);
    }
    for (size_t i = first_block; remaining; ++i) {
        auto input_block_size = block_sizes_[i];
        auto output_size = (i + 1 < this->block_sizes_.size()) ? BLOCK_SIZE : (uncompressed_size_ - i * BLOCK_SIZE);
        compressed_block.assign(tail.data(), tail.data() + input_block_size); // we need to copy here as Oodle decompression mutates the input
        auto out = poe::util::decompress_oodle(compressed_block, output_size);
        if (!out) {
            return {};
        }
        auto copy_amount = (std::min)(remaining, output_size - sub_offset);
        auto copy_start = out->begin() + sub_offset;
        ret.insert(ret.end(), copy_start, copy_start + copy_amount);
        tail = tail.subspan(input_block_size);
        sub_offset = 0;
        remaining -= copy_amount;
    }

    return ret;
}

std::optional<std::vector<std::byte>> parsed_bundle::decompress_all() const {
    std::vector<std::byte> ret;
    ret.reserve(uncompressed_size_);

    auto tail = std::span<std::byte const>(compressed_storage_);
    std::vector<std::byte> compressed_block;
    auto remaining = uncompressed_size_;
    for (auto input_block_size : this->block_sizes_) {
        compressed_block.assign(tail.data(), tail.data() + input_block_size);
        auto output_size = std::min<uint64_t>(256 * (1 << 10), remaining);
        auto out = poe::util::decompress_oodle(compressed_block, output_size);
        if (!out) {
            return {};
        }
        ret.insert(ret.end(), out->begin(), out->end());
        tail = tail.subspan(input_block_size);
        remaining -= output_size;
    }

    return ret;
}

std::shared_ptr<parsed_bundle> parse_bundle(std::span<std::byte const> src) {
    poe::util::reader r(src);

    bundle_fixed_header fix_h{};
    if (!r.read(fix_h.uncompressed_size_, fix_h.total_payload_size_, fix_h.head_payload_size_, fix_h.first_file_encode_,
                fix_h.unk10_, fix_h.uncompressed_size2_, fix_h.total_payload_size2_, fix_h.block_count_,
                fix_h.unk28_)) {
        return {};
    }

    auto ret = std::make_shared<parsed_bundle>();
    ret->uncompressed_size_ = fix_h.uncompressed_size2_;
    ret->block_sizes_.resize(fix_h.block_count_);
    if (!r.read(ret->block_sizes_)) {
        return {};
    }

    if (r.remaining() < fix_h.total_payload_size2_) {
        return {};
    }

    auto *p = reinterpret_cast<std::byte const *>(r.data());
    ret->compressed_storage_ = std::span<std::byte const>(p, p + fix_h.total_payload_size2_);

    return ret;
}
} // namespace poe::format::bundle