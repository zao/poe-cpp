#include <poe/format/gft.hpp>

#include <poe/util/parse_ops.hpp>
#include <poe/util/utf.hpp>

#include <loguru/loguru.hpp>

using namespace std::string_view_literals;

namespace poe::format::gft {
std::shared_ptr<GftFile> parse_gft(std::span<std::byte const> data) {
    auto input_text = poe::util::map_unicode_to_utf8_string(data);
    if (!input_text) {
        LOG_F(WARNING, "Could not detect encoding of input text");
        return {};
    }
    auto view = std::string_view(input_text.value());

    auto ret = std::make_shared<GftFile>();
    poe::util::ParseOps po(view);
    if (auto err = po.reset().string_lit("version").spaces().integer(ret->version).eol().err()) {
        LOG_F(WARNING, "version line error: {}", *err);
        return {};
    }

    // Although we read this value, we don't use it as there is enough structure to delimit sections anyway.
    std::optional<int> sectionCount;
    if (ret->version == 1) {
        if (auto err = po.reset().integer(sectionCount).eol().err()) {
            LOG_F(WARNING, "section count error: {}", *err);
            return {};
        }
    }

    while (!po.at_end()) {
        while (po.is_next_newline()) {
            po.eol();
        }

        std::string key;
        if (auto err = po.reset().quoted_string(key).err()) {
            LOG_F(WARNING, "ground key error: {}", *err);
            return {};
        }

        auto &section = ret->sections[key];
        po.whitespace_opt(); // some files have tailing whitespace on section key line without a tail value
        if (!po.is_next_newline() && !po.at_end()) {
            if (auto err = po.reset().integer(section.keyTailVal).eol().err()) {
                LOG_F(WARNING, "ground key val error: {}", *err);
                return {};
            }
        } else {
            po.eol();
        }

        // Like the section count, we don't use this entry count either as it's also self-delimiting.
        std::optional<int> entryCount;
        if (ret->version == 1) {
            if (auto err = po.reset().integer(entryCount).eol().err()) {
                LOG_F(WARNING, "entry count error: {}", *err);
                return {};
            }
        }
        while (po.is_next_digit() || po.is_next_newline()) {
            if (po.is_next_newline()) {
                po.eol();
                continue;
            }
            GftEntry e;
            if (auto err = po.reset().integer(e.val).whitespace().quoted_string(e.path).err()) {
                LOG_F(WARNING, "fill tile error: {}", *err);
                return {};
            }
            po.whitespace_opt();
            if (!po.is_next_newline() && !po.at_end()) {
                if (auto err = po.reset().string(e.mut).err()) {
                    LOG_F(WARNING, "fill tile mutation error: {}", *err);
                    return {};
                }
            }
            section.entries.push_back(std::move(e));
            if (!po.at_end()) {
                po.eol();
            }
        }
    }

    return ret;
}
} // namespace poe::format::gft
