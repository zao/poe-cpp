#include <poe/format/rs.hpp>

#include <poe/util/parse_ops.hpp>
#include <poe/util/utf.hpp>

#include <loguru/loguru.hpp>
#include <sstream>

using namespace std::string_view_literals;

namespace poe::format::rs {
std::shared_ptr<RSFile> parse_rs(std::span<std::byte const> data) {
    auto wide_sv = poe::util::to_u16string_view(data);
    if (!wide_sv) {
        return {};
    }
    auto input_text = poe::util::to_string(*wide_sv);
    auto view = std::string_view(input_text);

    auto ret = std::make_shared<RSFile>();
    poe::util::ParseOps po(view);
    if (auto err = po.reset().string_lit("version").spaces().integer(ret->version_).eol().err()) {
        LOG_F(WARNING, "version line error: {}", *err);
        return {};
    }

    if (ret->version_ > 2) {
        LOG_F(WARNING, "Version not supported: {}", ret->version_);
        return {};
    }

    while (!po.at_end()) {
        po.reset().whitespace_opt(); // skip possible leading whitespace
        if (po.is_next_char('\\')) {
            // assume leading backslash is a "comment"
            std::string _;
            po.reset().rest_of_line(_).eol().err();
            continue;
        }
        if (po.is_next_newline()) {
            // skip empty lines
            po.reset().eol();
            continue;
        }
        RSEntry entry;
        if (po.is_next_digit()) {
            if (auto err = po.reset().integer(entry.number_).whitespace().err()) {
                LOG_F(WARNING, "Entry number error: {}", *err);
                return {};
            }
        }
        if (auto err = po.reset().quoted_string(entry.arm_ref_).err()) {
            LOG_F(WARNING, "Entry ARM reference error: {}", *err);
            return {};
        }

        // collect tail entries like I FI R270
        if (!po.is_next_newline() && !po.at_end()) {
            std::string poseText;
            po.rest_of_line(poseText);
            std::istringstream iss(poseText);
            std::string pose;
            while (iss >> pose) {
                entry.poses_.push_back(pose);
            }
            if (!iss.eof()) {
                LOG_F(WARNING, "Entry ARM pose error");
                return {};
            }
        }
        ret->entries_.push_back(std::move(entry));
    }

    return ret;
}
} // namespace poe::format::rs