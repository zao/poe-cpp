#include <poe/format/gt.hpp>

#include <poe/util/parse_ops.hpp>
#include <poe/util/utf.hpp>

#include <loguru/loguru.hpp>

using namespace std::string_view_literals;

namespace poe::format::gt {
std::shared_ptr<GtFile> parse_gt(std::span<std::byte const> data) {
    auto input_text = poe::util::map_unicode_to_utf8_string(data);
    if (!input_text) {
        LOG_F(WARNING, "Could not detect encoding of input text");
        return {};
    }
    auto view = std::string_view(input_text.value());

    auto ret = std::make_shared<GtFile>();
    poe::util::ParseOps po(view);
    if (auto err = po.reset().string(ret->id).eol().err()) {
        LOG_F(WARNING, "id line error: {}", *err);
        return {};
    }

    if (auto err = po.reset().integer(ret->nums[0]).spaces().integer(ret->nums[1]).err()) {
        LOG_F(WARNING, "number line #1-2 error: {}", *err);
        return {};
    }
    for (int i = 2; i < 6; ++i) {
        if (!po.is_next_newline() && !po.at_end()) {
            if (auto err = po.reset().spaces().integer(ret->nums[i]).err()) {
                LOG_F(WARNING, "number line #{} error: {}", i + 1, *err);
                return {};
            }
        } else {
            break;
        }
    }
    if (po.is_next_newline()) {
        po.eol();
    }

    if (!po.at_end()) {
        if (auto err = po.reset().quoted_string(ret->option).eol().err()) {
            LOG_F(WARNING, "option error: {}", *err);
            return {};
        }
    }

    if (!po.at_end()) {
        LOG_F(WARNING, "data after expected end of input");
        return {};
    }

    return ret;
}
} // namespace poe::format::gt
