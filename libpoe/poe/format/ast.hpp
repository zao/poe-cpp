#pragma once
#include <memory>
#include <optional>
#include <span>
#include <string>
#include <vector>
#include <glm/gtc/quaternion.hpp>
#include <glm/vec4.hpp>

namespace poe::format::ast {
struct Bone {
    uint8_t nextSibling, firstChild;
    float transform[16];
    uint8_t nameFlag;
    std::string name;
};

struct Shape {
    uint8_t unk1;
    std::vector<float> a;
    std::vector<uint8_t> b;
    std::string name;

    void SetVersion(int version);
};

struct AnimationHeader {
    uint8_t trackCount;
    uint8_t unk1_0, frameRate, unk1_2;
    std::optional<uint8_t> blendFlag;
    uint32_t offset, size;
    std::string name;
    std::optional<std::string> baseName;
};

struct Track {
    struct QuatKey {
        float time;
        glm::quat v; // storage format is XYZW both in animation files and glm
    };
    struct Vec3Key {
        float time;
        glm::vec3 v;
    };

    uint8_t unk1;
    uint32_t boneIdx;

    std::vector<Vec3Key> translations, translations2;
    std::vector<QuatKey> rotations, rotations2;
    std::vector<Vec3Key> scales, scales2;
};

struct Animation {
    std::vector<Track> tracks;
};

struct AstFile {
    int version;

    std::vector<Bone> bones;
    uint8_t attachmentCount;
    std::vector<Shape> shapes;
    std::vector<AnimationHeader> animationHeaders;
    std::vector<Animation> animations;
    std::vector<std::byte> data;

    std::optional<float> Duration(int animIdx) const;
};

std::shared_ptr<AstFile> parse_ast(std::span<std::byte const> data);
} // namespace poe::format::ast
