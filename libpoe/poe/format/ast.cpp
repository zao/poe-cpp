#include <poe/format/ast.hpp>
#define _AMD64_

#include <fmt/format.h>

#include <loguru/loguru.hpp>

#include <poe/format/bundle.hpp>
#include <poe/util/parse_info.hpp>
#include <poe/util/parse_ops.hpp>
#include <poe/util/random_access_file.hpp>
#include <poe/util/utf.hpp>

#include <debugapi.h>

namespace poe::format::ast {

#define AST_BAIL(msg)                                                                                                  \
    LOG_F(WARNING, "SMD: {}", (msg));                                                                                  \
    if (IsDebuggerPresent()) {                                                                                         \
        /*DebugBreak();*/                                                                                              \
    }                                                                                                                  \
    return {}

void Shape::SetVersion(int version) {
    if (version >= 9) {
        a.resize(14);
        b.resize(2);
    } else if (version == 8) {
        a.resize(13);
        b.resize(2);
    } // versions < 8 not handled, see binary template
}

std::shared_ptr<AstFile> parse_ast(std::span<std::byte const> data) {
    std::shared_ptr<AstFile> ret = std::make_shared<AstFile>();
    auto r = poe::util::make_stream_reader(data, 0);

    uint8_t version;
    if (!r->read_one(version)) {
        AST_BAIL("Could not read version");
    }

    if (version < 8) {
        AST_BAIL("Versions below 8 not supported");
    }

    ret->version = version;

    uint8_t boneCount;
    uint16_t animCount;
    uint8_t unkHead[2];
    uint8_t shapeCount;

    if (!r->read_one(boneCount, ret->attachmentCount, animCount, unkHead, shapeCount)) {
        AST_BAIL("Could not read header counts");
    }

    for (int boneIdx = 0; boneIdx < boneCount; ++boneIdx) {
        Bone bone{};
        uint8_t nameByteCount;
        if (!r->read_one(bone.nextSibling, bone.firstChild, bone.transform, nameByteCount, bone.nameFlag)) {
            AST_BAIL("Could not read bone header");
        }
        bone.name.resize(nameByteCount);
        if (!r->read_seq(bone.name)) {
            AST_BAIL("Could not read bone name");
        }
        ret->bones.push_back(std::move(bone));
    }

    for (int shapeIdx = 0; shapeIdx < shapeCount; ++shapeIdx) {
        Shape shape{};
        shape.SetVersion(version);
        uint8_t nameByteCount;

        if (!r->read_one(nameByteCount, shape.unk1) || !r->read_seq(shape.a, shape.b)) {
            AST_BAIL("Could not read shape header");
        }

        shape.name.resize(nameByteCount);
        if (!r->read_seq(shape.name)) {
            AST_BAIL("Could not read shape name");
        }
        ret->shapes.push_back(std::move(shape));
    }

    for (int animIdx = 0; animIdx < animCount; ++animIdx) {
        AnimationHeader anim{};
        uint8_t nameByteCount, baseNameByteCount;

        if (!r->read_one(anim.trackCount, anim.unk1_0, anim.frameRate, anim.unk1_2)) {
            AST_BAIL("Could not read animation header part 1a");
        }

        if (version >= 11) {
            if (!r->read_one(anim.blendFlag)) {
                AST_BAIL("Could not read animation header part 1b");
            }
        }

        if (!r->read_one(nameByteCount)) {
            AST_BAIL("Could not read animation header name length");
        }

        if (version > 9) {
            if (!r->read_one(baseNameByteCount)) {
                AST_BAIL("Could not read animation blend flag (?)");
            }
        }

        if (version >= 8) {
            if (!r->read_one(anim.offset, anim.size)) {
                AST_BAIL("Could not read animation header data extent");
            }
        }

        anim.name.resize(nameByteCount);
        if (!r->read_seq(anim.name)) {
            AST_BAIL("Could not read animation header name");
        }

        if (anim.blendFlag && *anim.blendFlag) {
            std::string s(baseNameByteCount, '\0');
            if (!r->read_seq(s)) {
                AST_BAIL("Could not read animation header base name");
            }
            anim.baseName = s;
        }

        ret->animationHeaders.push_back(std::move(anim));
    }

    auto tail = r->tail();
    auto bundle = poe::format::bundle::parse_bundle(tail);
    if (!bundle) {
        AST_BAIL("Could not parse animation bundle");
    }

    auto payload = bundle->decompress_all();
    if (!payload) {
        AST_BAIL("Could not decompress animation bundle");
    }

    ret->data = std::move(*payload);
    r.reset();

    // First phase complete, time to parse the compressed animation payload:

    struct TrackHeader {
        uint32_t scaleCount, rotationCount, translationCount;
        uint32_t scaleCount2, rotationCount2, translationCount2;
    };

    for (auto &hdr : ret->animationHeaders) {
        auto dataSpan = std::span<std::byte const>(ret->data).subspan(hdr.offset, hdr.size);
        // LOG_F(INFO, "data span {}+{}", hdr.offset, hdr.size);
        auto r2 = poe::util::make_stream_reader(dataSpan, 0);

        Animation anim{};
        anim.tracks.resize(hdr.trackCount);

        for (auto &track : anim.tracks) {
            TrackHeader trackHdr{};
            uint32_t unk2{}; // v11

            if (!r2->read_one(track.unk1, track.boneIdx, trackHdr)) {
                AST_BAIL("Could not read animation track header");
            }
            if (version == 11) {
                if (!r2->read_one(unk2)) {
                    AST_BAIL("Could not read extra animation track header word");
                }
            }

            auto const sanityLimit = r2->tail().size_bytes();
            if (trackHdr.scaleCount > sanityLimit || trackHdr.rotationCount > sanityLimit ||
                trackHdr.translationCount > sanityLimit || trackHdr.scaleCount2 > sanityLimit ||
                trackHdr.rotationCount2 > sanityLimit || trackHdr.translationCount2 > sanityLimit) {
                AST_BAIL("Track entry counts larger than the remaining animation data, likely desynced.");
            }

            track.scales.resize(trackHdr.scaleCount);
            track.rotations.resize(trackHdr.rotationCount);
            track.translations.resize(trackHdr.translationCount);
            track.scales2.resize(trackHdr.scaleCount2);
            track.rotations2.resize(trackHdr.rotationCount2);
            track.translations2.resize(trackHdr.translationCount2);

            if (!r2->read_seq(track.scales, track.rotations, track.translations, track.scales2, track.rotations2,
                              track.translations2)) {
                AST_BAIL("Could not read animation track data");
            }
        }

        if (r2->tail().size()) {
            AST_BAIL("Animation had extra data at the end");
        }

        ret->animations.push_back(std::move(anim));
    }

    return ret;
}

std::optional<float> AstFile::Duration(int animIdx) const {
#define RETURN_LAST_TIME_IF_PRESENT(Collection)                                                                        \
    if (!(Collection).empty()) {                                                                                       \
        return (Collection).back().time;                                                                               \
    }

    auto &anim = animations[animIdx];
    for (auto &track : anim.tracks) {
        RETURN_LAST_TIME_IF_PRESENT(track.translations)
        RETURN_LAST_TIME_IF_PRESENT(track.rotations)
        RETURN_LAST_TIME_IF_PRESENT(track.scales)
        RETURN_LAST_TIME_IF_PRESENT(track.translations2)
        RETURN_LAST_TIME_IF_PRESENT(track.rotations2)
        RETURN_LAST_TIME_IF_PRESENT(track.scales2)
    }
    return std::nullopt;
#undef RETURN_LAST_TIME_IF_PRESENT
}
} // namespace poe::format::ast
