#pragma once

#include <gsl/span>
#include <map>
#include <memory>
#include <optional>
#include <string>

namespace poe::format::tsi {
	struct TSIFile {
		std::optional<std::string> find_quoted_field(std::string_view name) const;

		size_t version_;
		std::map<std::string, std::string, std::less<>> fields_;
	};

	std::shared_ptr<TSIFile> parse_tsi(std::span<std::byte const> data);
}