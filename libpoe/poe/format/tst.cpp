#include <poe/format/tst.hpp>

#include <poe/util/parse_ops.hpp>
#include <poe/util/utf.hpp>

#include <charconv>
#include <regex>
#include <sstream>
#include <string>

namespace poe::format::tst {
std::optional<Tst> parse_tst(std::span<std::byte const> data) {
    Tst ret;
    auto wideText = poe::util::to_u16string_view(data);
    if (!wideText) {
        return std::nullopt;
    }
    auto text = poe::util::to_string(*wideText);

    std::istringstream iss(text);
    std::string line;

    std::regex blank_re{R"#(^\s*$)#"};
    std::regex entry_re{R"#(^\s*(\d+)?\s*"?([^"]*)"\s*(.*)\s*$)#"}; // first quote may be omitted, typo exists in game files
    std::regex mut_re{R"#(\S+)#"};

    Tst::Entries currentSet;
    while (std::getline(iss, line)) {
        std::smatch m;

        // Check for blank and append part.
        // Stray characters "sq" found in Innocent Hideout, assuming typo on blank line
        if (std::regex_match(line, blank_re) || line == "sq\r") {
            if (!currentSet.empty()) {
                ret.parts.push_back(currentSet);
                currentSet.clear();
            }
        } else if (std::regex_match(line, m, entry_re)) {
            Tst::Entry entry;
            if (m[1].matched) {
                int value;
                if (auto res = std::from_chars(&*m[1].first, &*m[1].second, value, 10); res.ec != std::errc{}) {
                    return std::nullopt;
                }
                entry.value = value;
            }
            entry.tdt = m[2];
            if (auto &tail = m[3]; m[3].matched) {
                for (std::sregex_iterator I(tail.first, tail.second, mut_re), J; I != J; ++I) {
                    std::smatch match = *I;
                    entry.orientations.push_back(match.str());
                }
            }
            currentSet.push_back(entry);
        } else {
            return std::nullopt;
        }
    }

    if (!currentSet.empty()) {
        ret.parts.push_back(std::move(currentSet));
    }

    return ret;
}
} // namespace poe::format::tst

/*
"Metadata/Terrain/Blank.tdt"
100 "Metadata/Terrain/forced_blank.tdt"
"Metadata/Terrain/Missions/Tiles/Oriath_generic_mission_01_01.tdt"

*/