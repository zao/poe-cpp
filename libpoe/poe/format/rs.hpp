#pragma once

#include <gsl/span>
#include <memory>
#include <optional>
#include <string>
#include <utility>
#include <vector>

namespace poe::format::rs {
using Coord2 = std::pair<size_t, size_t>;

struct RSEntry {
    std::optional<size_t> number_;
    std::string arm_ref_;
    std::vector<std::string> poses_;
};

struct RSFile {
    size_t version_;
    std::vector<RSEntry> entries_;
};

std::shared_ptr<RSFile> parse_rs(std::span<std::byte const> data);
} // namespace poe::format::rs