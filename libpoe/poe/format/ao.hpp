#pragma once

#include <memory>
#include <span>
#include <string>
#include <string_view>
#include <utility>
#include <variant>
#include <vector>

namespace poe::format::ao {
struct kv_component_entry {
    explicit kv_component_entry(std::string key, std::string value) : key_(key), value_(value) {}

    std::string key_;
    std::string value_;
    std::vector<kv_component_entry> children_;
};

struct unknown_component_body {
    std::vector<std::string> lines;
};

struct kv_component_body {
    std::vector<kv_component_entry> entries_;
};

using component_body = std::variant<unknown_component_body, kv_component_body>;
using ao_component = std::pair<std::string, component_body>;

struct ao_parse {
    size_t version_;
    std::vector<std::string> extends_;
    std::vector<ao_component> components_;

    ao_component const* find_component(std::string_view name) const;
};
// Share data types/parse between ao/aoc
std::shared_ptr<ao_parse> parse_ao(std::span<std::byte const> data);
}