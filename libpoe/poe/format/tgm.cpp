#include <poe/format/tgm.hpp>
#define _AMD64_

#include <fmt/format.h>

#include <loguru/loguru.hpp>

#include <poe/util/parse_info.hpp>
#include <poe/util/parse_ops.hpp>
#include <poe/util/random_access_file.hpp>

#include <debugapi.h>

namespace poe::format::tgm {
struct MeshExtent {
    uint32_t triangleCount, vertexCount;
};

#define TGM_BAIL(msg)                                                                                                  \
    LOG_F(WARNING, "TGM: {}", (msg));                                                                                  \
    if (IsDebuggerPresent()) {                                                                                         \
        DebugBreak();                                                                                                  \
    }                                                                                                                  \
    return {}

std::shared_ptr<TgmFile> parse_tgm(std::span<std::byte const> data) {
    std::shared_ptr<TgmFile> ret = std::make_shared<TgmFile>();
    auto r = poe::util::make_stream_reader(data, 0);

    uint8_t version;
    if (!r->read_one(version)) {
        TGM_BAIL("Could not read version");
    }
    ret->version = version;

    if (version > 10) {
        TGM_BAIL("Unhandled version");
    }

    if (!r->read_one(ret->bbox)) {
        TGM_BAIL("Could not read bounding box");
    }

    if (version >= 9) {
        // read v9+
        uint16_t globalShapeCount;
        std::array<uint8_t, 4> c1bh;
        if (!r->read_one(globalShapeCount, c1bh)) {
            TGM_BAIL("Could not read shape count");
        }

        for (int geomIdx = 0; geomIdx < 2; ++geomIdx) {
            TgmGeometry geom;

            // read standard DOLm and resume parsing at the end
            {
                auto [dolm, tail] = poe::format::dolm::parse_dolm(r->tail());
                if (!dolm) {
                    TGM_BAIL("Could not read mesh");
                }
                geom.dolm = std::move(*dolm);
                r = poe::util::make_stream_reader(tail, 0);
            }

            // populate shapeBounds
            {
                geom.shapeBounds.resize(geom.dolm.shapeCount);
                for (auto &bound : geom.shapeBounds) {
                    if (version == 9 || geomIdx > 0) {
                        uint16_t ordinal;
                        if (!r->read_one(ordinal, bound.bbox)) {
                            TGM_BAIL("Could not read shape bounding box");
                        }
                        bound.ordinal = ordinal;
                    } else {
                        uint32_t ordinal;
                        if (!r->read_one(ordinal, bound.bbox)) {
                            TGM_BAIL("Could not read shape bounding box");
                        }
                        bound.ordinal = ordinal;
                    }
                }
            }

            if (geomIdx == 0) {
                ret->mainGeometry = std::move(geom);
            } else {
                ret->groundGeometry = std::move(geom);
            }
        }
        if (auto tailV9Count = c1bh[3]) {
            ret->tailEntriesV9.resize(tailV9Count);
            for (size_t t9Idx = 0; t9Idx < tailV9Count; ++t9Idx) {
                auto &t9 = ret->tailEntriesV9[t9Idx];
                if (!r->read_one(t9.u00hZero, t9.f04h, t9.u34hOne, t9.u38h)) {
                    TGM_BAIL("Could not read tail entry v9");
                }
            }
        }
    } else {
        // read v1+
#pragma pack(push, 1)
        struct GeomInfo {
            uint16_t shapeCount;
            uint32_t vertexCount, triangleCount;
        };

        struct ShapeInfo {
            uint16_t ordinal;
            BBox bbox;
            uint32_t indexBase, indexCount;
        };

#pragma pack(pop)

        GeomInfo mainGeomInfo, groundGeomInfo;
        uint8_t vertexFormat{}, tailEntryCount{}; // only in version 4+?
        uint32_t c2fh{};                          // only in version 8

        if (!r->read_one(mainGeomInfo, groundGeomInfo)) {
            TGM_BAIL("Could not read header");
        }
        if (!r->read_one(vertexFormat, tailEntryCount)) {
            TGM_BAIL("Could not read v4 header addition");
        }
        if (version == 8 && !r->read_one(c2fh)) {
            TGM_BAIL("Could not read v8 header addition");
        }

        TgmGeometry *geoms[2]{
            &ret->mainGeometry,
            &ret->groundGeometry,
        };

        GeomInfo const *geomInfos[2]{
            &mainGeomInfo,
            &groundGeomInfo,
        };

        // Read out the main and ground meshes into a geometry each
        for (size_t geomIdx = 0; geomIdx < 2; ++geomIdx) {
            auto geom = geoms[geomIdx];
            auto info = geomInfos[geomIdx];

            auto &dolm = geom->dolm;
            dolm.shapeCount = info->shapeCount;
            dolm.legacyNormals = true;
            auto mesh = std::make_shared<dolm::DolmMesh>();

            std::vector<ShapeInfo> shapes(info->shapeCount);
            if (version < 3) {
                for (auto &shape : shapes) {
                    shape.ordinal = 0;
                    if (!r->read_one(shape.bbox, shape.indexBase, shape.indexCount)) {
                        TGM_BAIL("Could not read mesh shape");
                    }
                }
            } else {
                if (!r->read_seq(shapes)) {
                    TGM_BAIL("Could not read mesh shapes");
                }
            }

            geom->shapeBounds.reserve(info->shapeCount);
            mesh->shapeExtents.reserve(info->shapeCount);
            for (auto &shape : shapes) {
                TgmGeometry::ShapeBounds bounds{.ordinal = shape.ordinal, .bbox = shape.bbox};
                dolm::DolmMesh::ShapeExtent extent{.indexStart = shape.indexBase, .indexCount = shape.indexCount};
                geom->shapeBounds.push_back(bounds);
                mesh->shapeExtents.push_back(extent);
            }

            constexpr VertexComponent const commonFormat =
                (VertexComponent)(VertexComponent::VtxPosition_3f32 | VertexComponent::VtxNormal_4i8 |
                                  VertexComponent::VtxTangent_4i8);
            if (version >= 4) {
                if (geomIdx == 0) {
                    dolm.format = (VertexComponent)(commonFormat | VertexComponent::VtxTexcoord0_2f16);
                } else {
                    if (vertexFormat == 1) {
                        dolm.format = commonFormat;
                    } else if (vertexFormat == 2) {
                        dolm.format = (VertexComponent)(commonFormat | VertexComponent::VtxTailFloat_2f16);
                    } else if (vertexFormat == 3) {
                        dolm.format = (VertexComponent)(commonFormat | VertexComponent::VtxTexcoord0_2f16 |
                                                        VertexComponent::VtxTexcoord1_2f16);
                    } else if (vertexFormat == 4) {
                        dolm.format = VertexComponent{};
                    } else {
                        TGM_BAIL("Unknown vertex format");
                    }
                }
            } else {
                if (geomIdx == 0) {
                    dolm.format = (VertexComponent)(commonFormat | VertexComponent::VtxTexcoord0_2f16);
                } else {
                    dolm.format = commonFormat;
                }
            }

            mesh->triangleCount = info->triangleCount;
            mesh->vertexCount = info->vertexCount;

            size_t const indexWidth = mesh->vertexCount > 0xFFFF ? 4 : 2;
            size_t const vertexWidth = VertexWidth(dolm.format);

            mesh->vertexData.resize(vertexWidth * mesh->vertexCount);
            mesh->indexData.resize(indexWidth * mesh->triangleCount * 3);

            if (!r->read_many(mesh->vertexData.data(), mesh->vertexData.size()) ||
                !r->read_many(mesh->indexData.data(), mesh->indexData.size())) {
                TGM_BAIL("Could not read mesh data");
            }

            dolm.meshes.push_back(std::move(mesh));
            UnbiasLegacyVertexData(dolm);
        }

        ret->tailEntries.resize(tailEntryCount);
        for (auto &entry : ret->tailEntries) {
            int dataSize{};
            if (version <= 2) {
                dataSize = 70;
            } else if (version <= 3) {
                dataSize = 74;
            } else if (version <= 4) {
                dataSize = 78;
            } else if (version <= 6) {
                dataSize = 83;
            } else {
                dataSize = 87;
            }
            entry.data.resize(dataSize);
            if (!r->read_seq(entry.data)) {
                TGM_BAIL("Could not read tail entry");
            }
        }
    }

    // Check if we've fully consumed the file
    uint8_t endTest;
    if (r->read_one(endTest)) {
        TGM_BAIL("Trailing file data after parsing");
    }

    return ret;
}
} // namespace poe::format::tgm