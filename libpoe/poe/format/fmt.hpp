#pragma once

#include <glm/vec3.hpp>

#include <poe/format/dolm.hpp>

#include <array>
#include <memory>
#include <string>
#include <variant>
#include <vector>

#include <tbb/scalable_allocator.h>

namespace poe::format::fmt {
using BBox = std::array<float, 6>;
using Vec3 = std::array<float, 3>;

struct Shape {
    std::string name;
    std::string material;
    uint32_t triangle_start;
};

using ByteVec = std::vector<std::byte, tbb::scalable_allocator<std::byte>>;

using IndexBuffer16 = std::vector<uint16_t>;
using IndexBuffer32 = std::vector<uint32_t>;
using IndexBuffer = std::variant<IndexBuffer16, IndexBuffer32>;

using HalfFloat = uint16_t;

#pragma pack(push, 1)
struct Vertex {
    glm::vec3 pos;
    std::array<std::byte, 8> unk_0h;
    std::array<HalfFloat, 2> uv;
};

struct Vertex57 {
    glm::vec3 pos;
    std::array<std::byte, 8> unk_0h;
    std::array<HalfFloat, 2> uv;
    std::array<HalfFloat, 2> uv2;
};

struct D0 {
    std::array<uint8_t, 6> unk_0h;
};

struct D1 {
    std::array<uint8_t, 12> unk_0h;
};

struct D3Generic {
    ByteVec b;
};

enum class D3Width {
    V1 = 45,
    V3 = 70,
    V4 = 78,
    V6 = 83,
    V7 = 87,
};

// struct D3V6 {
//     std::array<uint8_t, 4> unk_0h;
//     std::array<float, 12> unk_4h;
//     std::array<uint8_t, 26 + 5> unk_52h;
// };
//
// struct D3V7 {
//     std::array<uint8_t, 4> unk_0h;
//     std::array<float, 12> unk_4h;
//     std::array<uint8_t, 26 + 5 + 4> unk_52h;
// };
#pragma pack(pop)

using D3S = std::vector<D3Generic>;

struct FMTv6 {
    uint8_t version_{};

    uint32_t triangle_count, vertex_count;
    uint16_t shape_count;
    BBox bbox_{};
    uint32_t vertex_format;

    std::vector<Shape> shapes;
    IndexBuffer indices;
    std::vector<std::byte> vertex_data;
    size_t vertex_stride;

    std::vector<D0> d0s;
    std::vector<D1> d1s;
    D3S d3s;

    std::byte const *index_data() const {
        if (auto *ib = std::get_if<IndexBuffer16>(&indices)) {
            return (std::byte const *)(ib->data());
        }
        auto &ib = std::get<IndexBuffer32>(indices);
        return (std::byte const *)ib.data();
    }

    size_t index_size() const {
        if (auto *ib = std::get_if<IndexBuffer16>(&indices)) {
            return ib->size() * 2;
        }
        auto &ib = std::get<IndexBuffer32>(indices);
        return ib.size() * 4;
    }

    size_t index_width() const {
        if (auto *ib = std::get_if<IndexBuffer16>(&indices)) {
            return 2;
        }
        return 4;
    }
};

struct ShapeExtent {
    uint32_t triangle_start;
    uint32_t triangle_count;
};

struct Mesh {
    std::vector<ShapeExtent> shape_extents;

    IndexBuffer indices;
    std::vector<std::byte> vertex_data;
    size_t vertex_stride;
};

struct ShapeStrings {
    std::string name;
    std::string material;
};

struct FMTv9 {
    uint8_t version_{};
    uint8_t originalVersion_{};

    uint16_t shape_count{};
    BBox bbox_{};

    std::vector<D0> d0s;
    std::vector<D1> d1s;
    D3S d3s;

    std::vector<ShapeStrings> shape_strings_;

    poe::format::dolm::DolmGeometry dolm_;

    std::shared_ptr<poe::format::dolm::DolmMesh> Mesh(int meshIdx) const {
        if (meshIdx < 0 || meshIdx >= dolm_.meshes.size()) {
            return nullptr;
        }
        return dolm_.meshes[meshIdx];
    }

    std::span<std::byte const> vertex_span(int mesh_idx) const {
        if (auto mesh = Mesh(mesh_idx)) {
            return mesh->vertexData;
        }
        return {};
    }

    size_t vertex_stride(int mesh_idx) const { return VertexWidth(dolm_.format); }

    std::span<std::byte const> index_span(int mesh_idx) const {
        if (auto mesh = Mesh(mesh_idx)) {
            return mesh->indexData;
        }
        return {};
    }

    size_t index_width(int mesh_idx) const {
        if (auto mesh = Mesh(mesh_idx)) {
            return mesh->vertexCount > 0xFFFF ? 4 : 2;
        }
        return 0;
    }
};

using FMT = FMTv9;

std::shared_ptr<FMT> parse_fmt(std::span<std::byte const> data);
} // namespace poe::format::fmt