#include <poe/format/ffx.hpp>
#include <poe/util/hexdump.hpp>
#include <poe/util/utf.hpp>

#include <fmt/format.h>
#include <loguru/loguru.hpp>
#include <re2/re2.h>

#include <sstream>

using namespace std::string_view_literals;

namespace poe::format::ffx {
std::optional<CostDeclaration> TryParseCost(std::string_view line) {
    static re2::RE2 expr(R"(\s*cost:\s+(\S+)\s*)");
    std::string cost;
    if (RE2::FullMatch(line, expr, &cost)) {
        return CostDeclaration{.cost = cost};
    }
    return std::nullopt;
}

std::optional<DeclUniformDeclaration> TryParseDeclUniform(std::string_view line) {
    // uniform type name
    static re2::RE2 exprShort(R"(\s*uniform\s+(\S+)\s+(\S+)\s*)");

    std::string type, name;
    if (RE2::FullMatch(line, exprShort, &type, &name)) {
        return DeclUniformDeclaration{.type = type, .name = name};
    }

    // uniform type name "label" ["rangeMin" "rangeMax" "rangeDefault"] [: semantic]
    static re2::RE2 exprLong(
        R"(\s*uniform\s+(\S+)\s+(\S+)\s+("[^"]*")(?:\s+("[^"]*")\s+("[^"]*")\s+("[^"]*"))?(?:\s*:\s*(\S+))?\s*)");

    std::string label, rangeMin, rangeMax, rangeDefault, semantic;
    if (RE2::FullMatch(line, exprLong, &type, &name, &label, &rangeMin, &rangeMax, &rangeDefault, &semantic)) {
        return DeclUniformDeclaration{
            .type = type,
            .name = name,
            .ui = DeclUniformDeclaration::UiPart{.label = label, .rangeMin = rangeMin, .rangeMax = rangeMax},
            .semantic = semantic,
        };
    }
    return std::nullopt;
}

std::optional<DefineDeclaration> TryParseDefine(std::string_view line) {
    static re2::RE2 expr(R"(#define )"); // TODO(LV): this seems wrong
    std::string name;
    if (RE2::FullMatch(line, expr, &name)) {
        return DefineDeclaration{.name_ = name};
    }
    return std::nullopt;
}

std::optional<IncludeDeclaration> TryParseInclude(std::string_view line) {
    static re2::RE2 expr(R"(\s*include\s+(\S+)\s*)");
    std::string target;
    if (RE2::FullMatch(line, expr, &target)) {
        return IncludeDeclaration{.target_ = target};
    }
    return std::nullopt;
}

std::optional<KeyValueDeclaration> TryParseKeyValue(std::string_view line) {
    static re2::RE2 expr(R"(\s*(\S+)\s+(\S+)\s*)");
    std::string key, value;
    if (RE2::FullMatch(line, expr, &key, &value)) {
        return KeyValueDeclaration{.key = key, .value = value};
    }
    return std::nullopt;
}

std::optional<KeywordDeclaration> TryParseKeyword(std::string_view line) {
    static re2::RE2 expr(R"(\s*(commutative|engineonly|fragment|pixel|read|vertex|write)\s*)");
    std::string keyword;
    if (RE2::FullMatch(line, expr, &keyword)) {
        return KeywordDeclaration{.keyword = keyword};
    }
    return std::nullopt;
}

std::optional<MacroDeclaration> TryParseMacro(std::string_view line) {
    static re2::RE2 expr(R"(\s*macro\s+(\S+)\s*)");
    std::string name;
    if (RE2::FullMatch(line, expr)) {
        return MacroDeclaration{.name = name};
    }
    return std::nullopt;
}

std::optional<PreprocessorBranchDeclaration> TryParsePreprocessorBranch(std::string_view line) {
    static re2::RE2 ifdefExpr(R"(\s*#ifdef\s+(\S+)\s*)");
    static re2::RE2 endifExpr(R"(\s*#endif\s*)");
    std::string condition;
    if (RE2::FullMatch(line, ifdefExpr, &condition)) {
        return PreprocessorBranchDeclaration{.condition = condition};
    }
    if (RE2::FullMatch(line, endifExpr)) {
        return PreprocessorBranchDeclaration{};
    }
    return std::nullopt;
}

std::optional<VariableDeclaration> TryParseVariable(std::string_view line) {
    static re2::RE2 expr(R"(\s*(dynamic|in|inout|out)\s+(\S+)\s+(\S+)(?:\s*:\s*(\S+))?\s*)");
    std::string kind, type, name, semantic;
    if (RE2::FullMatch(line, expr, &kind, &type, &name, &semantic)) {
        return VariableDeclaration{.kind = kind, .type = type, .name = name, .semantic = semantic};
    }
    return std::nullopt;
}

std::shared_ptr<ParsedFfx> parse_ffx(std::span<std::byte const> data) {
    auto wide_text = poe::util::to_u16string_view(data);
    if (!wide_text) {
        return {};
    }
    auto text = poe::util::to_string(*wide_text);
    auto ret = std::make_shared<ParsedFfx>();

    std::istringstream iss(text);
    size_t line_number = 0;
    std::string line;
    enum class top_parsing {
        nothing,
        declarations,
        fragment,
    } top_state = top_parsing::nothing;
    enum class decl_parsing {
        name,
        prelude,
        open_curly,
        code,
        close_curly,
    } decl_state;
    enum class frag_parsing {
        name,
        prelude,
        open_curly,
        code,
        close_curly,
    } frag_state;

    std::optional<Declarations> decl;
    std::optional<Fragment> frag;
    std::vector<std::string> annotations;
    fmt::memory_buffer code;

    while (std::getline(iss, line)) {
        if (line.size() && line.back() == '\r') {
            line.pop_back();
        }
        line_number += 1;
        switch (top_state) {
        case top_parsing::nothing: {
            static auto const decl_lit = "DECLARATIONS "sv;
            static auto const frag_lit = "FRAGMENT "sv;
            static auto const autoinc_lit = "AUTOINCREMENT "sv;
            if (line.empty() || line.starts_with("//") || line.find_first_not_of(" \t") == std::string::npos) {
                continue;
            } else if (line.starts_with(autoinc_lit)) {
                annotations.push_back(line);
            } else if (line.starts_with(decl_lit)) {
                top_state = top_parsing::declarations;
                decl_state = decl_parsing::name;
                decl = Declarations(line.substr(decl_lit.size()));
                annotations.clear();
            } else if (line.starts_with(frag_lit)) {
                top_state = top_parsing::fragment;
                frag_state = frag_parsing::name;
                frag = Fragment(line.substr(frag_lit.size()), annotations);
                annotations.clear();
            } else {
                LOG_F(WARNING, "Unexpected top-level line in FFX file on line {}: {}", line_number, line);
                return {};
            }
        } break;
        case top_parsing::declarations: {
            switch (decl_state) {
            case decl_parsing::name:
            case decl_parsing::prelude: {
                if (line.starts_with("{{"sv)) {
                    decl_state = decl_parsing::open_curly;
                } else {
                    std::istringstream iss(line);
                    std::string token;
                    if (iss >> token) {
                        if (token == "uniform") {
                            std::string scope, type, name, array_bounds, comment;
                            if (iss >> scope >> type && std::getline(iss, name)) {
                                name = name.substr(name.find_first_not_of(" \t"));
                                if (auto bracket_pos = name.find_first_of('['); bracket_pos != name.npos) {
                                    array_bounds = name.substr(bracket_pos);
                                    name.resize(bracket_pos);
                                }
                                if (auto comment_pos = array_bounds.find("//"); comment_pos != array_bounds.npos) {
                                    comment = array_bounds.substr(comment_pos);
                                    array_bounds.resize(array_bounds.find(']') + 1);
                                }
                                UniformDeclaration ud{.scope_ = scope, .type_ = type, .name_ = name};
                                if (!array_bounds.empty()) {
                                    ud.arrayBounds_ = array_bounds;
                                }
                                if (!comment.empty()) {
                                    ud.comment_ = comment;
                                }
                                decl->declarations_.push_back(ud);
                            } else {
                                LOG_F(WARNING, "Unexpected uniform line in FFX file on line {}: {}", line_number, line);
                            }
                        } else if (token == "include") {
                            if (iss >> token) {
                                decl->declarations_.push_back(IncludeDeclaration{.target_ = token});
                            } else {
                                LOG_F(WARNING, "Unexpected include line in FFX file on line {}: {}", line_number, line);
                            }
                        } else if (token == "#define") {
                            std::string name, value;
                            if (iss >> name && std::getline(iss, value)) {
                                decl->declarations_.push_back(DefineDeclaration{.name_ = name, .value_ = value});
                            } else {
                                LOG_F(WARNING, "Unexpected define line in FFX file on line {}: {}", line_number, line);
                            }
                        } else {
                            LOG_F(WARNING, "Unexpected prelude line in FFX file on line {}: {}", line_number, line);
                        }
                    }
                    decl_state = decl_parsing::prelude;
                }
            } break;
            case decl_parsing::open_curly:
            case decl_parsing::code: {
                if (line.starts_with("}}"sv)) {
                    decl->code_ = to_string(code);
                    code.clear();
                    ret->declarationsList_.insert({decl->name_, *decl});
                    decl.reset();
                    decl_state = decl_parsing::close_curly;
                    top_state = top_parsing::nothing;
                } else {
                    fmt::format_to(fmt::appender(code), "{}\n", line);
                    decl_state = decl_parsing::code;
                }
            } break;
            case decl_parsing::close_curly:
                break;
            }
        } break;
        case top_parsing::fragment: {
            switch (frag_state) {
            case frag_parsing::name:
            case frag_parsing::prelude: {
                if (line.starts_with("{{"sv)) {
                    frag_state = frag_parsing::open_curly;
                } else {
                    if (line.find_first_not_of(" \t"sv) == line.npos) {
                        continue;
                    }
                    // TODO(LV): missing declarations
                    if (auto uniform = TryParseDeclUniform(line)) {
                        frag->declarations_.push_back(*uniform);
                    } else if (auto cost = TryParseCost(line)) {
                        frag->declarations_.push_back(*cost);
                    } else if (auto include = TryParseInclude(line)) {
                        frag->declarations_.push_back(*include);
                    } else if (auto keyword = TryParseKeyword(line)) {
                        frag->declarations_.push_back(*keyword);
                    } else if (auto kv = TryParseKeyValue(line)) {
                        frag->declarations_.push_back(*kv);
                    } else if (auto macro = TryParseMacro(line)) {
                        frag->declarations_.push_back(*macro);
                    } else if (auto ppBranch = TryParsePreprocessorBranch(line)) {
                        frag->declarations_.push_back(*ppBranch);
                    } else if (auto variable = TryParseVariable(line)) {
                        frag->declarations_.push_back(*variable);
                    } else {
                        LOG_F(WARNING, "Unexpected or malformed prelude line in FFX file on line {}: {}", line_number,
                              line);
                    }
                    frag_state = frag_parsing::prelude;
                }
            } break;
            case frag_parsing::open_curly:
            case frag_parsing::code: {
                if (line.starts_with("}}"sv)) {
                    frag->code_ = to_string(code);
                    code.clear();
                    ret->fragmentList_.insert({frag->name_, *frag});
                    frag.reset();
                    frag_state = frag_parsing::close_curly;
                    top_state = top_parsing::nothing;
                } else {
                    fmt::format_to(fmt::appender(code), "{}\n", line);
                    frag_state = frag_parsing::code;
                }
            } break;
            case frag_parsing::close_curly:
                break;
            }
        } break;
        }
    }
    return ret;
}

Declarations const *ParsedFfx::FindDeclarations(std::string_view name) const {
    auto I = declarationsList_.find(name);
    if (I != declarationsList_.end()) {
        return &I->second;
    }
    return nullptr;
}

Fragment const *ParsedFfx::FindFragment(std::string_view name) const {
    auto I = fragmentList_.find(name);
    if (I != fragmentList_.end()) {
        return &I->second;
    }
    return nullptr;
}

bool Fragment::HasKeyword(std::string_view keyword) const {
    for (auto &decl : declarations_) {
        if (auto *kw = std::get_if<KeywordDeclaration>(&decl)) {
            if (kw->keyword == keyword) {
                return true;
            }
        }
    }
    return false;
}
} // namespace poe::format::ffx