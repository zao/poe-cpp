#include <poe/format/tsi.hpp>

#include <poe/util/parse_ops.hpp>
#include <poe/util/utf.hpp>

#include <loguru/loguru.hpp>

namespace poe::format::tsi {
std::shared_ptr<TSIFile> parse_tsi(std::span<std::byte const> data) {
    auto text = poe::util::to_string(*poe::util::to_u16string_view(data));

    auto ret = std::make_shared<TSIFile>();
    poe::util::ParseOps po(text);
    if (auto err = po.reset().string_lit("version").spaces().integer(ret->version_).eol().err()) {
        LOG_F(WARNING, "version line error: {}", *err);
        return {};
    }

    while (!po.at_end()) {
        std::string key, value;
        if (auto err = po.reset().string(key).whitespace().rest_of_line(value).eol().err()) {
            LOG_F(WARNING, "field error: {}", *err);
            return {};
        }
        ret->fields_.insert_or_assign(std::move(key), std::move(value));
    }

    return ret;
}

std::optional<std::string> TSIFile::find_quoted_field(std::string_view name) const {
    if (auto I = fields_.find(name); I == fields_.end()) {
        //LOG_F(INFO, "Missing field {}", name);
        return {};
    } else {
        poe::util::ParseOps po(I->second);
        std::string value;
        if (auto err = po.reset().quoted_string(value).eol().err()) {
            LOG_F(INFO, "{}", *err);
            return {};
        }
        return value;
    }
}
} // namespace poe::format::tsi