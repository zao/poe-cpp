#include <poe/format/dgr.hpp>

#include <poe/util/parse_ops.hpp>
#include <poe/util/utf.hpp>

#include <loguru/loguru.hpp>

using namespace std::string_view_literals;

namespace poe::format::dgr {
std::shared_ptr<DGRFile> parse_dgr(std::span<std::byte const> data) {
    auto wide_sv = poe::util::to_u16string_view(data);
    if (!wide_sv) {
        return {};
    }
    auto input_text = poe::util::to_string(*wide_sv);
    auto view = std::string_view(input_text);

    auto ret = std::make_shared<DGRFile>();
    poe::util::ParseOps po(view);
    if (auto err = po.reset().string_lit("version").spaces().integer(ret->version_).eol().err()) {
        LOG_F(WARNING, "version line error: {}", *err);
        return {};
    }

    if (ret->version_ > 18) {
        // LOG_F(WARNING, "Version not supported: {}", ret->version_);
        // return {};
    }

    if (auto err = po.reset()
                       .string_lit("Size:")
                       .spaces()
                       .integer(ret->size_.first)
                       .spaces()
                       .integer(ret->size_.second)
                       .eol()
                       .err()) {
        LOG_F(WARNING, "Size line error: {}", *err);
        return {};
    }

    if (ret->version_ <= 18) {
        Coord2 resolution{};
        if (auto err = po.reset()
                           .string_lit("Resolution:")
                           .spaces()
                           .integer(resolution.first)
                           .spaces()
                           .integer(resolution.second)
                           .eol()
                           .err()) {
            LOG_F(WARNING, "Resolution line error: {}", *err);
            return {};
        }
        ret->resolution_ = resolution;
    }

    if (auto err = po.reset().string_lit("MasterFile:").spaces().quoted_string(ret->master_file_).eol().err()) {
        LOG_F(WARNING, "MasterFile line error: {}", *err);
        return {};
    }

    size_t node_count;
    if (auto err = po.reset().string_lit("Nodes:").spaces().integer(node_count).eol().err()) {
        LOG_F(WARNING, "Nodes line error: {}", *err);
        return {};
    }

    size_t edge_count;
    if (auto err = po.reset().string_lit("Edges:").spaces().integer(edge_count).eol().err()) {
        LOG_F(WARNING, "Edges line error: {}", *err);
        return {};
    }

    if (auto err = po.reset()
                       .quoted_string(ret->gt_top_)
                       .eol()
                       .quoted_string(ret->gt_mid_)
                       .eol()
                       .quoted_string(ret->gt_bot_)
                       .eol()
                       .err()) {
        LOG_F(WARNING, "GT lines error: {}", *err);
        return {};
    }

    if (auto err = po.reset()
                       .string_lit("Default%:")
                       .spaces()
                       .integer(std::get<0>(ret->default_percent_))
                       .spaces()
                       .integer(std::get<1>(ret->default_percent_))
                       .spaces()
                       .integer(std::get<2>(ret->default_percent_))
                       .eol()
                       .err()) {
        LOG_F(WARNING, "Default% lines error: {}", *err);
        return {};
    }

    for (size_t node_idx = 0; node_idx < node_count; ++node_idx) {
        DGRNode node;
        size_t val_count;
        if (auto err = po.reset().integer(node.x_).spaces().integer(node.y_).spaces().integer(val_count).err()) {
            LOG_F(WARNING, "node line error in part 1: {}", *err);
            return {};
        }
        node.vals_.resize(val_count);
        for (size_t val_idx = 0; val_idx < val_count; ++val_idx) {
            if (auto err = po.reset().spaces().integer(node.vals_[val_idx]).err()) {
                LOG_F(WARNING, "node line error in part 2: {}", *err);
                return {};
            }
        }
        size_t str_count;
        if (auto err = po.reset()
                           .spaces()
                           .quoted_string(node.key_)
                           .spaces()
                           .string(node.rotate_flip_)
                           .spaces()
                           .integer(str_count)
                           .err()) {
            LOG_F(WARNING, "node line error in part 3: {}", *err);
            return {};
        }
        node.strs_.resize(str_count);
        for (size_t str_idx = 0; str_idx < str_count; ++str_idx) {
            if (auto err = po.reset().spaces().quoted_string(node.strs_[str_idx]).err()) {
                LOG_F(WARNING, "node line error in part 4: {}", *err);
                return {};
            }
        }
        if (auto err = po.reset()
                           .spaces()
                           .integer(node.unk_num1_)
                           .spaces()
                           .integer(node.unk_num2_)
                           .spaces()
                           .string(node.unk_last_)
                           .eol()
                           .err()) {
            LOG_F(WARNING, "node line error in part 5: {}", *err);
            return {};
        }
        ret->nodes_.push_back(std::move(node));
    }

    for (size_t edge_idx = 0; edge_idx < edge_count; ++edge_idx) {
        DGREdge edge;
        size_t coord_count;
        if (auto err = po.reset()
                           .integer(edge.from_node_)
                           .spaces()
                           .integer(edge.to_node_)
                           .spaces()
                           .integer(coord_count)
                           .err()) {
            LOG_F(WARNING, "Edge line error in part 1: {}", *err);
            return {};
        }
        edge.coords_.resize(coord_count);
        for (size_t coord_idx = 0; coord_idx < coord_count; ++coord_idx) {
            auto &coord = edge.coords_[coord_idx];
            if (auto err = po.reset().spaces().integer(coord.first).spaces().integer(coord.second).err()) {
                LOG_F(WARNING, "Edge line error in part 2: {}", *err);
                return {};
            }
        }

        if (auto err = po.reset()
                           .spaces()
                           .integer(edge.unk2_num_)
                           .spaces()
                           .integer(edge.unk3_num_)
                           .spaces()
                           .quoted_string(edge.et_)
                           .spaces()
                           .integer(edge.unk4_num_)
                           .spaces()
                           .integer(edge.unk5_num_)
                           .err()) {
            LOG_F(WARNING, "Edge line error in part 3: {}", *err);
            return {};
        }

        if (ret->version_ >= 21) {
            if (auto err = po.reset().spaces().integer(edge.unk5a_num_).err()) {
                LOG_F(WARNING, "Edge line error in part 4: {}", *err);
                return {};
            }
        }

        if (ret->version_ >= 18) {
            if (auto err = po.reset().spaces().quoted_string(edge.unk5b_str_).err()) {
                LOG_F(WARNING, "Edge line error in part 5: {}", *err);
                return {};
            }
        }
        if (auto err = po.reset().spaces().integer(edge.unk6_num_).err()) {
            LOG_F(WARNING, "Edge line error in part 6: {}", *err);
            return {};
        }
        if (ret->version_ >= 20) {
            if (auto err = po.reset().spaces().quoted_string(edge.unk6a_str_).err()) {
                LOG_F(WARNING, "Edge line error in part 7: {}", *err);
                return {};
            }
        }
        if (auto err = po.reset()
                           .spaces()
                           .integer(edge.unk7_num_)
                           .spaces()
                           .integer(edge.unk8_num_)
                           .spaces()
                           .string(edge.unk9_letter_)
                           .spaces()
                           .string(edge.unk10_letter_)
                           .spaces()
                           .integer(edge.unk11_num_)
                           .eol()
                           .err()) {
            LOG_F(WARNING, "Edge line error in part 7: {}", *err);
            return {};
        }
        ret->edges_.push_back(std::move(edge));
    }

    return ret;
}
} // namespace poe::format::dgr