#include <poe/format/sm.hpp>

#include <fmt/format.h>
#include <loguru/loguru.hpp>

#include <poe/util/parse_info.hpp>
#include <poe/util/parse_ops.hpp>
#include <poe/util/utf.hpp>

namespace poe::format::sm {
std::shared_ptr<SM> parse_sm(std::span<std::byte const> data) {
    if (auto wide_view = poe::util::to_u16string_view(data)) {
        auto txt = poe::util::to_string(*wide_view);
        poe::util::ParseOps po(txt);

        SM ret;
        if (auto err = po.reset().string_lit("version").spaces().integer(ret.version_).eol().err()) {
            LOG_F(INFO, "Could not read version line: {}", *err);
            return {};
        }

        if (ret.version_ < 4 || ret.version_ > 6) {
            LOG_F(INFO, "Unknown SM version: {}", ret.version_);
            return {};
        }

        if (auto err =
                po.reset().string_lit("SkinnedMeshData").spaces().quoted_string(ret.skinned_mesh_data_).eol().err()) {
            LOG_F(INFO, "Could not read SkinnedMeshData line: {}", *err);
            return {};
        }

        size_t material_count{};
        if (auto err = po.reset().string_lit("Materials").spaces().integer(material_count).eol().err()) {
            LOG_F(INFO, "Could not read Materials line: {}", *err);
            return {};
        }
        ret.materials_.reserve(material_count);

        for (size_t mat_idx = 0; mat_idx < material_count; ++mat_idx) {
            SMMaterial mat{};
            if (auto err = po.reset().whitespace().quoted_string(mat.path_).err()) {
                LOG_F(INFO, "Could not read material line part 1: {}", *err);
                return {};
            }
            if (!po.is_next_newline()) {
                if (auto err = po.reset().spaces().integer(mat.value_).eol().err()) {
                    LOG_F(INFO, "Could not read material line part 2: {}", *err);
                    return {};
                }
            }
            ret.materials_.push_back(std::move(mat));
        }

        if (ret.version_ >= 5) {
            BBox bbox;
            if (auto err = po.reset()
                               .string_lit("BoundingBox")
                               .spaces()
                               .real(bbox[0])
                               .spaces()
                               .real(bbox[1])
                               .spaces()
                               .real(bbox[2])
                               .spaces()
                               .real(bbox[3])
                               .spaces()
                               .real(bbox[4])
                               .spaces()
                               .real(bbox[5])
                               .eol()
                               .err()) {
                LOG_F(INFO, "Could not read BoundingBox line: {}", *err);
                return {};
            }
            ret.bbox_ = bbox;
        }

        if (ret.version_ >= 6) {
            int groupCount;
            if (auto err = po.reset().string_lit("BoneGroups").spaces().integer(groupCount).eol().err()) {
                LOG_F(INFO, "Could not read BoneGroups line: {}", *err);
                return {};
            }
            ret.boneGroups_.resize(groupCount);
            for (auto &bg : ret.boneGroups_) {
            }
        }

        return std::make_shared<SM>(std::move(ret));
    }

    return {};
}
} // namespace poe::format::sm