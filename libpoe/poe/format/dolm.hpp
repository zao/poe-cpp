#pragma once

#include <gsl/span>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include <absl/container/inlined_vector.h>

#include <tbb/scalable_allocator.h>

#include <array>
#include <memory>
#include <optional>
#include <span>
#include <vector>

namespace poe::format::dolm {
struct BBox {
    glm::vec2 x, y, z;
};

enum VertexComponent {
    VtxPosition_3f32 = 1,
    VtxNormal_4i8 = 2,
    VtxTangent_4i8 = 4,
    VtxTexcoord0_2f16 = 8,
    VtxTailFloat_2f16 = 16,
    VtxSkinBones_4u8 = 32,
    VtxSkinWeights_4u8 = 64,
    VtxSkinExtra_4u8 = 128,
    VtxTexcoord1_2f16 = 256,
};

#if 0
struct TgmMesh {
    struct ShapeExtent {
        uint32_t indexStart, indexCount;
    };

    int triangleCount, vertexCount;
    std::vector<ShapeExtent> shapeExtents;

    using ByteVec = std::vector<std::byte, tbb::scalable_allocator<std::byte>>;
    ByteVec indexData;
    ByteVec vertexData;
};

struct TgmGeometry {
    VertexComponent format; // bitmask
    bool legacyNormals;

    std::vector<std::shared_ptr<TgmMesh>> meshes;

    struct ShapeBounds {
        int ordinal;
        BBox bbox;
    };

    std::vector<ShapeBounds> shapeBounds;
};

struct TgmTailEntry {
    uint32_t id;
    glm::vec3 color;
    glm::vec3 pos;
    std::array<uint8_t, 12> b1;
    float f2;
    std::array<uint8_t, 39> b2;
    std::optional<std::array<uint8_t, 4>> b3; // only in v7-v8
};

struct TgmTailEntryV9 {
    uint32_t u00hZero;
    float f04h[12];
    uint32_t u34hOne;
    uint8_t u38h[31];
};

struct TgmFile {
    int version;
    BBox bbox;
    int shapeCount;

    TgmGeometry mainGeometry, groundGeometry;

    std::vector<TgmTailEntry> tailEntries; // only in v6-v8
    std::vector<TgmTailEntryV9> tailEntriesV9; // only in v9?
};
#endif

struct DolmMesh {
    struct ShapeExtent {
        uint32_t indexStart, indexCount;
    };

    int triangleCount, vertexCount;
    std::vector<ShapeExtent> shapeExtents;

    using ByteVec = std::vector<std::byte, tbb::scalable_allocator<std::byte>>;
    ByteVec indexData;
    ByteVec vertexData;
};

struct DolmGeometry {
    uint16_t shapeCount{};
    VertexComponent format{}; // bitmask
    bool legacyNormals{false};

    using MeshVec = absl::InlinedVector<std::shared_ptr<DolmMesh>, 2>;
    MeshVec meshes;
};

struct DolmParseResult {
    std::shared_ptr<DolmGeometry> result;
    std::span<std::byte const> tail;
};

DolmParseResult parse_dolm(std::span<std::byte const> data);

size_t VertexWidth(VertexComponent format);
size_t VertexComponentOffset(VertexComponent format, VertexComponent comp);
size_t VertexComponentSize(VertexComponent comp);

// Remap legacy normals/tangents to modern SNORM convention from biased UINT
void UnbiasLegacyVertexData(DolmGeometry &geom);
} // namespace poe::format::dolm