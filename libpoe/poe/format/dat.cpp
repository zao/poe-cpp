#include <poe/format/dat.hpp>

#include <poe/util/install_location.hpp>
#include <poe/util/memmem.hpp>
#include <poe/util/random_access_file.hpp>
#include <poe/util/utf.hpp>

#include <fmt/format.h>
#include <loguru/loguru.hpp>

#include <array>
#include <deque>
#include <fstream>
#include <iterator>
#include <mutex>
#include <regex>

namespace poe::format::dat {
size_t spec_field_width(field_def const &def) {
    if (def.indirection_.empty()) {
        switch (def.type_) {
        case Bool:
            [[fallthrough]];
        case I8:
            [[fallthrough]];
        case U8:
            return 1;
        case I16:
            [[fallthrough]];
        case U16:
            return 2;
        case F32:
            [[fallthrough]];
        case I32:
            [[fallthrough]];
        case U32:
            return 4;
        case F64:
            [[fallthrough]];
        case I64:
            [[fallthrough]];
        case U64:
            return 8;
        case String:
            return 0;
        default:
            // unreachable
            abort();
            return 0;
        }
    } else if (def.indirection_.front() == Ref) {
        return 4;
    } else { // RefList
        return 8;
    }
}

field_def HIDEOUTS_DAT[] = {
    {"Id", {Ref}, String},
    {"SmallWorldAreasKey", {}, U64},
    {"Unknown", {}, I32},
    {"HideoutFile", {Ref}, String},
    {"Keys0", {RefList}, U64},
    {"LargeWorldAreasKey", {}, U64},
    {"HideoutImage", {Ref}, String},
    {"IsEnabled", {}, U8},
    {"Weight", {}, I32},
    {"Key0", {}, U64},
    {"Flag1", {}, Bool},
    {"Name", {Ref}, String},
    {"Data0", {RefList}, I32},
    {"Flag2", {}, Bool},
};

using spec_map = std::map<std::string, dat_spec>;

dat_spec::dat_spec(field_def const *defs, size_t n) : defs_(defs, defs + n) {
    for (size_t col = 0; col < n; ++col) {
        field_offsets_.push_back(byte_width_);
        auto field_width = spec_field_width(defs_[col]);
        field_widths_.push_back(field_width);
        byte_width_ += field_width;
    }
}

std::optional<size_t> dat_spec::find_field(std::string_view name) const {
    auto I = std::find_if(defs_.begin(), defs_.end(), [name](auto &def) { return def.label_ == name; });
    if (I != defs_.end()) {
        return std::distance(defs_.begin(), I);
    }
    return {};
}

static std::regex def_type_re(R"!!(\|)!!");

std::optional<field_def> parse_def(std::string_view name, std::string_view type) {
    field_def def;
    def.label_ = name;
    std::deque<std::csub_match> parts;
    std::copy(std::cregex_token_iterator(type.data(), type.data() + type.size(), def_type_re, -1),
              std::cregex_token_iterator(), std::back_inserter(parts));
    while (parts.size() > 1 && parts[0] == "ref") {
        parts.pop_front();
        if (parts.size() > 1 && parts[0] == "list") {
            def.indirection_.push_back(RefList);
            parts.pop_front();
        } else {
            def.indirection_.push_back(Ref);
        }
    }
    if (parts.size() != 1) {
        return {};
    }
    auto subtype = parts.front();
    if (subtype == "bool") {
        def.type_ = Bool;
    } else if (subtype == "byte") {
        def.type_ = U8;
    } else if (subtype == "float") {
        def.type_ = F32;
    } else if (subtype == "double") {
        def.type_ = F64;
    } else if (subtype == "int") {
        def.type_ = I32;
    } else if (subtype == "long") {
        def.type_ = I64;
    } else if (subtype == "generic") {
        def.type_ = I64;
    } else if (subtype == "short") {
        def.type_ = I16;
    } else if (subtype == "ushort") {
        def.type_ = U16;
    } else if (subtype == "string") {
        def.type_ = String;
    } else if (subtype == "uint") {
        def.type_ = U32;
    } else if (subtype == "ulong") {
        def.type_ = U64;
    } else {
        LOG_F(FATAL, "Unhandled subtype {} in type {}", std::string(subtype), std::string(type));
    }

    return def;
}

spec_map const &get_specs() {
    static std::mutex mutex;
    static std::optional<std::filesystem::file_time_type> last_change;
    static spec_map map;
    auto lk = std::lock_guard(mutex);
    auto spec_path = *poe::util::executable_dir() / "stable.json";
    auto t = std::filesystem::last_write_time(spec_path);
    if (!last_change || t > *last_change) {
        spec_map m;
        std::ifstream is(spec_path);
        nlohmann::ordered_json src;
        if (is && is >> src) {
            for (auto &[key, value] : src.items()) {
                auto fields = value["fields"];
                std::vector<field_def> defs;
                for (auto &[name, field] : fields.items()) {
                    auto type = static_cast<std::string>(field["type"]);
                    auto def = parse_def(name, field["type"]);
                    if (!def) {
                        LOG_F(WARNING, "Could not load DAT spec for {}", name);
                    }
                    defs.push_back(*def);
                }
                m.emplace(key, dat_spec(std::data(defs), std::size(defs)));
            }
        }
        map = m;
        last_change = t;
    }
    return map;
}

std::shared_ptr<dat_table> open_dat(std::byte const *p, size_t n, std::string_view filename) {
    std::array<uint8_t, 8> separator;
    std::fill(separator.begin(), separator.end(), 0xBB);
    auto data_ptr = reinterpret_cast<std::byte const *>(poe::util::memmem(p, n, separator.data(), separator.size()));
    if (!data_ptr) {
        return {};
    }
    auto data_start = data_ptr - p;

    auto reader = poe::util::make_stream_reader(p, data_start, 0);
    uint32_t row_count;
    if (!reader->read_one(row_count)) {
        return {};
    }

    std::optional<size_t> expected_width;
    if (row_count) {
        expected_width = (data_start - 4) / row_count;
    }

    auto &specs = get_specs();
    auto I = specs.find(std::string(filename));
    if (I == specs.end()) {
        if (expected_width) {
            LOG_F(WARNING, "{}: No spec available for row width, got {} in file", filename, *expected_width);
        } else {
            LOG_F(WARNING, "{}: No spec available for row width, and file has no rows", filename);
        }
        return {};
    }

    auto &spec = I->second;
    if (row_count * spec.byte_width_ + 4 != data_start) {
        if (expected_width < spec.byte_width_) {
            LOG_F(WARNING, "{}: Expected spec row width {}, got {} in file", filename, spec.byte_width_,
                  *expected_width);
            return {};
        } else {
            LOG_F(WARNING, "{}: Expected spec row width {}, got {} in file", filename, spec.byte_width_,
                  *expected_width);
        }
    }

    auto ret = std::make_shared<dat_table>();
    ret->spec_ = &spec;
    ret->rows_ = row_count;
    ret->row_stride_ = *expected_width;
    ret->storage_.assign(p, p + n);
    ret->data_start_ = data_start;
    return ret;
}

dat_table::value dat_table::read_field(field_def def, uint64_t offset) const {
    std::byte const *ptr = storage_.data() + offset;
    std::byte const *eof = storage_.data() + storage_.size();
    if (offset >= storage_.size()) {
        return {};
    }
    auto read_one = [ptr](auto t) {
        memcpy(&t, ptr, sizeof(t));
        return t;
    };
    auto read_one_or_null = [ptr](auto t) -> value {
        memcpy(&t, ptr, sizeof(t));
        if (t == -0x0101'0102ll || t == 0xFEFE'FEFEull || t == -0x0101'0101'0101'0102ll ||
            t == 0xFEFE'FEFE'FEFE'FEFEull || t == 0xFFFF'FFFFull) {
            return {};
        }
        return t;
    };
    auto read_string = [ptr, eof]() -> value {
        auto *data_ptr = reinterpret_cast<uint8_t const *>(ptr);
        auto *data_end = reinterpret_cast<uint8_t const *>(eof);
        std::vector<char16_t> codeunits;
        auto p = data_ptr;
        for (; p <= data_end - 2; p += 2) {
            if (p[0] || p[1]) {
                char16_t cu = p[0] | (static_cast<uint16_t>(p[1]) << 8);
                codeunits.push_back(cu);
            } else {
                break;
            }
        }
        if (p > data_end - 2) {
            return {};
        }
        value ret = poe::util::to_string(std::u16string_view(codeunits.data(), codeunits.size()));
        return ret;
    };
    if (def.indirection_.empty()) {
        switch (def.type_) {
        case Bool:
            return read_one(bool{});
        case F32:
            return read_one(float{});
        case F64:
            return read_one(double{});
        case I8:
            return read_one(int8_t{});
        case I16:
            return read_one(int16_t{});
        case I32:
            return read_one_or_null(int32_t{});
        case I64:
            return read_one_or_null(int64_t{});
        case U8:
            return read_one(uint8_t{});
        case U16:
            return read_one(uint16_t{});
        case U32:
            return read_one_or_null(uint32_t{});
        case U64:
            return read_one_or_null(uint64_t{});
        case String:
            return read_string();
        }
    } else {
        field_indirection ind = def.indirection_.front();
        if (ind == Ref) {
            auto ref = read_one_or_null(uint32_t{});
            if (ref.is_null()) {
                return {};
            }
            field_def sub_def{.type_ = def.type_};
            sub_def.indirection_.assign(def.indirection_.begin() + 1, def.indirection_.end());
            return read_field(sub_def, data_start_ + static_cast<uint64_t>(ref));
        } else { // RefList
            auto count = read_one(uint32_t{});
            field_def ref_def{.type_ = U32};
            auto ref = read_field(ref_def, offset + 4);
            if (ref.is_null()) {
                return {};
            }
            field_def elem_def{.type_ = def.type_};
            elem_def.indirection_.assign(def.indirection_.begin() + 1, def.indirection_.end());
            auto list_base = static_cast<uint32_t>(ref);
            auto w = spec_field_width(elem_def);
            if (list_base + count * w > data_size()) {
                return {};
            }
            std::vector<value> ret;
            for (size_t i = 0; i < count; ++i) {
                ret.push_back(read_field(elem_def, data_start_ + list_base + i * w));
            }
            return ret;
        }
    }
    return {};
}

dat_table::value dat_table::get(size_t row, size_t col) const {
    auto row_base = 4 + row * row_stride_;
    auto &def = spec_->defs_[col];
    auto field_offset = spec_->field_offsets_[col];
    return read_field(def, row_base + field_offset);
}

std::optional<size_t> dat_table::find_field(std::string_view name) const { return spec_->find_field(name); }

std::string_view dat_table::field_name(size_t i) const { return spec_->defs_[i].label_; }

size_t dat_table::field_count() const { return spec_->field_count(); }

size_t dat_table::row_count() const { return rows_; }

std::shared_ptr<dat_skeleton> open_skeleton(std::byte const *p, size_t n) {
    auto r = poe::util::make_stream_reader(p, n, 0);
    uint32_t row_count{};
    if (!r->read_one(row_count) || 4 + (size_t)row_count + 8 > n) {
        return {};
    }
    auto ret = std::make_shared<dat_skeleton>();
    ret->rows_.resize(row_count);
    auto data_ptr = (std::byte const *)poe::util::memmem(p + 4, n - 4, "\xbb\xBB\xbb\xBB\xbb\xBB\xbb\xBB", 8);
    if (!data_ptr) {
        return {};
    }
    auto row_start = 4;
    auto row_ptr = p + row_start;
    auto row_extent = data_ptr - row_ptr;

    if (row_count) {
        if (row_extent % row_count) {
            return {};
        }
        size_t row_length = row_extent / row_count;
        for (size_t i = 0; i < row_count; ++i) {
            auto base = row_ptr + i * row_length;
            ret->rows_[i].assign(base, base + row_length);
        }
    }
    ret->var_data_.assign(data_ptr, p + n);
    return ret;
}

void preload_specs() { get_specs(); }

} // namespace poe::format::dat