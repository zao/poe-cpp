#pragma once

#include <optional>
#include <span>
#include <string>
#include <vector>

#include <glm/vec2.hpp>

namespace poe::format::tgt {
struct Tgt {
    size_t version;
    glm::ivec2 size{1, 1};
    std::optional<std::string> tileMesh; // in v1
    std::optional<std::string> tileMeshRoot; // in v3
    std::optional<std::string> groundMask;
    std::vector<std::string> normalMaterials;
    std::vector<std::vector<size_t>> subTileMaterialIndices;
};

std::optional<Tgt> parse_tgt(std::span<std::byte const> data);
} // namespace poe::format::tgt