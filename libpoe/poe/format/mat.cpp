#include <poe/format/mat.hpp>

#include <loguru/loguru.hpp>

#include "poe/util/utf.hpp"

using namespace std::string_view_literals;

namespace poe::format::fxgraph {
void from_json(nlohmann::json const &j, FxGraphKey &k) {
    j.at("index").get_to(k.index);
    j.at("type").get_to(k.type);
    if (auto key = "stage"; j.contains(key)) {
        j.at(key).get_to(k.stage);
    }
}

void from_json(nlohmann::json const &j, FxGraphLink::Connector &c) {
    j.get_to(c.key);
    j.at("variable").get_to(c.variable);
    if (auto key = "swizzle"; j.contains(key)) {
        j.at(key).get_to(c.swizzle);
    }
}

void from_json(nlohmann::json const &j, FxGraphLink &l) {
    j.at("src").get_to(l.src);
    j.at("dst").get_to(l.dst);
}

void from_json(nlohmann::json const &j, FxGraphNode &n) {
    j.get_to(n.key);
    if (auto key = "custom_parameter"; j.contains(key)) {
        j.at(key).get_to(n.customParameter);
    }
    if (auto key = "parameters"; j.contains(key)) {
        j.at(key).get_to(n.parameters);
    }
    auto &pos = j.at("ui_position");
    pos.at(0).get_to(n.uiPosition.x);
    pos.at(1).get_to(n.uiPosition.y);
}

void from_json(nlohmann::json const &j, FxGraph &g) {
    j.at("version").get_to(g.version);
    if (auto key = "nodes"; j.contains(key)) {
        j.at(key).get_to(g.nodes);
    }
    if (auto key = "links"; j.contains(key)) {
        j.at(key).get_to(g.links);
    }
    if (auto key = "overriden_blend_mode"; j.contains(key)) {
        j.at(key).get_to(g.overridenBlendMode);
    }
    if (auto key = "overriden_lighting_model"; j.contains(key)) {
        j.at(key).get_to(g.overridenLightingModel);
    }
    if (auto key = "lighting_disabled"; j.contains(key)) {
        g.lightingDisabled = j.at(key).get<bool>();
    }
}

std::shared_ptr<FxGraph> parse_fxgraph_json(nlohmann::json const &js) {
    auto ret = std::make_shared<FxGraph>();
    js.get_to(*ret);
    return ret;
}
} // namespace poe::format::fxgraph

namespace poe::format::mat {
void from_json(nlohmann::json const &j, CustomParameter &cp) {
    j.at("name").get_to(cp.name);
    if (auto key = "parameters"; j.contains(key)) {
        j.at(key).get_to(cp.parameters);
    }
}

void from_json(nlohmann::json const &j, MatGraphInstance &gi) {
    j.at("parent").get_to(gi.parent);
    if (auto key = "custom_parameters"; j.contains(key)) {
        j.at(key).get_to(gi.customParameters);
    }
}

void from_json(nlohmann::json const &j, MatTexture::Source &s) { j.at("filename").get_to(s.filename); }

void from_json(nlohmann::json const &j, MatTexture &t) {
    j.at("filename").get_to(t.filename);
    j.at("format").get_to(t.format);
    j.at("sources").get_to(t.sources);
    j.at("count").get_to(t.count);
}

void from_json(nlohmann::json const &j, MatFileV4 &m) {
    j.at("version").get_to(m.version);
    m.defaultGraph = poe::format::fxgraph::parse_fxgraph_json(j.at("defaultgraph"));
    if (auto key = "textures"; j.contains(key)) {
        j.at(key).get_to(m.textures);
    }
    if (auto key = "graphinstances"; j.contains(key)) {
        j.at(key).get_to(m.graphInstances);
    }
}

std::shared_ptr<MatFileV4> parse_mat(std::span<std::byte const> data) {
    auto input_text = poe::util::map_unicode_to_utf8_string(data);
    if (!input_text) {
        LOG_F(WARNING, "Could not detect encoding of input text");
        return {};
    }
    auto view = std::string_view(input_text.value());

    auto ret = std::make_shared<MatFileV4>();
    auto js = nlohmann::json::parse(view);
    js.get_to(*ret);
    return ret;
}
} // namespace poe::format::mat
