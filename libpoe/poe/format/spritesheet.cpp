#include <poe/format/spritesheet.hpp>
#include <poe/util/reader.hpp>
#include <poe/util/utf.hpp>

#include <charconv>
#include <iomanip>
#include <sstream>

namespace poe::format::spritesheet {
	std::shared_ptr<sheet> parse_spritesheet(std::span<std::byte const> data) {
		auto wide_text = poe::util::to_u16string_view(data);
		if (!wide_text) {
			return {};
		}
		auto text = poe::util::to_string(*wide_text);

		auto ret = std::make_shared<sheet>();
		std::istringstream iss(text);

		sheet::entry e;
		while (iss >> std::quoted(e.id) >> std::quoted(e.path) >> e.x0 >> e.y0 >> e.x1 >> e.y1) {
			ret->entries_.push_back(e);
			e = {};
		}
		for (size_t i = 0; i < ret->entries_.size(); ++i) {
			auto const& e = ret->entries_[i];
			ret->by_id_[e.id] = i;
		}

		return ret;
	}
}