#include <poe/format/dolm.hpp>
#define _AMD64_

#include <fmt/format.h>

#include <loguru/loguru.hpp>

#include <poe/util/parse_info.hpp>
#include <poe/util/parse_ops.hpp>
#include <poe/util/random_access_file.hpp>
#include <poe/util/utf.hpp>

#include <debugapi.h>

namespace poe::format::dolm {
struct MeshExtent {
    uint32_t triangleCount, vertexCount;
};

#define DOLM_BAIL(msg)                                                                                                 \
    LOG_F(WARNING, "DOLM: {}", (msg));                                                                                 \
    if (IsDebuggerPresent()) {                                                                                         \
        /*DebugBreak();*/                                                                                              \
    }                                                                                                                  \
    return {}

template <typename T> using ScalableVec = std::vector<T, tbb::scalable_allocator<T>>;

DolmParseResult parse_dolm(std::span<std::byte const> data) {
    std::shared_ptr<DolmGeometry> ret = std::make_shared<DolmGeometry>();
    auto r = poe::util::make_stream_reader(data, 0);

    char tag[4];
    uint16_t c0h;
    uint8_t lodCount;
    uint32_t vertexFormatId;

    if (!r->read_one(tag, c0h, lodCount, ret->shapeCount, vertexFormatId)) {
        DOLM_BAIL("Could not read header");
    }
    if (memcmp(tag, "DOLm", 4) != 0) {
        DOLM_BAIL("Tag mismatch");
    }

    switch (vertexFormatId) {
    case 0:
        ret->format = (VertexComponent)0;
        break;
    case 48:
        ret->format = (VertexComponent)(VtxPosition_3f32 | VtxNormal_4i8 | VtxTangent_4i8);
        break;
    case 56:
        ret->format = (VertexComponent)(VtxPosition_3f32 | VtxNormal_4i8 | VtxTangent_4i8 | VtxTexcoord0_2f16);
        break;
    case 57:
        ret->format = (VertexComponent)(VtxPosition_3f32 | VtxNormal_4i8 | VtxTangent_4i8 | VtxTexcoord0_2f16 |
                                        VtxTexcoord1_2f16);
        break;
    case 60:
        ret->format = (VertexComponent)(VtxPosition_3f32 | VtxNormal_4i8 | VtxTangent_4i8 | VtxTexcoord0_2f16 |
                                        VtxSkinBones_4u8 | VtxSkinWeights_4u8);
        break;
    case 58:
        // TODO(LV): Final four bytes of uncertain purpose.
        ret->format =
            (VertexComponent)(VtxPosition_3f32 | VtxNormal_4i8 | VtxTangent_4i8 | VtxTexcoord0_2f16 | VtxSkinExtra_4u8);
        break;
    case 62:
        // TODO(LV): Final four bytes of uncertain purpose.
        ret->format = (VertexComponent)(VtxPosition_3f32 | VtxNormal_4i8 | VtxTangent_4i8 | VtxTexcoord0_2f16 |
                                        VtxSkinBones_4u8 | VtxSkinWeights_4u8 | VtxSkinExtra_4u8);
        break;
    default:
        DOLM_BAIL(fmt::format("Unhandled vertex format: {}", vertexFormatId));
    }

    struct HeaderLodExtent {
        uint32_t triangleCount, vertexCount;
    };

    absl::InlinedVector<HeaderLodExtent, 2> headerLodExtents(lodCount);
    if (!r->read_seq(headerLodExtents)) {
        DOLM_BAIL("Could not read LoD extents");
    }

    for (int lodIdx = 0; lodIdx < lodCount; ++lodIdx) {
        auto lodMesh = std::make_shared<DolmMesh>();

        auto &lodExtent = headerLodExtents[lodIdx];
        lodMesh->triangleCount = lodExtent.triangleCount;
        lodMesh->vertexCount = lodExtent.vertexCount;
        lodMesh->shapeExtents.resize(ret->shapeCount);

        auto vertexStride = VertexWidth(ret->format);
        auto indexStride = lodExtent.vertexCount > 0xFFFF ? 4 : 2;
        lodMesh->indexData.resize(lodExtent.triangleCount * 3 * indexStride);
        lodMesh->vertexData.resize(lodExtent.vertexCount * vertexStride);

        if (!r->read_seq(lodMesh->shapeExtents, lodMesh->indexData, lodMesh->vertexData)) {
            DOLM_BAIL("Could not read mesh");
        }
        ret->meshes.push_back(lodMesh);
    }

    return DolmParseResult{
        .result = ret,
        .tail = data.subspan(r->cursor()),
    };
}

size_t VertexWidth(VertexComponent comps) {
    size_t size{};
    if (comps & VtxPosition_3f32) {
        size += 12;
    }
    if (comps & VtxNormal_4i8) {
        size += 4;
    }
    if (comps & VtxTangent_4i8) {
        size += 4;
    }
    if (comps & VtxTexcoord0_2f16) {
        size += 4;
    }
    if (comps & VtxTailFloat_2f16) {
        size += 4;
    }
    if (comps & VtxSkinBones_4u8) {
        size += 4;
    }
    if (comps & VtxSkinWeights_4u8) {
        size += 4;
    }
    if (comps & VtxSkinExtra_4u8) {
        size += 4;
    }
    if (comps & VtxTexcoord1_2f16) {
        size += 4;
    }
    return size;
}

size_t VertexComponentOffset(VertexComponent format, VertexComponent comp) {
    if ((format & comp) == 0) {
        return 0;
    }
    size_t off{};
#define COMP_CASE(C, N)                                                                                                \
    if (comp == (C)) {                                                                                                 \
        return off;                                                                                                    \
    }                                                                                                                  \
    if (format & (C)) {                                                                                                \
        off += (N);                                                                                                    \
    }
    COMP_CASE(VtxPosition_3f32, 12)
    COMP_CASE(VtxNormal_4i8, 4)
    COMP_CASE(VtxTangent_4i8, 4)
    COMP_CASE(VtxTexcoord0_2f16, 4)
    COMP_CASE(VtxTailFloat_2f16, 4)
    COMP_CASE(VtxSkinBones_4u8, 4)
    COMP_CASE(VtxSkinWeights_4u8, 4)
    COMP_CASE(VtxSkinExtra_4u8, 4)
    COMP_CASE(VtxTexcoord1_2f16, 4)
    abort();
}

size_t VertexComponentSize(VertexComponent comp) {
    switch (comp) {
    case VtxPosition_3f32:
        return 12;
    case VtxNormal_4i8:
    case VtxTangent_4i8:
    case VtxTexcoord0_2f16:
    case VtxTailFloat_2f16:
    case VtxSkinBones_4u8:
    case VtxSkinWeights_4u8:
    case VtxSkinExtra_4u8:
    case VtxTexcoord1_2f16:
        return 4;
    }
    abort();
}

namespace {
void Unbias(std::span<std::byte> values) {
    for (auto &val : values) {
        val = (std::byte)((int8_t)val - 128);
    }
}
} // namespace

void UnbiasLegacyVertexData(DolmGeometry &geom) {
    if (geom.legacyNormals) {
        auto const vertexStride = VertexWidth(geom.format);
        auto const normalOffset = dolm::VertexComponentOffset(geom.format, VertexComponent::VtxNormal_4i8);
        auto const tangentOffset = dolm::VertexComponentOffset(geom.format, VertexComponent::VtxTangent_4i8);
        for (auto &mesh : geom.meshes) {
            std::span vertices(mesh->vertexData);
            for (size_t vtxIdx = 0; vtxIdx < mesh->vertexCount; ++vtxIdx) {
                auto vertex = vertices.subspan(vtxIdx * vertexStride);
                if (geom.format & VertexComponent::VtxNormal_4i8) {
                    Unbias(vertex.subspan(normalOffset, 4));
                }
                if (geom.format & VertexComponent::VtxTangent_4i8) {
                    Unbias(vertex.subspan(tangentOffset, 4));
                }
            }
        }
        geom.legacyNormals = false;
    }
}

} // namespace poe::format::dolm