#pragma once

#include <optional>
#include <span>
#include <string>
#include <vector>

namespace poe::format::tst {
struct Tst {
    struct Entry {
        std::optional<int> value;
        std::string tdt;
        std::vector<std::string> orientations;
    };

    using Entries = std::vector<Entry>;

    std::vector<Entries> parts;
};

std::optional<Tst> parse_tst(std::span<std::byte const> data);
}