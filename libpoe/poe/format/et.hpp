#pragma once

#include <array>
#include <memory>
#include <optional>
#include <span>
#include <string>

namespace poe::format::et {
struct EtVirtual {
    std::string etPaths[2];
    int etVals[2];
    int nums[2];
};

struct EtFile {
    std::string id;
    std::string color;
    std::string gts[2];
    int nums[6];
    std::optional<std::string> middleGt;
    std::array<int, 4> wetNums; // related to historical water flow
    std::optional<EtVirtual> virt;
};

std::shared_ptr<EtFile> parse_et(std::span<std::byte const> data);
} // namespace poe::format::et