#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <variant>
#include <vector>

#include <poe/io/vfs.hpp>

namespace poe::format::dds {
	struct proxy_texture {
		uint32_t version_;
		uint32_t full_width_;
		uint32_t full_height_;
		using payload_t = std::vector<std::byte>;
		using symlink_t = std::string;
		std::variant<payload_t, symlink_t> body_;
	};

	std::shared_ptr<proxy_texture> parse_dds_header(std::span<std::byte const> data);
}