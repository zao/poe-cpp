#include <poe/format/index.hpp>

#include <poe/format/bundle.hpp>
#include <poe/util/decompress.hpp>
#include <poe/util/reader.hpp>

namespace {
/* The blocks in the tail section of the index bundle are generated through an
 * alternating-phase algorithm where in the first phase a set of strings are built
 * to serve as bases for the second phase where they are combined with suffixes
 * to form the full outputs of the algorithm.
 *
 * u32 words control the generation and strings are null-terminated UTF-8.
 * A zero word toggles the phase, input starts with a 0 to indicate the base phase.
 *
 * In the base phase a non-zero word inserts a new string into the base set or
 * inserts a the concatenation of the base string and the following input.
 *
 * In the generation phase a non-zero word references a string in the base set
 * and outputs the concatenation of the base string and the following input.
 *
 * The index words are one-based where 1 refers to the first string.
 *
 * A zero can toggle back to the generation phase in which all base strings shall
 * be removed as if starting from scratch.
 *
 * The template section can be empty (two zero words after each other), typically
 * done to emit a single string as-is.
 */
bool generate_paths(std::span<std::byte const> dir_rec, std::function<bool(std::string_view)> const &cb) {
    poe::util::reader r(dir_rec);

    bool base_phase = false;
    std::vector<std::string> bases;
    std::vector<std::string> results;
    while (r.n_) {
        uint32_t cmd;
        if (!r.read(cmd)) {
            abort();
        }
        if (cmd == 0) {
            base_phase = !base_phase;
            if (base_phase) {
                bases.clear();
            }
        }
        if (cmd != 0) {
            std::string fragment;
            if (!r.read(fragment)) {
                return false;
            }

            // the input is one-indexed
            size_t index = cmd - 1;
            if (index < bases.size()) {
                // Back-ref to existing string, concatenate and insert/emit.
                std::string full = bases[index] + fragment;
                if (base_phase) {
                    bases.push_back(full);
                } else {
                    if (!cb(full)) {
                        return false;
                    }
                }
            } else {
                // New string, insert or emit as-is.
                if (base_phase) {
                    bases.push_back(fragment);
                } else {
                    if (!cb(fragment)) {
                        return false;
                    }
                }
            }
        }
    }

    return true;
}
} // namespace

namespace poe::format::index {
void parsed_index::enumerate_directory_files(size_t record_index, enumerate_directory_files_fn op) const {
    if (record_index >= directory_records_.size()) {
        return;
    }
    auto const& rec = directory_records_[record_index];
    auto slice = std::span<std::byte const>(directory_storage_).subspan(rec.offset_, rec.size_);
    generate_paths(slice, [&](auto path) {
        op(path);
        return true;
    });
}

std::shared_ptr<parsed_index> parse_index(std::span<std::byte const> src) {
    auto index_bundle = poe::format::bundle::parse_bundle(src);
    if (!index_bundle) {
        return {};
    }
    auto index_storage = index_bundle->decompress_all();
    if (!index_storage) {
        return {};
    }

    poe::util::reader r(*index_storage);

    auto idx = std::make_shared<parsed_index>();
    uint32_t bundle_count;
    if (!r.read(bundle_count)) {
        return {};
    }
    idx->bundle_records_.reserve(bundle_count);

    for (size_t i = 0; i < bundle_count; ++i) {
        bundle_record rec;

        uint32_t name_length;
        if (!r.read(name_length)) {
            return {};
        }

        std::vector<char> name_buf(name_length);
        if (!r.read(name_buf)) {
            return {};
        }
        rec.name_.assign(name_buf.begin(), name_buf.end());

        if (!r.read(rec.uncompressed_size_)) {
            return {};
        }

        idx->bundle_records_.push_back(rec);
    }

    uint32_t file_count;
    if (!r.read(file_count)) {
        return {};
    }
    idx->file_records_.reserve(file_count);

    for (size_t i = 0; i < file_count; ++i) {
        file_record rec;

        if (!r.read(rec.path_hash_, rec.bundle_index_, rec.file_offset_, rec.file_size_)) {
            return {};
        }

        idx->path_hash_to_file_record_[rec.path_hash_] = i;
        idx->file_records_.push_back(rec);
    }

    uint32_t directory_count;
    if (!r.read(directory_count)) {
        return {};
    }
    idx->directory_records_.reserve(directory_count);

    for (size_t i = 0; i < directory_count; ++i) {
        directory_record rec;
        if (!r.read(rec.hash_, rec.offset_, rec.size_, rec.recursive_size_)) {
            return {};
        }
        idx->directory_records_.push_back(rec);
    }

    std::span<std::byte const> tail(reinterpret_cast<std::byte const *>(r.p_), r.n_);
    auto tail_bundle = poe::format::bundle::parse_bundle(tail);
    if (auto directory_storage = tail_bundle->decompress_all()) {
        idx->directory_storage_ = std::move(*directory_storage);
    } else {
        return {};
    }

    return idx;
}
} // namespace poe::format::index