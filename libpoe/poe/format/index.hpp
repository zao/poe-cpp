#pragma once

#include <cstddef>
#include <cstdint>
#include <functional>
#include <memory>
#include <string_view>
#include <unordered_map>
#include <vector>

#include <gsl/span>

namespace poe::format::index {
struct bundle_record {
    std::string name_;
    uint32_t uncompressed_size_;
};

struct file_record {
    uint64_t path_hash_;
    uint32_t bundle_index_;
    uint32_t file_offset_;
    uint32_t file_size_;
};

struct directory_record {
    uint64_t hash_;
    uint32_t offset_;
    uint32_t size_;
    uint32_t recursive_size_;
};

struct parsed_index {
    std::vector<bundle_record> bundle_records_;
    std::vector<file_record> file_records_;
    std::vector<directory_record> directory_records_;
    std::unordered_map<uint64_t, size_t> path_hash_to_file_record_;

    size_t directory_record_count() const { return directory_records_.size(); }

    using enumerate_directory_files_fn = std::function<void (std::string_view path)>;
    void enumerate_directory_files(size_t record_index, enumerate_directory_files_fn op) const;

    std::vector<std::byte> directory_storage_;
};

std::shared_ptr<parsed_index> parse_index(std::span<std::byte const> data);
} // namespace poe::format::index