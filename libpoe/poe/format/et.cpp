#include <poe/format/et.hpp>

#include <poe/util/parse_ops.hpp>
#include <poe/util/utf.hpp>

#include <loguru/loguru.hpp>

using namespace std::string_view_literals;

namespace poe::format::et {
std::shared_ptr<EtFile> parse_et(std::span<std::byte const> data) {
    auto input_text = poe::util::map_unicode_to_utf8_string(data);
    if (!input_text) {
        LOG_F(WARNING, "Could not detect encoding of input text");
        return {};
    }
    auto view = std::string_view(input_text.value());

    auto ret = std::make_shared<EtFile>();
    poe::util::ParseOps po(view);
    if (auto err = po.reset().string(ret->id).err()) {
        LOG_F(WARNING, "id line id error: {}", *err);
        return {};
    }

    if (!po.is_next_newline()) {
        if (auto err = po.reset().spaces().string(ret->color).err()) {
            LOG_F(WARNING, "id line color error: {}", *err);
            return {};
        }
    }
    if (auto err = po.reset().eol().err()) {
        LOG_F(WARNING, "id line eol error: {}", *err);
        return {};
    }

    for (int i = 0; i < 2; ++i) {
        if (auto err = po.reset().string(ret->gts[i]).eol().err()) {
            LOG_F(WARNING, "gt line error: {}", *err);
            return {};
        }
    }

    // TODO(LV): replace the gnarly newline logic with reading a line and parsing that
    int numIdx = 0;
    if (!po.at_end()) {
        for (int i = 0; i < 6; ++i) {
            // workaround for the file that has "1-" as a number
            if (po.is_next_char('-')) {
                po.string_lit("-");
            }
            if (!po.at_end() && !po.is_next_newline()) {
                if (i != 0) {
                    if (auto err = po.reset().spaces().err()) {
                        LOG_F(WARNING, "number line spaces #{} error: {}", i + 1, *err);
                        return {};
                    }
                }
                if (po.at_end() || !po.is_next_newline()) {
                    if (auto err = po.reset().integer(ret->nums[i]).err()) {
                        LOG_F(WARNING, "number line entry #{} error: {}", i + 1, *err);
                        return {};
                    }
                }
            }
        }
        po.eol();
    }

    auto bpo = po;
    if (!po.at_end()) {
        // Three cases here: virtual, et entry, or water flow nums
        if (po.is_next_char('v')) {
            po = bpo;
        } else if (po.is_next_digit()) {
            for (int i = 0; i < ret->wetNums.size(); ++i) {
                if (!po.at_end() && !po.is_next_newline()) {
                    if (i != 0) {
                        if (auto err = po.reset().spaces().err()) {
                            LOG_F(WARNING, "water flow line spaces #{} error: {}", i + 1, *err);
                            return {};
                        }
                    }
                    if (po.at_end() || !po.is_next_newline()) {
                        if (auto err = po.reset().integer(ret->wetNums[i]).err()) {
                            LOG_F(WARNING, "water flow line entry #{} error: {}", i + 1, *err);
                            return {};
                        }
                    }
                }
            }
        } else {
            if (auto err = po.string(ret->middleGt).eol().err()) {
                LOG_F(WARNING, "middle-gt error: {}", *err);
                return {};
            }
        }
    }

    if (!po.at_end()) {
        if (auto err = po.reset().string_lit("virtual").eol().err()) {
            LOG_F(WARNING, "virtual line error: {}", *err);
            return {};
        }
        EtVirtual virt;
        for (int i = 0; i < 2; ++i) {
            if (auto err =
                    po.reset().string(virt.etPaths[i]).spaces().integer(virt.etVals[i]).whitespace_opt().eol().err()) {
                LOG_F(WARNING, "virtual et line error: {}", *err);
                return {};
            }
        }
        if (auto err = po.reset().integer(virt.nums[0]).spaces().integer(virt.nums[1]).whitespace_opt().eol().err()) {
            LOG_F(WARNING, "virtual number line error: {}", *err);
            return {};
        }
        ret->virt = virt;
    }

    if (!po.at_end()) {
        LOG_F(WARNING, "data after expected end of input");
        return {};
    }

    return ret;
}
} // namespace poe::format::et
