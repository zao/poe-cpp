#include <poe/format/fmt.hpp>
#define _AMD64_

#include <fmt/format.h>

#include <poe/util/parse_info.hpp>
#include <poe/util/random_access_file.hpp>
#include <poe/util/utf.hpp>

#include <algorithm>
#include <span>

#include <debugapi.h>

namespace poe::format::fmt {
std::shared_ptr<FMTv6> parse_fmt_v6(std::span<std::byte const> data) {
    auto r = poe::util::make_stream_reader(data, 0);
    auto pi = poe::util::get_parse_info_service().current_parse();
    auto ret = std::make_shared<FMTv6>();

    if (pi) {
        pi->at_stage("version");
    }

    if (!r->read_one(ret->version_)) {
        if (pi) {
            pi->record_fault("Could not read version.");
        }
        return {};
    }
    if (ret->version_ < 2 || ret->version_ > 9) {
        if (pi) {
            pi->record_fault(::fmt::format("Version {} not in supported range [2, 9]", ret->version_));
        }
        return {};
    }

    struct FixedHeader {
        uint32_t triangle_count, vertex_count;
        uint16_t shape_count;
        std::array<uint8_t, 4> unk_Bh; // TODO(LV): figure out the meaning of these bytes
        std::array<float, 6> bbox;
        uint32_t vertex_format{};
    } header;

    if (pi) {
        pi->at_stage("header");
    }

    if (!r->read_one(header.triangle_count, header.vertex_count, header.shape_count, header.unk_Bh, header.bbox)) {
        if (pi) {
            pi->record_fault(::fmt::format("Could not read fixed header."));
        }
        return {};
    }

    if (ret->version_ >= 8) {
        if (!r->read_one(header.vertex_format)) {
            if (pi) {
                pi->record_fault(::fmt::format("Could not read fixed header tail."));
            }
            return {};
        }
    }

    ret->triangle_count = header.triangle_count;
    ret->vertex_count = header.vertex_count;
    ret->shape_count = header.shape_count;
    size_t d0_count = header.unk_Bh[0];
    size_t d1_count = header.unk_Bh[1] | ((uint16_t)header.unk_Bh[2] << 8);
    size_t d3_count = header.unk_Bh[3];
    ret->bbox_ = header.bbox;
    ret->vertex_format = header.vertex_format;

    // if (std::any_of(header.unk_Bh.begin(), header.unk_Bh.end(), [](uint8_t b) { return b != 0; })) {
    if (ret->vertex_format != 0) {
        // DebugBreak();
    }

    struct RawShape {
        uint32_t name_offset, material_name_offset, triangle_start;
    };

    if (pi) {
        pi->at_stage("shapes");
    }

    std::vector<RawShape> shapes(header.shape_count);
    for (size_t shape_idx = 0; shape_idx < header.shape_count; ++shape_idx) {
        auto &shape = shapes[shape_idx];
        if (!r->read_one(shape.name_offset, shape.material_name_offset, shape.triangle_start)) {
            if (pi) {
                pi->record_fault(::fmt::format("Could not read shape."));
            }
            return {};
        }
    }

    if (pi) {
        pi->at_stage("d0");
    }

    auto &d0s = ret->d0s;
    d0s.resize(d0_count);
    if (!r->read_seq(d0s)) {
        if (pi) {
            pi->record_fault(::fmt::format("Could not read d0s."));
        }
        return {};
    }

    if (pi) {
        pi->at_stage("indices");
    }

    auto validate_triangle_indices = [&](auto const &buf) -> bool {
        for (auto index : buf) {
            if (index >= header.vertex_count) {
                if (pi) {
                    pi->record_fault(::fmt::format("Triangle index too large."));
                }
                return false;
            }
        }
        return true;
    };

    if (header.vertex_count > 0xFFFF) {
        IndexBuffer32 buf(header.triangle_count * 3);
        if (!r->read_seq(buf)) {
            if (pi) {
                pi->record_fault(::fmt::format("Could not read triangles."));
            }
            return {};
        }
        if (!validate_triangle_indices(buf)) {
            return {};
        }
        ret->indices = std::move(buf);
    } else {
        IndexBuffer16 buf(header.triangle_count * 3);
        if (!r->read_seq(buf)) {
            if (pi) {
                pi->record_fault(::fmt::format("Could not read triangles."));
            }
            return {};
        }
        if (!validate_triangle_indices(buf)) {
            return {};
        }
        ret->indices = std::move(buf);
    }

    if (pi) {
        pi->at_stage("vertices");
    }

    constexpr auto const VertexFormatStride = [](uint8_t version, uint32_t vertex_format) -> size_t {
        switch (version) {
        case 8: {
            switch (vertex_format) {
            case 0:
                return sizeof(Vertex);
            case 1:
                return sizeof(Vertex57);
            default:
                DebugBreak();
            }
        }
        default:
            return sizeof(Vertex);
        }
    };

    ret->vertex_stride = VertexFormatStride(ret->version_, ret->vertex_format);
    ret->vertex_data.resize(header.vertex_count * ret->vertex_stride);
    if (!r->read_seq(ret->vertex_data)) {
        if (pi) {
            pi->record_fault(::fmt::format("Could not read vertices."));
        }
        return {};
    }

    if (pi) {
        pi->at_stage("d1");
    }

    auto &d1s = ret->d1s;
    d1s.resize(d1_count);
    if (!r->read_seq(d1s)) {
        if (pi) {
            pi->record_fault(::fmt::format("Could not read d1s."));
        }
        return {};
    }

    if (pi) {
        pi->at_stage("d3");
    }

    {
        std::vector<D3Generic> d3s(d3_count);
        size_t d3Width{};
        switch (ret->version_) {
        case 1:
        case 2:
            d3Width = (size_t)D3Width::V1;
            break;
        case 3:
            d3Width = (size_t)D3Width::V3;
            break;
        case 4:
        case 5:
            d3Width = (size_t)D3Width::V4;
            break;
        case 6:
            d3Width = (size_t)D3Width::V6;
            break;
        case 7:
        case 8:
            d3Width = (size_t)D3Width::V7;
            break;
        default:
            if (pi) {
                pi->record_fault(::fmt::format("Unknown D3 size for version {}", ret->version_));
            }
            return {};
        }
        for (auto &d3 : d3s) {
            d3.b.resize(d3Width);
            if (!r->read_seq(d3.b)) {
                if (pi) {
                    pi->record_fault(::fmt::format("Could not read d3s."));
                }
                return {};
            }
        }
        ret->d3s = d3s;
    }

    if (pi) {
        pi->at_stage("string_table");
    }

    uint32_t string_table_size;
    if (!r->read_one(string_table_size)) {
        if (pi) {
            pi->record_fault(::fmt::format("Could not read string table size."));
        }
        return {};
    }
    if (string_table_size > data.size()) {
        if (pi) {
            pi->record_fault(::fmt::format("String table size larger than whole file."));
        }
        return {};
    }

    std::vector<char16_t> string_table(string_table_size);
    if (!r->read_seq(string_table)) {
        if (pi) {
            pi->record_fault(::fmt::format("Could not read string table."));
        }
        return {};
    }

    if (!string_table.empty() && string_table.back() != u'\0') {
        if (pi) {
            pi->record_fault(::fmt::format("String table malformed."));
        }
        return {};
    }

    if (pi) {
        pi->at_stage("eof");
    }

    std::byte end_sentinel;
    if (r->read_one(end_sentinel)) {
        if (pi) {
            pi->record_fault(::fmt::format("Additional data found at end of file."));
        }
        return {};
    }

    if (pi) {
        pi->at_stage("resolve");
    }

    auto resolve_string = [&string_table](uint32_t offset) -> std::optional<std::string> {
        if (offset >= string_table.size()) {
            return {};
        }
        return poe::util::to_string(std::u16string_view(&string_table[offset]));
    };

    // Resolve shape strings against string table
    for (auto &shape : shapes) {
        auto name = resolve_string(shape.name_offset);
        auto material = resolve_string(shape.material_name_offset);
        Shape s{
            .name = *name,
            .material = *material,
            .triangle_start = shape.triangle_start,
        };
        ret->shapes.push_back(std::move(s));
    }

    if (pi) {
        pi->at_stage("finish");
    }
    return ret;
}

std::shared_ptr<FMTv9> into_fmt_v9(std::shared_ptr<FMTv6> old) {
    using poe::format::dolm::VertexComponent;
    auto ret = std::make_shared<FMTv9>();
    ret->version_ = 9;
    ret->originalVersion_ = old->version_;
    ret->shape_count = old->shape_count;
    ret->bbox_ = old->bbox_;

    ret->d0s = old->d0s;
    ret->d1s = old->d1s;
    ret->d3s = old->d3s;

    auto &geom = ret->dolm_;

    geom.shapeCount = old->shape_count;

    if (old->version_ < 8 || old->vertex_format == 0) {
        geom.format = (VertexComponent)(VertexComponent::VtxPosition_3f32 | VertexComponent::VtxNormal_4i8 |
                                        VertexComponent::VtxTangent_4i8 | VertexComponent::VtxTexcoord0_2f16);
    } else if (old->vertex_format == 1) {
        geom.format = (VertexComponent)(VertexComponent::VtxPosition_3f32 | VertexComponent::VtxNormal_4i8 |
                                        VertexComponent::VtxTangent_4i8 | VertexComponent::VtxTexcoord0_2f16 |
                                        VertexComponent::VtxTexcoord1_2f16);
    } else {
        return {};
    }

    geom.legacyNormals = true; // TODO(LV): Find the version where the normals change kind.

    auto mesh = std::make_shared<poe::format::dolm::DolmMesh>();
    geom.meshes.push_back(mesh);
    mesh->triangleCount = old->triangle_count;
    mesh->vertexCount = old->vertex_count;

    mesh->shapeExtents.reserve(old->shapes.size());
    for (auto &shape : old->shapes) {
        ShapeStrings strings;
        strings.name = shape.name;
        strings.material = shape.material;
        ret->shape_strings_.push_back(strings);

        using ShapeExtent = poe::format::dolm::DolmMesh::ShapeExtent;
        ShapeExtent extent;
        extent.indexStart = 3 * shape.triangle_start;

        // Fix-up triangle count for previous shape
        if (!mesh->shapeExtents.empty()) {
            auto &prev = mesh->shapeExtents.back();
            prev.indexCount = 3 * shape.triangle_start - prev.indexStart;
        }
        mesh->shapeExtents.push_back(extent);
    }

    // Fix-up triangle count for last shape
    if (!mesh->shapeExtents.empty()) {
        auto &prev = mesh->shapeExtents.back();
        prev.indexCount = 3 * old->triangle_count - prev.indexStart;
    }

    std::span<std::byte const> indexData;
    if (auto const *indices = std::get_if<IndexBuffer16>(&old->indices)) {
        indexData = std::span(reinterpret_cast<std::byte const *>(indices->data()), indices->size() * sizeof(uint16_t));
    } else if (auto const *indices = std::get_if<IndexBuffer32>(&old->indices)) {
        indexData = std::span(reinterpret_cast<std::byte const *>(indices->data()), indices->size() * sizeof(uint32_t));
    }
    mesh->indexData.assign(indexData.begin(), indexData.end());
    mesh->vertexData.assign(old->vertex_data.begin(), old->vertex_data.end());

    UnbiasLegacyVertexData(geom);

    return ret;
}

bool extract_utf16le_string(std::span<uint16_t const> mass, uint32_t offset, std::string &out) {
    if (offset >= mass.size()) {
        return false;
    }

    auto text = mass.subspan(offset);
    auto term = std::find(text.begin(), text.end(), 0);
    if (term == mass.end()) {
        return false;
    }
    text = text.subspan(0, term - text.begin());

    out = poe::util::to_string(std::u16string_view((char16_t const *)text.data(), text.size()));
    return true;
}

std::shared_ptr<FMTv9> parse_fmt_v9(std::span<std::byte const> data) {
    auto r = poe::util::make_stream_reader(data, 0);
    auto pi = poe::util::get_parse_info_service().current_parse();
    auto ret = std::make_shared<FMTv9>();

    uint8_t version;
    if (!r->read_one(version) || version != 9) {
        return nullptr;
    }
    ret->version_ = version;
    ret->originalVersion_ = version;

    uint8_t d0{};
    uint16_t d1{};
    uint8_t d3{};
    if (!r->read_one(ret->shape_count, d0, d1, d3, ret->bbox_)) {
        return {};
    }

    // Call out to parse DOLm and resume parsing where it leaves off.
    {
        auto [dolm, tail] = poe::format::dolm::parse_dolm(r->tail());
        if (!dolm) {
            return {};
        }
        ret->dolm_ = std::move(*dolm);
        r = poe::util::make_stream_reader(tail, 0);
    }

    std::vector<std::array<uint32_t, 2>> shape_offsets(ret->shape_count);
    if (!r->read_many(shape_offsets.data(), shape_offsets.size())) {
        return {};
    }

    ret->d0s.resize(d0);
    for (auto &d : ret->d0s) {
        if (!r->read_one(d.unk_0h)) {
            return {};
        }
    }

    ret->d1s.resize(d1);
    for (auto &d : ret->d1s) {
        if (!r->read_one(d.unk_0h)) {
            return {};
        }
    }

    std::vector<D3Generic> d3s(d3);
    for (auto &d : d3s) {
        d.b.resize((size_t)D3Width::V7);
        if (!r->read_seq(d.b)) {
            return {};
        }
    }
    ret->d3s = d3s;

    uint32_t code_unit_count;
    if (!r->read_one(code_unit_count)) {
        return {};
    }

    std::vector<uint16_t> text_mass(code_unit_count);
    if (!r->read_many(text_mass.data(), text_mass.size())) {
        return {};
    }

    for (auto offsets : shape_offsets) {
        ShapeStrings strings;
        if (!extract_utf16le_string(text_mass, offsets[0], strings.name) ||
            !extract_utf16le_string(text_mass, offsets[1], strings.material)) {
            return {};
        }
        ret->shape_strings_.push_back(strings);
    }

    char eofTest;
    if (r->read_one(eofTest)) {
        return {};
    }

    return ret;
}

std::shared_ptr<FMT> parse_fmt(std::span<std::byte const> data) {
    if ((uint8_t)data[0] < 9) {
        if (auto legacy = parse_fmt_v6(data)) {
            return into_fmt_v9(legacy);
        }
        return nullptr;
    } else {
        return parse_fmt_v9(data);
    }
}
} // namespace poe::format::fmt