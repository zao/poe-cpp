#pragma once

#include <nlohmann/json.hpp>

#include <memory>
#include <optional>
#include <span>

namespace poe::format::dat {
enum field_indirection {
    Ref,
    RefList,
};
enum field_type {
    Bool,
    F32,
    F64,
    I8,
    I16,
    I32,
    I64,
    String,
    U8,
    U16,
    U32,
    U64,
};

struct field_def {
    std::string label_;
    std::vector<field_indirection> indirection_;
    field_type type_;
};

struct dat_spec {
    dat_spec(field_def const *defs, size_t n);

    std::optional<size_t> find_field(std::string_view name) const;
    size_t field_count() const { return defs_.size(); }

    std::vector<field_def> defs_;
    std::vector<size_t> field_widths_;
    std::vector<size_t> field_offsets_;
    size_t byte_width_ = 0;
};

struct dat_table {
    using value = nlohmann::json;

    value get(size_t row, size_t col) const;
    std::optional<size_t> find_field(std::string_view name) const;

    std::string_view field_name(size_t i) const;
    size_t field_count() const;
    size_t row_count() const;

  private:
    value read_field(field_def def, uint64_t offset) const;
    size_t data_size() const { return storage_.size() - data_start_; }
    friend std::shared_ptr<dat_table> open_dat(std::byte const *p, size_t n, std::string_view filename);
    dat_spec const *spec_;

    size_t rows_;
    size_t row_stride_;
    std::vector<std::byte> storage_;
    uint64_t data_start_;
};

std::shared_ptr<dat_table> open_dat(std::byte const *p, size_t n, std::string_view filename);
inline std::shared_ptr<dat_table> open_dat(std::span<std::byte const> data, std::string_view filename) {
    return open_dat(data.data(), data.size(), filename);
}

struct dat_skeleton {
    using row = std::vector<std::byte>;

    std::vector<row> rows_;
    std::vector<std::byte> var_data_;
};

std::shared_ptr<dat_skeleton> open_skeleton(std::byte const *p, size_t n);
inline std::shared_ptr<dat_skeleton> open_skeleton(std::span<std::byte const> data) {
    return open_skeleton(data.data(), data.size());
}

void preload_specs();

} // namespace poe::format::dat