#pragma once

#include <poe/io/vfs.hpp>

#include <map>
#include <memory>
#include <span>
#include <string>
#include <variant>

namespace poe::format::ffx {
struct IncludeDeclaration {
    std::string target_;
};

struct UniformDeclaration {
    std::string scope_;
    std::string type_;
    std::string name_;
    std::optional<std::string> arrayBounds_;
    std::optional<std::string> comment_;
};

struct DefineDeclaration {
    std::string name_;
    std::string value_;
};

struct Declarations {
    explicit Declarations(std::string_view name) : name_(name) {}

    std::string name_;

    using PreludeItem = std::variant<IncludeDeclaration, UniformDeclaration, DefineDeclaration>;
    std::vector<PreludeItem> declarations_;
    std::string code_;
};
struct CostDeclaration {
    std::string cost;
};

struct DeclUniformDeclaration {
    std::string type;
    std::string name;
    struct UiPart {
        std::string label;
        std::string rangeMin;
        std::string rangeMax;
        std::string rangeDefault;
    };
    std::optional<UiPart> ui;
    std::string semantic;
};

struct KeyValueDeclaration {
    std::string key;
    std::string value;
};


struct KeywordDeclaration {
    std::string keyword;
};
struct MacroDeclaration {
    std::string name;
};

struct PreprocessorBranchDeclaration {
    std::optional<std::string> condition; // if present it's an #ifdef, if not it's an #endif
};

struct VariableDeclaration {
    std::string kind;
    std::string type;
    std::string name;
    std::string semantic;
};

struct Fragment {
    explicit Fragment(std::string_view name, std::span<std::string> annotations)
        : name_(name), annotations_(annotations.begin(), annotations.end()) {}

    std::string name_;
    std::vector<std::string> annotations_;
    using PreludeItem =
        std::variant<CostDeclaration, DeclUniformDeclaration, IncludeDeclaration, KeyValueDeclaration,
                     KeywordDeclaration, MacroDeclaration, PreprocessorBranchDeclaration, VariableDeclaration>;
    std::vector<PreludeItem> declarations_;
    std::vector<std::string> prelude_;
    std::string code_;

    bool HasKeyword(std::string_view keyword) const;
};

struct ParsedFfx {
    Declarations const *FindDeclarations(std::string_view name) const;
    Fragment const *FindFragment(std::string_view name) const;

    std::map<std::string, Declarations, std::less<>> declarationsList_;
    std::map<std::string, Fragment, std::less<>> fragmentList_;
};

std::shared_ptr<ParsedFfx> parse_ffx(std::span<std::byte const> data);
} // namespace poe::format::ffx