#include <poe/format/smd.hpp>
#define _AMD64_

#include <fmt/format.h>

#include <loguru/loguru.hpp>

#include <poe/util/parse_info.hpp>
#include <poe/util/parse_ops.hpp>
#include <poe/util/random_access_file.hpp>
#include <poe/util/utf.hpp>

#include <debugapi.h>

namespace poe::format::smd {
struct MeshExtent {
    uint32_t triangleCount, vertexCount;
};

#define SMD_BAIL(msg)                                                                                                  \
    LOG_F(WARNING, "SMD: {}", (msg));                                                                                  \
    if (IsDebuggerPresent()) {                                                                                         \
        /*DebugBreak();*/                                                                                                  \
    }                                                                                                                  \
    return {}

std::shared_ptr<SmdFile> parse_smd(std::span<std::byte const> data) {
    std::shared_ptr<SmdFile> ret = std::make_shared<SmdFile>();
    auto r = poe::util::make_stream_reader(data, 0);

    uint8_t version;
    if (!r->read_one(version)) {
        SMD_BAIL("Could not read version");
    }
    ret->version = version;

    if (ret->version > 3) {
        SMD_BAIL("Unhandled version");
    }

    if (ret->version <= 2) {
        uint32_t triangleCount, vertexCount;
        uint8_t c04;
        uint16_t shapeCount;
        uint32_t totalShapeNameByteCount;

        if (!r->read_one(triangleCount, vertexCount, c04, shapeCount, totalShapeNameByteCount, ret->bbox)) {
            SMD_BAIL("Could not read header");
        }

        uint32_t c04_2{4};
        if (version == 2 && !r->read_one(c04_2)) {
            SMD_BAIL("Could not read post-header value");
        }

        struct ShapeInfo {
            uint32_t nameByteCount, triangleOffset;
        };

        std::vector<ShapeInfo> shapes(shapeCount);
        if (!r->read_many(shapes.data(), shapes.size())) {
            SMD_BAIL("Could not read shape information");
        }

        std::vector<std::byte> nameData(totalShapeNameByteCount);
        if (!r->read_many(nameData.data(), nameData.size())) {
            SMD_BAIL("Could not read name string data");
        }

        ret->shapes.reserve(shapeCount);
        ret->geom = std::make_shared<dolm::DolmGeometry>();
        auto &geom = *ret->geom;

        geom.shapeCount = shapeCount;
        VertexComponent const commonFormat =
            (VertexComponent)(VertexComponent::VtxPosition_3f32 | VertexComponent::VtxNormal_4i8 |
                              VertexComponent::VtxTangent_4i8 | VertexComponent::VtxTexcoord0_2f16 |
                              VertexComponent::VtxSkinBones_4u8 | VertexComponent::VtxSkinWeights_4u8);
        if (c04_2 == 4) {
            geom.format = commonFormat;
        } else if (c04_2 == 5) {
            geom.format = (VertexComponent)(commonFormat | VertexComponent::VtxTexcoord1_2f16);
        } else if (c04_2 == 6) {
            geom.format = (VertexComponent)(commonFormat | VertexComponent::VtxSkinExtra_4u8);
        } else {
            SMD_BAIL("Unknown v2 vertex format");
        }
        geom.legacyNormals = true;

        auto mesh = std::make_shared<dolm::DolmMesh>();
        geom.meshes.push_back(mesh);

        mesh->triangleCount = triangleCount;
        mesh->vertexCount = vertexCount;

        {
            std::span<std::byte const> nameSource(nameData);
            for (auto &shape : shapes) {
                if (shape.nameByteCount > nameSource.size()) {
                    SMD_BAIL("Name out of bounds");
                }
                auto prefix = nameSource.subspan(0, shape.nameByteCount);
                nameSource = nameSource.subspan(shape.nameByteCount);
                auto wideText =
                    std::u16string_view((char16_t const *)prefix.data(), prefix.size() / 2); // assume aligned data
                ret->shapes.push_back({
                    .name = poe::util::to_string(wideText),
                });
                mesh->shapeExtents.push_back({.indexStart = shape.triangleOffset * 3});
            }

            uint32_t lastIndexEnd = triangleCount * 3;
            for (int i = (int)shapes.size() - 1; i >= 0; --i) {
                auto &shape = mesh->shapeExtents[i];

                shape.indexCount = lastIndexEnd - shape.indexStart;
                lastIndexEnd = shape.indexStart;
            }
        }

        if (vertexCount > 0xFFFF) {
            mesh->indexData.resize(triangleCount * 3 * sizeof(uint32_t));
        } else {
            mesh->indexData.resize(triangleCount * 3 * sizeof(uint16_t));
        }
        if (!r->read_seq(mesh->indexData)) {
            SMD_BAIL("Could not read index data");
        }

        auto vertexStride = VertexWidth(geom.format);
        mesh->vertexData.resize(vertexCount * vertexStride);
        if (!r->read_seq(mesh->vertexData)) {
            SMD_BAIL("Could not read vertex data");
        }

        UnbiasLegacyVertexData(geom);
    } else if (ret->version == 3) {
        uint8_t c04;
        uint16_t shapeNameCount;
        uint32_t totalShapeNameByteCount;
        if (!r->read_one(c04, shapeNameCount, totalShapeNameByteCount)) {
            SMD_BAIL("Could not read header");
        }

        if (!r->read_one(ret->bbox)) {
            SMD_BAIL("Could not read bounding box");
        }

        auto [geom, tail] = dolm::parse_dolm(data.subspan(r->cursor()));
        if (!geom) {
            SMD_BAIL("Could not read mLOD");
        }
        ret->geom = geom;
        r = poe::util::make_stream_reader(tail, 0);

        std::vector<uint32_t> shapeNameByteCounts(shapeNameCount);
        if (!r->read_seq(shapeNameByteCounts)) {
            SMD_BAIL("Could not read shape name byte counts");
        }

        std::vector<std::byte> nameData(totalShapeNameByteCount);
        if (!r->read_seq(nameData)) {
            SMD_BAIL("Could not read name table data");
        }

        std::span<std::byte const> nameSource(nameData);
        for (auto &nameByteCount : shapeNameByteCounts) {
            if (nameByteCount > nameSource.size()) {
                SMD_BAIL("Name out of bounds");
            }
            auto prefix = nameSource.subspan(0, nameByteCount);
            nameSource = nameSource.subspan(nameByteCount);
            auto wideText =
                std::u16string_view((char16_t const *)prefix.data(), prefix.size() / 2); // assume aligned data
            ret->shapes.push_back({
                .name = poe::util::to_string(wideText),
            });
        }
    }

    // Tail common to SMD v1-v3, may be optional by omission in v1
    uint32_t tailVersion{};
    if (!r->read_one(tailVersion)) {
        if (version != 1) {
            SMD_BAIL("Could not read tail version");
        }
    }
    ret->tailVersion = tailVersion;

    struct RawT0 {
        std::array<float, 15> f0;
        uint32_t u60h;
    };

    struct RawT1 {
        glm::vec3 pos;
        float radius;
        uint32_t u16h;
    };

    auto const ReadName = [&](auto &out) -> bool {
        uint32_t byteCount;
        if (!r->read_one(byteCount)) {
            return false;
        }
        std::vector<char16_t> buf(byteCount / 2);
        if (!r->read_seq(buf)) {
            return false;
        }
        out = poe::util::to_string(std::u16string_view(buf.data(), buf.size()));
        return true;
    };

    auto const ReadT0s = [&](auto &arr) -> bool {
        RawT0 rt0;
        for (int idx = 0; idx < arr.size(); ++idx) {
            if (!r->read_one(rt0)) {
                return false;
            }
            auto &out = arr[idx];
            out.f0 = rt0.f0;
            out.u60h = rt0.u60h;
            if (tailVersion > 2) {
                if (!ReadName(out.name)) {
                    return false;
                }
            }
        }
        return true;
    };

    auto const ReadT1s = [&](auto &arr) -> bool {
        RawT1 rt1;
        for (int idx = 0; idx < arr.size(); ++idx) {
            if (!r->read_one(rt1)) {
                return false;
            }
            auto &out = arr[idx];
            out.pos = rt1.pos;
            out.radius = rt1.radius;
            out.u16h = rt1.u16h;
            if (tailVersion > 2) {
                if (!ReadName(out.name)) {
                    return false;
                }
            }
        }
        return true;
    };

    if (tailVersion == 2) {
        std::array<uint32_t, 4> tailCounts;
        if (!r->read_one(tailCounts)) {
            SMD_BAIL("Could not read tail counts");
        }
        ret->t4s.resize(tailCounts[1]);
        ret->t5s.resize(tailCounts[2]);
        ret->t0Ellipsoids.resize(tailCounts[0]);
        ret->t6s.resize(tailCounts[3]);
        if (!r->read_seq(ret->t4s, ret->t5s) || !ReadT0s(ret->t0Ellipsoids) || !r->read_seq(ret->t6s)) {
            SMD_BAIL("Could not read T4, T5, T0, T6 sections");
        }
    } else if (tailVersion == 3) {
        std::array<uint32_t, 7> tailCounts;
        if (!r->read_one(tailCounts)) {
            SMD_BAIL("Could not read tail counts");
        }
        ret->t0Ellipsoids.resize(tailCounts[0]);
        ret->t1Spheres.resize(tailCounts[1]);
        ret->t2s.resize(tailCounts[2]);
        ret->t3s.resize(tailCounts[3]);
        ret->t4s.resize(tailCounts[4]);
        ret->t5s.resize(tailCounts[5]);
        ret->t6s.resize(tailCounts[6]);
        if (!ReadT0s(ret->t0Ellipsoids) || !ReadT1s(ret->t1Spheres) ||
            !r->read_seq(ret->t2s, ret->t3s, ret->t4s, ret->t5s, ret->t6s)) {
            SMD_BAIL("Could not read T0-T6 sections");
        }
    } else if (tailVersion == 4) {
        std::array<uint32_t, 7> tailCounts;
        if (!r->read_one(tailCounts)) {
            SMD_BAIL("Could not read tail counts");
        }
        ret->t4s.resize(tailCounts[4]);
        ret->t5s.resize(tailCounts[5]);
        ret->t0Ellipsoids.resize(tailCounts[0]);
        ret->t1Spheres.resize(tailCounts[1]);
        ret->t2s.resize(tailCounts[2]);
        ret->t6s.resize(tailCounts[6]);
        if (!r->read_seq(ret->t4s, ret->t5s) || !ReadT0s(ret->t0Ellipsoids) || !ReadT1s(ret->t1Spheres) ||
            !r->read_seq(ret->t2s, ret->t6s)) {
            SMD_BAIL("Could not read T4, T5, T0, T1, T2, T6 sections");
        }
    } else if (tailVersion != 0) {
        SMD_BAIL("Unhandled tail version");
    }

    // Check if we've fully consumed the file
    uint8_t endTest;
    if (r->read_one(endTest)) {
        SMD_BAIL("Trailing file data after parsing");
    }

    return ret;
}
} // namespace poe::format::smd