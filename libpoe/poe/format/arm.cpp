#include <poe/format/arm.hpp>

#include <poe/util/parse_info.hpp>
#include <poe/util/parse_ops.hpp>
#include <poe/util/utf.hpp>

#include <fmt/format.h>

#include <loguru/loguru.hpp>

#include <algorithm>
#include <iostream>
#include <optional>
#include <regex>
#include <string>
#include <variant>

namespace poe::format::arm {
std::optional<Entry> read_k_entry(poe::util::ParseOps &po, size_t version) {
    Entry entry;
    std::string kind;
    if (auto err = po.reset().string(kind).err()) {
        LOG_F(WARNING, "K kind error: {}", *err);
        return {};
    }
    if (kind == "f") {
        FEntry e;
        if (auto err = po.reset().spaces().integer(e.value_).err()) {
            LOG_F(WARNING, "K-F value error: {}", *err);
            return {};
        }
        entry = {e};
    } else if (kind == "k") {
        KEntry e;
        if (auto err = po.reset().spaces().integer(e.w_).spaces().integer(e.h_).err()) {
            LOG_F(WARNING, "K-K leading error: {}", *err);
            return {};
        }
        size_t const tail_count = 21;
        e.tail_.resize(tail_count);
        for (size_t i = 0; i < tail_count; ++i) {
            if (auto err = po.reset().spaces().integer(e.tail_[i]).err()) {
                LOG_F(WARNING, "K-K tail {} error: {}", i, *err);
                return {};
            }
        }
        if (version >= 19) {
            if (auto err = po.reset().spaces().integer(e.last_).err()) {
                LOG_F(WARNING, "K-K last error: {}", *err);
                return {};
            }
        }
        entry = {e};
    } else if (kind == "n") {
        entry = {NEntry{}};
    } else if (kind == "s") {
        entry = {SEntry{}};
    } else if (kind == "o") {
        entry = {OEntry{}};
    } else {
        LOG_F(WARNING, "K1x1 unknown kind {}", kind);
        return {};
    }
    return entry;
}

std::shared_ptr<ARMFile> parse_arm(std::span<std::byte const> data) {
    auto wide_sv = poe::util::to_u16string_view(data);
    if (!wide_sv) {
        return {};
    }
    auto input_text = poe::util::to_string(*wide_sv);
    auto view = std::string_view(input_text);

    auto ret = std::make_shared<ARMFile>();
    poe::util::ParseOps po(view);

    // Parse version
    if (auto err = po.reset().string_lit("version").spaces().integer(ret->version_).eol().err()) {
        LOG_F(WARNING, "version line error: {}", *err);
        return {};
    }

    // Parse string table
    size_t string_count;
    if (auto err = po.reset().integer(string_count).eol().err()) {
        LOG_F(WARNING, "String count error: {}", *err);
        return {};
    }
    for (size_t string_idx = 0; string_idx < string_count; ++string_idx) {
        std::string s;
        if (auto err = po.reset().quoted_string(s).eol().err()) {
            LOG_F(WARNING, "String line error: {}", *err);
            return {};
        }
        ret->string_table_.push_back(std::move(s));
    }

    // Parse dims
    if (auto err = po.reset().integer(ret->dims_xy_[0]).spaces().integer(ret->dims_xy_[1]).err()) {
        LOG_F(WARNING, "Dimensions X/Y error: {}", *err);
        return {};
    }
    if (!po.is_next_newline()) {
        if (auto err = po.reset().spaces().integer(ret->dims_z_).eol().err()) {
            LOG_F(WARNING, "Dimensions Z error: {}", *err);
            return {};
        }
    } else {
        po.eol();
    }

    // Parse a_upair
    if (auto err = po.reset().integer(ret->a_upair_.first).spaces().integer(ret->a_upair_.second).eol().err()) {
        LOG_F(WARNING, "a_upair error: {}", *err);
        return {};
    }

    // Parse name
    if (auto err = po.reset().quoted_string(ret->name_).eol().err()) {
        LOG_F(WARNING, "name error: {}", *err);
        return {};
    }

    // Parse b_upair
    if (po.is_next_newline()) {
        po.reset().eol();
    }
    if (ret->version_ >= 16) {
        if (auto err = po.reset().integer(ret->b_upair_.first).spaces().integer(ret->b_upair_.second).eol().err()) {
            LOG_F(WARNING, "b_upair error: {}", *err);
            return {};
        }
    } else {
        if (auto err = po.reset().integer(ret->b_upair_.first).eol().err()) {
            LOG_F(WARNING, "b_upair error: {}", *err);
            return {};
        }
    }

    // Parse 1-by-1 K-entry
    glm::ivec2 gridSize{1, 1};
    {
        auto entry = read_k_entry(po, ret->version_);
        if (!entry) {
            LOG_F(WARNING, "K entry error");
            return {};
        }
        if (auto err = po.reset().eol().err()) {
            LOG_F(WARNING, "K unexpected trailing data error: {}", *err);
            return {};
        }
        ret->k_1_by_1_ = *entry;
        if (auto *k = std::get_if<KEntry>(&entry->var)) {
            gridSize = {k->w_, k->h_};
        }
    }

    // Parse c_uious list
    {
        size_t const n = 2 * (ret->a_upair_.first + ret->a_upair_.second);
        ret->c_uious_.resize(n);
        for (size_t i = 0; i < n; ++i) {
            auto &seq = ret->c_uious_[i];
            if (auto err = po.reset().integer(seq.a).spaces().integer(seq.b).err()) {
                LOG_F(WARNING, "C part 1 error: {}", *err);
                return {};
            }
            if (po.is_next_newline()) {
                po.eol();
            } else {
                if (auto err = po.reset().spaces().integer(seq.c).err()) {
                    LOG_F(WARNING, "C part 2 error: {}", *err);
                    return {};
                }
                if (po.is_next_newline()) {
                    po.eol();
                } else {
                    if (auto err = po.reset().spaces().integer(seq.d).spaces().integer(seq.e).eol().err()) {
                        LOG_F(WARNING, "C part 3 error: {}", *err);
                        return {};
                    }
                }
            }
        }
    }

    // Parse D-entries
    {
        auto ver = ret->version_;
        size_t const d_kinds = ver >= 29 ? 6 : ver >= 26 ? 5 : ver >= 20 ? 10 : 9;
        ret->dentries_.resize(d_kinds);
        for (size_t kind = 0; kind < d_kinds; ++kind) {
            size_t d_count;
            if (auto err = po.reset().integer(d_count).eol().err()) {
                LOG_F(WARNING, "D count error: {}", *err);
                return {};
            }

            auto ds = ret->dentries_[kind];
            ds.resize(d_count);
            for (size_t i = 0; i < d_count; ++i) {
                auto &d = ds[i];
                if (auto err = po.reset()
                                   .integer(d.x_)
                                   .spaces()
                                   .integer(d.y_)
                                   .spaces()
                                   .real(d.h_)
                                   .spaces()
                                   .quoted_string(d.key_)
                                   .eol()
                                   .err()) {
                    LOG_F(WARNING, "D entry error: {}", *err);
                    return {};
                }
            }
        }
    }

    // Parse X-by-Y K-entries
    {
        auto k_width = gridSize.x;
        auto k_height = gridSize.y;
        ret->k_x_by_y_.resize(k_height);
        for (size_t k_row = 0; k_row < k_height; ++k_row) {
            for (size_t k_col = 0; k_col < k_width; ++k_col) {
                auto entry = read_k_entry(po, ret->version_);
                if (!entry) {
                    LOG_F(WARNING, "K entry error");
                    return {};
                }
                ret->k_x_by_y_[k_row].push_back(*entry);

                if (k_col + 1 < k_width) {
                    if (auto err = po.reset().spaces().err()) {
                        LOG_F(WARNING, "K unexpected lack of spaces: {}", *err);
                        return {};
                    }
                }
            }
            if (auto err = po.reset().eol().err()) {
                LOG_F(WARNING, "K unexpected trailing data error: {}", *err);
                return {};
            }
        }
    }

    // TODO(LV): Parse whole tail, should be split up into actual things in the future

    return ret;
}

glm::ivec2 ARMFile::Size() const {
    if (auto *k = std::get_if<KEntry>(&k_1_by_1_.var)) {
        return {k->w_, k->h_};
    }
    return {1, 1};
}

std::optional<std::string> ARMFile::TableString(int64_t index) const {
    if (index <= 0 || index > (int64_t)string_table_.size()) {
        return {};
    }
    return string_table_[index - 1];
}
} // namespace poe::format::arm