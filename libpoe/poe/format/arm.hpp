#pragma once

#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <utility>
#include <variant>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <gsl/span>

namespace poe::format::arm {
struct FEntry {
    size_t value_;
};

enum class KSlot {
    EdgeStr = 0,
    EdgeIdx = 4, // Interleaved edge indices [real0, virt0, real1, virt1, ..] where the even slots are only used for virtual edges
    GroundStr = 12,
    Height = 16,
    Tag = 20,
};

struct KEntry {
    size_t w_, h_;
    std::vector<int64_t> tail_;
    std::optional<int64_t> last_;
};

struct NEntry {};
struct OEntry {};
struct SEntry {};

struct Entry {
    using EntryVariant = std::variant<NEntry, SEntry, OEntry, FEntry, KEntry>;
    EntryVariant var;
};

struct DEntry {
    size_t x_, y_;
    float h_;
    std::string key_;
};

enum class DKey {
    MobPack,
    Chest,
    Trigger,
};

struct Doodad {
    size_t x_, y_;
    float h_;
    std::optional<glm::quat> r_;
    size_t q1_;
    std::optional<size_t> q1_5_;
    std::vector<float> q2s_;
    float q3_;
    std::string ao_;
    std::string spec_;
};

struct Decal {
    float x_, y_;
    float h_;
    size_t q1_;
    float s_;
    std::string atlas_;
    std::string key_;
};

struct CUIOU {
    int64_t a, b;
    std::optional<int64_t> c;
    std::optional<int64_t> d, e;
};

struct ARMFile {
    size_t version_;
    std::vector<std::string> string_table_;
    std::array<size_t, 2> dims_xy_;
    std::optional<size_t> dims_z_;
    std::pair<size_t, size_t> a_upair_;
    std::string name_;
    std::pair<size_t, std::optional<size_t>> b_upair_;
    Entry k_1_by_1_;
    std::vector<CUIOU> c_uious_;
    std::vector<std::vector<DEntry>> dentries_;
    std::vector<std::vector<Entry>> k_x_by_y_;
    std::vector<std::string> whole_tail_;
    // std::vector<Doodad> doodads_;
    // std::vector<Decal> decals_;
    // std::vector<Decal> more_decals_;
    // std::optional<size_t> e2_usize_;
    // std::optional<std::string> tail_line_;

    glm::ivec2 Size() const;
    std::optional<std::string> TableString(int64_t index) const; // 1-indexed
};

std::shared_ptr<ARMFile> parse_arm(std::span<std::byte const> data);
} // namespace poe::format::arm