#pragma once

#include <gsl/span>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include <tbb/scalable_allocator.h>

#include <array>
#include <memory>
#include <optional>
#include <span>
#include <vector>

#include <poe/format/dolm.hpp>

namespace poe::format::smd {
using dolm::BBox;
using dolm::VertexComponent;
using dolm::VertexWidth;

struct SmdT0Ellipsoid {
    std::array<float, 15> f0;
    uint32_t u60h;
    std::string name;
};

struct SmdT1Sphere {
    glm::vec3 pos;
    float radius;
    uint32_t u16h;
    std::string name;
};

struct SmdTailT2 {
    uint32_t u0, u4;
};

struct SmdTailT3 {
    uint8_t unk; // never occurred in files, unknown size
};

struct SmdT4SkinnedVertex {
    float vec0[3];
    uint32_t index12[4];
    float weight28[4];
};

struct SmdTailT5 {
    uint32_t u0;
};

struct SmdTailT6 {
    uint32_t u0;
};

struct SmdShape {
    std::string name;
};

struct SmdFile {
    int version;
    BBox bbox;

    std::vector<SmdShape> shapes;
    std::shared_ptr<dolm::DolmGeometry> geom;

    #if LEGACY_SMD
    VertexComponent vertexFormat;
    int vertexCount, triangleCount;
    std::vector<std::byte> vertexData;
    std::vector<std::byte> indexData;
    #endif

    int tailVersion;
    std::vector<SmdT0Ellipsoid> t0Ellipsoids;
    std::vector<SmdT1Sphere> t1Spheres;
    std::vector<SmdTailT2> t2s; // references t1 bounding spheres in pairs
    std::vector<SmdTailT3> t3s; // never seen, unknown purpose
    std::vector<SmdT4SkinnedVertex> t4s;
    std::vector<SmdTailT5> t5s; // references t4 skinned vertices
    std::vector<SmdTailT6> t6s; // references t4 skinned vertices
};

std::shared_ptr<SmdFile> parse_smd(std::span<std::byte const> data);
} // namespace poe::format::smd