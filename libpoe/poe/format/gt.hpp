#pragma once

#include <memory>
#include <span>
#include <string>

namespace poe::format::gt {
struct GtFile {
    std::string id;
    int nums[6]{};
    std::string option;
};

std::shared_ptr<GtFile> parse_gt(std::span<std::byte const> data);
} // namespace poe::format::et