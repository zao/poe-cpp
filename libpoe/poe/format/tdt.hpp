#pragma once

#include <array>
#include <map>
#include <optional>
#include <span>
#include <string>
#include <vector>

#include <glm/vec2.hpp>

namespace poe::format::tdt {
constexpr const int tileSize = 23;
constexpr const int paddedSize = 31;

struct Tdt {
    uint32_t version;

    std::optional<std::string> unkLeadingRef; 
    std::optional<std::string> commonTgt;
    std::string tag;

    struct Header {
        std::array<std::optional<std::string>, 4> sideEts;
        glm::ivec2 dim;
        std::array<std::optional<std::string>, 4> sideGts;
        uint16_t zero1;
        glm::i8vec2 dim2{0, 0};
        std::array<uint8_t, 8> sideOffsets;
    } header;

    enum Flags : uint8_t {
        FlagsHasTailBytes = 0b1,
        FlagsHasGtGrid = 0b10,
        FlagsHasUnkBit2 = 0b100,
        FlagsHasUnkBit4 = 0b1'0000,
    };
    Flags flags{};

    using FixedBlock = std::array<uint8_t, tileSize * tileSize>;
    using DistBlock = std::array<uint8_t, paddedSize * paddedSize>;
    using VaryingBlock = std::vector<uint8_t>;

    struct SubTileV3 {
        uint32_t preFixed;
        FixedBlock fixed;
        uint32_t preDist;
        DistBlock dist;
    };
    std::optional<SubTileV3> subTileV3;

    struct SubTile {
        uint8_t type;
        uint8_t val;
        std::optional<FixedBlock> fixed;
        std::optional<VaryingBlock> varying;
        std::optional<std::string> tgt;
    };
    std::vector<SubTile> subTiles;

    template <typename T> using Grid = std::vector<std::vector<T>>;

    template <typename T> using GridRow = std::vector<T>;

    template <typename T> static Grid<T> makeGrid(size_t width, size_t height) {
        return Grid<T>(height, GridRow<T>(width, T{}));
    }

    Grid<std::optional<std::string>> gtGrid;
    std::vector<uint8_t> tailBytes;

    std::optional<std::string> tmd;

    struct TailV7 {
        std::optional<std::string> str0;
        uint8_t num1;
    };
    std::optional<TailV7> tailV7;

    std::optional<std::string> resolveString(uint32_t ref) const;

  private:
    friend std::optional<Tdt> parse_tdt(std::span<std::byte const> data);
    std::map<uint32_t, std::string> stringTable_;
};

std::optional<Tdt> parse_tdt(std::span<std::byte const> data);
} // namespace poe::format::tdt