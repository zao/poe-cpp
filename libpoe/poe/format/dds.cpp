#include <poe/format/dds.hpp>
#include <poe/util/reader.hpp>

#include "poe/util/random_access_file.hpp"

namespace poe::format::dds {
std::shared_ptr<proxy_texture> parse_dds_header(std::span<std::byte const> data) {
    auto r = poe::util::make_stream_reader(data.data(), data.size(), 0);
    auto ret = std::make_shared<proxy_texture>();

    uint32_t body_size{};
    if (!r->read_one(ret->version_) || !r->read_one(ret->full_width_) || !r->read_one(ret->full_height_) ||
        !r->read_one(body_size)) {
        return {};
    }

    std::vector<std::byte> body(body_size);
    if (!r->read_exact(body.data(), body.size())) {
        return {};
    }

    if (body.size() && body[0] == (std::byte)'*') {
        auto p = (char const*)body.data();
        ret->body_ = std::string(p + 1, p + body.size());
    } else {
        ret->body_ = std::move(body);
    }
    return ret;
}
} // namespace poe::format::dds