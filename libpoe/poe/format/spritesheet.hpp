#pragma once

#include <map>
#include <memory>
#include <string>
#include <string_view>
#include <vector>

#include <gsl/span>

namespace poe::format::spritesheet {
	struct sheet {
		struct entry {
			std::string id, path;
			uint16_t x0, y0, x1, y1;
		};

		std::vector<entry> entries_;
		std::map<std::string_view, size_t> by_id_;
	};

	std::shared_ptr<sheet> parse_spritesheet(std::span<std::byte const> data);
}