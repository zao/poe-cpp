#pragma once

#include <optional>
#include <span>
#include <absl/container/inlined_vector.h>
#include <glm/vec2.hpp>
#include <nlohmann/json.hpp>

namespace poe::format::fxgraph {
using Parameter = nlohmann::json; // shallow dict

struct FxGraphKey {
    std::string type;
    int index;
    std::optional<std::string> stage;

    auto operator<=>(FxGraphKey const &) const = default;
};

struct FxGraphNode {
    FxGraphKey key;
    glm::ivec2 uiPosition;
    std::optional<std::string> customParameter;
    std::vector<Parameter> parameters;
};

struct FxGraphLink {
    struct Connector {
        FxGraphKey key;
        std::string variable;
        std::optional<std::string> swizzle;
    };

    Connector src, dst;
};

struct FxGraph {
    int version;
    std::vector<FxGraphNode> nodes;
    std::vector<FxGraphLink> links;

    std::optional<std::string> overridenBlendMode;
    std::optional<std::string> overridenLightingModel;
    std::optional<bool> lightingDisabled;
};

std::shared_ptr<FxGraph> parse_fxgraph_json(nlohmann::json const &js);
} // namespace poe::format::fxgraph

namespace poe::format::mat {
struct MatTexture {
    struct Source {
        std::string filename;
    };
    std::string filename;
    std::string format;
    absl::InlinedVector<Source, 2> sources;
    int count;
};

struct CustomParameter {
    std::string name;
    std::vector<poe::format::fxgraph::Parameter> parameters;
};

struct MatGraphInstance {
    std::string parent;
    std::vector<CustomParameter> customParameters;
};

struct MatFileV4 {
    int version;
    std::vector<MatTexture> textures;
    std::shared_ptr<poe::format::fxgraph::FxGraph> defaultGraph;
    std::vector<MatGraphInstance> graphInstances;
};

std::shared_ptr<MatFileV4> parse_mat(std::span<std::byte const> data);
} // namespace poe::format::mat