#include <poe/util/path.hpp>

#include <poe/util/fnv1a.hpp>

namespace poe::util {
uint64_t file_path_hash(path const &p) { return file_path_hash(p.path_); }

uint64_t file_path_hash(std::string_view p) {
    auto hasher = poe::util::incremental_fnv1a64();
    for (auto ch : p) {
        auto b = (std::byte)std::tolower((unsigned char)ch);
        hasher.feed(&b, 1);
    }
    hasher.feed((std::byte const *)"++", 2);
    return hasher.finish();
}

uint64_t file_path_hash(std::u8string_view p) {
    return file_path_hash(std::string_view((char const *)p.data(), p.size()));
}
} // namespace poe::util