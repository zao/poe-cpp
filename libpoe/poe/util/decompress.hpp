#pragma once

#include <cstddef>
#include <optional>
#include <vector>

#include <gsl/span>

namespace poe::util {
std::optional<std::vector<std::byte>> decompress_oodle(void *data, size_t size, size_t dst_size);

inline std::optional<std::vector<std::byte>> decompress_oodle(std::span<uint8_t> src, size_t dst_size) {
    return decompress_oodle(src.data(), src.size(), dst_size);
}

inline std::optional<std::vector<std::byte>> decompress_oodle(std::span<std::byte> src, size_t dst_size) {
    return decompress_oodle(src.data(), src.size(), dst_size);
}
} // namespace poe::util