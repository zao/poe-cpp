#include <poe/util/parse_info.hpp>

#include <map>
#include <memory>
#include <mutex>
#include <string_view>

#include <loguru/loguru.hpp>

namespace poe::util {
static thread_local std::string current_parse_name;
static thread_local std::unique_ptr<parse_info> current_parse_info;

struct parse_info_service_impl : parse_info_service {
    mutable std::mutex mutex;
    std::map<std::string, std::shared_ptr<parse_info>> infos;

    mutable std::atomic<size_t> infos_changed = false;
    mutable std::mutex mirror_mutex;
    mutable std::map<std::string, std::shared_ptr<parse_info>> infos_mirror;

    void begin_parse(std::string_view name) override {
        if (current_parse_info) {
            abort();
        }
        current_parse_name = std::string(name);
        current_parse_info = std::make_unique<parse_info>();
    }

    parse_info *current_parse() override { return current_parse_info.get(); }

    void end_parse() override {
        auto lk = std::lock_guard(mutex);
        infos_changed = true;
        infos[current_parse_name] = std::shared_ptr(std::move(current_parse_info));
    }

    std::map<std::string, std::shared_ptr<parse_info>> get_infos() const override {
        if (infos_changed) {
            auto lk = std::lock_guard(mutex);
            auto lk2 = std::lock_guard(mirror_mutex);
            infos_mirror = infos;
            infos_changed = false;
            return infos_mirror;
        }
        auto lk2 = std::lock_guard(mirror_mutex);
        return infos_mirror;
    }
};

parse_info_service &get_parse_info_service() {
    static parse_info_service_impl s;
    return s;
}

parse_info::parse_info() {}

void parse_info::at_stage(std::string_view stage) {
    data_["stages"] += nlohmann::json::object({{"name", std::string(stage)}});
}

void parse_info::record_fault(std::string_view fault) {
    data_["faults"] += fault;
    LOG_F(INFO, "{}", fault);
}

} // namespace poe::util