#pragma once

#include <nlohmann/json.hpp>

namespace poe::util {
struct parse_info {
    parse_info();

    void at_stage(std::string_view stage);
    void record_fault(std::string_view fault);
    nlohmann::json data_;
};

struct parse_info_service {
    virtual void begin_parse(std::string_view name) = 0;
    virtual parse_info *current_parse() = 0;
    virtual void end_parse() = 0;

    virtual std::map<std::string, std::shared_ptr<parse_info>> get_infos() const = 0;
};

parse_info_service &get_parse_info_service();
} // namespace poe::util