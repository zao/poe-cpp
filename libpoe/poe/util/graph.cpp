#include <poe/util/graph.hpp>

#include <algorithm>
#include <iterator>
#include <unordered_set>
#include <utility>

namespace poe::util {
std::optional<std::vector<size_t>> topologic_sort(std::span<std::pair<size_t, size_t>> edges) {
    // Using [Kahn's algorithm](https://en.wikipedia.org/wiki/Topological_sorting#Kahn%27s_algorithm)
    std::vector<size_t> sorted;   // L
    std::unordered_set<size_t> no_incoming; // S

    /* Measure vertex count */
    size_t max_vertex = 0;
    for (auto const &[src, dst] : edges) {
        max_vertex = (std::max)({max_vertex, src, dst});
    }
    size_t const vertex_count = max_vertex + 1;

    /* Build set of vertices without incoming edges */
    std::vector<std::unordered_set<size_t>> out_edges(vertex_count);
    std::vector<std::unordered_set<size_t>> in_edges(vertex_count);
    std::vector<bool> has_incoming(vertex_count, false);
    for (auto const &[src, dst] : edges) {
        out_edges[src].insert({dst});
        in_edges[dst].insert({src});
        has_incoming[dst] = true;
    }

    for (size_t i = 0; i < vertex_count; ++i) {
        if (!has_incoming[i]) {
            no_incoming.insert(i);
        }
    }

    /* Actual algorithm */
    while (!no_incoming.empty()) {
        size_t n = *no_incoming.begin();
        no_incoming.erase(no_incoming.begin());
        sorted.push_back(n);
        for (auto I = out_edges[n].begin(); I != out_edges[n].end();) {
            size_t m = *I;
            I = out_edges[n].erase(I);
            in_edges[m].erase(n);
            if (in_edges[m].empty()) {
                no_incoming.insert(m);
            }
        }
    }

    auto has_edges = [](auto const &assoc) {
        return std::any_of(assoc.begin(), assoc.end(), [](auto const &outs) { return !outs.empty(); });
    };
    if (has_edges(out_edges) || has_edges(in_edges)) {
        return {};
    }
    return sorted;
}
} // namespace poe::util