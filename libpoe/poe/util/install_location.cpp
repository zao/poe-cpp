#include <poe/util/install_location.hpp>

#include <poe/util/random_access_file.hpp>

#include <fmt/format.h>

#include <fstream>
#include <optional>
#include <regex>
#include <string>

#ifdef _WIN32
#include <Windows.h>
#include <Shlobj.h>
#endif

#ifdef __linux__
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>
#endif

namespace poe::util {
namespace {
#ifdef _WIN32
std::optional<std::filesystem::path> get_reg_path(wchar_t const *subkey, wchar_t const *value) {
    std::vector<char> buf;
    while (true) {
        DWORD path_cb = (DWORD)buf.size();
        void *p = buf.empty() ? nullptr : buf.data();
        if (ERROR_SUCCESS == RegGetValueW(HKEY_CURRENT_USER, subkey, value, RRF_RT_REG_SZ, nullptr, p, &path_cb)) {
            if (path_cb <= buf.size()) {
                return std::filesystem::path(reinterpret_cast<char16_t *>(buf.data()));
            }
            buf.resize(path_cb);
        } else {
            return {};
        }
    }
}
#endif

std::optional<std::filesystem::path> standalone_install_dir() {
    if (auto root = get_reg_path(LR"(SOFTWARE\GrindingGearGames\Path of Exile)", L"InstallLocation")) {
        if (exists(*root / "Content.ggpk")) {
            return root;
        }
    }
    return {};
}

std::optional<std::filesystem::path> steam_install_dir() {
    auto steam_root = get_reg_path(LR"(SOFTWARE\Valve\Steam)", L"SteamPath");
    if (!steam_root || !exists(*steam_root)) {
        return {};
    }

    auto check_for_poe_dir = [](auto steamapps) -> std::optional<std::filesystem::path> {
        uint32_t const poe_appid = 238960;
        std::string const manifest_filename = fmt::format("appmanifest_{}.acf", poe_appid);
        auto manifest_path = steamapps / manifest_filename;
        if (exists(manifest_path)) {
            auto poe_path = steamapps / "common/Path of Exile";
            if (exists(poe_path)) {
                return poe_path;
            }
        }
        return {};
    };

    auto steamapps_path = *steam_root / "steamapps";
    if (auto dir = check_for_poe_dir(steamapps_path)) {
        return dir;
    }

    auto lib_folders_path = steamapps_path / "libraryfolders.vdf";
    if (std::ifstream lib_folders_file(lib_folders_path); lib_folders_file) {
        enum class parse_step {
            Nothing,
            LibraryFoldersLiteral,
            LibraryFoldersOpenCurly,
            LibraryFoldersCloseCurly,
        };

        parse_step step = parse_step::Nothing;
        std::string line;
        while (std::getline(lib_folders_file, line)) {
            switch (step) {
            case parse_step::Nothing: {
                if (line == R"("LibraryFolders")") {
                    step = parse_step::LibraryFoldersLiteral;
                }
            } break;
            case parse_step::LibraryFoldersLiteral: {
                if (line == "{") {
                    step = parse_step::LibraryFoldersOpenCurly;
                }
            } break;
            case parse_step::LibraryFoldersOpenCurly: {
                if (line == "}") {
                    step = parse_step::LibraryFoldersCloseCurly;
                } else {
                    std::regex rx(R"!!(\s*"(\d+)"\s+"([^"]*)"\s*)!!");
                    std::smatch match;
                    if (regex_match(line, match, rx)) {
                        std::filesystem::path p = match.str(2);
                        if (auto dir = check_for_poe_dir(p / "steamapps")) {
                            return dir;
                        }
                    }
                }
            } break;
            case parse_step::LibraryFoldersCloseCurly: {
                return {};
            } break;
            }
        }
    }
    return {};
}
} // namespace

std::filesystem::path data_local_root() {
#ifdef _WIN32
    PWSTR os_path{};
    SHGetKnownFolderPath(FOLDERID_LocalAppData, KF_FLAG_DEFAULT, nullptr, &os_path);
    std::filesystem::path path(os_path);
    CoTaskMemFree(os_path);
    return path;
#else
    if (char const *data_home_path = getenv("XDG_DATA_HOME")) {
        return data_home_path;
    }
    if (char const *home_path = getenv("HOME")) {
        return std::filesystem::path(home_path) / ".local/share";
    }
    uid_t uid = getuid();
    struct passwd *pw = getpwuid(uid);
    return std::filesystem::path(pw->pw_dir) / ".local/share";
#endif
}

std::optional<std::filesystem::path> executable_dir() {
    std::vector<wchar_t> buf(1 << 20);
    GetModuleFileNameW(nullptr, buf.data(), static_cast<DWORD>(buf.size()));
    std::filesystem::path p = buf.data();
    if (p.has_parent_path()) {
        return p.parent_path();
    }
    return {};
}

std::optional<std::filesystem::path> own_install_dir() {
#ifdef _WIN32
    auto exe_dir = executable_dir();
    if (exe_dir) {
        if (exists(*exe_dir / "Content.ggpk")) {
            return exe_dir;
        }
    }
    return {};
#else
    return {}; // TODO(LV): Implement
#endif
}

std::vector<std::pair<install_kind, std::filesystem::path>> install_locations() {
    std::vector<std::pair<install_kind, std::optional<std::filesystem::path>>> candidate_dirs = {
        {
            install_kind::Steam,
            steam_install_dir(),
        },
        {
            install_kind::Standalone,
            standalone_install_dir(),
        },
        {
            install_kind::Bundled,
            own_install_dir(),
        },
    };

    std::vector<std::pair<install_kind, std::filesystem::path>> valid;
    for (auto [kind, cand] : candidate_dirs) {
        if (cand) {
            valid.push_back({kind, *cand});
        }
    }
    return valid;
}

auto SlurpFile(std::filesystem::path path) -> std::optional<std::vector<std::byte>> {
    try {
        poe::util::random_access_file fh(path);
        std::vector<std::byte> ret(fh.size());
        fh.read_exact(0, ret.data(), ret.size());
        return ret;
    } catch (std::runtime_error &) {
        return {};
    }
}

std::filesystem::path AssetPath(std::string_view filename) { return poe::util::executable_dir().value() / filename; }

auto SlurpAsset(std::string_view filename) -> std::optional<std::vector<std::byte>> {
    return SlurpFile(AssetPath(filename));
}

ReloadableFile::ReloadableFile(std::filesystem::path path) : path_(path) {}

bool ReloadableFile::Updated() {
    if (path_.empty()) {
        return false;
    }
    try {
        auto curWriteTime = std::filesystem::last_write_time(path_);
        if (lastWrite_ < curWriteTime) {
            lastWrite_ = curWriteTime;
            return true;
        }
    } catch (std::filesystem::filesystem_error) {
    }
    return false;
}

std::optional<std::vector<std::byte>> ReloadableFile::Slurp() { return SlurpFile(path_); }
} // namespace poe::util