#pragma once

#include <array>
#include <cstdint>
#include <cstring>
#include <filesystem>
#include <optional>
#include <span>
#include <vector>

namespace poe::util {
class random_access_file {
  public:
    explicit random_access_file(std::filesystem::path path);
    ~random_access_file();

    random_access_file(random_access_file &) = delete;
    random_access_file &operator=(random_access_file &) = delete;

    uint64_t size() const;
    bool read_exact(uint64_t offset, std::byte *p, uint64_t n) const;
    uint64_t read_some(uint64_t offset, std::byte *p, uint64_t n) const;

  private:
    uintptr_t os_handle_;
    std::optional<uint64_t> cached_size_;
};

uint64_t read_some(std::byte const *src_data, uint64_t src_size, uint64_t offset, std::byte *p, size_t n);

struct stream_reader {
    stream_reader(std::byte const *p, size_t n, uint64_t offset);

    bool read_exact(std::byte *p, size_t n);

    template <typename T> bool read_one(T &t) {
        constexpr size_t N = sizeof(T);
        std::array<std::byte, N> buf;
        if (!read_exact(buf.data(), buf.size())) {
            return false;
        }
        memcpy(&t, buf.data(), buf.size());

        return true;
    }

    template <typename T> bool read_one(std::optional<T> &t) {
        T tmp;
        if (read_one(tmp)) {
            t = tmp;
            return true;
        }
        return false;
    }

    template <typename T, typename... Tail> bool read_one(T &t, Tail &...tail) {
        return read_one(t) && read_one(tail...);
    }

    template <typename T> bool read_many(T *t, size_t n) {
        if (!n) {
            return true;
        }
        size_t const N = sizeof(T) * n;
        if (!read_exact(reinterpret_cast<std::byte *>(t), N)) {
            return false;
        }

        return true;
    }

    template <typename C> bool read_seq(C &cont) { return read_many(std::data(cont), std::size(cont)); }

    template <typename C, typename... Tail> bool read_seq(C &cont, Tail &...tail) {
        return read_seq(cont) && read_seq(tail...);
    }

    bool read_terminated_u16string(std::u16string &s);

    bool read_terminated_u16string(std::u16string &s, size_t cch_including_terminator);

    bool skip(uint64_t n);

    uint64_t cursor() const;
    std::span<std::byte const> tail() const;

  private:
    std::byte const *data_;
    uint64_t size_;
    uint64_t cursor_;
    std::vector<std::byte> buffer_;
    uint64_t buffer_valid_begin_;
    uint64_t buffer_valid_end_;
    uint64_t buffer_cursor_;
};

std::unique_ptr<stream_reader> make_stream_reader(std::byte const *p, size_t n, uint64_t offset);

std::unique_ptr<stream_reader> make_stream_reader(std::span<std::byte const> data, uint64_t offset);
} // namespace poe::util