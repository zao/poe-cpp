#include <poe/util/utf.hpp>

#include <string_view>
#include <vector>

#ifdef _WIN32
#include <Windows.h>
#else
#include <unicase.h>
#include <unistr.h>
#endif

namespace poe::util {
std::strong_ordering compare_without_case(std::u16string_view a, std::u16string_view b) {
#ifdef _WIN32
    switch (CompareStringEx(LOCALE_NAME_INVARIANT, LINGUISTIC_IGNORECASE, reinterpret_cast<LPCWCH>(a.data()),
                            static_cast<int>(a.size()), reinterpret_cast<LPCWCH>(b.data()), static_cast<int>(b.size()),
                            nullptr, nullptr, 0)) {
    case CSTR_LESS_THAN:
        return std::strong_ordering::less;
    case CSTR_GREATER_THAN:
        return std::strong_ordering::greater;
    default: // zero means error, but we assume here that strings are valid
        return std::strong_ordering::equal;
    }
#else
    return std::strong_ordering::equal; // TODO(LV): implement if needed
#endif
}

std::strong_ordering compare_without_case(std::u16string const &a, std::u16string const &b) {
    return compare_without_case(std::u16string_view(a), std::u16string_view(b));
}

std::strong_ordering compare_without_case(std::u16string_view a, std::u16string const &b) {
    return compare_without_case(a, std::u16string_view(b));
}

std::strong_ordering compare_without_case(std::u16string const &a, std::u16string_view b) {
    return compare_without_case(std::u16string_view(a), b);
}

std::u8string lowercase(std::u8string_view s) { return to_u8string(lowercase(to_u16string(s))); }
std::string lowercase(std::string_view s) { return to_string(lowercase(to_u16string(s))); }

std::u16string lowercase(std::u16string_view s) {
    if (s.empty()) {
        return std::u16string(s);
    }
#ifdef _WIN32
    auto src = reinterpret_cast<LPCWSTR>(s.data());
    auto src_cch = static_cast<int>(s.size());
    auto dst_cch = LCMapStringEx(LOCALE_NAME_INVARIANT, LCMAP_LOWERCASE, src, src_cch, nullptr, 0, nullptr, nullptr, 0);
    std::vector<char16_t> dst(dst_cch);
    LCMapStringEx(LOCALE_NAME_INVARIANT, LCMAP_LOWERCASE, src, src_cch, reinterpret_cast<LPWSTR>(dst.data()), dst_cch,
                  nullptr, nullptr, 0);
    return std::u16string(std::u16string_view(dst.data(), dst.size()));
#else
    // TODO(LV): implement

    auto src = reinterpret_cast<uint16_t const *>(s.data());
    size_t dst_cch = 0;
    u16_tolower(src, s.size(), nullptr, nullptr, nullptr, &dst_cch);
    std::vector<char16_t> dst(dst_cch);
    u16_tolower(src, s.size(), nullptr, nullptr, reinterpret_cast<uint16_t *>(dst.data()), &dst_cch);
    return std::u16string(std::u16string_view(dst.data(), dst.size()));
#endif
}

namespace {
template <typename String> void convert_string(std::u16string_view src, String &dest) {
#ifdef _WIN32
    LPCWCH src_p = reinterpret_cast<LPCWCH>(src.data());
    int src_n = static_cast<int>(src.size());
    int cch = WideCharToMultiByte(CP_UTF8, 0, src_p, src_n, nullptr, 0, nullptr, nullptr);
    std::vector<char> buf(cch);
    WideCharToMultiByte(CP_UTF8, 0, src_p, src_n, buf.data(), static_cast<int>(buf.size()), nullptr, nullptr);
    dest.assign(reinterpret_cast<typename String::value_type *>(buf.data()), buf.size());
#else
    size_t cch{};
    uint8_t *p = u16_to_u8(src.data(), src.size(), nullptr, &cch);
    dest.assign(reinterpret_cast<char8_t *>(p), cch);
    free(p);
#endif
}

void convert_string(std::u8string_view src, std::u16string &dest) {
#ifdef _WIN32
    LPCCH src_p = reinterpret_cast<LPCCH>(src.data());
    int src_n = static_cast<int>(src.size());
    int cch = MultiByteToWideChar(CP_UTF8, 0, src_p, src_n, nullptr, 0);
    std::vector<wchar_t> buf(cch);
    MultiByteToWideChar(CP_UTF8, 0, src_p, src_n, buf.data(), static_cast<int>(buf.size()));
    dest.assign(reinterpret_cast<char16_t *>(buf.data()), buf.size());
#else
    size_t cch{};
    uint16_t *p = u8_to_u16(src.data(), src.size(), nullptr, &cch);
    dest.assign(reinterpret_cast<char16_t *>(p), cch);
    free(p);
#endif
}
} // namespace

std::u8string to_u8string(std::u16string_view s) {
    std::u8string ret;
    convert_string(s, ret);
    return ret;
}

std::string to_string(std::u16string_view s) {
    std::string ret;
    convert_string(s, ret);
    return ret;
}

std::u16string to_u16string(std::u8string_view s) {
    std::u16string ret;
    convert_string(s, ret);
    return ret;
}

std::u16string to_u16string(std::string_view s) {
    std::u16string ret;
    std::u8string_view src(reinterpret_cast<char8_t const *>(s.data()), s.size());
    convert_string(src, ret);
    return ret;
}

std::optional<std::string_view> to_string_view(std::span<std::byte const> data) {
    auto p = (char const *)data.data();
    if (std::find(data.begin(), data.end(), (std::byte)'\0') != data.end()) {
        return {};
    }
    auto q = p + data.size();
    if (data.size() >= 3 && p[0] == '\xEF' && p[1] == '\xBB' && p[2] == '\xBF') {
        p += 3;
    }
    return std::string_view(p, q);
}

std::optional<std::u16string_view> to_u16string_view(std::span<std::byte const> data) {
    if (data.size() % 2 != 0) {
        return {};
    }
    auto p = (char16_t const*)data.data();
    auto n = data.size() / 2;
    auto q = p + n;
    if (n >= 1 && p[0] == u'\xFEFF') {
        p += 1;
    }
    return std::u16string_view(p, q);
}

std::optional<std::string> map_unicode_to_utf8_string(std::span<std::byte const> data) {
    if (data.empty()) {
        return "";
    }
    auto wide_sv = poe::util::to_u16string_view(data);
    if (auto wide_sv = poe::util::to_u16string_view(data); wide_sv && !wide_sv->empty() && (*wide_sv)[0] < u'\u0100') {
        return poe::util::to_string(wide_sv.value());
    }
    if (auto narrow_sv = poe::util::to_string_view(data); narrow_sv) {
        return std::string(narrow_sv.value());
    }
    return {};
}

} // namespace poe::util