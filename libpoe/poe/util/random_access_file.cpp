#include <poe/util/random_access_file.hpp>

#include <fmt/format.h>

#ifdef _WIN32
#include <Windows.h>
#else
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#endif

namespace {
uint64_t get_file_size(uintptr_t os_handle) {
#ifdef _WIN32
    HANDLE h = reinterpret_cast<HANDLE>(os_handle);
    LARGE_INTEGER size{};
    GetFileSizeEx(h, &size);
    return static_cast<uint64_t>(size.QuadPart);
#else
    int fd = static_cast<int>(os_handle);
    struct stat buf;
    fstat(fd, &buf);
    return static_cast<uint64_t>(buf.st_size);
#endif
}
} // namespace

namespace poe::util {
random_access_file::random_access_file(std::filesystem::path path) {
#ifdef _WIN32
    HANDLE h =
        CreateFileW(path.c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, nullptr);
    if (h == INVALID_HANDLE_VALUE) {
        throw std::runtime_error("Could not load file");
    }
    this->os_handle_ = reinterpret_cast<uintptr_t>(h);
#else
    int fd = open(path.c_str(), O_RDONLY);
    if (fd < 0) {
        throw std::runtime_error("Could not load file");
    }
    this->os_handle_ = static_cast<uintptr_t>(fd);
#endif
    cached_size_ = get_file_size(this->os_handle_);
}

random_access_file::~random_access_file() {
#ifdef _WIN32
    HANDLE h = reinterpret_cast<HANDLE>(this->os_handle_);
    CloseHandle(h);
#else
    int fd = static_cast<int>(this->os_handle_);
    close(fd);
#endif
}

uint64_t random_access_file::size() const { return *cached_size_; }

void record_histogram_entry(std::array<std::atomic<uint64_t>, 32> &buckets, uint64_t n) {
    for (size_t i = 0; i < 32; ++i) {
        uint64_t upper_bound = 2ull << i;
        if (n < upper_bound) {
            ++buckets[i];
            break;
        }
    }
}

bool random_access_file::read_exact(uint64_t offset, std::byte *p, uint64_t n) const {
    if (offset + n > size()) {
        return false;
    }
#ifdef _WIN32
    HANDLE h = reinterpret_cast<HANDLE>(this->os_handle_);
    HANDLE guard = CreateEvent(nullptr, TRUE, FALSE, nullptr);
    uint64_t const BLOCK_SIZE = 1 << 20;
    uint64_t num_read = 0;
    for (uint64_t i = 0; i < n; i += BLOCK_SIZE) {
        uint64_t const block_n = (std::min)(BLOCK_SIZE, n - i);
        OVERLAPPED overlapped{};
        uint64_t off = offset + i;
        overlapped.Offset = off & 0xFFFFFFFF;
        overlapped.OffsetHigh = off >> 32;
        overlapped.hEvent = guard;
        BOOL res = ReadFile(h, p + i, static_cast<DWORD>(block_n), nullptr, &overlapped);
        if (res == FALSE) {
            if (GetLastError() == ERROR_IO_PENDING) {
                DWORD block_read = 0;
                GetOverlappedResult(h, &overlapped, &block_read, TRUE);
                num_read += block_read;
            } else {
                CloseHandle(guard);
                return false;
            }
        } else {
            DWORD block_read = 0;
            GetOverlappedResult(h, &overlapped, &block_read, FALSE);
            num_read += block_read;
        }
    }
    CloseHandle(guard);
    return num_read == n;
#else
    int fd = static_cast<int>(this->os_handle_);
    ssize_t num_read = TEMP_FAILURE_RETRY(pread(fd, p, n, offset));
    return num_read == n;
#endif
}

uint64_t random_access_file::read_some(uint64_t offset, std::byte *p, uint64_t n) const {
    if (offset < size()) {
        uint64_t actual_n = (std::min)(n, size() - offset);
        if (this->read_exact(offset, p, actual_n)) {
            return actual_n;
        }
    }
    return 0;
}

uint64_t read_some(std::byte const *src_data, uint64_t src_size, uint64_t offset, std::byte *p, size_t n) {
    if (!n) {
        return 0;
    }
    uint64_t end = (std::min)(offset + n, src_size);
    uint64_t amount = end - offset;
    if (!amount) {
        return 0;
    }
    std::memcpy(p, src_data + offset, amount);
    return amount;
}

stream_reader::stream_reader(std::byte const *p, size_t n, uint64_t offset)
    : data_(p), size_(n), cursor_(offset), buffer_(8 << 10), buffer_valid_begin_(0), buffer_valid_end_(0),
      buffer_cursor_(offset) {}

bool stream_reader::read_exact(std::byte *p, size_t n) {
    if (!n) {
        return true;
    }
    while (true) {
        size_t in_buffer = buffer_valid_end_ - buffer_valid_begin_;
        size_t amount = (std::min)(in_buffer, n);
        memcpy(p, buffer_.data() + buffer_valid_begin_, amount);
        buffer_valid_begin_ += amount;
        p += amount;
        n -= amount;
        cursor_ += amount;
        if (!n) {
            break;
        }

        // Couldn't fulfil the whole read with just the buffered contents, see if we should do an unbuffered read
        // when the remainder to read is larger than the buffer size, otherwise refill the buffer and loop.
        buffer_valid_begin_ = 0;
        if (n > buffer_.size()) {
            size_t amountRead = read_some(data_, size_, buffer_cursor_, p, n);
            p += amountRead;
            n -= amountRead;
            cursor_ += amountRead;
            buffer_cursor_ = cursor_;
            buffer_valid_end_ = 0;
            return n == 0;
        } else {
            buffer_valid_end_ = read_some(data_, size_, buffer_cursor_, buffer_.data(), buffer_.size());
            buffer_cursor_ += buffer_valid_end_;
            if (buffer_valid_end_ == 0) {
                return false;
            }
        }
    }
    return true;
}

bool stream_reader::read_terminated_u16string(std::u16string &s) {
    std::vector<char16_t> buf;
    char16_t ch;
    while (true) {
        if (!read_one(ch)) {
            return false;
        }
        buf.push_back(ch);
        if (!ch) {
            s = std::u16string(buf.data());
            return true;
        }
    }
}

bool stream_reader::read_terminated_u16string(std::u16string &s, size_t cch_including_terminator) {
    std::vector<char16_t> buf((std::max<size_t>)(1u, cch_including_terminator));
    if (!read_many(buf.data(), buf.size())) {
        return false;
    }
    s = std::u16string(buf.data());
    return true;
}

bool stream_reader::skip(uint64_t n) {
    auto buffer_cut = (std::min)(buffer_valid_end_ - buffer_valid_begin_, n);
    if (buffer_cut != 0) {
        n -= buffer_cut;
        buffer_valid_begin_ += buffer_cut;
    }
    if (cursor_ + n <= size_) {
        cursor_ += n;
        return true;
    }
    return false;
}

uint64_t stream_reader::cursor() const { return cursor_; }

std::span<std::byte const> stream_reader::tail() const { return std::span(data_, size_).subspan(cursor_); }

std::unique_ptr<stream_reader> make_stream_reader(std::byte const *p, size_t n, uint64_t offset) {
    return std::make_unique<stream_reader>(p, n, offset);
}

std::unique_ptr<stream_reader> make_stream_reader(std::span<std::byte const> data, uint64_t offset) {
    return std::make_unique<stream_reader>(data.data(), data.size(), offset);
}
} // namespace poe::util