#pragma once

#include <mio/mmap.hpp>

namespace poe::util {
using mmap_source = mio::basic_mmap_source<std::byte>;
}