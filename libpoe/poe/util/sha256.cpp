#include <poe/util/sha256.hpp>

#include <charconv>
#include <memory>

#include <fmt/format.h>

#ifdef _WIN32
#include <Windows.h>
#include <bcrypt.h>
#pragma comment(lib, "Bcrypt.lib")
#else
#include <sodium/crypto_hash_sha256.h>
#endif

namespace poe::util {
std::string digest_to_string(sha256_digest const &digest) {
    fmt::memory_buffer buf;
    for (auto b : digest) {
        fmt::format_to(fmt::appender(buf), "{:02x}", static_cast<uint8_t>(b));
    }
    return std::string(buf.data(), buf.data() + 64);
};

sha256_digest text_to_sha256_digest(std::string_view text_digest) {
    sha256_digest ret;
    for (size_t i = 0; i < 32; ++i) {
        auto octet = text_digest.substr(i * 2, 2);
        uint8_t byte;
        std::from_chars(octet.data(), octet.data() + 2, byte, 16);
        ret[i] = (std::byte)byte;
    }
    return ret;
}

#ifdef _WIN32
struct sha256_win32 : sha256 {
    sha256_win32() {
        BCryptOpenAlgorithmProvider(&alg_handle_, BCRYPT_SHA256_ALGORITHM, nullptr, BCRYPT_HASH_REUSABLE_FLAG);
        BCryptCreateHash(alg_handle_, &hash_handle_, nullptr, 0, nullptr, 0, BCRYPT_HASH_REUSABLE_FLAG);
    }

    ~sha256_win32() {
        BCryptDestroyHash(hash_handle_);
        BCryptCloseAlgorithmProvider(alg_handle_, 0);
    }

    sha256_win32(sha256_win32 &) = delete;
    sha256_win32 operator=(sha256_win32 &) = delete;

    void reset() override { finish(); }

    void feed(std::byte const *data, size_t size) override {
        UCHAR *p = const_cast<UCHAR *>(reinterpret_cast<UCHAR const *>(data));
        ULONG n = static_cast<ULONG>(size);
        BCryptHashData(hash_handle_, p, n, 0);
    }

    sha256_digest finish() override {
        sha256_digest ret{};

        UCHAR *p = const_cast<UCHAR *>(reinterpret_cast<UCHAR const *>(ret.data()));
        ULONG n = static_cast<ULONG>(ret.size());
        BCryptFinishHash(hash_handle_, p, n, 0);
        return ret;
    }

  private:
    BCRYPT_ALG_HANDLE alg_handle_;
    BCRYPT_HASH_HANDLE hash_handle_;
};
using sha256_impl = sha256_win32;

#else
struct sha256_linux : sha256 {
    sha256_linux() { reset(); }

    sha256_linux(sha256_linux &) = delete;
    sha256_linux operator=(sha256_linux &) = delete;

    void reset() override { crypto_hash_sha256_init(&state_); }

    void feed(std::byte const *data, size_t size) override {
        crypto_hash_sha256_update(&state_, reinterpret_cast<unsigned char const *>(data), size);
    }

    sha256_digest finish() override {
        sha256_digest ret{};
        crypto_hash_sha256_final(&state_, reinterpret_cast<unsigned char *>(ret.data()));
        reset();
        return ret;
    }

  private:
    crypto_hash_sha256_state state_;
};
using sha256_impl = sha256_linux;

#endif

sha256_digest oneshot_sha256(std::byte const *data, size_t size) {
    sha256_impl hasher;
    hasher.feed(data, size);
    return hasher.finish();
}

sha256_digest oneshot_sha256(std::span<std::byte const> data) { return oneshot_sha256(data.data(), data.size()); }

std::unique_ptr<sha256> incremental_sha256() { return std::make_unique<sha256_impl>(); }

} // namespace poe::util