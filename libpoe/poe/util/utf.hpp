#pragma once

#include <compare>
#include <optional>
#include <string>
#include <string_view>

#include <gsl/span>

namespace poe::util {
std::strong_ordering compare_without_case(std::u16string const& a, std::u16string const& b);
std::strong_ordering compare_without_case(std::u16string const &a, std::u16string const &b);
std::strong_ordering compare_without_case(std::u16string_view a, std::u16string const &b);
std::strong_ordering compare_without_case(std::u16string const &a, std::u16string_view b);

struct less_without_case_predicate {
    bool operator()(std::u16string const &a, std::u16string const &b) const {
        return compare_without_case(a, b) == std::strong_ordering::less;
    }
};


std::string lowercase(std::string_view s);
std::u8string lowercase(std::u8string_view s);
std::u16string lowercase(std::u16string_view s);

std::u8string to_u8string(std::u16string_view s);
std::string to_string(std::u16string_view s);
std::u16string to_u16string(std::string_view s);
std::u16string to_u16string(std::u8string_view s);

std::optional<std::string_view> to_string_view(std::span<std::byte const> data);
std::optional<std::u16string_view> to_u16string_view(std::span<std::byte const> data);

// Assumes that the first character is below U+0100 in order to detect the encoding.
std::optional<std::string> map_unicode_to_utf8_string(std::span<std::byte const> data);
} // namespace poe::util