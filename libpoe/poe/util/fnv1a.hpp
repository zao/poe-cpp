#pragma once

#include <array>
#include <memory>
#include <string>

namespace poe::util {
using fnv1a64_digest = uint64_t;

std::string digest_to_string(fnv1a64_digest const &digest);

struct fnv1a64 {
    fnv1a64();

    void reset();
    void feed(std::byte const *data, size_t size);
    fnv1a64_digest finish();

    private:
    uint64_t hash_;
};

fnv1a64_digest oneshot_fnv1a64(std::byte const *data, size_t size);
fnv1a64 incremental_fnv1a64();
} // namespace poe::util