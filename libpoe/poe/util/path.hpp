#pragma once

#include <string>
#include <string_view>
#include <vector>

#include <poe/util/utf.hpp>

#include <fmt/format.h>
#include <loguru/loguru.hpp>

namespace poe::util {
struct path {
    using extent = std::pair<size_t, size_t>;

    std::string path_;
    std::vector<extent> components_;

    path() : path_() {}

    path(std::string_view src) {
        path_ = src;
        static char const SLASH = '/';

        while (!src.empty() && src.front() == SLASH) {
            src = src.substr(1);
        }

        size_t start = 0;
        while (true) {
            if (auto off = src.find_first_of(SLASH, start); off != std::string_view::npos) {
                if (off > start) {
                    components_.push_back({start, off}); // add non-empty component
                }
                start = off + 1; // skip slash
            } else {
                if (!src.empty()) {
                    components_.push_back({start, src.size()}); // add final non-empty component
                }
                break;
            }
        }
    }

    std::string_view leaf() const {
        if (components_.empty()) {
            return std::string_view("");
        }
        return component(components_.back());
    }

    std::string_view extension() const {
        if (components_.empty()) {
            return std::string_view("");
        }
        auto leaf = component(components_.back());
        auto dot_pos = leaf.find_first_of('.');
        if (dot_pos != std::string_view::npos) {
            return std::string_view(leaf).substr(dot_pos);
        }
        return std::string_view("");
    }

    std::string_view component(extent ex) const {
        std::string_view ret(path_.data() + ex.first, ex.second - ex.first);
        return ret;
    }

    path make_relative(std::string_view fragment) const {
        if (this->components_.empty()) {
            LOG_F(WARNING, "Attempted to make relative path on empty path with ", path_, fragment);
            return path(fragment);
        }
        if (fragment.find("./") != fragment.npos) {
            LOG_F(WARNING, "Attempted to make relative path with dots on path {} with {}: not implemented", path_, fragment);
        }
        auto leaf_extent = components_.back();
        fmt::memory_buffer buf;
        buf.append(path_.substr(0, leaf_extent.first));
        buf.append(fragment);
        return poe::util::path(to_string(buf));
    }

    static path from_string(std::string_view src) { return path(src); }
    static path from_string(std::u16string_view src) { return path(poe::util::to_string(src)); }
};

uint64_t file_path_hash(path const &p);
uint64_t file_path_hash(std::string_view p);
uint64_t file_path_hash(std::u8string_view p);

namespace string_literals {
inline path operator"" _poe(char const *str, size_t n) { return path::from_string(str); }
inline path operator"" _poe(char16_t const *str, size_t n) { return path::from_string(str); }
} // namespace string_literals

} // namespace poe::util