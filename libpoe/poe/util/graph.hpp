#pragma once

#include <optional>
#include <utility>
#include <vector>

#include <gsl/span>

namespace poe::util {
	std::optional<std::vector<size_t>> topologic_sort(std::span<std::pair<size_t, size_t>> edges);
}