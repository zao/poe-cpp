#include <poe/util/decompress.hpp>

#include <poe/util/reader.hpp>

#include <functional>

#ifdef _WIN32
#include <Windows.h>
#define DECOMPRESS_API WINAPI
#else
#include <dlfcn.h>
#define DECOMPRESS_API
#endif

namespace poe::util {
namespace {
struct decompressor {
    decompressor() {
        std::tuple<char const *, char const *> candidates[] = {
//            {"oo2core_3_win64.dll", "OodleLZ_Decompress"},
//            {"oo2core_7_win64.dll", "OodleLZ_Decompress"},
            {"libooz.dll", "Ooz_Decompress"},
            {"ooz.dll", "Ooz_Decompress"},
            {"oo2core_7_win64.dll", "OodleLZ_Decompress"},
        };
        for (auto &[filename, export_name] : candidates) {
#ifdef _WIN32
            auto mod = LoadLibraryA(filename);
            if (!mod) {
                continue;
            }
            mod_.reset(mod, &FreeLibrary);

            auto fun = reinterpret_cast<decompress_sig>(GetProcAddress((HMODULE)mod_.get(), export_name));
            if (!fun) {
                continue;
            }
            fun_ = fun;
#else
            auto mod = dlopen(filename, RTLD_NOW | RTLD_LOCAL);
            if (!mod) {
                continue;
            }
            mod_.reset(mod, &dlclose);

            auto fun = reinterpret_cast<decompress_sig>(dlsym(mod, export_name));
            if (!fun) {
                continue;
            }
            fun_ = fun;
#endif
            break;
        }
    }

    int operator()(std::span<uint8_t> src, std::span<uint8_t> dst) {
        int rc = fun_(src.data(), static_cast<int>(src.size()), dst.data(), static_cast<int>(dst.size()), 0, 0, 0,
                      nullptr, 0, nullptr, nullptr, nullptr, 0, 0);
        return rc;
    }

  private:
    decompressor(decompressor const &) = delete;
    decompressor &operator=(decompressor const &) = delete;

    using decompress_sig = int(DECOMPRESS_API *)(uint8_t *src_buf, int src_len, uint8_t *dst, size_t dst_size, int, int,
                                                 int, uint8_t *, size_t, void *, void *, void *, size_t, int);

    std::shared_ptr<void> mod_;
    decompress_sig fun_;
};

size_t const SAFE_SPACE = 64;
} // namespace

std::optional<std::vector<std::byte>> decompress_oodle(void *src_data, size_t src_size, size_t dst_size) {
    static decompressor decompress_fun;

    std::vector<std::byte> dst(dst_size + SAFE_SPACE);

    std::span<uint8_t> src_span(reinterpret_cast<uint8_t *>(src_data), src_size);
    std::span<uint8_t> dst_span(reinterpret_cast<uint8_t *>(dst.data()), dst_size);
    if (decompress_fun(src_span, dst_span) == dst_size) {
        dst.resize(dst_size);
        return dst;
    } else {
        return {};
    }
}
} // namespace poe::util