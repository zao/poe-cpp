#include <poe/util/fnv1a.hpp>

#include <fmt/core.h>

namespace poe::util {
namespace {
static uint64_t const FNV1_OFFSET_BASIS_64 = 0xcbf29ce484222325ull;
static uint64_t const FNV1_PRIME_64 = 0x100000001b3;
} // namespace

std::string digest_to_string(fnv1a64_digest const &digest) {
    char buf[17]{};
    fmt::format_to(buf, "{:016X}", digest);
    return buf;
}

fnv1a64::fnv1a64() { reset(); }

void fnv1a64::reset() { hash_ = FNV1_OFFSET_BASIS_64; }

void fnv1a64::feed(std::byte const *data, size_t n) {
    auto *p = reinterpret_cast<uint8_t const *>(data), *end = p + n;
    while (p != end) {
        hash_ = hash_ ^ *p;
        hash_ = hash_ * FNV1_PRIME_64;
        ++p;
    }
}

uint64_t fnv1a64::finish() { return hash_; }

fnv1a64_digest oneshot_fnv1a64(std::byte const *data, size_t size) {
    fnv1a64 hasher;
    hasher.feed(data, size);
    return hasher.finish();
}

fnv1a64 incremental_fnv1a64() { return fnv1a64{}; }
} // namespace poe::util