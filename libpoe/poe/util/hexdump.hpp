#pragma once

#include <gsl/span>
#include <string>

namespace poe::util {
	std::string hexdump(std::span<std::byte const> data, size_t row_size);
}