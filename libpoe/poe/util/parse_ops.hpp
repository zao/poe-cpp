#pragma once

#include <charconv>
#include <optional>
#include <string>
#include <string_view>
#include <system_error>

#include <fmt/format.h>

namespace poe::util {
struct ParseOps {
    ParseOps(ParseOps const &) = default;
    explicit ParseOps(std::string_view sv) : sv_(sv), good_(true) {}

    bool ok() { return good_; }

    std::optional<std::string> err() {
        if (!good_) {
            std::string_view s(err_);
            if (auto pos = s.find_last_not_of('\n'); pos != s.npos) {
                s = s.substr(0, pos + 1);
            }
            return std::string(s);
        }
        return std::nullopt;
    }

    ParseOps &reset() {
        err_.clear();
        good_ = true;
        return *this;
    }

    ParseOps &fail() {
        good_ = false;
        return *this;
    }

    bool is_next_char(char ch) const { return !sv_.empty() && sv_[0] == ch; }
    bool is_next_digit() const { return !sv_.empty() && std::isdigit((int)(char)sv_[0]); }
    bool is_next_doublequote() const { return !sv_.empty() && sv_[0] == '"'; }
    bool is_next_newline() const { return !sv_.empty() && (sv_[0] == '\r' || sv_[0] == '\n'); }

    void advance(size_t amount) {
        if (amount != sv_.npos) {
            sv_ = sv_.substr(amount);
        } else {
            sv_ = sv_.substr(sv_.size());
        }
    }

    ParseOps &string_lit(std::string_view ref_lit) {
        if (!good_) {
            return *this;
        }

        if (!sv_.starts_with(ref_lit)) {
            auto other = sv_.substr(0, sv_.find_first_of(" \t\r\n"));
            fmt::format_to(std::back_inserter(err_), "literal mismatch: expected \"{}\", got \"{}\"\n", ref_lit, other);
            return fail();
        }

        advance(ref_lit.size());
        return *this;
    }

    ParseOps &string(std::optional<std::string> &out) {
        std::string s;
        string(s);
        if (good_) {
            out = s;
        }
        return *this;
    }

    ParseOps &string(std::string &out) {
        if (!good_) {
            return *this;
        }
        if (sv_.empty()) {
            fmt::format_to(std::back_inserter(err_), "string parse failed: end of input\n");
            return fail();
        }

        auto end = sv_.find_first_of(" \t\r\n");
        if (end == 0) {
            fmt::format_to(std::back_inserter(err_), "string parse failed: found leading whitespace\n");
            return fail();
        }

        out = sv_.substr(0, end);
        advance(end);
        return *this;
    }

    ParseOps &rest_of_line(std::optional<std::string> &out) {
        std::string s;
        rest_of_line(s);
        if (good_) {
            out = s;
        }
        return *this;
    }

    ParseOps &rest_of_line(std::string &out) {
        if (!good_) {
            return *this;
        }
        if (sv_.empty()) {
            fmt::format_to(std::back_inserter(err_), "rest of line parse failed: end of input\n");
            return fail();
        }

        auto end = sv_.find_first_of("\r\n");
        if (end == 0) {
            fmt::format_to(std::back_inserter(err_), "rest of line parse failed: found leading newline\n");
            return fail();
        }

        out = sv_.substr(0, end);
        advance(end);
        return *this;
    }

    ParseOps &quoted_string(std::optional<std::string> &out) {
        std::string s;
        quoted_string(s);
        if (good_) {
            out = s;
        }
        return *this;
    }

    ParseOps &quoted_string(std::string &out) {
        using namespace std::string_view_literals;
        if (!good_) {
            return *this;
        }
        if (sv_.size() < 2) {
            fmt::format_to(std::back_inserter(err_), "quoted string parse failed: input too short\n");
            return fail();
        }
        if (sv_[0] != '"') {
            fmt::format_to(std::back_inserter(err_), "quoted string parse failed: expected leading double-quote\n");
            return fail();
        }
        size_t start = 1;
        fmt::memory_buffer out_buf;
        while (true) {
            auto next = sv_.find_first_of("\"\\", start);
            if (next == sv_.npos) {
                fmt::format_to(std::back_inserter(err_), "quoted string parse failed: end of input\n");
                return fail();
            }
            out_buf.append(sv_.data() + start, sv_.data() + next);
            if (sv_[next] == '"') {
                out = to_string(out_buf);
                advance(next + 1);
                return *this;
            }
            if (sv_[next] == '\\') {
                if (next + 1 == sv_.size()) {
                    fmt::format_to(std::back_inserter(err_), "quoted string parse failed: end of input on escape\n");
                    return fail();
                }
                char ch = sv_[next + 1];
                switch (ch) {
                case 'r':
                    out_buf.append("\r"sv);
                    break;
                case 'n':
                    out_buf.append("\n"sv);
                    break;
                case 't':
                    out_buf.append("\t"sv);
                    break;
                default:
                    fmt::format_to(std::back_inserter(err_), "quoted string parse failed: unexpected escape \\{}\n", ch);
                    return fail();
                }
                start = next + 2;
            }
        }
    }

    template <typename Int> ParseOps &integer(std::optional<Int> &out) {
        Int i;
        integer(i);
        if (good_) {
            out = i;
        }
        return *this;
    }

    template <typename Int> ParseOps &integer(Int &out) {
        if (!good_) {
            return *this;
        }

        auto rc = std::from_chars(sv_.data(), sv_.data() + sv_.size(), out);
        if (rc.ec != std::errc{}) {
            fmt::format_to(std::back_inserter(err_), "integer parse failed: {}\n",
                           std::make_error_condition(rc.ec).message());
            return fail();
        }

        advance(rc.ptr - sv_.data());
        return *this;
    }

    template <typename Real> ParseOps &real(Real &out) {
        if (!good_) {
            return *this;
        }

        auto rc = std::from_chars(sv_.data(), sv_.data() + sv_.size(), out);
        if (rc.ec != std::errc{}) {
            fmt::format_to(std::back_inserter(err_), "real parse failed: {}\n",
                           std::make_error_condition(rc.ec).message());
            return fail();
        }

        advance(rc.ptr - sv_.data());
        return *this;
    }

    ParseOps &whitespace(bool optional = false) {
        if (!good_) {
            return *this;
        }
        if (sv_.empty()) {
            if (optional) {
                return *this;
            }
            fmt::format_to(std::back_inserter(err_), "whitespace skip failed: end of input\n");
            return fail();
        }

        auto end = sv_.find_first_not_of(" \t");
        if (end == 0) {
            if (optional) {
                return *this;
            }
            fmt::format_to(std::back_inserter(err_), "whitespace skip failed: no whitespace\n");
            return fail();
        }

        advance(end);
        return *this;
    }

    ParseOps &whitespace_opt() { return this->whitespace(true); }

    ParseOps &spaces() {
        if (!good_) {
            return *this;
        }
        if (sv_.empty()) {
            fmt::format_to(std::back_inserter(err_), "spaces skip failed: end of input\n");
            return fail();
        }

        auto end = sv_.find_first_not_of(' ');
        if (end == 0) {
            fmt::format_to(std::back_inserter(err_), "spaces skip failed: no spaces\n");
            return fail();
        }

        advance(end);
        return *this;
    }

    ParseOps &eol() {
        if (!good_) {
            return *this;
        }
        if (sv_.empty()) {
            return *this;
        }
        bool is_lf = sv_.starts_with("\n");
        bool is_crlf = sv_.starts_with("\r\n");
        if (!is_lf && !is_crlf) {
            fmt::format_to(std::back_inserter(err_), "eol skip failed: no newline\n");
            return fail();
        }

        if (is_lf) {
            advance(1);
        } else if (is_crlf) {
            advance(2);
        }
        return *this;
    }

    bool at_end() const { return sv_.empty() || sv_.find_first_not_of(" \t\r\n") == sv_.npos; }

    std::string_view sv_;
    std::string err_;
    bool good_;
};
} // namespace poe::util