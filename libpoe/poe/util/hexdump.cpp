#include <poe/util/hexdump.hpp>

#include <fmt/format.h>

namespace poe::util {
	std::string hexdump(std::span<std::byte const> data, size_t row_size) {
		using namespace std::string_view_literals;
		fmt::memory_buffer out;
		auto partial_row_size = data.size() % row_size;
		auto const rows = data.size() / row_size + (size_t)(bool)partial_row_size;
		for (size_t row = 0; row < rows; ++row) {
			auto rem = data.size() - row * row_size;
			auto const cols = rem < row_size ? rem : row_size;
			auto row_span = data.subspan(row * row_size, cols);
			bool first_byte = true;
			for (auto byte : row_span) {
				if (!first_byte) {
					out.append(" "sv);
				}
				first_byte = false;
				fmt::format_to(fmt::appender(out), "{:02X}", byte);
			}
			for (size_t col = cols; col < row_size; ++col) {
				out.append("   "sv);
			}
			out.append("  "sv);
			for (auto byte : row_span) {
				if (isprint((int)(unsigned)byte)) {
					fmt::format_to(fmt::appender(out), "{}", (char)byte);
				} else {
					out.append("."sv);
				}
			}
			out.append("\n"sv);
		}
		return std::string(out.begin(), out.end());
	}
}