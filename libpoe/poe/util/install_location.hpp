#pragma once

#include <filesystem>
#include <optional>
#include <vector>

namespace poe::util {
enum class install_kind {
    Steam,
    Standalone,
    Bundled,
    Manual,
};

std::filesystem::path data_local_root();
std::optional<std::filesystem::path> executable_dir();
std::vector<std::pair<install_kind, std::filesystem::path>> install_locations();

auto SlurpFile(std::filesystem::path path) -> std::optional<std::vector<std::byte>>;

std::filesystem::path AssetPath(std::string_view filename);

auto SlurpAsset(std::string_view filename) -> std::optional<std::vector<std::byte>>;

struct ReloadableFile {
    ReloadableFile() {}
    explicit ReloadableFile(std::filesystem::path path);

    bool Updated();

    std::optional<std::vector<std::byte>> Slurp();

  private:
    std::filesystem::path path_;
    std::optional<std::filesystem::file_time_type> lastWrite_;
};
} // namespace poe::util