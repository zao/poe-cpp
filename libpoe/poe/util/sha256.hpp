#pragma once

#include <array>
#include <memory>
#include <span>
#include <string>

namespace poe::util {
using sha256_digest = std::array<std::byte, 32>;

std::string digest_to_string(sha256_digest const &digest);
sha256_digest text_to_sha256_digest(std::string_view text_digest);

struct sha256 {
    virtual ~sha256() {}

    virtual void reset() = 0;
    virtual void feed(std::byte const *data, size_t size) = 0;
    virtual sha256_digest finish() = 0;
};

sha256_digest oneshot_sha256(std::span<std::byte const> data);
sha256_digest oneshot_sha256(std::byte const *data, size_t size);
std::unique_ptr<sha256> incremental_sha256();
} // namespace poe::util