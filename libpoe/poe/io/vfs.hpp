#pragma once

#include <filesystem>
#include <functional>
#include <future>
#include <memory>
#include <vector>

#include <poe/io/file_collective.hpp>
#include <poe/io/file_handle.hpp>

#include <poe/util/path.hpp>

namespace poe::format::bundle {
struct parsed_bundle;
}

namespace poe::format::ggpk {
struct parsed_ggpk;
}

namespace poe::format::index {
struct parsed_index;
}

namespace poe::io {
using poe::util::path;

/*
 * There are several types of installations and data sources we might want to consider:
 *  - a legacy standalone installation with a v2 GGPK file that contain just loose files;
 *  - a legacy Steam installation that contains a v2 GGPK file that contain just loose files;
 *  - a standalone installation with a v3 GGPK file that contain loose files and bundles;
 *  - a Steam installation with loose files and bundles;
 */

struct vfs;

struct outline_db {};
struct file_council_connection;

struct vfs : std::enable_shared_from_this<vfs> {
    // Loads Steam installations, Standalone installations, dedicated GGPK files.
    explicit vfs(std::filesystem::path source_path);
    explicit vfs(std::shared_ptr<file_council_connection> council, std::string_view branch, uint32_t build);
    explicit vfs(file_collective_info collective);

    ~vfs() {}

  private:
    vfs &operator=(vfs const &) = delete;
    vfs(vfs const &) = delete;

  public:
    std::shared_ptr<file_handle> open_file(path const &p) const;
    std::future<std::shared_ptr<file_handle>> async_open_file(path const &p) const;

    using enumerate_files_fn = std::function<void(poe::util::path const &)>;
    void enumerate_files(enumerate_files_fn op) const;

  private:
    enum class vfs_kind {
        legacy_ggpk,
        tech_steam,
        tech_standalone,
        file_council,
        file_collective,
    };
    vfs_kind kind_;
    std::filesystem::path source_path_;
    std::shared_ptr<poe::format::index::parsed_index> index_;
    std::shared_ptr<poe::format::ggpk::parsed_ggpk> pack_;
    std::vector<std::shared_ptr<poe::format::bundle::parsed_bundle>> bundles_;
    std::shared_ptr<file_council_connection> council_;
    std::shared_ptr<file_collective_state> collective_;
};

std::shared_ptr<std::vector<std::byte>> chase_symlinks(std::shared_ptr<poe::io::vfs> vfs,
                                                       std::shared_ptr<std::vector<std::byte>> payload,
                                                       std::string &out_path);
} // namespace poe::io