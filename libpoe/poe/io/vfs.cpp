#include <poe/io/vfs.hpp>

#include <poe/format/bundle.hpp>
#include <poe/format/ggpk.hpp>
#include <poe/format/index.hpp>

#include <poe/util/utf.hpp>

#include <cpr/cpr.h>
#include <fmt/format.h>
#include <nlohmann/json.hpp>

#include <fstream>
#include <queue>

namespace poe::io {
namespace {
std::optional<std::vector<std::byte>> slurp_file(std::filesystem::path path) {
    if (auto is = std::ifstream(path, std::ios::binary)) {
        std::vector<std::byte> ret(std::filesystem::file_size(path));
        if (is.read(reinterpret_cast<char *>(ret.data()), ret.size())) {
            return ret;
        }
    }
    return {};
}
} // namespace

struct ggpk_file : file_handle {
    ggpk_file(std::shared_ptr<poe::format::ggpk::parsed_ggpk> pack, poe::format::ggpk::parsed_file *file)
        : pack_(pack), file_(file) {}

    uint64_t size() const override { return file_->data_size_; }

    std::vector<std::byte> read_all() const override {
        if (file_->data_offset_ + file_->data_size_ > pack_->mapping_.size()) {
            return {};
        }
        auto bytes = data_span();
        return std::vector<std::byte>(bytes.begin(), bytes.end());
    }

    std::span<std::byte const> data_span() const {
        std::span<std::byte const> pack_span(pack_->mapping_);
        return pack_span.subspan(file_->data_offset_, file_->data_size_);
    }

  private:
    std::shared_ptr<poe::format::ggpk::parsed_ggpk> pack_;
    poe::format::ggpk::parsed_file *file_;
};

std::shared_ptr<ggpk_file> open_ggpk_file(std::shared_ptr<poe::format::ggpk::parsed_ggpk> pack,
                                          poe::util::path const &path) {
    namespace ggpk = poe::format::ggpk;
    ggpk::parsed_entry *q = pack->root_;
    for (auto ex : path.components_) {
        auto comp = path.component(ex);
        auto u16comp = poe::util::to_u16string(comp);
        if (auto dir = dynamic_cast<ggpk::parsed_directory *>(q)) {
            auto &entries = dir->entries_;
            auto I = std::find_if(entries.begin(), entries.end(), [&u16comp](auto &&e) {
                return poe::util::compare_without_case(e->name_, u16comp) == std::strong_ordering::equal;
            });
            if (I != entries.end()) {
                q = I->get();
            } else {
                return {};
            }
        } else {
            return {};
        }
    }
    if (auto file = dynamic_cast<ggpk::parsed_file *>(q)) {
        return std::make_shared<ggpk_file>(pack, file);
    }
    return {};
}

struct disk_file : file_handle {
    disk_file(std::filesystem::path path) {
        std::error_code ec;
        mapping_.map(path.string(), ec);
    }

    uint64_t size() const override { return mapping_.size(); }

    std::vector<std::byte> read_all() const override {
        std::vector<std::byte> ret(mapping_.begin(), mapping_.end());
        return ret;
    }

    std::span<std::byte const> data_span() const {
        std::span<std::byte const> pack_span(mapping_);
        return mapping_;
    }

  private:
    poe::util::mmap_source mapping_;
};

std::shared_ptr<disk_file> open_disk_file(std::filesystem::path path) { return std::make_shared<disk_file>(path); }

vfs::vfs(std::filesystem::path source_path) : source_path_(source_path) {
    using namespace poe::util::string_literals;
    if (!std::filesystem::exists(source_path)) {
        throw std::runtime_error("VFS source does not exist.");
    }
    if (std::filesystem::is_directory(source_path)) {
        // Determine if it's a Steam installation, a Standalone installation
        // or an exploded installation tree.
        if (auto ggpk_path = source_path / "Content.ggpk"; exists(ggpk_path) && is_regular_file(ggpk_path)) {
            pack_ = poe::format::ggpk::index_ggpk(ggpk_path);
            if (!pack_) {
                throw std::runtime_error("Could not open GGPK file.");
            }
            if (pack_->version_ == 2) {
                kind_ = vfs_kind::legacy_ggpk;
            } else {
                auto index_file = open_ggpk_file(pack_, "Bundles2/_.index.bin"_poe);
                if (index_file) {
                    auto index_data = index_file->read_all();
                    index_ = poe::format::index::parse_index(index_data);
                    bundles_.reserve(index_->bundle_records_.size());
                    for (auto const &rec : index_->bundle_records_) {
                        struct bundle_state {
                            std::shared_ptr<ggpk_file> file_;
                            std::shared_ptr<poe::format::bundle::parsed_bundle> bundle_;
                        };
                        auto state = std::make_shared<bundle_state>();
                        state->file_ =
                            open_ggpk_file(pack_, poe::util::path(fmt::format("Bundles2/{}.bundle.bin", rec.name_)));
                        if (state->file_) {
                            state->bundle_ = poe::format::bundle::parse_bundle(state->file_->data_span());
                        }
                        bundles_.push_back(
                            std::shared_ptr<poe::format::bundle::parsed_bundle>(state, state->bundle_.get()));
                    }
                    kind_ = vfs_kind::tech_standalone;
                } else {
                    throw std::runtime_error("Could not find index in GGPK file.");
                }
            }
        } else if (auto bundles2_path = source_path / "Bundles2";
                   exists(bundles2_path) && is_directory(bundles2_path)) {
            auto index_data = slurp_file(bundles2_path / "_.index.bin");
            if (index_data) {
                index_ = poe::format::index::parse_index(*index_data);
                bundles_.reserve(index_->bundle_records_.size());
                for (auto const &rec : index_->bundle_records_) {
                    struct bundle_state {
                        std::shared_ptr<disk_file> file_;
                        std::shared_ptr<poe::format::bundle::parsed_bundle> bundle_;
                    };
                    auto state = std::make_shared<bundle_state>();
                    state->file_ = open_disk_file(bundles2_path / fmt::format("{}.bundle.bin", rec.name_));
                    state->bundle_ = poe::format::bundle::parse_bundle(state->file_->data_span());
                    bundles_.push_back(
                        std::shared_ptr<poe::format::bundle::parsed_bundle>(state, state->bundle_.get()));
                }
                kind_ = vfs_kind::tech_steam;
            } else {
                throw std::runtime_error("No valid index bundle in Steam installation.");
            }
        } else {
            throw std::runtime_error(fmt::format("Could not open game location \"{}\"", source_path.string()));
        }
    } else {
        pack_ = poe::format::ggpk::index_ggpk(source_path);
        if (pack_->version_ != 2) {
            throw std::runtime_error("Specified GGPK not a legacy standalone GGPK");
        }
        kind_ = vfs_kind::legacy_ggpk;
    }
}

vfs::vfs(file_collective_info collective)
    : kind_(vfs_kind::file_collective), collective_(std::make_shared<file_collective_state>(collective)) {}

struct bundle_file : file_handle {
    uint64_t size() const override { return file_rec_.file_size_; }
    std::vector<std::byte> read_all() const override {
        auto data = bundle_->decompress_range(file_rec_.file_offset_, file_rec_.file_size_);
        return *data;
    }

    std::shared_ptr<poe::format::bundle::parsed_bundle> bundle_;
    poe::format::index::file_record file_rec_;
};

std::shared_ptr<bundle_file> open_loose_bundle_file(std::filesystem::path path,
                                                    poe::format::index::file_record file_rec) {
    auto bf = std::make_shared<bundle_file>();
    std::error_code ec;
    poe::util::mmap_source mapping;
    if (mapping.map(path.native(), ec); ec) {
        return {};
    }
    bf->bundle_ = poe::format::bundle::parse_bundle(mapping);
    bf->file_rec_ = file_rec;
    return bf;
}

std::shared_ptr<bundle_file> open_pack_bundle_file(poe::format::ggpk::parsed_ggpk const *pack,
                                                   poe::format::ggpk::parsed_file const *f,
                                                   poe::format::index::file_record file_rec) {
    auto bf = std::make_shared<bundle_file>();
    auto pack_span = std::span<std::byte const>(pack->mapping_);
    auto slice = pack_span.subspan(f->data_offset_, f->data_size_);
    bf->bundle_ = poe::format::bundle::parse_bundle(slice);
    bf->file_rec_ = file_rec;
    return bf;
}

std::shared_ptr<bundle_file> open_bundle_file(std::shared_ptr<poe::format::bundle::parsed_bundle> bundle,
                                              poe::format::index::file_record file_rec) {
    if (!bundle) {
        return {};
    }
    auto bf = std::make_shared<bundle_file>();
    bf->bundle_ = bundle;
    bf->file_rec_ = file_rec;
    return bf;
}

std::string concat_path_parts(std::string_view base, std::string_view leaf) {
    if (base.empty()) {
        return std::string(leaf);
    }
    return fmt::format("{}/{}", base, leaf);
}

void vfs::enumerate_files(vfs::enumerate_files_fn op) const {
    // Not handling any loose files here, all the formats we care about (for now)
    // are in bundles.
    switch (kind_) {
    case vfs_kind::tech_standalone:
    case vfs_kind::tech_steam: {
        for (size_t i = 0; i < index_->directory_record_count(); ++i) {
            index_->enumerate_directory_files(i, [&](std::string_view path) { op(path); });
        }
    } break;
    case vfs_kind::legacy_ggpk: {
        using poe::format::ggpk::parsed_directory;
        using poe::format::ggpk::parsed_file;
        struct iter_state {
            parsed_directory const *dir;
            std::string path;
        };
        std::queue<iter_state> q;
        q.push({pack_->root_, ""});
        while (!q.empty()) {
            auto parent = q.front();
            q.pop();
            for (auto const &e : parent.dir->entries_) {
                auto new_path = concat_path_parts(parent.path, poe::util::to_string(e->name_));
                if (auto fp = dynamic_cast<parsed_file const *>(e.get())) {
                    op(poe::util::path(new_path));
                } else {
                    q.push({static_cast<parsed_directory const *>(e.get()), new_path});
                }
            }
        }
    } break;
    case vfs_kind::file_collective: {
        collective_->enumerate_files(op);
    } break;
    default: {
    } break;
    }
}

std::shared_ptr<file_handle> vfs::open_file(path const &p) const {
    switch (kind_) {
    case vfs_kind::tech_steam: {
        auto path_hash = file_path_hash(p);
        if (auto I = index_->path_hash_to_file_record_.find(path_hash); I != index_->path_hash_to_file_record_.end()) {
            auto const &file_rec = index_->file_records_[I->second];
            auto &bundle = bundles_[file_rec.bundle_index_];
            return open_bundle_file(bundle, file_rec);
        }
        auto disk_path = source_path_ / p.path_;
        if (exists(disk_path) && is_regular_file(disk_path)) {
            return open_disk_file(disk_path);
        }
    } break;
    case vfs_kind::tech_standalone: {
        auto path_hash = file_path_hash(p);
        if (auto I = index_->path_hash_to_file_record_.find(path_hash); I != index_->path_hash_to_file_record_.end()) {
            auto const &file_rec = index_->file_records_[I->second];
            auto &bundle = bundles_[file_rec.bundle_index_];
            return open_bundle_file(bundle, file_rec);
        }
        if (auto from_pack = open_ggpk_file(pack_, p)) {
            return from_pack;
        }
    } break;
    case vfs_kind::legacy_ggpk: {
        return open_ggpk_file(pack_, p);
    } break;
    case vfs_kind::file_collective: {
        return collective_->open_file(p);
    } break;
    default:
        abort();
    }
    return std::shared_ptr<file_handle>();
}

std::future<std::shared_ptr<file_handle>> vfs::async_open_file(path const &p) const {
    auto self = shared_from_this();
    return std::async(std::launch::async, [self, p] { return self->open_file(p); });
}

std::shared_ptr<std::vector<std::byte>> chase_symlinks(std::shared_ptr<poe::io::vfs> vfs,
                                                       std::shared_ptr<std::vector<std::byte>> payload,
                                                       std::string &out_path) {
    while (payload->size() && (*payload)[0] == (std::byte)'*') {
        auto p = (char const *)payload->data();
        out_path = std::string(p + 1, p + payload->size());
        poe::util::path next_path(out_path);
        auto next = vfs->open_file(next_path);
        if (next) {
            payload = std::make_shared<std::vector<std::byte>>(next->read_all());
        } else {
            return {};
        }
    }
    return payload;
}
} // namespace poe::io