#pragma once

#include <array>
#include <cstdint>
#include <filesystem>
#include <functional>
#include <memory>
#include <string>

#include <poe/io/file_handle.hpp>
#include <poe/util/path.hpp>

struct sqlite3;

namespace cpr {
class Session;
}

namespace poe::io {

struct file_collective_info {
    std::optional<std::filesystem::path> collective_root_;
    int build;
};

struct file_collective_state {
    explicit file_collective_state(file_collective_info info);
    ~file_collective_state();

    void load_ndjson_manifest(uint64_t manifest_id);
    void load_bungle_manifest(uint64_t manifest_id);
    std::shared_ptr<poe::io::file_handle> open_file(poe::util::path path);

    void enumerate_files(std::function<void(std::string_view path)> op);

    struct cache_info {
        std::filesystem::path collective_root_;
        std::filesystem::path index_root_;
        std::filesystem::path data_root_;

        void create_directories() const {
            using std::filesystem::create_directories;
            create_directories(collective_root_);
            create_directory(index_root_);
            create_directory(data_root_);

            // Create partition directories 00 through ff.
            for (int i = 0; i < 256; ++i) {
                char buf[3] = {};
                fmt::format_to(buf, "{:02x}", i);
                create_directory(data_root_ / buf);
            }
        }
    };

    struct remote_info {
        std::string meta_url_base_;
        std::string index_url_base_;
        std::string data_url_base_;
    };

  private:
    file_collective_info info_;
    cache_info cache_info_;
    remote_info remote_info_;

    using content_hash = std::array<uint8_t, 32>;

    struct IndexEntry {
        uint64_t path_hash;
        std::u8string path;
        content_hash file_hash;
        uint32_t file_size;
        bool compressed;
    };

    struct FileIndex;
    struct BungleFile;

    uint64_t manifest_id_;
    std::shared_ptr<FileIndex> index_;
    std::shared_ptr<BungleFile> bungle_;

    struct Caches;
    std::unique_ptr<Caches> caches_;
};
} // namespace poe::io