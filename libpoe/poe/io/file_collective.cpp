#include <poe/io/file_collective.hpp>

#include <cassert>
#include <charconv>
#include <chrono>
#include <functional>
#include <memory>
#include <random>
#include <span>

#include <absl/container/flat_hash_map.h>
#include <blake3.h>
#include <cpr/cpr.h>
#include <jansson.h>
#include <nlohmann/json.hpp>
#include <sqlite3.h>
#include <tbb/concurrent_queue.h>
#include <tbb/parallel_for.h>
#include <tbb/enumerable_thread_specific.h>
#include <tbb/scalable_allocator.h>
#include <zstd.h>

#include <poe/util/install_location.hpp>

using namespace std::chrono_literals;

namespace {
template <typename DbPtr> std::shared_ptr<sqlite3_stmt> prepare_stmt(DbPtr db, std::string_view sql) {
    sqlite3_stmt *stmt{};
    if (auto rc = sqlite3_prepare_v2(std::to_address(db), sql.data(), (int)sql.size(), &stmt, nullptr);
        rc == SQLITE_OK) {
        return std::shared_ptr<sqlite3_stmt>(stmt, &sqlite3_finalize);
    }
    sqlite3_finalize(stmt);
    return nullptr;
}
} // namespace

namespace poe::io {
struct file_collective_state::FileIndex {
    std::vector<IndexEntry, tbb::scalable_allocator<IndexEntry>> entries;
    absl::flat_hash_map<absl::string_view, IndexEntry const *> byPath;
    absl::flat_hash_map<uint64_t, IndexEntry const *> byPathHash;
};

struct file_collective_state::Caches {
    Caches()
        : dStreams([] { return std::shared_ptr<ZSTD_DStream>(ZSTD_createDStream(), &ZSTD_freeDStream); }),
          cprSessions(std::make_shared<cpr::Session>) {}

    tbb::enumerable_thread_specific<std::shared_ptr<ZSTD_DStream>> dStreams;
    tbb::enumerable_thread_specific<std::shared_ptr<cpr::Session>> cprSessions;
};

bool enumerate_zstd_lines(std::span<uint8_t const> compressed, std::function<void(std::string_view path)> cb) {
    auto ds = std::shared_ptr<ZSTD_DStream>(ZSTD_createDStream(), &ZSTD_freeDStream);
    ZSTD_initDStream(ds.get());

    ZSTD_inBuffer ibuf{
        .src = compressed.data(),
        .size = compressed.size(),
        .pos = 0,
    };

    std::vector<uint8_t> out_buf(ZSTD_DStreamOutSize());
    ZSTD_outBuffer obuf{.dst = out_buf.data(), .size = out_buf.size(), .pos = 0};

    std::vector<char> line_buf;
    // Decompress compressed data piecemeal
    while (true) {
        auto rc = ZSTD_decompressStream(ds.get(), &obuf, &ibuf);
        if (rc < 0) {
            return false;
        }

        // Scan new chunk for complete lines
        std::span<char const> chunk((char const *)obuf.dst, obuf.pos);
        while (true) {
            auto I = std::find(chunk.begin(), chunk.end(), '\n');
            line_buf.insert(line_buf.end(), chunk.begin(), I);
            if (I == chunk.end()) {
                break;
            }
            cb(std::string_view(line_buf.begin(), line_buf.end()));
            line_buf.clear();
            chunk = chunk.subspan(std::distance(chunk.begin(), I) + 1);
        }
        obuf.pos = 0;
        if (rc == 0) {
            if (line_buf.size()) {
                cb(std::string_view(line_buf.begin(), line_buf.end()));
            }
            break;
        }
    }
    return true;
}

template <size_t ByteCount> std::optional<std::array<uint8_t, ByteCount>> hex_to_array(std::string_view text) {
    if (text.size() != ByteCount * 2) {
        return std::nullopt;
    }

    std::array<uint8_t, ByteCount> ret;
    for (size_t i = 0; i < ByteCount; ++i) {
        char const *src = &text[2 * i];
        auto [ptr, ec] = std::from_chars(src, src + 2, ret[i], 16);
        if (ptr != src + 2) {
            return std::nullopt;
        }
    }
    return ret;
}

std::string bytes_to_hex(std::span<uint8_t const> bytes) {
    fmt::memory_buffer buf;
    for (auto b : bytes) {
        fmt::format_to(fmt::appender(buf), "{:02x}", b);
    }
    return to_string(buf);
}

namespace {
std::filesystem::path UserCollectiveRoot() { return poe::util::data_local_root() / "Inya" / "FileCollective"; }
} // namespace

file_collective_state::file_collective_state(file_collective_info info)
    : info_(info), caches_(std::make_unique<Caches>()) {

    cache_info_.collective_root_ = info.collective_root_.value_or(UserCollectiveRoot());
    cache_info_.index_root_ = cache_info_.collective_root_ / "index";
    cache_info_.data_root_ = cache_info_.collective_root_ / "data";

    cache_info_.create_directories();

    remote_info_.meta_url_base_ = "http://10.0.1.21/poe-meta";
    remote_info_.meta_url_base_ = "https://inya.zao.se/poe-meta";
    remote_info_.index_url_base_ = "http://10.0.1.21/poe-index";
    remote_info_.data_url_base_ = "http://10.0.1.21/poe-data";

    // auto builds_url = cpr::Url(fmt::format("{}/builds.json", remote_info_.meta_url_base_));
    auto builds_url = cpr::Url(fmt::format("{}/builds/public", remote_info_.meta_url_base_));
    cpr::Response res = cpr::Get(builds_url);
    if (res.status_code < 200 || res.status_code >= 300) {
        throw std::runtime_error("could not fetch build list");
    }
    auto builds_json = nlohmann::json::parse(res.text);
    nlohmann::json build;
    if (info.build) {
        for (auto &e : builds_json) {
            if (e["build_id"] == info.build) {
                build = e;
                break;
            }
        }
    } else {
        build = builds_json.back();
        info.build = std::stoi((std::string)build["build_id"]);
    }
    if (!build.is_object()) {
        throw std::runtime_error("could not find requested build online");
    }
    uint64_t manifest_id = std::stoull(std::string(build["manifests"]["238961"]));

    load_ndjson_manifest(manifest_id);
}

file_collective_state::~file_collective_state() {}

static uint64_t RoundUp(uint64_t val, uint64_t granularity) {
    auto rem = val % granularity;
    if (rem) {
        return val - rem + granularity;
    }
    return val;
}

bool atomic_write_file(std::filesystem::path path, std::span<uint8_t const> data) {
    std::shared_ptr<uint8_t[]> alignedStorage;
    uint64_t trueSize = data.size();
    {
        alignedStorage.reset((uint8_t *)VirtualAlloc(nullptr, data.size(), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE),
                             [](void *p) { VirtualFree(p, 0, MEM_RELEASE); });
        memcpy(alignedStorage.get(), data.data(), data.size());
        SYSTEM_INFO systemInfo{};
        GetSystemInfo(&systemInfo);
        data = std::span<uint8_t const>(alignedStorage.get(), RoundUp(data.size(), systemInfo.dwPageSize));
    }
    while (true) {
        auto tmp_path = path.parent_path() / fmt::format(".{}-{}", path.filename().string(), std::random_device()());
        std::shared_ptr<void> fh(
            CreateFileW(tmp_path.wstring().c_str(), GENERIC_READ | GENERIC_WRITE, 0, nullptr, CREATE_NEW,
                        FILE_FLAG_DELETE_ON_CLOSE | FILE_FLAG_NO_BUFFERING | FILE_FLAG_WRITE_THROUGH, nullptr),
            &CloseHandle);
        if (fh.get() == INVALID_HANDLE_VALUE) {
            DWORD ec = GetLastError();
            if (ec == ERROR_FILE_EXISTS) {
                continue;
            }
            OutputDebugStringA(fmt::format("CreateFile: {}\n", ec).c_str());
            return false;
        }
        DWORD bytesWritten{};
        BOOL writeResult = WriteFile(fh.get(), data.data(), (DWORD)data.size(), &bytesWritten, nullptr);
        if (!writeResult) {
            DWORD ec = GetLastError();
            OutputDebugStringA(fmt::format("WriteFile: {}\n", ec).c_str());
            return false;
        }
        if (trueSize != data.size()) {
            FILE_END_OF_FILE_INFO trueEnd;
            trueEnd.EndOfFile.QuadPart = trueSize;
            BOOL setFileSizeResult = SetFileInformationByHandle(fh.get(), FileEndOfFileInfo, &trueEnd, sizeof(trueEnd));
            if (!setFileSizeResult) {
                DWORD ec = GetLastError();
                OutputDebugStringA(fmt::format("CreateHardLink: {}\n", ec).c_str());
                return false;
            }
        }
        BOOL flushFileResult = FlushFileBuffers(fh.get());
        if (!flushFileResult) {
            DWORD ec = GetLastError();
            OutputDebugStringA(fmt::format("FlushFileBuffers: {}\n", ec).c_str());
            return false;
        }
        BOOL hardlinkResult = CreateHardLinkW(path.wstring().c_str(), tmp_path.wstring().c_str(), nullptr);
        if (!hardlinkResult) {
            DWORD ec = GetLastError();
            if (ec == ERROR_ALREADY_EXISTS) {
                return true;
            }
            OutputDebugStringA(fmt::format("CreateHardLink: {}\n", ec).c_str());
            return false;
        }

        return true;
    }
}

struct file_collective_state::BungleFile {
    using FileHash = std::array<uint8_t, 32>;
    BungleFile(std::span<uint8_t const> data) {
        uint32_t version, fileCount;
        memcpy(&version, data.data(), 4);
        memcpy(&fileCount, data.data() + 4, 4);
        files.resize(fileCount);
        data = data.subspan(8);

        size_t const fileStride = sizeof(uint64_t) + sizeof(FileHash);
        for (size_t i = 0; i < fileCount; ++i) {
            memcpy(&files[i], data.data() + i * fileStride, fileStride);
        }
        data = data.subspan(fileCount * fileStride);

        uint32_t pathRepCount;
        memcpy(&pathRepCount, data.data(), 4);
        data = data.subspan(4);

        pathReps.resize(pathRepCount);
        size_t const pathRepStride = 8 + 4 + 4 + 4;
        for (size_t i = 0; i < pathRepCount; ++i) {
            memcpy(&pathReps[i], data.data() + i * pathRepStride, pathRepStride);
        }
        data = data.subspan(pathRepCount * pathRepStride);

        pathData.assign(data.data(), data.data() + data.size());
    }

    struct FileEntry {
        uint64_t pathHash;
        FileHash fileHash;
    };
    std::vector<FileEntry> files;

    struct PathRep {
        uint64_t dirHash;
        uint32_t offset;
        uint32_t size;
        uint32_t recursiveSize;
    };
    std::vector<PathRep> pathReps;

    std::vector<uint8_t> pathData;
};

std::optional<std::vector<uint8_t>> zstd_decompress_unsized(std::span<uint8_t const> compressed) {
    std::shared_ptr<ZSTD_DStream> ds(ZSTD_createDStream(), &ZSTD_freeDStream);
    ZSTD_initDStream(ds.get());

    std::vector<uint8_t> out(ZSTD_DStreamOutSize());
    std::vector<uint8_t> ret;

    ZSTD_outBuffer outBuf{.dst = out.data(), .size = out.size(), .pos = 0};
    ZSTD_inBuffer inBuf{.src = compressed.data(), .size = compressed.size(), .pos = 0};
    while (ZSTD_decompressStream(ds.get(), &outBuf, &inBuf)) {
        ret.insert(ret.end(), out.data(), out.data() + outBuf.pos);
        outBuf.pos = 0;
    }
    return ret;
}

void file_collective_state::load_bungle_manifest(uint64_t manifest_id) {
    manifest_id_ = manifest_id;
    auto manifest_path = cache_info_.index_root_ / fmt::format("{}.bungle.zst", manifest_id);
    std::vector<uint8_t> payload;
    if (auto fh = std::ifstream(manifest_path, std::ios::binary)) {
        payload.resize(file_size(manifest_path));
        fh.read((char *)payload.data(), payload.size());
    } else {
        cpr::Url manifest_url = fmt::format("{}/index/{}.bungle.zst", remote_info_.index_url_base_, manifest_id);
        cpr::Response res = cpr::Get(manifest_url);
        std::span<uint8_t const> data((uint8_t const *)res.text.data(), res.text.size());
        atomic_write_file(manifest_path, data);
        payload.assign(data.begin(), data.end());
    }

    auto data = zstd_decompress_unsized(payload);
    bungle_ = std::make_shared<BungleFile>(*data);
}

void file_collective_state::load_ndjson_manifest(uint64_t manifest_id) {
    manifest_id_ = manifest_id;
    auto manifest_path = cache_info_.index_root_ / fmt::format("{}-bundled.ndjson.zst", manifest_id);
    std::vector<uint8_t> payload;
    if (auto fh = std::ifstream(manifest_path, std::ios::binary)) {
        payload.resize(file_size(manifest_path));
        fh.read((char *)payload.data(), payload.size());
    } else {
        cpr::Url manifest_url =
            fmt::format("{}/{}/{}-bundled.ndjson.zst", remote_info_.index_url_base_, 238961, manifest_id);
        cpr::Response res = cpr::Get(manifest_url);
        if (res.status_code < 200 || res.status_code >= 300) {
            throw std::runtime_error(
                fmt::format("Could not obtain index {}: code {}", manifest_url.c_str(), res.status_code));
        }
        std::span<uint8_t const> data((uint8_t const *)res.text.data(), res.text.size());
        atomic_write_file(manifest_path, data);
        payload.assign(data.begin(), data.end());
    }

    index_ = std::make_shared<FileIndex>();
    using EntryChunkPtr = std::shared_ptr<std::vector<IndexEntry, tbb::scalable_allocator<IndexEntry>>>;
    tbb::concurrent_queue<EntryChunkPtr> entryQueue;
    int pendingEntries = 0;
    std::vector<std::string> lines;
    enumerate_zstd_lines(payload, [&](std::string_view line) {
        lines.emplace_back(line);
        ++pendingEntries;
    });
    tbb::parallel_for(tbb::blocked_range<size_t>(0, lines.size()),
                      [&lines, &entryQueue](tbb::blocked_range<size_t> const &r) {
                          auto retChunk = std::make_shared<EntryChunkPtr::element_type>();
                          retChunk->reserve(r.size());
                          for (auto i = r.begin(); i != r.end(); ++i) {
                              auto &line = lines[i];
                              auto rootJson = json_loads(line.c_str(), 0, nullptr);
                              auto pathHashJson = json_object_get(rootJson, "phash");
                              auto pathJson = json_object_get(rootJson, "path");
                              auto fileHashJson = json_object_get(rootJson, "sha256");
                              auto fileSizeJson = json_object_get(rootJson, "size");
                              auto compJson = json_object_get(rootJson, "comp");
                              auto pathHash = std::strtoull(json_string_value(pathHashJson), nullptr, 10);
                              auto path = (char8_t const *)json_string_value(pathJson);
                              auto fileHash = *hex_to_array<32>(json_string_value(fileHashJson));
                              auto fileSize = (uint32_t)json_number_value(fileSizeJson);
                              auto comp = json_boolean_value(compJson);
                              IndexEntry entry{
                                  .path_hash = pathHash,
                                  .path = path,
                                  .file_hash = fileHash,
                                  .file_size = fileSize,
                                  .compressed = comp,
                              };
                              json_decref(rootJson);
                              retChunk->push_back(std::move(entry));
                          }
                          entryQueue.push(retChunk);
                      });

    index_->entries.reserve(pendingEntries);
    EntryChunkPtr chunk;
    while (pendingEntries) {
        if (entryQueue.try_pop(chunk)) {
            size_t const start = index_->entries.size();
            size_t const n = chunk->size();
            std::ranges::move(*chunk, std::back_inserter(index_->entries));
            for (auto &entry : std::span(index_->entries).subspan(start, n)) {
                absl::string_view pathView((char const *)entry.path.c_str());
                index_->byPath.insert({pathView, &entry});
                index_->byPathHash.insert({entry.path_hash, &entry});
            }
            pendingEntries -= (int)n;
        }
    }
}

struct collective_file_handle : file_handle {
    using DataVec = std::vector<uint8_t, tbb::scalable_allocator<uint8_t>>;
    collective_file_handle(DataVec &&data) : data_(std::move(data)) {}

    std::vector<std::byte> read_all() const {
        auto p = (std::byte const *)data_.data();
        return std::vector<std::byte>(p, p + data_.size());
    }

    uint64_t size() const { return data_.size(); }

    DataVec data_;
};

bool decompress_zstd_istream(std::istream &is, std::span<uint8_t> data, ZSTD_DStream *reusedCtx = nullptr) {
    std::shared_ptr<ZSTD_DStream> ds;
    if (reusedCtx) {
        ds.reset(reusedCtx, [](auto) {});
    } else {
        ds.reset(ZSTD_createDStream(), &ZSTD_freeDStream);
    }
    ZSTD_initDStream(ds.get());

    std::vector<uint8_t, tbb::scalable_allocator<uint8_t>> in_data(ZSTD_DStreamInSize());
    ZSTD_outBuffer outbuf{.dst = data.data(), .size = data.size(), .pos = 0};
    ZSTD_inBuffer inbuf{.src = in_data.data(), .size = 0, .pos = 0};

    while (true) {
        if (!is) {
            return false;
        }

        // Top up input buffer
        auto in_tail = std::span<uint8_t>(in_data).subspan(inbuf.pos);
        is.read((char *)in_tail.data(), in_tail.size());
        inbuf.size += is.gcount();

        // Decompress as much as we can
        size_t rc = ZSTD_decompressStream(ds.get(), &outbuf, &inbuf);
        if (!rc) {
            return true;
        }

        // Shift remaining input data to the front
        memmove(in_data.data(), in_data.data() + inbuf.pos, inbuf.size - inbuf.pos);
        inbuf.size -= inbuf.pos;
        inbuf.pos = 0;
    }
    return false;
}

std::shared_ptr<file_handle> file_collective_state::open_file(poe::util::path path) {
    auto path_hash = file_path_hash(path);
    if (auto I = index_->byPathHash.find(path_hash); I != index_->byPathHash.end()) {
        auto &entry = *I->second;
        auto file_size = entry.file_size;
        auto hash_str = bytes_to_hex(entry.file_hash);
        std::string local_filename, remote_filename;
        if (entry.compressed) {
            local_filename = fmt::format("{}/{}.bin.zst", hash_str.substr(0, 2), hash_str);
        } else {
            local_filename = fmt::format("{}/{}.bin", hash_str.substr(0, 2), hash_str);
        }
        remote_filename = local_filename;
        auto disk_path = cache_info_.data_root_ / local_filename;
        collective_file_handle::DataVec data(file_size);
        for (int retry = 0; retry < 5; ++retry) {
            if (auto fh = std::ifstream(disk_path, std::ios::binary)) {
                if (entry.compressed) {
                    auto &dStream = caches_->dStreams.local();
                    if (!decompress_zstd_istream(fh, data, dStream.get())) {
                        std::this_thread::sleep_for(0.5s);
                        continue;
                    }
                } else {
                    if (!fh.read(reinterpret_cast<char *>(data.data()), data.size())) {
                        std::this_thread::sleep_for(0.5s);
                        continue;
                    }
                }
            } else {
                cpr::Url file_url = fmt::format("{}/{}", remote_info_.data_url_base_, remote_filename);
                auto session = caches_->cprSessions.local();
                session->SetUrl(file_url);
                cpr::Response res = session->Get();
                if (res.status_code < 200 || res.status_code >= 300) {
                    LOG_F(WARNING, "HTTP non-success {}: {}: {} \"{}\"", res.status_code, res.error.message, hash_str,
                          path.path_);
                    std::this_thread::sleep_for(5s);
                    continue;
                }
                std::span<uint8_t const> source_data((uint8_t const *)res.text.data(), res.text.size());
                atomic_write_file(disk_path, source_data);
                LOG_F(INFO, "Fetched {} \"{}\": {} bytes", hash_str, path.path_, source_data.size());
                if (entry.compressed) {
                    ZSTD_decompress(data.data(), data.size(), source_data.data(), source_data.size());
                } else {
                    data.assign(source_data.begin(), source_data.end());
                }
            }
            auto ret = std::make_shared<collective_file_handle>(std::move(data));
            return ret;
        }
    }
    return nullptr;
}

void file_collective_state::enumerate_files(std::function<void(std::string_view path)> op) {
    for (auto &[path, entry] : index_->byPath) {
        op({path.data(), path.size()});
    }
}
} // namespace poe::io