#pragma once

#include <cstddef>
#include <cstdint>
#include <vector>

namespace poe::io {
struct file_handle {
    virtual ~file_handle() = default;
    virtual uint64_t size() const = 0;
    virtual std::vector<std::byte> read_all() const = 0;
};
} // namespace poe::io