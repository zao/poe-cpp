#pragma once

#include <iterator>
#include <vector>

#include <glm/vec2.hpp>

namespace pog {
template <typename T, typename I> struct GridIterator {
    using iterator_category = std::forward_iterator_tag;
    using difference_type = std::intptr_t;
    using value_type = std::pair<glm::ivec2, T *>;
    using pointer = value_type const *;
    using reference = value_type const &;

    GridIterator() {}
    explicit GridIterator(I inner, glm::ivec2 coord, int stride) : inner_(inner), stride_(stride), cur_(coord) {
        SetLast();
    }

    reference operator*() const { return last_; }

    pointer operator->() const { return &**this; }

    difference_type operator-(GridIterator const &rhs) const { return LinearIndex() - rhs.LinearIndex(); }

    GridIterator &operator++() {
        ++inner_;
        ++cur_.x;
        cur_.y += cur_.x / stride_;
        cur_.x %= stride_;
        SetLast();
        return *this;
    }

    GridIterator operator++(int) {
        GridIterator ret = *this;
        ++*this;
        return ret;
    }

    bool operator==(GridIterator const &rhs) const { return cur_ == rhs.cur_; }

    bool operator!=(GridIterator const &rhs) const { return cur_ != rhs.cur_; }

  private:
    void SetLast() { last_ = {cur_, &*inner_}; }

    difference_type LinearIndex() const { return cur_.x + cur_.y * stride_; }

    I inner_{};
    glm::ivec2 cur_{};
    int stride_{};
    value_type last_{};
};

template <typename Elem> struct Grid {
    Grid() = default;
    explicit Grid(glm::ivec2 size) : size_(size), elems_(size.x * size.y) {}

    Elem const *operator[](glm::ivec2 coord) const {
        if (coord.x < 0 || coord.x >= size_.x || coord.y < 0 || coord.y >= size_.y) {
            return {};
        }
        return &elems_[coord.x + coord.y * size_.x];
    }
    Elem *operator[](glm::ivec2 coord) {
        if (coord.x < 0 || coord.x >= size_.x || coord.y < 0 || coord.y >= size_.y) {
            return {};
        }
        return &elems_[coord.x + coord.y * size_.x];
    }

    glm::ivec2 Size() const { return size_; }

    glm::ivec2 size_{0, 0};
    std::vector<Elem> elems_;

    using Iterator = GridIterator<Elem, typename std::vector<Elem>::iterator>;

    Iterator begin() { return Iterator(elems_.begin(), {0, 0}, size_.x); }
    Iterator end() { return Iterator(elems_.begin(), {0, size_.y}, size_.x); }
};
} // namespace pog