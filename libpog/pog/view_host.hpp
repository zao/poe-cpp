#pragma once

#include <pog/dx_context.hpp>

#include <glm/vec4.hpp>

namespace pog {
struct Resizable {
    virtual void Resize(glm::ivec2 size) = 0;
};

struct ViewHost : Resizable {
    explicit ViewHost(pog::DxContext const &dx, HWND wnd);

    void Resize(glm::ivec2 size) override;

    void Clear();
    void BlendTarget(ID3D11ShaderResourceView *srcSrv);
    void ResolveTarget(ID3D11Texture2D *rt);
    void Present() { resDep_.swapChain->Present(1, 0); }

    glm::vec4 clearColor_{};

  private:
    void HandleResize(glm::ivec2 newSize);

    pog::DxContext const &dx_;
    HWND wnd_{};
    glm::ivec2 fbSize_{};

    struct ResDependent {
        CComPtr<IDXGISwapChain> swapChain;
        CComPtr<ID3D11Texture2D> rt;
        CComPtr<ID3D11RenderTargetView> rtv;
    };

    ResDependent resDep_;

    CComPtr<ID3D11Buffer> fsGeom_;
    UINT fsStride_{}, fsOffset_{};

    CComPtr<ID3D11VertexShader> blendVs_;
    CComPtr<ID3D11PixelShader> blendPs_;
    CComPtr<ID3D11InputLayout> blendFsLayout_;
    CComPtr<ID3D11SamplerState> blendSampler_;
    CComPtr<ID3D11BlendState> rtBlend_;
};

struct LayerTarget : Resizable {
    LayerTarget(pog::DxContext const &dx);

    void Resize(glm::ivec2 size) override;
    glm::ivec2 Size() const;

    CComPtr<ID3D11DepthStencilState> dss;

    CComPtr<ID3D11Texture2D> rt;
    CComPtr<ID3D11Texture2D> depth;
    CComPtr<ID3D11RenderTargetView> rtv;
    CComPtr<ID3D11DepthStencilView> dsv;
    CComPtr<ID3D11ShaderResourceView> rtSrv;

  private:
    pog::DxContext const &dx_;
    glm::ivec2 fbSize_{};
};
} // namespace pog