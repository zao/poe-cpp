#pragma once

#include <memory>
#include <optional>
#include <string_view>

#include <absl/container/flat_hash_map.h>
#include <absl/container/inlined_vector.h>
#include <glm/vec2.hpp>

#include "game_data.hpp"
#include "grid.hpp"

#include <poe/format/arm.hpp>

namespace pog {
struct Room;

namespace arm_ns = poe::format::arm;

struct RoomInfo {
    struct EdgeConstraint {
        TileSide side;
        std::optional<std::string> name;
        int real, virt;
    };

    struct GroundConstraint {
        TileCorner corner;
        std::optional<std::string> name;
    };

    struct HeightConstraint {
        TileCorner corner;
        int height;
    };

    struct EntryInfo {
        EntryInfo() {}
        EntryInfo(std::shared_ptr<arm_ns::ARMFile const> arm, std::shared_ptr<arm_ns::Entry const> entry,
                  glm::ivec2 coord);

        arm_ns::FEntry const *AsF() const { return std::get_if<arm_ns::FEntry>(&entry->var); }
        arm_ns::KEntry const *AsK() const { return std::get_if<arm_ns::KEntry>(&entry->var); }
        arm_ns::NEntry const *AsN() const { return std::get_if<arm_ns::NEntry>(&entry->var); }
        arm_ns::OEntry const *AsO() const { return std::get_if<arm_ns::OEntry>(&entry->var); }
        arm_ns::SEntry const *AsS() const { return std::get_if<arm_ns::SEntry>(&entry->var); }

        std::optional<std::string> TagConstraint() const;

        absl::InlinedVector<EdgeConstraint, 4> EdgeConstraints() const;
        absl::flat_hash_map<TileSide, EdgeConstraint> EdgeConstraintAssoc() const;

        absl::InlinedVector<GroundConstraint, 4> GroundConstraints() const;
        absl::flat_hash_map<TileCorner, GroundConstraint> GroundConstraintAssoc() const;

        absl::InlinedVector<HeightConstraint, 4> CornerHeights() const;
        absl::flat_hash_map<TileCorner, HeightConstraint> CornerHeightAssoc() const;

        glm::ivec2 CoordMin() const;
        glm::ivec2 CoordMax() const;

        char KindChar() const;

        std::shared_ptr<arm_ns::ARMFile const> arm;
        std::shared_ptr<poe::format::arm::Entry const> entry;
        glm::ivec2 coord{};
        glm::ivec2 size{};
        TileCorner originCorner{};
    };

    explicit RoomInfo(std::shared_ptr<poe::format::arm::ARMFile> arm);

    glm::ivec2 RoomSize() const;

    std::optional<glm::ivec2> ResolveCoord(glm::ivec2 coord) const;

    std::shared_ptr<poe::format::arm::ARMFile> arm;
    std::map<glm::ivec2, EntryInfo, GlmLess> rootEntries;
    std::map<glm::ivec2, glm::ivec2, GlmLess> proxyEntries;
};

struct TileSlot {
    glm::ivec2 size;
    std::optional<TileKey> key;
    std::optional<TileData> usedTile;
};

namespace arm_ns = poe::format::arm;

enum RoomDesignFlags {
    RoomDesignFlag_None = 0,
    RoomDesignFlag_StoreRejectionReasons = 1,
};

struct RoomDesign {
    struct Choice {
        std::string path;
        std::shared_ptr<poe::format::tdt::Tdt> tile;
        TileMutation mutation; // rotation and flipping
    };

    struct RejectedCandidate {
        Choice candidate;
        std::string reason;
    };

    struct Slot {
        RoomInfo::EntryInfo const *info;
        std::optional<Choice> choice;
        std::vector<Choice> candidates;
        std::vector<RejectedCandidate> rejects;
    };

    RoomDesign(std::shared_ptr<RoomInfo const> room, std::shared_ptr<GameData> gameData,
               std::shared_ptr<GraphAssets> graph, RoomDesignFlags flags = RoomDesignFlag_None);

    std::map<TileMutation, TileKey> TileKeyMutations(TileKey baseKey);

    struct SlotFitResult {
        bool success{true};
        std::string failureReason;

        explicit operator bool() const { return success; }
    };

    SlotFitResult FitsInSlot(TileKey const &tile, RoomInfo::EntryInfo const &slot);
    void BuildCandidateList(Slot &slot);

    std::shared_ptr<RoomInfo const> room_;
    std::shared_ptr<GameData> gameData_;
    std::shared_ptr<GraphAssets> graph_;
    RoomDesignFlags flags_{RoomDesignFlag_None};
    std::map<glm::ivec2, Slot, GlmLess> slots_;
};

struct Room {
    explicit Room(std::shared_ptr<poe::format::arm::ARMFile> arm);

    std::shared_ptr<poe::format::arm::ARMFile> arm_;
    glm::ivec2 size_;
    int storeyHeight_;
    std::map<glm::ivec2, pog::TileSlot, GlmLess> slots_;
    std::map<glm::ivec2, char, GlmLess> slotKind_;
};
} // namespace pog