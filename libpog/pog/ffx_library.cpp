#include <pog/ffx_library.hpp>

#include <poe/format/ffx.hpp>

namespace pog {
FFXLibrary::FFXLibrary(std::shared_ptr<poe::io::vfs> vfs) : vfs_(vfs) { Populate(); }

poe::format::ffx::Declarations const *FFXLibrary::LookupDeclarations(std::string_view name) const {
    for (auto const &[_, ffx] : parsed_ffx_files_) {
        if (auto *decl = ffx->FindDeclarations(name)) {
            return decl;
        }
    }
    return nullptr;
}

poe::format::ffx::Fragment const *FFXLibrary::LookupFragment(std::string_view name) const {
    for (auto const &[_, ffx] : parsed_ffx_files_) {
        if (auto *frag = ffx->FindFragment(name)) {
            return frag;
        }
    }
    return nullptr;
}

void FFXLibrary::Populate() {
    vfs_->enumerate_files([&](poe::util::path const &path) {
        if (path.extension() == ".ffx") {
            auto fh = vfs_->open_file(path);
            parsed_ffx_files_[path.path_] = poe::format::ffx::parse_ffx(fh->read_all());
        }
    });
    std::map<std::string, std::u16string> mockFallbacks{
        {
            "Player",
            uR"(
FRAGMENT PenanceBrandChargeCount
            cost: free
            out float output
{{
            output = 0.0;
}}

FRAGMENT TrackedKills
            cost: free
            out float output
{{
            output = 0.0;
}}

FRAGMENT VoidBurstTimer
            cost: free
            out float output
{{
            output = 0.0;
}}

FRAGMENT WinterBrandIntensity
            cost: free
            out float output
{{
            output = 0.0;
}}
)",
        },
        {
            "Spatial",
            uR"(
FRAGMENT AttachedObjectSeed
            cost: free
            out float output
{{
            output = 0.0;
}}

FRAGMENT LocalPlayerPosition
            cost: free
            out float3 output
{{
            output = player_position.xyz - world_transform[3].xyz;
}}

FRAGMENT NearestPlayerPosition
            cost: free
            out float3 output
{{
            output = player_position.xyz;
}}

FRAGMENT ObjectBoundingBox
            cost: free
            out float3 output
{{
            output = float3(250.0, 250.0, 250.0);
}}

FRAGMENT ObjectSeed
            cost: free
            out float output
{{
            output = 0;
}}

FRAGMENT ObjectTransform
            cost: free
            out float4x4 output
{{
            output = world_transform;
}}

FRAGMENT PlayerTrailingGhost
            cost: free
            out float3 output
{{
            output = player_position.xyz;
}}

FRAGMENT SpecificObjectPosition
            cost: free
            out float3 output
{{
            output = float3(0.0, 0.0, 0.0);
}}
)",
        },
        {
            "League",
            uR"(
FRAGMENT AfflictionOrigin
            cost: free
            out float3 output
{{
            output = float3(0.0, 0.0, 0.0);
}}

FRAGMENT BannerStages
            cost: free
            out float output
{{
            output = 0.0;
}}          

FRAGMENT CurrentBossPosition
            cost: free
            out float3 output
{{
            output = float3(0.0, 0.0, 0.0);
}}
            
FRAGMENT HarvestFluidAmount
            cost: free
            out float output
{{
            output = 0.0;
}}

FRAGMENT MavenCreationProgress
            cost: free
            out float output
{{
            output = 0.0;
}}

FRAGMENT MorphRatio
            cost: free
            out float output
{{
            output = 0.0;
}}

FRAGMENT OshabiPhaseColour
            cost: free
            out float3 output
{{
            output = float3(1.0, 1.0, 1.0);
}}

FRAGMENT RitualSacrificePercent
            cost: free
            out float output
{{
            output = 0.0;
}}

FRAGMENT UltimatumOrigin
            cost: free
            out float3 output
{{
            output = float3(0.0, 0.0, 0.0);
}}

FRAGMENT UltimatumTimeStopProgress
            cost: free
            out float output
{{
            output = 0.0;
}}
)",
        },
    };
    for (auto &[name, text] : mockFallbacks) {
        auto data = std::as_bytes(std::span(text));
        parsed_ffx_files_["Mock/" + name + ".ffx"] = poe::format::ffx::parse_ffx(data);
    }
}
} // namespace pog