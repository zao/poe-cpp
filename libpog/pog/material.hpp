#pragma once

#include <poe/format/ffx.hpp>
#include <poe/format/mat.hpp>
#include <poe/io/vfs.hpp>
#include <pog/ffx_library.hpp>

#include <atlcomcli.h>
#include <d3dcommon.h>

#include <map>
#include <set>

namespace pog {
struct Material {
    Material(std::shared_ptr<poe::io::vfs> vfs, std::shared_ptr<pog::FFXLibrary> ffxLib, std::string path);

    std::shared_ptr<poe::io::vfs> vfs_;
    std::string path_;
    std::shared_ptr<poe::format::mat::MatFileV4> mat_;
};

struct MaterialThingie {
    CComPtr<ID3DBlob> vsBlob, psBlob;
};

struct MaterialLibrary {
    MaterialLibrary(std::shared_ptr<poe::io::vfs> vfs, std::shared_ptr<pog::FFXLibrary> ffx) : vfs_(vfs), ffx_(ffx) {
        vertexStages_.assign({"VertexInit", "LocalTransform_Init", "LocalTransform", "LocalTransform_Calc",
                              "LocalTransform_Final", "WorldTransform_Init", "WorldTransform", "WorldTransform_Calc",
                              "WorldTransform_Final", "VertexOutput_Init", "VertexOutput", "VertexOutput_Calc",
                              "VertexOutput_Final"});
        pixelStages_.assign({
            "PixelInit",         "UVSetup_Init",       "UVSetup",           "UVSetup_Calc",      "UVSetup_Final",
            "Texturing_Init",    "Texturing",          "Texturing_Calc",    "Texturing_Final",   "PreLighting_Init",
            "PreLighting",       "PreLighting_Calc",   "PreLighting_Final", "Lighting_Init",     "Lighting",
            "Lighting_Calc",     "Lighting_Final",     "CustomLighting",    "PostLighting_Init", "PostLighting",
            "PostLighting_Calc", "PostLighting_Final", "LightingEnd",       "PixelOutput_Init",  "PixelOutput",
            "PixelOutput_Calc",  "PixelOutput_Final",
        });
    }

    std::shared_ptr<MaterialThingie> MakeMaterialThingie(poe::util::path baseGraphPath, poe::util::path matPath);

    using StageIdx = size_t;
    using NodeIdx = size_t;

    struct CachedGraph {
        std::shared_ptr<poe::format::fxgraph::FxGraph> fxg;

        std::map<StageIdx, std::vector<NodeIdx>> sortedNodesPerStage;
    };

    struct CachedMaterial {
        std::shared_ptr<poe::format::mat::MatFileV4> mat;

        // deduplicated and out of order from graph instance list
        std::map<std::string, CachedGraph> usedGraphs;
    };

    std::optional<CachedGraph> LoadGraph(std::shared_ptr<poe::format::fxgraph::FxGraph> fxg);
    std::optional<CachedGraph> LoadGraph(poe::util::path const &path);
    std::optional<CachedMaterial> LoadMaterial(poe::util::path const &path);

    std::optional<StageIdx> LookupStage(std::string_view name) const {
        if (auto I = std::ranges::find(vertexStages_, name); I != vertexStages_.end()) {
            return std::distance(vertexStages_.begin(), I);
        }
        if (auto I = std::ranges::find(pixelStages_, name); I != pixelStages_.end()) {
            return std::distance(pixelStages_.begin(), I);
        }
        return std::nullopt;
    }

  private:
    std::optional<CachedGraph> LoadGraph(nlohmann::json const &js);

    std::shared_ptr<poe::io::vfs> vfs_;
    std::shared_ptr<pog::FFXLibrary> ffx_;
    std::vector<std::string> vertexStages_, pixelStages_;
    std::map<std::string, CachedGraph> graphCache_;
    std::map<std::string, CachedMaterial> materialCache_;
};

struct FxGraphGraph {
    explicit FxGraphGraph(poe::format::fxgraph::FxGraph &fxg, std::shared_ptr<FFXLibrary> ffxLib,
                          MaterialLibrary &matLib)
        : fxg(fxg), nodeCount(fxg.nodes.size()), ffxLib(ffxLib), nodeFragments(nodeCount), nodeStageIndex(nodeCount),
          nodeIsRead(nodeCount), nodeIsWrite(nodeCount) {
        // Build a lookup table from node key to node index to make edge building cheaper.
        // Nodes with fully duplicate keys will be unreachable but those seem to not have edges anyway.
        for (size_t nodeIdx = 0; nodeIdx < nodeCount; ++nodeIdx) {
            auto &node = fxg.nodes[nodeIdx];
            auto &key = node.key;
            nodeLookup[key] = nodeIdx;
            auto frag = nodeFragments[nodeIdx] = ffxLib->LookupFragment(key.type);
            if (frag) {
                if (key.stage) {
                    nodeStageIndex[nodeIdx] = matLib.LookupStage(*key.stage);
                }
                nodeIsRead[nodeIdx] = frag->HasKeyword("read");
                nodeIsWrite[nodeIdx] = frag->HasKeyword("write");
            }
        }

        // Build up edge map with indices instead of keys
        for (auto &link : fxg.links) {
            auto srcIdx = nodeLookup.at(link.src.key);
            auto dstIdx = nodeLookup.at(link.dst.key);
            edges[srcIdx].insert(dstIdx);
            reverseEdges[dstIdx].insert(srcIdx);
        }
    }

    poe::format::fxgraph::FxGraph &fxg;
    size_t nodeCount;
    std::map<poe::format::fxgraph::FxGraphKey, size_t> nodeLookup;
    std::map<size_t, std::set<size_t>> edges;
    std::map<size_t, std::set<size_t>> reverseEdges;

    std::shared_ptr<FFXLibrary> ffxLib; // keeping the fragment pointers attributes alive

    // Original attributes
    std::vector<poe::format::ffx::Fragment const *> nodeFragments;
    std::vector<std::optional<size_t>> nodeStageIndex;
    std::vector<bool> nodeIsRead, nodeIsWrite;

    // TODO(LV): Nail down whether edges/reverseEdges can have empty sets.
    bool IsSource(size_t nodeIdx) const { return !reverseEdges.contains(nodeIdx) || reverseEdges.at(nodeIdx).empty(); }
    bool IsSink(size_t nodeIdx) const { return !edges.contains(nodeIdx) || edges.at(nodeIdx).empty(); }
};
} // namespace pog