#pragma once

#include <future>
#include <pog/drawer.hpp>
#include <pog/scene_view_controller.hpp>

#include <memory>
#include <pog/dx_context.hpp>

#include <absl/container/node_hash_map.h>
#include <absl/container/inlined_vector.h>
#include <absl/hash/hash.h>

#include <nlohmann/json.hpp>

#include <poe/format/ao.hpp>
#include <poe/format/tgm.hpp>
#include <poe/format/tgt.hpp>

#include <memory>

namespace poe::format::mat {
struct Material {
    nlohmann::json js;
};
} // namespace poe::format::mat

namespace pog {
struct GraphAssets;
struct MultiTileInfoWindow;

using ActorId = uint64_t;
struct ActorInstance {
    std::shared_ptr<poe::format::ao::ao_parse> aoc;
};

struct SmallTile {
    std::shared_ptr<poe::format::tgt::Tgt> tgt;
    std::shared_ptr<poe::format::tgm::TgmFile> tgm;
    absl::InlinedVector<std::shared_ptr<poe::format::mat::Material>, 4> materials;
};

struct BigTile {
    std::shared_ptr<poe::format::tdt::Tdt> tdt;
    std::vector<SmallTile> smallTiles;
};

struct LevelInstance {
    ActorId nextActorId_{};

    absl::node_hash_map<ActorId, ActorInstance> actors;
    absl::node_hash_map<glm::ivec2, glm::ivec2, GlmHash> rootTileProxies;
    absl::node_hash_map<glm::ivec2, std::shared_ptr<BigTile>, GlmHash> rootTiles;
};

struct LevelViewController : SceneViewController {
    explicit LevelViewController(std::shared_ptr<DxContext> dx);

    void Update(FrameUpdate const &update, std::shared_ptr<GameData> gameData) override;

    void Draw(LayerTarget const &rt) override;

    void ChangeLevel(std::shared_ptr<LevelInstance> level);

  private:
    std::shared_ptr<GameData> gameData_;
    std::shared_future<std::shared_ptr<GraphAssets>> pendingGraph_;
    std::shared_ptr<GraphAssets> graph_;

    std::shared_ptr<LevelInstance> level_;

    LookatCamera cam;
    glm::mat4 viewMat_{1.0f}, projMat_{1.0f};

    ScaffoldDrawer scaffoldDrawer_;
};
} // namespace pog
