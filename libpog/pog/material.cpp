#include <atlcomcli.h>
#include <poe/util/install_location.hpp>
#include <pog/ffx_library.hpp>
#include <pog/material.hpp>

#include <d3dcompiler.h>
#include <fstream>
#include <queue>

using namespace std::string_view_literals;

namespace ffx_ns = poe::format::ffx;

namespace pog {
Material::Material(std::shared_ptr<poe::io::vfs> vfs, std::shared_ptr<pog::FFXLibrary> ffxLib, std::string path)
    : vfs_(vfs), path_(path) {
    auto fh = vfs->open_file(poe::util::path(path));
    if (!fh) {
        throw std::runtime_error(fmt::format("Material {} not found", path));
    }
    auto data = fh->read_all();
    mat_ = poe::format::mat::parse_mat(data);
    std::filesystem::path vsPath = poe::util::AssetPath("../assets/VertexTemplate.hlsl");
    std::filesystem::path psPath = poe::util::AssetPath("../assets/PixelTemplate.hlsl");
    std::vector<char> vsSource, psSource;
    {
        std::ifstream is(vsPath);
        vsSource.resize(file_size(vsPath));
        is.read(vsSource.data(), vsSource.size());
    }
    {
        std::ifstream is(psPath);
        psSource.resize(file_size(psPath));
        is.read(psSource.data(), psSource.size());
    }
    std::vector<D3D_SHADER_MACRO> macros;
    UINT flags = D3DCOMPILE_DEBUG;
    HRESULT hr = S_OK;

    struct FakeInclude : ID3DInclude {
        FakeInclude(std::shared_ptr<pog::FFXLibrary> ffxLib) : ffxLib_(ffxLib) {}

        std::shared_ptr<pog::FFXLibrary> ffxLib_;

        HRESULT __stdcall Open(D3D_INCLUDE_TYPE includeType, char const *fileName, void const *parentData,
                               void const **outDataPtr, UINT *outSize) {
            std::optional<std::string> src;
            std::string_view fileBase = std::string_view(fileName);
            std::filesystem::path filePath = fileName;
            if (auto ext = filePath.extension(); ext == ".declarations"sv) {

            } else if (ext == ".fragment") {
            }
            if (!src) {
                if ("GenTopDefines.inc"sv == fileName) {
                    src = R"(
#define VERTEX_OUTPUT_TEXCOORD1
#define VERTEX_OUTPUT_POSITION
#define VERTEX_OUTPUT_TBN
#define VERTEX_OUTPUT_COLOR0
#define VERTEX_OUTPUT_COLOR1
#define VERTEX_OUTPUT_TEXCOORD2
#define VERTEX_OUTPUT_TEXCOORD3
#define VERTEX_OUTPUT_TEXCOORD4
#define VERTEX_OUTPUT_TEXCOORD8
#define VERTEX_OUTPUT_TEXCOORD9
#define VERTEX_OUTPUT_SEED
#define VERTEX_OUTPUT_UV_SET_2
#define VERTEX_OUTPUT_MODEL_ORIGIN
#define GGX

#define TEXTURE2D_DECL(Name) Texture2D Name
#define STRUCTURED_BUFFER_DECL(Name, Type) StructuredBuffer<Type> Name
#define SAMPLER_DECL(Name) SamplerState Name
#define SAMPLER_CMPDECL(Name) SamplerComparisonState Name
#define SAMPLE_TEX2DLOD(Tex, Smp, Loc) Tex.SampleLevel(Smp, (Loc).xy, (Loc).z, (Loc).w)
)";
                } else if ("GenConstantBuffers.inc"sv == fileName) {
                    src = R"(
cbuffer CBPass : register(b0) {
	float4x4 view_projection_transform;
	float4x4 view_projection_transform_inv;
	float4x4 view_projection_transform_nopost;
	float4x4 view_transform;
	float4x4 view_transform_inv;
	float4 player_position;
	float3 camera_position;
	float4 camera_right_vector;
	float4 camera_up_vector;
	float4 camera_forward_vector;
	float4 specular_cube_size;
	float4 shadow_samples;
	float4 sphere_info;
	float4 cube_brightness;
	float4x4 env_map_rotation;
	float4 light_lookup_size;
	float4 water_color_terrain;
	float4 water_color_open;
	float4 water_wind_direction;
	float water_subsurface_scattering;
	float water_refraction_index;
	float water_reflectiveness;
	float water_caustics_mult;
	float water_wind_intensity;
	float water_wind_speed;
	float water_directionness;
	float water_swell_intensity;
	float water_swell_height;
	float water_swell_angle;
	float water_swell_period;
	float water_flow_intensity;
	float water_flow_foam;
	float water_dispersion;
	float water_clarity;
	float water_uv_freq_mult;
	float water_uv_time_mult;
	float4x4 projection_matrix;
	float4x4 inv_projection_matrix;
	float4x4 view_matrix;
	float4x4 inv_view_matrix;
	float4 ambient_light_color;
	float4 ambient_light_dir;
	float4 scene_planar_size;
	float numerical_normal;
	float current_time;
	float sum_longitude;
	float dist_to_color;
	float flow_to_color;
	bool water_use_downsampling; // discards 100% transparent fragments
	bool water_debug;
	float depth_sampler_type;
	float4 shoreline_coord_tex_size;
	float4 heightmap_world_size;
	float4 heightmap_tex_size;
	float4 burn_tex_size;
	float4 blight_tex_size;
	float4 affliction_pos;
	float4 affliction_radii;
	float4 infinite_hunger_arena_water_level;
	float4 frame_resolution;
	float4 frame_to_dynamic_scale;
	float rain_intensity;
	float4 rain_fall_dir;
	float rain_turbulence;
	float rain_amount;
	float rain_dist;
	float clouds_scale;
	float clouds_intensity;
	float clouds_midpoint;
	float clouds_sharpness;
	float4 clouds_velocity;
	float clouds_fade_radius;
	float clouds_pre_fade;
	float clouds_post_fade;
	int pass_downscale;
	int pass_type;
	float time;
	float tool_time;
	float4 ground_scalemove_uv;
	float4 dust_color;
	float direct_light_env_ratio;
	float depth_bias_scale;
	bool diffuse_cube_enabled;
	bool specular_cube_enabled;
	bool fog_exp_enabled;
	bool fog_linear_enabled;
	float4 fog_vals_exp;
	float4 fog_vals_linear;
	float3 fog_colour_exp;
	float3 fog_colour_linear;
	float ssgi_intensity;
	float ssgi_debug;
	bool  ssgi_detail;
	float exposure;
	float roof_fade;
}

cbuffer CBPipe : register(b1) {
    float4 alpha_test_desc;
	bool use_double_sided;
}

cbuffer CBObject : register(b2) {
    float4x4 world_transform;
	float start_time;
	float flip_tangent;
    float4 material_specular_color;
    float material_glossiness;
    float material_ambient_occlusion;
    float material_fresnel;
    float material_emission;
    float material_energy_conservation;
    float subsurface_scattering_intensity;
    float glossiness_exponent;
    float anisotropic_glossiness_roughness;
    float anisotropic_glossiness_contrast;
    float anisotropic_glossiness_fade;
    float4 subsurface_color;
}
)";
                } else if ("GenTextures.inc"sv == fileName) {
                    src = R"(
StructuredBuffer<Light> lights : register(t0);
Texture2D tex_AlbedoTransparency_TEX : register(t1);
Texture2D tex_NormalGlossAO_TEX : register(t2);
Texture2D tex_SpecularColour_TEX : register(t3);
Texture2D tex_Glow_TEX : register(t4);

SamplerState smp_AlbedoTransparency_TEX : register(s0);
SamplerState smp_NormalGlossAO_TEX : register(s1);
SamplerState smp_SpecularColour_TEX : register(s2);
#define smp_Glow_TEX smp_AlbedoTransparency_TEX
)";
                }
            }
            if (!src) {
                // for dev, try to fall back to deployed or build dir includes
                std::ifstream is;
                auto diskPath = poe::util::AssetPath(fileName);
                is.open(diskPath, std::ios::binary);
                if (!is) {
                    diskPath = poe::util::AssetPath(fmt::format("../assets/{}", fileName));
                    is.open(diskPath, std::ios::binary);
                }
                if (is) {
                    size_t size = file_size(diskPath);
                    std::vector<char> buf(size);
                    is.read(buf.data(), buf.size());
                    src = std::string(buf.begin(), buf.end());
                }
            }
            if (src) {
                auto buf = new char[src->size()];
                std::ranges::copy(src->begin(), src->end(), buf);
                *outDataPtr = buf;
                *outSize = (UINT)src->size();
                return S_OK;
            }
            return D3D11_ERROR_FILE_NOT_FOUND;
        }

        HRESULT __stdcall Close(void const *outData) {
            auto buf = (char *)outData;
            delete[] buf;
            return S_OK;
        }

        bool inVs{true};
    };

    FakeInclude include(ffxLib);
    include.inVs = true;
    CComPtr<ID3DBlob> vsCode, vsErrors;
    hr = D3DCompile(vsSource.data(), vsSource.size(), vsPath.string().c_str(), macros.data(), &include, "vs_main",
                    "vs_5_0", flags, 0, &vsCode, &vsErrors);
    if (FAILED(hr)) {
        std::string_view error((char const *)vsErrors->GetBufferPointer(), vsErrors->GetBufferSize());
        throw std::runtime_error(fmt::format("Could not compile vertex shader: {}", error));
    }

    include.inVs = false;
    CComPtr<ID3DBlob> psCode, psErrors;
    hr = D3DCompile(psSource.data(), psSource.size(), psPath.string().c_str(), macros.data(), &include, "ps_main",
                    "ps_5_0", flags, 0, &psCode, &psErrors);
    if (FAILED(hr)) {
        std::string_view error((char const *)psErrors->GetBufferPointer(), psErrors->GetBufferSize());
        throw std::runtime_error(fmt::format("Could not compile pixel shader: {}", error));
    }
}

std::optional<MaterialLibrary::CachedGraph>
MaterialLibrary::LoadGraph(std::shared_ptr<poe::format::fxgraph::FxGraph> fxg) {
    CachedGraph ret;
    ret.fxg = fxg;
    auto &g = *ret.fxg;
    pog::FxGraphGraph gg(g, ffx_, *this);

    std::queue<NodeIdx> wavefront;
    std::vector<size_t> incomingEdgeCounts(g.nodes.size());
    std::vector<StageIdx> nodeStages(g.nodes.size());
    std::vector<ffx_ns::Fragment const *> nodeFrags(g.nodes.size());
    std::set<NodeIdx> readNodes, writeNodes;
    for (size_t nodeIdx = 0; nodeIdx < g.nodes.size(); ++nodeIdx) {
        size_t incomingEdgeCount = gg.reverseEdges.contains(nodeIdx) ? gg.reverseEdges[nodeIdx].size() : 0;
        if (incomingEdgeCount == 0) {
            wavefront.push(nodeIdx);
        }
        incomingEdgeCounts[nodeIdx] = incomingEdgeCount;
        auto &node = g.nodes[nodeIdx];
        auto &frag = nodeFrags[nodeIdx] = ffx_->LookupFragment(node.key.type);
        if (frag) {
            for (auto &decl : frag->declarations_) {
                if (auto *kw = std::get_if<ffx_ns::KeywordDeclaration>(&decl)) {
                    if ("read"sv == kw->keyword) {
                        readNodes.insert(nodeIdx);
                    } else if ("write"sv == kw->keyword) {
                        writeNodes.insert(nodeIdx);
                    }
                }
            }
        }
        if (auto &stage = node.key.stage) {
            if (auto stageIdx = LookupStage(stage.value())) {
                nodeStages[nodeIdx] = *stageIdx;
            } else {
                LOG_F(ERROR, "Unknown stage {}", stage.value());
            }
        }
    }

    // Sweep through the nodes in dependency order, marking each node as with the latest stage that contributes to it.
    // Once we have that, we can cut graphs by stage into a forest where each graph has a single stage.
    // Those graphs can then be individually ordered topologically as a base to generate stage processing code from.
    // Note: care must be taken in that "read" nodes must be evaluated before any "write" nodes in a stage, it is not
    // sufficient to run all apparent source nodes first in a stage as they may also be sinks with all dependencies
    // being from a previous stage.

    // Some game files have inconsistent orders between stages where an output for an earlier stage relies on an input
    // from a later stage. There's a few possible propagation strategies:
    // 1) propagate the stage of the dependency which would push the node evaluation into a later stage which may
    //    possibly clobber data written in the meantime;
    // 2) refuse to overwrite the inherent stage, having it be evaluated before its inputs are ready with defaults;
    // 3) refuse to evaluate the node at all, effectively disabling it.

    // The code here currently propagates the derived stage order resulting in alternative 1 out of the box but as the
    // inherent stage is still known code can later decide to disable out-of-order nodes or move them to their true stage
    // despite the indeterminate (zero) inputs that it will entail.

    // Propagate stages throughout graphs as we traverse in a topological order, preserving that order as we record
    // the nodes visited grouped by stage.
    std::map<StageIdx, std::vector<NodeIdx>> stageNodes;
    while (!wavefront.empty()) {
        auto nodeIdx = wavefront.front();
        wavefront.pop();
        auto &neighbours = gg.edges[nodeIdx];
        auto &stage = nodeStages[nodeIdx];
        stageNodes[stage].push_back(nodeIdx);
        for (auto neighIdx : neighbours) {
            nodeStages[neighIdx] = std::max(nodeStages[neighIdx], stage);
            if (--incomingEdgeCounts[neighIdx] == 0) {
                wavefront.push(neighIdx);
            }
        }
    }

    // Fixup the node set so that "write" nodes don't occur before "read" in a stage by moving them all to the end of
    // the order.
    for (auto &[stage, nodes] : stageNodes) {
        std::ranges::stable_partition(nodes, [&writeNodes](size_t nodeIdx) { return !writeNodes.contains(nodeIdx); });
    }

    ret.sortedNodesPerStage = stageNodes;

    return ret;
}

std::optional<MaterialLibrary::CachedGraph> MaterialLibrary::LoadGraph(nlohmann::json const &js) {
    auto fxg = poe::format::fxgraph::parse_fxgraph_json(js);
    if (!fxg) {
        return std::nullopt;
    }
    return LoadGraph(fxg);
}

std::optional<MaterialLibrary::CachedGraph> MaterialLibrary::LoadGraph(poe::util::path const &graphPath) {
    if (auto I = graphCache_.find(graphPath.path_); I != graphCache_.end()) {
        LOG_F(INFO, "Found cached graph {}", graphPath.path_);
        return I->second;
    }

    auto fh = vfs_->open_file(graphPath);
    if (fh) {
        auto data = fh->read_all();
        try {
            if (auto text = poe::util::map_unicode_to_utf8_string(data)) {
                auto js = nlohmann::json::parse(*text);
                auto ret = LoadGraph(js);
                if (ret) {
                    graphCache_.insert({graphPath.path_, *ret});
                }
                return ret;
            }
        } catch (nlohmann::json::exception &) {
        }
    }
    LOG_F(INFO, "Failed loading graph {}", graphPath.path_);
    return std::nullopt;
}

std::optional<MaterialLibrary::CachedMaterial> MaterialLibrary::LoadMaterial(poe::util::path const &matPath) {
    if (auto I = materialCache_.find(matPath.path_); I != materialCache_.end()) {
        LOG_F(INFO, "Found cached material {}", matPath.path_);
        return I->second;
    }

    auto fh = vfs_->open_file(matPath);
    if (fh) {
        auto data = fh->read_all();
        auto ret = CachedMaterial{};
        ret.mat = poe::format::mat::parse_mat(data);
        for (auto &gi : ret.mat->graphInstances) {
            if (!ret.usedGraphs.contains(gi.parent)) {
                auto subPath = poe::util::path(gi.parent);
                if (auto sub = LoadGraph(subPath)) {
                    ret.usedGraphs.insert({subPath.path_, *sub});
                } else {
                    LOG_F(INFO, "Failed loading subgraph {}", subPath.path_);
                    return std::nullopt;
                }
            }
        }
        materialCache_.insert({matPath.path_, ret});
        return ret;
    }
    LOG_F(INFO, "Failed loading material {}", matPath.path_);
    return std::nullopt;
}

std::shared_ptr<MaterialThingie> MaterialLibrary::MakeMaterialThingie(poe::util::path baseGraphPath,
                                                                      poe::util::path matPath) {
    auto ret = std::make_shared<MaterialThingie>();
    LOG_F(INFO, "Requesting base graph {}", baseGraphPath.path_);
    auto baseGraph = LoadGraph(baseGraphPath);
    LOG_F(INFO, "Requesting material {}", matPath.path_);
    auto mat = LoadMaterial(matPath);

    if (baseGraph && mat) {
    }
    return ret;
}
} // namespace pog