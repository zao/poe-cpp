#pragma once

#include <pog/tile_meta.hpp>

#include <absl/container/flat_hash_map.h>
#include <glm/glm.hpp>

#include <map>
#include <memory>
#include <span>

#include <atlbase.h>
#include <d3d11.h>
#include <d3d11shader.h>

#include <poe/format/fmt.hpp>
#include <poe/format/smd.hpp>
#include <poe/format/tgm.hpp>
#include <poe/util/install_location.hpp>
#include <pog/dx_context.hpp>

#include "skinning.hpp"

namespace pog {
/* Scaffolding here refers to the lines and walls that indicate level / tile boundaries and other UI-related lines.
 */
struct ScaffoldGeometry {
    void AddVerticalQuadStrip(std::span<glm::vec2 const> points, std::span<glm::vec3 const> colors, float zLow,
                              float zHigh, bool loop = false);

    void AddTriangle(std::span<glm::vec3 const, 3> tri, glm::vec3 color);
    void AddTriangle(std::span<glm::vec3 const, 3> tri, std::span<glm::vec3 const, 3> colors);

    void AddQuad(glm::vec3 origin, glm::vec3 xSide, glm::vec3 ySide, glm::vec3 color);
    void AddQuad(glm::vec3 origin, glm::vec3 xSide, glm::vec3 ySide, std::span<glm::vec3 const, 4> colors);

    void AddLineAABB(glm::vec3 origin, std::span<float const, 6> bbox, glm::vec3 color);
    void AddLineAABB(glm::vec3 origin, std::span<glm::vec2 const, 3> bbox, glm::vec3 color);

    void AddLineRect(glm::vec3 origin, glm::vec3 xSide, glm::vec3 ySide, glm::vec3 color);

    void AddLineRect(glm::vec3 origin, glm::vec3 xSide, glm::vec3 ySide, std::span<glm::vec3 const, 4> colors);

    void AddLineStrip(std::span<glm::vec3 const> points, std::span<glm::vec3 const> colors, bool loop = false);

    struct TintedUnlitVertex {
        glm::vec3 pos;
        glm::vec3 color;
    };

    std::vector<uint32_t> triIndices;
    std::vector<TintedUnlitVertex> triVertices;
    std::vector<uint32_t> lineIndices;
    std::vector<TintedUnlitVertex> lineVertices;
};

struct ScaffoldDrawer {
    ScaffoldDrawer(std::shared_ptr<pog::DxContext> dx);

    struct PassCB {
        glm::mat4 viewProjectionTransform;
    };
    static_assert(sizeof(PassCB) == 64);

    struct PipeCB {
        glm::vec4 padding;
    };
    static_assert(sizeof(PipeCB) == 16);

    struct ObjectCB {
        glm::mat4 worldTransform;
    };
    static_assert(sizeof(ObjectCB) == 64);

    void Draw(ScaffoldGeometry const &geom, glm::mat4 viewProjMat, glm::mat4 worldMat);

  private:
    std::shared_ptr<pog::DxContext> dx_;
    CComPtr<ID3D11VertexShader> vs_;
    CComPtr<ID3D11PixelShader> ps_;
    CComPtr<ID3D11InputLayout> il_;
    CComPtr<ID3D11RasterizerState> rasterState_;
    CComPtr<ID3D11DepthStencilState> dss_;

    poe::util::ReloadableFile vsObj_, psObj_;

    CComPtr<ID3D11Buffer> cbPass_;
    CComPtr<ID3D11Buffer> cbPipe_;
    CComPtr<ID3D11Buffer> cbObject_;
};

struct LightSet {
    LightSet(std::shared_ptr<pog::DxContext> dx) : dx_(dx) {}

    struct Light {
        glm::vec4 pos;
        glm::vec3 color;
        float intensity;
    };

    std::vector<Light> lights;

    ID3D11ShaderResourceView *LightSB();

  private:
    std::shared_ptr<pog::DxContext> dx_;
    bool dirty_ = true;
    CComPtr<ID3D11Buffer> sb_;
    CComPtr<ID3D11ShaderResourceView> sbSrv_;
};

struct TileKeyDrawer {
    TileKeyDrawer(std::shared_ptr<pog::DxContext> dx) : dx_(dx), scaffoldDrawer_(dx) {}

    void DrawTileKey(glm::mat4 worldMat, glm::mat4 viewMat, glm::mat4 projMat, TileKey const &key);

  private:
    std::shared_ptr<pog::DxContext> dx_;
    ScaffoldDrawer scaffoldDrawer_;
};

struct ConstantInfo {
    enum class Category {
        Pass,
        Pipe,
        Object,
    };

    size_t offset;
    size_t size;
    Category category;
};

struct CBBase {
    CBBase(size_t size = 0) : cpu(size) {}
    CComPtr<ID3D11Buffer> gpu;

    void SetCpuPart(ConstantInfo const &info, std::span<std::byte const> data);

    template <typename T> void SetCpuPart(ConstantInfo const &info, T const &t) {
        SetCpuPart(info, as_bytes(std::span(&t, 1)));
    }

    void SetGpu(ID3D11DeviceContext *ctx);

  protected:
    std::vector<std::byte> cpu;
    bool dirty{false};
};

struct Shader {
    explicit Shader(std::shared_ptr<DxContext> &dx, std::filesystem::path vsPath, std::filesystem::path psPath);

    void LoadIfNeeded();
    void BindShaders();

    using VertexComponent = poe::format::dolm::VertexComponent;

    bool BindVertexFormat(VertexComponent comp);

    void BindResources(LightSet &lights, std::span<ID3D11ShaderResourceView *const> texSrvs,
                       std::span<ID3D11SamplerState *const> texSamplers);

    std::shared_ptr<DxContext> dx_;
    poe::util::ReloadableFile vsObj_, psObj_;
    CComPtr<ID3D11ShaderReflection> vsRefl_, psRefl_;

    CComPtr<ID3D11VertexShader> vs_;
    CComPtr<ID3D11PixelShader> ps_;

    std::map<VertexComponent, CComPtr<ID3D11InputLayout>> inputLayouts_;

    template <typename T> bool SetConstant(absl::string_view name, T const &value) {
        if (auto I = vsConstants.find(name); I != vsConstants.end()) {
            constantBuffers[I->second.category].SetCpuPart(I->second, value);
            return true;
        }
        return false;
    }

    poe::format::dolm::VertexComponent vertexFormat_;
    absl::flat_hash_map<std::string, ConstantInfo> vsConstants, psConstants;
    absl::flat_hash_map<ConstantInfo::Category, CBBase> constantBuffers;
};

struct MeshDrawer {
    struct CachedGeometry {
        CComPtr<ID3D11Buffer> vb_, ib_;
    };

#pragma pack(push, 1)
    struct VertexPVV {
        glm::vec3 pos;
        uint8_t v0[4];
        uint8_t v1[4];
    };
#pragma pack(pop)

    MeshDrawer(std::shared_ptr<pog::DxContext> dx);

    void PollResources() const;

    std::shared_ptr<CachedGeometry> LoadGeometry(poe::format::dolm::DolmGeometry const &geom, int meshIdx = 0);
    std::shared_ptr<CachedGeometry> LoadGeometry(poe::format::fmt::FMT const &geom, int meshIdx = 0);

    using ShapeSrvSeq = std::span<ID3D11ShaderResourceView *const>;

    void DrawGeometry(glm::mat4 const &worldMat, glm::mat4 const &viewMat, glm::mat4 const &projMat, LightSet &lights,
                      poe::format::dolm::DolmGeometry const &dolm, int meshIdx = 0,
                      CachedGeometry const *cachedGeom = nullptr, std::vector<bool> const *enabledShapes = nullptr,
                      ShapeSrvSeq const *shapeSrvs = nullptr, std::shared_ptr<Shader> shader = nullptr, pog::Pose const *pose = nullptr);

    void DrawGeometry(glm::mat4 const &worldMat, glm::mat4 const &viewMat, glm::mat4 const &projMat, LightSet &lights,
                      poe::format::fmt::FMT const &geom, int meshIdx = 0, CachedGeometry const *cachedGeom = nullptr,
                      std::vector<bool> const *enabledShapes = nullptr, ShapeSrvSeq const *shapeSrvs = nullptr,
                      std::shared_ptr<Shader> shader = nullptr);

    void DrawGeometry(glm::mat4 const &worldMat, glm::mat4 const &viewMat, glm::mat4 const &projMat, LightSet &lights,
                      poe::format::tgm::TgmGeometry const &geom, int meshIdx = 0,
                      CachedGeometry const *cachedGeom = nullptr, std::vector<bool> const *enabledShapes = nullptr,
                      ShapeSrvSeq const *shapeSrvs = nullptr, std::shared_ptr<Shader> shader = nullptr);

#pragma pack(push, 4)
    struct PassCB {
        glm::mat4 viewProjectionTransform;
        glm::mat4 viewTransform;
        glm::mat4 invViewTransform;
    };
    static_assert(sizeof(PassCB) == 64 * 3);

    struct PipeCB {
        uint32_t hasTex0, hasTex1;
        uint32_t hasLegacyNormals;
        uint32_t isFlipped;
    };
    static_assert(sizeof(PipeCB) == 16);

    struct ObjectCB {
        glm::mat4 worldTransform;
        glm::mat4 invWorldTransform;
        glm::vec4 animationMatricesIndices;
    };
    static_assert(sizeof(ObjectCB) == 64 * 2 + 16 * 1);
#pragma pack(pop)

  private:
    std::shared_ptr<pog::DxContext> dx_;

    std::shared_ptr<Shader> fallbackShader_;
    CComPtr<ID3D11RasterizerState> rasterState_, rasterStateFlipped_;
    CComPtr<ID3D11ShaderResourceView> fallbackTex_, fallbackNormalTex_;
    CComPtr<ID3D11Buffer> noOpSkinBuffer_;
};
} // namespace pog
