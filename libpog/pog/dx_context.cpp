#include "dx_context.hpp"

#include <GLFW/glfw3.h>
#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3native.h>

namespace pog {
DxContext::DxContext() {
    HRESULT hr = S_OK;
    hr = CreateDXGIFactory(IID_PPV_ARGS(&dxgiFactory_));
    DWORD devFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
#ifndef NDEBUG
    devFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
    D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0, chosenFeatureLevel{};
    hr = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, devFlags, &featureLevel, 1, D3D11_SDK_VERSION,
                           &d3dDev_, &chosenFeatureLevel, &d3dCtx_);
}
} // namespace pog