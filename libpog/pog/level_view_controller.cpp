﻿#include "level_view_controller.hpp"

#include <pog/game_data.hpp>

#include <poe/format/arm.hpp>

#include <fmt/format.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <imgui.h>

namespace pog {
LevelViewController::LevelViewController(std::shared_ptr<DxContext> dx)
    : SceneViewController(dx), scaffoldDrawer_(dx) {}

void LevelViewController::Update(FrameUpdate const &update, std::shared_ptr<GameData> gameData) {
    auto fbSize = update.fbSize;
    projMat_ = glm::perspectiveFovLH_ZO(glm::radians(75.0f), (float)fbSize.x, (float)fbSize.y, 0.1f, 10000.0f);
    glm::vec3 cam_eye =
        cam.at + (glm::vec3)(glm::rotate(glm::rotate(glm::identity<glm::mat4>(), glm::radians(cam.azimuth_deg),
                                                     glm::vec3{0.0f, 0.0f, 1.0f}),
                                         glm::radians(cam.altitude_deg), glm::vec3{0.0f, 1.0f, 0.0f}) *
                             glm::vec4{cam.distance, 0.0f, 0.0f, 1.0f});

    viewMat_ = glm::lookAtLH(cam_eye, cam.at, glm::vec3{0, 0, -1});

    if (!gameData) {
        return;
    }

    gameData_ = gameData;

    if (auto graph = gameData_->CurrentGraph(); graph_ != graph) {
        graph_ = graph;

        level_ = std::make_shared<LevelInstance>();
        auto big = std::make_shared<BigTile>();
        big->tdt = graph_->tdts.begin()->second;
        auto origin = glm::ivec2{0, 0};
        auto size = big->tdt->header.dim;
        level_->rootTiles[origin] = big;
        for (int row = 0; row < size.y; ++row) {
            for (int col = 0; col < size.x; ++col) {
                glm::ivec2 smallCoord{col, row};
                level_->rootTileProxies[smallCoord] = origin;
            }
        }
    }
}

void LevelViewController::Draw(LayerTarget const &rt) {
    constexpr float const tileSize = 250.0f;
    auto fbSize = rt.Size();
    ScaffoldGeometry scaffoldGeom;
    auto DrawGrid = [](ScaffoldGeometry &geom, glm::vec2 minCoord, glm::vec2 maxCoord, float lineSep) {
    };

    if (level_) {
        DrawGrid(scaffoldGeom, glm::vec2{0, 0}, glm::vec2(10, 10) * tileSize, 250.0f);
        for (auto &[rootCoord, rootTile] : level_->rootTiles) {
            auto rootPos = glm::vec3(rootCoord, 0) * tileSize;
            auto rootSize = rootTile->tdt->header.dim;
            auto xBase = glm::vec3(1, 0, 0);
            auto yBase = glm::vec3(0, 1, 0);
            scaffoldGeom.AddQuad(rootPos, xBase * (float)rootSize.x, yBase * (float)rootSize.y,
                                 glm::vec3(glm::mod(glm::vec2(rootCoord) / 20.0f, 1.0f), 0.5f));
        }
    }
    if (graph_ && (graph_->dgr || graph_->tgr)) {
        constexpr float const tileSize = 250.0f;
        struct Node {
            Node(poe::format::dgr::DGRNode &src) : x((int)src.x_), y((int)src.y_), tag(src.key_) {}
            Node(poe::format::tgr::TGRNode &src) : x((int)src.x_), y((int)src.y_), tag(src.tag_) {}

            int x, y;
            std::string tag;
        };

        auto AddWorldWallSquare = [&scaffoldGeom](glm::vec2 worldV0, glm::vec2 worldV1, glm::vec3 color,
                                                  glm::vec2 heightRange) {
            float inset = 23.0f;
            glm::vec2 insets[]{
                {+inset, +inset},
                {+inset, -inset},
                {-inset, -inset},
                {-inset, +inset},
            };
            glm::vec2 corners[]{
                worldV0,
                {worldV0.x, worldV1.y},
                worldV1,
                {worldV1.x, worldV0.y},
            };
            for (int i = 0; i < std::size(corners); ++i) {
                corners[i] += insets[i];
            }
            glm::vec3 colors[]{color, color, color, color};
            scaffoldGeom.AddVerticalQuadStrip(corners, colors, heightRange[0], heightRange[1], true);
        };

        auto AddWorldGridSquare = [&scaffoldGeom](glm::vec2 worldV0, glm::vec2 worldV1, glm::vec3 color, float height) {
            glm::vec3 corners[]{
                glm::vec3{worldV0, height},
                {worldV0.x, worldV1.y, height},
                glm::vec3{worldV1, height},
                {worldV1.x, worldV0.y, height},
            };
            glm::vec3 colors[]{color, color, color, color};
            scaffoldGeom.AddLineStrip(corners, colors, true);
        };

        auto AddTileFloor = [&scaffoldGeom, tileSize](glm::ivec2 origin, glm::ivec2 size, glm::vec3 color) {
            auto xSide = tileSize * glm::vec3(size.x, 0.0f, 0.0f);
            auto ySide = tileSize * glm::vec3(0.0f, size.y, 0.0f);
            std::array<glm::vec3, 4> colors{color, color, color, color};
            scaffoldGeom.AddQuad(tileSize * glm::vec3(origin, 0.0f), xSide, ySide, colors);
        };

        glm::vec2 maxCover{0.0f, 0.0f};

        std::vector<Node> nodes;
        if (graph_->dgr) {
            auto &src = graph_->dgr->nodes_;
            nodes.assign(begin(src), end(src));
        } else {
            auto &src = graph_->tgr->nodes_;
            nodes.assign(begin(src), end(src));
        }
        {
            auto [xExtent, yExtent] = graph_->dgr ? graph_->dgr->size_ : graph_->tgr->size_;
            auto origin = glm::vec2{0.0f, 0.0f};
            auto extent = glm::vec2{(float)xExtent, (float)yExtent};
            auto v0 = tileSize * origin;
            auto v1 = v0 + tileSize * extent;
            glm::vec3 sharedColor{0.5f, 0.5f, 0.5f};
            AddWorldGridSquare(v0, v1, sharedColor, -5.0f);

            maxCover = v1;
        }

        for (auto &node : nodes) {
            auto nodeOrigin = glm::ivec2{node.x, node.y};
            auto v0 = tileSize * glm::vec2(nodeOrigin);
            glm::vec2 v1;
            glm::vec3 sharedColor;

            if (auto roomI = std::find_if(std::begin(graph_->arms), std::end(graph_->arms),
                                          [&node](auto &item) -> bool { return item.second->name_ == node.tag; });
                roomI != std::end(graph_->arms)) {
                auto &[roomName, room] = *roomI;

                auto extent = glm::vec2{room->dims_xy_[0], room->dims_xy_[1]};
                v1 = v0 + tileSize * extent;
                sharedColor = glm::vec3{0.5f, 0.2f, 0.2f};

                for (size_t rowIdx = 0; rowIdx < room->k_x_by_y_.size(); ++rowIdx) {
                    auto &kRow = room->k_x_by_y_[rowIdx];
                    for (size_t colIdx = 0; colIdx < kRow.size(); ++colIdx) {
                        auto &kEntry = kRow[colIdx];
                        glm::ivec2 subOrigin{colIdx, rowIdx};
                        auto cover = GetKCover(kEntry);
                        if (cover.kind != 'n') {
                            auto sub0 = tileSize * glm::vec2(nodeOrigin + subOrigin + cover.i0);
                            auto sub1 = sub0 + tileSize * glm::vec2(cover.i1);

                            glm::vec3 color{};
                            switch (cover.kind) {
                            case 'k':
                                color = glm::vec3{0.1f, 0.4f, 0.1f};
                                break;
                            case 's':
                                color = glm::vec3{0.3f, 0.3f, 0.3f};
                                break;
                            case 'f':
                                color = glm::vec3{0.1f, 0.1f, 0.4f};
                                break;
                            case 'o':
                                color = glm::vec3{0.4f, 0.1f, 0.1f};
                                break;
                            }
                            AddTileFloor(nodeOrigin + subOrigin + cover.i0, cover.i1 - cover.i0, color);
                        }
                    }
                }
            } else {
                auto extent = glm::vec2{1.0f, 1.0f};
                v1 = v0 + tileSize * extent;
                sharedColor = glm::vec3{0.9f, 0.0f, 0.0f};
            }
            AddWorldGridSquare(v0, v1, sharedColor, -10.0f);
            maxCover = glm::max(maxCover, v1);
        }

        cam.at = glm::vec3(maxCover / 2.0f, 0.0f);
    }

    auto *ctx = dx_->Context();
    constexpr glm::vec4 const clearColor{0.1f, 0.1f, 0.1f, 1.0f};
    ctx->ClearRenderTargetView(rt.rtv, glm::value_ptr(clearColor));
    ctx->ClearDepthStencilView(rt.dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
    ctx->OMSetRenderTargets(1, &rt.rtv.p, rt.dsv);

    glm::mat4 viewProjMat = projMat_ * viewMat_;

    scaffoldDrawer_.Draw(scaffoldGeom, viewProjMat, glm::mat4(1.0f));
}

void LevelViewController::ChangeLevel(std::shared_ptr<LevelInstance> level) { level_ = level; }
} // namespace pog
