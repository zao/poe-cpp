#include <pog/platform.hpp>

#include <atltypes.h>

#include <imgui_impl_win32.h>
#include <imgui_impl_dx11.h>

#include <poe/util/install_location.hpp>

#include <array>

// Forward declare message handler from imgui_impl_win32.cpp
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

namespace pog {

RenderTarget2D::RenderTarget2D(ID3D11Device *dev, ID2D1Factory *fac, ImVec2 size) : fac_(fac), size_(size) {
    HRESULT hr = S_OK;
    (void)hr;

    D3D11_TEXTURE2D_DESC color_desc{
        .Width = (UINT)size.x,
        .Height = (UINT)size.y,
        .MipLevels = 1,
        .ArraySize = 1,
        .Format = DXGI_FORMAT_R8G8B8A8_UNORM,
        .SampleDesc =
            {
                .Count = 1,
                .Quality = 0,
            },
        .Usage = D3D11_USAGE_DEFAULT,
        .BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE,
        .CPUAccessFlags = 0,
        .MiscFlags = 0,
    };

    hr = dev->CreateTexture2D(&color_desc, nullptr, &color_tex_);
    hr = dev->CreateShaderResourceView(color_tex_, nullptr, &color_srv_);

    auto dpi = 96 * ImGui::GetWindowDpiScale();
    auto props =
        D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_DEFAULT,
                                     D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED), dpi, dpi);
    CComPtr<IDXGISurface> surf;
    surf = color_tex_;
    hr = fac->CreateDxgiSurfaceRenderTarget(surf, props, &rt_);
}

bool RenderTarget2D::is_size(ImVec2 size) const { return size.x == size_.x && size.y == size_.y; }

RenderTarget::RenderTarget(ID3D11Device *dev, ImVec2 size) : size_(size) {
    HRESULT hr = S_OK;
    (void)hr;
    D3D11_TEXTURE2D_DESC color_desc{
        .Width = (UINT)size.x,
        .Height = (UINT)size.y,
        .MipLevels = 1,
        .ArraySize = 1,
        .Format = DXGI_FORMAT_R8G8B8A8_UNORM,
        .SampleDesc =
            {
                .Count = 1,
                .Quality = 0,
            },
        .Usage = D3D11_USAGE_DEFAULT,
        .BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE,
        .CPUAccessFlags = 0,
        .MiscFlags = 0,
    };
    hr = dev->CreateTexture2D(&color_desc, nullptr, &color_tex_);

    D3D11_TEXTURE2D_DESC depth_desc{
        .Width = (UINT)size.x,
        .Height = (UINT)size.y,
        .MipLevels = 1,
        .ArraySize = 1,
        .Format = DXGI_FORMAT_D32_FLOAT,
        .SampleDesc =
            {
                .Count = 1,
                .Quality = 0,
            },
        .Usage = D3D11_USAGE_DEFAULT,
        .BindFlags = D3D11_BIND_DEPTH_STENCIL,
        .CPUAccessFlags = 0,
        .MiscFlags = 0,
    };
    hr = dev->CreateTexture2D(&depth_desc, nullptr, &depth_tex_);

    hr = dev->CreateRenderTargetView(color_tex_, nullptr, &color_rtv_);
    hr = dev->CreateDepthStencilView(depth_tex_, nullptr, &depth_dsv_);
    hr = dev->CreateShaderResourceView(color_tex_, nullptr, &color_srv_);

    D3D11_DEPTH_STENCIL_DESC dss_desc{.DepthEnable = TRUE,
                                      .DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL,
                                      .DepthFunc = D3D11_COMPARISON_LESS,
                                      .StencilEnable = FALSE,
                                      .StencilReadMask = 0xFF,
                                      .StencilWriteMask = 0xFF,
                                      .FrontFace =
                                          {
                                              .StencilFailOp = D3D11_STENCIL_OP_KEEP,
                                              .StencilDepthFailOp = D3D11_STENCIL_OP_KEEP,
                                              .StencilPassOp = D3D11_STENCIL_OP_KEEP,
                                              .StencilFunc = D3D11_COMPARISON_ALWAYS,
                                          },
                                      .BackFace = {
                                          .StencilFailOp = D3D11_STENCIL_OP_KEEP,
                                          .StencilDepthFailOp = D3D11_STENCIL_OP_KEEP,
                                          .StencilPassOp = D3D11_STENCIL_OP_KEEP,
                                          .StencilFunc = D3D11_COMPARISON_ALWAYS,
                                      }};
    hr = dev->CreateDepthStencilState(&dss_desc, &depth_dss_);

    D3D11_RASTERIZER_DESC rs_desc{
        .FillMode = D3D11_FILL_SOLID,
        .CullMode = D3D11_CULL_NONE,
    };
    hr = dev->CreateRasterizerState(&rs_desc, &raster_state_);
}

bool RenderTarget::is_size(ImVec2 size) const { return size.x == size_.x && size.y == size_.y; }

void WinBits::CreateOrResize(HWND wnd, UINT width, UINT height) {
    HRESULT hr = S_OK;
    (void)hr;
    // Release sized stuff
    dx.bb_rtv_.Release();

    if (!dx.swapchain_) {
        D2D1_FACTORY_OPTIONS d2d_opts{
            .debugLevel = D2D1_DEBUG_LEVEL_INFORMATION,
        };
        hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, d2d_opts, &dx.d2d_fac_);
        hr = CreateDXGIFactory(IID_PPV_ARGS(&dx.factory_));
        UINT create_flags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
#if defined(DEBUG)
        create_flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
        D3D_FEATURE_LEVEL req_feature_level = D3D_FEATURE_LEVEL_11_0;
        hr = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, create_flags, &req_feature_level, 1,
                               D3D11_SDK_VERSION, &dx.dev_, &dx.feature_level_, &dx.ctx_);

        DXGI_SWAP_CHAIN_DESC scd{
            .BufferDesc =
                DXGI_MODE_DESC{
                    .Width = width,
                    .Height = height,
                    .RefreshRate = {0, 1},
                    .Format = DXGI_FORMAT_R8G8B8A8_UNORM,
                    .ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED,
                    .Scaling = DXGI_MODE_SCALING_UNSPECIFIED,
                },
            .SampleDesc =
                DXGI_SAMPLE_DESC{
                    .Count = 1,
                    .Quality = 0,
                },
            .BufferUsage = DXGI_USAGE_BACK_BUFFER | DXGI_USAGE_RENDER_TARGET_OUTPUT,
            .BufferCount = 2,
            .OutputWindow = wnd,
            .Windowed = TRUE,
            .SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD,
            .Flags = 0,
        };
        hr = dx.factory_->CreateSwapChain(dx.dev_, &scd, &dx.swapchain_);
    } else {
        DXGI_SWAP_CHAIN_DESC scd;
        hr = dx.swapchain_->GetDesc(&scd);
        hr = dx.swapchain_->ResizeBuffers(scd.BufferCount, width, height, DXGI_FORMAT_UNKNOWN, scd.Flags);
    }

    // Create sized stuff
    {
        CComPtr<ID3D11Texture2D> backbuffer;
        hr = dx.swapchain_->GetBuffer(0, IID_PPV_ARGS(&backbuffer));
        hr = dx.dev_->CreateRenderTargetView(backbuffer, nullptr, &dx.bb_rtv_);
    }
}

// Win32 message handler
LRESULT WINAPI WndProc(HWND wnd, UINT msg, WPARAM wparam, LPARAM lparam) {
    if (ImGui_ImplWin32_WndProcHandler(wnd, msg, wparam, lparam))
        return true;

    auto *wb = (WinBits *)GetWindowLongPtrW(wnd, GWLP_USERDATA);
    switch (msg) {
    case WM_NCCREATE: {
        auto *cs = (CREATESTRUCTW *)lparam;
        SetWindowLongPtrW(wnd, GWLP_USERDATA, (LONG_PTR)cs->lpCreateParams);
    } break;
    case WM_SIZE:
        if (wb && wparam != SIZE_MINIMIZED) {
            wb->CreateOrResize(wnd, (UINT)LOWORD(lparam), (UINT)HIWORD(lparam));
        }
        return 0;
    case WM_SYSCOMMAND:
        if ((wparam & 0xFFF0) == SC_KEYMENU)
            return 0;
        break;
    case WM_DESTROY:
        ::PostQuitMessage(0);
        return 0;
    case WM_DPICHANGED:
        if (ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_DpiEnableScaleViewports) {
            CRect suggested_rect = *(RECT *)lparam;
            SetWindowPos(wnd, nullptr, suggested_rect.left, suggested_rect.top, suggested_rect.Width(),
                         suggested_rect.Height(), SWP_NOZORDER | SWP_NOACTIVATE);
        }
        break;
    }

    return DefWindowProcW(wnd, msg, wparam, lparam);
}

WindowFrame::WindowFrame(std::u16string const &name, ImGuiConfigFlags config_flags)
    : io_work_(asio::make_work_guard(io_ctx_)) {
    WNDCLASSEXW wc{
        .cbSize = sizeof(WNDCLASSEXW),
        .style = CS_CLASSDC,
        .lpfnWndProc = WndProc,
        .hInstance = GetModuleHandle(nullptr),
        .lpszClassName = L"Cubbyhole",
    };
    RegisterClassExW(&wc);
    MONITORINFO mi{.cbSize = sizeof(MONITORINFO)};
    GetMonitorInfoW(MonitorFromPoint(CPoint{}, MONITOR_DEFAULTTOPRIMARY), &mi);
    CRect work = mi.rcWork;
    UINT win_w = work.Width() * 8 / 10, win_h = work.Height() * 8 / 10;
    wnd_ = CreateWindowExW(WS_EX_OVERLAPPEDWINDOW, wc.lpszClassName, (LPCWSTR)name.c_str(), WS_OVERLAPPEDWINDOW, 100,
                           100, win_w, win_h, nullptr, nullptr, wc.hInstance, &win_bits_);
    CRect client_rect;
    GetClientRect(wnd_, &client_rect);
    win_bits_.CreateOrResize(wnd_, client_rect.Width(), client_rect.Height());

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    io_ = &ImGui::GetIO();
    io_->ConfigFlags |= config_flags | ImGuiConfigFlags_NavEnableKeyboard | ImGuiConfigFlags_DockingEnable |
                        ImGuiConfigFlags_DpiEnableScaleViewports | ImGuiConfigFlags_ViewportsEnable;
    float dpi_scale_factor = ImGui_ImplWin32_GetDpiScaleForHwnd(wnd_);
    ImGui::StyleColorsLight();
    {
        auto &style = ImGui::GetStyle();
        style.ScaleAllSizes(dpi_scale_factor);
    }

    ImGui_ImplWin32_Init(wnd_);
    ImGui_ImplDX11_Init(win_bits_.dx.dev_, win_bits_.dx.ctx_);

    {
        glyph_ranges_.push_back(u'\0');
        auto push_ranges = [this](ImWchar const *ranges) {
            glyph_ranges_.pop_back();
            for (; *ranges; ++ranges) {
                glyph_ranges_.push_back(*ranges);
            }
            glyph_ranges_.push_back(u'\0');
        };
        push_ranges(io_->Fonts->GetGlyphRangesDefault());
        push_ranges(io_->Fonts->GetGlyphRangesCyrillic());
        push_ranges(io_->Fonts->GetGlyphRangesKorean());
        auto exe_dir = poe::util::executable_dir();
        if (exe_dir) {
            auto sans_file = *exe_dir / "Roboto-Regular.ttf";
            if (exists(sans_file)) {
                sans_font_ = io_->Fonts->AddFontFromFileTTF(sans_file.string().c_str(), 15.0f * dpi_scale_factor,
                                                            nullptr, glyph_ranges_.data());
                io_->FontDefault = sans_font_;
            }
            auto mono_file = *exe_dir / "RobotoMono-Regular.ttf";
            if (exists(mono_file)) {
                mono_font_ = io_->Fonts->AddFontFromFileTTF(mono_file.string().c_str(), 15.0f * dpi_scale_factor,
                                                            nullptr, glyph_ranges_.data());
            }
        }
    }

    ShowWindow(wnd_, SW_SHOWDEFAULT);
}

WindowFrame::~WindowFrame() {
    ImGui_ImplDX11_Shutdown();
    ImGui_ImplWin32_Shutdown();
}

void WindowFrame::push_sans_font() const { ImGui::PushFont(sans_font_); }

void WindowFrame::push_mono_font() const { ImGui::PushFont(mono_font_); }

bool WindowFrame::new_frame() {
    MSG msg{};
    while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
        TranslateMessage(&msg);
        DispatchMessageW(&msg);

        if (msg.message == WM_QUIT) {
            app_running_ = false;
        }
    }

    if (!app_running_) {
        return false;
    }

    {
        using clock_t = std::chrono::high_resolution_clock;
        auto const MAIN_THREAD_TASK_TIMESLICE = std::chrono::duration<float>(1.0f / 200.0f);
        auto tic = clock_t::now();
        while (true) {
            auto count = io_ctx_.poll_one();
            auto toc = clock_t::now();
            if (toc - tic >= MAIN_THREAD_TASK_TIMESLICE || !count) {
                break;
            }
        };
    }

    ImGui_ImplDX11_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();

    return true;
}

void WindowFrame::begin_window() {
    ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;

    {
        ImGuiViewport *viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->WorkPos);
        ImGui::SetNextWindowSize(viewport->WorkSize);
        ImGui::SetNextWindowViewport(viewport->ID);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
        window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize |
                        ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
    }

    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
    ImGui::Begin("RootWindow", nullptr, window_flags);
    ImGui::PopStyleVar();

    ImGui::PopStyleVar(2);

    {
        ImGuiID dockspace_id = ImGui::GetID("TopDockspace");
        ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
    }
}

void WindowFrame::render_and_present() {
    ImGui::Render();
    if (io_->ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
        ImGui::UpdatePlatformWindows();
        ImGui::RenderPlatformWindowsDefault();
    }

    std::array<ID3D11RenderTargetView *, 1> rtvs{win_bits_.dx.bb_rtv_};
    win_bits_.dx.ctx_->OMSetRenderTargets((UINT)rtvs.size(), rtvs.data(), nullptr);

    ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

    win_bits_.dx.swapchain_->Present(0, 0);
}

} // namespace pog