#pragma once

#include <asio.hpp>
#include <imgui.h>
#include <d3d11.h>
#include <d2d1.h>

#include <atlbase.h>

#include <string>

namespace pog {
struct RenderTarget2D {
    explicit RenderTarget2D(ID3D11Device *dev, ID2D1Factory * fac, ImVec2 size);
    RenderTarget2D(RenderTarget2D const&) = delete;
    RenderTarget2D& operator=(RenderTarget2D const&) = delete;

    bool is_size(ImVec2 size) const;

    CComPtr<ID2D1Factory> fac_;
    CComPtr<ID2D1RenderTarget> rt_;
    CComPtr<ID3D11Texture2D> color_tex_;
    CComPtr<ID3D11ShaderResourceView> color_srv_;
    ImVec2 size_;
};

struct RenderTarget {
    explicit RenderTarget(ID3D11Device *dev, ImVec2 size);
    RenderTarget(RenderTarget const &) = delete;
    RenderTarget &operator=(RenderTarget const &) = delete;

    bool is_size(ImVec2 size) const;

    CComPtr<ID3D11Texture2D> color_tex_, depth_tex_;
    CComPtr<ID3D11RenderTargetView> color_rtv_;
    CComPtr<ID3D11DepthStencilView> depth_dsv_;
    CComPtr<ID3D11ShaderResourceView> color_srv_;
    CComPtr<ID3D11DepthStencilState> depth_dss_;
    CComPtr<ID3D11RasterizerState> raster_state_;
    ImVec2 size_;
};

struct DXBits {
    CComPtr<IDXGIFactory> factory_;
    CComPtr<ID3D11Device> dev_;
    CComPtr<ID3D11DeviceContext> ctx_;
    CComPtr<IDXGISwapChain> swapchain_;
    D3D_FEATURE_LEVEL feature_level_;
    CComPtr<ID3D11RenderTargetView> bb_rtv_;
    CComPtr<ID3D11Texture2D> bb_ds_tex_;

    CComPtr<ID2D1Factory> d2d_fac_;
};

struct WinBits {
    void CreateOrResize(HWND wnd, UINT width, UINT height);
    DXBits dx;
};

LRESULT WINAPI WndProc(HWND wnd, UINT msg, WPARAM wparam, LPARAM lparam);

struct WindowFrame {
    explicit WindowFrame(std::u16string const &name, ImGuiConfigFlags config_flags);
    ~WindowFrame();
    WindowFrame &operator=(WindowFrame const &) = delete;
    WindowFrame(WindowFrame const &) = delete;

    void push_sans_font() const;
    void push_mono_font() const;

    bool new_frame();
    void begin_window();

    void render_and_present();

    DXBits &dx() { return win_bits_.dx; }

    WinBits win_bits_;
    HWND wnd_{};
    ImGuiIO *io_{};
    asio::io_context io_ctx_;
    decltype(asio::make_work_guard(io_ctx_)) io_work_;
    bool app_running_ = true;

    std::vector<ImWchar> glyph_ranges_;
    ImFont *sans_font_{}, *mono_font_{};
};
} // namespace pog