#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <pog/skinning.hpp>

namespace pog {
static glm::vec3 Lerp(glm::vec3 a, glm::vec3 b, float t) { return (1.0f - t) * a + t * b; }

std::optional<glm::vec3> SampleVec3(std::span<ast_ns::Track::Vec3Key const> track, float time) {
    if (track.empty()) {
        return std::nullopt;
    }
    if (track.size() == 1) {
        return track[0].v;
    }
    if (time < 0.0f) {
        time = 0.0f;
    }
    if (time > track.back().time) {
        time = fmodf(time, track.back().time);
    }
    for (int idx = 0; idx < track.size() - 1; ++idx) {
        auto p1 = track[idx];
        auto p2 = track[idx + 1];
        if (p1.time <= time && p2.time >= time) {
            // return p1.v;
            float t = (time - p1.time) / (p2.time - p1.time);
            t = 0.5f;
            return Lerp(p1.v, p2.v, t);
        }
    }
    return std::nullopt;
}

std::optional<glm::quat> SampleQuat(std::span<ast_ns::Track::QuatKey const> track, float time) {
    if (track.empty()) {
        return std::nullopt;
    }
    if (track.size() == 1) {
        return track[0].v;
    }
    if (time < 0.0f) {
        time = 0.0f;
    }
    if (time > track.back().time) {
        time = fmodf(time, track.back().time);
    }
    for (int idx = 0; idx < track.size() - 1; ++idx) {
        auto p1 = track[idx];
        auto p2 = track[idx + 1];
        if (p1.time <= time && p2.time >= time) {
            // return p1.v;
            float t = (time - p1.time) / (p2.time - p1.time);
            t = 0.5f;
            return glm::lerp(p1.v, p2.v, t);
        }
    }
    return std::nullopt;
}

glm::vec3 Pose::BonePos(int boneIdx) const { return paletteCpu[boneIdx][3]; }

Poser::Poser(ast_ns::AstFile const &ast, int animIdx)
    : xMat(ast.bones.size()), xAcc(ast.bones.size(), glm::mat4(1)), parents(ast.bones.size(), 0xFF),
      hdr(ast.animationHeaders[animIdx]), anim(ast.animations[animIdx]) {
    binds.resize(ast.bones.size());
    invBinds.resize(ast.bones.size());

    firstAttachment = static_cast<int>(ast.bones.size() - ast.attachmentCount);
    for (int boneIdx = 0; boneIdx < ast.bones.size(); ++boneIdx) {
        auto const &b = ast.bones[boneIdx];
        glm::mat4 m = glm::make_mat4(ast.bones[boneIdx].transform);
        xMat[boneIdx] = m;

        int child = b.firstChild;
        while (child != 0xFF) {
            parents[child] = boneIdx;
            child = ast.bones[child].nextSibling;
        }
    }

    for (int boneIdx = 0; boneIdx < ast.bones.size(); ++boneIdx) {
        if (auto parent = parents[boneIdx]; parent != 0xFF) {
            binds[boneIdx] = binds[parent] * xMat[boneIdx];
        } else {
            binds[boneIdx] = xMat[boneIdx];
        }
        invBinds[boneIdx] = glm::inverse(binds[boneIdx]);
    }
}

Pose Poser::ResolveAt(float time) const {
    auto const boneCount = binds.size();
    Pose ret;
    ret.paletteCpu.resize(boneCount, glm::mat4(1));
    ret.paletteGpu.resize(boneCount, glm::mat4(1));

    for (int trackIdx = 0; trackIdx < anim.tracks.size(); ++trackIdx) {
        auto const &track = anim.tracks[trackIdx];
        if (track.unk1) {
            continue;
        }

        auto &m = ret.paletteCpu[track.boneIdx];
        glm::vec3 vT(0, 0, 0);
        if (auto trans = SampleVec3(track.translations, time)) {
            vT = glm::vec3(*trans);
        }
        glm::vec3 vS{1, 1, 1};
        if (auto scale = SampleVec3(track.scales, time)) {
            vS = *scale;
        }
        glm::quat vR(1, 0, 0, 0);
        if (auto rot = SampleQuat(track.rotations, time)) {
            vR = *rot;
        }

        // v' = T * R * S * v
        m = glm::scale(glm::translate(glm::mat4{1}, vT) * glm::mat4_cast(vR), vS);

        // HACK(LV): testing some permutations
        glm::mat4 const I{1};
        glm::mat4 const T = glm::translate(I, vT);
        glm::mat4 const R = glm::mat4_cast(vR);
        glm::mat4 const S = glm::scale(I, vS);
        m = T * R * S;
    }

    for (int boneIdx = 0; boneIdx < boneCount; ++boneIdx) {
        if (auto parent = parents[boneIdx]; parent != 0xFF) {
            ret.paletteCpu[boneIdx] = ret.paletteCpu[parent] * ret.paletteCpu[boneIdx];
        }
    }

    for (int boneIdx = 0; boneIdx < boneCount; ++boneIdx) {
        glm::mat4 m = ret.paletteCpu[boneIdx] * invBinds[boneIdx];
        ret.paletteCpu[boneIdx] = m;
        ret.paletteGpu[boneIdx] = glm::transpose(m);
    }

    return ret;
}
} // namespace pog
