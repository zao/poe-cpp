﻿#include "room.hpp"

namespace pog {
static glm::ivec2 ArmSize(arm_ns::ARMFile const &arm) {
    auto &k = std::get<arm_ns::KEntry>(arm.k_1_by_1_.var);
    return glm::ivec2(k.w_, k.h_);
}

RoomDesign::RoomDesign(std::shared_ptr<RoomInfo const> room, std::shared_ptr<GameData> gameData,
                       std::shared_ptr<GraphAssets> graph, RoomDesignFlags flags)
    : room_(room), gameData_(gameData), graph_(graph), flags_(flags) {
    for (auto &[coord, root] : room_->rootEntries) {
        Slot slot{.info = &root};
        BuildCandidateList(slot);
        slots_[coord] = slot;
    }
}

std::map<TileMutation, TileKey> RoomDesign::TileKeyMutations(TileKey baseKey) {
    using Rotation = TileKey::Rotation;
    std::map<TileMutation, TileKey> ret;
    TileKey baseFlip = baseKey.Flipped();

    // Intentionally including R0 here to have a single insertion place
    Rotation rots[]{Rotation::R0, Rotation::R90, Rotation::R180, Rotation::R270};
    for (auto rot : rots) {
        ret[TileMutation{.rot = rot, .flip = false}] = baseKey.Rotated(rot);
        ret[TileMutation{.rot = rot, .flip = true}] = baseFlip.Rotated(rot);
    }
    return ret;
}

RoomDesign::SlotFitResult RoomDesign::FitsInSlot(TileKey const &tile, RoomInfo::EntryInfo const &slot) {
    SlotFitResult res;
    auto RecordFailure = [&res](std::string_view reason) {
        res.success = false;
        if (res.failureReason.empty()) {
            res.failureReason = reason;
        } else {
            res.failureReason += "\n";
            res.failureReason += reason;
        }
    };

    // check tag and size
    if (tile.tag != slot.TagConstraint()) {
        RecordFailure("Tag mismatch");
    }
    if (!res) {
        return res;
    }

    if (tile.size != slot.size) {
        RecordFailure("Size mismatch");
    }
    if (!res) {
        return res;
    }

    // check edges
    // TODO(LV): handle wildcards?
    {
        auto edges = slot.EdgeConstraints();
        for (int i = 0; i < 4; ++i) {
            auto side = (TileSide)i;
            auto I = std::find_if(edges.begin(), edges.end(), [&](auto ec) { return ec.side == side; });

            std::shared_ptr<poe::format::et::EtFile> slotEt, tileEt;
            TileKey::EdgeIndex slotIdx{}, tileIdx{};
            if (auto &etName = tile.etStr[i]) {
                slotEt = gameData_->GetEt(etName.value());
                slotIdx = tile.etVal[i];
            }
            if (I != edges.end()) {
                tileEt = gameData_->GetEt(I->name.value());
                tileIdx = {.real = I->real, .virt = I->virt};
            }

            // Several cases here based on if there's an edge or not
            // slot | tile | outcome
            // -----+------+--------
            // yes  | yes  | must match on ID or either be a wildcard
            // no   | yes  | must be a wildcard
            // yes  | no   | must be a wildcard
            // no   | no   | always good

            auto IsWild = [](auto &et) { return et ? et->id == "WildcardEdge" : false; };

            bool eitherWild = IsWild(slotEt) || IsWild(tileEt);
            if (slotEt && tileEt) {
                if (!eitherWild && slotIdx != tileIdx) {
                    RecordFailure("Edge index mismatch");
                }
            } else if (slotEt || tileEt) {
                if (!eitherWild) {
                    RecordFailure("Sole edge not wildcard");
                }
            }

#if 0
            if (auto etName = tile.etStr[i]) {
                // if the side has an edge string, the slot needs to as well
                if (I != edges.end()) {
                    if (I->name != etName || tile.etVal[i].real != I->real || tile.etVal[i].virt != I->virt) {
                        RecordFailure("Edge mismatch");
                    }
                } else {
                    RecordFailure("Edge in tile but not in slot");
                }
            } else if (I != edges.end()) {
                // if the side doesn't have an edge string, the slot must not have one
                RecordFailure("Edge in slot but not in tile");
            }
#endif
        }
    }
    if (!res) {
        return res;
    }

    // check ground
    // TODO(LV): handle wildcards?
    {
        auto grounds = slot.GroundConstraints();
        for (int i = 0; i < 4; ++i) {
            auto corner = (TileCorner)i;
            auto I = std::find_if(grounds.begin(), grounds.end(), [&](auto gc) { return gc.corner == corner; });
            if (auto gtName = tile.gtStr[i]) {
                // if the corner has a ground string, the slot needs to as well
                if (I != grounds.end()) {
                    if (I->name != gtName) {
                        RecordFailure("Ground mismatch");
                    }
                } else {
                    RecordFailure("Ground in tile but not in slot");
                }
            } else if (I != grounds.end()) {
                // if the corner does not have a ground string, the slot must not have one
                RecordFailure("Ground in slot but not in tile");
            }
        }
    }
    if (!res) {
        return res;
    }

    // check height
    // TODO(LV): check height
    {
        auto slotHeights = slot.CornerHeightAssoc();
        int commonDelta = 0;
        for (int i = 0; i < 4; ++i) {
            auto corner = (TileCorner)i;
            auto slotHeight = slotHeights[corner];
            int delta = slotHeight.height - tile.height[i];
            if (i != 0 && delta != commonDelta) {
                RecordFailure("Corner height delta mismatch");
            }
            commonDelta = delta;
        }
    }
    return res;
}

void RoomDesign::BuildCandidateList(Slot &slot) {
    // Sweep across the tiles in the tileset, collecting possible candidates and locking in the choice if there is
    // an unique fit.
    for (auto &[name, tdt] : graph_->tdts) {
        // TODO(LV): actually test against refererence set
        auto keyMuts = TileKeyMutations(TileKey(*tdt));
        for (auto &[tileMut, tileKey] : keyMuts) {
            auto res = FitsInSlot(tileKey, *slot.info);
            Choice choice{};
            choice.path = name;
            choice.tile = tdt;
            choice.mutation = tileMut;
            if (res) {
                slot.candidates.push_back(choice);
            } else if (flags_ & RoomDesignFlag_StoreRejectionReasons) {
                RejectedCandidate rej{.candidate = choice, .reason = res.failureReason};
                slot.rejects.push_back(std::move(rej));
            }
        }
    }
}

#if 0
Room::Room(std::shared_ptr<poe::format::arm::ARMFile> arm) : arm_(arm) {
    if (auto *k = std::get_if<arm_ns::KEntry>(&arm->k_1_by_1_)) {
        size_ = {k->w_, k->h_};
    } else {
        size_ = {1, 1};
    }

    for (size_t kRowIdx = 0; kRowIdx < arm->k_x_by_y_.size(); ++kRowIdx) {
        auto &kRow = arm->k_x_by_y_[kRowIdx];
        for (size_t kColIdx = 0; kColIdx < kRow.size(); ++kColIdx) {
            auto &entry = kRow[kColIdx];
            auto cover = GetKCover(entry);
            auto lowerLeft = glm::ivec2(kColIdx, kRowIdx);
            if (auto *k = std::get_if<arm_ns::KEntry>(&entry)) {
                using arm_ns::KSlot;

                pog::TileSlot slot{};
                slot.size = {k->w_, k->h_};

                TileKey key;
                TileSide armSideMap[4]{TileSide::Top, TileSide::Right, TileSide::Bottom, TileSide::Left};
                TileCorner armCornerMap[4]{TileCorner::BottomLeft, TileCorner::BottomRight, TileCorner::TopRight,
                                           TileCorner::TopLeft};
                for (int i = 0; i < 4; ++i) {
                    auto side = armSideMap[i];
                    auto corner = armCornerMap[i];
                    if (auto idx = k->tail_[(int)KSlot::EdgeStr + i]) {
                        key.etStr[(int)side] = arm->TableString(idx);
                    }
                    if (auto idx = k->tail_[(int)KSlot::GroundStr + i]) {
                        key.gtStr[(int)corner] = arm->TableString(idx);
                    }
                    key.etVal[(int)side] = {
                        .real = (int)k->tail_[(int)KSlot::EdgeIdx + i * 2],
                        .virt = (int)k->tail_[(int)KSlot::EdgeIdx + i * 2 + 1],
                    };
                    key.height[(int)corner] = (int)k->tail_[(int)KSlot::Height + i];
                }
                key.tag = arm->TableString(k->tail_[(int)KSlot::Tag]);
                {
                    char const *sep{};
                    fmt::memory_buffer buf;
                    fmt::format_to(fmt::appender(buf), "k WxH[{}, {}", k->w_, k->h_);
                    auto tail = std::span(k->tail_);

                    sep = "], Edge[";
                    for (auto val : tail.subspan(0, 4)) {
                        fmt::format_to(fmt::appender(buf), "{}{}", sep, val);
                        sep = ", ";
                    }

                    sep = "], Off[";
                    for (auto val : tail.subspan(4, 8)) {
                        fmt::format_to(fmt::appender(buf), "{}{}", sep, val);
                        sep = ", ";
                    }

                    sep = "], Ground[";
                    for (auto val : tail.subspan(12, 4)) {
                        fmt::format_to(fmt::appender(buf), "{}{}", sep, val);
                        sep = ", ";
                    }

                    sep = "], Height[";
                    for (auto val : tail.subspan(16, 4)) {
                        fmt::format_to(fmt::appender(buf), "{}{}", sep, val);
                        sep = ", ";
                    }
                    fmt::format_to(fmt::appender(buf), "], {}", tail[20]);

                    if (k->last_) {
                        fmt::format_to(fmt::appender(buf), ", Corner[{}]", k->last_.value());
                    }
                    key.source = to_string(buf);
                }
                key.size = slot.size;
                slot.key = key;

                slots_[lowerLeft] = slot;
                slotKind_[lowerLeft] = 'k';
            } else if (auto *f = std::get_if<arm_ns::FEntry>(&entry)) {
                pog::TileSlot slot;
                slot.size = {1, 1};

                TileKey key;
                key.tag = arm->TableString(f->value_);
                key.size = {1, 1};

                slot.key = key;

                slots_[lowerLeft] = slot;
                slotKind_[lowerLeft] = 'f';
            } else if (std::get_if<arm_ns::SEntry>(&entry)) {
                pog::TileSlot slot;
                slot.size = {1, 1};

                TileKey key;
                key.size = {1, 1};
                slot.key = key;

                slots_[lowerLeft] = slot;
                slotKind_[lowerLeft] = 's';
            } else if (std::get_if<arm_ns::OEntry>(&entry)) {
                pog::TileSlot slot;
                slot.size = {1, 1};

                TileKey key;
                key.size = {1, 1};
                slot.key = key;

                slots_[lowerLeft] = slot;
                slotKind_[lowerLeft] = 'o';
            } else if (std::get_if<arm_ns::NEntry>(&entry)) {
                pog::TileSlot slot;
                slot.size = {1, 1};

                TileKey key;
                key.size = {0, 0};
                slot.key = key;

                slots_[lowerLeft] = slot;
                slotKind_[lowerLeft] = 'n';
            }
        }
    }
}
#endif

constexpr TileSide const armSideMap[4]{TileSide::Bottom, TileSide::Right, TileSide::Top, TileSide::Left};
constexpr TileCorner const armCornerMap[4]{TileCorner::BottomLeft, TileCorner::BottomRight, TileCorner::TopRight,
                                           TileCorner::TopLeft};

RoomInfo::RoomInfo(std::shared_ptr<poe::format::arm::ARMFile> arm) : arm(arm) {
    auto roomSize = RoomSize();
    for (int row = 0; row < roomSize.y; ++row) {
        for (int col = 0; col < roomSize.x; ++col) {
            glm::ivec2 coord{col, row};
            auto *entry = &arm->k_x_by_y_[row][col];
            auto entryPtr = std::shared_ptr<arm_ns::Entry>(arm, entry);
            EntryInfo entryInfo(arm, entryPtr, coord);

            if (auto *n = entryInfo.AsN()) {
                continue;
            }
            rootEntries[{col, row}] = entryInfo;

            auto coordMin = entryInfo.CoordMin();
            auto coordMax = entryInfo.CoordMax();

            for (int coverRow = coordMin.y; coverRow <= coordMax.y; ++coverRow) {
                for (int coverCol = coordMin.x; coverCol <= coordMax.x; ++coverCol) {
                    glm::ivec2 coverCoord{coverCol, coverRow};
                    if (coord != coverCoord) {
                        proxyEntries[coverCoord] = coord;
                    }
                }
            }
        }
    }
}

glm::ivec2 RoomInfo::RoomSize() const {
    auto *entry = &arm->k_1_by_1_;
    if (auto *k = std::get_if<arm_ns::KEntry>(&entry->var)) {
        return {k->w_, k->h_};
    }
    return {1, 1};
}

RoomInfo::EntryInfo::EntryInfo(std::shared_ptr<arm_ns::ARMFile const> arm, std::shared_ptr<arm_ns::Entry const> entry,
                               glm::ivec2 coord)
    : arm(arm), entry(entry), coord(coord), size(1, 1) {
    if (auto *k = AsK()) {
        size = {k->w_, k->h_};
        int64_t cornerIndicator = k->last_.value_or(0);
        constexpr TileCorner const originCorners[]{TileCorner::BottomLeft, TileCorner::BottomRight,
                                                   TileCorner::TopRight, TileCorner::TopLeft};
        originCorner = originCorners[cornerIndicator];
    }
}

std::optional<std::string> RoomInfo::EntryInfo::TagConstraint() const {
    if (auto *k = AsK()) {
        return arm->TableString(k->tail_[(int)arm_ns::KSlot::Tag]);
    }
    if (auto *f = AsF()) {
        return arm->TableString(f->value_);
    }
    return std::nullopt;
}

namespace {
bool VerticalSide(TileSide side) { return side == TileSide::Left || side == TileSide::Right; }
bool HorizontalSide(TileSide side) { return side == TileSide::Bottom || side == TileSide::Top; }
} // namespace

absl::InlinedVector<RoomInfo::EdgeConstraint, 4> RoomInfo::EntryInfo::EdgeConstraints() const {
    absl::InlinedVector<EdgeConstraint, 4> ret;
    using arm_ns::KSlot;
    if (auto *k = AsK()) {
        for (int i = 0; i < 4; ++i) {
            auto side = armSideMap[i];
            auto *indices = &k->tail_[(int)KSlot::EdgeIdx + i * 2];
            int limitIdx = VerticalSide(side) ? (size.y * 3) : (size.x * 3);
            int real = (int)indices[0];
            int virt = (int)indices[1];
            auto nameIdx = k->tail_[(int)KSlot::EdgeStr + i];
            auto name = arm->TableString(nameIdx);
            if (real != limitIdx && name) {
                EdgeConstraint ec{
                    .side = side,
                    .name = name,
                    .real = real,
                    .virt = virt,
                };
                ret.push_back(std::move(ec));
            }
        }
    }
    return ret;
}

absl::InlinedVector<RoomInfo::GroundConstraint, 4> RoomInfo::EntryInfo::GroundConstraints() const {
    absl::InlinedVector<GroundConstraint, 4> ret;
    using arm_ns::KSlot;
    if (auto *k = AsK()) {
        for (int i = 0; i < 4; ++i) {
            auto corner = armCornerMap[i];
            auto nameIdx = k->tail_[(int)KSlot::GroundStr + i];
            auto name = arm->TableString(nameIdx);
            if (name) {
                GroundConstraint gc{
                    .corner = corner,
                    .name = name,
                };
                ret.push_back(std::move(gc));
            }
        }
    }
    return ret;
}

absl::InlinedVector<RoomInfo::HeightConstraint, 4> RoomInfo::EntryInfo::CornerHeights() const {
    absl::InlinedVector<HeightConstraint, 4> ret;
    using arm_ns::KSlot;
    if (auto *k = AsK()) {
        for (int i = 0; i < 4; ++i) {
            auto corner = armCornerMap[i];
            auto height = (int)k->tail_[(int)KSlot::Height + i];
            HeightConstraint hc{
                .corner = corner,
                .height = height,
            };
            ret.push_back(std::move(hc));
        }
    }
    return ret;
}

absl::flat_hash_map<TileSide, RoomInfo::EdgeConstraint> RoomInfo::EntryInfo::EdgeConstraintAssoc() const {
    decltype(EdgeConstraintAssoc()) ret;
    for (auto &ec : EdgeConstraints()) {
        ret[ec.side] = ec;
    }
    return ret;
}

absl::flat_hash_map<TileCorner, RoomInfo::GroundConstraint> RoomInfo::EntryInfo::GroundConstraintAssoc() const {
    decltype(GroundConstraintAssoc()) ret;
    for (auto &gc : GroundConstraints()) {
        ret[gc.corner] = gc;
    }
    return ret;
}

absl::flat_hash_map<TileCorner, RoomInfo::HeightConstraint> RoomInfo::EntryInfo::CornerHeightAssoc() const {
    decltype(CornerHeightAssoc()) ret;
    for (auto &hc : CornerHeights()) {
        ret[hc.corner] = hc;
    }
    return ret;
}

glm::ivec2 RoomInfo::EntryInfo::CoordMin() const {
    if (size == glm::ivec2{1, 1}) {
        return coord;
    }
    glm::ivec2 jut = size - 1;
    switch (originCorner) {
    case TileCorner::BottomLeft:
        return coord;
    case TileCorner::BottomRight:
        return coord - glm::ivec2{jut.x, 0};
    case TileCorner::TopRight:
        return coord - jut;
    case TileCorner::TopLeft:
        return coord - glm::ivec2{0, jut.y};
    default:
        // unreachable
        return {};
    }
}

glm::ivec2 RoomInfo::EntryInfo::CoordMax() const {
    if (size == glm::ivec2{1, 1}) {
        return coord;
    }
    glm::ivec2 jut = size - 1;
    switch (originCorner) {
    case TileCorner::BottomLeft:
        return coord + jut;
    case TileCorner::BottomRight:
        return coord + glm::ivec2(0, jut.y);
    case TileCorner::TopRight:
        return coord;
    case TileCorner::TopLeft:
        return coord + glm::ivec2(jut.x, 0);
    default:
        // unreachable
        return {};
    }
}

char RoomInfo::EntryInfo::KindChar() const {
    return AsF() ? 'f' : AsK() ? 'k' : AsN() ? 'n' : AsO() ? 'o' : AsS() ? 's' : '?';
}

std::optional<glm::ivec2> RoomInfo::ResolveCoord(glm::ivec2 coord) const {
    if (auto proxyI = proxyEntries.find(coord); proxyI != proxyEntries.end()) {
        coord = proxyI->second;
    }
    if (rootEntries.contains(coord)) {
        return coord;
    }
    return std::nullopt;
}

} // namespace pog