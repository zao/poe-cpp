#pragma once
#include <vector>
#include <glm/fwd.hpp>

#include <poe/format/ast.hpp>

namespace pog {
namespace ast_ns = poe::format::ast;

std::optional<glm::vec3> SampleVec3(std::span<ast_ns::Track::Vec3Key const> track, float time);
std::optional<glm::quat> SampleQuat(std::span<ast_ns::Track::QuatKey const> track, float time);

struct Pose {
    glm::vec3 BonePos(int boneIdx) const;

    std::vector<glm::mat4> paletteCpu;
    std::vector<glm::mat4> paletteGpu;
};

struct Poser {
    explicit Poser(ast_ns::AstFile const &ast, int animIdx);

    Pose ResolveAt(float time) const;

    std::vector<glm::mat4> invBinds;
    std::vector<glm::mat4> binds;

    std::vector<glm::mat4> xMat;
    std::vector<glm::mat4> xAcc;
    std::vector<uint8_t> parents;
    ast_ns::AnimationHeader const &hdr;
    ast_ns::Animation const &anim;
    int firstAttachment;
};
}
