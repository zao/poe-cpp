#include <glm/glm.hpp>
#include <vector>

namespace pog {
struct GameCameraParameters {
    glm::mat4 projection_transform;
    glm::mat4 R;
    glm::vec4 tScaled;
};

extern std::vector<GameCameraParameters> gameCameraParameters;
} // namespace pog