#pragma once

struct GLFWwindow;

#include <atlbase.h>
#include <dxgi.h>
#include <d3d11.h>
#include <glm/vec2.hpp>

namespace pog {
struct DxContext {
    DxContext();

    IDXGIFactory *DxgiFactory() const { return dxgiFactory_; }
    ID3D11Device *Device() const { return d3dDev_; }
    ID3D11DeviceContext *Context() const { return d3dCtx_; }

  private:
    void SetupResolutionIndependentMembers();

    CComPtr<IDXGIFactory> dxgiFactory_;
    CComPtr<ID3D11Device> d3dDev_;
    CComPtr<ID3D11DeviceContext> d3dCtx_;
};
} // namespace pog