#pragma once

#include <absl/hash/hash.h>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <optional>
#include <string>

namespace poe::format::arm {
struct ARMFile;
struct Entry;
}

namespace poe::format::tdt {
struct Tdt;
}

namespace poe::format::tgm {
struct TgmFile;
}

namespace poe::format::tgt {
struct Tgt;
}

namespace pog {
struct GlmLess {
    template <size_t N, typename T> bool operator()(glm::vec<N, T> a, glm::vec<N, T> b) const {
        static_assert(N >= 2 && N <= 4);
        if (a.x != b.x) {
            return a.x < b.x;
        }
        if constexpr (N == 2) {
            return a.y < b.y;
        } else {
            if (a.y != b.y) {
                return a.y < b.y;
            }
            if constexpr (N == 3) {
                return a.z < b.z;
            } else {
                if (a.z != b.z)
                    return a.z < b.z;
                return a.w < b.w;
            }
        }
    }
};

struct GlmHash {
    template <typename T> size_t operator()(glm::vec<2, T> v) const { return absl::HashOf(v.x, v.y); }
    template <typename T> size_t operator()(glm::vec<3, T> v) const { return absl::HashOf(v.x, v.y, v.z); }
    template <typename T> size_t operator()(glm::vec<4, T> v) const { return absl::HashOf(v.x, v.y, v.z, v.w); }
};

struct SubTileData {
    std::shared_ptr<poe::format::tgt::Tgt> tgt;
    std::shared_ptr<poe::format::tgm::TgmFile> tgm;
};

struct TileData {
    std::string path;
    std::shared_ptr<poe::format::tdt::Tdt> tdt;
    std::map<glm::ivec2, SubTileData, GlmLess> subTiles;
};


struct KCover {
    char kind;
    glm::ivec2 i0, i1;
};

KCover GetKCover(poe::format::arm::Entry const &kEntry);

enum class TileSide { Left = 0, Bottom, Right, Top };
enum class TileCorner { TopLeft = 0, BottomLeft, BottomRight, TopRight };

char8_t const *SideSymbol(TileSide side);
char8_t const *CornerSymbol(TileCorner corner);

struct TileKey {
    enum class Rotation { R0 = 0, R90, R180, R270 };

    TileKey() = default;
    TileKey(poe::format::arm::ARMFile const &arm, glm::ivec2 cellCoord);
    TileKey(poe::format::arm::ARMFile const &arm, poe::format::arm::Entry const *entry);
    explicit TileKey(poe::format::tdt::Tdt const &tdt);

    TileKey Flipped(bool flip = true) const;
    TileKey Rotated(Rotation rot) const;

    struct EdgeIndex {
        int real, virt;

        auto operator<=>(EdgeIndex const &rhs) const = default;
    };

    std::array<EdgeIndex, 4> etVal{};
    std::array<std::optional<std::string>, 4> etStr, gtStr;
    std::array<int, 4> height{};
    std::optional<std::string> tag;
    std::string source;
    glm::ivec2 size;
};

struct TileMutation {
    TileKey::Rotation rot;
    bool flip;

    auto operator<=>(TileMutation const &rhs) const = default;
};
}