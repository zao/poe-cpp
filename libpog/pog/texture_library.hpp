#pragma once

#include <poe/io/vfs.hpp>

#include <pog/platform.hpp>

#include <comdef.h>
#include <atlbase.h>
#include <d3d11.h>

#include <DirectXTK/DDSTextureLoader.h>

#include <unordered_set>

namespace pog {
struct LoadedTexture {
    CComPtr<ID3D11Resource> res_;
    CComPtr<ID3D11ShaderResourceView> srv_;
};

struct TextureLibrary {
    explicit TextureLibrary(std::shared_ptr<poe::io::vfs> vfs, DXBits const &dx);

    std::shared_ptr<LoadedTexture> load(std::string source_path);

private:
    std::shared_ptr<LoadedTexture> load_dds(std::span<std::byte const> data, std::string source_path,
                                            std::string const &load_path);

    void record_success(std::vector<std::string> const &history, std::shared_ptr<LoadedTexture> tex);

    void record_failure(std::vector<std::string> const &history);

    std::shared_ptr<poe::io::vfs> vfs_;
    DXBits const &dx_;
    std::unordered_map<std::string, std::weak_ptr<LoadedTexture>> textures_;
    std::unordered_set<std::string> unreachable_;
};
} // namespace pog