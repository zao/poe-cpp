#include <pog/texture_library.hpp>

#include <poe/format/dds.hpp>
#include <loguru/loguru.hpp>

pog::TextureLibrary::TextureLibrary(std::shared_ptr<poe::io::vfs> vfs, DXBits const &dx): vfs_(vfs), dx_(dx) {}

std::shared_ptr<pog::LoadedTexture> pog::TextureLibrary::load(std::string source_path) {
    std::string path = source_path;
    std::vector<std::string> load_history;
    while (true) {
        // Check first if cached or known unreachable.
        if (unreachable_.contains(path)) {
            return {};
        }
        if (auto I = textures_.find(path); I != textures_.end()) {
            if (auto sp = I->second.lock()) {
                return sp;
            }
        }

        load_history.push_back(path);
        // Not cached or known unreachable, try loading it.
        if (auto actual_fh = vfs_->open_file(poe::util::path(path))) {
            auto tex = load_dds(actual_fh->read_all(), source_path, path);
            if (tex) {
                record_success(load_history, tex);
                return tex;
            } else {
                record_failure(load_history);
                return {};
            }
        }
        auto proxy_path = path + ".header";
        load_history.push_back(proxy_path);
        if (auto proxy_fh = vfs_->open_file(poe::util::path(proxy_path))) {
            auto header = poe::format::dds::parse_dds_header(proxy_fh->read_all());
            if (header) {
                if (auto *sym = std::get_if<poe::format::dds::proxy_texture::symlink_t>(&header->body_)) {
                    path = *sym;
                    continue;
                } else {
                    auto const &payload = std::get<poe::format::dds::proxy_texture::payload_t>(header->body_);
                    auto tex = load_dds(payload, source_path, proxy_path);
                    if (tex) {
                        record_success(load_history, tex);
                        return tex;
                    }
                }
            }
        }

        // We either found neither of DDS or proxy, or the proxy failed loading.
        record_failure(load_history);
        return {};
    }
}

std::shared_ptr<pog::LoadedTexture> pog::TextureLibrary::load_dds(std::span<std::byte const> data,
    std::string source_path, std::string const &load_path) {
    auto ret = std::make_shared<LoadedTexture>();
    HRESULT hr = DirectX::CreateDDSTextureFromMemory(this->dx_.dev_, (uint8_t const *)data.data(), data.size(),
                                                     &ret->res_, &ret->srv_);
    if (FAILED(hr)) {
        auto err_msg = poe::util::to_string((char16_t const *)_com_error(hr).ErrorMessage());
        LOG_F(WARNING, "Could not create texture for \"{}\" wanted by \"{}\": {}", load_path, source_path, err_msg);
    }
    return ret;
}

void pog::TextureLibrary::record_success(std::vector<std::string> const &history, std::shared_ptr<LoadedTexture> tex) {
    for (auto const &p : history) {
        textures_.insert({p, tex});
    }
}

void pog::TextureLibrary::record_failure(std::vector<std::string> const &history) {
    for (auto const &p : history) {
        unreachable_.insert(p);
    }
}
