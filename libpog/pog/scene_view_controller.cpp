#include "scene_view_controller.hpp"

#include <pog/game_data.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <imgui.h>

#include <poe/util/install_location.hpp>

namespace pog {
glm::vec3 FixedCamera::Position() const {
    return look.at + (glm::vec3)(glm::rotate(glm::rotate(glm::identity<glm::mat4>(), glm::radians(look.azimuth_deg),
                                                         glm::vec3{0.0f, 0.0f, 1.0f}),
                                             glm::radians(look.altitude_deg), glm::vec3{0.0f, 1.0f, 0.0f}) *
                                 glm::vec4{look.distance, 0.0f, 0.0f, 1.0f});
}

glm::mat4 FixedCamera::ViewMatrix() const {
    glm::vec3 cam_eye = Position();
    glm::mat4 viewMat = glm::lookAtLH(cam_eye, look.at, glm::vec3{0, 0, -1});
    return viewMat;
}

glm::mat4 FixedCamera::ProjectionMatrix() const {
    glm::mat4 projMat = glm::perspectiveFovLH_ZO(glm::radians(fovDeg), (float)winSize.x, (float)winSize.y, zNear, zFar);
    return projMat;
}

SceneViewController::SceneViewController(std::shared_ptr<pog::DxContext> dx) : dx_(dx) {}

void RunCameraControlUi(FixedCamera &cam) {
    if (ImGui::Begin("Camera control")) {
        ImGui::DragFloat("Azimuth", &cam.look.azimuth_deg, 1.0f, -360.0f, 360.0f, "%.0f deg");
        cam.look.azimuth_deg = fmodf(cam.look.azimuth_deg + 720.0f, 360.0f);
        ImGui::DragFloat("Altitude", &cam.look.altitude_deg, 1.0f, -89.9f, 89.9f, "%.2f deg");
        ImGui::DragFloat("Distance", &cam.look.distance, 10.0f, 100.0f, 10000.0f);
        ImGui::DragFloat("Near plane Z", &cam.zNear, 0.1f, 0.1f, cam.zFar);
        ImGui::DragFloat("Far plane Z", &cam.zFar, 0.1f, cam.zNear, 10000.0f);
        ImGui::DragFloat3("Target", glm::value_ptr(cam.look.at), 10.0f, -10000.0f, 10000.0f);
    }
    ImGui::End();
}
} // namespace pog
