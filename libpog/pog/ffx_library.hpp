#pragma once

#include <map>
#include <memory>
#include <string>

namespace poe::format::ffx {
struct Declarations;
struct Fragment;
struct ParsedFfx;
}

namespace poe::io {
struct vfs;
}

namespace pog {
struct FFXLibrary {
    explicit FFXLibrary(std::shared_ptr<poe::io::vfs> vfs);

    poe::format::ffx::Declarations const *LookupDeclarations(std::string_view name) const;
    poe::format::ffx::Fragment const *LookupFragment(std::string_view name) const;

  private:
    void Populate();

  public:
    std::shared_ptr<poe::io::vfs> vfs_;
    std::map<std::string, std::shared_ptr<poe::format::ffx::ParsedFfx>> parsed_ffx_files_;
};
} // namespace pog