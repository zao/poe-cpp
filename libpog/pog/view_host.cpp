#include "view_host.hpp"

#include <poe/util/install_location.hpp>
#include <poe/util/random_access_file.hpp>

#include <glm/gtc/type_ptr.hpp>

namespace pog {
ViewHost::ViewHost(pog::DxContext const &dx, HWND wnd) : dx_(dx), wnd_(wnd) {
    auto assetRoot = *poe::util::executable_dir();
    HRESULT hr = S_OK;
    auto *dev = dx.Device();

    struct FsVert {
        glm::vec2 pos;
        glm::vec2 tc;
    };

    /* 0-1  0: (-1, +1), (0.0, 0.0)
     * |/   1: (+3, +1)  (2.0, 0.0)
     * 2    2: (-1, -3), (0.0, 2.0)
     */
    constexpr std::array<FsVert, 3> const fsVerts{
        FsVert{.pos = {-1.0f, +1.0f}, .tc = {0.0f, 0.0f}},
        FsVert{.pos = {+3.0f, +1.0f}, .tc = {2.0f, 0.0f}},
        FsVert{.pos = {-1.0f, -3.0f}, .tc = {0.0f, 2.0f}},
    };

    D3D11_BUFFER_DESC fsGeomDesc{
        .ByteWidth = sizeof(fsVerts),
        .Usage = D3D11_USAGE_IMMUTABLE,
        .BindFlags = D3D11_BIND_VERTEX_BUFFER,
        .CPUAccessFlags = 0,
        .MiscFlags = sizeof(FsVert),
    };

    D3D11_SUBRESOURCE_DATA fsGeomSrd{
        .pSysMem = fsVerts.data(),
        .SysMemPitch = 0,
        .SysMemSlicePitch = 0,
    };
    hr = dev->CreateBuffer(&fsGeomDesc, &fsGeomSrd, &fsGeom_);
    fsStride_ = sizeof(FsVert);
    fsOffset_ = 0;

    auto SlurpAsset = [&assetRoot](std::string_view filename) {
        poe::util::random_access_file fh(assetRoot / filename);
        std::vector<std::byte> ret(fh.size());
        fh.read_exact(0, ret.data(), ret.size());
        return ret;
    };

    std::vector<std::byte> blendVsData = SlurpAsset("FsBlend-vs_4_0.cso"),
                           blendPsData = SlurpAsset("FsBlend-ps_4_0.cso");

    hr = dev->CreateVertexShader(blendVsData.data(), blendVsData.size(), nullptr, &blendVs_);
    hr = dev->CreatePixelShader(blendPsData.data(), blendPsData.size(), nullptr, &blendPs_);

    constexpr std::array<D3D11_INPUT_ELEMENT_DESC, 2> blendFsIeds{
        D3D11_INPUT_ELEMENT_DESC{
            "NdcPosition",
            0,
            DXGI_FORMAT_R32G32_FLOAT,
            0,
            0,
            D3D11_INPUT_PER_VERTEX_DATA,
            0,
        },
        D3D11_INPUT_ELEMENT_DESC{
            "TexCoord",
            0,
            DXGI_FORMAT_R32G32_FLOAT,
            0,
            D3D11_APPEND_ALIGNED_ELEMENT,
            D3D11_INPUT_PER_VERTEX_DATA,
            0,
        },
    };
    hr = dev->CreateInputLayout(blendFsIeds.data(), (UINT)blendFsIeds.size(), blendVsData.data(), blendVsData.size(),
                                &blendFsLayout_);

    D3D11_SAMPLER_DESC rtSamplerDesc{
        .Filter = D3D11_FILTER_MIN_MAG_MIP_POINT,
        .AddressU = D3D11_TEXTURE_ADDRESS_CLAMP,
        .AddressV = D3D11_TEXTURE_ADDRESS_CLAMP,
        .AddressW = D3D11_TEXTURE_ADDRESS_CLAMP,
        .MipLODBias{},
        .MaxAnisotropy = 1,
        .ComparisonFunc = D3D11_COMPARISON_NEVER,
        .BorderColor{},
        .MinLOD = -D3D11_FLOAT32_MAX,
        .MaxLOD = +D3D11_FLOAT32_MAX,
    };
    hr = dev->CreateSamplerState(&rtSamplerDesc, &blendSampler_);

    D3D11_BLEND_DESC rtBlendDesc{
        .AlphaToCoverageEnable{},
        .IndependentBlendEnable{},
        .RenderTarget{
            {
                .BlendEnable = TRUE,
                .SrcBlend = D3D11_BLEND_SRC_ALPHA,
                .DestBlend = D3D11_BLEND_INV_SRC_ALPHA,
                .BlendOp = D3D11_BLEND_OP_ADD,
                .SrcBlendAlpha = D3D11_BLEND_ONE,
                .DestBlendAlpha = D3D11_BLEND_ZERO,
                .BlendOpAlpha = D3D11_BLEND_OP_ADD,
                .RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL,
            },
        },
    };
    hr = dev->CreateBlendState(&rtBlendDesc, &rtBlend_);
}

void ViewHost::Resize(glm::ivec2 size) { HandleResize(size); }

void ViewHost::Clear() {
    auto *ctx = dx_.Context();
    ctx->ClearRenderTargetView(resDep_.rtv.p, glm::value_ptr(clearColor_));
}

void ViewHost::BlendTarget(ID3D11ShaderResourceView *srcSrv) {
    HRESULT hr = S_OK;
    auto *ctx = dx_.Context();
    ctx->VSSetShader(blendVs_, nullptr, 0);
    ctx->PSSetShader(blendPs_, nullptr, 0);
    ctx->IASetVertexBuffers(0, 1, &fsGeom_.p, &fsStride_, &fsOffset_);
    ctx->IASetIndexBuffer(nullptr, DXGI_FORMAT_UNKNOWN, 0);
    ctx->IASetInputLayout(blendFsLayout_);
    ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    ctx->OMSetRenderTargets(1, &resDep_.rtv.p, nullptr);

    ctx->OMSetBlendState(rtBlend_, nullptr, ~0);
    ctx->PSSetShaderResources(0, 1, &srcSrv);
    ctx->PSSetSamplers(0, 1, &blendSampler_.p);
    ctx->Draw(3, 0);

    ctx->OMSetBlendState(nullptr, nullptr, ~0);

    ID3D11ShaderResourceView *nullSrv{};
    ctx->PSSetShaderResources(0, 1, &nullSrv);

    ID3D11SamplerState *nullSampler{};
    ctx->PSSetSamplers(0, 1, &nullSampler);
}

void ViewHost::ResolveTarget(ID3D11Texture2D *rt) {
    HRESULT hr = S_OK;
    dx_.Context()->ResolveSubresource(resDep_.rt, 0, rt, 0, DXGI_FORMAT_B8G8R8A8_UNORM);
}

void ViewHost::HandleResize(glm::ivec2 newSize) {
    HRESULT hr = S_OK;
    if (newSize != fbSize_) {
        resDep_.swapChain.Release();
        resDep_.rt.Release();
        resDep_.rtv.Release();

        DXGI_SWAP_CHAIN_DESC swapChainDesc{
            .BufferDesc{
                .Width = (UINT)newSize.x,
                .Height = (UINT)newSize.y,
                .RefreshRate{0, 0},
                .Format = DXGI_FORMAT_B8G8R8A8_UNORM,
                .ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED,
                .Scaling = DXGI_MODE_SCALING_UNSPECIFIED,
            },
            .SampleDesc{
                .Count = 1,
                .Quality = 0,
            },
            .BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT,
            .BufferCount = 2,
            .OutputWindow = wnd_,
            .Windowed = TRUE,
            .SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD,
            .Flags = 0,
        };
        hr = dx_.DxgiFactory()->CreateSwapChain(dx_.Device(), &swapChainDesc, &resDep_.swapChain);
        fbSize_ = newSize;
        CComPtr<IDXGIResource> backbuffer;
        hr = resDep_.swapChain->GetBuffer(0, IID_PPV_ARGS(&backbuffer));
        resDep_.rt = backbuffer;
        hr = dx_.Device()->CreateRenderTargetView(resDep_.rt, nullptr, &resDep_.rtv);
    }
}

LayerTarget::LayerTarget(pog::DxContext const &dx) : dx_(dx) {
    HRESULT hr = S_OK;
    auto *dev = dx_.Device();
    D3D11_DEPTH_STENCIL_DESC dssDesc{.DepthEnable = TRUE,
                                     .DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL,
                                     .DepthFunc = D3D11_COMPARISON_LESS,
                                     .StencilEnable = FALSE,
                                     .StencilReadMask = 0xFF,
                                     .StencilWriteMask = 0xFF,
                                     .FrontFace =
                                         {
                                             .StencilFailOp = D3D11_STENCIL_OP_KEEP,
                                             .StencilDepthFailOp = D3D11_STENCIL_OP_KEEP,
                                             .StencilPassOp = D3D11_STENCIL_OP_KEEP,
                                             .StencilFunc = D3D11_COMPARISON_ALWAYS,
                                         },
                                     .BackFace = {
                                         .StencilFailOp = D3D11_STENCIL_OP_KEEP,
                                         .StencilDepthFailOp = D3D11_STENCIL_OP_KEEP,
                                         .StencilPassOp = D3D11_STENCIL_OP_KEEP,
                                         .StencilFunc = D3D11_COMPARISON_ALWAYS,
                                     }};
    hr = dev->CreateDepthStencilState(&dssDesc, &dss);
}

void LayerTarget::Resize(glm::ivec2 size) {
    if (size != fbSize_) {
        HRESULT hr = S_OK;
        auto *dev = dx_.Device();
        rt.Release();
        depth.Release();
        dsv.Release();
        rtv.Release();
        rtSrv.Release();

        fbSize_ = size;

        D3D11_TEXTURE2D_DESC rtDesc{
            .Width = (UINT)size.x,
            .Height = (UINT)size.y,
            .MipLevels = 1,
            .ArraySize = 1,
            .Format = DXGI_FORMAT_B8G8R8A8_UNORM,
            .SampleDesc{
                .Count = 1,
                .Quality = 0,
            },
            .Usage = D3D11_USAGE_DEFAULT,
            .BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE,
            .CPUAccessFlags = 0,
            .MiscFlags = 0,
        };
        D3D11_TEXTURE2D_DESC depthDesc = rtDesc;
        depthDesc.Format = DXGI_FORMAT_D32_FLOAT;
        depthDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;

        hr = dev->CreateTexture2D(&rtDesc, nullptr, &rt);
        hr = dev->CreateTexture2D(&depthDesc, nullptr, &depth);

        hr = dev->CreateRenderTargetView(rt, nullptr, &rtv);
        hr = dev->CreateDepthStencilView(depth, nullptr, &dsv);

        D3D11_SHADER_RESOURCE_VIEW_DESC rtSrvDesc{
            .Format = rtDesc.Format,
            .ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D,
            .Texture2D =
                {
                    .MostDetailedMip = 0,
                    .MipLevels = (UINT)-1,
                },
        };

        hr = dev->CreateShaderResourceView(rt, &rtSrvDesc, &rtSrv);
    }
}

glm::ivec2 LayerTarget::Size() const { return fbSize_; }
} // namespace pog