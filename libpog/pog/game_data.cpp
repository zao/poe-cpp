﻿#include "game_data.hpp"
#define _AMD64_

#include <future>
#include <regex>
#include <set>
#include <thread>

#include <absl/container/node_hash_map.h>

#include <debugapi.h>

namespace pog {
#define GAME_DATA_FILE_LOOKUP(TypeName, Parser, Extension)                                                             \
    absl::node_hash_map<std::string, Get##TypeName##Result> cached##TypeName##Files_;

struct GameData::Private {
    GAME_DATA_FILE_LOOKUP_TYPES

    absl::node_hash_map<std::string, std::shared_ptr<GraphAssets>> graphs_;
    std::string desiredGraphPath_;
    std::future<void> pendingGraphFut_;
    std::shared_ptr<GraphAssets> currentGraph_;
};
#undef GAME_DATA_FILE_LOOKUP

void GameData::RequestGraphChange(std::string_view path) {
    using namespace std::chrono_literals;
    auto lock = std::scoped_lock(cacheMtx_);
    std::string pathKey(path.data(), path.size());
    priv_->desiredGraphPath_ = pathKey;

    if (auto I = priv_->graphs_.find(pathKey); I != priv_->graphs_.end()) {
        priv_->currentGraph_ = I->second;
    } else {
        priv_->pendingGraphFut_ = std::async(
            std::launch::async, [ self = shared_from_this(), pathKey ]() -> auto {
                auto graph = self->GetGraph(pathKey);
                auto lock = std::scoped_lock(self->cacheMtx_);
                // Protect against racing requests, last one issued wins and the rest discard their results when they
                // complete.
                if (self->priv_->desiredGraphPath_ == pathKey) {
                    self->priv_->currentGraph_ = graph;
                }
            });
    }
}

std::shared_ptr<GraphAssets> GameData::CurrentGraph() {
    auto lock = std::scoped_lock(cacheMtx_);
    return priv_->currentGraph_;
}

GameData::GameData() : priv_(std::make_unique<Private>()) {}

std::shared_ptr<GraphAssets> GameData::GetGraph(std::string_view path) {
    poe::util::path graphPath(path);
    auto ret = std::make_shared<GraphAssets>();

    poe::util::path masterPath;
    {
        auto fh = vfs->open_file(graphPath);
        auto graphData = fh->read_all();
        if (graphPath.extension() == ".tgr") {
            ret->tgr = poe::format::tgr::parse_tgr(graphData);
            masterPath = poe::util::path(ret->tgr->master_file_);
        } else if (graphPath.extension() == ".dgr") {
            ret->dgr = poe::format::dgr::parse_dgr(graphData);
            masterPath = poe::util::path(ret->dgr->master_file_);
        } else {
            return {};
        }
    }

    {
        auto fh = vfs->open_file(poe::util::path(masterPath));
        ret->tsi = poe::format::tsi::parse_tsi(fh->read_all());
    }

    auto BuildPath = [&masterPath](std::string_view tail) -> poe::util::path {
        if (tail.find('/') != tail.npos) {
            return poe::util::path(tail);
        }
        return masterPath.make_relative(tail);
    };

    std::set<std::string> wantedArms, wantedTdts;
    if (auto str = ret->tsi->find_quoted_field("FillTiles")) {
        auto fh = vfs->open_file(BuildPath(*str));
        ret->gft = poe::format::gft::parse_gft(fh->read_all());

        for (auto &[key, section] : ret->gft->sections) {
            for (auto &tdt : section.entries) {
                auto fullPath = BuildPath(tdt.path);
                if (tdt.path.ends_with(".tdt")) {
                    wantedTdts.insert(fullPath.path_);
                } else if (tdt.path.ends_with(".arm")) {
                    wantedArms.insert(fullPath.path_);
                }
            }
        }
    }

    if (auto str = ret->tsi->find_quoted_field("RoomSet")) {
        auto fh = vfs->open_file(BuildPath(*str));
        ret->rs = poe::format::rs::parse_rs(fh->read_all());

        for (auto &entry : ret->rs->entries_) {
            auto fullPath = BuildPath(entry.arm_ref_);
            wantedArms.insert(fullPath.path_);
        }
    }

    for (auto &fullPath : wantedArms) {
        auto fh = vfs->open_file(poe::util::path(fullPath));
        auto arm = poe::format::arm::parse_arm(fh->read_all());
        ret->arms[fullPath] = arm;
    }

    if (auto str = ret->tsi->find_quoted_field("TileSet")) {
        auto fh = vfs->open_file(BuildPath(*str));
        if (auto tst = poe::format::tst::parse_tst(fh->read_all())) {
            ret->tst = std::make_shared<poe::format::tst::Tst>(std::move(*tst));
        }

        for (auto &sec : ret->tst->parts) {
            for (auto &entry : sec) {
                auto fullPath = BuildPath(entry.tdt);
                auto pathStr = fullPath.path_;
                wantedTdts.insert(pathStr);
            }
        }
    }

    std::map<std::string, std::shared_ptr<poe::format::tgt::Tgt>> loadedTgts;
    std::map<std::string, std::shared_ptr<poe::format::tgm::TgmFile>> loadedTgms;
    std::set<std::string> seenTdtPaths;
    for (auto &pathStr : wantedTdts) {
        if (seenTdtPaths.contains(pathStr)) {
            continue;
        }
        auto fh = vfs->open_file(poe::util::path(pathStr));
        auto tdtOpt = poe::format::tdt::parse_tdt(fh->read_all());
        auto tdt = std::make_shared<poe::format::tdt::Tdt>(std::move(tdtOpt.value()));
        ret->tdts[pathStr] = tdt;
        auto &tileData = ret->tileData[tdt] = TileData{
            .path = pathStr,
            .tdt = tdt,
        };

        auto dim = tdt->header.dim;

        // Gather all TGT paths as written in TDT files, including any _cArB suffixes.
        struct TgtRef {
            std::string path;
            glm::ivec2 subCoord; // tile col/row in sub grid
            glm::ivec2 colRow;   // cArB indices for tgt/tgm
        };
        std::vector<TgtRef> tgtRefs;
        tgtRefs.reserve(1 + dim.x * dim.y);
        if (auto &tgt = tdt->commonTgt) {
            tgtRefs.push_back(TgtRef{.path = tgt.value(), .subCoord{0, 0}, .colRow{1, 1}});
        }
        for (int subIdx = 0; subIdx < tdt->subTiles.size(); ++subIdx) {
            int subCol = subIdx % dim.x;
            int subRow = subIdx / dim.x;
            int col = 1 + subCol;
            int row = 1 + (dim.y - 1 - subRow);
            auto &sub = tdt->subTiles[subIdx];
            std::string subTgt = sub.tgt ? sub.tgt.value() : tdt->commonTgt.value();
            tgtRefs.push_back(TgtRef{
                .path = subTgt,
                .subCoord = {subCol, subRow},
                .colRow = {col, row},
            });
        }
        for (auto &tgtRef : tgtRefs) {
            auto &tgtPath = tgtRef.path;
            std::shared_ptr<poe::format::tgt::Tgt> tgt;
            if (auto I = loadedTgts.find(tgtPath); I != loadedTgts.end()) {
                tgt = ret->tgts[tgtPath] = I->second;
            } else {
                poe::util::path p(tgtPath);
                auto fh = vfs->open_file(p);
                if (!fh) {
                    static std::regex const multiRe(R"#(^(.*)_c(\d+)r(\d+).tgt$)#");
                    std::string leafStr = (std::string)p.leaf(); // because <regex> doesn't believe in string_views
                    std::smatch m;
                    if (std::regex_match(leafStr, m, multiRe)) {
                        p = p.make_relative(m[1].str() + ".tgt");
                        fh = vfs->open_file(p);
                    }
                }
                auto tgtOpt = poe::format::tgt::parse_tgt(fh->read_all());
                if (!tgtOpt) {
                    DebugBreak();
                }
                tgt = std::make_shared<poe::format::tgt::Tgt>(std::move(tgtOpt.value()));
                loadedTgts[p.path_] = tgt;
                ret->tgts[tgtPath] = tgt;
            }

            absl::InlinedVector<std::string, 2> tgmCandidates;
            if (tgt->tileMeshRoot) {
                tgmCandidates.push_back(tgt->tileMeshRoot.value() + ".tgm");
                tgmCandidates.push_back(
                    fmt::format("{}_c{}r{}.tgm", tgt->tileMeshRoot.value(), tgtRef.colRow.x, tgtRef.colRow.y));
            } else {
                tgmCandidates.push_back(tgt->tileMesh.value());
            }

            std::shared_ptr<poe::format::tgm::TgmFile> tgm;
            for (auto &cand : tgmCandidates) {
                if (auto tgmI = loadedTgms.find(cand); tgmI != loadedTgms.end()) {
                    tgm = tgmI->second;
                    break;
                }
                auto fh = vfs->open_file(poe::util::path(cand));
                if (fh) {
                    tgm = poe::format::tgm::parse_tgm(fh->read_all());
                    loadedTgms[cand] = tgm;
                    ret->tgms[cand] = tgm;
                    break;
                }
            }
            if (!tgm) {
                DebugBreak();
            }
            tileData.subTiles[tgtRef.subCoord] = {.tgt = tgt, .tgm = tgm};
        }
    }
    return ret;
}

std::shared_future<std::shared_ptr<GraphAssets>> GameData::GetGraphAsync(std::string_view path) {
    auto lock = std::unique_lock(cacheMtx_);
    auto graphKey = absl::string_view(path.data(), path.size());
    if (auto I = priv_->graphs_.find(graphKey); I != priv_->graphs_.end()) {
        std::promise<std::shared_ptr<GraphAssets>> p;
        p.set_value(I->second);
        return p.get_future();
    }

    return std::async(std::launch::async,
                      [self = shared_from_this(), graphPath = std::string(graphKey)]() -> std::shared_ptr<GraphAssets> {
                          auto ret = self->GetGraph(graphPath);
                          return ret;
                      });
}

auto LoadGameData(std::shared_ptr<poe::io::vfs> vfs) -> std::shared_ptr<GameData> {
    auto ret = std::make_shared<GameData>();
    ret->vfs = vfs;
    return ret;
}

struct KCoverVtor {
    KCover operator()(poe::format::arm::KEntry const &k) {
        glm::ivec2 i0{0, 0};
        glm::ivec2 i1{k.w_, k.h_};
        glm::ivec2 off{0, 0};
        if (k.last_) {
            switch (*k.last_) {
            case 1:
                off = {-i1.x, 0};
                break;
            case 2:
                off = {-i1.x, -i1.y};
                break;
            case 3:
                off = {0, -i1.y};
                break;
            }
        }
        return {
            .kind = 'k',
            .i0 = i0 + off,
            .i1 = i1 + off,
        };
    }

    KCover CommonCase(char kind) {
        return {
            .kind = kind,
            .i0{0, 0},
            .i1{1, 1},
        };
    }

    KCover operator()(poe::format::arm::FEntry const &) { return CommonCase('f'); }
    KCover operator()(poe::format::arm::OEntry const &) { return CommonCase('o'); }
    KCover operator()(poe::format::arm::NEntry const &) { return CommonCase('n'); }
    KCover operator()(poe::format::arm::SEntry const &) { return CommonCase('s'); }
};

KCover GetKCover(poe::format::arm::Entry const &kEntry) { return std::visit(KCoverVtor{}, kEntry.var); }

std::shared_ptr<poe::format::et::EtFile> GameData::GetEt(std::string_view path) {
    auto pathView = absl::string_view(path.data(), path.size());

    auto lock = std::scoped_lock(cacheMtx_);
    auto &ets = priv_->cachedEtFiles_;
    if (auto I = ets.find(pathView); I != ets.end()) {
        return I->second;
    }

    if (auto fh = vfs->open_file(poe::util::path(path))) {
        auto data = fh->read_all();
        auto et = poe::format::et::parse_et(data);
        ets[pathView] = et;
        for (auto &etGt : et->gts) {
            if (etGt != "wildcard") {
                GetGt(etGt);
            }
        }
        if (auto &virt = et->virt) {
            for (auto etEt : virt->etPaths) {
                GetEt(etEt);
            }
        }
        return et;
    }
    return nullptr;
}

std::shared_ptr<poe::format::gt::GtFile> GameData::GetGt(std::string_view path) {
    auto pathView = absl::string_view(path.data(), path.size());

    auto lock = std::scoped_lock(cacheMtx_);
    auto &gts = priv_->cachedGtFiles_;
    if (auto I = gts.find(pathView); I != gts.end()) {
        return I->second;
    }
    if (auto fh = vfs->open_file(poe::util::path(path))) {
        auto data = fh->read_all();
        auto gt = poe::format::gt::parse_gt(data);
        gts[pathView] = gt;
        return gt;
    }
    return nullptr;
}

char8_t const *SideSymbol(TileSide side) {
    char8_t const *arrows[4]{
        u8"⮜",
        u8"⮟",
        u8"⮞",
        u8"⮝",
    };
    return arrows[(int)side];
}

char8_t const *CornerSymbol(TileCorner corner) {
    char8_t const *corners[4]{
        u8"▛",
        u8"▙",
        u8"▟",
        u8"▜",
    };
    // char8_t const *corners[4]{
    //     u8"┌",
    //     u8"└",
    //     u8"┘",
    //     u8"┐",
    // };
    return corners[(int)corner];
}

TileKey::TileKey(poe::format::arm::ARMFile const &arm, glm::ivec2 cell)
    : TileKey(arm, &arm.k_x_by_y_[cell.y][cell.x]) {}

TileKey::TileKey(poe::format::arm::ARMFile const &arm, poe::format::arm::Entry const *entry) {
    namespace arm_ns = poe::format::arm;
    if (auto *k = std::get_if<arm_ns::KEntry>(&entry->var)) {
        using arm_ns::KSlot;
        auto tail = std::span(k->tail_);
        auto edgeStrs = tail.subspan((size_t)KSlot::EdgeStr, 4);
        auto edgeIdxs = tail.subspan((size_t)KSlot::EdgeIdx, 8);
        auto groundStrs = tail.subspan((size_t)KSlot::GroundStr, 4);
        auto heights = tail.subspan((size_t)KSlot::Height, 4);

        for (size_t i = 0; i < 4; ++i) {
            constexpr TileSide const armSideMap[4]{
                TileSide::Top,
                TileSide::Right,
                TileSide::Bottom,
                TileSide::Left,
            };
            constexpr TileCorner const armCornerMap[4]{
                TileCorner::BottomLeft,
                TileCorner::BottomRight,
                TileCorner::TopRight,
                TileCorner::TopLeft,
            };
            auto side = armSideMap[i];
            auto corner = armCornerMap[i];
            etStr[(int)side] = arm.TableString(edgeStrs[i]);
            etVal[(int)side] = {(int)edgeIdxs[i * 2], (int)edgeIdxs[i * 2 + 1]};
            gtStr[(int)corner] = arm.TableString(groundStrs[i]);
            height[(int)corner] = (int)heights[i];
        }

        tag = arm.TableString(tail[(int)KSlot::Tag]);
        size = {k->w_, k->h_};
    } else if (auto *f = std::get_if<arm_ns::FEntry>(&entry->var)) {
        tag = arm.TableString(f->value_);
        size = {1, 1};
    } else {
        size = {1, 1};
    }
}

TileKey::TileKey(poe::format::tdt::Tdt const &tdt) {
    auto &hdr = tdt.header;

    TileSide remap[4]{
        TileSide::Bottom,
        TileSide::Right,
        TileSide::Top,
        TileSide::Left,
    };

    for (int i = 0; i < 4; ++i) {
        TileSide side = remap[i];
        etVal[(int)side] = {hdr.sideOffsets[i], hdr.sideOffsets[4 + i]};
        etStr[(int)side] = hdr.sideEts[i];
        gtStr[(int)side] = hdr.sideGts[i];
    }

    if (!tdt.tailBytes.empty()) {
        auto rightCol = hdr.dim.x;
        auto topRow = hdr.dim.y;
        auto rowStride = hdr.dim.x + 1;
        height[(int)TileCorner::TopLeft] = tdt.tailBytes[topRow * rowStride];
        height[(int)TileCorner::BottomLeft] = tdt.tailBytes[0];
        height[(int)TileCorner::BottomRight] = tdt.tailBytes[rightCol];
        height[(int)TileCorner::TopRight] = tdt.tailBytes[topRow * rowStride + rightCol];
    } else {
        std::fill(begin(height), end(height), 0);
    }

    if (!tdt.tag.empty()) {
        tag = tdt.tag;
    }

    size = glm::ivec2{hdr.dim};
}

TileKey TileKey::Flipped(bool flip) const {
    TileKey ret = *this;
    if (!flip) {
        return ret;
    }
    auto FlipSides = [&](auto &arr) { std::swap(arr[(int)TileSide::Left], arr[(int)TileSide::Right]); };
    auto FlipValue = [&](auto &arr, TileSide side, int length) {
        EdgeIndex &val = arr[(int)side];
        if (val.real != length * 3) {
            val.real = length * 3 - 1 - val.real;
        }
        if (val.virt != length * 3) {
            val.virt = length * 3 - 1 - val.virt;
        }
    };
    auto FlipCorners = [&](auto &arr) {
        std::swap(arr[(int)TileCorner::TopLeft], arr[(int)TileCorner::TopRight]);
        std::swap(arr[(int)TileCorner::BottomLeft], arr[(int)TileCorner::BottomRight]);
    };

    FlipSides(ret.etVal);
    FlipValue(ret.etVal, TileSide::Left, ret.size.y);
    FlipValue(ret.etVal, TileSide::Right, ret.size.y);
    FlipValue(ret.etVal, TileSide::Top, ret.size.x);
    FlipValue(ret.etVal, TileSide::Bottom, ret.size.x);
    FlipSides(ret.etStr);
    FlipCorners(ret.gtStr);
    FlipCorners(ret.height);

    return ret;
}

TileKey TileKey::Rotated(Rotation rot) const {
    TileKey ret = *this;
    int quarters{};
    switch (rot) {
    case Rotation::R0:
        return ret;
    case Rotation::R90:
        quarters = 3;
        std::swap(ret.size.x, ret.size.y);
        break;
    case Rotation::R180:
        quarters = 2;
        break;
    case Rotation::R270:
        quarters = 1;
        std::swap(ret.size.x, ret.size.y);
        break;
    }
    auto RotateArray = [&](auto &arr) { std::rotate(begin(arr), begin(arr) + quarters, end(arr)); };
    RotateArray(ret.etVal);
    RotateArray(ret.etStr);
    RotateArray(ret.gtStr);
    RotateArray(ret.height);

    return ret;
}
} // namespace pog
