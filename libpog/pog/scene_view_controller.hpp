#pragma once

#include <pog/dx_context.hpp>
#include <pog/view_host.hpp>

#include <array>
#include <filesystem>
#include <memory>
#include <optional>
#include <span>
#include <vector>

#include <glm/mat4x4.hpp>
#include <poe/format/tgm.hpp>
#include <d3dcompiler.h>
#include <poe/format/tdt.hpp>

namespace pog {
enum class MouseButton {
    Left,
    Right,
    Middle,
    Back,
    Forward,
};

enum ModKey {
    ModNone = 0,
    ModShift = 1,
    ModAlt = 2,
    ModCtrl = 4,
};

struct FrameUpdate {
    struct Input {
        glm::vec2 cursorPos;

        struct InputEvent {
            enum Kind {
                MouseMove,
                MousePress,
                MouseRelease,
            } kind;
            union Data {
                struct {
                    glm::vec2 pos;
                } mouseMove;
                struct {
                    glm::vec2 pos;
                    MouseButton button;
                } mousePress, mouseRelease;
            } data;
        };
        // Track whether a mouse button was held entering the start of the frame, again pressed and released during the
        // frame, and whether it's held at the end of the frame.
        std::vector<InputEvent> events;
    } input;
    double time, dt;
    glm::ivec2 fbSize;
};

struct LookatCamera {
    glm::vec3 at{0.0f, 0.0f, 0.0f};
    float distance = 1630.0f;
    // float distance = 2500.0f;
    float azimuth_deg = -135.0f;
    float altitude_deg = 50.31f;
    // float altitude_deg = 70.0f;
};

struct FixedCamera {
    glm::vec3 Position() const;
    glm::mat4 ViewMatrix() const;
    glm::mat4 ProjectionMatrix() const;

    glm::vec2 winSize{1, 1};
    LookatCamera look{};
    float fovDeg = 75.0f;
    float zNear = 100.0f;
    float zFar = 10000.0f;
};

void RunCameraControlUi(FixedCamera &cam);

struct GameData;

struct SceneViewController {
    explicit SceneViewController(std::shared_ptr<pog::DxContext> dx);
    virtual ~SceneViewController() = default;

    virtual void Update(FrameUpdate const &update, std::shared_ptr<GameData> gameData) = 0;
    virtual void Draw(pog::LayerTarget const &rt) = 0;

  protected:
    std::shared_ptr<pog::DxContext> dx_;
};
} // namespace pog
