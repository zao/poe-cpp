#pragma once

#include <memory>

#include <poe/format/arm.hpp>
#include <poe/format/dgr.hpp>
#include <poe/format/et.hpp>
#include <poe/format/gft.hpp>
#include <poe/format/gt.hpp>
#include <poe/format/rs.hpp>
#include <poe/format/tdt.hpp>
#include <poe/format/tgm.hpp>
#include <poe/format/tgr.hpp>
#include <poe/format/tgt.hpp>
#include <poe/format/tsi.hpp>
#include <poe/format/tst.hpp>
#include <poe/io/vfs.hpp>

#include <absl/hash/hash.h>

#include <pog/tile_meta.hpp>

namespace pog {
struct GraphAssets {
    // Graph, only one of these is valid
    std::shared_ptr<poe::format::dgr::DGRFile> dgr;
    std::shared_ptr<poe::format::tgr::TGRFile> tgr;

    // Master from graph
    std::shared_ptr<poe::format::tsi::TSIFile> tsi;

    std::shared_ptr<void> cht, clt, ddt, env, mtd, toy;

    // Fill tiles from master
    std::shared_ptr<poe::format::gft::GftFile> gft;

    // Roomset from master
    std::shared_ptr<poe::format::rs::RSFile> rs;

    // Rooms from roomset
    std::map<std::string, std::shared_ptr<poe::format::arm::ARMFile>> arms;

    // Tileset from master
    std::shared_ptr<poe::format::tst::Tst> tst;

    // Tiles from tileset and fill tiles
    std::map<std::string, std::shared_ptr<poe::format::tdt::Tdt>> tdts;

    // Files referenced from tiles
    std::map<std::string, std::shared_ptr<poe::format::tgt::Tgt>> tgts;
    std::map<std::string, std::shared_ptr<poe::format::tgm::TgmFile>> tgms;

    // Tile-derived data
    std::map<std::shared_ptr<poe::format::tdt::Tdt>, TileData> tileData;
};

struct GameData : std::enable_shared_from_this<GameData> {
    GameData();

    std::shared_ptr<poe::io::vfs> vfs;

    std::shared_ptr<GraphAssets> GetGraph(std::string_view path);
    std::shared_future<std::shared_ptr<GraphAssets>> GetGraphAsync(std::string_view path);

    void RequestGraphChange(std::string_view path);
    std::shared_ptr<GraphAssets> CurrentGraph();

  private:
    // Cached files, populated on the fly, may contain empty pointers to indicate failure
    std::recursive_mutex cacheMtx_;

#define GAME_DATA_FILE_LOOKUP_TYPES                                                                                    \
    GAME_DATA_FILE_LOOKUP(Et, poe::format::et::parse_et, ".et")                                                        \
    GAME_DATA_FILE_LOOKUP(Gt, poe::format::gt::parse_gt, ".gt")

#define GAME_DATA_FILE_LOOKUP(TypeName, Parser, Extension)                                                             \
  public:                                                                                                              \
    using Get##TypeName##Result = decltype(Parser(std::declval<std::span<std::byte const>>()));                        \
    Get##TypeName##Result Get##TypeName(std::string_view path);                                                        \
    std::shared_future<Get##TypeName##Result> Get##TypeName##Async(std::string_view path);

    GAME_DATA_FILE_LOOKUP_TYPES
#undef GAME_DATA_FILE_LOOKUP

  private:
    struct Private;
    std::unique_ptr<Private> priv_;
};

auto LoadGameData(std::shared_ptr<poe::io::vfs> vfs) -> std::shared_ptr<GameData>;
} // namespace pog
