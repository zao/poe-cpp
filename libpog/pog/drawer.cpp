#include "drawer.hpp"

#include <d3dcompiler.h>
#include <ranges>

#include <loguru/loguru.hpp>

namespace pog {
void ScaffoldGeometry::AddVerticalQuadStrip(std::span<glm::vec2 const> points, std::span<glm::vec3 const> colors,
                                            float zLow, float zHigh, bool loop) {
    assert(points.size() == colors.size());
    auto const n = points.size();
    if (n < 2) {
        return;
    }
    uint32_t baseIndex = (uint32_t)triVertices.size();
    auto PointIndex = [baseIndex, n](size_t pointIdx, size_t verticalIdx) -> uint32_t {
        return (uint32_t)(baseIndex + 2 * (pointIdx % n) + verticalIdx);
    };
    for (size_t i = 0; i < n; ++i) {
        triVertices.push_back(TintedUnlitVertex{
            .pos = glm::vec3(points[i], zLow),
            .color = colors[i],
        });
        triVertices.push_back(TintedUnlitVertex{
            .pos = glm::vec3(points[i], zHigh),
            .color = colors[i],
        });
    }
    for (size_t i = 0; i < n; ++i) {
        std::array<uint32_t, 6> tris{};

        if (i < n - 1 || loop) {
            triIndices.insert(triIndices.end(), {PointIndex(i, 1), PointIndex(i + 1, 1), PointIndex(i, 0),
                                                 PointIndex(i, 0), PointIndex(i + 1, 1), PointIndex(i + 1, 0)});
        }
    }
}

void ScaffoldGeometry::AddTriangle(std::span<glm::vec3 const, 3> tri, glm::vec3 color) {
    std::array<glm::vec3, 3> colors{color, color, color};
    AddTriangle(tri, colors);
}

void ScaffoldGeometry::AddTriangle(std::span<glm::vec3 const, 3> tri, std::span<glm::vec3 const, 3> colors) {
    using std::begin, std::end;
    uint32_t baseIndex = (uint32_t)triVertices.size();

    TintedUnlitVertex verts[]{
        {.pos = tri[0], .color = colors[0]},
        {.pos = tri[1], .color = colors[1]},
        {.pos = tri[2], .color = colors[2]},
    };
    triVertices.insert(end(triVertices), begin(verts), end(verts));

    triIndices.push_back(baseIndex);
    triIndices.push_back(baseIndex + 1);
    triIndices.push_back(baseIndex + 2);
}

void ScaffoldGeometry::AddLineAABB(glm::vec3 origin, std::span<float const, 6> bbox, glm::vec3 color) {
    glm::vec2 byAxis[]{glm::vec2{bbox[0], bbox[1]}, glm::vec2{bbox[2], bbox[3]}, glm::vec2{bbox[4], bbox[5]}};
    AddLineAABB(origin, byAxis, color);
}

void ScaffoldGeometry::AddLineAABB(glm::vec3 origin, std::span<glm::vec2 const, 3> bbox, glm::vec3 color) {
    glm::vec3 cMin = origin + glm::vec3(bbox[0].x, bbox[1].x, bbox[2].x),
              cMax = origin + glm::vec3(bbox[0].y, bbox[1].y, bbox[2].y);
    std::array<glm::vec3, 8> corners{
        glm::mix(cMin, cMax, glm::vec3{0, 0, 0}), glm::mix(cMin, cMax, glm::vec3{1, 0, 0}), //
        glm::mix(cMin, cMax, glm::vec3{1, 1, 0}), glm::mix(cMin, cMax, glm::vec3{0, 1, 0}), //
        glm::mix(cMin, cMax, glm::vec3{0, 0, 1}), glm::mix(cMin, cMax, glm::vec3{1, 0, 1}), //
        glm::mix(cMin, cMax, glm::vec3{1, 1, 1}), glm::mix(cMin, cMax, glm::vec3{0, 1, 1}), //
    };
    std::array<glm::vec3, 4> colors{color, color, color, color};
    AddLineStrip(std::span(corners).subspan<0, 4>(), colors, true);
    AddLineStrip(std::span(corners).subspan<4, 4>(), colors, true);
    for (int i = 0; i < 4; ++i) {
        glm::vec3 seg[]{corners[i], corners[i + 4]};
        AddLineStrip(seg, std::span(colors).subspan<0, 2>(), false);
    }
}

void ScaffoldGeometry::AddLineRect(glm::vec3 origin, glm::vec3 xSide, glm::vec3 ySide, glm::vec3 color) {
    std::array<glm::vec3, 4> colors{color, color, color, color};
    AddLineRect(origin, xSide, ySide, colors);
}

void ScaffoldGeometry::AddLineRect(glm::vec3 origin, glm::vec3 xSide, glm::vec3 ySide,
                                   std::span<glm::vec3 const, 4> colors) {
    std::array<glm::vec3, 4> coords{
        origin,
        origin + xSide,
        origin + xSide + ySide,
        origin + ySide,
    };
    AddLineStrip(coords, colors, true);
}

void ScaffoldGeometry::AddQuad(glm::vec3 origin, glm::vec3 xSide, glm::vec3 ySide, glm::vec3 color) {
    std::array<glm::vec3, 4> colors{color, color, color, color};
    AddQuad(origin, xSide, ySide, colors);
}

void ScaffoldGeometry::AddQuad(glm::vec3 origin, glm::vec3 xSide, glm::vec3 ySide,
                               std::span<glm::vec3 const, 4> colors) {
    using std::begin, std::end;
    assert(colors.size() == 4);
    if (colors.size() != 4) {
        return;
    }
    uint32_t baseIndex = (uint32_t)triVertices.size();
    /* 2 3
     * 0 1
     */
    TintedUnlitVertex verts[]{
        {.pos = origin, .color = colors[0]},
        {.pos = origin + xSide, .color = colors[1]},
        {.pos = origin + ySide, .color = colors[2]},
        {.pos = origin + xSide + ySide, .color = colors[3]},
    };
    triVertices.insert(triVertices.end(), begin(verts), end(verts));

    uint32_t indices[]{0, 2, 1, 1, 2, 3};
    std::transform(begin(indices), end(indices), std::back_inserter(triIndices),
                   [baseIndex](uint32_t idx) { return baseIndex + idx; });
}

void ScaffoldGeometry::AddLineStrip(std::span<glm::vec3 const> points, std::span<glm::vec3 const> colors, bool loop) {
    assert(points.size() == colors.size());
    auto const n = points.size();
    if (n < 2) {
        return;
    }
    uint32_t baseIndex = (uint32_t)lineVertices.size();
    auto PointIndex = [baseIndex, n](size_t pointIdx) -> uint32_t { return (uint32_t)(baseIndex + (pointIdx % n)); };
    for (size_t i = 0; i < n; ++i) {
        lineVertices.push_back(TintedUnlitVertex{
            .pos = points[i],
            .color = colors[i],
        });
    }
    for (size_t i = 0; i < n; ++i) {
        if (i < n - 1 || loop) {
            lineIndices.push_back(PointIndex(i));
            lineIndices.push_back(PointIndex(i + 1));
        }
    }
}

ScaffoldDrawer::ScaffoldDrawer(std::shared_ptr<pog::DxContext> dx)
    : dx_(dx), vsObj_(poe::util::AssetPath("TintedUnlit3D-vs_4_0.cso")),
      psObj_(poe::util::AssetPath("TintedUnlit3D-ps_4_0.cso")) {
    HRESULT hr = S_OK;
    auto *dev = dx_->Device();

    std::vector<std::byte> vsData = vsObj_.Slurp().value(), psData = psObj_.Slurp().value();

    hr = dev->CreateVertexShader(vsData.data(), vsData.size(), nullptr, &vs_);
    hr = dev->CreatePixelShader(psData.data(), psData.size(), nullptr, &ps_);

    constexpr std::array<D3D11_INPUT_ELEMENT_DESC, 2> ieds{
        D3D11_INPUT_ELEMENT_DESC{
            "Position",
            0,
            DXGI_FORMAT_R32G32B32_FLOAT,
            0,
            0,
            D3D11_INPUT_PER_VERTEX_DATA,
            0,
        },
        D3D11_INPUT_ELEMENT_DESC{
            "Color",
            0,
            DXGI_FORMAT_R32G32B32_FLOAT,
            0,
            D3D11_APPEND_ALIGNED_ELEMENT,
            D3D11_INPUT_PER_VERTEX_DATA,
            0,
        },
    };
    hr = dev->CreateInputLayout(ieds.data(), (UINT)ieds.size(), vsData.data(), vsData.size(), &il_);

    D3D11_RASTERIZER_DESC rasterDesc{
        .FillMode = D3D11_FILL_SOLID,
        .CullMode = D3D11_CULL_NONE,
        .FrontCounterClockwise = FALSE,
        .DepthBias = 0,
        .DepthBiasClamp = 0.0f,
        .SlopeScaledDepthBias = 0.0f,
        .DepthClipEnable = TRUE,
        .ScissorEnable = FALSE,
        .MultisampleEnable = TRUE,
        .AntialiasedLineEnable = TRUE,
    };
    hr = dev->CreateRasterizerState(&rasterDesc, &rasterState_);

    D3D11_DEPTH_STENCILOP_DESC opDesc{
        .StencilFailOp = D3D11_STENCIL_OP_KEEP,
        .StencilDepthFailOp = D3D11_STENCIL_OP_KEEP,
        .StencilPassOp = D3D11_STENCIL_OP_KEEP,
        .StencilFunc = D3D11_COMPARISON_ALWAYS,
    };
    D3D11_DEPTH_STENCIL_DESC dssDesc{
        .DepthEnable = TRUE,
        .DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL,
        .DepthFunc = D3D11_COMPARISON_ALWAYS,
        .StencilEnable = FALSE,
        .StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK,
        .StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK,
        .FrontFace = opDesc,
        .BackFace = opDesc,
    };
    hr = dev->CreateDepthStencilState(&dssDesc, &dss_);

    D3D11_BUFFER_DESC cbPassDesc{
        .ByteWidth = sizeof(PassCB),
        .Usage = D3D11_USAGE_DEFAULT,
        .BindFlags = D3D11_BIND_CONSTANT_BUFFER,
        .CPUAccessFlags{},
        .MiscFlags{},
        .StructureByteStride{},
    },
        cbPipeDesc = cbPassDesc, cbObjectDesc = cbPassDesc;
    cbPipeDesc.ByteWidth = sizeof(PipeCB);
    cbObjectDesc.ByteWidth = sizeof(ObjectCB);
    hr = dev->CreateBuffer(&cbPassDesc, nullptr, &cbPass_);
    hr = dev->CreateBuffer(&cbPipeDesc, nullptr, &cbPipe_);
    hr = dev->CreateBuffer(&cbObjectDesc, nullptr, &cbObject_);
}

void ScaffoldDrawer::Draw(ScaffoldGeometry const &geom, glm::mat4 viewProjMat, glm::mat4 worldMat) {
    if (geom.triIndices.empty() && geom.lineIndices.empty()) {
        return;
    }

    HRESULT hr = S_OK;
    auto *dev = dx_->Device();
    auto *ctx = dx_->Context();
    // create temporary buffer from geometry sets
    CComPtr<ID3D11Buffer> triVb, triIb, lineVb, lineIb;
    {
        struct BufferMeta {
            CComPtr<ID3D11Buffer> &dst;
            void const *data;
            size_t size;
        };
        auto MakeBuffer = [dev](auto const &src, D3D11_BIND_FLAG bindFlags) -> CComPtr<ID3D11Buffer> {
            HRESULT hr = S_OK;
            CComPtr<ID3D11Buffer> ret;
            if (src.size() == 0) {
                return ret;
            }
            D3D11_SUBRESOURCE_DATA bufSrd{
                .pSysMem = src.data(),
            };
            D3D11_BUFFER_DESC bufDesc{
                .ByteWidth = (UINT)(sizeof(src[0]) * src.size()),
                .Usage = D3D11_USAGE_IMMUTABLE,
                .BindFlags = (UINT)bindFlags,
                .MiscFlags{},
                .StructureByteStride{},
            };
            hr = dev->CreateBuffer(&bufDesc, &bufSrd, &ret);
            return ret;
        };
        triVb = MakeBuffer(geom.triVertices, D3D11_BIND_VERTEX_BUFFER);
        triIb = MakeBuffer(geom.triIndices, D3D11_BIND_INDEX_BUFFER);
        lineVb = MakeBuffer(geom.lineVertices, D3D11_BIND_VERTEX_BUFFER);
        lineIb = MakeBuffer(geom.lineIndices, D3D11_BIND_INDEX_BUFFER);
    }

    // fill in CBs
    PassCB passData{
        .viewProjectionTransform = glm::transpose(viewProjMat),
    };
    PipeCB pipeData{};
    ObjectCB objectData{
        .worldTransform = glm::transpose(worldMat),
    };
    ctx->UpdateSubresource(cbPass_, 0, nullptr, &passData, 0, 0);
    ctx->UpdateSubresource(cbPipe_, 0, nullptr, &pipeData, 0, 0);
    ctx->UpdateSubresource(cbObject_, 0, nullptr, &objectData, 0, 0);

    // draw
    ctx->VSSetShader(vs_, nullptr, 0);
    ctx->PSSetShader(ps_, nullptr, 0);
    ctx->IASetInputLayout(il_);
    ctx->RSSetState(rasterState_);

    CComPtr<ID3D11DepthStencilState> oldDss;
    UINT oldStencilRef{};
    ctx->OMGetDepthStencilState(&oldDss, &oldStencilRef);
    ctx->OMSetDepthStencilState(dss_, 0);

    ID3D11Buffer *cbs[]{
        cbPass_,
        cbPipe_,
        cbObject_,
    };
    ctx->VSSetConstantBuffers(0, (UINT)std::size(cbs), std::data(cbs));

    UINT lineStride = sizeof(ScaffoldGeometry::TintedUnlitVertex), triStride = lineStride;
    UINT lineOffset = 0, triOffset = 0;

    if (triIb) {
        ctx->IASetVertexBuffers(0, 1, &triVb.p, &triStride, &triOffset);
        ctx->IASetIndexBuffer(triIb, DXGI_FORMAT_R32_UINT, 0);
        ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
        ctx->DrawIndexed((UINT)geom.triIndices.size(), 0, 0);
    }

    if (lineIb) {
        ctx->IASetVertexBuffers(0, 1, &lineVb.p, &lineStride, &lineOffset);
        ctx->IASetIndexBuffer(lineIb, DXGI_FORMAT_R32_UINT, 0);
        ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
        ctx->DrawIndexed((UINT)geom.lineIndices.size(), 0, 0);
    }

    ctx->OMSetDepthStencilState(oldDss, oldStencilRef);
}

ID3D11ShaderResourceView *LightSet::LightSB() {
    if (dirty_) {
        // Check if we have a buffer with the same number of lights and reuse that, otherwise create a new one
        // and populate it.
        HRESULT hr = S_OK;
        auto *dev = dx_->Device();
        auto *ctx = dx_->Context();
        auto lightSpan = std::span(lights);
        D3D11_SUBRESOURCE_DATA srd{.pSysMem = lightSpan.data()};
        if (sb_) {
            D3D11_BUFFER_DESC sbDesc;
            sb_->GetDesc(&sbDesc);
            size_t currentCount = sbDesc.ByteWidth / sizeof(Light);
            if (currentCount == lights.size()) {
                ctx->UpdateSubresource(sb_, 0, nullptr, srd.pSysMem, 0, 0);
                dirty_ = false;
                return sbSrv_;
            }
            sb_.Release();
            sbSrv_.Release();
        }

        D3D11_BUFFER_DESC sbDesc{
            .ByteWidth = (UINT)lightSpan.size_bytes(),
            .Usage = D3D11_USAGE_DEFAULT,
            .BindFlags = D3D11_BIND_SHADER_RESOURCE,
            .CPUAccessFlags{},
            .MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED,
            .StructureByteStride = sizeof(Light),
        };
        hr = dev->CreateBuffer(&sbDesc, &srd, &sb_);
        hr = dev->CreateShaderResourceView(sb_, nullptr, &sbSrv_);
        dirty_ = false;
    }
    return sbSrv_;
}

void TileKeyDrawer::DrawTileKey(glm::mat4 worldMat, glm::mat4 viewMat, glm::mat4 projMat, TileKey const &key) {
    ScaffoldGeometry scaffoldGeom;

    // Colours for different types of geometry
    constexpr glm::vec3 const realTabColor{0.2f, 0.2f, 0.9f};
    constexpr glm::vec3 const virtTabColor{0.5f, 0.0f, 0.5f};
    constexpr glm::vec3 const groundTabColor{0.2f, 0.8f, 0.2f};
    constexpr glm::vec3 const edgeRimColor{0.3f, 0.3f, 0.3f};

    // Base vectors for the tile
    auto dim = key.size;
    constexpr float const tileSide = 250.0f;
    constexpr glm::vec3 const xSide{tileSide, 0.0f, 0.0f};
    constexpr glm::vec3 const ySide{0.0f, tileSide, 0.0f};

    // Base vectors for tabs
    constexpr glm::vec3 const vtabBase = {0.0f, 250.0f / 3.0f, 0.0f};
    constexpr glm::vec3 const htabBase = {250.0f / 3.0f, 0.0f, 0.0f};
    constexpr glm::vec3 const vtabOut = {50.0f, 0.0f, 0.0f};
    constexpr glm::vec3 const htabOut = {0.0f, 50.0f, 0.0f};

    // Tile corners to anchor geometry against
    glm::vec3 const lowerLeft = {0.0f, 0.0f, 0.0f};
    glm::vec3 const lowerRight = {250.0f * key.size.x, 0.0f, 0.0f};
    glm::vec3 const upperLeft = {0.0f, 250.0f * key.size.y, 0.0f};
    glm::vec3 const upperRight = {250.0f * key.size.x, 250.0f * key.size.y, 0.0f};

    // Draw left/right tab outlines
    for (size_t vidx = 0; vidx < key.size.y * 3; ++vidx) {
        scaffoldGeom.AddLineRect(vtabBase * (float)vidx, -vtabOut * 2.0f, vtabBase, edgeRimColor);
        scaffoldGeom.AddLineRect(lowerRight + vtabBase * (float)vidx, +vtabOut * 2.0f, vtabBase, edgeRimColor);
    }

    // Draw bottom/top tab outlines
    for (size_t hidx = 0; hidx < key.size.x * 3; ++hidx) {
        scaffoldGeom.AddLineRect(htabBase * (float)hidx, -htabOut * 2.0f, htabBase, edgeRimColor);
        scaffoldGeom.AddLineRect(upperLeft + htabBase * (float)hidx, +htabOut * 2.0f, htabBase, edgeRimColor);
    }

    struct EdgeTab {
        TileSide side;
        std::string str;
        int idx;
        bool isReal;
    };

    auto SideTabCount = [&](TileSide side) -> int {
        switch (side) {
        case TileSide::Left:
        case TileSide::Right:
            return key.size.y * 3;
        case TileSide::Bottom:
        case TileSide::Top:
            return key.size.x * 3;
        default:
            return 0; // [[unreachable]]
        }
    };

    // Collect edge tabs for drawing
    std::vector<EdgeTab> edgeTabs;
    for (int i = 0; i < 4; ++i) {
        auto side = (TileSide)i;
        auto val = key.etVal[i];
        auto &str = key.etStr[i];
        if (!str) {
            continue;
        }
        EdgeTab tab{.side = side, .str = str.value()};
        if (val.real < SideTabCount(side)) {
            EdgeTab t = tab;
            t.isReal = true;
            t.idx = val.real;
            edgeTabs.push_back(t);
        }
        if (val.virt < SideTabCount(side)) {
            EdgeTab t = tab;
            t.isReal = false;
            t.idx = val.virt;
            edgeTabs.push_back(t);
        }
    }

    // Draw collected edge tabs
    for (auto &tab : edgeTabs) {
        glm::vec3 origin{}, xSide{}, ySide{};
        switch (tab.side) {
        case TileSide::Left:
            origin = upperLeft - vtabBase * (float)tab.idx;
            xSide = -vtabOut;
            ySide = -vtabBase;
            break;
        case TileSide::Right:
            origin = lowerRight + vtabBase * (float)tab.idx;
            xSide = +vtabOut;
            ySide = vtabBase;
            break;
        case TileSide::Bottom:
            origin = lowerLeft + htabBase * (float)tab.idx;
            xSide = htabBase;
            ySide = -htabOut;
            break;
        case TileSide::Top:
            origin = upperRight - htabBase * (float)tab.idx;
            xSide = -htabBase;
            ySide = +htabOut;
            break;
        }
        auto color = tab.isReal ? realTabColor : virtTabColor;
        scaffoldGeom.AddQuad(origin, xSide, ySide, color);
    }

    // Draw ground tabs in corners
    for (int i = 0; i < 4; ++i) {
        auto corner = (TileCorner)i;
        if (!key.gtStr[i]) {
            continue;
        }

        glm::vec3 origin{}, xSide{}, ySide{};
        switch (corner) {
        case TileCorner::TopLeft:
            origin = upperLeft;
            xSide = -vtabOut;
            ySide = +htabOut;
            break;
        case TileCorner::BottomLeft:
            origin = lowerLeft;
            xSide = -vtabOut;
            ySide = -htabOut;
            break;
        case TileCorner::BottomRight:
            origin = lowerRight;
            xSide = +vtabOut;
            ySide = -htabOut;
            break;
        case TileCorner::TopRight:
            origin = upperRight;
            xSide = +vtabOut;
            ySide = +htabOut;
            break;
        }
        auto color = groundTabColor;
        scaffoldGeom.AddQuad(origin, xSide, ySide, color);
    }

    // Finally draw all collected geometry
    scaffoldDrawer_.Draw(scaffoldGeom, projMat * viewMat, worldMat);
}

void CBBase::SetCpuPart(ConstantInfo const &info, std::span<std::byte const> data) {
    if (data.size() != info.size || info.offset + info.size > cpu.size()) {
        return;
    }
    std::ranges::copy(data, cpu.data() + info.offset);
    dirty = true;
}

void CBBase::SetGpu(ID3D11DeviceContext *ctx) {
    if (dirty) {
        ctx->UpdateSubresource(gpu, 0, nullptr, cpu.data(), 0, 0);
        dirty = false;
    }
}

template <typename CpuData> struct CB : CBBase {
    CB() : CBBase(sizeof(CpuData)) {}

    void SetCpu(CpuData const &data) {
        auto src = as_bytes(std::span(&data, 1));
        auto dst = as_writable_bytes(std::span(cpu));
        std::ranges::copy(src, dst.data());
        dirty = true;
    }
};

Shader::Shader(std::shared_ptr<DxContext> &dx, std::filesystem::path vsPath, std::filesystem::path psPath)
    : dx_(dx), vsObj_(vsPath), psObj_(psPath) {
    using Cat = ConstantInfo::Category;
    LoadIfNeeded();
}

void Shader::BindShaders() {
    auto *ctx = dx_->Context();
    ctx->VSSetShader(vs_, nullptr, 0);
    ctx->PSSetShader(ps_, nullptr, 0);
}

bool Shader::BindVertexFormat(VertexComponent comp) {
    auto *ctx = dx_->Context();
    if (auto I = inputLayouts_.find(comp); I != inputLayouts_.end()) {
        ctx->IASetInputLayout(I->second);
        return true;
    }
    return false;
}

void Shader::BindResources(LightSet &lights, std::span<ID3D11ShaderResourceView *const> texSrvs,
                           std::span<ID3D11SamplerState *const> texSamplers) {
    auto *ctx = dx_->Context();
    for (auto &[_, cb] : constantBuffers) {
        cb.SetGpu(ctx);
    }
    ID3D11Buffer *cbs[]{
        constantBuffers[ConstantInfo::Category::Pass].gpu,
        constantBuffers[ConstantInfo::Category::Pipe].gpu,
        constantBuffers[ConstantInfo::Category::Object].gpu,
    };
    ctx->VSSetConstantBuffers(0, (UINT)std::size(cbs), std::data(cbs));
    ctx->PSSetConstantBuffers(0, (UINT)std::size(cbs), std::data(cbs));

    absl::InlinedVector<ID3D11ShaderResourceView *, 8> srvs{
        lights.LightSB(),
    };
    srvs.insert(srvs.end(), texSrvs.begin(), texSrvs.end());
    ctx->VSSetShaderResources(0, (UINT)srvs.size(), srvs.data());
    ctx->PSSetShaderResources(0, (UINT)srvs.size(), srvs.data());
    ctx->VSSetSamplers(0, (UINT)texSamplers.size(), texSamplers.data());
    ctx->PSSetSamplers(0, (UINT)texSamplers.size(), texSamplers.data());
}

template <typename DevPtr, typename T>
static CComPtr<ID3D11Buffer> MakeBuffer(DevPtr dev, std::span<T> src, D3D11_BIND_FLAG bindFlags) {
    HRESULT hr = S_OK;
    CComPtr<ID3D11Buffer> ret;
    auto srcBytes = as_bytes(src);
    if (srcBytes.size() == 0) {
        return ret;
    }
    D3D11_SUBRESOURCE_DATA bufSrd{
        .pSysMem = srcBytes.data(),
    };
    D3D11_BUFFER_DESC bufDesc{
        .ByteWidth = (UINT)srcBytes.size(),
        .Usage = D3D11_USAGE_IMMUTABLE,
        .BindFlags = (UINT)bindFlags,
        .MiscFlags{},
        .StructureByteStride{},
    };
    hr = dev->CreateBuffer(&bufDesc, &bufSrd, &ret);
    return ret;
};

MeshDrawer::MeshDrawer(std::shared_ptr<pog::DxContext> dx) : dx_(dx) {
    HRESULT hr = S_OK;
    auto *dev = dx_->Device();

    fallbackShader_ = std::make_shared<Shader>(dx_, poe::util::AssetPath("TileMesh3D-vs_5_0.cso"),
                                               poe::util::AssetPath("TileMesh3D-ps_5_0.cso"));

    D3D11_RASTERIZER_DESC rasterDesc{
        .FillMode = D3D11_FILL_SOLID,
        .CullMode = D3D11_CULL_BACK, // HACK(LV): NONE until we fix flipping, BACK normally
        .FrontCounterClockwise = FALSE,
        .DepthBias = 0,
        .DepthBiasClamp = 0.0f,
        .SlopeScaledDepthBias = 0.0f,
        .DepthClipEnable = TRUE,
        .ScissorEnable = FALSE,
        .MultisampleEnable = FALSE,
        .AntialiasedLineEnable = FALSE,
    };
    hr = dev->CreateRasterizerState(&rasterDesc, &rasterState_);

    rasterDesc.FrontCounterClockwise = TRUE;
    hr = dev->CreateRasterizerState(&rasterDesc, &rasterStateFlipped_);

    {
        D3D11_TEXTURE2D_DESC desc{
            .Width = 1,
            .Height = 1,
            .MipLevels = 1,
            .ArraySize = 1,
            .Format = DXGI_FORMAT_R8G8B8A8_UNORM,
            .SampleDesc = {.Count = 1, .Quality = 0},
            .Usage = D3D11_USAGE_IMMUTABLE,
            .BindFlags = D3D11_BIND_SHADER_RESOURCE,
        };

        uint8_t data[4]{255, 255, 255, 255};
        D3D11_SUBRESOURCE_DATA srd = {.pSysMem = data, .SysMemPitch = 4};
        CComPtr<ID3D11Texture2D> tex;
        dev->CreateTexture2D(&desc, &srd, &tex);
        dev->CreateShaderResourceView(tex, nullptr, &fallbackTex_);

        uint8_t normalData[4]{128, 128, 0, 0};
        srd.pSysMem = normalData;
        tex.Release();
        dev->CreateTexture2D(&desc, &srd, &tex);
        dev->CreateShaderResourceView(tex, nullptr, &fallbackNormalTex_);
    }

    {
        std::array<uint8_t, 8> noOpSkinning{
            0,    0, 0, 0, //
            0xFF, 0, 0, 0, //
        };
        noOpSkinBuffer_ = MakeBuffer(dev, std::span<uint8_t const>(noOpSkinning), D3D11_BIND_VERTEX_BUFFER);
    }
}

void MeshDrawer::PollResources() const { fallbackShader_->LoadIfNeeded(); }

namespace {
template <typename C>
std::shared_ptr<MeshDrawer::CachedGeometry> LoadGeometryCommon(DxContext const *dx, C &vertexData, C &indexData) {
    HRESULT hr = S_OK;
    auto *dev = dx->Device();

    auto ret = std::make_shared<MeshDrawer::CachedGeometry>();
    ret->vb_ = MakeBuffer(dev, std::span(vertexData), D3D11_BIND_VERTEX_BUFFER);
    ret->ib_ = MakeBuffer(dev, std::span(indexData), D3D11_BIND_INDEX_BUFFER);
    return ret;
}
} // namespace

std::shared_ptr<MeshDrawer::CachedGeometry> MeshDrawer::LoadGeometry(poe::format::dolm::DolmGeometry const &geom,
                                                                     int meshIdx) {
    if (meshIdx < 0 || meshIdx >= geom.meshes.size()) {
        return nullptr;
    }

    auto &mesh = geom.meshes[meshIdx];
    if (mesh->triangleCount == 0) {
        return nullptr;
    }

    return LoadGeometryCommon(dx_.get(), mesh->vertexData, mesh->indexData);
}

std::shared_ptr<MeshDrawer::CachedGeometry> MeshDrawer::LoadGeometry(poe::format::fmt::FMT const &geom, int meshIdx) {
    auto vertices = geom.vertex_span(meshIdx);
    auto indices = geom.index_span(meshIdx);
    if (vertices.empty() || indices.empty()) {
        return nullptr;
    }

    return LoadGeometryCommon(dx_.get(), vertices, indices);
}

void MeshDrawer::DrawGeometry(glm::mat4 const &worldMat, glm::mat4 const &viewMat, glm::mat4 const &projMat,
                              LightSet &lights, poe::format::dolm::DolmGeometry const &dolm, int meshIdx,
                              CachedGeometry const *cachedGeom, std::vector<bool> const *enabledShapes,
                              ShapeSrvSeq const *shapeSrvs, std::shared_ptr<Shader> shader, pog::Pose const *pose) {
    if (!shader || !shader->vs_) {
        shader = fallbackShader_;
    }

    std::shared_ptr<CachedGeometry> transientGeom;
    if (!cachedGeom) {
        transientGeom = LoadGeometry(dolm, meshIdx);
        cachedGeom = transientGeom.get();
    }
    if (!cachedGeom) {
        return;
    }

    auto mesh = dolm.meshes[meshIdx];
    if (mesh->triangleCount == 0) {
        return;
    }

    std::vector<bool> allShapes;
    if (!enabledShapes) {
        allShapes.resize(mesh->shapeExtents.size(), true);
        enabledShapes = &allShapes;
    }

    HRESULT hr = S_OK;
    auto *dev = dx_->Device();
    auto *ctx = dx_->Context();

    // TODO(LV): Rework material system...
    // At this point for each shape we need a concrete material that has a rendition of the effect graphs that make
    // up the material. This type will need compiled shaders, textures and the metadata of where to bind them, the
    // shape of the constant buffers expected, and ways to bind/prepare it for rendering.

    // This DrawGeometry function probably should be inverted so that instead of going:
    // * for all meshes, draw all enabled shapes
    // we ought to go:
    // * for all meshes, bucket enabled shapes by material
    // * for each material, draw each shape

    // Somewhere we need to push global state into used materials like the time, player position, etc. That probably
    // goes outside this drawer.

    struct MaterialBuffers {
        PassCB passData;
        PipeCB pipeData;
        ObjectCB objectData;
    } matBuffers;

    // fill in CBs
    matBuffers.passData = {
        .viewProjectionTransform = glm::transpose(projMat * viewMat),
        .viewTransform = glm::transpose(viewMat),
        .invViewTransform = glm::transpose(glm::inverse(viewMat)),
    };

    bool hasTex0 = !!(dolm.format & poe::format::dolm::VtxTexcoord0_2f16);
    bool hasTex1 = !!(dolm.format & poe::format::dolm::VtxTexcoord1_2f16);

    auto isFlipped = std::signbit(glm::determinant(worldMat));

    matBuffers.pipeData = {
        .hasTex0 = hasTex0,
        .hasTex1 = hasTex1,
        .hasLegacyNormals = dolm.legacyNormals,
        .isFlipped = isFlipped,
    };

    matBuffers.objectData = {
        .worldTransform = glm::transpose(worldMat),
        .invWorldTransform = glm::inverse(worldMat), // inverse-transpose transposed for transfer
        .animationMatricesIndices = glm::vec4{0, 0, 0, 0},
    };

    shader->SetConstant("view_projection_transform", matBuffers.passData.viewProjectionTransform);
    shader->SetConstant("view_transform", matBuffers.passData.viewTransform);
    shader->SetConstant("inv_view_transform", matBuffers.passData.invViewTransform);

    shader->SetConstant("hasTex0", matBuffers.pipeData.hasTex0);
    shader->SetConstant("hasTex1", matBuffers.pipeData.hasTex1);
    shader->SetConstant("hasLegacyNormals", matBuffers.pipeData.hasLegacyNormals);
    shader->SetConstant("hasFlippedNormals", matBuffers.pipeData.isFlipped);

    shader->SetConstant("world_transform", matBuffers.objectData.worldTransform);
    shader->SetConstant("inv_world_transform", matBuffers.objectData.invWorldTransform);
    shader->SetConstant("animation_matrices_indices", matBuffers.objectData.animationMatricesIndices);

    // draw
    shader->BindShaders();
    if (!shader->BindVertexFormat(dolm.format)) {
        return;
    }

    // TODO(LV): Use sampler definitions from Shaders/SamplerDeclarations.inc
    CComPtr<ID3D11SamplerState> dummySmp;
    {
        D3D11_SAMPLER_DESC desc{
            .Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR,
            .AddressU = D3D11_TEXTURE_ADDRESS_WRAP,
            .AddressV = D3D11_TEXTURE_ADDRESS_WRAP,
            .AddressW = D3D11_TEXTURE_ADDRESS_WRAP,
            .MipLODBias = 0.0f,
            .MaxAnisotropy = 1,
            .ComparisonFunc = D3D11_COMPARISON_NEVER,
            .BorderColor = {1.0f, 1.0f, 1.0f, 1.0f},
            .MinLOD = -FLT_MAX,
            .MaxLOD = +FLT_MAX,
        };
        dev->CreateSamplerState(&desc, &dummySmp);
    }

    ctx->RSSetState(isFlipped ? rasterStateFlipped_.p : rasterState_.p);

    std::optional<UINT> poseSlot;
    CComPtr<ID3D11ShaderResourceView> poseSrv;
    {
        D3D11_SHADER_INPUT_BIND_DESC bindDesc{};
        hr = shader->vsRefl_->GetResourceBindingDescByName("animation_matrices", &bindDesc);
        if (SUCCEEDED(hr)) {
            poseSlot = bindDesc.BindPoint;
            std::span<std::byte const> poseSpan;
            if (pose) {
                poseSpan = as_bytes(std::span(pose->paletteGpu));
            } else {
                static std::vector<glm::mat4> noOpPalette{256, glm::mat4{1}};
                poseSpan = as_bytes(std::span(noOpPalette));
            }
            D3D11_BUFFER_DESC sbDesc{
                .ByteWidth = (UINT)poseSpan.size_bytes(),
                .Usage = D3D11_USAGE_IMMUTABLE,
                .BindFlags = D3D11_BIND_SHADER_RESOURCE,
                .CPUAccessFlags = 0,
                .MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED,
                .StructureByteStride = sizeof(pose->paletteGpu[0]),
            };
            D3D11_SUBRESOURCE_DATA srd{
                .pSysMem = poseSpan.data(),
            };
            CComPtr<ID3D11Buffer> sbPose;
            hr = dev->CreateBuffer(&sbDesc, &srd, &sbPose);

            hr = dev->CreateShaderResourceView(sbPose, nullptr, &poseSrv);
        }
    }

    UINT strides[]{(UINT)VertexWidth(dolm.format), 8};
    UINT offsets[]{0, 0};

    if (auto g = *cachedGeom; g.ib_) {
        std::array<ID3D11ShaderResourceView *, 4> fallbackTexSrvs{
            fallbackTex_,
            fallbackNormalTex_,
            fallbackTex_,
            fallbackTex_,
        };
        std::array<ID3D11SamplerState *, 4> texSamplers{
            dummySmp,
            dummySmp,
            dummySmp,
            dummySmp,
        };
        auto const indexFormat = mesh->vertexCount > 0xFFFF ? DXGI_FORMAT_R32_UINT : DXGI_FORMAT_R16_UINT;

        ID3D11Buffer *vbs[]{g.vb_, noOpSkinBuffer_};
        ctx->IASetVertexBuffers(0, 2, vbs, strides, offsets);
        ctx->IASetIndexBuffer(g.ib_, indexFormat, 0);
        ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

        {
            auto combinedTexSrvs = fallbackTexSrvs;
            for (size_t texIdx = 0; texIdx < shapeSrvs->size(); ++texIdx) {
                if (auto tex = (*shapeSrvs)[texIdx]) {
                    combinedTexSrvs[texIdx] = tex;
                }
            }
            auto shapeTexSmps = texSamplers;
            shader->BindResources(lights, combinedTexSrvs, shapeTexSmps);

            if (poseSlot && poseSrv) {
                ctx->VSSetShaderResources(*poseSlot, 1, &poseSrv.p);
            }

            for (size_t shapeIdx = 0; shapeIdx < enabledShapes->size(); ++shapeIdx) {
                if ((*enabledShapes)[shapeIdx]) {
                    auto &extents = mesh->shapeExtents[shapeIdx];
                    ctx->DrawIndexedInstanced((UINT)extents.indexCount, 1, (UINT)extents.indexStart, 0, 0);
                }
            }
        }
    }
}

void MeshDrawer::DrawGeometry(glm::mat4 const &worldMat, glm::mat4 const &viewMat, glm::mat4 const &projMat,
                              LightSet &lights, poe::format::fmt::FMT const &geom, int meshIdx,
                              CachedGeometry const *cachedGeom, std::vector<bool> const *enabledShapes,
                              ShapeSrvSeq const *shapeSrvs, std::shared_ptr<Shader> shader) {
    DrawGeometry(worldMat, viewMat, projMat, lights, geom.dolm_, meshIdx, cachedGeom, enabledShapes, shapeSrvs, shader);
}

void MeshDrawer::DrawGeometry(glm::mat4 const &worldMat, glm::mat4 const &viewMat, glm::mat4 const &projMat,
                              LightSet &lights, poe::format::tgm::TgmGeometry const &geom, int meshIdx,
                              CachedGeometry const *cachedGeom, std::vector<bool> const *enabledShapes,
                              ShapeSrvSeq const *shapeSrvs, std::shared_ptr<Shader> shader) {
    DrawGeometry(worldMat, viewMat, projMat, lights, geom.dolm, meshIdx, cachedGeom, enabledShapes, shapeSrvs, shader);
}

using poe::format::dolm::VertexComponent;

std::vector<D3D11_INPUT_ELEMENT_DESC> VertexInputElements(VertexComponent format) {
    std::vector<D3D11_INPUT_ELEMENT_DESC> ret;
    UINT offset{0};
#define COMP_CASE(SemName, SemIndex, Format, Comp, Size)                                                               \
    if (format & VertexComponent::Comp) {                                                                              \
        ret.push_back({#SemName, (SemIndex), Format, 0, offset, D3D11_INPUT_PER_VERTEX_DATA, 0});                      \
        offset += (Size);                                                                                              \
    }
    COMP_CASE(Position, 0, DXGI_FORMAT_R32G32B32_FLOAT, VtxPosition_3f32, 12)
    COMP_CASE(Normal, 0, DXGI_FORMAT_R8G8B8A8_SNORM, VtxNormal_4i8, 4)
    COMP_CASE(Tangent, 0, DXGI_FORMAT_R8G8B8A8_SNORM, VtxTangent_4i8, 4)
    COMP_CASE(Texcoord, 0, DXGI_FORMAT_R16G16_FLOAT, VtxTexcoord0_2f16, 4)
    COMP_CASE(TailFloat, 0, DXGI_FORMAT_R16G16_FLOAT, VtxTailFloat_2f16, 4)
    // COMP_CASE(SkinBones, 0, DXGI_FORMAT_R8G8B8A8_UINT, VtxSkinBones_4u8, 4)
    // COMP_CASE(SkinWeights, 0, DXGI_FORMAT_R8G8B8A8_UNORM, VtxSkinWeights_4u8, 4)
    COMP_CASE(BLENDINDICES, 0, DXGI_FORMAT_R8G8B8A8_UINT, VtxSkinBones_4u8, 4)
    COMP_CASE(BLENDWEIGHT, 0, DXGI_FORMAT_R8G8B8A8_UNORM, VtxSkinWeights_4u8, 4)
    COMP_CASE(SkinExtra, 0, DXGI_FORMAT_R8G8B8A8_UINT, VtxSkinExtra_4u8, 4)
    COMP_CASE(Texcoord, 1, DXGI_FORMAT_R16G16_FLOAT, VtxTexcoord1_2f16, 4)

    // HACK(LV): Pretend everything is skinned by providing a fallback per-instance (0,0,0,0) boneset
    // and (0xFF,0,0,0) weights with a single-entry palette of matrix identity.
    if (!(format & VertexComponent::VtxSkinBones_4u8)) {
        ret.push_back({"BLENDINDICES", 0, DXGI_FORMAT_R8G8B8A8_UINT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 0});
    }
    if (!(format & VertexComponent::VtxSkinWeights_4u8)) {
        ret.push_back({"BLENDWEIGHT", 0, DXGI_FORMAT_R8G8B8A8_UNORM, 1, 4, D3D11_INPUT_PER_INSTANCE_DATA, 0});
    }
    return ret;
}

void Shader::LoadIfNeeded() {
    HRESULT hr = S_OK;
    auto *dev = dx_->Device();

    if (vsObj_.Updated()) {
        std::vector<std::byte> vsData = vsObj_.Slurp().value();

        CComPtr<ID3D11VertexShader> newVs;
        hr = dev->CreateVertexShader(vsData.data(), vsData.size(), nullptr, &newVs);

        CComPtr<ID3D11ShaderReflection> newVsRefl;
        hr = D3DReflect(vsData.data(), vsData.size(), IID_PPV_ARGS(&newVsRefl));

        D3D11_SHADER_DESC vsDesc{};
        hr = newVsRefl->GetDesc(&vsDesc);

        VertexComponent shaderReqComp{};
        std::vector<VertexComponent> inputComp(vsDesc.InputParameters);

        using Semantic = std::pair<std::string, size_t>;
        std::map<Semantic, D3D11_SIGNATURE_PARAMETER_DESC> inputDescs;
        for (size_t inputIdx = 0; inputIdx < vsDesc.InputParameters; ++inputIdx) {
            D3D11_SIGNATURE_PARAMETER_DESC inputDesc{};
            newVsRefl->GetInputParameterDesc((UINT)inputIdx, &inputDesc);
            Semantic sem{inputDesc.SemanticName, inputDesc.SemanticIndex};
            inputDescs[sem] = inputDesc;
            LOG_F(INFO, "{} {}", sem.first, sem.second);
        }

        std::vector<VertexComponent> knownFormats{
            (VertexComponent)(VertexComponent::VtxPosition_3f32 | VertexComponent::VtxNormal_4i8 |
                              VertexComponent::VtxTangent_4i8),
            (VertexComponent)(VertexComponent::VtxPosition_3f32 | VertexComponent::VtxNormal_4i8 |
                              VertexComponent::VtxTangent_4i8 | VertexComponent::VtxTexcoord0_2f16),
            (VertexComponent)(VertexComponent::VtxPosition_3f32 | VertexComponent::VtxNormal_4i8 |
                              VertexComponent::VtxTangent_4i8 | VertexComponent::VtxTexcoord0_2f16 |
                              VertexComponent::VtxTexcoord1_2f16),
            (VertexComponent)(VertexComponent::VtxPosition_3f32 | VertexComponent::VtxNormal_4i8 |
                              VertexComponent::VtxTangent_4i8 | VertexComponent::VtxTexcoord0_2f16 |
                              VertexComponent::VtxSkinBones_4u8 | VertexComponent::VtxSkinWeights_4u8),
        };

        if (newVs && newVsRefl) {
            vs_ = newVs;
            using VC = Shader::VertexComponent;

            for (auto format : knownFormats) {
                auto formatIeds = VertexInputElements(format);

                // TODO(LV): check if IED can satisfy input signature
                bool insufficient{false};
                for (auto &[sem, desc] : inputDescs) {
                    if (auto I = std::ranges::find(formatIeds, sem,
                                                   [](auto &ied) {
                                                       return Semantic{ied.SemanticName, ied.SemanticIndex};
                                                   });
                        I == formatIeds.end()) {
                        insufficient = true;
                        break;
                    }
                }
                if (!insufficient) {
                    CComPtr<ID3D11InputLayout> layout;
                    hr = dev->CreateInputLayout(formatIeds.data(), (UINT)formatIeds.size(), vsData.data(),
                                                vsData.size(), &layout);
                    if (SUCCEEDED(hr)) {
                        inputLayouts_[format] = layout;
                    }
                }
            }
            vsRefl_ = newVsRefl;

            {
                using Cat = ConstantInfo::Category;
                D3D11_SHADER_DESC vsDesc{};
                hr = vsRefl_->GetDesc(&vsDesc);
                vsConstants.clear();

                char const *names[]{"CBPass", "CBObject", "CBPipe"};
                Cat categories[]{Cat::Pass, Cat::Object, Cat::Pipe};
                for (int bufIdx = 0; bufIdx < 3; ++bufIdx) {
                    auto bufName = names[bufIdx];
                    auto bufCat = categories[bufIdx];

                    if (auto *cb = vsRefl_->GetConstantBufferByName(bufName)) {
                        D3D11_SHADER_BUFFER_DESC cbDesc{};
                        hr = cb->GetDesc(&cbDesc);

                        auto cbSize = (std::max)(16u, cbDesc.Size); // Handle cases where no members are used and as
                                                                    // such, we're optimized to zero size
                        auto &constantBuffer = constantBuffers[bufCat] = CBBase(cbSize);
                        D3D11_BUFFER_DESC bufDesc{
                            .ByteWidth = cbSize,
                            .Usage = D3D11_USAGE_DEFAULT,
                            .BindFlags = D3D11_BIND_CONSTANT_BUFFER,
                            .CPUAccessFlags{},
                            .MiscFlags{},
                            .StructureByteStride{},
                        };
                        hr = dev->CreateBuffer(&bufDesc, nullptr, &constantBuffer.gpu);

                        for (UINT varIdx = 0; varIdx < cbDesc.Variables; ++varIdx) {
                            auto *var = cb->GetVariableByIndex(varIdx);
                            D3D11_SHADER_VARIABLE_DESC varDesc{};
                            hr = var->GetDesc(&varDesc);
                            LOG_F(INFO, "CB {}: {}, @{}, ={}", cbDesc.Name, varDesc.Name, varDesc.StartOffset,
                                  varDesc.Size);
                            vsConstants[varDesc.Name] = {
                                .offset = varDesc.StartOffset,
                                .size = varDesc.Size,
                                .category = bufCat,
                            };
                        }
                    }
                }

                // HACK(LV): do the same thing for psConstants when PS changes
                psConstants = vsConstants;
            }
        }
    }

    if (psObj_.Updated()) {
        auto psData = psObj_.Slurp().value();

        CComPtr<ID3D11PixelShader> newPs;
        hr = dev->CreatePixelShader(psData.data(), psData.size(), nullptr, &newPs);

        CComPtr<ID3D11ShaderReflection> newPsRefl;
        hr = D3DReflect(psData.data(), psData.size(), IID_PPV_ARGS(&newPsRefl));

        if (newPs && newPsRefl) {
            ps_ = newPs;
            psRefl_ = newPsRefl;

            D3D11_SHADER_DESC psDesc{};
            hr = psRefl_->GetDesc(&psDesc);
        }
    }
}
} // namespace pog
