#!/usr/bin/env python

from PyPoE.poe.constants import VERSION
from PyPoE.poe.file import specification
import json

with open('libpoe/spec/stable.json', 'w', newline='\n') as f:
    json.dump(specification.load(version = VERSION.STABLE).as_dict(), f, indent = 4)
