@setlocal enableextensions
@set TRIPLET=x64-windows
@for %%i in ("%~dp0..\..") do @set "WORKSPACE=%%~fi"

vcpkg.exe --triplet %TRIPLET% install --recurse ^
@%~dp0\vcpkg.txt && ^
vcpkg.exe --triplet %TRIPLET% export ^
--output=vcpkg --output-dir=%WORKSPACE%\deps --raw ^
@%~dp0\vcpkg-export.txt

:eof
@endlocal