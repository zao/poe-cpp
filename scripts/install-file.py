import pathlib
import shutil
import sys

src_path = pathlib.Path(sys.argv[1])
dst_path = pathlib.Path(sys.argv[2])

print(f'Copying {src_path} to {dst_path}\n')
shutil.copyfile(src_path, dst_path)