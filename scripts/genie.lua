local action = _ACTION or ""

QT_HOME= "C:/Qt/6.2.2/msvc2019_64"
FXC = "fxc"
MOC = path.join(QT_HOME, "bin/moc")
UIC = path.join(QT_HOME, "bin/uic")

SCRIPTS_DIR = path.getabsolute(".")
POE_CPP_DIR = path.getabsolute("..")
DEPS_DIR = path.getabsolute("../../deps/vcpkg/installed/x64-windows")

ASSETS_DIR = path.join(POE_CPP_DIR, "assets")
VENDOR_DIR = path.join(POE_CPP_DIR, "vendor")
LIBPOE_DIR = path.join(POE_CPP_DIR, "libpoe")
LIBPOG_DIR = path.join(POE_CPP_DIR, "libpog")
UI_COMMON_DIR = path.join(POE_CPP_DIR, "libs/ui_common")

BLAKE3_DIR = path.join(VENDOR_DIR, "BLAKE3-team/BLAKE3")

CUBBY_DIR = path.join(POE_CPP_DIR, "apps/cubbyhole")
MATGEN_DIR = path.join(POE_CPP_DIR, "apps/matgen")
SQUINT_DIR = path.join(POE_CPP_DIR, "apps/squint")
SWEEP_DIR = path.join(POE_CPP_DIR, "apps/sweep")

GEN_DIR = path.getabsolute("../.build/" .. action .. "/gen")
TARGET_DIR = path.join(POE_CPP_DIR, "bin")

function copy_file(src, dst)
	if dst:match('[/\\]$') then dst = path.join(dst, path.getname(src)) end
		
	return {
		src,
		dst,
		{
			path.join(DEPS_DIR, "tools/python3/python.exe"),
			-- "$(ProjectDir)" .. path.translate(path.getrelative("scripts", path.join(DEPS_DIR, "tools/python3/python.exe"))),
			path.join(SCRIPTS_DIR, "install-file.py"),
		},
		{
			--"echo $(1) $(2) $(<) $(@)",
			"start /B $(1) $(2) $(<) $(@)",
		}
	}
end

function install_asset(src, dst_name)
	if not dst_name then dst_name = path.getname(src) end
	return copy_file(path.join(ASSETS_DIR, src), TARGET_DIR .. "/" .. dst_name)
end

function compile_shader(src, shader_models, entrypoint)
	local ret = {}
	for _, sm in ipairs(shader_models) do
		local dst = path.join(TARGET_DIR, path.getbasename(src).."-"..sm ..".cso")
		ret[#ret+1] = {
			src,
			dst,
			{},
			{
				"fxc.exe /T "..sm.." /E "..entrypoint.." /Zi /Fo $(@) $(<)",
			}
		}
	end
	return ret
end

solution "poe-cpp"
    configurations { "Debug", "Release" }
    platforms { "x64" }

    location("../.build/" .. action)

    startproject "Cubbyhole"
	
	flags {
        "CppLatest",
		"ReleaseRuntime",
		"FullSymbols",
		"Symbols",
		"Unicode",
    }

    defines {
        "NOMINMAX",
        "_WIN32_WINNT=0x0601",
		"_CRT_SECURE_NO_DEPRECATE=1",
		"_CRT_SECURE_NO_WARNINGS=1",
		"_SCL_SECURE_NO_DEPRECATE=1",
		"_SCL_SECURE_NO_WARNINGS=1",
		"LOGURU_USE_FMTLIB",
    }
	
	systemincludedirs(path.join(DEPS_DIR, "include"))
    libdirs(path.join(DEPS_DIR, "lib"))
	
	targetdir(TARGET_DIR)
	
	configuration "Release"
		flags {
			"OptimizeSpeed"
		}
		
	configuration "vs*"
		buildoptions_cpp {"/Zc:__cplusplus"}

project "loguru"
	kind "StaticLib"
	language "C++"
	
	files {
		path.join(DEPS_DIR, "include/loguru/loguru.cpp"),
		path.join(DEPS_DIR, "include/loguru/loguru.hpp"),
	}


project "BLAKE3"
	kind "StaticLib"
	language "C"
	
	files {
		path.join(BLAKE3_DIR, "c/blake3.c"),
		path.join(BLAKE3_DIR, "c/blake3.h"),
		path.join(BLAKE3_DIR, "c/blake3_dispatch.c"),
		path.join(BLAKE3_DIR, "c/blake3_impl.h"),
		path.join(BLAKE3_DIR, "c/blake3_portable.h"),
		path.join(BLAKE3_DIR, "c/blake3_*.asm"),
	}
	
	removefiles {
		path.join(BLAKE3_DIR, "c/blake3_neon.*")
	}


project "libpoe"
    kind "StaticLib"
    language "C++"
	
	links {
		"BLAKE3",
	}

    local ROOT = LIBPOE_DIR
    includedirs(ROOT)
	includedirs(path.join(BLAKE3_DIR, "c"))
    
    files {
		path.join(ROOT, "**.cpp"),
		path.join(ROOT, "**.hpp"),
	}
	
	custombuildtask {
		copy_file(path.join(ROOT, "spec/stable.json"), path.join(TARGET_DIR, "stable.json")),
		copy_file(path.join(ASSETS_DIR, "simple_fixed.hlsl"), path.join(TARGET_DIR, "simple_fixed.hlsl")),
		copy_file(path.join(ASSETS_DIR, "RobotoMono-Regular.ttf"), path.join(TARGET_DIR, "RobotoMono-Regular.ttf")),
		copy_file(path.join(ASSETS_DIR, "Roboto-Regular.ttf"), path.join(TARGET_DIR, "Roboto-Regular.ttf")),
	}
	
	configuration "stable.json"
		buildaction "Copy"


project "libpog"
    kind "StaticLib"
    language "C++"
	
	links {}

    local ROOT = LIBPOG_DIR
    includedirs(ROOT)
    includedirs(LIBPOE_DIR)
    
    files {
		path.join(ROOT, "**.cpp"),
		path.join(ROOT, "**.hpp"),
	}


project "ui_common"
	kind "StaticLib"
	language "C++"
	
	links {
		"libpoe"
	}
	
	local ROOT = UI_COMMON_DIR
	includedirs(ROOT)
    includedirs(LIBPOE_DIR)
	
	files {
		path.join(ROOT, "ui_common.cpp"),
		path.join(ROOT, "ui_common.hpp"),
		path.join(ROOT, "imgui_memory_editor.h"),
	}

project "squint"
    kind "ConsoleApp"
    language "C++"

    local ROOT = SQUINT_DIR
    includedirs(ROOT)
    includedirs(LIBPOE_DIR)
    includedirs(LIBPOG_DIR)
	includedirs(UI_COMMON_DIR)

    links {
		"abseil_dll",
		"cpr",
		"d2d1",
		"d3d11",
		"d3dcompiler",
		"DirectXTK",
		"dxgi",
        "fmt",
		"glfw3dll",
        "imgui",
		"jansson",
		"libcurl",
		"libpoe",
		"libpog",
		"loguru",
		"openvr_api",
		"re2",
		"zstd",
    }

    files {
        path.join(ROOT, "**.cpp"),
        path.join(ROOT, "**.hpp"),
    }

    custombuildtask {
		copy_file(path.join(DEPS_DIR, "bin/abseil_dll.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/fmt.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/glfw3.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/jansson.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/libcurl.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/openvr_api.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/re2.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/tbb.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/tbbmalloc.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/zstd.dll"), TARGET_DIR .. "/"),
		install_asset("NotoSansCJKjp-VF.ttf"),
		install_asset("NotoSansSymbols-Regular.ttf"),
		install_asset("NotoSansSymbols2-Regular.ttf"),
		install_asset("OpenFontIcons.ttf"),
        install_asset("RecMonoDuotone-Regular.ttf"),
		install_asset("Roboto-Regular.ttf"),
        install_asset("RobotoMono-Regular.ttf"),
    }
	
	custombuildtask(compile_shader(path.join(ASSETS_DIR, "Art@Textures@NPC@Octavius@Octaviusc.hlsl"), {"vs_5_0"}, "vs_main"))
	custombuildtask(compile_shader(path.join(ASSETS_DIR, "Art@Textures@NPC@Octavius@Octaviusc.hlsl"), {"ps_5_0"}, "ps_main"))
	custombuildtask(compile_shader(path.join(ASSETS_DIR, "FsBlend.hlsl"), {"vs_4_0"}, "vs_main"))
	custombuildtask(compile_shader(path.join(ASSETS_DIR, "FsBlend.hlsl"), {"ps_4_0"}, "ps_main"))
	custombuildtask(compile_shader(path.join(ASSETS_DIR, "TintedUnlit3D.hlsl"), {"vs_4_0"}, "vs_main"))
	custombuildtask(compile_shader(path.join(ASSETS_DIR, "TintedUnlit3D.hlsl"), {"ps_4_0"}, "ps_main"))
	custombuildtask(compile_shader(path.join(ASSETS_DIR, "TileMesh3D.hlsl"), {"vs_5_0"}, "vs_main"))
	custombuildtask(compile_shader(path.join(ASSETS_DIR, "TileMesh3D.hlsl"), {"ps_5_0"}, "ps_main"))

project "sweep"
    kind "ConsoleApp"
    language "C++"

    local ROOT = SWEEP_DIR
    includedirs(ROOT)
    includedirs(LIBPOE_DIR)
	includedirs(LIBPOG_DIR)

    links {
		"abseil_dll",
		"cpr",
        "fmt",
		"jansson",
		"libcurl",
		"libpoe",
		"libpog",
		"loguru",
		"re2",
		"zstd",
    }

    files {
        path.join(ROOT, "**.cpp"),
        path.join(ROOT, "**.hpp"),
    }

    custombuildtask {
		copy_file(path.join(DEPS_DIR, "bin/abseil_dll.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/fmt.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/jansson.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/libcurl.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/re2.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/tbb.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/tbbmalloc.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/zstd.dll"), TARGET_DIR .. "/"),
    }

project "matgen"
    kind "ConsoleApp"
    language "C++"

    local ROOT = MATGEN_DIR
    includedirs(ROOT)
    includedirs(LIBPOE_DIR)
	includedirs(LIBPOG_DIR)

    links {
		"abseil_dll",
		"cpr",
		"d3dcompiler",
        "fmt",
		"jansson",
		"libcurl",
		"libpoe",
		"libpog",
		"loguru",
		"re2",
		"zstd",
    }

    files {
        path.join(ROOT, "**.cpp"),
        path.join(ROOT, "**.hpp"),
    }

    custombuildtask {
		copy_file(path.join(DEPS_DIR, "bin/abseil_dll.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/fmt.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/jansson.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/libcurl.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/re2.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/tbb.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/tbbmalloc.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(DEPS_DIR, "bin/zstd.dll"), TARGET_DIR .. "/"),
    }

project "Cubbyhole"
	kind "ConsoleApp"
    language "C++"

    links {
		"abseil_dll",
		"cpr",
		"d2d1",
		"d3d11",
		"d3dcompiler",
		"DirectXTK",
		"dxgi",
        "fmt",
        "imgui",
		"jansson",
		"libcurl",
		"libpoe",
		"libpog",
		"loguru",
		"re2",
		"Qt6Core",
		"Qt6Gui",
		"Qt6Widgets",
		"zstd",
    }

    local ROOT = CUBBY_DIR
	local GEN = path.join(GEN_DIR, "Cubbyhole")
    includedirs(ROOT)
    includedirs(LIBPOE_DIR)
    includedirs(LIBPOG_DIR)
	includedirs(UI_COMMON_DIR)
	systemincludedirs(path.join(QT_HOME, "include"))
	systemincludedirs(path.join(POE_CPP_DIR, "vendor/samhocevar/portable-file-dialogs"))
	includedirs(GEN)
	
	libdirs(path.join(QT_HOME, "lib"))

    files {
		path.join(CUBBY_DIR, "**.cc"),
        path.join(CUBBY_DIR, "**.cpp"),
        path.join(CUBBY_DIR, "**.hpp"),
		path.join(GEN, "**.moc.cpp"),
		path.join(GEN, "**.ui.hpp"),
    }
	
	removefiles {
		path.join(CUBBY_DIR, "cubby_qt/main_window.hpp"),
        path.join(CUBBY_DIR, "cubby_qt/tilesets.hpp"),
	}
	
	custombuildtask {
		{ path.join(ROOT, "main_window.ui"), path.join(GEN, "main_window.ui.hpp"), {}, { UIC .. " $(<) -o $(@)" }},
        { path.join(ROOT, "tilesets.ui"), path.join(GEN, "tilesets.ui.hpp"), {}, { UIC .. " $(<) -o $(@)" }},
		{ path.join(ROOT, "cubby_qt/main_window.hpp"), path.join(GEN, "cubby_qt/main_window.moc.cpp"), {}, { MOC .. " $(<) -o $(@)" }},
        { path.join(ROOT, "cubby_qt/tilesets.hpp"), path.join(GEN, "cubby_qt/tilesets.moc.cpp"), {}, { MOC .. " $(<) -o $(@)" }},
		copy_file(path.join(DEPS_DIR, "bin/abseil_dll.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(QT_HOME, "bin/Qt6Core.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(QT_HOME, "bin/Qt6Gui.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(QT_HOME, "bin/Qt6Widgets.dll"), TARGET_DIR .. "/"),
		copy_file(path.join(QT_HOME, "plugins/platforms/qwindows.dll"), TARGET_DIR .. "/plugins/platforms/")
	}