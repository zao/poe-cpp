./vcpkg install --triplet x64-windows --recurse \
abseil \
asio \
docopt \
fmt \
glew \
glfw3 \
imgui[docking-experimental,glfw-binding,opengl3-glew-binding] \
loguru \
mio \
ms-gsl \
nlohmann-json \
opengl \
sqlite3
