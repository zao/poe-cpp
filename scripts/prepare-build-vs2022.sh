if [ "$#" -ne 1 ]; then
    echo "Need a directory for dependencies"
    exit 1
fi

deps_dir="$1"

cmake "$PWD/repos/poe-cpp" \
-B "$PWD/builds/poe-cpp-vs2022" \
-G "Visual Studio 17 2022" -A x64 \
-DCMAKE_TOOLCHAIN_FILE="${deps_dir}/vcpkg-vs2022/scripts/buildsystems/vcpkg.cmake"