./vcpkg install --triplet x64-windows --recurse \
abseil \
asio \
boost \
bustache \
crossguid \
ctre \
directxmath \
directxtk \
docopt \
fmt \
glew \
glfw3 \
gli \
glm \
imgui[docking-experimental,dx11-binding,glfw-binding,opengl3-binding,win32-binding] \
leveldb \
loguru \
mio \
ms-gsl \
nlohmann-json \
opengl \
sqlite3
