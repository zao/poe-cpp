#pragma once

#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "imgui_stdlib.h"

#include <poe/io/vfs.hpp>

#include <string_view>

namespace ui_common {
enum ui_flags {
    ui_flags_none = 0,
    ui_flags_use_default_fonts = 1,
};

struct ui_context {
    ui_context(std::string_view window_title, ui_flags flags = ui_flags_none);
    ~ui_context();

    ui_context(ui_context const &) = delete;
    ui_context &operator=(ui_context const &) = delete;

    void load_fonts(poe::io::vfs const &vfs);

    bool new_frame_with_window();
    bool new_frame();

    std::string window_title_;
    ui_flags flags_;
    GLFWwindow *win_;
    float dpi_scale_factor_;
    ImGuiIO *io_{};
    ImFont *fontin_;
};
} // namespace ui_common