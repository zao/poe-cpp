#include "ui_common.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

namespace ui_common {
ui_context::ui_context(std::string_view window_title, ui_flags flags) : window_title_(window_title), flags_(flags) {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

    int mon_x, mon_y, mon_w, mon_h;
    {
        auto *mon = glfwGetPrimaryMonitor();
        glfwGetMonitorWorkarea(glfwGetPrimaryMonitor(), &mon_x, &mon_y, &mon_w, &mon_h);

        float x_scale, y_scale;
        glfwGetMonitorContentScale(mon, &x_scale, &y_scale);
        dpi_scale_factor_ = (std::min)(x_scale, 1.0f);
    }

    win_ = glfwCreateWindow(mon_w * 8 / 10, mon_h * 8 / 10, window_title_.c_str(), nullptr, nullptr);
    glfwMakeContextCurrent(win_);
    glfwSwapInterval(1);

    glewInit();

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    io_ = &ImGui::GetIO();
    io_->ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    io_->ConfigFlags |= ImGuiConfigFlags_DockingEnable;

    ImGui::StyleColorsLight();
    {
        auto &style = ImGui::GetStyle();
        style.ScaleAllSizes(dpi_scale_factor_);
    }

    ImGui_ImplGlfw_InitForOpenGL(win_, true);
    ImGui_ImplOpenGL3_Init("#version 450");

    if (flags_ & ui_flags_use_default_fonts) {
        ImFontConfig cfg{};
        cfg.SizePixels = 18.0f * dpi_scale_factor_;
        io_->Fonts->AddFontDefault(&cfg);
    }
}

ui_context::~ui_context() {
    glfwMakeContextCurrent(nullptr);
    glfwDestroyWindow(win_);
    glfwTerminate();
}

void ui_context::load_fonts(poe::io::vfs const &vfs) {
    struct load_info {
        poe::util::path path_;
        ImWchar const *glyph_range_;
    };

    using namespace poe::util::string_literals;
    load_info infos[] = {
        {"Art/2DArt/Fonts/Fontin-Regular.ttf"_poe, io_->Fonts->GetGlyphRangesDefault()},
        {"Art/2DArt/Fonts/Fontin-Regular.ttf"_poe, io_->Fonts->GetGlyphRangesCyrillic()},
        {"Art/2DArt/Fonts/YDSapphIIM.ttf"_poe, io_->Fonts->GetGlyphRangesKorean()},
    };

    bool font_loaded = false;
    for (auto &info : infos) {
        if (auto file = vfs.open_file(info.path_)) {
            auto payload = file->read_all();
            auto buf_size = payload.size();
            auto *buf_data = static_cast<std::byte *>(ImGui::MemAlloc(buf_size));
            memcpy(buf_data, payload.data(), payload.size());

            ImFontConfig cfg{};
            cfg.MergeMode = font_loaded;
            // Memory ownership of buf_data is transfered with the AddFont call
            fontin_ = io_->Fonts->AddFontFromMemoryTTF(buf_data, static_cast<int>(buf_size), 18.0f * dpi_scale_factor_,
                                                       &cfg, info.glyph_range_);
            font_loaded = true;
        }
    }
}

bool ui_context::new_frame_with_window() {
    if (glfwWindowShouldClose(win_)) {
        return false;
    }

    glfwPollEvents();

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;

    {
        ImGuiViewport *viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->WorkPos);
        ImGui::SetNextWindowSize(viewport->WorkSize);
        ImGui::SetNextWindowViewport(viewport->ID);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
        window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize |
                        ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
    }

    ImGui::Begin("RootWindow", nullptr, window_flags);

    ImGui::PopStyleVar(2);

    return true;
}

bool ui_context::new_frame() {
    if (glfwWindowShouldClose(win_)) {
        return false;
    }

    glfwPollEvents();

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;

    {
        ImGuiViewport *viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->WorkPos);
        ImGui::SetNextWindowSize(viewport->WorkSize);
        ImGui::SetNextWindowViewport(viewport->ID);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
        window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize |
                        ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
    }

    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
    ImGui::Begin("RootWindow", nullptr, window_flags);
    ImGui::PopStyleVar();

    ImGui::PopStyleVar(2);

    {
        ImGuiID dockspace_id = ImGui::GetID("TopDockspace");
        ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
    }
    return true;
}
} // namespace ui_common