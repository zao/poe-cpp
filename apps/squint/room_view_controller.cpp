﻿#include "room_view_controller.hpp"

#include <pog/grid.hpp>

#include <glm/gtc/type_ptr.hpp>
#include <imgui.h>
#include <imgui_stdlib.h>

#include <algorithm>
#include <numeric>
#include <map>
#include <random>
#include <set>

namespace arm_ns = poe::format::arm;

RoomViewController::RoomViewController(std::shared_ptr<pog::DxContext> dx) : SceneViewController(dx), meshDrawer_(dx) {}

std::optional<glm::vec3> AimCoord(glm::ivec2 screenPos, glm::ivec2 screenSize, glm::mat4 viewMatrix,
                                  glm::mat4 projMatrix) {
    glm::vec3 srcNear{screenPos.x, screenSize.y - 1 - screenPos.y, 0.0f};
    glm::vec3 srcFar{screenPos.x, screenSize.y - 1 - screenPos.y, 1.0f};
    glm::vec4 viewport{0.0f, 0.0f, screenSize};
    glm::vec3 vNear = glm::unProjectZO(srcNear, viewMatrix, projMatrix, viewport);
    glm::vec3 vFar = glm::unProjectZO(srcFar, viewMatrix, projMatrix, viewport);
    glm::vec3 worldRay = glm::normalize(vFar - vNear);

    glm::vec3 planeNormal{0.0f, 0.0f, -1.0f};
    glm::vec3 planePoint{0.0f, 0.0f, 0.0f};
    float denom = glm::dot(worldRay, planeNormal);
    if (fabs(denom) > 0.00001f) {
        float d = glm::dot(planePoint - vNear, planeNormal) / denom;
        glm::vec3 p = vNear + worldRay * d;
        return p;
    }
    return std::nullopt;
}

void RoomViewController::Update(pog::FrameUpdate const &update, std::shared_ptr<pog::GameData> gameData) {
    using pog::TileCorner;
    using pog::TileKey;
    using pog::TileMutation;
    cam_.winSize = update.fbSize;
    RunCameraControlUi(cam_);

    debugTime_ = update.time;

    if (!gameData) {
        return;
    }
    gameData_ = gameData;

    glm::mat4 viewMat = cam_.ViewMatrix();
    glm::mat4 projMat = cam_.ProjectionMatrix();

    slotAim_ = AimCoord(update.input.cursorPos, update.fbSize, viewMat, projMat);

    for (auto &ev : update.input.events) {
        using InputEvent = pog::FrameUpdate::Input::InputEvent;
        if (ev.kind == InputEvent::MouseRelease && ev.data.mouseRelease.button == pog::MouseButton::Left) {
            slotFocus_.reset();
            if (roomInfo_) {
                if (slotAim_) {
                    slotFocus_ = glm::ivec2(glm::floor(glm::vec2(*slotAim_) / 250.0f));
                }
            }
        }
    }

    if (auto graph = gameData->CurrentGraph(); graph_ != graph) {
        graph_ = graph;
        desiredArm_.reset();
        desiredArmName_.clear();
        if (graph_) {
            if (auto fallbackI = graph_->arms.begin(); fallbackI != graph_->arms.end()) {
                desiredArm_ = fallbackI->second;
                desiredArmName_ = fallbackI->first;
            }
        }
    }

    if (graph_) {
        if (ImGui::Begin("Room choice")) {
            if (ImGui::BeginListBox("##roomselect", {-FLT_MIN, ImGui::GetTextLineHeight() * 20.0f})) {
                for (auto &[name, arm] : graph_->arms) {
                    bool selected = desiredArm_ == arm;
                    if (ImGui::Selectable(name.c_str(), selected)) {
                        desiredArm_ = arm;
                        desiredArmName_ = name;
                    }
                }
                ImGui::EndListBox();
            }
        }
        ImGui::End();
    }

    if (graph_ && desiredArm_ != arm_) {
        ChangeRoom();
        if (auto I = graph_->tsi->fields_.find("StoreyHeight"); I != end(graph_->tsi->fields_)) {
            storeyHeight_ = std::stoi(I->second);
        } else {
            storeyHeight_ = 32; // TODO(LV): figure out the default unless it's always specified
        }

        std::mt19937 rng(42u);

        for (auto &[_, slot] : design_->slots_) {
            if (!slot.candidates.empty()) {
                slot.choice = slot.candidates.front();
            }
        }

#if 0
        for (auto &[slotCoord, slot] : room_->slots_) {
            auto candidates = SlotCandidates(slot.key);
            if (candidates.size() == 1) {
                TileData tileData = candidates.front();
                PopulateGeometry(tileData, gameData);
                slot.usedTile = tileData;
            } else if (candidates.size() > 1) {
                size_t idx = std::uniform_int<uint32_t>(0u, (uint32_t)candidates.size() - 1)(rng);
                TileData tileData = candidates[idx];
                PopulateGeometry(tileData, gameData);
                slot.usedTile = tileData;
            }
        }
        glm::vec3 roomV1 = glm::vec3(glm::vec2(room_->size_) * 250.0f, 0.0f);
        cam_.look.at = glm::vec3(roomV1 / 3.0f);
        cam_.look.distance = 3500.0f;
#endif
    }

    if (graph_ && design_) {
        ImGui::Begin("Design slots");

        if (ImGui::CollapsingHeader("Slot grid")) {
            glm::ivec2 slotCover{};
            for (auto &[coord, slot] : design_->slots_) {
                slotCover = glm::max(slotCover, coord + 1);
            }
            std::vector<std::vector<pog::RoomDesign::Slot const *>> grid(
                slotCover.y, std::vector<pog::RoomDesign::Slot const *>(slotCover.x));
            for (auto &[coord, slot] : design_->slots_) {
                grid[coord.y][coord.x] = &slot;
            }
            for (int rowIdx = slotCover.y - 1; rowIdx >= 0; --rowIdx) {
                auto &gridRow = grid[rowIdx];
                for (int colIdx = 0; colIdx < slotCover.x; ++colIdx) {
                    glm::ivec2 coord{colIdx, rowIdx};
                    if (coord.x != 0) {
                        ImGui::SameLine();
                    }

                    ImVec2 gridSize{80, 20};
                    auto slotPtr = gridRow[colIdx];
                    if (slotPtr) {
                        auto &slot = *slotPtr;
                        char kind = slot.info->KindChar();
                        bool selected = slotFocus_ == coord;
                        std::string label;
                        if (kind == 'k') {
                            label = fmt::format("({} {}x{})##slot_c{}r{}", (char)std::toupper(kind), slot.info->size.x,
                                                slot.info->size.y, coord.x, coord.y);
                        } else {
                            label = fmt::format("({})##slot_c{}r{}", (char)std::toupper(kind), coord.x, coord.y);
                        }
                        {
                            ImU32 c;
                            switch (kind) {
                            case 'k':
                                c = ImColor::HSV(0.0f, 1.0f, 0.3f);
                                break;
                            case 'f':
                                c = ImColor::HSV(0.2f, 1.0f, 0.3f);
                                break;
                            case 'n':
                                c = ImColor::HSV(0.4f, 1.0f, 0.3f);
                                break;
                            case 's':
                                c = ImColor::HSV(0.6f, 1.0f, 0.3f);
                                break;
                            case 'o':
                                c = ImColor::HSV(0.8f, 1.0f, 0.3f);
                                break;
                            default:
                                c = ImColor::HSV(0.0f, 0.0f, 0.0f);
                            }
                            ImGui::PushStyleColor(ImGuiCol_Text, c);
                        }
                        if (ImGui::Selectable(label.c_str(), &selected, 0, gridSize)) {
                            slotFocus_ = coord;
                        }
                        ImGui::PopStyleColor(ImGuiCol_Text);
                    } else {
                        ImGui::Dummy(gridSize);
                    }
                }
            }
        }

        if (slotFocus_) {
            if (auto resCoord = roomInfo_->ResolveCoord(*slotFocus_)) {
                auto &slot = design_->slots_.find(*resCoord)->second;
                // ImGui::Text("%s", key->source.c_str());
                auto slotTag = slot.info->TagConstraint();
                ImGui::Text("Tag: %s", std::string(slotTag.value_or("None")).c_str());
                auto ecs = slot.info->EdgeConstraintAssoc();
                for (int i = 0; i < 4; ++i) {
                    auto side = (pog::TileSide)i;
                    if (auto ecI = ecs.find(side); ecI != ecs.end()) {
                        auto &ec = ecI->second;

                        struct ResolvedEt {
                            std::shared_ptr<poe::format::et::EtFile> self;
                            std::string selfPath;
                            std::shared_ptr<poe::format::gt::GtFile> gts[2];
                        };

                        auto ResolveEt = [&gameData](auto etPath) -> ResolvedEt {
                            ResolvedEt ret{
                                .self = gameData->GetEt(etPath),
                                .selfPath = etPath,
                            };
                            for (int i = 0; i < 2; ++i) {
                                ret.gts[i] = gameData->GetGt(ret.self->gts[i]);
                            }
                            return ret;
                        };
                        auto resEt = ResolveEt(ec.name.value());

                        if (resEt.self) {
                            auto et = resEt.self;
                            ImGui::Text("ET %s (%s)", et->id.c_str(), ec.name->c_str());
                            ImGui::Indent();
                            auto PrintGts = [&](ResolvedEt &resEt) {
                                for (int gtIdx = 0; gtIdx < 2; ++gtIdx) {
                                    auto &gtPath = resEt.self->gts[gtIdx];
                                    if (gtPath != "wildcard") {
                                        auto gt = resEt.gts[gtIdx];
                                        ImGui::Text("GT %s (%s)", gt->id.c_str(), gtPath.c_str());
                                    } else {
                                        ImGui::Text("GT wildcard");
                                    }
                                }
                            };

                            PrintGts(resEt);

                            if (et->virt) {
                                ImGui::Indent();
                                int virtOffs[2]{ec.real, ec.virt};
                                for (int virtIdx = 0; virtIdx < 2; ++virtIdx) {
                                    auto virtEt = ResolveEt(et->virt->etPaths[virtIdx]);
                                    ImGui::Text("%s %d %s %d (%s)", SideSymbol(side), virtOffs[virtIdx],
                                                virtEt.self->id.c_str(), et->virt->etVals[virtIdx],
                                                et->virt->etPaths[virtIdx].c_str());
                                    ImGui::Indent();
                                    PrintGts(virtEt);
                                    ImGui::Unindent();
                                }
                                ImGui::Unindent();
                            } else {
                                ImGui::Text("%s %d %s (%s)", SideSymbol(side), ec.real, et->id.c_str(),
                                            ec.name->c_str());
                            }
                            ImGui::Unindent();
                        } else {
                            ImGui::Text("ET %s %d/%d (%s)", SideSymbol(side), ec.real, ec.virt, ec.name->c_str());
                        }
                    } else {
                        ImGui::Text("ET %s (%s)", SideSymbol(side), "None");
                    }
                }
                auto gcs = slot.info->GroundConstraintAssoc();
                auto heights = slot.info->CornerHeightAssoc();
                for (int i = 0; i < 4; ++i) {
                    auto corner = (TileCorner)i;
                    auto height = heights[corner];
                    if (auto gcI = gcs.find(corner); gcI != gcs.end()) {
                        auto &gc = gcI->second;
                        auto gt = gameData->GetGt(*gc.name);
                        if (gt) {
                            ImGui::Text("GT %s %s (%s), height %d", pog::CornerSymbol(corner), gt->id.c_str(),
                                        gc.name->c_str(), height.height);
                        } else {
                            ImGui::Text("GT %s (%s), height %d", pog::CornerSymbol(corner), gc.name->c_str(),
                                        height.height);
                        }
                    } else {
                        ImGui::Text("GT %s (%s), height %d", pog::CornerSymbol(corner), "None", height.height);
                    }
                }

                auto MutStr = [](TileMutation mut) -> std::string {
                    switch (mut.rot) {
                    case TileKey::Rotation::R0:
                        return mut.flip ? "F" : "I";
                    case TileKey::Rotation::R90:
                        return mut.flip ? "FR90" : "R90";
                    case TileKey::Rotation::R180:
                        return mut.flip ? "FR180" : "R180";
                    case TileKey::Rotation::R270:
                        return mut.flip ? "FR270" : "R270";
                    default:
                        return "";
                    }
                };

                if (ImGui::CollapsingHeader("Candidates")) {
                    for (auto &cand : slot.candidates) {
                        std::string label = fmt::format("{} {}", cand.path, MutStr(cand.mutation));
                        bool selected = slot.choice
                                            ? slot.choice->tile == cand.tile && slot.choice->mutation == cand.mutation
                                            : false;
                        if (ImGui::Selectable(label.c_str(), selected)) {
                            slot.choice = cand;
                        }
                    }
                }
                if (ImGui::CollapsingHeader("Rejects")) {
                    for (auto &rej : slot.rejects) {
                        auto &cand = rej.candidate;
                        std::string label = fmt::format("{} {}", cand.path, MutStr(cand.mutation));
                        ImGui::Text("Candidate: %s", label.c_str());
                        ImGui::Text("Reason: %s", rej.reason.c_str());
                        ImGui::Spacing();
                    }
                }
            }
        }

        ImGui::End();
    }

#if 0
    if (room_) {
        ImGui::Begin("Room slots");

        if (ImGui::CollapsingHeader("Slot grid")) {
            for (int row = room_->size_.y - 1; row >= 0; --row) {
                for (int col = 0; col < room_->size_.x; ++col) {
                    if (col != 0) {
                        ImGui::SameLine();
                    }
                    glm::ivec2 coord{col, row};
                    ImVec2 gridSize{80, 0};
                    if (auto slotI = room_->slots_.find(coord); slotI != end(room_->slots_)) {
                        auto &slot = slotI->second;
                        char kind = room_->slotKind_[coord];
                        bool selected = slotFocus_ == coord;
                        std::string label;
                        if (kind == 'k') {
                            label = fmt::format("({} {}x{})##slot_c{}r{}", (char)std::toupper(kind), slot.size.x,
                                                slot.size.y, col, row);
                        } else {
                            label = fmt::format("({})##slot_c{}r{}", (char)std::toupper(kind), col, row);
                        }
                        {
                            ImU32 c;
                            switch (kind) {
                            case 'k':
                                c = ImColor::HSV(0.0f, 1.0f, 0.3f);
                                break;
                            case 'f':
                                c = ImColor::HSV(0.2f, 1.0f, 0.3f);
                                break;
                            case 'n':
                                c = ImColor::HSV(0.4f, 1.0f, 0.3f);
                                break;
                            case 's':
                                c = ImColor::HSV(0.6f, 1.0f, 0.3f);
                                break;
                            case 'o':
                                c = ImColor::HSV(0.8f, 1.0f, 0.3f);
                                break;
                            default:
                                c = ImColor::HSV(0.0f, 0.0f, 0.0f);
                            }
                            ImGui::PushStyleColor(ImGuiCol_Text, c);
                        }
                        if (ImGui::Selectable(label.c_str(), &selected, 0, gridSize)) {
                            slotFocus_ = coord;
                        }
                        ImGui::PopStyleColor(ImGuiCol_Text);
                    } else {
                        ImGui::Dummy(gridSize);
                    }
                }
            }
        }

        if (slotFocus_) {
            auto &slot = room_->slots_[slotFocus_.value()];
            auto &key = slot.key;
            if (key) {
                ImGui::Text("%s", key->source.c_str());
                ImGui::Text("Tag: %s", key->tag ? key->tag->c_str() : "None");
                for (int i = 0; i < 4; ++i) {
                    auto side = (TileSide)i;
                    auto &str = key->etStr[i];
                    ImGui::Text("ET %s %d (%s)", SideSymbol(side), key->etVal[i], str ? str->c_str() : "None");
                }
                for (int i = 0; i < 4; ++i) {
                    auto corner = (TileCorner)i;
                    auto &str = key->gtStr[i];
                    ImGui::Text("GT %s (%s), height %d", CornerSymbol(corner), str ? str->c_str() : "None",
                                key->height[i]);
                }
            }
            if (ImGui::CollapsingHeader("Candidates")) {
                auto candidates = SlotCandidates(slot.key);
                for (auto &cand : candidates) {
                    ImGui::Text("%s", cand.path.c_str());
                }
            }
        }

        ImGui::End();

        for (auto &ev : update.input.events) {
            if (ev.kind == FrameUpdate::Input::InputEvent::MouseRelease) {
                auto &evData = ev.data.mouseRelease;
                if (evData.button == MouseButton::Left) {
                    if (design_) {
                        auto roomSize = design_->slots_.Size();
                        for (int row = 0; row < roomSize.y; ++row) {
                            for (int col = 0; col < roomSize.x; ++col) {
                                // hit test against reasonable bounds for tile selection
                            }
                        }
                    }
                }
            }
        }
    }
#endif

    struct EntryShim {
        using KSlot = arm_ns::KSlot;
        using TileSide = pog::TileSide;

        EntryShim(arm_ns::ARMFile const &arm, arm_ns::Entry const &entry)
            : arm(arm), entry(entry), k(std::get_if<arm_ns::KEntry>(&entry.var)), f(std::get_if<arm_ns::FEntry>(&entry.var)) {}

        std::optional<std::string> Tag() const {
            if (k) {
                return ArmString(k->tail_[(int)KSlot::Tag]);
            }
            if (f) {
                return ArmString(f->value_);
            }
            return {};
        }

        static int SideMap(TileSide side) {
            static std::map<TileSide, int> const sideMap{
                {TileSide::Top, 0},
                {TileSide::Right, 1},
                {TileSide::Bottom, 2},
                {TileSide::Left, 3},
            };
            return sideMap.find(side)->second;
        }

        std::optional<std::string> EdgeString(TileSide side) const {
            if (k) {
                return ArmString(k->tail_[(int)KSlot::EdgeStr + SideMap(side)]);
            }
            return {};
        }

        std::optional<std::string> GroundString(TileSide side) const {
            if (k) {
                return ArmString(k->tail_[(int)KSlot::GroundStr + SideMap(side)]);
            }
            return {};
        }

        int EdgeIndexReal(TileSide side) const {
            if (k) {
                return (int)k->tail_[(int)KSlot::EdgeIdx + SideMap(side) * 2];
            }
            return {};
        }

        int EdgeIndexVirt(TileSide side) const {
            if (k) {
                return (int)k->tail_[(int)KSlot::EdgeIdx + SideMap(side) * 2 + 1];
            }
            return {};
        }

        int Height(TileSide side) const {
            if (k) {
                return (int)k->tail_[(int)KSlot::Height + SideMap(side)];
            }
            return 0;
        }

        std::optional<std::string> ArmString(int64_t index) const {
            if (index <= 0 || index > (int64_t)arm.string_table_.size()) {
                return {};
            }
            return arm.string_table_[index - 1];
        }

        arm_ns::ARMFile const &arm;
        arm_ns::Entry const &entry;
        arm_ns::KEntry const *k;
        arm_ns::FEntry const *f;
    };

    if (ImGui::Begin("Config")) {
        ImGui::Checkbox("Render meshes", &renderGeometry_);
        ImGui::Checkbox("Render slot heights", &renderSlotHeights_);
        ImGui::Checkbox("Draw ground mesh", &drawGroundMesh_);
        ImGui::Checkbox("Draw main mesh", &drawMainMesh_);
        bool drawMainMesh_{true};
        {
            ImGui::Checkbox("Override mutation", &hasMutationOverride_);
            int rot = (int)mutationOverride_.rot;
            ImGui::RadioButton((char const *)u8"0°", &rot, 0);
            ImGui::SameLine();
            ImGui::RadioButton((char const *)u8"90°", &rot, 1);
            ImGui::SameLine();
            ImGui::RadioButton((char const *)u8"180°", &rot, 2);
            ImGui::SameLine();
            ImGui::RadioButton((char const *)u8"270°", &rot, 3);
            ImGui::Checkbox("Flip", &mutationOverride_.flip);
            mutationOverride_.rot = (TileKey::Rotation)rot;
        }
    }
    ImGui::End();

    if (roomInfo_) {
        if (ImGui::Begin("Focused design slot")) {
            if (slotFocus_) {
                ImGui::Text("Coord: %d, %d", slotFocus_->x, slotFocus_->y);
                glm::ivec2 coord = *slotFocus_;
                if (auto proxyI = roomInfo_->proxyEntries.find(coord); proxyI != roomInfo_->proxyEntries.end()) {
                    coord = proxyI->second;
                }
                if (auto rootI = roomInfo_->rootEntries.find(coord); rootI != roomInfo_->rootEntries.end()) {
                    auto &entryInfo = rootI->second;
                    if (auto *k = entryInfo.AsK()) {
                        using arm_ns::KSlot;
                        auto tail = std::span(k->tail_);
                        auto edgeStrs = tail.subspan((size_t)KSlot::EdgeStr, 4);
                        auto edgeIdxs = tail.subspan((size_t)KSlot::EdgeIdx, 8);
                        auto groundStrs = tail.subspan((size_t)KSlot::GroundStr, 4);
                        ImGui::Text("K %dx%d", k->w_, k->h_);
                        for (auto eStr : edgeStrs) {
                            ImGui::Text("ET string %d", eStr);
                        }
                    }
                }
            }
        }
        ImGui::End();
    }
}

static glm::mat4 MutationMatrix(pog::TileMutation mut, glm::ivec2 worldSize) {
    using Rot = pog::TileKey::Rotation;

    // Early-out for the identity case
    if (!mut.flip && mut.rot == Rot::R0) {
        return glm::mat4{1};
    }

    // `ret = uncenter * rotate * scale * center`
    // Center the tile on the origin; scale and rotate; move tile back adjusting for new extents to put the new lower
    // left corner at the origin. As glm functions go M*v we need to build ret up from left to right so this function
    // will seem backwards.

    bool oddRotation = mut.rot == Rot::R90 || mut.rot == Rot::R270;
    glm::vec3 const center = glm::vec3{worldSize, 0} / 2.0f;
    glm::mat4 ret =
        glm::translate(glm::mat4{1}, oddRotation ? center : glm::vec3{center.y, center.x, center.z}); // uncenter
    ret = glm::mat4{1};

    // scale
    if (mut.flip) {
        ret = glm::scale(ret, glm::vec3{-1, 1, 1});
    }

    // rotate
    if (mut.rot != Rot::R0) {
        float angle = 0.0f;
        switch (mut.rot) {
        case Rot::R0:
            angle = 0.0f;
            break;
        case Rot::R90:
            angle = glm::radians(90.0f);
            break;
        case Rot::R180:
            angle = glm::radians(180.0f);
            break;
        case Rot::R270:
            angle = glm::radians(270.0f);
            break;
        }
        ret = glm::rotate(ret, angle, glm::vec3{0, 0, 1});
    }

    return ret;

    // Center tile
    return glm::translate(ret, -center);
}

void RoomViewController::Draw(pog::LayerTarget const &rt) {
    using pog::LightSet;
    using pog::TileKey;

    auto *ctx = dx_->Context();
    glm::vec4 clearColor{0.3f, 0.1f, 0.2f, 1.0f};
    ctx->ClearRenderTargetView(rt.rtv, glm::value_ptr(clearColor));
    ctx->ClearDepthStencilView(rt.dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);

    glm::mat4 viewMat = cam_.ViewMatrix();
    glm::mat4 projMat = cam_.ProjectionMatrix();

    LightSet lights(dx_);
    // lights.lights.push_back(LightSet::Light{
    //     .pos = {-500.0f, -500.0f, -500.0f, 1.0f},
    //     .color = {1.0f, 1.0f, 1.0f},
    //     .intensity = 5.0f,
    // });
    lights.lights.push_back(LightSet::Light{
        .pos{-500.0f, -500.0f, -500.0f, 1.0f},
        .color{1.0f, 1.0f, 1.0f},
        .intensity = 1.0f,
    });

    lights.lights.push_back(LightSet::Light{
        .pos{0.0f, 0.0f, -1.0f, 0.0f},
        .color{1.0f, 0.9216f, 0.8039f},
        .intensity = 0.2f,
    });

    lights.lights.push_back(LightSet::Light{
        .pos{0.0f, 0.0f, 1.0f, 0.0f},
        .color{0.529f, 0.808f, 0.922f},
        .intensity = 0.2f,
    });

    ctx->OMSetRenderTargets(1, &rt.rtv.p, rt.dsv);
    constexpr float const tileSize = 250.0f;

    pog::ScaffoldGeometry cursorGeom;
    auto AddCursorBox = [&](glm::ivec2 tileCoord, glm::ivec3 size, glm::vec3 color) {
        glm::vec2 tileMin(tileCoord);
        glm::vec3 focusLow(tileMin * tileSize, 0.0f);
        glm::vec3 focusHigh = focusLow + glm::vec3(0, 0, size.z) * tileSize;

        glm::vec3 xSide{size.x * tileSize, 0, 0};
        glm::vec3 ySide{0, size.y * tileSize, 0};
        glm::vec3 zSide{0, 0, size.z * tileSize};

        cursorGeom.AddLineRect(focusLow, xSide, ySide, color);
        cursorGeom.AddLineRect(focusHigh, xSide, ySide, color);
        cursorGeom.AddLineRect(focusLow, xSide, zSide, color);
        cursorGeom.AddLineRect(focusLow + ySide, xSide, zSide, color);
    };

    if (slotAim_) {
        auto aimBox = glm::ivec2(glm::floor(glm::vec2(*slotAim_) / 250.0f));
        AddCursorBox(aimBox, {1, 1, -5}, {0.4f, 0.4f, 0.8f});
    }
    if (slotFocus_) {
        AddCursorBox(*slotFocus_, {1, 1, -5}, {0.6f, 0.6f, 0.8f});
    }

    if (roomInfo_) {
        constexpr float const heightStep = 250.0f / 32.0f;
        float const storeyStep = heightStep * storeyHeight_;

        pog::ScaffoldGeometry heightMapGeom;
        for (auto &[rootCoord, entryInfo] : roomInfo_->rootEntries) {
            auto coord = entryInfo.CoordMin();
            glm::mat4 tileBaseMat =
                glm::translate(glm::mat4(1.0f), glm::vec3{tileSize * glm::vec2(entryInfo.CoordMin()), 0.0f});
            if (renderGeometry_) {
                auto &slot = design_->slots_[rootCoord];
                if (auto &choice = slot.choice) {
                    auto tileDim = choice->tile->header.dim;
                    auto mut = choice->mutation;
                    if (rootCoord == slotFocus_ && hasMutationOverride_) {
                        mut = mutationOverride_;
                    }

                    auto RotationRad = [](TileKey::Rotation rot) -> float {
                        return (int)rot / 4.0f * glm::two_pi<float>();
                    };

                    auto tdt = choice->tile;

                    auto &tileData = graph_->tileData[choice->tile];
                    auto tileKey = TileKey(*tdt).Flipped(mut.flip).Rotated(mut.rot);

                    int storeyDelta = 0;
                    for (auto &slotH : slot.info->CornerHeights()) {
                        storeyDelta = slotH.height - tileKey.height[(int)slotH.corner];
                    }

                    auto tilePlaceMat = glm::translate(tileBaseMat, glm::vec3{0, 0, -storeyDelta * storeyStep});

                    // Find mid- and full extents of tile
                    auto tileFar = glm::vec3(tileDim, 0) * 250.0f;
                    auto tileMid = tileFar / 2.0f;
                    using Rot = TileKey::Rotation;
                    auto rotTileMid = (mut.rot == Rot::R90 || mut.rot == Rot::R270)
                                          ? glm::vec3{tileMid.y, tileMid.x, tileMid.z}
                                          : tileMid;

                    float flipScale = mut.flip ? -1.0f : 1.0f;
                    auto angle = RotationRad(mut.rot);
                    // angle += (float)(debugTime_ * glm::two_pi<double>() * 0.3); // spinning for debug purposes

                    // Compute intermediary matrices for flipping, rotation and elevation.
                    // Some of these can be simplified in the case of 90° rotations but it's more fun to do
                    // arbitrary rotation.
                    glm::vec3 flipShift = mut.flip ? glm::vec3{tileFar.x, 0, 0} : glm::vec3{};
                    glm::mat4 tileFlipMat =
                        glm::scale(glm::translate(glm::mat4{1.0f}, flipShift), glm::vec3{flipScale, 1, 1});
                    glm::mat4 tileRotMat = glm::translate(
                        glm::rotate(glm::translate(glm::mat4{1.0f}, rotTileMid), angle, glm::vec3{0, 0, 1}), -tileMid);
                    glm::mat4 tileWorldMat = tilePlaceMat * tileRotMat * tileFlipMat;

                    for (auto &[subCoord, sub] : tileData.subTiles) {
                        auto subPos = glm::vec3(subCoord, 0.0f) * tileSize;
                        glm::mat4 subPlaceMat = glm::translate(glm::mat4{1}, subPos);
                        glm::mat4 worldMat = tileWorldMat * subPlaceMat;
                        SubtileGpuData gpuData;
                        if (auto I = subtileGpuDataCache_.find(sub.tgm); I != end(subtileGpuDataCache_)) {
                            gpuData = I->second;
                        } else {
                            auto tgm = sub.tgm;
                            gpuData.mainMesh = meshDrawer_.LoadGeometry(tgm->mainGeometry.dolm, 0);
                            gpuData.groundMesh = meshDrawer_.LoadGeometry(tgm->groundGeometry.dolm, 0);
                            subtileGpuDataCache_[tgm] = gpuData;
                        }
                        if (drawMainMesh_) {
                            meshDrawer_.DrawGeometry(worldMat, viewMat, projMat, lights, sub.tgm->mainGeometry, 0,
                                                     gpuData.mainMesh.get());
                        }
                        if (drawGroundMesh_) {
                            meshDrawer_.DrawGeometry(worldMat, viewMat, projMat, lights, sub.tgm->groundGeometry, 0,
                                                     gpuData.groundMesh.get());
                        }
                    }
                }
            }
            if (renderSlotHeights_) {
                glm::vec3 const scale{tileSize * glm::vec2(entryInfo.size), -storeyStep};

                float avgHeight = 0.0f;
                std::array<float, 4> heights{};
                for (auto &h : entryInfo.CornerHeights()) {
                    avgHeight += h.height;
                    heights[(int)h.corner] = (float)h.height;
                }
                avgHeight /= 4.0f;
                glm::vec3 corners[]{
                    {0, 1, heights[0]}, //
                    {0, 0, heights[1]}, //
                    {1, 0, heights[2]}, //
                    {1, 1, heights[3]}, //
                    {0.5f, 0.5f, avgHeight},
                };
                for (auto &c : corners) {
                    c = tileBaseMat * glm::vec4(c * scale, 1.0f);
                }
                glm::vec3 colors[]{
                    {0.9f, 0, 0}, {0, 0.9f, 0}, {0, 0, 0.9f}, {0.9f, 0, 0.9f}, {0.1f, 0.1f, 0.1f},
                };
                heightMapGeom.AddLineStrip(std::span(corners).subspan<0, 4>(), std::span(colors).subspan<0, 4>(), true);

                glm::ivec3 tris[]{
                    {0, 1, 4},
                    {1, 2, 4},
                    {2, 3, 4},
                    {3, 0, 4},
                };
                for (auto &tri : tris) {
                    glm::vec3 triGeom[]{corners[tri.x], corners[tri.y], corners[tri.z]};
                    glm::vec3 triColor[]{colors[tri.x], colors[tri.y], colors[tri.z]};
                    heightMapGeom.AddTriangle(triGeom, triColor);
                }
            }
        }
        pog::ScaffoldDrawer scaffoldDrawer(dx_);
        scaffoldDrawer.Draw(heightMapGeom, projMat * viewMat, glm::mat4{1});
        scaffoldDrawer.Draw(cursorGeom, projMat * viewMat, glm::mat4{1});
    }
    ID3D11RenderTargetView *nullRtv{};
    ctx->OMSetRenderTargets(1, &nullRtv, nullptr);
}

void RoomViewController::ChangeRoom() {
    arm_ = desiredArm_;
    armName_ = desiredArmName_;
    // room_.reset();
    roomInfo_.reset();
    design_.reset();
    if (arm_) {
        // room_ = std::make_shared<pog::Room>(arm_);
        roomInfo_ = std::make_shared<pog::RoomInfo>(arm_);
        design_ =
            std::make_shared<pog::RoomDesign>(roomInfo_, gameData_, graph_, pog::RoomDesignFlag_StoreRejectionReasons);
    }
}

struct KeyShape {
    KeyShape() = default;
    explicit KeyShape(pog::TileKey key) {
        for (auto &et : key.etStr) {
            if (et) {
                etSig.insert(et.value());
            } else {
                ++etWild;
            }
        }
        for (auto &gt : key.gtStr) {
            if (gt) {
                gtSig.insert(gt.value());
            } else {
                ++gtWild;
            }
        }

        // TODO(LV): this is wrong, should be replaced with real/virtual index logic
#if 0
        for (auto val : key.etVal) {
            if (val) {
                etValSig.insert(val);
            } else {
                ++etValWild;
            }
        }
        for (auto val : key.gtVal) {
            if (val) {
                gtValSig.insert(val);
            } else {
                ++gtValWild;
            }
        }
#endif
    }

    int etWild{}, gtWild{};
    std::multiset<std::string> etSig, gtSig;

    int etValWild{}, gtValWild{};
    std::multiset<int> etValSig, gtValSig;
};

bool ShapeMatchOnStrings(KeyShape shape, KeyShape const &cand) {
    for (auto &et : cand.etSig) {
        if (auto I = shape.etSig.find(et); I != end(shape.etSig)) {
            shape.etSig.erase(I);
        } else if (shape.etWild > 0) {
            --shape.etWild;
        } else {
            return false;
        }
    }
    for (auto &gt : cand.gtSig) {
        if (auto I = shape.gtSig.find(gt); I != end(shape.gtSig)) {
            shape.gtSig.erase(I);
        } else if (shape.gtWild > 0) {
            --shape.gtWild;
        } else {
            return false;
        }
    }
    return shape.etSig.empty() && shape.gtSig.empty();
}

bool ShapeMatchOnIndices(KeyShape shape, KeyShape const &cand) {
    for (auto &et : cand.etValSig) {
        if (auto I = shape.etValSig.find(et); I != end(shape.etValSig)) {
            shape.etValSig.erase(I);
        } else if (shape.etValWild > 0) {
            --shape.etValWild;
        } else {
            return false;
        }
    }
    for (auto &gt : cand.gtValSig) {
        if (auto I = shape.gtValSig.find(gt); I != end(shape.gtValSig)) {
            shape.gtValSig.erase(I);
        } else if (shape.gtValWild > 0) {
            --shape.gtValWild;
        } else {
            return false;
        }
    }
    return shape.etValSig.empty() && shape.gtValSig.empty();
}

std::vector<pog::TileData> RoomViewController::SlotCandidates(std::optional<pog::TileKey> const &slotKey) {
    std::vector<pog::TileData> candidates;

    KeyShape trueShape(slotKey.value());
    auto slotSize = slotKey->size;
    for (auto &[name, tdt] : graph_->tdts) {
        if (tdt) {
            if (slotKey->tag && tdt->tag != slotKey->tag) {
                continue;
            }

            if (auto dim = tdt->header.dim; dim != slotSize && glm::ivec2{dim.y, dim.x} != slotSize) {
                continue;
            }

            auto tdtKey = pog::TileKey(*tdt);
            KeyShape candShape(tdtKey);
            if (!ShapeMatchOnIndices(trueShape, candShape)) {
                continue;
            }
            if (!ShapeMatchOnStrings(trueShape, candShape)) {
                continue;
            }

            pog::TileData cand{.path = name, .tdt = tdt};
            candidates.push_back(std::move(cand));
        }
    }
    return candidates;
}

void RoomViewController::PopulateGeometry(pog::TileData &tileData, std::shared_ptr<pog::GameData> gameData) {
    auto tdt = tileData.tdt;
    auto hdr = tdt->header;
    if (auto &sub = tdt->subTileV3) {
        auto tgt = graph_->tgts[tdt->commonTgt.value()];
        std::string tgmPath;
        if (tgt->tileMeshRoot) {
            tgmPath = tgt->tileMeshRoot.value() + ".tgm";
        } else {
            tgmPath = tgt->tileMesh.value();
        }
        auto tgm = graph_->tgms[tgmPath];
        tileData.subTiles[{0, 0}].tgm = tgm;
    } else {
        size_t const subCount = tdt->subTiles.size();
        for (size_t subIdx = 0; subIdx < subCount; ++subIdx) {
            size_t const subRow = subIdx / hdr.dim.x;
            size_t const subCol = subIdx % hdr.dim.x;
            auto &sub = tdt->subTiles[subIdx];
            bool sharedTgt = sub.tgt.has_value();
            auto tgt = graph_->tgts[sharedTgt ? sub.tgt.value() : tdt->commonTgt.value()];

            std::string tgmPath;
            if (tgt->tileMeshRoot) {
                tgmPath = tgt->tileMeshRoot.value();
                if (hdr.dim.x > 1 || hdr.dim.y > 1) {
                    tgmPath += fmt::format("_c{}r{}", subCol + 1, hdr.dim.y - subRow);
                }
                tgmPath += ".tgm";
            } else {
                tgmPath = tgt->tileMesh.value();
            }
            auto tgm = graph_->tgms[tgmPath];
            tileData.subTiles[{subCol, subRow}].tgm = tgm;
        }
    }
}
