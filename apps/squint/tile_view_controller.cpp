﻿#include "tile_view_controller.hpp"

#include <pog/game_data.hpp>

#include <glm/gtc/type_ptr.hpp>

TileViewController::TileViewController(std::shared_ptr<pog::DxContext> dx)
    : SceneViewController(dx), scaffoldDrawer_(dx), tileKeyDrawer_(dx_), meshDrawer_(dx), lightSet_(dx) {
    using pog::LightSet;
    lightSet_.lights.push_back(LightSet::Light{
        .pos{-500.0f, -500.0f, -500.0f, 1.0f},
        .color{1.0f, 1.0f, 1.0f},
        .intensity = 1.0f,
    });

    lightSet_.lights.push_back(LightSet::Light{
        .pos{0.0f, 0.0f, -1.0f, 0.0f},
        .color{1.0f, 0.9216f, 0.8039f},
        .intensity = 0.2f,
    });

    lightSet_.lights.push_back(LightSet::Light{
        .pos{0.0f, 0.0f, 1.0f, 0.0f},
        .color{0.529f, 0.808f, 0.922f},
        .intensity = 0.2f,
    });
}

void TileViewController::Update(pog::FrameUpdate const &update, std::shared_ptr<pog::GameData> gameData) {
    debugTime_ = (float)update.time;
    cam_.winSize = update.fbSize;
    RunCameraControlUi(cam_);

    auto graph = gameData->CurrentGraph();
    if (selectedTdt_) {
        auto extent = glm::vec3(selectedTdt_->header.dim, 0.0f) * 250.0f;
        auto mid = extent / 2.0f;
        cam_.look.at = mid;
    }

    if (ImGui::Begin("Tiles")) {
        if (!graph) {
            ImGui::TextUnformatted("Graph not loaded yet.");
        } else {
            auto SelectTile = [&](std::string path, auto tdt) {
                selectedTdtPath_ = path;
                selectedTdt_ = tdt;

                tileTgms_.clear();
                auto hdr = tdt->header;
                if (auto &sub = tdt->subTileV3) {
                    auto tgt = graph->tgts[tdt->commonTgt.value()];
                    std::string tgmPath =
                        tgt->tileMeshRoot ? tgt->tileMeshRoot.value() + ".tgm" : tgt->tileMesh.value();
                    auto tgm = graph->tgms[tgmPath];
                    tileTgms_[{0, 0}] = tgm;
                } else {
                    size_t const subCount = tdt->subTiles.size();
                    for (size_t subIdx = 0; subIdx < subCount; ++subIdx) {
                        size_t const subRow = subIdx / hdr.dim.x;
                        size_t const subCol = subIdx % hdr.dim.x;
                        auto &sub = tdt->subTiles[subIdx];
                        bool sharedTgt = sub.tgt.has_value();
                        auto tgt = graph->tgts[sharedTgt ? sub.tgt.value() : tdt->commonTgt.value()];
                        ImGui::Text("Sub %d,%d", subCol, subRow);

                        std::string tgmPath;
                        if (tgt->tileMeshRoot) {
                            tgmPath = tgt->tileMeshRoot.value();
                            if (hdr.dim.x > 1 || hdr.dim.y > 1) {
                                tgmPath += fmt::format("_c{}r{}", subCol + 1, hdr.dim.y - subRow);
                            }
                            tgmPath += ".tgm";
                        } else {
                            tgmPath = tgt->tileMesh.value();
                        }
                        auto tgm = graph->tgms[tgmPath];
                        tileTgms_[{subCol, subRow}] = tgm;
                    }
                }
            };
            if (ImGui::ArrowButton("Prev. tile", ImGuiDir_Left)) {
                if (!selectedTdtPath_ && !graph->tdts.empty()) {
                    auto J = graph->tdts.rbegin();
                    SelectTile(J->first, J->second);
                } else {
                    auto I = graph->tdts.find(*selectedTdtPath_);
                    if (I == graph->tdts.begin()) {
                        auto J = graph->tdts.rbegin();
                        SelectTile(J->first, J->second);
                    } else {
                        --I;
                        SelectTile(I->first, I->second);
                    }
                }
            }
            ImGui::SameLine();
            if (ImGui::ArrowButton("Next tile", ImGuiDir_Right)) {
                if (!selectedTdtPath_ && !graph->tdts.empty()) {
                    auto J = graph->tdts.begin();
                    SelectTile(J->first, J->second);
                } else {
                    auto I = graph->tdts.find(*selectedTdtPath_);
                    if (++I == graph->tdts.end()) {
                        I = graph->tdts.begin();
                    }
                    SelectTile(I->first, I->second);
                }
            }
            if (ImGui::BeginListBox("Tile", {-FLT_MIN, 0.0f})) {
                auto const tdtCount = graph->tdts.size();
                for (auto &[path, tdt] : graph->tdts) {
                    bool wasSelected = path == selectedTdtPath_;
                    if ((ImGui::Selectable(path.c_str(), wasSelected)) && !wasSelected) {
                        SelectTile(path, tdt);
                    }
                }
                ImGui::EndListBox();
            }
            if (auto tdt = selectedTdt_) {
                ImGui::TextUnformatted(fmt::format("\"{}\"", *selectedTdtPath_).c_str());
                pog::TileKey key(*tdt);
                for (int i = 0; i < 4; ++i) {
                    auto side = (pog::TileSide)i;
                    ImGui::Text("%s", pog::SideSymbol(side));
                    ImGui::SameLine();
                    ImGui::Indent();
                    if (auto &str = key.etStr[i]) {
                        ImGui::Text("ET: %d %s", key.etVal[i], str->c_str());
                    } else {
                        ImGui::Dummy({0, 0});
                    }
                    ImGui::Unindent();
                }
                for (int i = 0; i < 4; ++i) {
                    auto corner = (pog::TileCorner)i;
                    ImGui::Text("%s", pog::CornerSymbol(corner));
                    ImGui::SameLine();
                    ImGui::Indent();
                    if (auto &str = key.gtStr[i]) {
                        ImGui::Text("GT: %s", str->c_str());
                    } /* else {
                         ImGui::Dummy({0, 0});
                     }*/
                    ImGui::Text("Height: %d", key.height[i]);
                    ImGui::Unindent();
                }
                ImGui::Text("Tag: %s", key.tag ? key.tag->c_str() : "None");

                if (false) {
                    for (auto &[coord, tgm] : tileTgms_) {
                        ImGui::Text("Sub %d,%d", coord.x, coord.y);
                        if (tgm) {
                            ImGui::Indent();
                            ImGui::Text("TGM version: %d", tgm->version);
                            ImGui::Unindent();
                        }
                    }
                }
            }
        }
    }
    ImGui::End();

    if (ImGui::Begin("tdt tgts")) {
        if (graph) {
            for (auto &[tdtPath, tdt] : graph->tdts) {
                ImGui::TextUnformatted(tdtPath.c_str());
                ImGui::Indent();
                ImGui::Text("Version: %d", tdt->version);
                if (tdt->commonTgt) {
                    ImGui::Text("Common: %s", tdt->commonTgt->c_str());
                    ImGui::Indent();
                    if (auto tgtI = graph->tgts.find(tdt->commonTgt.value()); tgtI != graph->tgts.end()) {
                        if (auto &tgt = tgtI->second; tgt->tileMeshRoot) {
                            ImGui::Text("Root: %s", tgtI->second->tileMeshRoot->c_str());
                        } else {
                            ImGui::Text("TGM: %s", tgtI->second->tileMesh->c_str());
                        }
                    } else {
                        ImGui::TextUnformatted("Missing TGT data!");
                    }
                    ImGui::Unindent();
                }
                if (auto &sub = tdt->subTileV3) {
                } else {
                    size_t const subCount = tdt->subTiles.size();
                    for (size_t subIdx = 0; subIdx < subCount; ++subIdx) {
                        auto &sub = tdt->subTiles[subIdx];
                        if (sub.tgt) {
                            ImGui::Text("Sub: %s", sub.tgt->c_str());
                            ImGui::Indent();
                            if (auto tgtI = graph->tgts.find(sub.tgt.value()); tgtI != graph->tgts.end()) {
                                if (auto &tgt = tgtI->second; tgt->tileMeshRoot) {
                                    ImGui::Text("Root: %s", tgtI->second->tileMeshRoot->c_str());
                                } else {
                                    ImGui::Text("TGM: %s", tgtI->second->tileMesh->c_str());
                                }
                            } else {
                                ImGui::TextUnformatted("Missing TGT data!");
                            }
                            ImGui::Unindent();
                        }
                    }
                }
                ImGui::Unindent();
            }
        }
    }
    ImGui::End();

    ImGui::SetNextWindowSize({250.0f, 120.0f}, ImGuiCond_FirstUseEver);
    if (ImGui::Begin("Knobs")) {
        ImGui::DragFloat("Storey height", &storeyHeightKnob_, 0.1f, 0.0f, 250.0f);
    }
    ImGui::End();
}

void TileViewController::Draw(pog::LayerTarget const &rt) {
    using pog::TileKey;
    using pog::TileMutation;
    auto *ctx = dx_->Context();
    constexpr glm::vec4 const clearColor{0.1f, 0.2f, 0.3f, 1.0f};
    ctx->ClearRenderTargetView(rt.rtv, glm::value_ptr(clearColor));
    ctx->ClearDepthStencilView(rt.dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
    ctx->OMSetRenderTargets(1, &rt.rtv.p, rt.dsv);

    auto projMat = cam_.ProjectionMatrix();
    auto viewMat = cam_.ViewMatrix();

    glm::mat4 viewProjMat = projMat * viewMat;

    pog::ScaffoldGeometry scaffoldGeom;
    if (selectedTdt_) {
        glm::mat4 worldMat{1.0f};
    }

    struct TilePlace {
        glm::ivec2 cell{};
        TileMutation mut{};
        int storey{};
    };

    using TileMeshes = decltype(tileTgms_);

    std::vector<std::pair<TilePlace, TileMeshes>> placedTiles{
        {
            {
                .cell = {0, 0},
                .mut = TileMutation{.rot = TileKey::Rotation::R0, .flip = false},
                .storey = 0,
            },
            tileTgms_,
        },
        {
            {
                .cell = {3, 0},
                .mut = TileMutation{.rot = TileKey::Rotation::R0, .flip = true},
                .storey = 0,
            },
            tileTgms_,
        },
        {
            {
                .cell = {0, 3},
                .mut = TileMutation{.rot = TileKey::Rotation::R90, .flip = false},
                .storey = 0,
            },
            tileTgms_,
        },
        {
            {
                .cell = {3, 3},
                .mut = TileMutation{.rot = TileKey::Rotation::R90, .flip = false},
                .storey = 1,
            },
            tileTgms_,
        },
    };
    placedTiles.resize(1);

    auto RotationRad = [](TileKey::Rotation rot) -> float { return (int)rot / 4.0f * glm::two_pi<float>(); };

    if (selectedTdt_) {
        for (auto &[place, meshes] : placedTiles) {
            // Find mid- and full extents of tile
            auto tileFar = glm::vec3(selectedTdt_->header.dim, 0) * 250.0f;
            auto tileMid = tileFar / 2.0f;

            float flipScale = place.mut.flip ? -1.0f : 1.0f;
            auto tilePos = glm::vec3(place.cell, 0.0f) * 250.0f + glm::vec3(0, 0, place.storey) * -storeyHeightKnob_;
            auto angle = RotationRad(place.mut.rot);
            // angle += debugTime_ * glm::two_pi<float>() * 0.3f; // spinning for debug purposes

            // Compute intermediary matrices for flipping, rotation and elevation.
            // Some of these can be simplified in the case of 90° rotations but it's more fun to do arbitrary rotation.
            glm::vec3 flipShift = place.mut.flip ? glm::vec3{tileFar.x, 0, 0} : glm::vec3{};
            glm::mat4 tileFlipMat = glm::scale(glm::translate(glm::mat4{1.0f}, flipShift), glm::vec3{flipScale, 1, 1});
            glm::mat4 tileRotMat = glm::translate(
                glm::rotate(glm::translate(glm::mat4{1.0f}, +tileMid), angle, glm::vec3{0, 0, 1}), -tileMid);
            glm::mat4 tilePlaceMat = glm::translate(glm::mat4{1.0f}, tilePos);
            glm::mat4 tileWorldMat = tilePlaceMat * tileRotMat * tileFlipMat;

            // Draw tile key
            TileKey key(*selectedTdt_);
            tileKeyDrawer_.DrawTileKey(tileWorldMat, viewMat, projMat, key);

            // Draw subtiles
            for (auto &[subCoord, tgm] : meshes) {
                if (tgm) {
                    auto subPos = glm::vec3(subCoord, 0.0f) * 250.0f;

                    glm::mat4 subPlaceMat = glm::translate(glm::mat4{1.0f}, subPos);
                    glm::mat4 worldMat = tileWorldMat * subPlaceMat;

                    if (auto &mainGeom = tgm->mainGeometry; !mainGeom.dolm.meshes.empty()) {
                        meshDrawer_.DrawGeometry(worldMat, viewMat, projMat, lightSet_, mainGeom, 0);
                    }
                    if (auto &groundGeom = tgm->groundGeometry; !groundGeom.dolm.meshes.empty()) {
                        meshDrawer_.DrawGeometry(worldMat, viewMat, projMat, lightSet_, groundGeom, 0);
                    }
                }
            }
        }
    }

    scaffoldDrawer_.Draw(scaffoldGeom, viewProjMat, glm::mat4(1.0f));
}
