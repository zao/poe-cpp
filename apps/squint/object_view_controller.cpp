#include "object_view_controller.hpp"

#include <pog/drawer.hpp>
#include <pog/game_data.hpp>
#include <poe/format/mat.hpp>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <re2/re2.h>

#define IM_VEC2_CLASS_EXTRA                                                                                            \
    ImVec2(glm::vec2 v) : x(v.x), y(v.y) {}                                                                            \
    operator glm::vec2() const { return glm::vec2{x, y}; }

#include <imgui.h>
#include <imgui_stdlib.h>
#include <ranges>

#include <DirectXTK/DDSTextureLoader.h>

#include "pog/material.hpp"
#include "pog/skinning.hpp"

namespace ao_ns = poe::format::ao;

struct ConcreteMaterial {
    nlohmann::json js;
    struct Tex {
        CComPtr<ID3D11Resource> res;
        CComPtr<ID3D11ShaderResourceView> srv;
    };
    std::map<std::string, Tex> textures;

    bool opaque; // HACK(LV): Until we handle more stages than opaque and blended.
    std::shared_ptr<pog::Shader> shader;
};

struct MaterialLibrary {
    MaterialLibrary(std::shared_ptr<poe::io::vfs> vfs) : vfs_(vfs), ffxLib_(std::make_shared<pog::FFXLibrary>(vfs)) {}

    std::optional<ConcreteMaterial> LoadMaterial(std::string srcPath, std::shared_ptr<pog::DxContext> dx);

  private:
    std::shared_ptr<poe::io::vfs> vfs_;
    std::shared_ptr<pog::FFXLibrary> ffxLib_;
};

ObjectViewController::ObjectViewController(std::shared_ptr<pog::DxContext> dx)
    : pog::SceneViewController(dx), lights_(dx), scaffoldDrawer_(dx), meshDrawer_(dx) {
    cam_.look.at.z = -90.0f;
    cam_.look.distance = 200.0f;
    cam_.zNear = 0.1f;

    using pog::LightSet;
    lights_.lights.push_back(LightSet::Light{
        .pos{-500.0f, -500.0f, -500.0f, 1.0f},
        .color{1.0f, 1.0f, 1.0f},
        .intensity = 0.6f,
    });

    lights_.lights.push_back(LightSet::Light{
        .pos{+500.0f, -500.0f, -500.0f, 1.0f},
        .color{1.0f, 1.0f, 1.0f},
        .intensity = 0.4f,
    });

    lights_.lights.push_back(LightSet::Light{
        .pos{0.0f, 0.0f, -1.0f, 0.0f},
        .color{1.0f, 0.9216f, 0.8039f},
        .intensity = 0.8f,
    });

    lights_.lights.push_back(LightSet::Light{
        .pos{0.0f, 0.0f, 1.0f, 0.0f},
        .color{0.529f, 0.808f, 0.922f},
        .intensity = 0.8f,
    });
}

void ObjectViewController::Update(pog::FrameUpdate const &update, std::shared_ptr<pog::GameData> gameData) {
    cam_.winSize = update.fbSize;
    pog::RunCameraControlUi(cam_);

    if (gameData && !matLib_) {
        matLib_ = std::make_shared<MaterialLibrary>(gameData->vfs);
    }

    if (gameData && allObjects_.empty()) {
        auto vfs = gameData->vfs;
        std::vector<std::string> aocPaths;
        vfs->enumerate_files([&](poe::util::path const &path) {
            if (path.extension() == ".aoc") {
                std::optional<std::string> quickPath;
                if (preferredFirstMesh_) {
                    quickPath = preferredFirstMesh_;
                }
                if (quickPath && path.path_ != *quickPath) {
                    return;
                }
                aocPaths.push_back(path.path_);
            }
        });

        for (auto &path : aocPaths) {
            ObjectInstance inst;

            auto fh = vfs->open_file(poe::util::path(path));
            auto data = fh->read_all();
            inst.aoc = poe::format::ao::parse_ao(data);
            if (inst.aoc) {
                auto EntryKey = [](ao_ns::kv_component_entry const &e) { return e.key_; };
                auto KVField = [&](ao_ns::component_body const &body,
                                   std::string_view refKey) -> std::optional<ao_ns::kv_component_entry> {
                    static RE2 unkRE(R"re(\s*([^=\s]+)\s*=\s*(.*))re");
                    if (auto *kvs = std::get_if<ao_ns::kv_component_body>(&body)) {
                        if (auto I = std::ranges::find(kvs->entries_, refKey, EntryKey); I != kvs->entries_.end()) {
                            return *I;
                        }
                    } else if (auto *unk = std::get_if<ao_ns::unknown_component_body>(&body)) {
                        std::string key, value;
                        for (auto &line : unk->lines) {
                            if (RE2::FullMatch(line, unkRE, &key, &value)) {
                                if (key == refKey) {
                                    return ao_ns::kv_component_entry(key, value);
                                }
                            }
                        }
                    }
                    return std::nullopt;
                };
                auto RemoveQuotes = [](std::string_view s) -> std::string {
                    s = s.substr(s.find_first_not_of('"'));
                    return (std::string)s.substr(0, s.find_last_of('"'));
                };

                if (auto *animCtrl = inst.aoc->find_component("ClientAnimationController")) {
                    if (auto skeleton = KVField(animCtrl->second, "skeleton")) {
                        auto path = RemoveQuotes(skeleton->value_);
                        inst.astPath = path;
                    }
                }
                if (auto *skinMesh = inst.aoc->find_component("SkinMesh")) {
                    if (auto skin = KVField(skinMesh->second, "skin")) {
                        auto path = RemoveQuotes(skin->value_);
                        inst.smPath = path;
                        if (auto I = loadCache_.find(path); I != loadCache_.end()) {
                            inst.sm = std::static_pointer_cast<poe::format::sm::SM>(I->second);
                        } else {
                            fh = vfs->open_file(poe::util::path(path));
                            if (fh) {
                                data = fh->read_all();
                                inst.sm = poe::format::sm::parse_sm(data);
                                loadCache_.insert({path, inst.sm});
                            }
                        }
                        if (inst.sm) {
                            auto path = inst.sm->skinned_mesh_data_;
                            inst.smdPath = path;
                        }
                    }
                }
                if (auto *fixedMesh = inst.aoc->find_component("FixedMesh")) {
                    if (auto fixed_mesh = KVField(fixedMesh->second, "fixed_mesh")) {
                        auto path = RemoveQuotes(fixed_mesh->value_);
                        inst.fmtPath = path;
                    }
                }
            }
            allObjects_.insert({path, std::move(inst)});
        }
    }

    if (!chosenObject_ && !allObjects_.empty()) {
        std::string nextPath;
        ObjectInstance *nextObj{};
        if (preferredFirstMesh_ && allObjects_.contains(*preferredFirstMesh_)) {
            nextPath = *preferredFirstMesh_;
            nextObj = &allObjects_[nextPath];
            preferredFirstMesh_.reset();
        } else {
            for (auto &[path, obj] : allObjects_) {
                if (obj.aoc && (!obj.fmtPath.empty() || !obj.smdPath.empty())) {
                    nextPath = path;
                    nextObj = &obj;
                    break;
                }
            }
        }
        if (nextObj) {
            ChangeObject(gameData->vfs, nextPath, nextObj);
        }
    }

    if (ImGui::Begin("Objects", nullptr)) {
        if (allObjects_.empty()) {
            ImGui::TextUnformatted("No objects loaded yet.");
        } else {
            auto &catFilter = objectCategoryFilter_;
            ImGui::RadioButton("All", &catFilter, 0);
            ImGui::RadioButton("Fixed mesh", &catFilter, 1);
            ImGui::RadioButton("Skinned mesh", &catFilter, 2);
            ImGui::RadioButton("Skinned non-mesh", &catFilter, 3);
            ImGui::RadioButton("Indeterminate", &catFilter, 4);

            ImGui::InputText("Path filter", &objectPathFilter_);

            auto const DerefLess = [](auto a, auto b) -> bool { return *a < *b; };
            std::map<std::string const *, ObjectInstance *, decltype(DerefLess)> kept(DerefLess);
            auto pathFilterLower = poe::util::lowercase(objectPathFilter_);
            for (auto &[path, obj] : allObjects_) {
                if (!pathFilterLower.empty()) {
                    // Yes avid reader, comparing lowercase names is bad form but it'll do here.
                    auto lc = poe::util::lowercase(path);
                    if (lc.find(pathFilterLower) == lc.npos) {
                        continue;
                    }
                }
                static std::function<bool(ObjectInstance & obj)> const catFilters[]{
                    [](auto &obj) { return true; },                 // all objects
                    [](auto &obj) { return !obj.fmtPath.empty(); }, // objects with fixed meshes
                    [](auto &obj) { return !obj.smdPath.empty(); }, // objects with skinned meshes
                    [](auto &obj) {
                        return !obj.astPath.empty() && obj.smdPath.empty();
                    }, // objects with skeletons but no skinned mesh
                    [](auto &obj) {
                        return obj.astPath.empty() && obj.fmtPath.empty() && obj.smdPath.empty();
                    }, // objects with none of skeleton, fixed mesh and skinned mesh
                };
                if (catFilters[catFilter](obj)) {
                    kept.insert({&path, &obj});
                }
            }

            if (ImGui::BeginListBox("##Objects", {-FLT_MIN, -FLT_MIN})) {
                for (auto &[path, obj] : kept) {
                    bool selected = chosenObject_ ? chosenObject_->obj == obj : false;
                    if (ImGui::Selectable(path->c_str(), &selected)) {
                        ChangeObject(gameData->vfs, *path, obj);
                    }
                }
                ImGui::EndListBox();
            }
        }
    }
    ImGui::End();

    if (ImGui::Begin("Object", nullptr)) {
        if (chosenObject_) {
            auto &obj = chosenObject_->obj;
            ImGui::Text("Path: %s", chosenObject_->path.c_str());
            if (obj->ast) {
                ImGui::Text("AST: v%d %s", obj->ast->version, obj->astPath.c_str());
            }
            if (obj->fmt) {
                ImGui::Text("FMT: v%d (was v%d) %s", obj->fmt->version_, obj->fmt->originalVersion_,
                            obj->fmtPath.c_str());
            }
            if (obj->sm) {
                ImGui::Text("SM: v%d %s", obj->sm->version_, obj->smPath.c_str());
            }
            if (obj->smd) {
                ImGui::Text("SMD: v%d (tail v%d) %s", obj->smd->version, obj->smd->tailVersion, obj->smdPath.c_str());
            }
            if (chosenObject_->enabledShapes.size()) {
                ImGui::Text("Shapes:");
                ImGui::Indent();
                int matCountdown{};
                size_t matIdx{};
                for (size_t shapeIdx = 0; shapeIdx < chosenObject_->enabledShapes.size(); ++shapeIdx) {
                    bool enabled = chosenObject_->enabledShapes[shapeIdx];
                    auto &shapeName = chosenObject_->shapeNames[shapeIdx];
                    if (--matCountdown <= 0) {
                        auto &matAss = obj->materialAssignments[matIdx];
                        ImGui::Text("Material %s (%d shapes)", matAss.path.c_str(), matAss.count);
                        auto &mat = obj->materialStore[matAss.path];
                        if (mat) {
                            ImGui::Indent();
                            for (auto &[texName, _] : mat->textures) {
                                ImGui::Text("%s", texName.c_str());
                            }
                            ImGui::Unindent();
                        } else {
                            ImGui::Text("Material could not load.");
                        }
                        matCountdown = matAss.count;
                        ++matIdx;
                    }
                    std::string label = fmt::format("{}: {}##enabledShape", shapeIdx, shapeName);

                    if (ImGui::Checkbox(label.c_str(), &enabled)) {
                        chosenObject_->enabledShapes[shapeIdx] = enabled;
                    }
                }
                ImGui::Unindent();
            }
        }
    }
    ImGui::End();

    if (ImGui::Begin("Shader##ShaderChoiceWindow")) {
        for (size_t altIdx = 0; altIdx < shaderAlternatives_.size(); ++altIdx) {
            auto &alt = shaderAlternatives_[altIdx];
            ImGui::RadioButton(alt.name.c_str(), &shaderChoice_, (int)altIdx);
        }
    }
    ImGui::End();

    if (chosenObject_ && chosenObject_->obj->ast) {
        auto &co = *chosenObject_;
        auto ast = co.obj->ast;
        if (ImGui::Begin("Animation##AnimChoiceWindow")) {
            ImGui::Checkbox("Show skeleton", &showSkeleton_);
            if (ImGui::BeginListBox("##Animations", {-FLT_MIN, -FLT_MIN})) {
                for (int animIdx = 0; animIdx < ast->animationHeaders.size(); ++animIdx) {
                    auto &hdr = ast->animationHeaders[animIdx];
                    auto &anim = ast->animations[animIdx];
                    float duration = ast->Duration(animIdx).value_or(0.0f);
                    std::string label;
                    {
                        fmt::memory_buffer buf;
                        std::string loopKind = "unknown";
                        if (hdr.unk1_2 == 0x6f) {
                            loopKind = "one-shot";
                        } else if (hdr.unk1_2 == 0x6c) {
                            loopKind = "loop";
                        }
                        fmt::format_to(fmt::appender(buf), "{} ({} FPS, {:.0f} frames {:.2f}s, {}", hdr.name, hdr.frameRate,
                                       duration, duration / hdr.frameRate, loopKind);
                        if (hdr.blendFlag && hdr.baseName) {
                            fmt::format_to(fmt::appender(buf), ", base: {} ({})", *hdr.baseName, *hdr.blendFlag);
                        }
                        fmt::format_to(fmt::appender(buf), ")", hdr.name, hdr.frameRate, duration);
                        label = to_string(buf);
                    }

                    if (ImGui::Selectable(label.c_str(), co.animIdx == animIdx)) {
                        co.animIdx = animIdx;
                        co.animStartTime = (float)update.time;
                    }
                }
                ImGui::EndListBox();
            }
        }
        ImGui::End();

        if (ImGui::Begin("Animation breakdown##AnimBreakdown")) {
            auto &hdr = ast->animationHeaders[co.animIdx];
            auto &anim = ast->animations[co.animIdx];

            ImGui::Text("Name: %s", hdr.name.c_str());
            if (hdr.baseName) {
                ImGui::SameLine();
                ImGui::Text("Base-name: %s", hdr.baseName->c_str());
            }
            if (hdr.blendFlag) {
                ImGui::SameLine();
                ImGui::Text("Blend flag: %d", *hdr.blendFlag);
            }
            ImGui::Text("Frame rate: %d", hdr.frameRate);
            ImGui::SameLine();
            ImGui::Text("Unk #0: %02x, #1: %02x", hdr.unk1_0, hdr.unk1_2);
            auto duration = ast->Duration(co.animIdx);
            if (duration) {
                ImGui::SameLine();
                ImGui::Text("Duration: %.0f frames", *duration);

                ImGui::Separator();
                for (auto &track : anim.tracks) {
                    std::string label = fmt::format("Subject {}, space {}", track.boneIdx, track.unk1);
                    ImGui::Button(label.c_str());

                    namespace ast_ns = poe::format::ast;
                    auto Vec3ComponentWidget = [](std::string name, std::span<ast_ns::Track::Vec3Key const> comp) {
                        if (comp.size() < 2) {
                            return;
                        }
                        std::vector<glm::vec3> samples;
                        for (float time = 0.0f; time < comp.back().time; time += 1.0f) {
                            samples.push_back(*pog::SampleVec3(comp, time));
                        }
                        char axis[] = "XYZ";
                        for (int cIdx = 0; cIdx < 3; ++cIdx) {
                            std::string label = fmt::format("{} {}: {} keys", name, axis[cIdx], comp.size());
                            float const *vals = glm::value_ptr(samples[0]) + cIdx;
                            int count = (int)samples.size();
                            int stride = sizeof(samples[0]);
                            ImGui::PlotLines(label.c_str(), vals, count, 0, nullptr,
                                             FLT_MAX, FLT_MAX, {}, stride);
                        }
                    };
                    auto QuatComponentWidget = [](std::string name, auto const &comp) {
                        if (comp.size() < 2) {
                            return;
                        }
                        std::vector<glm::quat> samples;
                        for (float time = 0.0f; time < comp.back().time; time += 1.0f) {
                            samples.push_back(*pog::SampleQuat(comp, time));
                        }
                        char axis[] = "XYZW";
                        for (int cIdx = 0; cIdx < 4; ++cIdx) {
                            std::string label = fmt::format("{} {}: {} keys", name, axis[cIdx], comp.size());
                            float const *vals = glm::value_ptr(samples[0]) + cIdx;
                            int count = (int)samples.size();
                            int stride = sizeof(samples[0]);
                            ImGui::PlotLines(label.c_str(), vals, count, 0, nullptr, FLT_MAX, FLT_MAX, {}, stride);
                        }
                    };
                    Vec3ComponentWidget("T", track.translations);
                    QuatComponentWidget("R", track.rotations);
                    Vec3ComponentWidget("S", track.scales);
                    Vec3ComponentWidget("T2", track.translations2);
                    QuatComponentWidget("R2", track.rotations2);
                    Vec3ComponentWidget("S2", track.scales2);
                    ImGui::Separator();
                }
            }
        }
        ImGui::End();

        co.animRelTime = (float)update.time - co.animStartTime;
    }
}

void ObjectViewController::Draw(pog::LayerTarget const &rt) {
    constexpr float const tileSize = 250.0f;
    auto *ctx = dx_->Context();
    glm::vec4 clearColor{0.3f, 0.3f, 0.3f, 1.0f};
    ctx->ClearRenderTargetView(rt.rtv, glm::value_ptr(clearColor));
    ctx->ClearDepthStencilView(rt.dsv, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
    ctx->OMSetRenderTargets(1, &rt.rtv.p, rt.dsv);

    meshDrawer_.PollResources();

    glm::mat4 projMat = cam_.ProjectionMatrix(), viewMat = cam_.ViewMatrix(), worldMat(1);

    pog::ScaffoldGeometry boneVis;
    pog::ScaffoldGeometry scaffold;
    if (chosenObject_) {
        auto obj = chosenObject_->obj;
        for (auto &val : obj->materialStore | std::views::values) {
            val->shader->LoadIfNeeded();
        }

        // scaffold.AddQuad(glm::vec3{-0.5, -0.5, 0} * tileSize, glm::vec3{tileSize, 0, 0}, glm::vec3{0, tileSize, 0},
        //                  glm::vec4{0.3f, 0.3f, 0.3f, 1.0f});
        if (auto &fmt = obj->fmt) {
            // scaffold.AddLineAABB({0, 0, 0}, fmt->bbox_, {0.3, 0.3, 0.3});
        }
        if (auto &smd = obj->smd) {
            glm::vec2 bbox[]{smd->bbox.x, smd->bbox.y, smd->bbox.z};
            // scaffold.AddLineAABB({0, 0, 0}, bbox, {0.3, 0.3, 0.3});
        }

        std::optional<pog::Pose> pose;

        if (auto &ast = obj->ast) {
            pog::Poser poser(*obj->ast, chosenObject_->animIdx);

            pose = poser.ResolveAt(chosenObject_->animRelTime * poser.hdr.frameRate);
            for (int boneIdx = 0; boneIdx < poser.firstAttachment; ++boneIdx) {
                auto const &bone = ast->bones[boneIdx];
                glm::vec3 frameShift{};
                // glm::vec3 frameShift = glm::vec3(0.0f, f * -50.0f, 0.0f);
                // glm::vec3 pos1 = p.BonePos(boneIdx) + frameShift;
                glm::vec4 v1 = poser.binds[boneIdx] * glm::vec4(0, 0, 0, 1);
                glm::vec4 pos1 = pose->paletteCpu[boneIdx] * v1;
                int nextChild = bone.firstChild;
                while (nextChild != 0xFF) {
                    auto const &b2 = ast->bones[nextChild];
                    if (nextChild < poser.firstAttachment) {
                        // auto pos2 = p.BonePos(nextChild) + frameShift;
                        glm::vec4 v2 = poser.binds[nextChild] * glm::vec4(0, 0, 0, 1);
                        glm::vec4 pos2 = pose->paletteCpu[nextChild] * v2;
                        glm::vec3 points[]{pos1, pos2};
                        glm::vec3 colors[]{glm::vec3{0.3f, 0.3f, 0.3f}, glm::vec3{0.9f, 0.9f, 0.9f}};
                        if (nextChild >= poser.firstAttachment) {
                            colors[1] = glm::vec3{0.9f, 0.1f, 0.1f};
                        }
                        boneVis.AddLineStrip(points, colors);
                    }
                    nextChild = b2.nextSibling;
                }
            }
        }

        struct BucketKey {
            enum class Stage { Opaque, Blended };
            Stage stage;
            std::shared_ptr<ConcreteMaterial> material;

            auto operator<=>(BucketKey const &) const = default;
        };

        struct BucketValue {
            std::vector<bool> enabledShapes;
            std::vector<ID3D11ShaderResourceView *> textures;
        };

        auto &enabledShapes = chosenObject_->enabledShapes;
        std::map<BucketKey, BucketValue> buckets;
        for (auto &[mat, usage] : obj->materialUsages) {
            BucketValue val;
            val.enabledShapes = usage.coveredShapes;
            for (size_t shapeIdx = 0; shapeIdx < val.enabledShapes.size(); ++shapeIdx) {
                val.enabledShapes[shapeIdx] = val.enabledShapes[shapeIdx] && enabledShapes[shapeIdx];
            }

            // Skip to next if all shapes for this material are disabled
            if (std::ranges::all_of(val.enabledShapes, [](bool enabled) { return !enabled; })) {
                continue;
            }

            auto SrvOrNull = [&](std::string const &name) -> ID3D11ShaderResourceView * {
                if (!mat) {
                    return nullptr;
                }
                if (auto I = mat->textures.find(name); I != mat->textures.end()) {
                    return I->second.srv;
                }
                return nullptr;
            };
            std::vector<ID3D11ShaderResourceView *> srvs{4};
            std::vector<std::pair<size_t, std::vector<std::string>>> cands{
                {0, {"AlbedoTransparency_TEX", "base_color_texture"}},
                {1, {"NormalGlossAO_TEX", "base_normalspec_texture"}},
                {2, {"SpecularColour_TEX"}},
                {3, {"Glow_TEX"}},
            };
            for (auto &[slot, names] : cands) {
                for (auto &name : names) {
                    if (auto srv = SrvOrNull(name)) {
                        srvs[slot] = srv;
                        break;
                    }
                }
            }
            val.textures = srvs;
            auto stage = mat->opaque ? BucketKey::Stage::Opaque : BucketKey::Stage::Blended;
            buckets[{stage, mat}] = std::move(val);
        }

        // auto shader = shaderAlternatives_[shaderChoice_].shader;
        for (auto &[key, bucket] : buckets) {
            auto shader = key.material->shader;
            if (!key.material->opaque) {
                // HACK(LV): fallback for hiding tane's face
                shader = shaderAlternatives_[shaderChoice_].shader;
            }
            if (shader) {
                shader->SetConstant("camera_position", cam_.Position());
            }
            auto textures = std::span<ID3D11ShaderResourceView *const>(bucket.textures);
            if (obj->fmtGeom) {
                meshDrawer_.DrawGeometry(worldMat, viewMat, projMat, lights_, *obj->fmt, 0, obj->fmtGeom.get(),
                                         &bucket.enabledShapes, &textures, shader);
            }

            if (obj->smdGeom) {
                meshDrawer_.DrawGeometry(worldMat, viewMat, projMat, lights_, *obj->smd->geom, 0, obj->smdGeom.get(),
                                         &bucket.enabledShapes, &textures, shader, pose ? &*pose : nullptr);
            }
        }
    }

    scaffoldDrawer_.Draw(scaffold, projMat * viewMat, worldMat);
    if (showSkeleton_) {
        scaffoldDrawer_.Draw(boneVis, projMat * viewMat, worldMat);
    }
}

std::optional<ConcreteMaterial> MaterialLibrary::LoadMaterial(std::string srcPath, std::shared_ptr<pog::DxContext> dx) {
    ConcreteMaterial ret;
    std::shared_ptr<pog::Material> material;
    try {
        material = std::make_shared<pog::Material>(vfs_, ffxLib_, srcPath);
    } catch (std::exception &e) {
        LOG_F(WARNING, "{}", e.what());
        return std::nullopt;
    }

    auto fh = vfs_->open_file(poe::util::path(srcPath));
    auto data = fh->read_all();
    auto text = poe::util::map_unicode_to_utf8_string(data);

    auto mat = material->mat_;
    if (!mat) {
        return std::nullopt;
    }

    {
        std::string shaderName = srcPath;
        for (auto &ch : shaderName) {
            if (ch == '/') {
                ch = '@';
            }
        }
        shaderName.resize(shaderName.find_last_of('.'));
        std::string vsName = shaderName + "-vs_5_0.cso", psName = shaderName + "-ps_5_0.cso";
        ret.shader = std::make_shared<pog::Shader>(dx, poe::util::AssetPath(vsName), poe::util::AssetPath(psName));
    }

    auto &js = ret.js = nlohmann::json::parse(*text);
    if (!js.is_object()) {
        return std::nullopt;
    }

    if (auto &blendOverride = material->mat_->defaultGraph->overridenBlendMode; blendOverride == "Opaque") {
        ret.opaque = true;
    } else {
        ret.opaque = false;
    }

    for (auto &gi : mat->graphInstances) {
        auto &parent = gi.parent;
        // if (std::string_view(parent).ends_with("SpecGloss.fxgraph")) {
        {
            for (auto &customParam : gi.customParameters) {
                auto &name = customParam.name;
                auto &params = customParam.parameters;
                for (auto &param : customParam.parameters) {
                    if (param.empty()) {
                        continue;
                    }
                    auto &path = param["path"];
                    if (path.is_null()) {
                        continue;
                    }
                    if (!path.is_string()) {
                        return std::nullopt;
                    }
                    poe::util::path p(path.get<std::string>());
                    auto fh = vfs_->open_file(p);
                    if (!fh) {
                        continue;
                    }
                    auto data = fh->read_all();
                    auto &tex = ret.textures[name];
                    bool srgb = false;
                    if (auto &p = param["srgb"]; p.is_boolean()) {
                        srgb = p.get<bool>();
                    }
                    HRESULT hr = DirectX::CreateDDSTextureFromMemoryEx(
                        dx->Device(), reinterpret_cast<uint8_t const *>(data.data()), data.size(), 0,
                        D3D11_USAGE_IMMUTABLE, D3D11_BIND_SHADER_RESOURCE, 0, 0, srgb, &tex.res, &tex.srv);
                    break;
                }
            }
        }
    }
    return ret;
}

void ObjectViewController::ChangeObject(std::shared_ptr<poe::io::vfs> vfs, std::string const &path,
                                        ObjectInstance *obj) {
    ChosenObject next;
    next.path = path;
    next.obj = obj;
    next.obj->materialAssignments.clear();
    next.obj->materialStore.clear();
    next.obj->materialUsages.clear();

    auto &assignments = obj->materialAssignments;
    auto AddMaterial = [&](std::string path, int count) {
        if (!assignments.empty() && assignments.back().path == path) {
            assignments.back().count += count;
        } else {
            assignments.push_back(ObjectInstance::MaterialAssignment{
                .path = path,
                .count = count,
            });
        }
    };

    if (auto &path = obj->fmtPath; !path.empty()) {
        if (auto I = loadCache_.find(path); I != loadCache_.end()) {
            obj->fmt = std::static_pointer_cast<poe::format::fmt::FMT>(I->second);
        } else {
            if (auto fh = vfs->open_file(poe::util::path(path))) {
                auto data = fh->read_all();
                obj->fmt = poe::format::fmt::parse_fmt(data);
                loadCache_.insert({path, obj->fmt});
            }
        }
    }

    if (auto &path = obj->smdPath; !path.empty()) {
        if (auto I = loadCache_.find(path); I != loadCache_.end()) {
            obj->smd = std::static_pointer_cast<poe::format::smd::SmdFile>(I->second);
        } else {
            if (auto fh = vfs->open_file(poe::util::path(path))) {
                auto data = fh->read_all();
                obj->smd = poe::format::smd::parse_smd(data);
                loadCache_.insert({path, obj->smd});
            }
        }
    }

    if (auto &path = obj->astPath; !path.empty()) {
        if (auto I = loadCache_.find(path); I != loadCache_.end()) {
            obj->ast = std::static_pointer_cast<poe::format::ast::AstFile>(I->second);
        } else {
            if (auto fh = vfs->open_file(poe::util::path(path))) {
                auto data = fh->read_all();
                obj->ast = poe::format::ast::parse_ast(data);
                loadCache_.insert({path, obj->ast});
            }
        }
    }

    if (obj->fmt) {
        if (!obj->fmtGeom) {
            obj->fmtGeom = meshDrawer_.LoadGeometry(*obj->fmt, 0);
        }
        next.enabledShapes.resize(obj->fmt->shape_count, true);
        next.shapeNames.reserve(obj->fmt->shape_count);
        for (auto &string : obj->fmt->shape_strings_) {
            next.shapeNames.push_back(string.name);
            AddMaterial(string.material, 1);
        }
    }
    if (obj->smd && obj->smd->geom) {
        if (!obj->smdGeom) {
            obj->smdGeom = meshDrawer_.LoadGeometry(*obj->smd->geom, 0);
        }
        next.enabledShapes.resize(obj->smd->geom->shapeCount, true);
        next.shapeNames.reserve(obj->smd->shapes.size());
        for (auto &mat : obj->sm->materials_) {
            AddMaterial(mat.path_, (int)mat.value_.value_or(1));
        }
        for (auto &shape : obj->smd->shapes) {
            next.shapeNames.push_back(shape.name);
        }
    }

    for (size_t matIdx = 0; matIdx < assignments.size(); ++matIdx) {
        auto &matAss = assignments[matIdx];
        if (!next.obj->materialStore.contains(matAss.path)) {
            std::shared_ptr<ConcreteMaterial> mat;
            if (auto opt = matLib_->LoadMaterial(matAss.path, dx_)) {
                mat = std::make_shared<ConcreteMaterial>(std::move(*opt));
            }
            next.obj->materialStore[matAss.path] = mat;
            next.obj->materialUsages[mat].coveredShapes.resize(next.shapeNames.size(), false);
        }
    }

    // Sweep material assignments, populating shape bitmask for each material
    {
        size_t shapeIdx = 0;
        for (auto &ass : next.obj->materialAssignments) {
            auto mat = next.obj->materialStore[ass.path];
            auto &usage = next.obj->materialUsages[mat];
            std::fill_n(usage.coveredShapes.begin() + shapeIdx, ass.count, true);
            shapeIdx += ass.count;
        }
    }

    chosenObject_ = next;
}