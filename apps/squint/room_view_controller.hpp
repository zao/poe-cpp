#pragma once

#include <pog/drawer.hpp>
#include <pog/game_data.hpp>
#include <pog/scene_view_controller.hpp>

#include <absl/container/flat_hash_map.h>

#include <poe/format/arm.hpp>
// #include <poe/format/et.hpp>
// #include <poe/format/gt.hpp>
#include <pog/room.hpp>

namespace poe::format::et {
struct EtFile;
}

namespace poe::format::gt {
struct GtFile;
}

struct SubtileGpuData {
    std::shared_ptr<pog::MeshDrawer::CachedGeometry> mainMesh, groundMesh;
};

struct RoomViewController : pog::SceneViewController {
    RoomViewController(std::shared_ptr<pog::DxContext> dx);

    void Update(pog::FrameUpdate const &update, std::shared_ptr<pog::GameData> gameData) override;
    void Draw(pog::LayerTarget const &rt) override;

  private:
    void ChangeRoom();

    std::vector<pog::TileData> SlotCandidates(std::optional<pog::TileKey> const &slotKey);
    void PopulateGeometry(pog::TileData &tileData, std::shared_ptr<pog::GameData> gameData);

    std::shared_ptr<pog::GameData> gameData_;
    std::shared_ptr<pog::GraphAssets> graph_;
    std::map<std::shared_ptr<poe::format::tgm::TgmFile>, SubtileGpuData> subtileGpuDataCache_;

    std::shared_ptr<poe::format::arm::ARMFile> desiredArm_, arm_;

    std::string desiredArmName_, armName_;

    struct LevelMeta {
        absl::flat_hash_map<std::string, std::shared_ptr<poe::format::et::EtFile>> etCache_;
        absl::flat_hash_map<std::string, std::shared_ptr<poe::format::gt::GtFile>> gtCache_;
    };

    std::shared_ptr<LevelMeta> levelMeta_;
    std::shared_ptr<pog::RoomInfo> roomInfo_;
    std::shared_ptr<pog::RoomDesign> design_;
    std::optional<glm::vec3> slotAim_{};
    std::optional<glm::ivec2> slotFocus_;
    int storeyHeight_{};

    pog::FixedCamera cam_;
    pog::MeshDrawer meshDrawer_;

    bool renderGeometry_{true};
    bool renderSlotHeights_{true};
    bool hasMutationOverride_{true};
    pog::TileMutation mutationOverride_{.rot = pog::TileKey::Rotation::R0, .flip = false};
    double debugTime_;
    bool drawGroundMesh_{true};
    bool drawMainMesh_{true};
};