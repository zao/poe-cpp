﻿#define SILLY_OLD_LEGACY_STUFF 0

#include <Windows.h>
#include <GLFW/glfw3.h>
#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3native.h>

#include <fmt/chrono.h>
#include <fmt/ostream.h>

#include <imgui_impl_dx11.h>
#include <imgui_impl_glfw.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/projection.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <imgui.h>
#include <imgui_impl_dx11.h>
#include <imgui_impl_glfw.h>

#include <d3dcompiler.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION 1
#include <stb_image_write.h>

#include <openvr.h>

#include <tbb/parallel_for.h>
#include <tbb/concurrent_unordered_map.h>

// cht clt ddt env mtd toy
#include <poe/format/ao.hpp>
#include <poe/format/arm.hpp>
#include <poe/format/dgr.hpp>
#include <poe/format/ffx.hpp>
#include <poe/format/fmt.hpp>
#include <poe/format/rs.hpp>
#include <poe/format/sm.hpp>
#include <poe/format/smd.hpp>
#include <poe/format/spritesheet.hpp>
#include <poe/format/tdt.hpp>
#include <poe/format/tgm.hpp>
#include <poe/format/tgr.hpp>
#include <poe/format/tgt.hpp>
#include <poe/format/tsi.hpp>
#include <poe/format/tst.hpp>

#include <poe/io/vfs.hpp>
#include <poe/util/install_location.hpp>
#include <poe/util/parse_info.hpp>
#include <poe/util/random_access_file.hpp>

#include <pog/dx_context.hpp>
#include <pog/view_host.hpp>

#include <chrono>
#include <fstream>
#include <regex>

#include <pog/game_data.hpp>
#include <pog/scene_view_controller.hpp>
#include <pog/level_view_controller.hpp>
#include "graph_view_controller.hpp"
#include "object_view_controller.hpp"
#include "room_view_controller.hpp"
#include "subtile_view_controller.hpp"
#include "tile_view_controller.hpp"

using namespace std::chrono_literals;

#if SILLY_OLD_LEGACY_STUFF
static std::string const graphList[]{
    "Metadata/Terrain/CargoHold/Graphs/cargohold1.dgr",
};
#else
static std::string const graphList[]{
    "Metadata/Terrain/Missions/Hideouts/Archaeological/Graphs/hideout_01.tgr",
    "Metadata/Terrain/Missions/Hideouts/ArenaAtziri/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/ArenaDaresso/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/ArenaDominus/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/ArenaElder/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/ArenaGraveyardTrio/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/ArenaInnocence/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/ArenaKaom/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/ArenaKitavaAct10/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/ArenaMaven/Graphs/hideout.dgr",
    // "Metadata/Terrain/Missions/Hideouts/ArenaOlroth/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/ArenaPiety/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/ArenaShaper/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/ArenaSolarisLunaris/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/ArenaVeritania/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/AzuriteCave/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Baths/Graphs/hideout_baths.dgr",
    "Metadata/Terrain/Missions/Hideouts/Battleground/Graphs/battleground_hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Beach/Graphs/hideout_beach.dgr",
    "Metadata/Terrain/Missions/Hideouts/BlankAbyss/Graphs/hideout.dgr",
    // "Metadata/Terrain/Missions/Hideouts/BlankBlack/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/BlankCity/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/BlankClouds/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/BlankDesert/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/BlankDirt/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/BlankGrass/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/BlankSea/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/BlankSnow/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Catacombs/Graphs/catacomb_hideout3.dgr",
    "Metadata/Terrain/Missions/Hideouts/Causeway/Graphs/hideout_causeway.dgr",
    "Metadata/Terrain/Missions/Hideouts/Cave/Graphs/hideout_cave.dgr",
    "Metadata/Terrain/Missions/Hideouts/CaveShipwreck/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Coral/Graphs/coral_hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Courts/Graphs/hideout_courts.dgr",
    "Metadata/Terrain/Missions/Hideouts/CrimsonTemple/Graphs/hideout_crimsontemple.dgr",
    "Metadata/Terrain/Missions/Hideouts/Darkwood/Graphs/hideout_darkwood.dgr",
    "Metadata/Terrain/Missions/Hideouts/Den/Graphs/den_hideout3.dgr",
    "Metadata/Terrain/Missions/Hideouts/Docks/Graphs/hideout_docks_3.dgr",
    "Metadata/Terrain/Missions/Hideouts/Doomguard/Graphs/hideout_doomguard.dgr",
    "Metadata/Terrain/Missions/Hideouts/Fellshrine/Graphs/hideout_fellshrine.dgr",
    "Metadata/Terrain/Missions/Hideouts/Forest/Graphs/forest_hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Gardens/Graphs/hideout_garden.dgr",
    "Metadata/Terrain/Missions/Hideouts/Glacier/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Hasina/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/HauntedMansion/Graphs/haunted_hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/HighGardens/Graphs/hideout_highgardens.dgr",
    "Metadata/Terrain/Missions/Hideouts/Library/Graphs/library_hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Lunaris/Graphs/hideout_lunaris_3.dgr",
    "Metadata/Terrain/Missions/Hideouts/LunarisWater/Graphs/hideout_moontemple.dgr",
    "Metadata/Terrain/Missions/Hideouts/Marketplace/Graphs/hideout_marketplace_3.dgr",
    "Metadata/Terrain/Missions/Hideouts/Mine/Graphs/mine_hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Mountain/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Oasis/Graphs/oasis_hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Ossuary/Graphs/hideout_ossuary.dgr",
    "Metadata/Terrain/Missions/Hideouts/PillarsOfArun/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Primeval/Graphs/hideout_upstairs.dgr",
    "Metadata/Terrain/Missions/Hideouts/Primeval/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/PrisonTower/Graphs/hideout_prisontower.dgr",
    "Metadata/Terrain/Missions/Hideouts/PvPOnly/Graphs/pvponly_town.dgr",
    "Metadata/Terrain/Missions/Hideouts/RitualLeague/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/RuinedTemple/Graphs/ruinedtemple_hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Scepter/Graphs/hideout_scepter_3.dgr",
    "Metadata/Terrain/Missions/Hideouts/Sewers/Graphs/sewer_hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Shaper/Graphs/_chimera.dgr",
    "Metadata/Terrain/Missions/Hideouts/Shaper/Graphs/_hydra.dgr",
    "Metadata/Terrain/Missions/Hideouts/Shaper/Graphs/_minotaur.dgr",
    "Metadata/Terrain/Missions/Hideouts/Shaper/Graphs/_phoenix.dgr",
    "Metadata/Terrain/Missions/Hideouts/Shaper/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Ship/Graphs/ship_hideout1.dgr",
    "Metadata/Terrain/Missions/Hideouts/Sins/Graphs/sins_hideout3.dgr",
    "Metadata/Terrain/Missions/Hideouts/Slums/Graphs/hideout_slum.dgr",
    "Metadata/Terrain/Missions/Hideouts/Solaris/Graphs/hideout_solaris.dgr",
    "Metadata/Terrain/Missions/Hideouts/Space/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Sunspire/Graphs/hideout_sunspire.dgr",
    "Metadata/Terrain/Missions/Hideouts/SynthesisHub/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/TemplarLab/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Thaumaturgy/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/TwilightTemple/Graphs/_hideout_lunariswater.dgr",
    "Metadata/Terrain/Missions/Hideouts/TwilightTemple/Graphs/hideout.dgr",
    "Metadata/Terrain/Missions/Hideouts/Warehouse/Graphs/hideout_warehouse_3.dgr",
    "Metadata/Terrain/Act11/Camp/Graphs/island_camp.dgr",
    "Metadata/Terrain/Leagues/Expedition/UndergroundFeatures/Bait/expedition_baitcave_1.dgr",
    "Metadata/Terrain/Leagues/Expedition/UndergroundFeatures/ArenaUhtred/arena_uhtred.dgr",
    "Metadata/Terrain/Act3/Area18Level2/Graphs/Level3/_city_tower1_1.dgr",
    "Metadata/Terrain/EndGame/MapBrambleFall/Graphs/bramblefall_1.tgr",
    "Metadata/Terrain/Leagues/Ultimatum/UltimatumArena/Graphs/ultimatum_arena_endgame.dgr",
    // "Metadata/Terrain/Missions/Hideouts/ArenaOlroth/hideout.dgr",
};
#endif

int wmain(int argc, wchar_t **argv) {
    std::string desiredGraph, pendingGraph;
    if (argc == 2) {
        desiredGraph = poe::util::to_string(reinterpret_cast<char16_t *>(argv[1]));
    } else {
        desiredGraph = graphList[0];
    }
    poe::util::path argPoePath;

    glfwInit();
    constexpr const std::tuple<int, int> windowHints[]{
        {
            GLFW_CLIENT_API,
            GLFW_NO_API,
        },
        {
            GLFW_SCALE_TO_MONITOR,
            GLFW_TRUE,
        },
        {
            GLFW_VISIBLE,
            GLFW_FALSE,
        },
    };
    for (auto &[k, v] : windowHints) {
        glfwWindowHint(k, v);
    }

    glm::ivec2 initialSize{1920, 1080};
    std::shared_ptr<GLFWwindow> win(glfwCreateWindow(initialSize.x, initialSize.y, "Squint", nullptr, nullptr),
                                    &glfwDestroyWindow);
    glm::ivec2 fbSize{};
    glfwGetFramebufferSize(win.get(), &fbSize.x, &fbSize.y);
    {
        glm::ivec2 monPos, monSize;
        glfwGetMonitorWorkarea(glfwGetPrimaryMonitor(), &monPos.x, &monPos.y, &monSize.x, &monSize.y);
        glm::ivec2 monMid = monPos + monSize / 2;
        glm::ivec2 winPos = monMid - fbSize / 2;
        glfwSetWindowPos(win.get(), winPos.x, winPos.y);
        glfwShowWindow(win.get());
    }
    auto dxCtx = std::make_shared<pog::DxContext>();

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();

    ImGui::StyleColorsLight();
    {
        glm::vec2 scale{};
        glfwGetWindowContentScale(win.get(), &scale.x, &scale.y);
        ImGuiStyle &style = ImGui::GetStyle();
        style.ScaleAllSizes(scale.x);

        ImFontConfig config{};
        auto assetRoot = *poe::util::executable_dir();

        ImFontGlyphRangesBuilder b;
        b.AddRanges(io.Fonts->GetGlyphRangesDefault());
        b.AddText((char const *)u8"×⮜⮟⮞⮝⬉⬋⬊⬈⭦⭩⭨⭧");
        b.AddText((char const *)u8"┌┐└┘▛▙▟▜");
        ImVector<ImWchar> glyphRanges;
        b.BuildRanges(&glyphRanges);

        io.Fonts->AddFontFromFileTTF((assetRoot / "Roboto-Regular.ttf").string().c_str(), 14.0f * scale.x, &config,
                                     glyphRanges.Data);
        config.MergeMode = true;
        io.Fonts->AddFontFromFileTTF((assetRoot / "NotoSansJP-VF.ttf").string().c_str(), 14.0f * scale.x, &config,
                                     glyphRanges.Data);
        io.Fonts->AddFontFromFileTTF((assetRoot / "NotoSansSymbols-Regular.ttf").string().c_str(), 14.0f * scale.x,
                                     &config, glyphRanges.Data);
        io.Fonts->AddFontFromFileTTF((assetRoot / "NotoSansSymbols2-Regular.ttf").string().c_str(), 14.0f * scale.x,
                                     &config, glyphRanges.Data);
        io.Fonts->Build();
    }

    struct WinCallbackContext {
        std::function<void(double, double)> onCursorPos;
        std::function<void(int, int, int)> onMouseButton;
    } callbackContext;

    glfwSetWindowUserPointer(win.get(), &callbackContext);
    glfwSetCursorPosCallback(win.get(), [](GLFWwindow *win, double xPos, double yPos) {
        auto *cbCtx = (WinCallbackContext *)glfwGetWindowUserPointer(win);
        cbCtx->onCursorPos(xPos, yPos);
    });
    glfwSetMouseButtonCallback(win.get(), [](GLFWwindow *win, int button, int action, int mods) {
        auto *cbCtx = (WinCallbackContext *)glfwGetWindowUserPointer(win);
        cbCtx->onMouseButton(button, action, mods);
    });

    ImGui_ImplGlfw_InitForOther(win.get(), true);
    ImGui_ImplDX11_Init(dxCtx->Device(), dxCtx->Context());

    auto vfs = std::make_shared<poe::io::vfs>(poe::io::file_collective_info {
    //.collective_root_ = "F:/Temp/poe/collective",
#if SILLY_OLD_LEGACY_STUFF
        .build = 1992927,
#else
        .build = 8930624,
#endif
    });
    if (!vfs) {
        throw std::runtime_error("could not open vfs");
    }

    std::shared_future<std::shared_ptr<pog::GameData>> futureGameData;
    std::shared_ptr<pog::GameData> gameData;
    std::string graphSource;

    pog::ViewHost host(*dxCtx, glfwGetWin32Window(win.get()));
    pog::LayerTarget uiRt(*dxCtx.get()), viewRt(*dxCtx.get());
    glm::vec4 viewClearColor = glm::vec4(0.1f, 0.2f, 0.0f, 1.0f);
    std::vector<pog::Resizable *> sizedViews{&host, &uiRt, &viewRt};

    bool showDemoWindow = true;
    std::optional<glm::ivec2> selectedTile = glm::ivec2{1, 1};

    pog::FrameUpdate frameUpdate{};

    callbackContext.onCursorPos = [&io, &frameUpdate](double xPos, double yPos) {
        if (io.WantCaptureMouse) {
            return;
        }
        using Event = pog::FrameUpdate::Input::InputEvent;
        auto &input = frameUpdate.input;

        input.cursorPos = glm::vec2{xPos, yPos};
        input.events.push_back(Event{.kind = Event::MouseMove,
                                     .data = {.mouseMove = {
                                                  .pos = glm::vec2(xPos, yPos),
                                              }}});
    };

    callbackContext.onMouseButton = [&io, &frameUpdate, &win](int button, int action, int mods) {
        using pog::MouseButton;
        using pog::ModKey;

        if (io.WantCaptureMouse) {
            return;
        }
        // We've got to translate from GLFW buttons, actions and modifier keys to our own because this system is
        // going to be used in other environments like a Qt widget in the future.
        static std::map<int, MouseButton> const buttonMap{
            {GLFW_MOUSE_BUTTON_1, MouseButton::Left},    {GLFW_MOUSE_BUTTON_2, MouseButton::Right},
            {GLFW_MOUSE_BUTTON_3, MouseButton::Middle},  {GLFW_MOUSE_BUTTON_4, MouseButton::Back},
            {GLFW_MOUSE_BUTTON_5, MouseButton::Forward},
        };

        auto mappedMods = ModKey::ModNone;
        using ModKey = ModKey;
        static constexpr std::pair<int, ModKey> const modMap[]{
            {GLFW_MOD_ALT, ModKey::ModAlt},
            {GLFW_MOD_CONTROL, ModKey::ModCtrl},
            {GLFW_MOD_SHIFT, ModKey::ModShift},
        };
        for (auto &[from, to] : modMap) {
            if (mods & from) {
                mappedMods = (ModKey)(mappedMods | to);
            }
        }

        if (auto buttonI = buttonMap.find(button); buttonI != buttonMap.end()) {
            using Event = pog::FrameUpdate::Input::InputEvent;
            glm::dvec2 pos;
            glfwGetCursorPos(win.get(), &pos.x, &pos.y);
            auto &input = frameUpdate.input;
            if (action == GLFW_PRESS) {
                input.events.push_back(Event{.kind = Event::MousePress,
                                             .data = {.mousePress = {
                                                          .pos = glm::vec2(pos),
                                                          .button = buttonI->second,
                                                      }}});
            } else if (action == GLFW_RELEASE) {
                input.events.push_back(Event{.kind = Event::MouseRelease,
                                             .data = {.mouseRelease = {
                                                          .pos = glm::vec2(pos),
                                                          .button = buttonI->second,
                                                      }}});
            }
        }
    };

    // gen better vis

    enum class ViewMode : int {
        ViewGraph = 0,
        ViewRoom = 1,
        ViewTile = 2,
        ViewSubTile = 3,
        ViewLevel = 4,
        ViewObject = 5,
    };

    ViewMode viewMode = ViewMode::ViewObject;
    std::shared_ptr<pog::SceneViewController> viewCtrl{};

    double frameLast = glfwGetTime();
    double frameDt = 0.0f;

    futureGameData = std::async(std::launch::async, &pog::LoadGameData, vfs);

    vr::IVRSystem *vrSys{};
    vr::IVRCompositor *vrComp{};

    bool screenshotRequested = false;

    while (true) {
        frameUpdate.input.events.clear();
        glfwPollEvents();

        double frameNow = glfwGetTime();

        frameUpdate.dt = frameNow - frameUpdate.time;
        frameUpdate.time = frameNow;

        if (!gameData && futureGameData.wait_for(0s) == std::future_status::ready) {
            viewClearColor = glm::vec4(0.1f, 0.2f, 0.3f, 1.0f);
            gameData = futureGameData.get();
            for (auto graphPath : graphList) {
                // gameData->GetGraphAsync(graphPath);
            }
        }

        if (gameData && desiredGraph != pendingGraph) {
            gameData->RequestGraphChange(desiredGraph);
            pendingGraph = desiredGraph;
        }

        if (glfwWindowShouldClose(win.get())) {
            break;
        }

        glfwGetFramebufferSize(win.get(), &fbSize.x, &fbSize.y);
        frameUpdate.fbSize = fbSize;

        for (auto &r : sizedViews) {
            r->Resize(fbSize);
        }

        ImGui_ImplDX11_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        ImGui::ShowDemoWindow(&showDemoWindow);

        if (ImGui::Begin("Debug")) {
            if (ImGui::Button("Screenshot")) {
                screenshotRequested = true;
            }
        }
        ImGui::End();

        if (ImGui::BeginMainMenuBar()) {
            if (ImGui::BeginMenu("Graph")) {
                for (auto &str : graphList) {
                    if (ImGui::MenuItem(str.c_str(), nullptr, str == desiredGraph)) {
                        desiredGraph = str;
                    }
                }
                ImGui::EndMenu();
            }
            static bool hasVr = vr::VR_IsHmdPresent();
            if (ImGui::BeginMenu("VR", hasVr)) {
                if (ImGui::MenuItem("Jack in") && !vrSys) {}
                ImGui::EndMenu();
            }
            ImGui::EndMainMenuBar();
        }

        auto *ctx = dxCtx->Context();
        D3D11_VIEWPORT viewport{.TopLeftX = 0,
                                .TopLeftY = 0,
                                .Width = (float)fbSize.x,
                                .Height = (float)fbSize.y,
                                .MinDepth = 0.0f,
                                .MaxDepth = 1.0f};
        ctx->RSSetViewports(1, &viewport);
        host.Clear();

        int viewModeChanged = 0;
        if (ImGui::Begin("Mode")) {
            int *v = (int *)&viewMode;
            // bit-or used here for non-shortcircuiting
            viewModeChanged = ImGui::RadioButton("Graph", v, (int)ViewMode::ViewGraph) |
                              ImGui::RadioButton("Room", v, (int)ViewMode::ViewRoom) |
                              ImGui::RadioButton("Tile", v, (int)ViewMode::ViewTile) |
                              ImGui::RadioButton("Subtile", v, (int)ViewMode::ViewSubTile) |
                              ImGui::RadioButton("Level", v, (int)ViewMode::ViewLevel) |
                              ImGui::RadioButton("Object", v, (int)ViewMode::ViewObject);
        }
        ImGui::End();

        if (!viewCtrl || viewModeChanged) {
            switch (viewMode) {
            case ViewMode::ViewGraph:
                viewCtrl = std::make_shared<GraphViewController>(dxCtx);
                break;
            case ViewMode::ViewRoom:
                viewCtrl = std::make_shared<RoomViewController>(dxCtx);
                break;
            case ViewMode::ViewTile:
                viewCtrl = std::make_shared<TileViewController>(dxCtx);
                break;
            case ViewMode::ViewSubTile:
                viewCtrl = std::make_shared<SubTileViewController>(dxCtx);
                break;
            case ViewMode::ViewLevel:
                viewCtrl = std::make_shared<pog::LevelViewController>(dxCtx);
                break;
            case ViewMode::ViewObject:
                viewCtrl = std::make_shared<ObjectViewController>(dxCtx);
                break;
            }
        }

        viewCtrl->Update(frameUpdate, gameData);
        viewCtrl->Draw(viewRt);

        ImGui::Render();

        ctx->ClearRenderTargetView(uiRt.rtv, glm::value_ptr(glm::vec4{0.0f, 0.0f, 0.0f, 0.0f}));
        ctx->ClearDepthStencilView(uiRt.dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
        ctx->OMSetRenderTargets(1, &uiRt.rtv.p, uiRt.dsv);
        ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

        // Blend UI and scene render targets onto backbuffer
        host.Clear();
        host.BlendTarget(viewRt.rtSrv);
        host.BlendTarget(uiRt.rtSrv);
        host.Present();

        if (screenshotRequested) {
            auto *dev = dxCtx->Device();
            CComPtr<ID3D11Texture2D> staging;
            D3D11_TEXTURE2D_DESC desc{};
            viewRt.rt->GetDesc(&desc);
            desc.Usage = D3D11_USAGE_STAGING;
            desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
            desc.BindFlags = 0;
            HRESULT hr = dev->CreateTexture2D(&desc, nullptr, &staging);
            // ctx->CopyResource(staging, viewRt.rt);
            ctx->CopySubresourceRegion(staging, 0, 0, 0, 0, viewRt.rt, 0, nullptr);
            D3D11_MAPPED_SUBRESOURCE msr{};
            hr = ctx->Map(staging, 0, D3D11_MAP_READ, 0, &msr);
            for (size_t row = 0; row < desc.Height; ++row) {
                char *p = (char *)msr.pData + msr.RowPitch * row;
                for (size_t col = 0; col < desc.Width; ++col) {
                    std::swap(p[0], p[2]);
                    p += 4;
                }
            }
            int pngSize{};
            auto *pngData = stbi_write_png_to_mem((unsigned char const*)msr.pData, msr.RowPitch, desc.Width, desc.Height, 4, &pngSize);
            auto now = std::chrono::system_clock::now();
            std::filesystem::path filename = fmt::format("screenshot-{:%F_%H_%M_%S}.png", fmt::localtime(now));
            std::ofstream os(filename, std::ios::binary);
            os.write((char const*)pngData, pngSize);
            STBIW_FREE(pngData);
            ctx->Unmap(staging, 0);
            screenshotRequested = false;
        }
    }

    ImGui_ImplDX11_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    return 0;
}