#include "graph_view_controller.hpp"

GraphViewController::GraphViewController(std::shared_ptr<pog::DxContext> dx) : SceneViewController(dx) {}

void GraphViewController::Update(pog::FrameUpdate const &update, std::shared_ptr<pog::GameData> gameData) {}

void GraphViewController::Draw(pog::LayerTarget const &rt) {}