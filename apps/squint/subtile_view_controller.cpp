#include "subtile_view_controller.hpp"

SubTileViewController::SubTileViewController(std::shared_ptr<pog::DxContext> dx) : SceneViewController(dx) {}

void SubTileViewController::Update(pog::FrameUpdate const &update, std::shared_ptr<pog::GameData> gameData) {}

void SubTileViewController::Draw(pog::LayerTarget const &rt) {}
