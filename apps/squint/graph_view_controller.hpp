#pragma once

#include <pog/scene_view_controller.hpp>

struct GraphViewController : pog::SceneViewController {
    explicit GraphViewController(std::shared_ptr<pog::DxContext> dx);

    virtual void Update(pog::FrameUpdate const &update, std::shared_ptr<pog::GameData> gameData) override;
    virtual void Draw(pog::LayerTarget const &rt) override;
};