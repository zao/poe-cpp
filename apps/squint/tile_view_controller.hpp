#pragma once

#include <pog/drawer.hpp>
#include <pog/scene_view_controller.hpp>

#include <glm/glm.hpp>
#include <imgui.h>

struct TileViewController : pog::SceneViewController {
    explicit TileViewController(std::shared_ptr<pog::DxContext> dx);

    void Update(pog::FrameUpdate const &update, std::shared_ptr<pog::GameData> gameData) override;

    void Draw(pog::LayerTarget const &rt) override;

  private:
    pog::FixedCamera cam_;
    pog::ScaffoldDrawer scaffoldDrawer_;
    pog::TileKeyDrawer tileKeyDrawer_;
    pog::MeshDrawer meshDrawer_;
    pog::LightSet lightSet_;
    std::optional<std::string> selectedTdtPath_;
    std::shared_ptr<poe::format::tdt::Tdt> selectedTdt_;

    float storeyHeightKnob_{125.0f};

    float debugTime_;

    struct RowMajorOrder {
        bool operator()(glm::ivec2 a, glm::ivec2 b) const {
            if (a.x != b.x) {
                return a.x < b.x;
            }
            return a.y < b.y;
        }
    };

    std::map<glm::ivec2, std::shared_ptr<poe::format::tgm::TgmFile>, RowMajorOrder> tileTgms_;
};