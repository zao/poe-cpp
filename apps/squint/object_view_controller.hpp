#pragma once

#include <pog/scene_view_controller.hpp>
#include <pog/drawer.hpp>

#include <absl/container/node_hash_map.h>

#include <nlohmann/json.hpp>

#include <poe/format/ao.hpp>
#include <poe/format/ast.hpp>
#include <poe/format/fmt.hpp>
#include <poe/format/sm.hpp>
#include <poe/format/smd.hpp>

namespace poe::io {
struct vfs;
}

struct ConcreteMaterial;
struct MaterialLibrary;

struct ObjectViewController : pog::SceneViewController {
    ObjectViewController(std::shared_ptr<pog::DxContext> dx);
    void Update(pog::FrameUpdate const &update, std::shared_ptr<pog::GameData> gameData) override;
    void Draw(pog::LayerTarget const &rt) override;

  private:
    struct ObjectInstance {
        std::shared_ptr<poe::format::ao::ao_parse> aoc;

        // Some of these will be populated based on what the aoc contains.

        // Fixed mesh group
        std::string fmtPath;
        std::shared_ptr<poe::format::fmt::FMT> fmt; // ao.FixedMesh.fixed_mesh
        std::shared_ptr<pog::MeshDrawer::CachedGeometry> fmtGeom;

        // Skinned mesh group
        std::string smPath;
        std::shared_ptr<poe::format::sm::SM> sm; // ao.SkinMesh.skin
        std::shared_ptr<pog::MeshDrawer::CachedGeometry> smGeom;

        std::string astPath;
        std::shared_ptr<poe::format::ast::AstFile> ast; // ao.ClientAnimationController.skeleton

        std::string smdPath;
        std::shared_ptr<poe::format::smd::SmdFile> smd; // sm.SkinnedMeshData
        std::shared_ptr<pog::MeshDrawer::CachedGeometry> smdGeom;

        struct MaterialAssignment {
            std::string path;
            int count;
        };

        // Assignments for UI to demonstrate how materials are assigned in shape order
        std::vector<MaterialAssignment> materialAssignments;

        // All the used materials, deduplicated by path
        std::map<std::string, std::shared_ptr<ConcreteMaterial>> materialStore;

        // Bitmask for the shapes to draw with each material
        struct MaterialUsage {
            std::vector<bool> coveredShapes;
        };
        std::map<std::shared_ptr<ConcreteMaterial>, MaterialUsage> materialUsages;
    };

    void ChangeObject(std::shared_ptr<poe::io::vfs> vfs, std::string const &path, ObjectInstance *obj);

    std::map<std::string, ObjectInstance> allObjects_;
    struct ChosenObject {
        std::string path;
        ObjectInstance *obj{};
        std::vector<bool> enabledShapes;
        std::vector<std::string> shapeNames;

        int animIdx = 0;
        float animStartTime = 0.0f;
        float animRelTime = 0.0f;
    };
    int objectCategoryFilter_{};
    std::string objectPathFilter_;
    std::optional<std::string> preferredFirstMesh_;
    // std::optional<std::string> preferredFirstMesh_ = "Metadata/Characters/StrDex/StrDexNoWind.aoc";
    // std::optional<std::string> preferredFirstMesh_ = "Metadata/Characters/Dex/DexNoWind.aoc";
    // std::optional<std::string> preferredFirstMesh_ = "Metadata/Monsters/AtlasBosses/Zana.aoc";
    // std::optional<std::string> preferredFirstMesh_ = "Metadata/NPC/League/Metamorphosis/MetamorphosisNPC.aoc";
    // std::optional<std::string> preferredFirstMesh_ = "Metadata/NPC/League/Heist/Kura/Kura.aoc";
    // std::optional<std::string> preferredFirstMesh_ = "Metadata/Terrain/Doodads/Leagues/Heist/HanaShrine.aoc";

    absl::node_hash_map<std::string, std::shared_ptr<void>> loadCache_;

    std::optional<ChosenObject> chosenObject_;
    pog::LightSet lights_;
    pog::FixedCamera cam_;
    pog::ScaffoldDrawer scaffoldDrawer_;
    pog::MeshDrawer meshDrawer_;
    std::shared_ptr<MaterialLibrary> matLib_;

    bool showSkeleton_ = true;

    int shaderChoice_{0};
    struct ShaderAlternative {
        std::string name;
        std::shared_ptr<pog::Shader> shader;
    };

    std::vector<ShaderAlternative> shaderAlternatives_{
        {"Fallback", nullptr},
    };
};
