#include <loguru/loguru.hpp>

#include "ui_common.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <gli/gli.hpp>

#include <visions_ui.hpp>

#include <poe/util/parse_info.hpp>
#include <poe/util/path.hpp>
#include <poe/io/vfs.hpp>

#include <poe/format/ao.hpp>
#include <poe/format/fmt.hpp>
#include <poe/format/spritesheet.hpp>
#include <poe/format/tgm.hpp>

#include <fmt/format.h>

#include <asio.hpp>

#include <nlohmann/json.hpp>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <regex>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <thread>
#include <unordered_set>
#include <vector>

using namespace std::literals::string_view_literals;

int main(int argc, char **argv) {
    loguru::init(argc, argv);
    asio::thread_pool pool{6};
    asio::thread_pool scan_pool{6};
    auto pool_wg = asio::make_work_guard(pool);
    auto scan_wg = asio::make_work_guard(scan_pool);

    std::optional<std::filesystem::path> pack_path;

    for (int i = 1; i < argc; ++i) {
        if (argv[i] == "--pack"sv && i + 1 < argc) {
            ++i;
            pack_path = argv[i];
        }
    }

    if (!pack_path) {
        LOG_F(FATAL, "Required argument --pack not specified");
    }

    if (pack_path) {
        if (!exists(*pack_path)) {
            LOG_F(FATAL, "Pack path \"{}\" does not exist", pack_path->string());
        }
        if (!is_regular_file(*pack_path) && !is_symlink(*pack_path) && !is_directory(*pack_path)) {
            LOG_F(FATAL, "Pack path \"{}\" is not a file", pack_path->string());
        }
    }

    struct scan_stats {
        std::mutex mutex;

        std::atomic<uint64_t> packs_processed{};
        std::atomic<uint64_t> packs_found{};
        std::atomic<uint64_t> aos_processed{};
        std::atomic<uint64_t> aos_found{};
        std::atomic<uint64_t> aos_good{};
        std::atomic<uint64_t> aos_bad{};

        std::map<size_t, size_t> ao_versions;
        std::map<std::string, size_t> ao_bases;
        std::vector<std::string> ao_comment_lines;
        std::set<poe::util::sha256_digest> unique_ao_files;
    } scan_stats;

    std::atomic<bool> scan_bail{};

    auto vfs = std::make_shared<poe::io::vfs>(*pack_path);
    scan_stats.packs_found = 1;

    {
        auto fmt_data =
            vfs->open_file(
                   poe::util::path("Art/Microtransactions/spell/explosive_arrow/revenant/mb/arrow/basearrow.fmt"))
                ->read_all();
        auto fmt_file = poe::format::fmt::parse_fmt(fmt_data);
        LOG_F(INFO, "FMT: {}", (void *)fmt_file.get());
    }

    {
        using VersionKey = std::pair<uint8_t, std::string>;
        std::map<VersionKey, size_t> end_stages;
        std::map<VersionKey, size_t> faults_seen;
        std::map<uint8_t, size_t> version_counts;
        vfs->enumerate_files([&](poe::util::path file_path) {
            if (file_path.extension() == ".fmt") {
                auto data = vfs->open_file(file_path)->read_all();
                if (!data.size()) {
                    return;
                }
                uint8_t version = (uint8_t)data[0];
                version_counts[version]++;
                if (version >= 6 && version <= 7) {
                    auto &pis = poe::util::get_parse_info_service();
                    pis.begin_parse(file_path.path_);
                    auto file = poe::format::fmt::parse_fmt(data);
                    if (!file) {
                        auto pi = pis.current_parse();
                        std::string last_stage = pi->data_["stages"].back()["name"];
                        end_stages[{version, last_stage}]++;
                        for (std::string fault : pi->data_["faults"]) {
                            faults_seen[{version, fault}]++;
                        }
                        // LOG_F(INFO, "{} {}", file_path.path_, to_string(pi->data_["faults"]));
                    }
                    pis.end_parse();
                }
            }
        });
        for (auto const &[version_fault, count] : faults_seen) {
            auto const &[version, fault] = version_fault;
            LOG_F(INFO, "v{} fault \"{}\": {}", version, fault, count);
        }
        for (auto const &[version_stage, count] : end_stages) {
            auto const &[version, stage] = version_stage;
            LOG_F(INFO, "v{} stage \"{}\": {}", version, stage, count);
        }
        return 0;
    }

#if 0
    {
        asio::post(pool, [&scan_pool, vfs, &scan_stats, &scan_bail] {
            vfs->enumerate_files([&](poe::util::path file_path) {
                asio::post(scan_pool, [vfs, file_path, &scan_stats] {
                    auto const ext = file_path.extension();
                    bool is_tgm = ext == ".tgm";
                    if (is_tgm) {
                        auto fh = vfs->open_file(file_path);
                        auto payload = fh->read_all();
                        auto &pis = poe::util::get_parse_info_service();
                        pis.begin_parse(file_path.path_);
                        auto tgm = poe::format::tgm::parse_tgm(payload);
                        pis.end_parse();
                    }
#if 0
                    if (is_ao || is_aoc) {
                        auto fh = vfs->open_file(file_path);
                        auto payload = fh->read_all();
                        std::unique_lock lk(scan_stats.mutex);
                        auto file_digest = poe::util::oneshot_sha256(payload.data(), payload.size());
                        if (scan_stats.unique_ao_files.insert(file_digest).second) {
                            ++scan_stats.aos_found;
                            auto &pis = poe::util::get_parse_info_service();
                            pis.begin_parse(file_path.path_);
                            auto parse_res = poe::format::ao::parse_ao(payload);
                            ++scan_stats.aos_processed;
                            pis.end_parse();
                        }
                    }
#endif
                });
            });
            ++scan_stats.packs_processed;
        });
    }
#endif

    ui_common::ui_context ui("Visions", ui_common::ui_flags_use_default_fonts);
    std::map<std::string, std::string> shown_files_contents;
    std::map<std::string, bool> shown_files_stay_open;

    ImFont *sans_font{}, *mono_font{};
    {
        auto exe_dir = poe::util::executable_dir();
        if (exe_dir) {
            auto sans_file = *exe_dir / "Roboto-Regular.ttf";
            if (exists(sans_file)) {
                sans_font = ui.io_->Fonts->AddFontFromFileTTF(sans_file.string().c_str(), 18.0f * ui.dpi_scale_factor_);
                ui.io_->FontDefault = sans_font;
            }
            auto mono_file = *exe_dir / "RobotoMono-Regular.ttf";
            if (exists(mono_file)) {
                mono_font = ui.io_->Fonts->AddFontFromFileTTF(mono_file.string().c_str(), 18.0f * ui.dpi_scale_factor_);
            }
        }
    }
    (void)sans_font;
    (void)mono_font;

    std::shared_ptr<poe::format::spritesheet::sheet> ui_sheet;
    poe::util::path ui_sheet_path("Art/UIImages1.txt");
    {
        auto data = vfs->open_file(ui_sheet_path)->read_all();
        ui_sheet = poe::format::spritesheet::parse_spritesheet(data);
    }
    // imgui_sprite_sheet_listing sprite_sheet_listing(vfs, ui_sheet_path, ui_sheet);
    // imgui_asset_picker asset_picker(vfs);
    // imgui_asset_info asset_info(asset_picker, mono_font);
    imgui_dat_viewer dat_viewer(vfs);
    // imgui_ao_parse_info ao_parse_info(*vfs, *hideout_doodads_dat);

    bool show_demo = false;

    while (ui.new_frame()) {
        if (ImGui::BeginMenuBar()) {
            if (ImGui::MenuItem("ImGui Demo")) {
                show_demo = true;
            }

            ImGui::EndMenuBar();
        }
        ImGui::End(); // For main window, do menu above this

        if (ImGui::Begin("AO survey")) {
            ImGui::Text("%llu/%llu packs processed.", scan_stats.packs_processed.load(), scan_stats.packs_found.load());
            ImGui::Text("%llu/%llu AOs parsed.", scan_stats.aos_processed.load(), scan_stats.aos_found.load());
            ImGui::Text("%llu good AOs, %llu bad AOs.", scan_stats.aos_good.load(), scan_stats.aos_bad.load());
            {
                std::lock_guard lk(scan_stats.mutex);
                ImGui::Text("%llu unique AO files.", scan_stats.unique_ao_files.size());
                for (auto &[version, count] : scan_stats.ao_versions) {
                    ImGui::Text("Version %llu: %llu AOs", version, count);
                }
                for (auto &[base, count] : scan_stats.ao_bases) {
                    ImGui::Text("Extending \"%s\": %llu AOs", base.c_str(), count);
                }
            }
        }
        ImGui::End();

        if (ImGui::Begin("Parse infos")) {
            auto &pis = poe::util::get_parse_info_service();
            auto infos = pis.get_infos();
            ImGui::Text("%llu parse infos.", infos.size());

            for (auto &[name, pi] : infos) {
                if (!pi || !pi->data_.contains("stages")) {
                    continue;
                }
                auto &last_stage = pi->data_["stages"].back();
                auto &faults = pi->data_["faults"];
                if (faults.size()) {
                    if (ImGui::Button(name.c_str())) {
                        if (!shown_files_contents.contains(name)) {
                            auto data = vfs->open_file(poe::util::path(name))->read_all();
                            std::u16string_view wide_view(reinterpret_cast<char16_t const *>(data.data()),
                                                          data.size() / 2);
                            if (wide_view.size() && wide_view[0] == u'\uFEFF') {
                                wide_view = wide_view.substr(1);
                            }
                            auto text = poe::util::to_string(wide_view);
                            shown_files_contents.insert({name, text});
                            shown_files_stay_open.insert({name, true});
                        }
                    }
                    ImGui::Text("stage: %s", std::string(last_stage["name"]).c_str());
                    for (auto &fault : faults) {
                        ImGui::Text("%s", std::string(fault).c_str());
                    }
                }
            }
        }
        ImGui::End();

        std::vector<std::string> close_windows;
        for (auto const &[name, contents] : shown_files_contents) {
            auto &stay_open = shown_files_stay_open[name];
            if (ImGui::Begin(name.c_str(), &shown_files_stay_open[name])) {
                if (ImGui::Button("Open in external editor")) {
                    std::string leaf = name;
                    for (auto &ch : leaf) {
                        if (ch == '/' || ch == '\\') {
                            ch = '#';
                        }
                    }
                    auto export_path = std::filesystem::temp_directory_path() / leaf;
                    std::ofstream os(export_path, std::ios::binary);
                    os.write(contents.data(), contents.size());
                    system(fmt::format("code \"{}\"", export_path.string()).c_str());
                }
                ImGui::TextUnformatted(contents.c_str());
            }
            ImGui::End();
            if (!stay_open) {
                close_windows.push_back(name);
            }
        }

        for (auto const &name : close_windows) {
            shown_files_contents.erase(name);
            shown_files_stay_open.erase(name);
        }

        // sprite_sheet_listing.run();
        // asset_picker.run();
        // asset_info.run();
        dat_viewer.run();
        // ao_parse_info.run();

        if (show_demo) {
            ImGui::ShowDemoWindow(&show_demo);
        }

        ImGui::Render();

        int win_w, win_h;
        glfwGetFramebufferSize(ui.win_, &win_w, &win_h);
        glViewport(0, 0, win_w, win_h);
        glClearColor(0.1f, 0.2f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(ui.win_);
    }

    scan_bail = true;
    scan_wg.reset();
    scan_pool.join();

    pool_wg.reset();
    pool.join();

    return 0;
}
