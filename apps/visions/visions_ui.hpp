#pragma once

#include <poe/io/vfs.hpp>

#include <poe/format/ao.hpp>
#include <poe/format/dat.hpp>
#include <poe/format/spritesheet.hpp>

#include <imgui.h>
#include <imgui_memory_editor.h>

#include <gl/glew.h>

#include <map>
#include <set>
#include <string>
#include <vector>

struct imgui_asset_picker {
    explicit imgui_asset_picker(std::shared_ptr<poe::io::vfs> vfs);

    void run();

    std::shared_ptr<poe::io::vfs> vfs_;
    struct directory {
        std::map<std::string, directory> directories_;
        std::map<std::string, poe::util::path> files_;
    };
    directory root_;
    std::vector<std::pair<std::string, directory const *>> dir_stack_;

    poe::util::path const *current_entry_path_ = nullptr;
    std::shared_ptr<poe::io::file_handle const> current_entry_;
};

struct imgui_asset_info {
    imgui_asset_info(imgui_asset_picker const &asset_picker, ImFont *mono_font);

    void run();

    ImFont *mono_font_;
    imgui_asset_picker const &asset_picker_;
    MemoryEditor editor_, dat_editor_;

    poe::util::path const *entry_path_;
    std::shared_ptr<poe::io::file_handle const> entry_;
    std::shared_ptr<std::vector<std::byte>> entry_payload_;
    std::optional<GLuint> image_tex_;
    bool treat_as_utf16le_ = false;
    bool treat_as_utf8_ = false;
};

struct imgui_dat_viewer {
    imgui_dat_viewer(std::shared_ptr<poe::io::vfs> vfs);

    void run();

    ImGuiTextFilter filter_;
    bool refresh_needed_;
    bool hide_good_;
    std::map<std::string, std::shared_ptr<poe::format::dat::dat_table>> dat_parses_;
    std::map<std::string, std::shared_ptr<poe::io::file_handle>> dat_files_;
    std::map<std::string, bool> open_dats_;
    std::map<std::string, std::vector<bool>> dat_visible_columns_;
};

struct imgui_ao_parse_info {
    imgui_ao_parse_info(std::shared_ptr<poe::io::vfs> vfs, poe::format::dat::dat_table const &hideout_doodads_dat);

    void run();

    std::vector<poe::util::path> ao_paths_;
    std::vector<poe::util::path> aoc_paths_;
    std::map<poe::util::path, std::shared_ptr<poe::format::ao::ao_parse>> ao_parses_;

    bool only_fail_ao_ = true;
    std::string limit_ao_version_;
};

struct imgui_sprite_sheet_listing {
    imgui_sprite_sheet_listing(std::shared_ptr<poe::io::vfs> vfs, poe::util::path path, std::shared_ptr<poe::format::spritesheet::sheet> sheet);
    ~imgui_sprite_sheet_listing();

    void run();

    std::shared_ptr<poe::io::vfs> vfs_;
    std::shared_ptr<poe::format::spritesheet::sheet> sheet_;
    std::string win_name_;
    std::map<std::string, GLuint> sheet_textures_;
};