#include <visions_ui.hpp>

#include <imgui.h>
#include <imgui_stdlib.h>

#include <poe/util/hexdump.hpp>

#include <fmt/format.h>
#include <loguru/loguru.hpp>

#include <GL/glew.h>
#include <gli/gli.hpp>

imgui_asset_picker::imgui_asset_picker(std::shared_ptr<poe::io::vfs> vfs) : vfs_(vfs) {
    vfs->enumerate_files([&](poe::util::path path) {
        directory *parent = &this->root_;
        size_t const n = path.components_.size();
        for (size_t i = 0; i < n; ++i) {
            auto const &ex = path.components_[i];
            auto comp = std::string(path.component(ex));
            if (i + 1 == n) {
                parent->files_.insert({comp, path});
            } else {
                parent = &parent->directories_[comp];
            }
        }
    });
    dir_stack_.push_back({"", &root_});
}

void imgui_asset_picker::run() {
    if (ImGui::Begin("Root entries")) {
        for (size_t i = 0; i < dir_stack_.size(); ++i) {
            if (i) {
                ImGui::SameLine();
                ImGui::Text("/");
                ImGui::SameLine();
            }
            auto &[name, dir] = dir_stack_[i];
            auto label = name.empty() ? " " : name.c_str();
            if (ImGui::Button(label)) {
                dir_stack_.resize(i + 1);
            }
        }
        ImGui::Separator();
        if (dir_stack_.size() > 1 && ImGui::Button("..")) {
            dir_stack_.pop_back();
        } else {
            auto &[_, cwd] = dir_stack_.back();

            std::vector<std::pair<std::string, directory const *>> directories;
            for (auto &[name, dir] : cwd->directories_) {
                directories.push_back({name, &dir});
            }
            std::sort(directories.begin(), directories.end(), [](auto &&a, auto &&b) -> bool {
                return poe::util::lowercase(a.first) < poe::util::lowercase(b.first);
            });

            std::vector<std::pair<std::string, poe::util::path const *>> files;
            for (auto &[name, path] : cwd->files_) {
                files.push_back({name, &path});
            }
            std::sort(files.begin(), files.end(), [](auto &&a, auto &&b) -> bool {
                return poe::util::lowercase(a.first) < poe::util::lowercase(b.first);
            });

            for (auto &[name, dir] : directories) {
                if (ImGui::Button(name.c_str())) {
                    dir_stack_.push_back({name, dir});
                }
            }
            for (auto &[name, path] : files) {
                if (ImGui::Button(name.c_str())) {
                    current_entry_path_ = path;
                    current_entry_ = vfs_->open_file(*path);
                }
            }
        }
    }
    ImGui::End();
}

imgui_asset_info::imgui_asset_info(imgui_asset_picker const &asset_picker, ImFont *mono_font)
    : mono_font_(mono_font), asset_picker_(asset_picker) {
    editor_.ReadOnly = true;
    dat_editor_.ReadOnly = true;
}

static std::set<std::string_view> image_extensions{".dds"};
static std::set<std::string_view> utf16le_extensions{".ao", ".aoc", ".ot", ".otc"};
static std::set<std::string_view> utf8_extensions{".hlsl"};

void imgui_asset_info::run() {
    if (ImGui::Begin("Current entry")) {
        if (entry_path_ != asset_picker_.current_entry_path_) {
            entry_path_ = asset_picker_.current_entry_path_;
            entry_ = asset_picker_.current_entry_;
            if (entry_) {
                entry_payload_ = std::make_shared<std::vector<std::byte>>(entry_->read_all());
            } else {
                entry_payload_.reset();
            }
            if (image_tex_) {
                glDeleteTextures(1, &*image_tex_);
                image_tex_ = std::nullopt;
            }

            if (entry_path_ && entry_path_->extension() == ".dds") {
                std::string actual_path = entry_path_->path_;
                auto payload = chase_symlinks(asset_picker_.vfs_, entry_payload_, actual_path);
                auto tex_gli = gli::load_dds((char const *)payload->data(), payload->size());

                gli::gl GL(gli::gl::PROFILE_GL33);
                gli::gl::format const format = GL.translate(tex_gli.format(), tex_gli.swizzles());

                if (tex_gli.target() == gli::TARGET_2D) {
                    GLuint tex;
                    glCreateTextures(GL_TEXTURE_2D, 1, &tex);
                    glTextureParameteri(tex, GL_TEXTURE_BASE_LEVEL, 0);
                    glTextureParameteri(tex, GL_TEXTURE_MAX_LEVEL, (GLint)(tex_gli.levels() - 1));
                    glTextureParameteriv(tex, GL_TEXTURE_SWIZZLE_RGBA, &format.Swizzles[0]);
                    auto extent = tex_gli.extent();
                    glTextureStorage2D(tex, (GLsizei)tex_gli.levels(), format.Internal, extent.x, extent.y);
                    for (size_t level = 0; level < tex_gli.levels(); ++level) {
                        auto extent = tex_gli.extent(level);
                        if (gli::is_compressed(tex_gli.format())) {
                            glCompressedTextureSubImage2D(tex, (GLint)level, 0, 0, extent.x, extent.y, format.Internal,
                                                          (GLsizei)tex_gli.size(level), tex_gli.data(0, 0, level));
                        } else {
                            glTextureSubImage2D(tex, (GLint)level, 0, 0, extent.x, extent.y, format.External,
                                                format.Type, tex_gli.data(0, 0, level));
                        }
                    }
                    image_tex_ = tex;
                }
            }

            treat_as_utf16le_ = false;
            treat_as_utf8_ = false;
        }
        if (entry_) {
            ImGui::Text("Name: %s", entry_path_->path_.c_str());
            ImGui::Text("Size: %llu", entry_->size());
            ImGui::Checkbox("Treat as UTF-16LE", &treat_as_utf16le_);
            ImGui::Checkbox("Treat as UTF-8", &treat_as_utf8_);
            if (ImGui::BeginTabBar("asset_tabs")) {
                auto &payload = *entry_payload_;
                auto ext = entry_path_->extension();
                if (image_extensions.contains(ext)) {
                    if (ImGui::BeginTabItem("Image")) {
                        if (image_tex_) {
                            int w, h;
                            glGetTextureLevelParameteriv(*image_tex_, 0, GL_TEXTURE_WIDTH, &w);
                            glGetTextureLevelParameteriv(*image_tex_, 0, GL_TEXTURE_HEIGHT, &h);
                            ImGui::Image((ImTextureID)(uintptr_t)*image_tex_, {(float)w, (float)h});
                        } else {
                            ImGui::Text("<insert picture here>");
                        }
                        ImGui::EndTabItem();
                    }
                }
                if (ext == ".dat") {
                    if (ImGui::BeginTabItem("DAT probe")) {
                        if (auto skel = poe::format::dat::open_skeleton(payload)) {
                            if (ImGui::Begin("DAT rows")) {
                                auto digits_needed = (int)fmt::format("{}", skel->rows_.size()).size();
                                ImGui::Text("Rows: %zu", skel->rows_.size());
                                ImGui::PushFont(mono_font_);
                                for (size_t i = 0; i < skel->rows_.size(); ++i) {
                                    auto const &row = skel->rows_[i];
                                    if (row.size()) {
                                        ImGui::TextUnformatted(
                                            fmt::format(
                                                "{:{}}: {}", i, digits_needed,
                                                poe::util::hexdump(gsl::span(row.data(), row.size()), row.size()))
                                                .c_str());
                                    } else {
                                        ImGui::Text("%*zu: Empty row", digits_needed, i);
                                    }
                                }
                                ImGui::PopFont();
                            }
                            ImGui::End();

                            {
                                ImGui::PushFont(mono_font_);
                                dat_editor_.DrawContents(skel->var_data_.data(), skel->var_data_.size());
                                ImGui::PopFont();
                            }
                        }
                        ImGui::End();
                    }
                }
                if (utf16le_extensions.contains(ext) || treat_as_utf16le_) {
                    if (ImGui::BeginTabItem("UTF-16LE")) {
                        if (payload.size() % 2 != 0) {
                            ImGui::Text("File is not valid UTF-16LE");
                        } else {
                            auto p = reinterpret_cast<char16_t const *>(payload.data());
                            auto as_u16 = std::u16string_view(p, payload.size() / 2);
                            if (as_u16.size() && as_u16[0] == u'\uFEFF') {
                                as_u16 = as_u16.substr(1);
                            }
                            auto text = poe::util::to_string(as_u16);
                            ImGui::PushFont(mono_font_);
                            ImGui::InputTextMultiline("##utf16le", &text, {-FLT_MIN, -FLT_MIN},
                                                      ImGuiInputTextFlags_ReadOnly);
                            ImGui::PopFont();
                        }
                        ImGui::EndTabItem();
                    }
                }
                if (utf8_extensions.contains(ext) || treat_as_utf8_) {
                    if (ImGui::BeginTabItem("UTF-8")) {
                        auto as_u8 = std::string_view(reinterpret_cast<char const *>(payload.data()), payload.size());
                        if (as_u8.size() >= 3 && as_u8.substr(0, 3) == "\xEF\xBB\xBF") {
                            as_u8 = as_u8.substr(3);
                        }
                        std::string text(as_u8.data(), as_u8.size());
                        ImGui::PushFont(mono_font_);
                        ImGui::InputTextMultiline("##utf8", &text, {-FLT_MIN, -FLT_MIN}, ImGuiInputTextFlags_ReadOnly);
                        ImGui::PopFont();
                        ImGui::EndTabItem();
                    }
                }
                if (ImGui::BeginTabItem("Raw")) {
                    ImGui::PushFont(mono_font_);
                    editor_.DrawContents(payload.data(), payload.size());
                    ImGui::PopFont();
                    ImGui::EndTabItem();
                }
                ImGui::EndTabBar();
            }
        } else {
            ImGui::Text("No entry selected.");
        }
    }
    ImGui::End();
}

imgui_dat_viewer::imgui_dat_viewer(std::shared_ptr<poe::io::vfs> vfs) {
    vfs->enumerate_files([&](poe::util::path const &path) {
        if (path.components_.size() == 2 && path.component(path.components_[0]) == "Data") {
            auto leaf = std::string(path.leaf());
            if (path.extension() == ".dat") {
                auto file = vfs->open_file(path);
                this->dat_files_[leaf] = file;
                this->open_dats_[leaf] = false;
            }
        }
    });
    refresh_needed_ = true;
    hide_good_ = false;
}

void imgui_dat_viewer::run() {
    if (refresh_needed_) {
        refresh_needed_ = false;
        for (auto const &[name, file] : dat_files_) {
            auto leaf = std::string(poe::util::path(name).leaf());
            auto contents = file->read_all();
            auto parse = poe::format::dat::open_dat(contents.data(), contents.size(), leaf);
            if (!parse) {
                LOG_F(WARNING, "Failed to parse DAT \"{}\"", leaf);
            }
            this->dat_parses_[leaf] = parse;
        }
    }

    if (ImGui::Begin("DAT files")) {
        if (ImGui::Button("Reload all")) {
            refresh_needed_ = true;
        }
        ImGui::Checkbox("Hide good parses", &hide_good_);

        filter_.Draw();
        for (auto &[name, file] : dat_files_) {
            if (!filter_.PassFilter(name.c_str())) {
                continue;
            }
            if (dat_parses_[name]) {
                if (hide_good_) {
                    continue;
                }
                ImGui::TextColored(ImVec4(0.1f, 0.5f, 0.1f, 1.0f), "Good");
            } else {
                ImGui::TextColored(ImVec4(0.5f, 0.1f, 0.1f, 1.0f), "Bad");
            }
            ImGui::SameLine();
            if (ImGui::Button(name.c_str())) {
                open_dats_[name] = true;
            }
        }
    }
    ImGui::End();

    for (auto &[name, dat] : dat_parses_) {
        if (!open_dats_[name]) {
            continue;
        }
        auto &column_vis = dat_visible_columns_[name];
        if (dat->field_count() != column_vis.size()) {
            column_vis = std::vector<bool>(dat->field_count(), false);
        }

        if (ImGui::Begin(name.c_str(), &open_dats_[name])) {
            if (!dat) {
                ImGui::Text("DAT failed to parse.");
            } else {
                size_t rows = dat->row_count();
                size_t fields = dat->field_count();
                ImGui::Text("Rows: %llu, Cols: %llu", rows, fields);
                size_t num_enabled_fields = std::count(column_vis.begin(), column_vis.end(), true);
                if (ImGui::CollapsingHeader("Fields")) {
                    for (size_t field = 0; field < fields; ++field) {
                        bool show_column = column_vis[field];
                        if (ImGui::Checkbox(std::string(dat->field_name(field)).c_str(), &show_column)) {
                            if (!show_column || num_enabled_fields < 62) {
                                column_vis[field] = show_column;
                            }
                        }
                    }
                }
                num_enabled_fields = std::count(column_vis.begin(), column_vis.end(), true);

                ImGuiTableFlags table_flags = ImGuiTableFlags_RowBg | ImGuiTableFlags_Borders |
                                              ImGuiTableFlags_Resizable | ImGuiTableFlags_Hideable |
                                              ImGuiTableFlags_ScrollX | ImGuiTableFlags_ScrollY;

                std::string table_name = fmt::format("##%s-table", name);
                if (ImGui::BeginTable(table_name.c_str(), (int)num_enabled_fields + 1, table_flags)) {
                    ImGui::TableSetupColumn("RowID");
                    ImGui::TableSetupScrollFreeze(1, 1);
                    std::vector<size_t> enabled_fields;
                    enabled_fields.reserve(num_enabled_fields);
                    for (size_t field = 0; field < fields; ++field) {
                        if (column_vis[field]) {
                            enabled_fields.push_back(field);
                        }
                    }
                    for (auto field : enabled_fields) {
                        ImGui::TableSetupColumn(std::string(dat->field_name(field)).c_str());
                    }
                    ImGui::TableHeadersRow();
                    for (size_t row = 0; row < dat->row_count(); ++row) {
                        ImGui::TableNextRow();
                        if (ImGui::TableSetColumnIndex(0)) {
                            ImGui::Text("%llu", row);
                        }
                        for (size_t col = 0; col < num_enabled_fields; ++col) {
                            auto field = enabled_fields[col];
                            // offset due to synthesised RowID
                            if (ImGui::TableSetColumnIndex((int)col + 1)) {
                                auto val = dat->get(row, field);
                                if (val.is_number_integer()) {
                                    ImGui::Text("%lld", static_cast<int64_t>(val));
                                } else if (val.is_number_float()) {
                                    ImGui::Text("%f", static_cast<float>(val));
                                } else if (val.is_boolean()) {
                                    ImGui::Text("%s", val ? "True" : "False");
                                } else if (val.is_null()) {
                                    ImGui::Text("None");
                                } else {
                                    ImGui::Text("%s", val.dump().c_str());
                                }
                            }
                        }
                    }
                    ImGui::EndTable();
                }
                ImGui::Text(" ");
            }
        }
        ImGui::End();
    }
}

#if 0
imgui_ao_parse_info::imgui_ao_parse_info(std::shared_ptr<poe::io::vfs> vfs,
    poe::format::dat::dat_table const& hideout_doodads_dat) {
        {
            auto vaos_field = *hideout_doodads_dat.find_field("Variation_AOFiles");
            for (size_t row = 0; row < hideout_doodads_dat.row_count(); ++row) {
                auto variations = hideout_doodads_dat.get(row, vaos_field);
                for (auto& var : variations) {
                    if (var.is_string()) {
                        auto s = poe::util::to_u16string(static_cast<std::string>(var));
                        ao_paths_.emplace_back(s);
                        aoc_paths_.emplace_back(s + u"c"); // .aoc
                    }
                }
            }
        }

        {
            for (auto& p : ao_paths_) {
                if (auto data = vfs->read_file(p)) {
                    auto name = to_string(p);
                    auto parse = poe::format::ao::parse_ao(*data);
                    ao_parses_[p] = parse;
                }
            }
            for (auto& p : aoc_paths_) {
                if (auto data = vfs->read_file(p)) {
                    auto name = to_string(p);
                    auto parse = poe::format::ao::parse_ao(*data);
                    ao_parses_[p] = parse;
                }
            }
        }
}

void imgui_ao_parse_info::run() {
    if (ImGui::Begin("AO parse results")) {
        ImGui::InputText("Limit version", &limit_ao_version_);
        std::optional<int> limit_version;
        try {
            limit_version = std::stoi(limit_ao_version_);
        }
        catch (...) {
        }

        ImGui::Checkbox("Only failures", &only_fail_ao_);
        ImGuiTableFlags table_flags = ImGuiTableFlags_RowBg | ImGuiTableFlags_Borders | ImGuiTableFlags_Resizable |
            ImGuiTableFlags_Hideable | ImGuiTableFlags_ScrollX | ImGuiTableFlags_ScrollY;
        if (ImGui::BeginTable("##ao_parses", 2, table_flags)) {
            ImGui::TableSetupScrollFreeze(0, 1);
            ImGui::TableSetupColumn("Path");
            ImGui::TableSetupColumn("Parse");
            ImGui::TableHeadersRow();
            for (auto& [path, parse] : ao_parses_) {
                if (only_fail_ao_ && parse) {
                    continue;
                }
                if (limit_version && (!parse || parse->version_ != limit_version)) {
                    continue;
                }
                ImGui::TableNextRow();
                ImGui::TableSetColumnIndex(0);
                ImGui::Text("%s", to_string(path).c_str());
                ImGui::TableSetColumnIndex(1);
                ImGui::Text("%s", parse ? "OK" : "FAIL");
            }
            ImGui::EndTable();
        }
    }
    ImGui::End();
}
#endif

imgui_sprite_sheet_listing::imgui_sprite_sheet_listing(std::shared_ptr<poe::io::vfs> vfs, poe::util::path path,
                                                       std::shared_ptr<poe::format::spritesheet::sheet> sheet)
    : vfs_(vfs), sheet_(sheet) {
    win_name_ = fmt::format("Spritesheet##{}", path.path_);

    for (auto &e : sheet->entries_) {
        if (!sheet_textures_.contains(e.path)) {
            auto path = poe::util::path(e.path);
            auto payload = std::make_shared<std::vector<std::byte>>(vfs->open_file(path)->read_all());
            std::string actual_path = e.path;
            payload = chase_symlinks(vfs, payload, actual_path);
            if (sheet_textures_.contains(actual_path)) {
                e.path = actual_path;
                continue;
            }
            auto tex_gli = gli::load_dds((char const *)payload->data(), payload->size());

            gli::gl GL(gli::gl::PROFILE_GL33);
            gli::gl::format const format = GL.translate(tex_gli.format(), tex_gli.swizzles());

            if (tex_gli.target() == gli::TARGET_2D) {
                GLuint tex;
                glCreateTextures(GL_TEXTURE_2D, 1, &tex);
                glTextureParameteri(tex, GL_TEXTURE_BASE_LEVEL, 0);
                glTextureParameteri(tex, GL_TEXTURE_MAX_LEVEL, (GLint)(tex_gli.levels() - 1));
                glTextureParameteriv(tex, GL_TEXTURE_SWIZZLE_RGBA, &format.Swizzles[0]);
                auto extent = tex_gli.extent();
                glTextureStorage2D(tex, (GLsizei)tex_gli.levels(), format.Internal, extent.x, extent.y);
                for (size_t level = 0; level < tex_gli.levels(); ++level) {
                    auto extent = tex_gli.extent(level);
                    if (gli::is_compressed(tex_gli.format())) {
                        glCompressedTextureSubImage2D(tex, (GLint)level, 0, 0, extent.x, extent.y, format.Internal,
                                                      (GLsizei)tex_gli.size(level), tex_gli.data(0, 0, level));
                    } else {
                        glTextureSubImage2D(tex, (GLint)level, 0, 0, extent.x, extent.y, format.External, format.Type,
                                            tex_gli.data(0, 0, level));
                    }
                }
                sheet_textures_[e.path] = tex;
            }
        }
    }
    LOG_F(INFO, "{} sheets loaded", sheet_textures_.size());
}

imgui_sprite_sheet_listing::~imgui_sprite_sheet_listing() {
    for (auto &[_, tex] : sheet_textures_) {
        glDeleteTextures(1, &tex);
    }
}

void imgui_sprite_sheet_listing::run() {
    if (ImGui::Begin(win_name_.c_str())) {
        ImGuiTableFlags table_flags = ImGuiTableFlags_Borders | ImGuiTableFlags_ScrollY;
        if (ImGui::BeginTable("##icons", 2, table_flags, {-FLT_MIN, -FLT_MIN})) {
            ImGui::TableSetupColumn("Icon", ImGuiTableColumnFlags_WidthFixed, 64.0f);
            ImGui::NextColumn();
            ImGui::TableSetupColumn("Name");
            for (auto &[id, idx] : sheet_->by_id_) {
                ImGui::TableNextRow();
                auto &entry = sheet_->entries_[idx];
                if (ImGui::TableSetColumnIndex(0)) {
                    auto &tex = sheet_textures_[entry.path];
                    int tex_w, tex_h;
                    glGetTextureLevelParameteriv(tex, 0, GL_TEXTURE_WIDTH, &tex_w);
                    glGetTextureLevelParameteriv(tex, 0, GL_TEXTURE_HEIGHT, &tex_h);
                    ImVec2 uv0{entry.x0 / (float)tex_w, entry.y0 / (float)tex_h};
                    ImVec2 uv1{(entry.x1 + 1) / (float)tex_w, (entry.y1 + 1) / (float)tex_h};
                    auto fit_size = [](auto entry) -> ImVec2 {
                        float side = 64.0f;
                        float w = (float)entry.x1 - entry.x0;
                        float h = (float)entry.y1 - entry.y0;
                        float aspect = w / h;
                        if (aspect >= 1.0f) {
                            return ImVec2{side, side / aspect};
                        } else {
                            return ImVec2{aspect * side, side};
                        }
                    };
                    ImVec2 size = fit_size(entry);
                    ImGui::Image((ImTextureID)(uintptr_t)tex, size, uv0, uv1);
                }
                if (ImGui::TableSetColumnIndex(1)) {
                    ImGui::TextUnformatted(std::string(id).c_str());
                }
            }
            ImGui::EndTable();
        }
    }
    ImGui::End();
}