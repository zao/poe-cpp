﻿#define SILLY_OLD_LEGACY_STUFF 0
#define TEST_IN_PARALLEL 1
#define TEST_FILE_SUBSET 0
#define TEST_LOG_FAILURES 1

#include <absl/container/flat_hash_map.h>

#include <fmt/ostream.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/projection.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <tbb/parallel_for.h>
#include <tbb/concurrent_unordered_map.h>
#include <tbb/concurrent_unordered_set.h>

// cht clt ddt env mtd toy
#include <poe/format/ao.hpp>
#include <poe/format/arm.hpp>
#include <poe/format/ast.hpp>
#include <poe/format/dgr.hpp>
#include <poe/format/et.hpp>
#include <poe/format/ffx.hpp>
#include <poe/format/fmt.hpp>
#include <poe/format/gft.hpp>
#include <poe/format/gt.hpp>
#include <poe/format/mat.hpp>
#include <poe/format/rs.hpp>
#include <poe/format/sm.hpp>
#include <poe/format/smd.hpp>
#include <poe/format/spritesheet.hpp>
#include <poe/format/tdt.hpp>
#include <poe/format/tgm.hpp>
#include <poe/format/tgr.hpp>
#include <poe/format/tgt.hpp>
#include <poe/format/tsi.hpp>
#include <poe/format/tst.hpp>

#include <poe/io/vfs.hpp>
#include <poe/util/install_location.hpp>
#include <poe/util/parse_info.hpp>
#include <poe/util/random_access_file.hpp>

#include <pog/ffx_library.hpp>
#include <pog/material.hpp>

#include <cmath>
#include <chrono>
#include <queue>
#include <regex>

using namespace std::chrono_literals;
using namespace std::string_view_literals;

#if TEST_FILE_SUBSET
std::string const testFileSubset[] = {

};
#endif

namespace ffx_ns = poe::format::ffx;
namespace fxg_ns = poe::format::fxgraph;

void SweepMaterials(std::shared_ptr<poe::io::vfs> vfs) {
    auto ffxLib = std::make_shared<pog::FFXLibrary>(vfs);
    auto matLib = std::make_shared<pog::MaterialLibrary>(vfs, ffxLib);

    auto HasMixedShaderNodes = [&](pog::FxGraphGraph &g) -> bool {
        auto &fxg = g.fxg;
        if (fxg.nodes.empty()) {
            return false;
        }

        // Keep track of whether a node is considered part of the vertex or pixel shader stage.
        // This evolves as we propagate it later on.
        enum class ShaderKind { Unknown, Wildcard, Vertex, Pixel };
        std::vector<ShaderKind> trueKinds(fxg.nodes.size(), ShaderKind::Unknown);
        std::vector<ShaderKind> propagatedKinds(fxg.nodes.size(), ShaderKind::Unknown);

        std::vector<ffx_ns::Fragment const *> nodeFragments(fxg.nodes.size());

        // Build a lookup table from node key to node index to make edge building cheaper.
        // Nodes with fully duplicate keys will be unreachable but those seem to not have edges anyway.
        std::map<fxg_ns::FxGraphKey, size_t> nodeLookup;
        for (size_t nodeIdx = 0; nodeIdx < fxg.nodes.size(); ++nodeIdx) {
            auto &node = fxg.nodes[nodeIdx];
            auto &key = node.key;
            nodeLookup[key] = nodeIdx;
            auto frag = nodeFragments[nodeIdx] = ffxLib->LookupFragment(key.type);
            if (!frag) {
                LOG_F(ERROR, "Missing fragment type {}", key.type);
            } else {
                ShaderKind kind = ShaderKind::Wildcard;
                for (auto const &decl : frag->declarations_) {
                    if (auto *kw = std::get_if<ffx_ns::KeywordDeclaration>(&decl)) {
                        if ("vertex"sv == kw->keyword) {
                            kind = ShaderKind::Vertex;
                            break;
                        }
                        if ("pixel"sv == kw->keyword) {
                            kind = ShaderKind::Pixel;
                            break;
                        }
                    }
                }
                trueKinds[nodeIdx] = kind;
            }
        }

        // Incremental colouring and testing if we're finding a conflict.
        std::set<size_t> unexplored;
        std::set<size_t> sinks;
        for (auto const &[idx, outs] : g.edges) {
            if (outs.empty()) {
                sinks.insert(idx);
            } else {
                unexplored.insert(idx);
            }
        }

        while (!sinks.empty()) {
            std::set<size_t> newSinks;
            for (auto sinkIdx : sinks) {
                unexplored.erase(sinkIdx);
                auto const &sinkFrag = nodeFragments[sinkIdx];
                auto sinkKind = trueKinds[sinkIdx];
                if (sinkKind == ShaderKind::Unknown) {
                    LOG_F(ERROR, "Missing fragment type {} found as sink, mixed result uncertain.",
                          fxg.nodes[sinkIdx].key.type);
                }
                for (auto neighIdx : g.reverseEdges[sinkIdx]) {
                    auto &neighKind = propagatedKinds[neighIdx];
                    if (neighKind == ShaderKind::Wildcard || neighKind == ShaderKind::Unknown) {
                        neighKind = sinkKind;
                    } else if (neighKind != sinkKind) {
                        return true;
                    }
                    newSinks.insert(neighIdx);
                }
            }

            sinks = newSinks;
        }

        return false;
    };

    auto HasDuplicateParents = [&](poe::format::mat::MatFileV4 const &mat) -> bool {
        std::set<std::string> parents;
        for (auto &gi : mat.graphInstances) {
            if (parents.contains(gi.parent)) {
                LOG_F(INFO, "Duplicate parent {}", gi.parent);
                return true;
            }
            parents.insert(gi.parent);
        }
        return false;
    };

    auto HasMissingFragment = [&](poe::format::fxgraph::FxGraph const &fxg) -> bool {
        bool anyMissing = false;
        for (size_t nodeIdx = 0; nodeIdx < fxg.nodes.size(); ++nodeIdx) {
            auto &node = fxg.nodes[nodeIdx];
            auto &key = node.key;
            auto frag = ffxLib->LookupFragment(key.type);
            if (!frag) {
                LOG_F(ERROR, "Missing fragment type {}", key.type);
                anyMissing = true;
            }
        }
        return anyMissing;
    };

    auto HasDuplicateNameIndex = [&](poe::format::fxgraph::FxGraph const &fxg) -> bool {
        std::set<fxg_ns::FxGraphKey> usedKeys;
        for (auto &node : fxg.nodes) {
            auto key = node.key;
            if (usedKeys.contains(key)) {
                if (auto I = std::ranges::find_if(
                        fxg.links,
                        [&](fxg_ns::FxGraphLink const &link) { return link.src.key == key || link.dst.key == key; });
                    I != fxg.links.end()) {
                    LOG_F(INFO, "Duplicate key with link usage");
                } else {
                    LOG_F(INFO, "Duplicate key without link usage");
                }
                return true;
            }
            usedKeys.insert(key);
        }
        return false;
    };

    std::map<std::string, std::set<std::string>> stageOrders;
    auto HasStageOrderConflict = [&](pog::FxGraphGraph g) -> bool {
        std::queue<size_t> sources;
        std::set<size_t> rest;
        for (size_t nodeIdx = 0; nodeIdx < g.nodeCount; ++nodeIdx) {
            if (g.reverseEdges[nodeIdx].empty()) {
                sources.push(nodeIdx);
            } else {
                rest.insert(nodeIdx);
            }
        }

        std::map<size_t, std::set<std::string>> nodeDeps;
        while (!sources.empty()) {
            auto srcIdx = sources.front();
            sources.pop();
            auto &src = g.fxg.nodes[srcIdx];
            auto &srcDeps = nodeDeps[srcIdx];
            auto &srcStage = src.key.stage;
            if (srcStage) {
                srcDeps.insert(*srcStage);
            }
            for (auto neighIdx : g.edges[srcIdx]) {
                nodeDeps[neighIdx].insert(srcDeps.begin(), srcDeps.end());
                g.edges[srcIdx].erase(neighIdx);
                g.reverseEdges[neighIdx].erase(srcIdx);
                if (g.reverseEdges[neighIdx].empty()) {
                    sources.push(neighIdx);
                }
            }
        }

        bool anyBad = false;
        for (auto &[selfIdx, upstream] : nodeDeps) {
            if (auto stage = g.fxg.nodes[selfIdx].key.stage) {
                auto stageOrdinal = matLib->LookupStage(*stage);
                if (!stageOrdinal) {
                    LOG_F(ERROR, "Unknown stage name {}", *stage);
                    anyBad = true;
                    continue;
                }
                for (auto &up : upstream) {
                    if (*stage != up) {
                        auto upOrdinal = matLib->LookupStage(up);
                        if (!upOrdinal) {
                            LOG_F(ERROR, "Unknown stage name {}", *stage);
                            anyBad = true;
                            continue;
                        }
                        if (*stageOrdinal < *upOrdinal) {
                            LOG_F(ERROR, "Bad stage order {} ({}) -> {} ({}) found", up, *upOrdinal, *stage,
                                  *stageOrdinal);
                            anyBad = true;
                            continue;
                        }
#if 0
                        if (stageOrders.contains(up) && stageOrders[up].contains(*stage)) {
                            LOG_F(ERROR, "Conflicting edge {} -> {} found", up, *stage);
                            return true;
                        }
#endif
                    }
                    stageOrders[*stage].insert(up);
                }
            }
        }

        return anyBad;
    };

    vfs->enumerate_files([&](poe::util::path const &path) {
        if (path.path_ != "Metadata/Effects/Graphs/General/CelestialMaterial.fxgraph") {
            // return;
        }
        if (path.extension() == ".mat") {
            auto fh = vfs->open_file(path);
            auto data = fh->read_all();
            auto mat = poe::format::mat::parse_mat(data);
            auto g = pog::FxGraphGraph(*mat->defaultGraph, ffxLib, *matLib);
            if (HasMissingFragment(g.fxg)) {
                LOG_F(INFO, "^ {}", path.path_);
            }
            // if (HasDuplicateParents(*mat)) {
            //     LOG_F(ERROR, "{} has duplicate graphinstance parents", path.path_);
            // }
            if (HasMixedShaderNodes(g)) {
                LOG_F(ERROR, "{} has mixed shader nodes", path.path_);
            }
            if (HasStageOrderConflict(g)) {
                LOG_F(INFO, "^ {}", path.path_);
            }
        }
        if (path.extension() == ".fxgraph") {
            auto fh = vfs->open_file(path);
            auto text = poe::util::map_unicode_to_utf8_string(fh->read_all());
            auto js = nlohmann::json::parse(*text);
            auto fxg = poe::format::fxgraph::parse_fxgraph_json(js);
            auto g = pog::FxGraphGraph(*fxg, ffxLib, *matLib);
            if (HasMissingFragment(*fxg)) {
                LOG_F(INFO, "^ {}", path.path_);
            }
            // if (HasDuplicateNameIndex(*fxg)) {
            //     LOG_F(ERROR, "{} has duplicate name/index tuples", path.path_);
            // }
            if (HasMixedShaderNodes(g)) {
                LOG_F(ERROR, "{} has mixed shader nodes", path.path_);
            }
            if (HasStageOrderConflict(g)) {
                LOG_F(INFO, "^ {}", path.path_);
            }
        }
    });

    for (auto &[self, upstream] : stageOrders) {
        for (auto &up : upstream) {
            if (self != up) {
                LOG_F(INFO, "{} -> {}", up, self);
            }
        }
    }

#if DUMP_GRAPHS_TO_DOTS
    {
        char const *graphs[]{
            "Metadata/Terrain/EndGame/MapMoonTemple/Graphs/moon_temple_1.dgr",
            "Metadata/Terrain/EndGame/MapMoonTemple/Graphs/moon_temple_2.dgr",
            "Metadata/Terrain/EndGame/MapMoonTemple/Graphs/moon_temple_3.dgr",
            "Metadata/Terrain/EndGame/MapMoonTemple/Graphs/moon_temple_4.dgr",
        };
        for (auto &src : graphs) {
            auto data = vfs->open_file(poe::util::path(src))->read_all();
            auto graph = poe::format::dgr::parse_dgr(data);
            auto dst = std::filesystem::path("C:/Temp") / std::filesystem::path(src).filename();
            dst.replace_extension(".dot");
            std::ofstream os(dst);
            os << "digraph G {\n";
            for (int i = 0; i < graph->nodes_.size(); ++i) {
                auto &n = graph->nodes_[i];
                float scale = 10.0f;
                auto mat =
                    glm::rotate(glm::scale(glm::mat4{1}, glm::vec3(scale)), glm::radians(45.0f), glm::vec3(0, 0, 1));
                glm::vec4 pos{n.x_, n.y_, 0, 1};
                auto tpos = mat * pos;
                auto label = fmt::format("\\\"{}\\\"\\n{}", n.key_, n.unk_num2_);
                fmt::print(os, "N{} [label=\"{}\", pos=\"{},{}\"];\n", i, label, tpos.x, tpos.y);
            }
            for (int i = 0; i < graph->edges_.size(); ++i) {
                auto &e = graph->edges_[i];
                fmt::print(os, "N{} -> N{};\n", e.from_node_, e.to_node_);
            }
            os << "}\n";
        }
        return 0;
    }
#endif
}

int wmain(int argc, wchar_t **argv) {
    std::optional<uint32_t> desiredBuild;
    if (argc == 2) {
        desiredBuild = (uint32_t)std::stoull(argv[1]);
    }

    auto collective = poe::io::file_collective_info {
        //.collective_root_ = "F:/Temp/poe/collective",
#if SILLY_OLD_LEGACY_STUFF
        // .build = 1992927, // 2.6.3
        // .build = 5540437, // 3.11.2b
        // .build = 6087811, // 3.13.0 167111
        // .build = 6272937, // 3.13.1d 169296 (v9)
        // .build = 6454148, // 3.13.1e 171804 (v9)
        // .build = 6467139, // 3.13.2 172073 (v11)
        .build = 6506650, // 3.13.2b 172572 (v11)
                          // .build = 6525207, // 3.13.2c 172852 (v11)
                          // .build = 6553522, // 3.14.0 173216
#else
        .build = 8930624, // 3.18.1 197103
#endif
    };
    if (desiredBuild) {
        collective.build = *desiredBuild;
    }
    auto vfs = std::make_shared<poe::io::vfs>(collective);
    if (!vfs) {
        throw std::runtime_error("could not open vfs");
    }

#if SWEEP_MATERIALS
    SweepMaterials(vfs);
#endif

    struct Outcome {
        uint32_t good{0}, all{0};
    };

    std::pair<char const *, Outcome> p({(char const *)"hi", Outcome()});

    tbb::concurrent_unordered_map<char const *, Outcome> outcomes;
    tbb::concurrent_unordered_set<std::string> failLog;
    auto TestParse = [&vfs, &outcomes, &failLog](poe::util::path const &path) {
        auto OptToSP = []<typename T>(std::optional<T> opt) -> std::shared_ptr<T> {
            if (!opt) {
                return {};
            }
            auto ret = std::make_shared<T>();
            *ret = std::move(opt.value());
            return ret;
        };
        auto ext = path.extension();
#define PARSE_TEST(Ext) {"." #Ext, [](auto data) { return (bool)poe::format::Ext::parse_##Ext(data); }},
#define PARSE_TEST_EXT_AS(Ext, Fmt) {"." #Ext, [](auto data) { return (bool)poe::format::Fmt::parse_##Fmt(data); }},
        static absl::flat_hash_map<std::string, std::function<bool(std::span<std::byte const>)>> parsers{
            // clang-format off
            PARSE_TEST(ao)
            PARSE_TEST_EXT_AS(aoc, ao)
            PARSE_TEST(arm)
            PARSE_TEST(ast)
            PARSE_TEST(dgr)
            PARSE_TEST(et)
            PARSE_TEST(ffx)
            PARSE_TEST(fmt)
            PARSE_TEST(gft)
            PARSE_TEST(gt)
            PARSE_TEST(rs)
            PARSE_TEST(sm)
            PARSE_TEST(smd)
            PARSE_TEST(spritesheet)
            PARSE_TEST(tdt)
            PARSE_TEST(tgm)
            PARSE_TEST(tgr)
            PARSE_TEST(tgt)
            PARSE_TEST(tsi)
            PARSE_TEST(tst)
            // clang-format on
        };
#undef PARSE_TEST
        if (auto I = parsers.find(absl::string_view(ext.data(), ext.size())); I != parsers.end()) {
            auto &p = path.path_;
            auto fh = vfs->open_file(path);
            auto data = fh->read_all();
            auto &svc = poe::util::get_parse_info_service();
            svc.begin_parse(p);
            auto res = I->second(data);
            svc.end_parse();
            auto &outcome = outcomes[I->first.c_str()];
            InterlockedIncrement(&outcome.all);
            if (res) {
                InterlockedIncrement(&outcome.good);
            }
            if (!res) {
#if TEST_LOG_FAILURES
                failLog.insert(p);
#endif
                LOG_F(ERROR, "{}: parse failure", p);
            }
        }
    };

    // Bulk parse all file types listed above to exercise parser.
    // Yes, this doesn't really belong here.

    // TestParse({"Metadata/Terrain/RuinedCity/Sewers/Floor/sewers_floor_dry_valley.et"});
    // TestParse({"Metadata/Terrain/Church/Abyss/church_fence_crevice_wallthick_v01_01.tdt"});
    // TestParse({"Metadata/Terrain/Act8/Area1/room_nodes.rs"});
    // TestParse({"Art/Models/Terrain/Act3/ElegantHouse/tgms/eleganthouse_broken_floor_v02_01_c1r2.tgm"});
    // TestParse({"Art/Models/Items/Quests/lantern_v01_01.fmt"});
    // TestParse({"Art/Models/Terrain/Act3/SceptreofGod/rubble_v01_01.fmt"});
    // TestParse({"Art/Models/Terrain/Act5/Templar Court/templar_bookshelf_v01_01.fmt"});
    {
        using Paths = std::vector<poe::util::path>;
        Paths paths;
#if TEST_FILE_SUBSET
        for (auto const &cand : testFileSubset) {
            paths.push_back(poe::util::path(cand));
        }
#else
        vfs->enumerate_files([&](poe::util::path path) { paths.push_back(path); });
#endif

        LOG_F(INFO, "off-parses gathered");
        using Range = tbb::blocked_range<Paths::const_iterator>;
#if TEST_IN_PARALLEL
        tbb::parallel_for(Range(paths.begin(), paths.end()), [&TestParse, &paths](Range &r) {
            for (auto &path : r) {
                TestParse(path);
            }
        });
#else
        std::sort(paths.begin(), paths.end(), [](auto &a, auto &b) { return a.path_ < b.path_; });
        for (auto &path : paths) {
            TestParse(path);
        }
#endif

        LOG_F(INFO, "off-parses done");
        std::map<std::string, Outcome> sortedOutcomes;
        for (auto &[ext, oc] : outcomes) {
            auto good = InterlockedCompareExchange(&oc.good, 0, 0);
            auto all = InterlockedCompareExchange(&oc.all, 0, 0);
            sortedOutcomes[ext] = {good, all};
        }
        for (auto &[ext, oc] : sortedOutcomes) {
            LOG_F(INFO, "Format {}: {} bad, {}/{} good", std::string(ext), oc.all - oc.good, oc.good, oc.all);
        }

#if TEST_LOG_FAILURES
        for (auto const &p : failLog) {
            std::cout << p << std::endl;
        }
#endif
    }
    return 0;
}
