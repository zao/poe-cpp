#pragma once

#include <cubby/project.hpp>

#include <cstddef>
#include <cstdint>
#include <map>

#include <ui_panes.hpp>

#include "poe/format/arm.hpp"
#include "poe/format/dgr.hpp"
#include "poe/format/tgr.hpp"
#include "poe/format/tst.hpp"

#include "cubby/level_gen.hpp"

namespace cubby {
struct HideoutLayout {
    using TopologyGraph =
        std::variant<std::shared_ptr<poe::format::dgr::DGRFile>, std::shared_ptr<poe::format::tgr::TGRFile>>;

    TopologyGraph topology_graph;

    struct Room {
        int x_pos;
        int y_pos;
        std::shared_ptr<poe::format::arm::ARMFile> arm;
    };

    poe::format::tst::Tst tst;
    std::vector<Room> rooms;
};

struct HideoutMeta {
    std::vector<cubby_decoration_item> all_decorations;
    std::map<uint32_t, size_t> decoration_idx_by_hash;

    std::map<uint16_t, HideoutLayout> layouts;
    std::map<uint16_t, std::shared_ptr<cubby::level_gen::TopologyGraph>> levelTopos;
};
} // namespace cubby