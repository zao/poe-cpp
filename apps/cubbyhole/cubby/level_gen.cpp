#include "cubby/level_gen.hpp"

#include <fmt/format.h>
#include <loguru/loguru.hpp>

#include <unordered_set>

poe::util::path makeMaybeRelative(poe::util::path anchor, std::string added) {
    if (auto pos = added.find('/'); pos != added.npos) {
        auto first = added.substr(0, pos);
        static std::unordered_set<std::string> roots{"Art",     "Audio",  "CachedHLSLShaders", "Data", "Metadata",
                                                     "minimap", "Shaders"};
        if (roots.count(first)) {
            return poe::util::path(added);
        }
    }
    return anchor.make_relative(added);
}

namespace cubby::level_gen {
TopologyGraph::TopologyGraph(std::shared_ptr<poe::io::vfs> vfs, poe::util::path const &path) {
    {
        auto file = vfs->open_file(path);
        if (!file) {
            throw std::runtime_error(fmt::format("Could not open graph file \"{}\"", path.path_));
        }

        auto data = file->read_all();
        if (path.extension() == ".tgr") {
            auto res = poe::format::tgr::parse_tgr(data);
            if (res) {
                graph = std::make_shared<TGraph>(res);
            }
        } else if (path.extension() == ".dgr") {
            auto res = poe::format::dgr::parse_dgr(data);
            if (res) {
                graph = std::make_shared<DGraph>(res);
            }
        }

        if (!graph) {
            throw std::runtime_error(fmt::format("Could not load graph \"{}\"", path.path_));
        }
    }

    auto masterPath = poe::util::path(graph->masterFile());
    {
        auto file = vfs->open_file(masterPath);
        if (!file) {
            throw std::runtime_error(fmt::format("Could not open master file \"{}\"", masterPath.path_));
        }

        masterFile = poe::format::tsi::parse_tsi(file->read_all());
        if (!masterFile) {
            throw std::runtime_error(fmt::format("Could not load master file \"{}\"", masterPath.path_));
        }
    }

    // outerGroundType

    // roomSet
    if (auto rsStr = masterFile->find_quoted_field("RoomSet")) {
        auto rsPath = makeMaybeRelative(masterPath, *rsStr);
        try {
            roomSet = std::make_shared<RoomSet>(vfs, rsPath);
        } catch (std::exception &e) {
            throw std::runtime_error(fmt::format("Could not load room set \"{}\": {}", rsPath.path_, e.what()));
        }
    } else {
        throw std::runtime_error(fmt::format("No room set in master file \"{}\"", masterPath.path_));
    }

    // edgeCombination

    // fillTiles

    // tileSet
    if (auto tsStr = masterFile->find_quoted_field("TileSet")) {
        auto tsPath = makeMaybeRelative(masterPath, *tsStr);
        try {
            tileSet = std::make_shared<TileSet>(vfs, tsPath);
        } catch (std::exception &e) {
            throw std::runtime_error(fmt::format("Could not load tile set \"{}\": {}", tsPath.path_, e.what()));
        }
    } else {
        throw std::runtime_error(fmt::format("No tile set in master file \"{}\"", masterPath.path_));
    }

    // materialsList

    // environment

    // edgePoints
}

RoomSet::RoomSet(std::shared_ptr<poe::io::vfs> vfs, poe::util::path const &path) {
    auto fh = vfs->open_file(path);
    if (!fh) {
        throw std::runtime_error(fmt::format("File not found: \"{}\"", path.path_));
    }
    rs = poe::format::rs::parse_rs(fh->read_all());
    if (!rs) {
        throw std::runtime_error(fmt::format("Parse failed: \"{}\"", path.path_));
    }

    for (auto &entry : rs->entries_) {
        auto armPath = poe::util::path(entry.arm_ref_);
        auto armFh = vfs->open_file(armPath);
        if (!armFh) {
            throw std::runtime_error(fmt::format("File not found: \"{}\"", armPath.path_));
        }
        auto arm = poe::format::arm::parse_arm(armFh->read_all());
        if (!arm) {
            throw std::runtime_error(fmt::format("Parse failed: \"{}\"", armPath.path_));
        }
        rooms.push_back(Room{.arm = arm});
    }
}

DGraph::DGraph(std::shared_ptr<poe::format::dgr::DGRFile> graph) : dgr(graph) {}

TGraph::TGraph(std::shared_ptr<poe::format::tgr::TGRFile> graph) : tgr(graph) {}

TileSet::TileSet(std::shared_ptr<poe::io::vfs> vfs, poe::util::path const &path) {
    {
        auto fh = vfs->open_file(path);
        if (!fh) {
            throw std::runtime_error(fmt::format("File not found: \"{}\"", path.path_));
        }
        auto res = poe::format::tst::parse_tst(fh->read_all());
        if (!res) {
            throw std::runtime_error(fmt::format("Parse failed: \"{}\"", path.path_));
        }
        tst = *res;
    }

    for (auto &part : tst.parts) {
        for (auto tdtStr : part) {
            auto path = poe::util::path(tdtStr.tdt);
            auto tile = std::make_shared<TdtTile>(vfs, path);
            tileDefs.push_back(tile);
            defByPath[tile->path] = tile;
            defByTag[tile->tdt.tag].insert(tile);
        }
    }
}

TdtTile::TdtTile(std::shared_ptr<poe::io::vfs> vfs, poe::util::path const &path) {
    auto fh = vfs->open_file(path);
    if (!fh) {
        throw std::runtime_error(fmt::format("File not found: \"{}\"", path.path_));
    }
    auto res = poe::format::tdt::parse_tdt(fh->read_all());
    if (!res) {
        throw std::runtime_error(fmt::format("Parse failed: \"{}\"", path.path_));
    }
    tdt = *res;

    this->path = path.path_;

    key.tag = tdt.tag;
    for (size_t i = 0; i < 4; ++i) {
        key.edgeKeys[i] = tdt.header.sideEts[i];
        key.groundKeys[i] = tdt.header.sideGts[i];
        key.edgeOffsets[i] = tdt.header.sideOffsets[i];
        key.groundOffsets[i] = tdt.header.sideOffsets[i + 4];
    }
}

} // namespace cubby::level_gen