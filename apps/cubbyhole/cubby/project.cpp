#include <cubby/project.hpp>

#include <poe/util/utf.hpp>

#include <fstream>
#include <optional>
#include <regex>
#include <span>
#include <sstream>
#include <variant>

#include <fmt/format.h>
#include <nlohmann/json.hpp>

namespace cubby {
struct text_file {
    std::string contents;
};

struct unknown_file {
    std::vector<std::byte> contents;
};

enum class TextBOM {
    None,
    UTF8,
    UTF16LE,
    UTF16BE,
};

TextBOM detect_bom(std::span<std::byte const> data) {
    if (data.size() >= 3 && memcmp(data.data(), "\xEF\xBB\xBF", 3) == 0) {
        return TextBOM::UTF8;
    }
    if (data.size() >= 2 && memcmp(data.data(), "\xFE\xFF", 2) == 0) {
        return TextBOM::UTF16BE;
    }
    if (data.size() >= 2 && memcmp(data.data(), "\xFF\xFE", 2) == 0) {
        return TextBOM::UTF16LE;
    }
    return TextBOM::None;
}

std::variant<std::monostate, text_file, unknown_file> read_text_file(std::filesystem::path path) {
    if (!exists(path)) {
        return {};
    }
    std::ifstream is(path, std::ios::binary);
    if (!is) {
        return {};
    }
    auto cb = file_size(path);
    size_t even_bytesize = (cb % 2) == 0;
    std::vector<std::byte> buf(cb);
    is.read(reinterpret_cast<char *>(buf.data()), cb);

    TextBOM bom = detect_bom(buf);
    if (bom == TextBOM::UTF8) {
        auto p = reinterpret_cast<char const *>(buf.data());
        auto n = cb - 3;
        return text_file{
            .contents = std::string(p + 3, n),
        };
    }

    if (!even_bytesize) {
        auto p = reinterpret_cast<char const *>(buf.data());
        auto n = cb;
        return text_file{
            .contents = std::string(p, n),
        };
    }

    switch (bom) {
    case TextBOM::UTF16BE:
        for (size_t i = 0; i < cb; i += 2) {
            std::swap(buf[i], buf[i + 1]);
        }
        [[fallthrough]];
    case TextBOM::UTF16LE: {
        auto p = reinterpret_cast<char16_t const *>(buf.data());
        auto n = (cb - 2) / 2;
        return text_file{
            .contents = poe::util::to_string(std::u16string_view(p + 1, n)),
        };
    }
    default:
        // Embedded nuls imply it cannot be UTF-8, but common in UTF-16 in the latin-1 range.
        if (std::find(buf.begin(), buf.end(), (std::byte)'\0') != buf.end()) {
            auto p = reinterpret_cast<char16_t const *>(buf.data());
            auto n = cb / 2;
            return text_file{
                .contents = poe::util::to_string(std::u16string_view(p, n)),
            };
        }
        return unknown_file{
            .contents = buf,
        };
    }
}

struct HideoutSAX : nlohmann::json_sax<nlohmann::json> {
    bool null() override { return false; }
    bool boolean(bool) override { return false; }

    template <typename T> bool handle_number(T val) {
        if (path_.empty()) {
            return false;
        }
        auto back = path_.back();
        if (back == Thing::Version) {
            version = val;
        } else if (back == Thing::HideoutHash) {
            proj_.hideout_hash_ = (uint16_t)val;
        } else if (back == Thing::MusicHash) {
            proj_.music_hash_ = (uint16_t)val;
        } else if (back == Thing::DoodadHash) {
            current_object_->hash_ = (uint32_t)val;
        } else if (back == Thing::DoodadX) {
            current_object_->x_ = (uint32_t)val;
        } else if (back == Thing::DoodadY) {
            current_object_->y_ = (uint32_t)val;
        } else if (back == Thing::DoodadR) {
            current_object_->rot_ = (uint16_t)val;
        } else if (back == Thing::DoodadFV) {
            current_object_->flip_ = !!(val & 0x80);
            current_object_->var_ = val & 0x7F;
        } else {
            return false;
        }
        path_.pop_back();
        return true;
    }

    bool number_integer(number_integer_t val) override { return handle_number(val); }

    bool number_unsigned(number_unsigned_t val) override { return handle_number(val); }

    bool number_float(number_float_t, string_t const &) override { return false; }

    bool string(string_t &val) override {
        if (path_.empty()) {
            return false;
        }
        auto back = path_.back();
        if (back == Thing::Language) {
            proj_.language_ = std::move(val);
        } else if (back == Thing::HideoutName) {
            proj_.hideout_name_ = std::move(val);
        } else if (back == Thing::MusicName) {
            proj_.music_name_ = std::move(val);
        } else if (back == Thing::Doodads) {
            current_object_->name_ = std::move(val);
        } else {
            return false;
        }
        path_.pop_back();
        return true;
    }

    bool binary(binary_t &val) override { return false; }

    bool start_object(size_t elements) override {
        if (path_.empty()) {
            return false;
        }
        auto const back = path_.back();
        if (back == Thing::Top) {
        } else if (back == Thing::Doodads) {
            current_object_ = hideout_object{};
        } else if (back == Thing::DoodadObject) {
        } else {
            return false;
        }
        return true;
    }

    bool end_object() override {
        if (path_.empty()) {
            return false;
        }
        auto const back = path_.back();
        if (back == Thing::DoodadObject) {
            path_.pop_back();
            objects_.push_back(*current_object_);
            current_object_.reset();
            current_object_ = hideout_object{};
            return true;
        } else if (back == Thing::Doodads) {
            path_.pop_back();
            proj_.objects_ = std::move(objects_);
            return true;
        } else if (back == Thing::Top) {
            path_.pop_back();
            return true;
        }
        return false;
    }

    bool start_array(size_t elements) override { return false; }
    bool end_array() override { return false; }

    bool key(string_t &key) override {
        if (path_.empty()) {
            return false;
        }
        auto const back = path_.back();
        if (back == Thing::Top) {
            if (key == "version") {
                path_.push_back(Thing::Version);
            } else if (key == "language") {
                path_.push_back(Thing::Language);
            } else if (key == "hideout_name") {
                path_.push_back(Thing::HideoutName);
            } else if (key == "hideout_hash") {
                path_.push_back(Thing::HideoutHash);
            } else if (key == "music_name") {
                path_.push_back(Thing::MusicName);
            } else if (key == "music_hash") {
                path_.push_back(Thing::MusicHash);
            } else if (key == "doodads") {
                path_.push_back(Thing::Doodads);
            } else {
                return false;
            }
        } else if (back == Thing::Doodads) {
            current_object_->name_ = std::move(key);
            path_.push_back(Thing::DoodadObject);
        } else if (back == Thing::DoodadObject) {
            if (key == "hash") {
                path_.push_back(Thing::DoodadHash);
            } else if (key == "x") {
                path_.push_back(Thing::DoodadX);
            } else if (key == "y") {
                path_.push_back(Thing::DoodadY);
            } else if (key == "r") {
                path_.push_back(Thing::DoodadR);
            } else if (key == "fv") {
                path_.push_back(Thing::DoodadFV);
            } else {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    bool parse_error(size_t, std::string const &, nlohmann::detail::exception const &) override { return false; }

    enum class ObjType {};

    enum class Thing {
        Top,
        Version,
        Language,
        HideoutName,
        HideoutHash,
        MusicName,
        MusicHash,
        Doodads,
        DoodadObject,
        DoodadHash,
        DoodadX,
        DoodadY,
        DoodadR,
        DoodadFV,
    };

    std::vector<Thing> path_{Thing::Top};
    std::string current_key_;
    std::optional<hideout_object> current_object_;

    size_t version{};
    project proj_{};
    std::vector<hideout_object> objects_;
};

std::shared_ptr<project> open_hideout(std::filesystem::path p) {
    if (!p.has_extension())
        return {};
    if (p.extension() == ".hideout") {
        enum class parse_step {
            GotNothing,
            GotLanguage,
            GotHideoutName,
            GotHideoutHash,
            GotMusicName,
            GotMusicHash,
            GotObject,
        };
        auto step = parse_step::GotNothing;

        static std::regex const language_re(R"!!(Language = "([^"]*)")!!");
        static std::regex const hideout_name_re(R"!!(Hideout Name = "([^"]*)")!!");
        static std::regex const hideout_hash_re(R"!!(Hideout Hash = (\d+))!!");
        static std::regex const music_name_re(R"!!(Music Name = "([^"]*)")!!");
        static std::regex const music_hash_re(R"!!(Music Hash = (\d+))!!");
        static std::regex const object_re(
            R"!!(([^=]+) = \{ Hash=(\d+), X=(\d+), Y=(\d+), Rot=(\d+), Flip=([01]), Var=(\d+) \})!!");

        auto file = read_text_file(p);

        if (auto *f = std::get_if<std::monostate>(&file)) {
            return {};
        }

        if (auto *f = std::get_if<unknown_file>(&file)) {
            if (f->contents.size() < 2) {
                return {};
            }
            // Check for opening curly for JSON or leading L for "Language"
            if (f->contents[0] == (std::byte)'{' || f->contents[0] == (std::byte)'L') {
                file = text_file{
                    .contents = std::string((char const *)f->contents.data(), f->contents.size()),
                };
            } else {
                return {};
            }
        }

        if (auto *f = std::get_if<text_file>(&file); f && f->contents.size() > 1) {
            if (f->contents.starts_with('{')) {
                HideoutSAX sax;
                if (!nlohmann::json::sax_parse(f->contents, &sax)) {
                    return {};
                }
                sax.proj_.source_path_ = p;
                return std::make_shared<project>(std::move(sax.proj_));
            } else if (f->contents.starts_with("Language")) {
                auto contents = &f->contents;

                project ret{};
                ret.source_path_ = p;

                std::istringstream is(contents->c_str());
                std::string line;
                bool consumed = true;
                while (true) {
                    if (consumed && !std::getline(is, line)) {
                        break;
                    }
                    consumed = true;
                    if (line.empty()) {
                        continue;
                    }
                    std::smatch match;
                    switch (step) {
                    case parse_step::GotNothing: {
                        if (!std::regex_match(line, match, language_re)) {
                            return {};
                        }
                        ret.language_ = match.str(1);
                        step = parse_step::GotLanguage;
                    } break;
                    case parse_step::GotLanguage: {
                        if (!std::regex_match(line, match, hideout_name_re)) {
                            return {};
                        }
                        ret.hideout_name_ = match.str(1);
                        step = parse_step::GotHideoutName;
                    } break;
                    case parse_step::GotHideoutName: {
                        if (!std::regex_match(line, match, hideout_hash_re)) {
                            return {};
                        }
                        ret.hideout_hash_ = (uint16_t)std::stoul(match.str(1));
                        step = parse_step::GotHideoutHash;
                    } break;
                    case parse_step::GotHideoutHash: {
                        if (std::regex_match(line, match, music_name_re)) {
                            ret.music_name_ = match.str(1);
                            step = parse_step::GotMusicName;
                        } else {
                            step = parse_step::GotObject;
                            consumed = false;
                        }
                    } break;
                    case parse_step::GotMusicName: {
                        if (!std::regex_match(line, match, music_hash_re)) {
                            return {};
                        }
                        ret.music_hash_ = (uint16_t)std::stoul(match.str(1));
                        step = parse_step::GotMusicHash;
                    } break;
                    case parse_step::GotMusicHash:
                        [[fallthrough]];
                    case parse_step::GotObject: {
                        if (!std::regex_match(line, match, object_re)) {
                            return {};
                        }
                        hideout_object obj{};
                        obj.name_ = match.str(1);
                        obj.hash_ = std::stoul(match.str(2));
                        obj.x_ = std::stoul(match.str(3));
                        obj.y_ = std::stoul(match.str(4));
                        obj.rot_ = (uint16_t)std::stoul(match.str(5));
                        obj.flip_ = !!std::stoul(match.str(6));
                        obj.var_ = (uint8_t)std::stoul(match.str(7));
                        ret.objects_.push_back(std::move(obj));
                        step = parse_step::GotObject;
                    } break;
                    }
                }
                if (step != parse_step::GotObject) {
                    return {};
                }
                return std::make_shared<project>(std::move(ret));
            }
        }
    } else if (p.extension() == ".cubby") {
    }
    return {};
}

bool export_hideout_legacy_format(project const &p, std::filesystem::path dest) {
    using namespace std::string_view_literals;
    fmt::memory_buffer buf;

    buf.append("\xEF\xBB\xBF"sv);

    fmt::format_to(fmt::appender(buf), R"(Language = "{}"
Hideout Name = "{}"
Hideout Hash = {}
)",
                   p.language_, p.hideout_name_, p.hideout_hash_);

    if (p.music_name_ && p.music_hash_) {
        fmt::format_to(fmt::appender(buf), R"(Music Name = "{}"
Music Hash = {}
)",
                       *p.music_name_, *p.music_hash_);
    }

    buf.append("\n"sv);
    for (auto const &obj : p.objects_) {
        fmt::format_to(fmt::appender(buf), "{} = {{ Hash={}, X={}, Y={}, Rot={}, Flip={}, Var={} }}\n", obj.name_,
                       obj.hash_, obj.x_, obj.y_, obj.rot_, (obj.flip_ ? 1 : 0), obj.var_);
    }

    std::u16string txt = poe::util::to_u16string(std::string_view(buf.data(), buf.size()));

    std::ofstream os(dest, std::ios::binary);
    return !!os.write((char const *)txt.data(), txt.size() * 2);
}

bool export_hideout_json_format(project const &p, std::filesystem::path dest) {
    using namespace std::string_view_literals;
    fmt::memory_buffer buf;

    buf.append("\xEF\xBB\xBF"sv);

    fmt::format_to(fmt::appender(buf), R"({{
  "version": 1,
  "language": "{}",
  "hideout_name": "{}",
  "hideout_hash": {},
)",
              p.language_, p.hideout_name_, p.hideout_hash_);

    if (p.music_name_ && p.music_hash_) {
        fmt::format_to(fmt::appender(buf), R"(  "music_name": "{}",
"music_hash": {},
)",
                  *p.music_name_, *p.music_hash_);
    }

    buf.append(R"(  "doodads": {
)"sv);

    size_t const obj_count = p.objects_.size();
    for (size_t obj_idx = 0; obj_idx < obj_count; ++obj_idx) {
        auto const &obj = p.objects_[obj_idx];
        uint8_t fv = (obj.flip_ ? 0x80 : 0) | obj.var_;
        bool is_last = obj_idx + 1 == obj_count;
        fmt::format_to(fmt::appender(buf), R"(    "{}": {{
      "hash": {},
      "x": {},
      "y": {},
      "r": {},
      "fv": {}
    }}{}
)",
                  obj.name_, obj.hash_, obj.x_, obj.y_, obj.rot_, fv, (is_last ? "" : ","));
    }

    buf.append("  }\n}"sv);

    std::ofstream os(dest, std::ios::binary);
    return !!os.write((char const *)buf.data(), buf.size());
}

std::optional<hideout_object> hideout_object::from_json(nlohmann::json js) {
    // Verify schema
    bool valid_schema = js["name"].is_string() && js["hash"].is_number_unsigned() && js["x"].is_number_unsigned() &&
                        js["y"].is_number_unsigned() && js["r"].is_number_unsigned() && js["fv"].is_number_unsigned();
    if (!valid_schema) {
        return {};
    }

    auto fv = js["fv"].get<uint8_t>();

    // Build result
    return hideout_object{
        .name_ = js["name"],
        .hash_ = js["hash"],
        .x_ = js["x"],
        .y_ = js["y"],
        .rot_ = js["r"],
        .flip_ = !!(fv & 0x80u),
        .var_ = (uint8_t)(fv & 0x7Fu),
    };
}

nlohmann::json hideout_object::to_json() const {
    return {name_,
            {
                {
                    "hash",
                    hash_,
                },
                {
                    "x",
                    x_,
                },
                {
                    "y",
                    y_,
                },
                {
                    "r",
                    rot_,
                },
                {
                    "fv",
                    (flip_ ? 0x80u : 0u) | var_,
                },
            }};
}

} // namespace cubby