#pragma once

#include <memory>
#include <string>
#include <vector>

#include <imgui.h>

namespace cubby {
struct project;
struct TransientProjectState;

struct ToolBase {
    virtual ~ToolBase() = default;
    virtual std::string Label() const = 0;
    virtual std::string Description() const = 0;

    virtual void Activate(cubby::project *project, TransientProjectState *transient_project_state) = 0;
};

struct Toolbox {
    void Run(cubby::project *project, cubby::TransientProjectState *transient_project_state);

    std::vector<std::shared_ptr<ToolBase>> tools;
};

struct Rotate90Tool : ToolBase {
    std::string Label() const override;

    std::string Description() const override;

    void Activate(cubby::project *project, cubby::TransientProjectState *transient_project_state) override;
};

} // namespace cubby