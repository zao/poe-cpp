﻿#include <cubby/ui_toolbox.hpp>

#include <cubby/project.hpp>
#include "math_util.hpp"

#include <glm/glm.hpp>

namespace cubby {
void Toolbox::Run(cubby::project *project, cubby::TransientProjectState *transient_project_state) {
    if (ImGui::Begin("Toolbox", nullptr)) {
        for (auto &tool : tools) {
            if (ImGui::Button(tool->Label().c_str(), {-FLT_MIN, 0})) {
                tool->Activate(project, transient_project_state);
            }
        }
    }
    ImGui::End();
}

std::string Rotate90Tool::Label() const { return (char const*)u8"Rotate 90°"; }

std::string Rotate90Tool::Description() const { return "Rotates the whole selection around its approximate midpoint."; }

void Rotate90Tool::Activate(cubby::project *project, cubby::TransientProjectState *transient_project_state) {
    auto const &selection = transient_project_state->selected_objects;
    if (!selection.empty()) {
        std::vector<glm::vec2> positions;
        for (auto obj_idx : selection) {
            auto const &obj = project->objects_[obj_idx];
            positions.push_back(glm::vec2{(float)obj.x_, (float)obj.y_});
        }
        auto bbox = *aabb_from_points(positions);
        glm::vec2 pivot = glm::round(bbox.c);
        {
            auto sel_I = selection.begin();
            for (auto pos_I = positions.begin(); pos_I != positions.end(); ++pos_I, ++sel_I) {
                auto pos = *pos_I;
                pos -= pivot;
                pos = glm::mat2(0, 1, -1, 0) * pos;
                pos += pivot;
                auto &obj = project->objects_[*sel_I];
                obj.x_ = (uint32_t)pos.x;
                obj.y_ = (uint32_t)pos.y;
                obj.rot_ += 0x4000U;
            }
        }
    }
}
} // namespace cubby
