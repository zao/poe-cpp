#pragma once

#include <poe/io/vfs.hpp>

#include <map>
#include <memory>
#include <string>
#include <unordered_map>
#include <variant>

namespace cubby {
struct ParserBase {
    virtual ~ParserBase() = default;

    virtual std::shared_ptr<void> parse(std::span<std::byte const> data, std::string_view path) const = 0;
};

template <typename T> struct OptionalParser : ParserBase {
    std::shared_ptr<void> parse(std::span<std::byte const> data, std::string_view path) const override {
        if (auto res = parser(data, path)) {
            return std::make_shared<T>(std::move(*res));
        }
        return {};
    }

    std::function<std::optional<T>(std::span<std::byte const>, std::string_view path)> parser;
};
template <typename T> struct SharedParser : ParserBase {
    std::shared_ptr<void> parse(std::span<std::byte const> data, std::string_view path) const override {
        return parser(data, path);
    }

    std::function<std::shared_ptr<T>(std::span<std::byte const>, std::string_view path)> parser;
};

struct ParseCache {
    explicit ParseCache(std::shared_ptr<poe::io::vfs> vfs);

    template <typename T> std::shared_ptr<T> parseFileAs(poe::util::path const &path) {
        return std::static_pointer_cast<T>(innerParseFile(path));
    }

    template <typename T> std::shared_ptr<T> parseFileAs(std::string_view path) {
        return parseFileAs<T>(poe::util::path(path));
    }

    std::shared_ptr<void> parseFile(poe::util::path const &path) { return innerParseFile(path); }

    std::shared_ptr<void> parseFile(std::string_view path) { return parseFile(poe::util::path(path)); }

    std::shared_ptr<poe::io::vfs> innerVfs() const { return vfs_; }

  private:
    std::shared_ptr<void> innerParseFile(poe::util::path const &path);

    std::shared_ptr<poe::io::vfs> vfs_;

    std::unordered_map<std::string, std::unique_ptr<ParserBase>> parsers_;
    std::map<std::string, std::shared_ptr<void>> results_;
};
} // namespace cubby