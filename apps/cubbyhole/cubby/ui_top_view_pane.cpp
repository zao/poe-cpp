#include <cubby/ui_top_view_pane.hpp>

namespace cubby {
void TopViewPane::Run() {
#if 0
        if (ImGui::Begin("Top view", {}, ImGuiWindowFlags_NoScrollbar)) {
            auto size = ImGui::GetContentRegionAvail();
            if (ImGui::Begin("Debug")) {
                ImGui::Text("%f %f", size.x, size.y);
                if (hideout_game_data) {
                    ImGui::Text("Current project row: %zu", hideout_game_data->ho_row_current);
                } else {
                    ImGui::Text("Current project row: None");
                }
            }
            ImGui::End();
            if (!rt_2d || !rt_2d->is_size(size)) {
                rt_2d = std::make_shared<pog::RenderTarget2D>(dx.dev_, dx.d2d_fac_, size);
            }

#if 0
            /* clear and draw 2D view */
            if (ImGui::BeginChild("top_canvas", {-FLT_MIN, -FLT_MIN})) {
                auto draw_list = ImGui::GetWindowDrawList();
                auto c0 = to_glm(ImGui::GetWindowPos());
                auto view_size = to_glm(ImGui::GetWindowSize());
                auto c1 = c0 + view_size;
                glm::vec2 offset = view_size / 2.0f;
                glm::vec2 origin = c0 + offset;
                float line_spacing = 10.0f;

                bool mmb_drag = ImGui::IsMouseDragging(ImGuiMouseButton_Middle);
                bool rmb_drag = ImGui::IsMouseDragging(ImGuiMouseButton_Right);
                if (!top_view.is_interacting()) {
                    auto mods = ImGui::GetMergedKeyModFlags();
                    if (mods == ImGuiKeyModFlags_Alt) {
                        if (mmb_drag && !rmb_drag) {
                            top_view.begin_drag();
                        } else if (!mmb_drag && rmb_drag) {
                            top_view.begin_zoom(origin);
                        }
                    }
                }

                if (top_view.is_interacting()) {
                    auto wpos = ImGui::GetWindowPos();
                    auto mpos = ImGui::GetMousePos();
                    if (auto &di = top_view.drag_info) {
                        if (mmb_drag) {
                            auto delta = ImGui::GetMouseDragDelta(ImGuiMouseButton_Middle);
                            di->offset = {delta.x, -delta.y};
                        } else {
                            top_view.commit_drag();
                        }
                    }
                    if (auto &zi = top_view.zoom_info) {
                        if (rmb_drag) {
                            auto delta = ImGui::GetMouseDragDelta(ImGuiMouseButton_Right);
                            zi->mod = (delta.x + delta.y) / 100.0f;
                        } else {
                            top_view.commit_zoom();
                        }
                    }
                }

                glm::mat4 view_mat = glm::identity<glm::mat4>();
                float constexpr const view_rot = glm::quarter_pi<float>();
                view_mat = glm::translate(view_mat, glm::vec3(top_view.view_shift(), 0.0f));
                view_mat = glm::rotate(view_mat, view_rot, {0, 0, 1.0f});
                view_mat = glm::scale(view_mat, glm::one<glm::vec3>());
                glm::mat4 view_inv = glm::inverse(view_mat);

                glm::mat4 proj_mat = glm::orthoLH(0.0f, .y, 0.0f, 0.0f, 1000.0f);
                glm::mat4 proj_inv = glm::inverse(proj_mat);

                {
                    glm::mat4 world_mat = glm::identity<glm::mat4>();
                    glm::vec3 pos = (proj_mat * view_mat * world_mat) * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
                    draw_list->AddCircleFilled({pos.x, pos.y}, 10.0f, ImColor::HSV(0.3f, 0.3f, 0.3f));
                }

                if (current_project) {
                    for (auto const &obj : current_project->objects_) {
                        std::array<glm::vec4, 4> world_vertices = {
                            glm::vec4{-0.5f, -0.5f, 0.0f, 1.0f},
                            glm::vec4{-0.5f, +0.5f, 0.0f, 1.0f},
                            glm::vec4{+0.5f, +0.5f, 0.0f, 1.0f},
                            glm::vec4{+0.5f, -0.5f, 0.0f, 1.0f},
                        };
                        std::array<ImVec2, 4> out_vertices;
                        float const obj_rot = obj.rot_ / 65536.0f;
                        glm::mat4 world_mat = glm::identity<glm::mat4>();
                        float const obj_scale = 10.0f;
                        world_mat[0][0] = obj_scale * cosf(obj_rot);
                        world_mat[0][1] = obj_scale * sinf(obj_rot);
                        world_mat[1][0] = obj_scale * -sinf(obj_rot);
                        world_mat[1][1] = obj_scale * cosf(obj_rot);
                        world_mat[3][0] = (float)obj.x_;
                        world_mat[3][1] = (float)obj.y_;
                        world_mat = glm::mat3(world_mat);
                        auto m = proj_mat * view_mat * world_mat;
                        for (size_t i = 0; i < 4; ++i) {
                            auto out_vtx = m * world_vertices[i];
                            out_vertices[i] = {out_vtx.x, out_vtx.y};
                        }
                        draw_list->AddPolyline(out_vertices.data(), (int)out_vertices.size(),
                                               ImColor::HSV(0.0f, 0.5f, 0.5f), true, 1.0f);
                    }
                }

                if (ImGui::Begin("Debug")) {
                    ImGui::Text("%f %f - %f %f", c0.x, c0.y, c1.x, c1.y);
                    auto mpos = to_glm(ImGui::GetMousePos());
                    auto munproj = view_inv * proj_inv * glm::vec4(mpos, 0.0f, 1.0f);
                    ImGui::Text("%f %f %f", munproj.x, munproj.y, munproj.z);
                    auto opos = view_inv * proj_inv * glm::vec4(origin.x, origin.y, 0.0f, 1.0f);
                    ImGui::Text("%f %f %f", opos.x, opos.y, opos.z);
                }
                ImGui::End();
            }
            ImGui::EndChild();
#endif
            if (auto *rt = rt_2d->rt_.p) {
                if (ImGui::BeginChild("top_canvas", {-FLT_MIN, -FLT_MIN})) {
                    if (auto proj = current_project) {
                        static std::optional<glm::vec2> sel_start;
                        static std::set<size_t> drag_change_set;
                        static enum class DragChangeType {
                            Add,
                            Toggle,
                            Replace,
                        } drag_change_type;

                        D2D1::Matrix3x2F view_mat = D2D1::Matrix3x2F::Identity();
                        D2D1::Matrix3x2F view_mat_inv;
                        auto mpos = to_glm(ImGui::GetMousePos());
                        auto c0 = to_glm(ImGui::GetWindowPos());
                        auto view_size = to_glm(ImGui::GetWindowSize());
                        auto c1 = c0 + view_size;
                        glm::vec2 offset = view_size / 2.0f;
                        offset.y += 500.0f;
                        glm::vec2 origin = c0 + offset;

                        bool start_inside = sel_start && sel_start->x >= 0.0f && sel_start->x < view_size.x &&
                                            sel_start->y >= 0.0f && sel_start->y < view_size.y;

                        glm::vec2 bbox[2] = {glm::vec2(+FLT_MAX), glm::vec2(-FLT_MAX)};
                        for (auto const &obj : proj->objects_) {
                            glm::vec4 p(obj.x_, obj.y_, 0.0f, 1.0f);
                            glm::mat4 rot45 = glm::rotate(glm::mat4(1), glm::quarter_pi<float>(), {0, 0, 1});
                            p = rot45 * p;
                            bbox[0].x = (std::min)(bbox[0].x, p.x);
                            bbox[0].y = (std::min)(bbox[0].y, p.y);
                            bbox[1].x = (std::max)(bbox[1].x, p.x);
                            bbox[1].y = (std::max)(bbox[1].y, p.y);
                        }

                        HRESULT hr = S_OK;
                        CComPtr<ID2D1SolidColorBrush> obj_brush, selected_obj_brush;
                        hr = rt->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Honeydew), &obj_brush);
                        hr = rt->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Crimson), &selected_obj_brush);

                        CComPtr<ID2D1SolidColorBrush> select_brush, select_fill_brush;
                        hr = rt->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Gray), &select_brush);
                        hr = rt->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::LightGray, 0.2f), &select_fill_brush);

                        CComPtr<ID2D1StrokeStyle> select_stroke;
                        {
                            std::array<float, 2> dashes{5.0f, 2.0f};
                            hr = dx.d2d_fac_->CreateStrokeStyle(D2D1::StrokeStyleProperties(), dashes.data(),
                                                                (uint32_t)dashes.size(), &select_stroke);
                        }

                        rt->BeginDraw();
                        rt->Clear(D2D1::ColorF(0.1f, 0.2f, 0.3f, 1.0f));

                        if (ImGui::Begin("Debug")) {
                            ImGui::Text("bbox gamespace: %f %f; %f %f", bbox[0].x, bbox[0].y, bbox[1].x, bbox[1].y);
                            ImGui::Text("c0, c1: %f %f; %f %f", c0.x, c0.y, c1.x, c1.y);
                            auto mpos = to_glm(ImGui::GetMousePos());
                            ImGui::Text("mpos: %f %f", mpos.x, mpos.y);
                        }
                        ImGui::End();

                        {
                            view_mat = D2D1::Matrix3x2F::Rotation(45.0f) *
                                       D2D1::Matrix3x2F::Translation(offset.x, -offset.y) *
                                       D2D1::Matrix3x2F::Scale(1.0f, -1.0f) * view_mat;
                            view_mat_inv = view_mat;
                            view_mat_inv.Invert();
                        }
                        rt->SetTransform(view_mat);

                        for (size_t obj_idx = 0; obj_idx < proj->objects_.size(); ++obj_idx) {
                            auto const &obj = proj->objects_[obj_idx];
                            auto dec_idx = decoration_idx_by_hash[obj.hash_];
                            auto mesh = decoration_list.items_[dec_idx].mesh_;

                            auto ext = glm::vec2(10.0f, 10.0f);
                            if (mesh) {
                                auto bbox = mesh->bounding_box();
                                ext[0] = bbox.e[0] * 2.0f;
                                ext[1] = bbox.e[1] * 2.0f;
                            }
                            float const w2 = ext.x / 2.0f;
                            float const h2 = ext.y / 2.0f;

                            D2D1::Matrix3x2F obj_mat;
                            {
                                float ang = 360.0f * obj.rot_ / 65536.0f;
                                obj_mat = D2D1::Matrix3x2F::Rotation(ang);
                            }
                            rt->SetTransform(obj_mat * view_mat);

                            auto r = D2D1::RectF(obj.x_ - w2, obj.y_ - h2, obj.x_ + w2, obj.y_ + h2);
                            bool selected = (sel_start && start_inside)
                                                ? transient_project_state->selection_preview.contains(obj_idx)
                                                : transient_project_state->selected_objects.contains(obj_idx);
                            auto *brush = selected ? selected_obj_brush.p : obj_brush.p;

#if 0
                            if (auto m = std::dynamic_pointer_cast<fixed_mesh const>(mesh)) {
                                auto const &fmt = m->fmt_;
                                auto get_triangle_indices =
                                    [&fmt](size_t tri_idx) -> std::tuple<uint32_t, uint32_t, uint32_t> {
                                    if (auto const *v = std::get_if<std::vector<uint32_t>>(&fmt->indices)) {
                                        return {
                                            (*v)[tri_idx * 3 + 0],
                                            (*v)[tri_idx * 3 + 1],
                                            (*v)[tri_idx * 3 + 2],
                                        };
                                    } else if (auto const *v = std::get_if<std::vector<uint16_t>>(&fmt->indices)) {
                                        return {
                                            (*v)[tri_idx * 3 + 0],
                                            (*v)[tri_idx * 3 + 1],
                                            (*v)[tri_idx * 3 + 2],
                                        };
                                    }
                                    return {};
                                };
                                for (size_t shape_idx = 0; shape_idx < fmt->shape_count; ++shape_idx) {
                                    auto const &shape = fmt->shapes[shape_idx];
                                    size_t tri_end = (shape_idx + 1 == fmt->shape_count)
                                                         ? fmt->triangle_count
                                                         : fmt->shapes[shape_idx + 1].triangle_start;
                                    for (size_t tri_idx = shape.triangle_start; tri_idx < tri_end; ++tri_idx) {
                                        auto [i0, i1, i2] = get_triangle_indices(tri_idx);
                                        std::array<poe::format::fmt::Vertex, 3> verts = {
                                            fmt->vertices[i0],
                                            fmt->vertices[i1],
                                            fmt->vertices[i2],
                                        };
                                        if (cross(verts[1].pos - verts[0].pos, verts[2].pos - verts[0].pos).z <= 0) {
                                            for (size_t i = 0; i < 3; ++i) {
                                                auto &a = verts[i];
                                                auto &b = verts[(i + 1) % 3];
                                                rt->DrawLine({obj.x_ + a.pos.x, obj.y_ + a.pos.y},
                                                             {obj.x_ + b.pos.x, obj.y_ + b.pos.y}, brush);
                                            }
                                        }
                                    }
                                }
                            }
#endif

                            rt->DrawRectangle(r, brush);
                            rt->SetTransform(view_mat);
                        }

                        rt->SetTransform(D2D1::IdentityMatrix());

                        if (ImGui::IsMouseDragging(ImGuiMouseButton_Left)) {
                            if (!sel_start) {
                                sel_start = mpos - c0;
                                auto mod_flags = ImGui::GetMergedKeyModFlags();
                                switch (mod_flags) {
                                case ImGuiKeyModFlags_Shift:
                                    drag_change_type = DragChangeType::Add;
                                    break;
                                case ImGuiKeyModFlags_Ctrl:
                                    drag_change_type = DragChangeType::Toggle;
                                    break;
                                default:
                                    drag_change_type = DragChangeType::Replace;
                                }
                            }
                            if (sel_start && start_inside) {
                                glm::vec2 sel_end = mpos - c0;
                                auto dpi_scale = ImGui::GetWindowDpiScale();
                                // auto lmb_delta = to_glm(ImGui::GetMouseDragDelta(ImGuiMouseButton_Left));
                                auto start = *sel_start / dpi_scale;
                                auto end = sel_end / dpi_scale;
                                auto r = D2D1::RectF(start.x, start.y, end.x, end.y);
                                rt->FillRectangle(r, select_fill_brush);
                                rt->DrawRectangle(r, select_brush, 1.0f, select_stroke);

                                AABB2D drag_box = aabb_from_corners(start, end);
                                OBB2D drag_box_game = transform(view_mat_inv, drag_box);
                                drag_change_set.clear();
                                for (size_t obj_idx = 0; obj_idx < proj->objects_.size(); ++obj_idx) {
                                    auto const &obj = proj->objects_[obj_idx];
                                    if (sq_dist_point_obb({(float)obj.x_, (float)obj.y_}, drag_box_game) <= 0.0f) {
                                        drag_change_set.insert(obj_idx);
                                    }
                                }
                                auto &selection = transient_project_state->selection_preview;
                                switch (drag_change_type) {
                                case DragChangeType::Add:
                                    selection = transient_project_state->selected_objects;
                                    for (auto obj_idx : drag_change_set) {
                                        selection.insert(obj_idx);
                                    }
                                    break;
                                case DragChangeType::Toggle:
                                    selection = transient_project_state->selected_objects;
                                    for (auto obj_idx : drag_change_set) {
                                        if (auto I = selection.find(obj_idx); I != selection.end()) {
                                            selection.erase(I);
                                        } else {
                                            selection.insert(obj_idx);
                                        }
                                    }
                                    break;
                                case DragChangeType::Replace:
                                    selection.clear();
                                    selection.insert(drag_change_set.begin(), drag_change_set.end());
                                    break;
                                }
                                drag_change_set.clear();
                            }
                        } else if (sel_start) {
                            if (start_inside) {
                                transient_project_state->selected_objects = transient_project_state->selection_preview;
                                transient_project_state->selection_preview.clear();
                            }
                            sel_start.reset();
                        }

                        hr = rt->EndDraw();
                    }
                }

                ImGui::Image((ImTextureID)(ID3D11ShaderResourceView *)rt_2d->color_srv_, size);
            }
            ImGui::EndChild();
        }
        ImGui::End();
#endif
}
} // namespace cubby
