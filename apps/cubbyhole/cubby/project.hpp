#pragma once

#include <nlohmann/json.hpp>

#include <cstdint>
#include <filesystem>
#include <optional>
#include <set>
#include <string>
#include <vector>

namespace cubby {
struct hideout_object {
    std::string name_;
    uint32_t hash_;
    uint32_t x_;
    uint32_t y_;
    uint16_t rot_;
    bool flip_;
    uint8_t var_;

    static std::optional<hideout_object> from_json(nlohmann::json js);
    nlohmann::json to_json() const;
};

struct project {
    std::filesystem::path source_path_;
    std::string language_;
    std::string hideout_name_;
    uint16_t hideout_hash_;
    std::optional<std::string> music_name_;
    std::optional<uint16_t> music_hash_;

    std::vector<hideout_object> objects_;
};

std::shared_ptr<project> open_hideout(std::filesystem::path p);

struct TransientProjectState {
    std::set<size_t> selected_objects, selection_preview;
};

bool export_hideout_json_format(project const &p, std::filesystem::path dest);
bool export_hideout_legacy_format(project const &p, std::filesystem::path dest);
} // namespace cubby