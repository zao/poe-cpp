#pragma once

#include <poe/format/arm.hpp>
#include <poe/format/dgr.hpp>
#include <poe/format/rs.hpp>
#include <poe/format/tdt.hpp>
#include <poe/format/tgr.hpp>
#include <poe/format/tsi.hpp>
#include <poe/format/tst.hpp>

#include <poe/io/vfs.hpp>
#include <poe/util/path.hpp>

#include <array>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>

namespace cubby::level_gen {
struct TileKey {
    std::string tag;
    std::array<std::optional<std::string>, 4> edgeKeys;
    std::array<std::optional<std::string>, 4> groundKeys;
    std::array<int, 4> edgeOffsets;
    std::array<int, 4> groundOffsets;
};

struct TdtTile {
    TdtTile(std::shared_ptr<poe::io::vfs> vfs, poe::util::path const &path);

    std::string path;
    TileKey key;

    poe::format::tdt::Tdt tdt;
};

struct TileSet {
    TileSet(std::shared_ptr<poe::io::vfs> vfs, poe::util::path const &path);

    std::vector<std::shared_ptr<TdtTile>> tileDefs;
    std::unordered_map<std::string, std::shared_ptr<TdtTile>> defByPath;
    std::unordered_map<std::string, std::set<std::shared_ptr<TdtTile>>> defByTag;

    poe::format::tst::Tst tst;
};

struct Room {
    std::shared_ptr<poe::format::arm::ARMFile> arm;
};

struct RoomSet {
    RoomSet(std::shared_ptr<poe::io::vfs> vfs, poe::util::path const &path);

    std::vector<Room> rooms;

    std::shared_ptr<poe::format::rs::RSFile> rs;
};

struct GraphBase {
    virtual std::string masterFile() const = 0;

    virtual ~GraphBase() = default;
};

struct DGraph : GraphBase {
    explicit DGraph(std::shared_ptr<poe::format::dgr::DGRFile> graph);

    std::string masterFile() const override { return dgr->master_file_; }

    std::shared_ptr<poe::format::dgr::DGRFile> dgr;
};

struct TGraph : GraphBase {
    explicit TGraph(std::shared_ptr<poe::format::tgr::TGRFile> graph);

    std::string masterFile() const override { return tgr->master_file_; }

    std::shared_ptr<poe::format::tgr::TGRFile> tgr;
};

using EdgeCombination = std::shared_ptr<void>;
using EdgePoints = std::shared_ptr<void>;
using Environment = std::shared_ptr<void>;
using FillTiles = std::shared_ptr<void>;
using GroundType = std::shared_ptr<void>;
using MaterialsList = std::shared_ptr<void>;
using MasterFile = std::shared_ptr<poe::format::tsi::TSIFile>;

struct TopologyGraph {
    TopologyGraph(std::shared_ptr<poe::io::vfs> vfs, poe::util::path const &path);

    std::shared_ptr<GraphBase> graph; // .dgr/.tgr
    MasterFile masterFile;            // .tsi
    GroundType outerGroundType;       // .gt
    std::shared_ptr<RoomSet> roomSet; // .rs
    EdgeCombination edgeCombination;  // .ecf
    FillTiles fillTiles;              // .gft
    std::shared_ptr<TileSet> tileSet; // .tst
    MaterialsList materialsList;      // .mtd
    Environment environment;          // str
    EdgePoints edgePoints;            // .edp
};
} // namespace cubby::level_gen