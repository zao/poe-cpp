﻿#include <cubby/ui_camera_view_pane.hpp>

#include <cubby/hideout_meta.hpp>
#include <cubby/project.hpp>
#include <ui_panes.hpp>

#include <poe/format/fmt.hpp>
#include <poe/util/install_location.hpp>

#include <pog/game_camera.hpp>
#include <pog/platform.hpp>

#include <imgui.h>
#include <loguru/loguru.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#include <filesystem>
#include <memory>

#include <d3dcompiler.h>

static float const obj_separation_factor = 125.0f / 11.5f;

namespace cubby {
struct CameraViewPane::Controls {
    struct Twiddle {
        int source = 0;
        int comp = 0;
        bool use_inverse_transpose = true;
    } twiddle;

    struct View {
        float zoom_level = 0.0f;
        int proj_choice = 1;
        int view_choice = 0;
    } view;

    struct Cam {
        glm::vec3 at = glm::vec3(0.0f, 0.0f, 0.0f);
        float distance = 150.0f;
        float azimuth_deg = -135.0f;
        float altitude_deg = 50.31f;
    } cam;
};

struct LoadedShaderProgram {
    CComPtr<ID3D11VertexShader> vs;
    CComPtr<ID3D11PixelShader> ps;
    CComPtr<ID3DBlob> signature;
};

std::optional<LoadedShaderProgram> LoadShaderProgram(pog::DXBits &dx, std::filesystem::path vs_path,
                                                     std::filesystem::path ps_path) {
    HRESULT hr = S_OK;
    CComPtr<ID3DBlob> vs_code, vs_errors, ps_code, ps_errors;
    if (FAILED(hr = D3DCompileFromFile(vs_path.wstring().c_str(), nullptr, nullptr, "vs_main", "vs_5_0", 0, 0, &vs_code,
                                       &vs_errors))) {
        return {};
    }
    if (FAILED(hr = D3DCompileFromFile(ps_path.wstring().c_str(), nullptr, nullptr, "ps_main", "ps_5_0", 0, 0, &ps_code,
                                       &ps_errors))) {
        return {};
    }

    LoadedShaderProgram ret{
        .signature = vs_code,
    };
    if (FAILED(hr = dx.dev_->CreateVertexShader(vs_code->GetBufferPointer(), vs_code->GetBufferSize(), nullptr,
                                                &ret.vs))) {
        return {};
    }
    if (FAILED(
            hr = dx.dev_->CreatePixelShader(ps_code->GetBufferPointer(), ps_code->GetBufferSize(), nullptr, &ret.ps))) {
        return {};
    }

    return ret;
};

struct FixedMeshD3D {
    CComPtr<ID3D11Buffer> vertex_buf;
    CComPtr<ID3D11Buffer> index_buf;
    DXGI_FORMAT index_fmt;
};

std::optional<FixedMeshD3D> LoadFixedMeshD3D(pog::DXBits &dx, CubbyDecorationVariant const &var) {
    FixedMeshD3D ret;
    auto fm = std::dynamic_pointer_cast<fixed_mesh>(var.mesh_);
    if (!fm || !fm->fmt_ || fm->fmt_->dolm_.meshes.empty()) {
        return {};
    }
    auto &fmt = *fm->fmt_;
    auto mesh = fmt.Mesh(0);

    D3D11_BUFFER_DESC vertex_bd{
        .ByteWidth = (UINT)mesh->vertexData.size(),
        .Usage = D3D11_USAGE_IMMUTABLE,
        .BindFlags = D3D11_BIND_VERTEX_BUFFER,
    };
    D3D11_SUBRESOURCE_DATA vertex_srd{
        .pSysMem = mesh->vertexData.data(),
    };

    if (FAILED(dx.dev_->CreateBuffer(&vertex_bd, &vertex_srd, &ret.vertex_buf))) {
        return {};
    }

    size_t index_bytewidth = fmt.index_width(0);
    ret.index_fmt = index_bytewidth == 2 ? DXGI_FORMAT_R16_UINT : DXGI_FORMAT_R32_UINT;
    D3D11_BUFFER_DESC index_bd{
        .ByteWidth = (UINT)mesh->indexData.size(),
        .Usage = D3D11_USAGE_IMMUTABLE,
        .BindFlags = D3D11_BIND_INDEX_BUFFER,
        .CPUAccessFlags{},
        .MiscFlags{},
        .StructureByteStride{},
    };
    D3D11_SUBRESOURCE_DATA index_srd{
        .pSysMem = mesh->indexData.data(),
    };

    if (FAILED(dx.dev_->CreateBuffer(&index_bd, &index_srd, &ret.index_buf))) {
        return {};
    }

    return ret;
};

struct CameraViewPane::Resources {
    std::shared_ptr<pog::RenderTarget> rt_3d;
    std::optional<LoadedShaderProgram> simple_fixed_prog;
    CComPtr<ID3D11InputLayout> simple_fixed_il;

    std::map<std::pair<size_t, size_t>, FixedMeshD3D> d3d_fixed_meshes;
};

CameraViewPane::CameraViewPane(pog::DXBits &dx, std::shared_ptr<HideoutMeta> &hideoutMeta)
    : dx_(dx), meta_(hideoutMeta), controls_(std::make_shared<Controls>()) {
    auto exe_dir = poe::util::executable_dir();

    resources_ = std::make_shared<Resources>();

    resources_->simple_fixed_prog =
        LoadShaderProgram(dx, *exe_dir / "simple_fixed.hlsl", *exe_dir / "simple_fixed.hlsl");
    if (!resources_->simple_fixed_prog) {
        LOG_F(FATAL, "Could not load simple_fixed shader programs");
    }

    {
        auto &sig = resources_->simple_fixed_prog->signature;
        std::vector<D3D11_INPUT_ELEMENT_DESC> ieds{
            D3D11_INPUT_ELEMENT_DESC{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,
                                     offsetof(poe::format::fmt::Vertex, pos), D3D11_INPUT_PER_VERTEX_DATA, 0},
            D3D11_INPUT_ELEMENT_DESC{"TEXCOORD", 0, DXGI_FORMAT_R16G16_FLOAT, 0, offsetof(poe::format::fmt::Vertex, uv),
                                     D3D11_INPUT_PER_VERTEX_DATA, 0},
        };
        for (size_t i = 0; i < 1; ++i) {
            ieds.push_back({"HFLOAT", (UINT)i, DXGI_FORMAT_R16G16B16A16_FLOAT, 0,
                            (UINT)offsetof(poe::format::fmt::Vertex, unk_0h) + (UINT)i * 8, D3D11_INPUT_PER_VERTEX_DATA,
                            0});
        }
        for (size_t i = 0; i < 2; ++i) {
            ieds.push_back({"BSNORM", (UINT)i, DXGI_FORMAT_R8G8B8A8_SNORM, 0,
                            (UINT)offsetof(poe::format::fmt::Vertex, unk_0h) + (UINT)i * 4, D3D11_INPUT_PER_VERTEX_DATA,
                            0});
            ieds.push_back({"BUNORM", (UINT)i, DXGI_FORMAT_R8G8B8A8_UNORM, 0,
                            (UINT)offsetof(poe::format::fmt::Vertex, unk_0h) + (UINT)i * 4, D3D11_INPUT_PER_VERTEX_DATA,
                            0});
        }
        CComPtr<ID3D11InputLayout> simple_fixed_il;
        if (FAILED(dx_.dev_->CreateInputLayout(ieds.data(), (UINT)ieds.size(), sig->GetBufferPointer(),
                                               sig->GetBufferSize(), &simple_fixed_il))) {
            LOG_F(FATAL, "Could not create input layout");
        }
        resources_->simple_fixed_il = simple_fixed_il;
    }
}

void CameraViewPane::Run(std::shared_ptr<cubby::project> nextProject) {
    if (nextProject) {
        project_ = nextProject;
        // Find bounding box for view centering
        if (project_ && !project_->objects_.empty()) {
            glm::vec2 LeftY{FLT_MAX, FLT_MAX}, RightY{-FLT_MAX, -FLT_MAX};
            for (auto &obj : project_->objects_) {
                auto pos = glm::vec2{obj.x_, obj.y_};
                LeftY = glm::min(LeftY, pos);
                RightY = glm::max(RightY, pos);
            }
            controls_->cam.at = glm::vec3{(RightY + LeftY) / 2.0f, 0.0f};
        }
    }
    if (meta_ && !initialized_) {
        for (size_t cdi_idx = 0; cdi_idx < meta_->all_decorations.size(); ++cdi_idx) {
            auto const &cdi = meta_->all_decorations[cdi_idx];
            for (size_t var_idx = 0; var_idx < cdi.variant_count(); ++var_idx) {
                if (auto m = LoadFixedMeshD3D(dx_, *cdi.variant(var_idx))) {
                    resources_->d3d_fixed_meshes[{cdi_idx, var_idx}] = std::move(*m);
                }
            }
        }
        initialized_ = true;
    }
    if (ImGui::Begin("3D controls")) {
        ImGui::SliderInt("Source##twiddle-source", &controls_->twiddle.source, 0, 4);
        ImGui::SliderInt("Component##twiddle-comp", &controls_->twiddle.comp, 0, 3);
        ImGui::DragFloat3("Camera focus##twiddle-cam_at", (float *)&controls_->cam.at);
        ImGui::DragFloat("Camera distance##twiddle-cam_dist", &controls_->cam.distance, 1.0f, 0.01f, 1500.0f, "%.3f",
                         ImGuiSliderFlags_AlwaysClamp | ImGuiSliderFlags_Logarithmic);
        ImGui::DragFloat("Camera azimuth##twiddle-cam_azimuth", &controls_->cam.azimuth_deg, 0.1f, -FLT_MAX, FLT_MAX,
                         (char const *)u8"%0.1f°");
        ImGui::DragFloat("Camera altitude##twiddle-cam_altitude", &controls_->cam.altitude_deg, 0.1f, -89.0f, 89.0f,
                         (char const *)u8"%0.2f°", ImGuiSliderFlags_AlwaysClamp);
        ImGui::Checkbox("Use inverse-transpose##twiddle-invtranspose", &controls_->twiddle.use_inverse_transpose);

        ImGui::SliderFloat("zoom", &controls_->view.zoom_level, 0, (float)pog::gameCameraParameters.size());

        ImGui::RadioButton("P(our)", &controls_->view.proj_choice, 0);
        ImGui::SameLine();
        ImGui::RadioButton("P(oct)", &controls_->view.proj_choice, 1);
        ImGui::RadioButton("V(our)", &controls_->view.view_choice, 0);
        ImGui::SameLine();
        ImGui::RadioButton("V(oct)", &controls_->view.view_choice, 1);
    }
    ImGui::End();

    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0, 0});
    if (ImGui::Begin("3D view", {}, ImGuiWindowFlags_NoScrollbar)) {
        auto &rt_3d = resources_->rt_3d;
        auto size = ImGui::GetContentRegionAvail();
        if (ImGui::Begin("Debug")) {
            ImGui::Text("%f %f", size.x, size.y);
        }
        ImGui::End();
        if (!rt_3d || !rt_3d->is_size(size)) {
            rt_3d = std::make_shared<pog::RenderTarget>(dx_.dev_, size);
        }

        /* clear and draw 3D view */
        {
            auto *rt = rt_3d.get();
            auto *ctx = dx_.ctx_.p;
            float clear_color[4] = {0.2f, 0.4f, 0.6f, 1.0f};
            ctx->ClearRenderTargetView(rt->color_rtv_, clear_color);
            ctx->ClearDepthStencilView(rt->depth_dsv_, D3D11_CLEAR_DEPTH, 1.0f, 0);

            ctx->OMSetDepthStencilState(rt->depth_dss_, 0);
            ctx->OMSetRenderTargets(1, &rt->color_rtv_.p, rt->depth_dsv_);

            D3D11_VIEWPORT viewport{
                .TopLeftX = 0.0f,
                .TopLeftY = 0.0f,
                .Width = size.x,
                .Height = size.y,
                .MinDepth = 0.0f,
                .MaxDepth = 1.0f,
            };
            ctx->RSSetViewports(1, &viewport);
            ctx->RSSetState(rt->raster_state_);

            struct TwiddleCB {
                int source, component;
                int use_inverse_transpose;
                int pad[1];
            } twiddle_cdata{
                .source = controls_->twiddle.source,
                .component = controls_->twiddle.comp,
                .use_inverse_transpose = controls_->twiddle.use_inverse_transpose,
            };

            CComPtr<ID3D11Buffer> twiddle_cb;
            {
                D3D11_BUFFER_DESC twiddle_bd{
                    .ByteWidth = sizeof(TwiddleCB),
                    .Usage = D3D11_USAGE_DEFAULT,
                    .BindFlags = D3D11_BIND_CONSTANT_BUFFER,
                };
                dx_.dev_->CreateBuffer(&twiddle_bd, nullptr, &twiddle_cb);
            }

            ctx->UpdateSubresource(twiddle_cb, 0, nullptr, &twiddle_cdata, 0, 0);

            ctx->IASetInputLayout(resources_->simple_fixed_il);
            ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

            ctx->VSSetShader(resources_->simple_fixed_prog->vs, nullptr, 0);
            ctx->PSSetShader(resources_->simple_fixed_prog->ps, nullptr, 0);

            size_t constexpr const MAX_NO_POINT_LIGHTS_IN_FRAME = 1;
            struct PassCB {
                glm::vec4 light_positions[MAX_NO_POINT_LIGHTS_IN_FRAME];  // With Theta
                glm::vec4 light_directions[MAX_NO_POINT_LIGHTS_IN_FRAME]; // With Phi
                glm::vec4 light_colors[MAX_NO_POINT_LIGHTS_IN_FRAME];     // With radius
                glm::mat4 light_matrices[MAX_NO_POINT_LIGHTS_IN_FRAME];
                glm::vec4 light_types[MAX_NO_POINT_LIGHTS_IN_FRAME];
                glm::mat4 view_projection_transform;
            };

            glm::mat4 projection_transform(1.0f);
            glm::mat4 view_transform(1.0f);

            {
                std::vector<glm::mat4> Ks;
                std::transform(pog::gameCameraParameters.begin(), pog::gameCameraParameters.end(),
                               std::back_inserter(Ks), [](auto gcp) { return gcp.projection_transform; });

                std::vector<glm::mat4> Rs;
                std::transform(pog::gameCameraParameters.begin(), pog::gameCameraParameters.end(),
                               std::back_inserter(Rs), [](auto gcp) { return gcp.R; });

                std::vector<glm::vec4> ts;
                std::transform(pog::gameCameraParameters.begin(), pog::gameCameraParameters.end(),
                               std::back_inserter(ts), [](auto gcp) { return gcp.tScaled; });

                auto interp = [](auto &arr, auto f) -> auto {
                    if (f <= 0) {
                        return arr.front();
                    }
                    if (f >= arr.size() - 1) {
                        return arr.back();
                    }
                    auto &A = arr[(int)f];
                    auto &B = arr[1 + (int)f];
                    auto v = fmodf(f, 1.0f);
                    return (1.0f - v) * A + v * B;
                };

                if (controls_->view.proj_choice == 0) {
                    projection_transform =
                        glm::perspectiveFovLH_ZO(glm::radians(75.0f), size.x, size.y, 0.1f, 10000.0f);
                } else if (controls_->view.proj_choice == 1) {
                    projection_transform = interp(Ks, controls_->view.zoom_level);
                    projection_transform[0][0] = projection_transform[1][1] * size.y / size.x;
                }

                auto &cam = controls_->cam;
                if (controls_->view.view_choice == 0) {
                    glm::vec3 shift_at = cam.at * obj_separation_factor;
                    glm::vec3 cam_eye =
                        shift_at +
                        (glm::vec3)(glm::rotate(glm::rotate(glm::identity<glm::mat4>(), glm::radians(cam.azimuth_deg),
                                                            glm::vec3{0.0f, 0.0f, 1.0f}),
                                                glm::radians(cam.altitude_deg), glm::vec3{0.0f, 1.0f, 0.0f}) *
                                    glm::vec4{cam.distance * obj_separation_factor, 0.0f, 0.0f, 1.0f});

                    view_transform = glm::lookAtLH(cam_eye, shift_at, glm::vec3{0, 0, -1});
                }
                if (controls_->view.view_choice == 1) {
                    glm::mat4 R = interp(Rs, controls_->view.zoom_level);
                    glm::vec4 tScaled = interp(ts, controls_->view.zoom_level);

                    view_transform = glm::mat4(R[0], R[1], R[2], tScaled);
                }
            }

            if (ImGui::Begin("Debug")) {
                ImGui::Text("choice: %d %d", controls_->view.proj_choice, controls_->view.view_choice);
                ImGui::Text("proj: %s\nview: %s", glm::to_string(projection_transform).c_str(),
                            glm::to_string(view_transform).c_str());
            }
            ImGui::End();

            if (initialized_ && project_) {
                PassCB pass_cdata{
                    .view_projection_transform = glm::transpose(projection_transform * view_transform),
                };

                struct PipeCB {
                    glm::vec4 padding;
                };

                PipeCB pipe_cdata{};

                struct ObjectCB {
                    glm::mat4 world_transform;
                };

                CComPtr<ID3D11Buffer> pass_cb, pipe_cb;
                auto make_constant_buffer = [&dev = dx_.dev_](auto const &data) {
                    CComPtr<ID3D11Buffer> cb;
                    D3D11_BUFFER_DESC bd{
                        .ByteWidth = sizeof(data),
                        .Usage = D3D11_USAGE_DYNAMIC,
                        .BindFlags = D3D11_BIND_CONSTANT_BUFFER,
                        .CPUAccessFlags = D3D11_CPU_ACCESS_WRITE,
                    };
                    D3D11_SUBRESOURCE_DATA srd{.pSysMem = &data};
                    dev->CreateBuffer(&bd, &srd, &cb);
                    return cb;
                };

                pass_cb = make_constant_buffer(pass_cdata);
                pipe_cb = make_constant_buffer(pipe_cdata);

                std::array<ID3D11Buffer *, 4> cbs{
                    pass_cb,
                    pipe_cb,
                };

                ctx->VSSetConstantBuffers(0, (UINT)cbs.size(), cbs.data());
                ctx->VSSetConstantBuffers(3, 1, &twiddle_cb.p);

                auto &all_decorations = meta_->all_decorations;
                auto &decoration_idx_by_hash = meta_->decoration_idx_by_hash;
                for (auto const &obj : project_->objects_) {
                    if (!decoration_idx_by_hash.contains(obj.hash_)) {
                        continue;
                    }
                    auto obj_var = obj.var_;
                    size_t dec_idx = decoration_idx_by_hash[obj.hash_];
                    auto cdi = all_decorations[dec_idx];
                    auto fm = std::dynamic_pointer_cast<fixed_mesh const>(cdi.mesh(obj_var));
                    if (!fm) {
                        auto I = std::find_if(all_decorations.begin(), all_decorations.end(),
                                              [](auto &cdi) { return strstr(cdi.name_.c_str(), "Crate"); });
                        dec_idx = std::distance(all_decorations.begin(), I);
                        cdi = all_decorations[dec_idx];
                        obj_var = 0;
                        fm = std::dynamic_pointer_cast<fixed_mesh const>(cdi.mesh(obj_var));
                    }
                    if (fm) {
                        auto fmt = fm->fmt_;
                        auto sub = fmt->Mesh(0);
                        auto mesh = resources_->d3d_fixed_meshes[{dec_idx, obj_var}];
                        glm::mat4 world_transform;
                        {
                            auto obj_sep = obj_separation_factor;
                            glm::vec3 obj_pos{(float)obj.x_ * obj_sep, (float)obj.y_ * obj_sep, 0.0f};
                            float obj_rot = glm::two_pi<float>() * obj.rot_ / 65536.0f;
                            glm::vec3 obj_rot_axis{0.0f, 0.0f, 1.0f};
                            glm::vec3 obj_flip_x{obj.flip_ ? -1.0f : 1.0f, 1.0f, 1.0f};
                            world_transform = glm::scale(
                                glm::rotate(glm::translate(glm::identity<glm::mat4>(), obj_pos), obj_rot, obj_rot_axis),
                                obj_flip_x);
                        }
                        ObjectCB object_cdata{
                            .world_transform = glm::transpose(world_transform),

                        };
                        CComPtr<ID3D11Buffer> object_cb = make_constant_buffer(object_cdata);
                        ctx->VSSetConstantBuffers(2, 1, &object_cb.p);

                        UINT vertex_strides[1] = {sizeof(poe::format::fmt::Vertex)};
                        UINT vertex_offsets[1] = {0};
                        ctx->IASetVertexBuffers(0, 1, &mesh.vertex_buf.p, vertex_strides, vertex_offsets);
                        ctx->IASetIndexBuffer(mesh.index_buf, mesh.index_fmt, 0);

                        for (size_t shape_idx = 0; shape_idx < fmt->shape_count; ++shape_idx) {
                            auto const &shape = sub->shapeExtents[shape_idx];
                            size_t this_idx = shape.indexStart;
                            size_t index_count = shape.indexCount;
                            ctx->DrawIndexed((UINT)index_count, (UINT)this_idx, 0);
                        }
                    }
                }

                cbs = {};
                ctx->VSSetConstantBuffers(0, (UINT)cbs.size(), cbs.data());
            }

            ctx->OMSetRenderTargets(0, nullptr, nullptr);
        }

        ImGui::Image((ImTextureID)(ID3D11ShaderResourceView *)rt_3d->color_srv_, size);
    }
    ImGui::End();
    ImGui::PopStyleVar();
}
} // namespace cubby