#pragma once

#include <memory>

namespace pog {
struct DXBits;
struct RenderTarget;
}

namespace cubby {
struct project;
struct HideoutMeta;

struct CameraViewPane {
    explicit CameraViewPane(pog::DXBits &dx, std::shared_ptr<HideoutMeta> &hideoutMeta);
    void Run(std::shared_ptr<cubby::project> nextProject);

    bool initialized_ = false;

    pog::DXBits &dx_;
    std::shared_ptr<HideoutMeta> &meta_;

    struct Controls;
    std::shared_ptr<Controls> controls_;

    struct Resources;
    std::shared_ptr<Resources> resources_;

    std::shared_ptr<cubby::project> project_;
};
} // namespace cubby