#include "parse_cache.hpp"

#include "poe/format/ao.hpp"
#include "poe/format/arm.hpp"
#include "poe/format/dat.hpp"
#include "poe/format/fmt.hpp"
#include "poe/format/rs.hpp"
#include "poe/format/sm.hpp"
#include "poe/format/tdt.hpp"
#include "poe/format/tgm.hpp"
#include "poe/format/tgt.hpp"
#include "poe/format/tsi.hpp"
#include "poe/format/tst.hpp"

using namespace std::string_view_literals;

#define REGISTER_OPTIONAL_PARSER(Ext, Type, Fun)                                                                       \
    {                                                                                                                  \
        auto p = std::make_unique<OptionalParser<Type>>();                                                             \
        p->parser = Fun;                                                                                               \
        parsers_["." #Ext] = std::move(p);                                                                             \
    }

#define REGISTER_OPTIONAL_PARSER_NO_PATH(Ext, Type, Fun)                                                               \
    {                                                                                                                  \
        auto p = std::make_unique<OptionalParser<Type>>();                                                             \
        p->parser = [fun = Fun](std::span<std::byte const> data, std::string_view path) { return fun(data); };         \
        parsers_["." #Ext] = std::move(p);                                                                             \
    }

#define REGISTER_SHARED_PARSER(Ext, Type, Fun)                                                                         \
    {                                                                                                                  \
        auto p = std::make_unique<SharedParser<Type>>();                                                               \
        p->parser = Fun;                                                                                               \
        parsers_["." #Ext] = std::move(p);                                                                             \
    }

#define REGISTER_SHARED_PARSER_NO_PATH(Ext, Type, Fun)                                                                 \
    {                                                                                                                  \
        auto p = std::make_unique<SharedParser<Type>>();                                                               \
        p->parser = [fun = Fun](std::span<std::byte const> data, std::string_view path) { return fun(data); };         \
        parsers_["." #Ext] = std::move(p);                                                                             \
    }

namespace {
std::shared_ptr<poe::format::dat::dat_table> parse_dat_shim(std::span<std::byte const> data, std::string_view path) {
    auto poePath = poe::util::path(path);
    auto filename = poePath.leaf();
    return poe::format::dat::open_dat(data, filename);
}
} // namespace

namespace cubby {
ParseCache::ParseCache(std::shared_ptr<poe::io::vfs> vfs) : vfs_(vfs) {
    REGISTER_SHARED_PARSER_NO_PATH(ao, poe::format::ao::ao_parse, poe::format::ao::parse_ao);
    REGISTER_SHARED_PARSER_NO_PATH(aoc, poe::format::ao::ao_parse, poe::format::ao::parse_ao);
    REGISTER_SHARED_PARSER_NO_PATH(arm, poe::format::arm::ARMFile, poe::format::arm::parse_arm);
    REGISTER_SHARED_PARSER(dat, poe::format::dat::dat_table, parse_dat_shim);
    REGISTER_SHARED_PARSER_NO_PATH(fmt, poe::format::fmt::FMT, poe::format::fmt::parse_fmt);
    REGISTER_SHARED_PARSER_NO_PATH(rs, poe::format::rs::RSFile, poe::format::rs::parse_rs);
    REGISTER_SHARED_PARSER_NO_PATH(sm, poe::format::sm::SM, poe::format::sm::parse_sm);
    REGISTER_OPTIONAL_PARSER_NO_PATH(tdt, poe::format::tdt::Tdt, poe::format::tdt::parse_tdt);
    REGISTER_SHARED_PARSER_NO_PATH(tgm, poe::format::tgm::TgmFile, poe::format::tgm::parse_tgm);
    REGISTER_OPTIONAL_PARSER_NO_PATH(tgt, poe::format::tgt::Tgt, poe::format::tgt::parse_tgt);
    REGISTER_SHARED_PARSER_NO_PATH(tsi, poe::format::tsi::TSIFile, poe::format::tsi::parse_tsi);
    REGISTER_OPTIONAL_PARSER_NO_PATH(tst, poe::format::tst::Tst, poe::format::tst::parse_tst);
}

std::shared_ptr<void> ParseCache::innerParseFile(poe::util::path const &path) {
    if (auto I = results_.find(path.path_); I != results_.end()) {
        return I->second;
    }

    auto fh = vfs_->open_file(path);
    if (!fh) {
        return {};
    }

    auto ext = std::string(path.extension());
    if (auto parserI = parsers_.find(ext); parserI != parsers_.end()) {
        auto ptr = parserI->second->parse(fh->read_all(), path.path_);
        results_.emplace(path.path_, ptr);
        return ptr;
    }
    return {};
}

} // namespace cubby