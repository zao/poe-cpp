#include "ui_panes.hpp"

#include <cubby/hideout_meta.hpp>

#include <imgui.h>
#include <imgui_stdlib.h>

#include <glm/glm.hpp>

std::optional<CubbyDecorationVariant> cubby_decoration_item::variant(size_t var_idx) const {
    if (var_idx < variants_.size()) {
        return variants_[var_idx];
    }
    return {};
}

size_t cubby_decoration_item::variant_count() const { return variants_.size(); }

std::shared_ptr<poe::format::ao::ao_parse> cubby_decoration_item::ao(size_t var_idx) const {
    if (var_idx < variants_.size()) {
        return variants_[var_idx].ao_;
    }
    return {};
}

std::shared_ptr<poe::format::ao::ao_parse> cubby_decoration_item::aoc(size_t var_idx) const {
    if (var_idx < variants_.size()) {
        return variants_[var_idx].aoc_;
    }
    return {};
}

std::shared_ptr<mesh_interface> cubby_decoration_item::mesh(size_t var_idx) const {
    if (var_idx < variants_.size()) {
        return variants_[var_idx].mesh_;
    }
    return {};
}

std::shared_ptr<std::string> cubby_decoration_item::mesh_fault(size_t var_idx) const {
    if (var_idx < variants_.size()) {
        return variants_[var_idx].mesh_fault_;
    }
    return {};
}

std::shared_ptr<std::string> cubby_decoration_item::mesh_path(size_t var_idx) const {
    if (var_idx < variants_.size()) {
        return variants_[var_idx].mesh_path_;
    }
    return {};
}

imgui_project_info::imgui_project_info(std::shared_ptr<cubby::project> &proj,
                                       std::shared_ptr<cubby::HideoutMeta> &hideout_meta)
    : proj_(proj), hideout_meta_(hideout_meta) {}

void imgui_project_info::run() {
    if (hideout_meta_ && !initialized_) {
        initialized_ = true;
    }
    if (ImGui::Begin("Project Information")) {
        if (!proj_) {
            ImGui::TextUnformatted("No project loaded.");
        } else if (!initialized_) {
            ImGui::TextUnformatted("Loading...");
        } else {
            ImGui::Text("Language: %s", proj_->language_.c_str());
            ImGui::Text("Hideout Name: %s", proj_->hideout_name_.c_str());
            ImGui::Text("Hideout Hash: %u", proj_->hideout_hash_);
            ImGui::Text("Music Name: %s", proj_->music_name_ ? proj_->music_name_->c_str() : "None");
            ImGui::Text("Music Hash: %s", proj_->music_hash_ ? std::to_string(*proj_->music_hash_).c_str() : "None");
            for (auto &obj : proj_->objects_) {
                ImGui::Text("%s at (%u,%u), rotated %u, %s, variant %u", obj.name_.c_str(), obj.x_, obj.y_, obj.rot_,
                            obj.flip_ ? "flipped" : "not flipped", obj.var_);
            }
        }
    }
    ImGui::End();
}

cubby_decoration_list::cubby_decoration_list(std::shared_ptr<cubby::HideoutMeta> &hideout_meta)
    : hideout_meta_(hideout_meta) {}

void cubby_decoration_list::run() {
    if (hideout_meta_ && !initialized_) {
        items_ = hideout_meta_->all_decorations;
        selected_item_ = 2;
        initialized_ = true;
    }
    if (ImGui::Begin("Available decorations")) {
        if (!initialized_) {
            ImGui::TextUnformatted("Loading...");
        } else {
            name_filter_.Draw();
            for (size_t item_idx = 0; item_idx < items_.size(); ++item_idx) {
                auto const &item = items_[item_idx];
                if (name_filter_.PassFilter(item.name_.c_str())) {
                    std::string radio_name = fmt::format("##radio-{}", item_idx);
                    if (ImGui::RadioButton(radio_name.c_str(), selected_item_ && *selected_item_ == item_idx)) {
                        selected_item_ = item_idx;
                    }
                    ImGui::SameLine();
                    ImVec2 art_box{64.0f, 64.0f};
                    if (item.tex_2dart_) {
                        if (item.cell_extent_.x >= item.cell_extent_.y) {
                            art_box.y /= (item.cell_extent_.x / item.cell_extent_.y);
                        } else {
                            art_box.x /= (item.cell_extent_.y / item.cell_extent_.x);
                        }
                        ImGui::Image((ImTextureID)(ID3D11ShaderResourceView *)item.tex_2dart_->srv_, art_box, {0, 0},
                                     {1, 1}, {1, 1, 1, 1}, {0.5f, 0.5f, 0.5f, 1.0f});
                        ImGui::SameLine();
                    }
                    ImGui::BulletText("%s (%.0fx%.0f) (\"%s\")", item.name_.c_str(), item.cell_extent_.x,
                                      item.cell_extent_.y, item.id_.c_str());
                }
            }
        }
    }
    ImGui::End();
}

AABB fixed_mesh::bounding_box() const {
    if (!fmt_) {
        return AABB{
            .c = glm::vec3(0),
            .e = glm::vec3(0),
        };
    }

    auto const &src = fmt_->bbox_;
    glm::vec3 c0{src[0], src[2], src[4]};
    glm::vec3 c1{src[1], src[3], src[5]};
    return AABB{.c = (c0 + c1) / 2.0f, .e = glm::abs(c1 - c0) / 2.0f};
}

AABB skinned_mesh::bounding_box() const {
    if (!sm_->bbox_) {
        return AABB{
            .c = glm::vec3(0),
            .e = glm::vec3(0),
        };
    }

    auto const &src = *sm_->bbox_;
    glm::vec3 c0{src[0], src[1], src[2]};
    glm::vec3 c1{src[3], src[4], src[5]};
    return AABB{.c = (c0 + c1) / 2.0f, .e = glm::abs(c1 - c0) / 2.0f};
}
