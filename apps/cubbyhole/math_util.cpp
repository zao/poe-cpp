#include "math_util.hpp"

AABB2D aabb_from_corners(glm::vec2 a, glm::vec2 b) {
    AABB2D ret;
    ret.c = (a + b) / 2.0f;
    ret.e = glm::vec2{(std::max)(a.x, b.x), (std::max)(a.y, b.y)} - ret.c;
    return ret;
}

std::optional<AABB2D> aabb_from_points(std::span<glm::vec2 const> points) {
    if (points.empty()) {
        return {};
    }
    AABB2D ret;
    glm::vec2 a = points[0];
    glm::vec2 b = a;
    for (glm::vec2 p : points.subspan(1)) {
        a = glm::min(a, p);
        b = glm::max(b, p);
    }
    ret.c = (a + b) / 2.0f;
    ret.e = glm::vec2{(std::max)(a.x, b.x), (std::max)(a.y, b.y)} - ret.c;
    return ret;
}

float sq_dist_point_obb(glm::vec2 p, OBB2D b) {
    glm::vec2 v = p - b.c;
    float sq_dist = 0.0f;
    for (int i = 0; i < 2; ++i) {
        float d = glm::dot(v, b.u[i]), excess = 0.0f;
        if (d < -b.e[i]) {
            excess = d + b.e[i];
        } else if (d > b.e[i]) {
            excess = d - b.e[i];
        }
        sq_dist += excess * excess;
    }

    return sq_dist;
}

OBB2D transform(D2D1::Matrix3x2F const &m, AABB2D bbox) {
    OBB2D inbox = {
        .c = bbox.c,
        .u = {glm::vec2{1.0f, 0.0f}, glm::vec2{0.0f, 1.0f}},
        .e = bbox.e,
    };

    return transform(m, inbox);
}

OBB2D transform(D2D1::Matrix3x2F const &m, OBB2D bbox) {
    OBB2D ret;

    // Transform center as-is
    ret.c = to_glm(m.TransformPoint(to_d2d(bbox.c)));

    D2D1::Matrix3x2F no_trans = m;
    no_trans.dx = 0;
    no_trans.dy = 0;

    // Transform unit vectors without translation
    ret.u[0] = to_glm(no_trans.TransformPoint(to_d2d(bbox.u[0])));
    ret.u[1] = to_glm(no_trans.TransformPoint(to_d2d(bbox.u[1])));

    // Determine new length of unit vectors
    glm::vec2 ne{glm::length(ret.u[0]), glm::length(ret.u[1])};
    ret.e = bbox.e * ne;

    // Renormalize unit vectors
    ret.u[0] /= ne[0];
    ret.u[1] /= ne[1];

    return ret;
}