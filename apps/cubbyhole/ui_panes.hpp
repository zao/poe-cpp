#pragma once

#include <cubby/project.hpp>

#include <cstddef>
#include <memory>

#include <poe/format/ao.hpp>
#include <poe/format/fmt.hpp>
#include <poe/format/sm.hpp>
#include <pog/texture_library.hpp>

#include <imgui.h>
#include <glm/vec3.hpp>

struct AABB {
    glm::vec3 c;
    glm::vec3 e;
};

struct mesh_interface {
    virtual ~mesh_interface() = default;

    virtual AABB bounding_box() const = 0;
};

struct fixed_mesh : mesh_interface {
    AABB bounding_box() const override;

    std::shared_ptr<poe::format::fmt::FMT> fmt_;
};

struct skinned_mesh : mesh_interface {
    AABB bounding_box() const override;

    std::shared_ptr<poe::format::sm::SM> sm_;
};

struct CubbyDecorationVariant {
    std::shared_ptr<poe::format::ao::ao_parse> ao_;
    std::shared_ptr<poe::format::ao::ao_parse> aoc_;
    std::shared_ptr<mesh_interface> mesh_;
    std::shared_ptr<std::string> mesh_path_;
    std::shared_ptr<std::string> mesh_fault_;
};

struct cubby_decoration_item {
    std::string id_;
    std::string name_;
    std::string dds_name_;
    ImVec2 cell_extent_;
    uint32_t hash_;
    std::shared_ptr<pog::LoadedTexture> tex_2dart_;
    std::vector<CubbyDecorationVariant> variants_;
    std::string inherits_from_;

    std::optional<CubbyDecorationVariant> variant(size_t var_idx) const;
    size_t variant_count() const;

    std::shared_ptr<poe::format::ao::ao_parse> ao(size_t var_idx) const;
    std::shared_ptr<poe::format::ao::ao_parse> aoc(size_t var_idx) const;
    std::shared_ptr<mesh_interface> mesh(size_t var_idx) const;
    std::shared_ptr<std::string> mesh_path(size_t var_idx) const;
    std::shared_ptr<std::string> mesh_fault(size_t var_idx) const;
};

namespace cubby {
struct HideoutMeta;
}

struct imgui_project_info {
    imgui_project_info(std::shared_ptr<cubby::project> &proj, std::shared_ptr<cubby::HideoutMeta> &hideout_meta);

    void run();

    bool initialized_ = false;
    std::shared_ptr<cubby::HideoutMeta> &hideout_meta_;

    std::shared_ptr<cubby::project> &proj_;
};

struct cubby_decoration_list {
    explicit cubby_decoration_list(std::shared_ptr<cubby::HideoutMeta> &hideout_meta);

    void run();

    bool initialized_ = false;
    std::shared_ptr<cubby::HideoutMeta> &hideout_meta_;

    ImGuiTextFilter name_filter_;
    std::vector<cubby_decoration_item> items_;
    std::optional<size_t> selected_item_;
};