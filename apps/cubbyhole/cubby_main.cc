#include <asio.hpp>
#include <asio/use_future.hpp>

#include <poe/format/ao.hpp>
#include <poe/format/arm.hpp>
#include <poe/format/dat.hpp>
#include <poe/format/dgr.hpp>
#include <poe/format/ggpk.hpp>
#include <poe/format/rs.hpp>
#include <poe/format/tgr.hpp>
#include <poe/format/tsi.hpp>
#include <poe/format/tst.hpp>

#include <poe/io/vfs.hpp>

#include <poe/util/install_location.hpp>
#include <poe/util/parse_ops.hpp>
#include <poe/util/utf.hpp>

#include <pog/game_camera.hpp>
#include <pog/platform.hpp>
#include <pog/texture_library.hpp>

#include <cubby/hideout_meta.hpp>
#include <cubby/project.hpp>
#include <cubby/ui_camera_view_pane.hpp>
#include <cubby/ui_toolbox.hpp>
#include <cubby/ui_top_view_pane.hpp>
#include "math_util.hpp"

#include <fmt/format.h>

#include <loguru/loguru.hpp>

#include <filesystem>
#include <fstream>
#include <future>
#include <sstream>
#include <string>

#include <portable-file-dialogs.h>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/string_cast.hpp>

#include "ui_common.hpp"
#include "ui_panes.hpp"
#include <cubby/ui_toolbox.hpp>

#include <imgui_impl_win32.h>
#include <imgui_internal.h>

#include <d3dcompiler.h>

#include <tbb/concurrent_queue.h>
#include <tbb/parallel_for.h>

#include <QtWidgets/QApplication>

#include <cubby_qt/main_window.hpp>

using namespace std::chrono_literals;
using namespace std::string_view_literals;

enum class ui_language {
    English,
    Brazilian,
    Russian,
    Thai,
    German,
    French,
    Spanish,
    Korean,
};

constexpr ui_language all_ui_languages[] = {
    ui_language::English, ui_language::Brazilian, ui_language::Russian, ui_language::Thai,
    ui_language::German,  ui_language::French,    ui_language::Spanish, ui_language::Korean,
};

char const *to_string(ui_language lang) {
    switch (lang) {
    case ui_language::English:
        return "English";
    case ui_language::Brazilian:
        return "Brazilian";
    case ui_language::Russian:
        return "Russian";
    case ui_language::Thai:
        return "Thai";
    case ui_language::German:
        return "German";
    case ui_language::French:
        return "French";
    case ui_language::Spanish:
        return "Spanish";
    case ui_language::Korean:
        return "Korean";
    }
    return "";
}

namespace poe::format {
namespace clt {
std::shared_ptr<void> parse_clt(std::span<std::byte const> data) { return {}; }
} // namespace clt

namespace dct {
std::shared_ptr<void> parse_dct(std::span<std::byte const> data) { return {}; }
} // namespace dct

namespace ddt {
std::shared_ptr<void> parse_ddt(std::span<std::byte const> data) { return {}; }
} // namespace ddt

namespace ecf {
std::shared_ptr<void> parse_ecf(std::span<std::byte const> data) { return {}; }
} // namespace ecf

namespace edp {
std::shared_ptr<void> parse_edp(std::span<std::byte const> data) { return {}; }
} // namespace edp

namespace gft {
std::shared_ptr<void> parse_gft(std::span<std::byte const> data) { return {}; }
} // namespace gft

namespace gt {
std::shared_ptr<void> parse_gt(std::span<std::byte const> data) { return {}; }
} // namespace gt

namespace mtd {
std::shared_ptr<void> parse_mtd(std::span<std::byte const> data) { return {}; }
} // namespace mtd
} // namespace poe::format

using namespace poe::util::string_literals;

int RealMain(int argc, char **argv);

int wmain(int argc, wchar_t **argv) {
    std::vector<std::string> argStrings(argc + 1);
    std::vector<char *> args(argc + 1);
    for (int arg = 0; arg < argc; ++arg) {
        auto cb = WideCharToMultiByte(CP_UTF8, 0, argv[arg], -1, nullptr, 0, nullptr, nullptr);
        std::vector<char> buf(cb);
        WideCharToMultiByte(CP_UTF8, 0, argv[arg], -1, buf.data(), (int)buf.size(), nullptr, nullptr);
        argStrings[arg].assign(buf.data());
        args[arg] = argStrings[arg].data();
    }
    RealMain((int)argc, args.data());
}

int main(int argc, char **argv) { return RealMain(argc, argv); }

auto TryResolveSharedFuture = [](auto &target, auto &fut) {
    if (fut.valid() && fut.wait_for(0s) == std::future_status::ready) {
        target = fut.get();
        fut = {};
    }
};

DatSet::DatSet(std::shared_ptr<poe::io::vfs> vfs) : vfs_(vfs) {}

std::future<std::shared_ptr<DatSet>> DatSet::Load() {
    LoadTable(base_item_types, "Data/BaseItemTypes.dat"_poe);
    LoadTable(environments, "Data/Environments.dat"_poe);
    LoadTable(hideout_doodads, "Data/HideoutDoodads.dat"_poe);
    LoadTable(hideouts, "Data/Hideouts.dat"_poe);
    LoadTable(item_visual_identity, "Data/ItemVisualIdentity.dat"_poe);
    LoadTable(pet, "Data/Pet.dat"_poe);
    LoadTable(topologies, "Data/Topologies.dat"_poe);
    LoadTable(world_areas, "Data/WorldAreas.dat"_poe);

    auto self = shared_from_this();
    return std::async(std::launch::async, [self] {
        std::vector<int> failedIndexes;
        for (int i = 0; i < self->issueCount_; ++i) {
            auto &fut = self->bgResults_[i];
            if (!fut.valid()) {
                failedIndexes.push_back(i);
            } else {
                try {
                    fut.wait();
                } catch (std::exception &) {
                    failedIndexes.push_back(i);
                }
            }
        }
        self->bgResults_.clear();
        if (!failedIndexes.empty()) {
            throw std::runtime_error("DAT file failed to load");
        }
        return self;
    });
}

void DatSet::LoadTable(std::shared_ptr<poe::format::dat::dat_table> &table, poe::util::path const &path) {
    ++issueCount_;
    bgPaths_.push_back(path);
    bgResults_.push_back(std::async(std::launch::async, [self = shared_from_this(), path, &table] {
        auto file_fut = self->vfs_->async_open_file(path);
        auto fh = file_fut.get();
        if (!fh) {
            throw std::runtime_error("could not open file");
        }
        auto data = fh->read_all();
        auto dat = poe::format::dat::open_dat(data, path.leaf());
        if (!dat) {
            throw std::runtime_error("could not open dat");
        }
        table = dat;
    }));
}

int RealMain(int argc, char **argv) {
    loguru::init(argc, argv);

    try {
        QApplication app(argc, argv);

        cubby_qt::MainWindow win;
        win.show();

        return app.exec();
    } catch (std::exception &e) {
        LOG_F(FATAL, "{}", e.what());
        throw;
    }
}

static int f() {
    ImGui_ImplWin32_EnableDpiAwareness();

    auto specFut = std::async(std::launch::async, poe::format::dat::preload_specs);
    std::shared_future<std::shared_ptr<poe::io::vfs>> vfsFut;

    auto datsFut = std::async(std::launch::async, [vfsFut] {
                       auto dats = std::make_shared<DatSet>(vfsFut.get());
                       return dats->Load().get();
                   }).share();

    auto dats = datsFut.get();
    auto vfs = vfsFut.get();

    std::shared_ptr<cubby::project> current_project, next_project;
    std::unique_ptr<cubby::TransientProjectState> transient_project_state;

    pog::WindowFrame window_frame(u"Project CH", ImGuiConfigFlags_None);
    auto tex_lib = std::make_shared<pog::TextureLibrary>(vfs, window_frame.dx());

    std::map<std::string, std::shared_ptr<pog::LoadedTexture>> decoration_textures;

    auto loadTextureOnMainThread =
        [&tex_lib, &frame = window_frame](std::string path) -> std::future<std::shared_ptr<pog::LoadedTexture>> {
        using TexPtr = std::shared_ptr<pog::LoadedTexture>;
        auto fut =
            asio::post(frame.io_ctx_, std::packaged_task<TexPtr()>([&tex_lib, path] { return tex_lib->load(path); }));
        return fut;
    };

    auto hideoutMetaFut =
        std::async(std::launch::async, [vfsFut, datsFut, loadTextureOnMainThread, &decoration_textures] {
            auto vfs = vfsFut.get();
            auto dats = datsFut.get();
            auto hideoutMeta = std::make_shared<cubby::HideoutMeta>();

            auto &all_decorations = hideoutMeta->all_decorations;
            auto &decoration_idx_by_hash = hideoutMeta->decoration_idx_by_hash;
            {
                std::map<size_t, std::future<std::shared_ptr<pog::LoadedTexture>>> pendingTextures;

                size_t const hd_col_bit_key = *dats->hideout_doodads->find_field("BaseItemTypesKey");
                size_t const hd_col_var_aos = *dats->hideout_doodads->find_field("Variation_AOFiles");
                size_t const hd_row_count = dats->hideout_doodads->row_count();

                size_t const bit_col_id = *dats->base_item_types->find_field("Id");
                size_t const bit_col_name = *dats->base_item_types->find_field("Name");
                size_t const bit_col_width = *dats->base_item_types->find_field("Width");
                size_t const bit_col_height = *dats->base_item_types->find_field("Height");
                size_t const bit_col_ivi_key = *dats->base_item_types->find_field("ItemVisualIdentityKey");
                size_t const bit_col_hash = *dats->base_item_types->find_field("HASH");
                // size_t const bit_row_count = dats->base_item_types->row_count();

                // size_t const ivi_col_id = *dats->item_visual_identity->find_field("Id");
                size_t const ivi_col_ddsfile = *dats->item_visual_identity->find_field("DDSFile");
                // size_t const ivi_row_count = dats->item_visual_identity->row_count();

                all_decorations.resize(hd_row_count);

                tbb::parallel_for(
                    tbb::blocked_range<size_t>(0, hd_row_count), [&](tbb::blocked_range<size_t> const &r) {
                        for (size_t i = r.begin(); i != r.end(); ++i) {
                            cubby_decoration_item cdi;
                            auto hd_var_aos = dats->hideout_doodads->get(i, hd_col_var_aos);
                            for (std::string ao : hd_var_aos) {
                                CubbyDecorationVariant var;
                                std::string aoc = ao + "c";
                                auto ao_data = vfs->open_file(poe::util::path(ao))->read_all();
                                var.ao_ = poe::format::ao::parse_ao(ao_data);

                                auto aoc_data = vfs->open_file(poe::util::path(aoc))->read_all();
                                var.aoc_ = poe::format::ao::parse_ao(aoc_data);
                                if (auto const *comp = var.aoc_->find_component("SkinMesh")) {
                                    std::optional<std::string> sm;
                                    if (auto *kvs = std::get_if<poe::format::ao::kv_component_body>(&comp->second)) {
                                        if (auto kv_I = std::find_if(kvs->entries_.begin(), kvs->entries_.end(),
                                                                     [](poe::format::ao::kv_component_entry const &kv) {
                                                                         return kv.key_ == "skin";
                                                                     });
                                            kv_I != kvs->entries_.end()) {
                                            sm = kv_I->value_;
                                        }
                                    } else if (auto *unk = std::get_if<poe::format::ao::unknown_component_body>(
                                                   &comp->second)) {
                                        for (std::string_view line : unk->lines) {
                                            std::string_view start_pat = "\tskin = "sv;
                                            if (line.starts_with(start_pat)) {
                                                sm = line.substr(start_pat.size());
                                                break;
                                            }
                                        }
                                    }
                                    if (sm) {
                                        poe::util::ParseOps po(*sm);
                                        std::string sm_path;
                                        if (po.reset().quoted_string(sm_path).eol().ok()) {
                                            auto sm_data = vfs->open_file(poe::util::path(sm_path))->read_all();
                                            if (auto sm_obj = poe::format::sm::parse_sm(sm_data)) {
                                                auto mesh = std::make_shared<skinned_mesh>();
                                                mesh->sm_ = sm_obj;
                                                var.mesh_ = mesh;
                                            }
                                        }
                                    }
                                } else if (auto const *comp = var.aoc_->find_component("FixedMesh")) {
                                    std::optional<std::string> fmt;
                                    if (auto *kvs = std::get_if<poe::format::ao::kv_component_body>(&comp->second)) {
                                        if (auto kv_I = std::find_if(kvs->entries_.begin(), kvs->entries_.end(),
                                                                     [](poe::format::ao::kv_component_entry const &kv) {
                                                                         return kv.key_ == "fixed_mesh";
                                                                     });
                                            kv_I != kvs->entries_.end()) {
                                            fmt = kv_I->value_;
                                        }
                                    } else if (auto *unk = std::get_if<poe::format::ao::unknown_component_body>(
                                                   &comp->second)) {
                                        for (std::string_view line : unk->lines) {
                                            std::string_view start_pat = "\tfixed_mesh = "sv;
                                            if (line.starts_with(start_pat)) {
                                                fmt = line.substr(start_pat.size());
                                                break;
                                            }
                                        }
                                    }
                                    if (fmt) {
                                        poe::util::ParseOps po(*fmt);
                                        std::string fmt_path;
                                        if (po.reset().quoted_string(fmt_path).eol().ok()) {
                                            // fmt_path = "Art/Models/ShaderBall.fmt";
                                            auto fmt_data = vfs->open_file(poe::util::path(fmt_path))->read_all();
                                            if (auto fmt_obj = poe::format::fmt::parse_fmt(fmt_data)) {
                                                auto mesh = std::make_shared<fixed_mesh>();
                                                mesh->fmt_ = fmt_obj;
                                                var.mesh_ = mesh;
                                            }
                                        }
                                    }
                                }
                                cdi.variants_.push_back(std::move(var));
                            }

                            auto hd_bit_key = (size_t)dats->hideout_doodads->get(i, hd_col_bit_key);
                            auto bit_id = dats->base_item_types->get(hd_bit_key, bit_col_id);
                            auto bit_name = dats->base_item_types->get(hd_bit_key, bit_col_name);
                            auto bit_width = dats->base_item_types->get(hd_bit_key, bit_col_width);
                            auto bit_height = dats->base_item_types->get(hd_bit_key, bit_col_height);
                            size_t bit_ivi_key = dats->base_item_types->get(hd_bit_key, bit_col_ivi_key);
                            auto bit_hash = dats->base_item_types->get(hd_bit_key, bit_col_hash);
                            cdi.id_ = std::string(bit_id);
                            cdi.name_ = std::string(bit_name);
                            cdi.dds_name_ = dats->item_visual_identity->get(bit_ivi_key, ivi_col_ddsfile);
                            cdi.cell_extent_ = ImVec2(bit_width, bit_height);
                            cdi.hash_ = bit_hash;
                            if (!decoration_textures.contains(cdi.dds_name_)) {
                                pendingTextures[i] = loadTextureOnMainThread(cdi.dds_name_);
                            }
                            decoration_idx_by_hash[bit_hash] = i;
                            all_decorations[i] = std::move(cdi);
                        }
                    });

                for (auto &[cdiIdx, fut] : pendingTextures) {
                    auto tex = fut.get();
                    if (tex) {
                        all_decorations[cdiIdx].tex_2dart_ = tex;
                    }
                }
            }
            return hideoutMeta;
        }).share();
    std::shared_ptr<cubby::HideoutMeta> hideoutMeta;

    struct HideoutTypes {};

    auto hideoutTypesFut =
        std::async(std::launch::async, [vfsFut, datsFut] {
            auto vfs = vfsFut.get();
            auto dats = datsFut.get();

            auto hideoutTypes = std::make_shared<HideoutTypes>();
            std::set<std::string> all_tsi_fields;
            std::map<std::string, std::set<std::string>> tsi_fields_per_file;
            {
                auto ho_col_id = dats->hideouts->find_field("Id");
                auto ho_col_small_world_areas_key = dats->hideouts->find_field("SmallWorldAreasKey");
                auto ho_col_hash = dats->hideouts->find_field("Unknown0");
                auto ho_col_hideout_file = dats->hideouts->find_field("HideoutFile");
                auto ho_col_hideout_image = dats->hideouts->find_field("HideoutImage");
                for (size_t row = 0; row < dats->hideouts->row_count(); ++row) {
                    auto ho_id = dats->hideouts->get(row, *ho_col_id);
                    auto ho_hash = dats->hideouts->get(row, *ho_col_hash);
                    auto ho_hideout_file = dats->hideouts->get(row, *ho_col_hideout_file);
                    auto ho_hideout_image = dats->hideouts->get(row, *ho_col_hideout_image);
                    auto ho_small_world_areas_key = dats->hideouts->get(row, *ho_col_small_world_areas_key);
                    auto wa_col_id = dats->world_areas->find_field("Id");
                    auto wa_col_name = dats->world_areas->find_field("Name");
                    auto wa_col_topologies_keys = dats->world_areas->find_field("TopologiesKeys");
                    auto wa_col_environments_key = dats->world_areas->find_field("EnvironmentsKey");
                    auto env_col_id = dats->environments->find_field("Id");
                    auto env_col_base_envfile = dats->environments->find_field("Base_ENVFile");
                    auto wa_id = dats->world_areas->get(ho_small_world_areas_key, *wa_col_id);
                    auto wa_name = dats->world_areas->get(ho_small_world_areas_key, *wa_col_name);
                    auto wa_topologies_keys = dats->world_areas->get(ho_small_world_areas_key, *wa_col_topologies_keys);
                    auto wa_topologies_key = wa_topologies_keys.front();
                    auto wa_environments_key =
                        dats->world_areas->get(ho_small_world_areas_key, *wa_col_environments_key);
                    auto env_id = dats->environments->get(wa_environments_key, *env_col_id);
                    auto env_base_envfile = dats->environments->get(wa_environments_key, *env_col_base_envfile);
                    auto topo_col_dgr_file = dats->topologies->find_field("DGRFile");
                    poe::util::path topo_dgr_file(
                        (std::string)dats->topologies->get(wa_topologies_key, *topo_col_dgr_file));
                    {
                        LOG_F(INFO, "Hideout {}, \"{}\"", ho_id, ho_hideout_file);
                        LOG_F(INFO, "Hideout Image \"{}\"", ho_hideout_image);
                        LOG_F(INFO, "SmallWorldArea {}, \"{}\"", wa_id, wa_name);
                        LOG_F(INFO, "Environments {}, \"{}\"", env_id, env_base_envfile);
                        LOG_F(INFO, "Parsing {}", topo_dgr_file.path_);
                        auto topo_data = vfs->open_file(topo_dgr_file)->read_all();
                        std::shared_ptr<void> topo_graph;
                        std::string master_file;
                        std::shared_ptr<poe::format::tsi::TSIFile> tsi;
                        std::shared_ptr<void> fill_tiles;
                        if (topo_dgr_file.extension() == ".tgr"sv) {
                            auto topo_tgr = poe::format::tgr::parse_tgr(topo_data);
                            topo_graph = topo_tgr;
                            master_file = topo_tgr->master_file_;
                        } else if (topo_dgr_file.extension() == ".dgr"sv) {
                            auto topo_dgr = poe::format::dgr::parse_dgr(topo_data);
                            topo_graph = topo_dgr;
                            master_file = topo_dgr->master_file_;
                        }
                        {
                            auto master_path = poe::util::path(master_file);
                            auto master_data = vfs->open_file(master_path)->read_all();
                            tsi = poe::format::tsi::parse_tsi(master_data);
                            for (auto const &[name, _] : tsi->fields_) {
                                all_tsi_fields.insert(name);
                                tsi_fields_per_file[master_file].insert(name);
                            }

                            auto parse_quoted_aspect =
                                [ tsi, vfs, &master_path ](std::string_view name, auto parse_fun) -> auto {
                                if (auto leaf = tsi->find_quoted_field(name)) {
                                    poe::util::path path(*leaf);
                                    if (path.components_.size() == 1) {
                                        path = master_path.make_relative(*leaf);
                                    }
                                    auto data = vfs->open_file(path)->read_all();
                                    // LOG_F(INFO, "{}: {} {}", master_file, name, data.size());
                                    return parse_fun(data);
                                } else {
                                    // LOG_F(INFO, "{}: No {}", master_file, name);
                                }
                                return decltype(parse_fun({}))();
                            };
                            auto fill_tiles = parse_quoted_aspect("FillTiles", poe::format::gft::parse_gft);
                            auto tile_set = parse_quoted_aspect("TileSet", poe::format::tst::parse_tst);
                            auto materials_list = parse_quoted_aspect("MaterialsList", poe::format::mtd::parse_mtd);
                            auto room_set = parse_quoted_aspect("RoomSet", poe::format::rs::parse_rs);
                            if (room_set) {
                                for (auto const &e : room_set->entries_) {
                                    poe::util::path arm_path(e.arm_ref_);
                                    auto arm_data = vfs->open_file(arm_path)->read_all();
                                    auto arm = poe::format::arm::parse_arm(arm_data);
                                    if (arm) {
                                        LOG_F(INFO, "ARM {}", e.arm_ref_);
                                        LOG_F(INFO, "ARM version {}", arm->version_);
                                    }
                                    if (!arm) {
                                        LOG_F(INFO, "Could not load {}", e.arm_ref_);
                                    }
                                }
                            }
                            auto edge_combination = parse_quoted_aspect("EdgeCombination", poe::format::ecf::parse_ecf);
                            auto edge_points = parse_quoted_aspect("EdgePoints", poe::format::edp::parse_edp);
                            auto outer_ground_type = parse_quoted_aspect("OuterGroundType", poe::format::gt::parse_gt);
                            auto doodads = parse_quoted_aspect("Doodads", poe::format::ddt::parse_ddt);
                            auto decals = parse_quoted_aspect("Decals", poe::format::dct::parse_dct);
                            auto critters = parse_quoted_aspect("Critters", poe::format::clt::parse_clt);
                        }
                        // LOG_F(INFO, "Result: {} {}", topo_graph.get(), (void const *)tsi.get());
                    }
                }
            }
            return hideoutTypes;
        }).share();

    std::shared_ptr<HideoutTypes> hideoutTypes;

    cubby_decoration_list decoration_list(hideoutMeta);
    imgui_project_info project_info(current_project, hideoutMeta);

    ui_language lang = ui_language::English;

    next_project = cubby::open_hideout(R"(C:\Users\Lars\Documents\blargh.hideout)");

    struct HideoutGameData {
        size_t ho_row_current;
        size_t ho_small_world_areas_key;
        size_t wa_topologies_key;
        std::string topo_dgr_file;
        std::shared_ptr<poe::format::dgr::DGRFile> topo_dgr;
    };
    std::optional<HideoutGameData> hideout_game_data;

    bool show_demo = false;

    std::shared_ptr<pog::RenderTarget2D> rt_2d;
    auto &dx = window_frame.dx();

    auto exe_dir = poe::util::executable_dir();

#if 0
    struct TopView {
        struct DragInfo {
            glm::vec2 offset{0.0f, 0.0f};
        };

        struct ZoomInfo {
            explicit ZoomInfo(glm::vec2 focus) : focus(focus) {}
            glm::vec2 focus;
            float mod = 0.0f;
        };

        glm::vec2 world_center{0.0f, 0.0f};
        float zoom_level = 0.0f;
        std::optional<DragInfo> drag_info;
        std::optional<ZoomInfo> zoom_info;

        glm::vec2 view_shift() const { return world_center + (drag_info ? drag_info->offset : glm::vec2{0.0f, 0.0f}); }
        float view_zoom() const { return pow(10.0f, zoom_level + (zoom_info ? zoom_info->mod : 0.0f)); }

        bool is_interacting() const { return drag_info || zoom_info; }

        DragInfo &begin_drag() {
            if (!drag_info) {
                drag_info = DragInfo();
            }
            return *drag_info;
        }

        void commit_drag() {
            if (drag_info) {
                world_center += drag_info->offset;
            }
            drag_info.reset();
        }

        ZoomInfo &begin_zoom(glm::vec2 focus) {
            if (!zoom_info) {
                zoom_info = ZoomInfo(focus);
            }
            return *zoom_info;
        }

        void commit_zoom() {
            if (zoom_info) {
                zoom_level += zoom_info->mod;
            }
            zoom_info.reset();
        }
    };

    TopView top_view;
#endif

    bool copy_command_invoked = false;
    bool paste_command_invoked = false;

    std::optional<std::string> last_paste;

    uint64_t time_base;
    uint64_t time_freq;
    uint64_t time_now;
    QueryPerformanceCounter((LARGE_INTEGER *)&time_base);
    QueryPerformanceFrequency((LARGE_INTEGER *)&time_freq);

    auto toolbox = std::make_shared<cubby::Toolbox>();
    toolbox->tools.push_back(std::make_shared<cubby::Rotate90Tool>());

    auto camViewPane = std::make_shared<cubby::CameraViewPane>(dx, hideoutMeta);
    auto topViewPane = std::make_shared<cubby::TopViewPane>();

    LOG_F(INFO, "Entering frame loop");
    while (window_frame.new_frame()) {
        TryResolveSharedFuture(dats, datsFut);
        TryResolveSharedFuture(vfs, vfsFut);
        TryResolveSharedFuture(hideoutMeta, hideoutMetaFut);
        TryResolveSharedFuture(hideoutTypes, hideoutTypesFut);

        bool is_copy_possible = [&] {
            return transient_project_state && !transient_project_state->selected_objects.empty();
        }();
        bool has_valid_clipboard_state = [&] {
            if (char const *clip_txt = ImGui::GetClipboardText()) {
                if (clip_txt[0] == '{') {
                    try {
                        auto json = nlohmann::json::parse(clip_txt);
                        return true;
                    } catch (nlohmann::json::parse_error &) {
                    }
                }
            }
            return false;
        }();
        QueryPerformanceCounter((LARGE_INTEGER *)&time_now);
        double const now = (time_now - time_base) / (double)time_freq;
        (void)now;
        window_frame.begin_window();
        if (ImGui::BeginMenuBar()) {
            if (ImGui::BeginMenu("File")) {
                if (ImGui::MenuItem("Open...")) {
                    std::vector<std::string> filters{"Hideout Export", "*.hideout", "Cubbyhole Project", "*.cubby"};
                    auto res = pfd::open_file("Open hideout/project", "", filters).result();
                    if (res.size()) {
                        std::filesystem::path p = res[0];
                        LOG_F(INFO, "Opening file {}", res[0]);
                        auto proj = cubby::open_hideout(p);
                        if (proj) {
                            next_project = proj;
                        }
                    }
                }
                if (ImGui::MenuItem("Export to .hideout (JSON)...", nullptr, nullptr, !!current_project)) {
                    std::vector<std::string> filters{"Hideout Export (JSON)", "*.hideout"};
                    auto res = pfd::save_file("Export hideout", "", filters, pfd::opt::force_overwrite).result();
                    if (res.size()) {
                        std::filesystem::path p = res;
                        LOG_F(INFO, "Exporting file {}", res);
                        cubby::export_hideout_json_format(*current_project, p);
                    }
                }
                if (ImGui::MenuItem("Export to .hideout (Legacy)...", nullptr, nullptr, !!current_project)) {
                    std::vector<std::string> filters{"Hideout Export (Legacy)", "*.hideout"};
                    auto res = pfd::save_file("Export hideout", "", filters, pfd::opt::force_overwrite).result();
                    if (res.size()) {
                        std::filesystem::path p = res;
                        LOG_F(INFO, "Exporting file {}", res);
                        cubby::export_hideout_legacy_format(*current_project, p);
                    }
                }
                if (ImGui::MenuItem("Exit", "Alt-F4")) {
                    PostQuitMessage(0);
                }
                ImGui::EndMenu();
            }
            if (ImGui::BeginMenu("Edit")) {
                if (ImGui::MenuItem("Copy", "Ctrl-C", nullptr, is_copy_possible)) {
                    copy_command_invoked = true;
                }
                if (ImGui::MenuItem("Paste", "Ctrl-V", nullptr, has_valid_clipboard_state)) {
                    paste_command_invoked = true;
                }
                ImGui::EndMenu();
            }
            if (ImGui::BeginMenu("Settings")) {
                char const *lang_str = to_string(lang);
                if (ImGui::BeginCombo("Object Language", lang_str)) {
                    for (auto uil : all_ui_languages) {
                        bool selected = lang == uil;
                        if (ImGui::Selectable(to_string(uil), &selected)) {
                            lang = uil;
                        }
                    }
                    ImGui::EndCombo();
                }
                ImGui::EndMenu();
            }

            if (ImGui::MenuItem("ImGui Demo")) {
                show_demo = true;
            }

            ImGui::EndMenuBar();
        }
        ImGui::End();

        if (copy_command_invoked) {
            if (auto const &selection = transient_project_state->selected_objects; !selection.empty()) {
                using json = nlohmann::json;
                json object_list;
                for (size_t obj_idx : selection) {
                    auto const &obj = current_project->objects_[obj_idx];
                    uint8_t fv = obj.var_ | (obj.flip_ ? 0x80 : 0);
                    object_list.push_back({
                        {
                            "name",
                            obj.name_,
                        },
                        {
                            "hash",
                            obj.hash_,
                        },
                        {
                            "x",
                            obj.x_,
                        },
                        {
                            "y",
                            obj.y_,
                        },
                        {
                            "rot",
                            obj.rot_,
                        },
                        {"fv", fv},
                    });
                }
                json copy_data = {
                    {
                        "version",
                        1,
                    },
                    {
                        "type",
                        "cubby-objects",
                    },
                    {
                        "objects",
                        object_list,
                    },
                };
                std::string copy_txt = to_string(copy_data);
                ImGui::SetClipboardText(copy_txt.c_str());
            }
            copy_command_invoked = false;
        }

        if (paste_command_invoked) {
            if (auto clip_txt = ImGui::GetClipboardText()) {
                last_paste = clip_txt;
            } else {
                last_paste.reset();
            }
            paste_command_invoked = false;
        }

        if (ImGui::Begin("Paste dump")) {
            if (last_paste) {
                try {
                    auto json = nlohmann::json::parse(last_paste->c_str());
                    ImGui::TextUnformatted("Valid JSON");
                    switch ((uint32_t)json["version"]) {
                    case 1:
                        if (json["type"] == "cubby-objects") {
                            for (auto const &desc : json["objects"]) {
                                auto obj = cubby::hideout_object::from_json(desc);
                                if (obj) {
                                    ImGui::Text("%s %08x %u %u %u %s %u", obj->name_.c_str(), obj->hash_, obj->x_,
                                                obj->y_, obj->rot_, obj->flip_ ? "flipped" : "unflipped", obj->var_);
                                } else {
                                    ImGui::Text("Invalid object: %s", nlohmann::to_string(desc).c_str());
                                }
                            }
                        } else {
                            ImGui::Text("Invalid paste type: %s", std::string(json["type"]).c_str());
                        }
                        break;
                    default:
                        ImGui::Text("Unknown version %u", (uint32_t)json["version"]);
                    }
                } catch (nlohmann::json::parse_error &) {
                    ImGui::TextUnformatted("Invalid JSON");
                }
                ImGui::TextUnformatted(last_paste->c_str());
            } else {
                ImGui::TextUnformatted("<no paste>");
            }
        }
        ImGui::End();

        bool new_project = false;
        if (next_project) {
            hideout_game_data = {};
            {
                auto ho_col_hash = dats->hideouts->find_field("Unknown0");
                auto ho_col_small_world_areas_key = dats->hideouts->find_field("SmallWorldAreasKey");
                for (size_t row = 0; row < dats->hideouts->row_count(); ++row) {
                    auto ho_hash = dats->hideouts->get(row, *ho_col_hash);
                    if (ho_hash == next_project->hideout_hash_) {
                        HideoutGameData hgd;
                        hgd.ho_row_current = row;
                        hgd.ho_small_world_areas_key = dats->hideouts->get(row, *ho_col_small_world_areas_key);
                        auto wa_col_topologies_keys = dats->world_areas->find_field("TopologiesKeys");
                        auto wa_topologies_keys =
                            dats->world_areas->get(hgd.ho_small_world_areas_key, *wa_col_topologies_keys);
                        hgd.wa_topologies_key = wa_topologies_keys.front();
                        auto topo_col_dgr_file = dats->topologies->find_field("DGRFile");
                        hgd.topo_dgr_file = dats->topologies->get(hgd.wa_topologies_key, *topo_col_dgr_file);
                        {
                            auto dgr_data = vfs->open_file(poe::util::path(hgd.topo_dgr_file))->read_all();
                            hgd.topo_dgr = poe::format::dgr::parse_dgr(dgr_data);
                        }
                        hideout_game_data = hgd;
                        current_project = next_project;
                        {
                            std::vector<glm::vec2> points;
                            for (auto const &obj : current_project->objects_) {
                                points.push_back(glm::vec2{obj.x_, obj.y_});
                            }
                            // auto bbox = aabb_from_points(points);
                            // cam_at = glm::vec3{bbox->c.x, bbox->c.y, 0.0f};
                        }
#if COND_DUMMY_OBJECTS_FOR_SELECTION
                        auto o0 = current_project->objects_[0];
                        current_project->objects_.clear();
                        for (size_t row = 0; row < 20; ++row) {
                            for (size_t col = 0; col < 20; ++col) {
                                auto o = o0;
                                o.x_ = (uint32_t)(col * 10);
                                o.y_ = (uint32_t)(row * 10);
                                current_project->objects_.push_back(o);
                            }
                        }
#endif
                        transient_project_state = std::make_unique<cubby::TransientProjectState>();
                        for (size_t i = 0; i < current_project->objects_.size(); i += 3) {
                            transient_project_state->selected_objects.insert(i);
                        }
                        break;
                    }
                }
            }
            next_project.reset();
            new_project = true;
        }

        if (show_demo) {
            ImGui::ShowDemoWindow(&show_demo);
        }

        decoration_list.run();

        project_info.run();
        if (ImGui::Begin("Doodads")) {
            [&] {
                if (!dats) {
                    ImGui::TextUnformatted("Loading tables...");
                } else {
                    size_t const n = dats->hideout_doodads->row_count();
                    auto idx_bit_key = dats->hideout_doodads->find_field("BaseItemTypesKey");
                    auto idx_vars = dats->hideout_doodads->find_field("Variation_AOFiles");
                    if (!idx_bit_key || !idx_vars) {
                        return;
                    }
                    for (size_t row = 0; row < n; ++row) {
                        auto bit_key = dats->hideout_doodads->get(row, *idx_bit_key);
                        auto vars = dats->hideout_doodads->get(row, *idx_vars);
                        ImGui::Text("%lld", (int64_t)bit_key);
                        ImGui::Indent();
                        for (auto const &var : vars) {
                            ImGui::TextUnformatted(std::string(var).c_str());
                        }
                        ImGui::Unindent();
                    }
                }
            }();
        }
        ImGui::End();

        if (ImGui::Begin("Objects")) {
            if (auto proj = current_project) {
                for (size_t obj_idx = 0; obj_idx < proj->objects_.size(); ++obj_idx) {
                    auto const &obj = proj->objects_[obj_idx];
                    bool selected = transient_project_state->selected_objects.contains(obj_idx);
                    ImColor color = selected ? ImColor::HSV(0.0f, 0.5f, 0.5f) : ImColor::HSV(0.5f, 0.5f, 0.5f);
                    ImGui::TextColored(color, "%s", obj.name_.c_str());
                }
            }
        }
        ImGui::End();

        toolbox->Run(current_project.get(), transient_project_state.get());

        camViewPane->Run(new_project ? current_project : nullptr);

        topViewPane->Run();

        window_frame.render_and_present();
    }
    LOG_F(INFO, "Exited frame loop");

    return 0;
}