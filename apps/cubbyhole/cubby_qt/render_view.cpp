#include "render_view.hpp"

#include <glm/gtc/type_ptr.hpp>

#include <QtGui/QResizeEvent>

namespace cubby_qt {
RenderView::RenderView(QWidget *parent) : QFrame(parent) {
    viewHost_ = std::make_unique<pog::ViewHost>(dx_, (HWND)winId());
    viewRt_ = std::make_unique<pog::LayerTarget>(dx_);

    QPalette pal = palette();
    pal.setColor(QPalette::Window, Qt::black);
    setAutoFillBackground(true);
    setPalette(pal);

    setFocusPolicy(Qt::StrongFocus);
    setAttribute(Qt::WA_NativeWindow);

    setAttribute(Qt::WA_PaintOnScreen);
    setAttribute(Qt::WA_NoSystemBackground);
}

constexpr float const fpsLimit = 250.0f;
constexpr float const msPerFrame = 1000.0f / fpsLimit;

void RenderView::Run() {
    updateTimer_.start(msPerFrame);
    connect(&updateTimer_, &QTimer::timeout, this, &RenderView::onFrame);
}

void RenderView::Stop() { updateTimer_.stop(); }

void RenderView::onFrame() {
    auto *ctx = dx_.Context();
    auto &rt = *viewRt_;

    D3D11_VIEWPORT viewport{.TopLeftX = 0,
                            .TopLeftY = 0,
                            .Width = (float)fbSize_.x,
                            .Height = (float)fbSize_.y,
                            .MinDepth = 0.0f,
                            .MaxDepth = 1.0f};
    ctx->RSSetViewports(1, &viewport);
    viewHost_->Clear();

    glm::vec4 clearColor{0.1f, 0.2f, 0.3f, 1.0f};
    ctx->ClearRenderTargetView(rt.rtv, glm::value_ptr(clearColor));
    viewHost_->BlendTarget(rt.rtSrv);

    viewHost_->Present();
}

bool RenderView::nativeEvent(QByteArray const &eventType, void *message, qintptr *result) {
    auto *msg = (MSG *)message;
    switch (msg->message) {
    case WM_PAINT: {
    }
    }

    return QWidget::nativeEvent(eventType, message, result);
}

void RenderView::resizeEvent(QResizeEvent *ev) {
    fbSize_ = glm::ivec2{ev->size().width(), ev->size().height()};
    viewRt_->Resize(fbSize_);
    viewHost_->Resize(fbSize_);

    QFrame::resizeEvent(ev);
}
} // namespace cubby_qt
