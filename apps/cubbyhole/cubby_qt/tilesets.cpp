#include "tilesets.hpp"

#include <QtGui/QStandardItemModel>
#include <QtWidgets/QGraphicsPixmapItem>

using namespace std::string_view_literals;

namespace cubby_qt {
TileSetsViewer::TileSetsViewer(std::shared_ptr<cubby::HideoutMeta> meta, QWidget *parent)
    : QDialog(parent), meta_(meta) {
    setupUi(this);

    std::map<std::string, uint16_t> hashByName;
    for (auto &[h, topo] : meta_->levelTopos) {
        hashByName[topo->graph->masterFile()] = h;
    }

    for (auto &[name, h] : hashByName) {
        setChoice->addItem(QString::fromStdString(name), h);
        // auto &topo = meta->levelTopos[h];
    }
    if (hashByName.size()) {
        setChoice->setCurrentIndex(0);
    }

    for (int i = 0; i < setChoice->count(); ++i) {
        if (setChoice->itemText(i).contains("Veritania")) {
            setChoice->setCurrentIndex(i);
        }
    }
}

std::string ShortTileKey(cubby::level_gen::TileKey const &key) {
    fmt::memory_buffer buf;
    fmt::format_to(fmt::appender(buf), "\"{}\"", key.tag);
    for (size_t i = 0; i < 4; ++i) {
        if (auto str = key.edgeKeys[i]) {
            fmt::format_to(fmt::appender(buf), " {}<\"{}\">", key.edgeOffsets[i], *str);
        } else {
            fmt::format_to(fmt::appender(buf), " {}<>", key.edgeOffsets[i]);
        }
    }
    for (size_t i = 0; i < 4; ++i) {
        if (auto str = key.groundKeys[i]) {
            fmt::format_to(fmt::appender(buf), "|{}<\"{}\">", key.groundOffsets[i], *str);
        } else {
            fmt::format_to(fmt::appender(buf), " {}<>", key.groundOffsets[i]);
        }
    }
    return to_string(buf);
}

void TileSetsViewer::on_setChoice_currentIndexChanged(int index) {
    auto h = (uint16_t)setChoice->itemData(index).toInt();
    auto topo = meta_->levelTopos[h];
    auto tileSet = topo->tileSet;

    auto members = new QStandardItemModel();
    for (auto &def : tileSet->tileDefs) {
        std::string shortKey = ShortTileKey(def->key);
        QString label = QString("%0\n%1").arg(QString::fromStdString(def->path)).arg(QString::fromStdString(shortKey));
        members->appendRow(new QStandardItem(label));
    }
    setMembers->setModel(members);
}

void TileSetsViewer::on_setMembers_activated(QModelIndex const &index) {
    constexpr const auto orNone = [](std::optional<std::string> const &opt) -> std::string_view {
        return opt ? std::string_view(*opt) : "<None>"sv;
    };

    auto h = (uint16_t)setChoice->currentData().toInt();
    auto tileset = meta_->levelTopos[h]->tileSet;
    fmt::memory_buffer buf;
    auto def = tileset->tileDefs[index.row()];
    auto &tdt = def->tdt;
    fmt::format_to(fmt::appender(buf), R"#(Path: {}
Version: {}
Common TGT: {}
Edge key:
)#",
                   def->path, tdt.version, orNone(tdt.commonTgt));
    auto &hdr = tdt.header;
    for (int i = 0; i < 4; ++i) {
        fmt::format_to(fmt::appender(buf), "  {}: {} {}\n", i, (int)hdr.sideOffsets[i], orNone(hdr.sideEts[i]));
    }
    fmt::format_to(fmt::appender(buf), "Ground key:\n");
    for (int i = 0; i < 4; ++i) {
        fmt::format_to(fmt::appender(buf), "  {}: {} {}\n", i, (int)hdr.sideOffsets[i + 4], orNone(hdr.sideGts[i]));
    }

    memberInfo->setPlainText(QString::fromUtf8(buf.data(), buf.size()));

    auto scene = new QGraphicsScene();

    for (size_t subIdx = 0; subIdx < tdt.subTiles.size(); ++subIdx) {
        int rowIdx = (int)(subIdx / hdr.dim.x);
        int qtRow = (int)(hdr.dim.y - rowIdx - 1);
        int colIdx = (int)(subIdx % hdr.dim.x);
        auto &sub = tdt.subTiles[subIdx];
        using poe::format::tdt::tileSize;

        char const *fSig = sub.fixed ? "F" : "-";
        char const *vSig = sub.varying ? "V" : "-";
        QString toolText = QString::fromStdString(
            fmt::format("{}, {}\n{:02X}:{:02X}\n{}{}", colIdx, rowIdx, sub.type, sub.val, fSig, vSig));
        QString bodyText = QString::fromStdString(fmt::format("{:02X}:{:02X}\n{}{}", sub.type, sub.val, fSig, vSig));

        QImage heightImg(tileSize, tileSize, QImage::Format_Grayscale8);
        if (sub.fixed) {
            std::vector<uint8_t> line(tileSize);
            for (int imgRow = 0; imgRow < tileSize; ++imgRow) {
                auto *p = sub.fixed->data() + imgRow * tileSize;
                line.assign(p, p + tileSize);
                for (auto &x : line) {
                    x += 0x80; // bias int8 to uint8 range.
                }
                memcpy(heightImg.scanLine(tileSize - imgRow - 1), line.data(), tileSize);
            }
        } else if (sub.type == 0x07) {
            for (int imgRow = 0; imgRow < tileSize; ++imgRow) {
                memset(heightImg.scanLine(tileSize - imgRow - 1), 0x80, tileSize);
            }
        } else {
            for (int imgRow = 0; imgRow < tileSize; ++imgRow) {
                memset(heightImg.scanLine(tileSize - imgRow - 1), sub.val + 0x80, tileSize);
            }
        }

        auto *item = new QGraphicsRectItem(0, 0, 250.0, 250.0);
        item->setPen(QPen(QColor("lightgreen"), 2.0));
        item->setPos(colIdx * 250.0, qtRow * 250.0);
        item->setToolTip(toolText);

        QPixmap heightPix = QPixmap::fromImage(std::move(heightImg));
        auto *heightItem = new QGraphicsPixmapItem(heightPix, item);
        heightItem->setScale(250.0 / tileSize);

        auto typeOverlay = new QGraphicsRectItem(0, 0, 250.0, 250.0, item);
        static const std::map<uint8_t, char const *> ovlColors{
            {0x00, "red"},     {0x01, "green"},     {0x02, "blue"},    {0x09, "yellow"}, {0x03, "purple"},
            {0x0B, "teal"},    {0x1B, "pink"},      {0x04, "silver"},  {0x06, "gold"},   {0x0D, "brown"},
            {0x07, "darkred"}, {0x0F, "darkgreen"}, {0x1F, "darkblue"}};
        QColor ovlColor = ovlColors.find(sub.type)->second;
        ovlColor.setAlphaF(0.2f);
        QBrush ovlBrush(ovlColor);
        typeOverlay->setBrush(ovlBrush);

        auto *textItem = new QGraphicsSimpleTextItem(bodyText, item);
        QPen textPen(QColor("gray"), 0.1);
        textItem->setFont(QFont("Consolas", 32.0));
        textItem->setPen(textPen);
        textItem->setBrush(QBrush(QColor("white")));

        scene->addItem(item);
    }
    memberView->setScene(scene);
    memberView->fitInView(scene->itemsBoundingRect(), Qt::KeepAspectRatio);
}

void TileSetsViewer::on_viewZoom_valueChanged(int value) {
    memberView->fitInView(memberView->scene()->itemsBoundingRect(), Qt::KeepAspectRatio);
}
} // namespace cubby_qt