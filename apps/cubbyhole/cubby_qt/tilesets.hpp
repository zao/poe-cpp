#pragma once

#include "tilesets.ui.hpp"
#include "cubby/hideout_meta.hpp"

#include <memory>

namespace cubby_qt {
struct TileSetsViewer : public QDialog, private Ui::TileSetsDialog {
    Q_OBJECT

  public:
    explicit TileSetsViewer(std::shared_ptr<cubby::HideoutMeta> meta, QWidget *parent = nullptr);

  private slots:
    void on_setChoice_currentIndexChanged(int index);
    void on_setMembers_activated(QModelIndex const &index);
    void on_viewZoom_valueChanged(int value);

  private:
    std::shared_ptr<cubby::HideoutMeta> meta_;
    std::vector<std::shared_ptr<poe::format::tdt::Tdt>> tiles_;
};
} // namespace cubby_qt