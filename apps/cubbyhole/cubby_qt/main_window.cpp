#include "main_window.hpp"

#include "tilesets.hpp"

#include <algorithm>

#include <QtCore/QStandardPaths>
#include <QtWidgets/QGraphicsItem>
#include <QtWidgets/QStyleOption>
#include <QtWidgets/QtWidgets>

#include "cubby/level_gen.hpp"

#include "poe/format/arm.hpp"
#include "poe/format/dat.hpp"
#include "poe/format/rs.hpp"
#include "poe/format/tdt.hpp"
#include "poe/format/tsi.hpp"
#include "poe/format/tst.hpp"
#include "poe/util/memmem.hpp"
#include "poe/util/parse_info.hpp"
#include "poe/util/parse_ops.hpp"

#include <glm/vec2.hpp>
#include <tbb/parallel_for.h>

#include <regex>

cubby::HideoutLayout::TopologyGraph LoadTopologyGraph(std::shared_ptr<poe::io::vfs> vfs, std::string path) {
    auto fh = vfs->open_file(poe::util::path(path));
    if (path.ends_with(".dgr")) {
        return poe::format::dgr::parse_dgr(fh->read_all());
    }
    if (path.ends_with(".tgr")) {
        return poe::format::tgr::parse_tgr(fh->read_all());
    }
    return {};
}

std::shared_ptr<cubby::HideoutMeta> LoadHideoutMeta(std::shared_ptr<cubby::ParseCache> cache) {
    using namespace std::string_view_literals;
    auto vfs = cache->innerVfs();
    auto dats = std::make_shared<DatSet>(vfs)->Load().get();
    auto hideoutMeta = std::make_shared<cubby::HideoutMeta>();

    auto &all_decorations = hideoutMeta->all_decorations;
    auto &decoration_idx_by_hash = hideoutMeta->decoration_idx_by_hash;
    {
        std::map<size_t, std::future<std::shared_ptr<pog::LoadedTexture>>> pendingTextures;

        size_t const hd_col_bit_key = *dats->hideout_doodads->find_field("BaseItemTypesKey");
        size_t const hd_col_var_aos = *dats->hideout_doodads->find_field("Variation_AOFiles");
        size_t const hd_col_inherits_from = *dats->hideout_doodads->find_field("InheritsFrom");
        size_t const hd_row_count = dats->hideout_doodads->row_count();

        size_t const bit_col_id = *dats->base_item_types->find_field("Id");
        size_t const bit_col_name = *dats->base_item_types->find_field("Name");
        size_t const bit_col_width = *dats->base_item_types->find_field("Width");
        size_t const bit_col_height = *dats->base_item_types->find_field("Height");
        size_t const bit_col_ivi_key = *dats->base_item_types->find_field("ItemVisualIdentityKey");
        size_t const bit_col_hash = *dats->base_item_types->find_field("HASH");
        size_t const bit_col_inherits_from = *dats->base_item_types->find_field("InheritsFrom");
        // size_t const bit_row_count = dats->base_item_types->row_count();

        // size_t const ivi_col_id = *dats->item_visual_identity->find_field("Id");
        size_t const ivi_col_ddsfile = *dats->item_visual_identity->find_field("DDSFile");
        // size_t const ivi_row_count = dats->item_visual_identity->row_count();

        auto &hideouts_dat = dats->hideouts;
        size_t const h_col_small_world_areas_key = *hideouts_dat->find_field("SmallWorldAreasKey");
        size_t const h_col_hash16 = *hideouts_dat->find_field("Unknown0");
        size_t const h_row_count = hideouts_dat->row_count();

        auto &world_areas_dat = dats->world_areas;
        size_t const wa_col_hash16 = *world_areas_dat->find_field("HASH16");
        size_t const wa_col_topologies_keys = *world_areas_dat->find_field("TopologiesKeys");
        size_t const wa_row_count = world_areas_dat->row_count();

        auto &topo_dat = dats->topologies;
        size_t const topo_col_dgr_file = *topo_dat->find_field("DGRFile");
        size_t const topo_row_count = topo_dat->row_count();

        std::map<uint16_t, size_t> h_to_wa;
        for (size_t h_idx = 0; h_idx < h_row_count; ++h_idx) {
            auto h_small_world_areas_key = hideouts_dat->get(h_idx, h_col_small_world_areas_key);
            auto h_hash16 = hideouts_dat->get(h_idx, h_col_hash16);
            h_to_wa[h_hash16] = h_small_world_areas_key;
        }

        std::map<uint16_t, size_t> h_to_topo;
        for (auto &[h, wa_idx] : h_to_wa) {
            auto wa_topologies_keys = world_areas_dat->get(wa_idx, wa_col_topologies_keys);
            h_to_topo[h] = wa_topologies_keys.front();
        }

        std::map<uint16_t, std::string> h_to_dgr_file;
        for (auto &[h, topo_idx] : h_to_topo) {
            auto topo_dgr_file = topo_dat->get(topo_idx, topo_col_dgr_file);
            h_to_dgr_file[h] = topo_dgr_file;
        }

        // std::map<uint16_t, int>
        for (auto &[h, dgr_file] : h_to_dgr_file) {
            auto levelTopo = std::make_shared<cubby::level_gen::TopologyGraph>(vfs, poe::util::path(dgr_file));
            hideoutMeta->levelTopos[h] = levelTopo;
            auto topo = LoadTopologyGraph(vfs, dgr_file);
            hideoutMeta->layouts[h].topology_graph = topo;
            std::optional<poe::util::path> master_path;
            struct GraphNode {
                std::string tag;
                int x;
                int y;
            };
            std::vector<GraphNode> nodes;
            if (auto *p = std::get_if<std::shared_ptr<poe::format::tgr::TGRFile>>(&topo)) {
                auto tgr = *p;
                master_path = poe::util::path(tgr->master_file_);
                for (auto &node : tgr->nodes_) {
                    GraphNode n;
                    n.tag = node.tag_;
                    n.x = (int)node.x_;
                    n.y = (int)node.y_;
                    nodes.push_back(n);
                }
            } else if (auto *p = std::get_if<std::shared_ptr<poe::format::dgr::DGRFile>>(&topo)) {
                auto dgr = *p;
                master_path = poe::util::path(dgr->master_file_);
                for (auto &node : dgr->nodes_) {
                    GraphNode n;
                    n.tag = node.key_;
                    n.x = (int)node.x_;
                    n.y = (int)node.y_;
                    nodes.push_back(n);
                }
            }
            auto tsi = cache->parseFileAs<poe::format::tsi::TSIFile>(*master_path);
            std::optional<poe::util::path> tst_path;
            if (auto p = tsi->find_quoted_field("TileSet")) {
                if (p->find('/') != p->npos) {
                    tst_path = poe::util::path(*p);
                } else {
                    tst_path = master_path->make_relative(*p);
                }
            }
            auto tst = cache->parseFileAs<poe::format::tst::Tst>(*tst_path);
            for (auto &part : tst->parts) {
                for (auto entry : part) {
                    auto tdt = cache->parseFileAs<poe::format::tdt::Tdt>(poe::util::path(entry.tdt));
                    if (!tdt) {
                        LOG_F(WARNING, "TDT parse failed: {}", entry.tdt);
                    } else {
                        // LOG_F(INFO, "TDT parsed: tag \"{}\", path \"{}\"", tdt->tag, entry.tdt);
                    }
                }
            }
            auto roomset_rel_path = tsi->find_quoted_field("RoomSet");
            auto roomset_path = master_path->make_relative(*roomset_rel_path);
            auto roomset = cache->parseFileAs<poe::format::rs::RSFile>(roomset_path);
            std::map<std::string, std::shared_ptr<poe::format::arm::ARMFile>> arms;
            for (auto &entry : roomset->entries_) {
                auto arm = cache->parseFileAs<poe::format::arm::ARMFile>(entry.arm_ref_);
                assert(arm);
                arms[arm->name_] = arm;
            }
            cubby::HideoutLayout layout;
            layout.tst = *tst;
            for (auto &node : nodes) {
                auto arm = arms[node.tag];
                cubby::HideoutLayout::Room room;
                room.arm = arm;
                room.x_pos = node.x;
                room.y_pos = node.y;
                layout.rooms.push_back(room);
            }
            hideoutMeta->layouts[h] = layout;
        }

        auto &pet_dat = dats->pet;
        size_t const pet_col_id = *pet_dat->find_field("Id");
        size_t const pet_col_hash32 = *pet_dat->find_field("HASH32");
        size_t const pet_col_bit_key = *pet_dat->find_field("BaseItemTypesKey");
        size_t const pet_row_count = pet_dat->row_count();
        size_t const pet_row_start = hd_row_count;

        auto decoration_count = hd_row_count + pet_row_count;

        all_decorations.resize(decoration_count);

        auto LoadVariant = [&, cache](std::string const &ao) -> std::optional<CubbyDecorationVariant> {
            CubbyDecorationVariant var;
            std::string aoc = ao + "c";
            var.ao_ = cache->parseFileAs<poe::format::ao::ao_parse>(ao);
            if (!var.ao_) {
                return std::nullopt;
            }

            var.aoc_ = cache->parseFileAs<poe::format::ao::ao_parse>(aoc);
            if (!var.aoc_) {
                return std::nullopt;
            }

            if (auto const *comp = var.aoc_->find_component("SkinMesh")) {
                std::optional<std::string> sm;
                if (auto *kvs = std::get_if<poe::format::ao::kv_component_body>(&comp->second)) {
                    if (auto kv_I = std::find_if(
                            kvs->entries_.begin(), kvs->entries_.end(),
                            [](poe::format::ao::kv_component_entry const &kv) { return kv.key_ == "skin"; });
                        kv_I != kvs->entries_.end()) {
                        sm = kv_I->value_;
                    }
                } else if (auto *unk = std::get_if<poe::format::ao::unknown_component_body>(&comp->second)) {
                    for (std::string_view line : unk->lines) {
                        std::string_view start_pat = "\tskin = "sv;
                        if (line.starts_with(start_pat)) {
                            sm = line.substr(start_pat.size());
                            break;
                        }
                    }
                }
                if (sm) {
                    poe::util::ParseOps po(*sm);
                    std::string sm_path;
                    if (po.reset().quoted_string(sm_path).eol().ok()) {
                        var.mesh_path_ = std::make_shared<std::string>(sm_path);
                        if (auto sm_obj = cache->parseFileAs<poe::format::sm::SM>(sm_path)) {
                            auto mesh = std::make_shared<skinned_mesh>();
                            mesh->sm_ = sm_obj;
                            var.mesh_ = mesh;
                        }
                    }
                }
            } else if (auto const *comp = var.aoc_->find_component("FixedMesh")) {
                std::optional<std::string> fmt;
                if (auto *kvs = std::get_if<poe::format::ao::kv_component_body>(&comp->second)) {
                    if (auto kv_I = std::find_if(
                            kvs->entries_.begin(), kvs->entries_.end(),
                            [](poe::format::ao::kv_component_entry const &kv) { return kv.key_ == "fixed_mesh"; });
                        kv_I != kvs->entries_.end()) {
                        fmt = kv_I->value_;
                    }
                } else if (auto *unk = std::get_if<poe::format::ao::unknown_component_body>(&comp->second)) {
                    for (std::string_view line : unk->lines) {
                        std::string_view start_pat = "\tfixed_mesh = "sv;
                        if (line.starts_with(start_pat)) {
                            fmt = line.substr(start_pat.size());
                            break;
                        }
                    }
                }
                if (fmt) {
                    poe::util::ParseOps po(*fmt);
                    std::string fmt_path;
                    if (po.reset().quoted_string(fmt_path).eol().ok()) {
                        var.mesh_path_ = std::make_shared<std::string>(fmt_path);
                        // fmt_path = "Art/Models/ShaderBall.fmt";
                        auto fmt_data = vfs->open_file(poe::util::path(fmt_path))->read_all();
                        auto &parse_svc = poe::util::get_parse_info_service();

                        struct ScopedParse {
                            ScopedParse(std::string_view name) {
                                poe::util::get_parse_info_service().begin_parse(name);
                            }
                            ~ScopedParse() { poe::util::get_parse_info_service().end_parse(); }
                        };

                        ScopedParse sp(fmt_path);
                        if (auto fmt_obj = cache->parseFileAs<poe::format::fmt::FMT>(fmt_path)) {
                            auto mesh = std::make_shared<fixed_mesh>();
                            mesh->fmt_ = fmt_obj;
                            var.mesh_ = mesh;
                        }
                    }
                }
            }
            return var;
        };

        auto LoadDecorationItem = [&](size_t baseItemTypesKey) -> cubby_decoration_item {
            cubby_decoration_item cdi;
            auto bit_id = dats->base_item_types->get(baseItemTypesKey, bit_col_id);
            auto bit_name = dats->base_item_types->get(baseItemTypesKey, bit_col_name);
            auto bit_width = dats->base_item_types->get(baseItemTypesKey, bit_col_width);
            auto bit_height = dats->base_item_types->get(baseItemTypesKey, bit_col_height);
            auto bit_inherits_from = dats->base_item_types->get(baseItemTypesKey, bit_col_inherits_from);
            size_t bit_ivi_key = dats->base_item_types->get(baseItemTypesKey, bit_col_ivi_key);
            auto bit_hash = dats->base_item_types->get(baseItemTypesKey, bit_col_hash);
            cdi.id_ = std::string(bit_id);
            cdi.name_ = std::string(bit_name);
            cdi.dds_name_ = dats->item_visual_identity->get(bit_ivi_key, ivi_col_ddsfile);
            cdi.cell_extent_ = ImVec2(bit_width, bit_height);
            cdi.hash_ = bit_hash;
            cdi.inherits_from_ = bit_inherits_from;
            return cdi;
        };

        std::function<std::optional<std::string>(std::shared_ptr<poe::io::file_handle>)>
            FindAnimatedObjectFromObjectType =
                [&](std::shared_ptr<poe::io::file_handle> otFile) -> std::optional<std::string> {
            if (!otFile) {
                return std::nullopt;
            }
            auto otData = otFile->read_all();
            std::u16string_view wideText((char16_t const *)otData.data(),
                                         (char16_t const *)otData.data() + otData.size() / 2);
            auto text = poe::util::to_string(wideText);
            std::regex extends_re(R"re(extends "([^"]+)")re");
            std::regex mesh_re(R"re(animated_object\s*=\s*"([^"]+)")re");

            std::optional<std::string> extends;
            std::smatch m;
            if (std::regex_search(text, m, extends_re)) {
                if (m[1].str() != "nothing"sv) {
                    extends = m[1].str();
                }
            }
            if (std::regex_search(text, m, mesh_re)) {
                return m[1].str();
            }
            if (extends) {
                auto nextOtPath = *extends + ".ot";
                return FindAnimatedObjectFromObjectType(vfs->open_file(poe::io::path(nextOtPath)));
            }
            return std::nullopt;
        };

        tbb::parallel_for(tbb::blocked_range<size_t>(0, hd_row_count), [&](tbb::blocked_range<size_t> const &r) {
            for (size_t i = r.begin(); i != r.end(); ++i) {
                auto hd_bit_key = (size_t)dats->hideout_doodads->get(i, hd_col_bit_key);
                auto hd_inherits_from = (std::string)dats->hideout_doodads->get(i, hd_col_inherits_from);
                auto hd_var_aos = dats->hideout_doodads->get(i, hd_col_var_aos);

                cubby_decoration_item cdi = LoadDecorationItem(hd_bit_key);

                if (hd_var_aos.empty()) {
                    // Check if there's a parent object type
                    auto otPath = std::string(hd_inherits_from) + ".ot";
                    if (auto otFile = vfs->open_file(poe::util::path(otPath))) {
                        // LOG_F(INFO, "No-var CDI {} has OT file: {}", cdi.id_, otPath);
                        auto aoPath = FindAnimatedObjectFromObjectType(otFile);
                        if (!aoPath) {
                            LOG_F(WARNING, "No-var CDI {} has OT file with no AO: {}", cdi.id_, otPath);
                        } else {
                            auto var = LoadVariant(*aoPath);
                            if (!var) {
                                LOG_F(WARNING, ".ot-sourced variant {} failed to load", *aoPath);
                                var = CubbyDecorationVariant{};
                            }
                            cdi.variants_.push_back(std::move(*var));
                        }
                    } else {
                        LOG_F(WARNING, "No-var CDI {} has no OT file: {}", cdi.id_, hd_inherits_from);
                    }
                } else {
                    for (std::string ao : hd_var_aos) {
                        auto var = LoadVariant(ao);
                        if (!var) {
                            LOG_F(WARNING, "Variation from {} failed to load", ao);
                            var = CubbyDecorationVariant{};
                        }
                        cdi.variants_.push_back(std::move(*var));
                    }
                }

                // if (!decoration_textures.contains(cdi.dds_name_)) {
                //     pendingTextures[i] = loadTextureOnMainThread(cdi.dds_name_);
                // }
                decoration_idx_by_hash[cdi.hash_] = i;
                all_decorations[i] = std::move(cdi);
            }
        });

        for (size_t i = 0; i < pet_row_count; ++i) {
            size_t dstIdx = pet_row_start + i;
            auto pet_id = pet_dat->get(i, pet_col_id);
            auto pet_hash32 = pet_dat->get(i, pet_col_hash32);
            auto pet_bit_key = pet_dat->get(i, pet_col_bit_key);

            cubby_decoration_item cdi = LoadDecorationItem(pet_bit_key);

            std::string petBase = pet_id;
            auto otPath = poe::util::path(petBase + ".ot");

            std::shared_ptr<poe::io::file_handle> aocFile;
            auto otFile = vfs->open_file(otPath);
            if (!otFile) {
                LOG_F(WARNING, "Pet .ot file {} not found", otPath.path_);
            } else {
                auto aoPath = FindAnimatedObjectFromObjectType(otFile);
                if (!aoPath) {
                    LOG_F(WARNING, ".ot had no .ao: {}", otPath.path_);
                } else {
                    auto aocPath = *aoPath + "c";
                    aocFile = vfs->open_file(poe::util::path(aocPath));
                    if (!aocFile) {
                        LOG_F(WARNING, ".ot-sourced .aoc file {} not found", aocPath);
                    } else {
                        auto var = LoadVariant(*aoPath);
                        if (!var) {
                            LOG_F(WARNING, ".ot-sourced variant {} failed to load", *aoPath);
                            var = CubbyDecorationVariant{};
                        }
                        cdi.variants_.push_back(std::move(*var));
                    }
                }
            }

            decoration_idx_by_hash[cdi.hash_] = dstIdx;
            all_decorations[dstIdx] = std::move(cdi);
        }

        for (auto &[cdiIdx, fut] : pendingTextures) {
            auto tex = fut.get();
            if (tex) {
                all_decorations[cdiIdx].tex_2dart_ = tex;
            }
        }
    }
    return hideoutMeta;
}

namespace cubby_qt {
struct SelectRectTool : ToolBase {};

struct SelectFreeTool : ToolBase {};

MainWindow::MainWindow() {
    setupUi(this);
    auto commandActionGroup = new QActionGroup(this);
    actionSelectRect->setActionGroup(commandActionGroup);
    actionSelectFree->setActionGroup(commandActionGroup);

    objectsModel_ = new QStandardItemModel(this);
    objectsTreeView->setModel(objectsModel_);

    // renderView->hide();

    vfsFut_ = std::async(std::launch::async, [] {
                  auto vfs = std::make_shared<poe::io::vfs>(poe::io::file_collective_info{
                      //.collective_root_ = "F:/Temp/poe/collective",
                      //.build = 7613843,
                      .build = 8077886,
                  });
                  if (!vfs) {
                      throw std::runtime_error("could not open vfs");
                  }
                  return vfs;
              }).share();
    auto datsFut = std::async(std::launch::async, [vfsFut = vfsFut_] {
                       auto dats = std::make_shared<DatSet>(vfsFut.get());
                       return dats->Load().get();
                   }).share();

    vfs_ = vfsFut_.get();
    parseCache_ = std::make_shared<cubby::ParseCache>(vfs_);
    hideoutMeta_ = LoadHideoutMeta(parseCache_);

    auto layout = new QVBoxLayout(decoScrollContents);

    decoButtonGroup_ = new QButtonGroup(this);
    for (auto &decoration : hideoutMeta_->all_decorations) {
        auto labelText = QString::fromUtf8(decoration.name_);
        auto *button = new QPushButton(labelText);
        decoButtonGroup_->addButton(button);
        layout->addWidget(button);
    }

    renderView->Run();

    // auto tsViewer = new cubby_qt::TileSetsViewer(hideoutMeta_, this);
    // tsViewer->show();

    LoadHideout(R"(C:\Users\Lars\Documents\Last stop to Sarn-1.0.hideout)");
}

void MainWindow::on_actionOpen_triggered() {
    LOG_F(INFO, "Open stuff");
    auto documentsDir = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).front();
    auto fileName = QFileDialog::getOpenFileName(
        this, tr("Open Hideout/Project"), documentsDir,
        tr("All Supported (*.cubby, *.hideout);;Cubbyhole Project (*.cubby);;Hideout Export (*.hideout)"));
    if (!fileName.isEmpty()) {
        LoadHideout(fileName);
    }
}

void MainWindow::on_actionSelectRect_toggled(bool checked) {
    if (checked) {
        LOG_F(INFO, "Select rect tool active.");
        currentTool_ = std::make_shared<SelectRectTool>();
    } else {
        LOG_F(INFO, "Select rect tool inactive.");
        currentTool_.reset();
    }
}

void MainWindow::on_actionSelectFree_toggled(bool checked) {
    if (checked) {
        LOG_F(INFO, "Select free tool active.");
        currentTool_ = std::make_shared<SelectFreeTool>();
    } else {
        LOG_F(INFO, "Select free tool inactive.");
        currentTool_.reset();
    }
}

struct DoodadGraphicsItem : QGraphicsRectItem {
    explicit DoodadGraphicsItem(QGraphicsItem *parent = nullptr) : QGraphicsRectItem(parent) {}
};

struct HideoutTile {
    glm::ivec2 size;
    char kind;
    std::vector<int64_t> values;
    std::optional<std::string> tag;
};

struct GlmLess {
    bool operator()(glm::ivec2 a, glm::ivec2 b) const {
        if (a.x != b.x) {
            return a.x < b.x;
        }
        return a.y < b.y;
    }
};

struct HideoutShape {
    std::map<glm::ivec2, HideoutTile, GlmLess> tiles;
};

std::shared_ptr<HideoutShape> MainWindow::LoadHideoutShape(uint16_t hideoutHash) {
    using poe::format::arm::FEntry;
    using poe::format::arm::KEntry;
    using poe::format::arm::NEntry;
    using poe::format::arm::OEntry;
    using poe::format::arm::SEntry;

    auto ret = std::make_shared<HideoutShape>();
    if (auto I = hideoutMeta_->layouts.find(hideoutHash); I != hideoutMeta_->layouts.end()) {
        auto &layout = I->second;
        for (auto &room : layout.rooms) {
            size_t k_rows, k_cols;
            if (auto *kEntry = std::get_if<KEntry>(&room.arm->k_1_by_1_.var)) {
                glm::ivec2 pos;
                pos.x = room.x_pos;
                pos.y = room.y_pos;

                k_rows = kEntry->h_;
                k_cols = kEntry->w_;

                HideoutTile tile;
                tile.size.x = (int)kEntry->w_;
                tile.size.y = (int)kEntry->h_;
                tile.kind = 'K';

                ret->tiles[pos] = tile;
            } else {
                continue;
            }

            for (int y_idx = 0; y_idx < k_rows; ++y_idx) {
                auto &row = room.arm->k_x_by_y_[y_idx];
                for (int x_idx = 0; x_idx < k_cols; ++x_idx) {
                    auto &[col] = row[x_idx];
                    glm::ivec2 pos;
                    pos.x = room.x_pos + x_idx;
                    pos.y = room.y_pos + y_idx;
                    HideoutTile tile;
                    tile.size = glm::ivec2(1, 1);
                    if (auto *fEntry = std::get_if<FEntry>(&col)) {
                        tile.kind = 'F';
                        tile.values.assign(1, fEntry->value_);
                    } else if (auto *kEntry = std::get_if<KEntry>(&col)) {
                        tile.size.x = (int)kEntry->w_;
                        tile.size.y = (int)kEntry->h_;
                        tile.kind = 'K';
                        tile.values = kEntry->tail_;
                        tile.tag = room.arm->TableString(tile.values[(int)poe::format::arm::KSlot::Tag]);

                        if (kEntry->last_) {
                            tile.values.push_back(*kEntry->last_);
                        }
                    } else if (auto *nEntry = std::get_if<NEntry>(&col)) {
                        continue;
                    } else if (auto *oEntry = std::get_if<OEntry>(&col)) {
                        tile.kind = 'O';
                    } else if (auto *sEntry = std::get_if<SEntry>(&col)) {
                        tile.kind = 'S';
                    } else {
                        continue;
                    }
                    ret->tiles[pos] = tile;
                }
            }
        }
    }
    return ret;
}

struct BaseGridItem : QGraphicsRectItem {
    BaseGridItem(QRectF area, float majorSeparation, int minorCount, QGraphicsItem *parent = nullptr)
        : QGraphicsRectItem(parent), majorPen(QColor("darkgray")), minorPen(QColor("lightgray")) {
        float off = area.left();
        int minorIdx = 0;
        while (off <= area.right()) {
            AddVerticalLine(off, area.top(), area.bottom(), minorIdx == 0);
            ++minorIdx;
            minorIdx %= minorCount;
            off += majorSeparation / minorCount;
        }
        off = area.top();
        minorIdx = 0;
        while (off <= area.bottom()) {
            AddHorizontalLine(off, area.left(), area.right(), minorIdx == 0);
            ++minorIdx;
            minorIdx %= minorCount;
            off += majorSeparation / minorCount;
        }
    }

    void AddVerticalLine(float x, float y1, float y2, bool major) {
        auto *line = new QGraphicsLineItem(x, -y1, x, -y2, this);
        line->setPen(major ? majorPen : minorPen);
    }

    void AddHorizontalLine(float y, float x1, float x2, bool major) {
        auto *line = new QGraphicsLineItem(x1, -y, x2, -y, this);
        line->setPen(major ? majorPen : minorPen);
    }

    QPen majorPen, minorPen;
};

void MainWindow::LoadHideout(QString const &fileName) {
    float const objSeparation = 250.0f / 23.0f;
    float const tileSeparation = 250.0f;
    project_ = cubby::open_hideout(fileName.toStdU16String());
    if (project_) {
        setWindowTitle(QString("Cubbyhole - %1").arg(fileName));

        auto hideoutShape = LoadHideoutShape(project_->hideout_hash_);

        objectsModel_->clear();
        objectsModel_->setHorizontalHeaderLabels(QStringList{"Name"});

        editScene_ = new QGraphicsScene(this);
        QPointF viewMin(FLT_MAX, FLT_MAX), viewMax(-FLT_MAX, -FLT_MAX);

        // add origin axis widget
        auto xAxis = editScene_->addLine(0.0f, 0.0f, 1000.0f, 0.0f, QPen(QColor::fromRgbF(0.9f, 0.1f, 0.1f, 1.0f)));
        auto yAxis = editScene_->addLine(0.0f, 0.0f, 0.0f, -1000.0f, QPen(QColor::fromRgbF(0.1f, 0.9f, 0.1f, 1.0f)));
        xAxis->setRotation(-45.0);
        yAxis->setRotation(-45.0);

        QPointF zoneBounds(0.0f, 0.0f);
        auto tileGroup = editScene_->addRect({});
        tileGroup->setRotation(-45.0);
        QFont tileFont("Courier", 16.0);
        for (auto &[pos, tile] : hideoutShape->tiles) {
            float objX = (float)pos.x * tileSeparation;
            float objY = (float)pos.y * tileSeparation;
            float angRad = (float)-M_PI_4;

            glm::ivec2 size = glm::vec2(tile.size) * tileSeparation;
            zoneBounds.setX(std::max<qreal>(zoneBounds.x(), objX + size.x));
            zoneBounds.setY(std::max<qreal>(zoneBounds.y(), objY + size.y));
            QRectF extent(0.0, -size.y, size.x, size.y);
            std::map<char, QColor> colors{
                {'F', QColor("mediumpurple")}, {'K', QColor("limegreen")}, {'N', QColor("navy")},
                {'O', QColor("darkorange")},   {'S', QColor("moccasin")},
            };
            QColor color = colors[tile.kind];
            color.setAlphaF(0.3f);

            auto *item = new QGraphicsRectItem(extent, tileGroup);
            item->setPen(QPen());
            item->setBrush(QBrush(color));

            auto *note = new QGraphicsSimpleTextItem(item);
            std::optional<QString> noteText;
            switch (tile.kind) {
            case 'F':
                noteText = QString("F %1").arg(tile.values[0]);
                break;
            case 'K': {
                fmt::memory_buffer buf;
                if (tile.tag) {
                    fmt::format_to(fmt::appender(buf), "K \"{}\"", tile.tag.value());
                } else {
                    buf.append(std::string_view("K <No tag>"));
                }
                for (size_t i = 0; i < tile.values.size(); ++i) {
                    auto val = tile.values[i];
                    char const *sep = (i % 4 == 0) ? "\n" : " ";
                    fmt::format_to(fmt::appender(buf), "{}{}", sep, val);
                }
                noteText = QString::fromStdString(to_string(buf));
            } break;
            default:
                noteText = QString(tile.kind);
            }
            if (noteText) {
                note->setText(*noteText);
                note->setToolTip(*noteText);
            }
            note->setFont(tileFont);
            note->setPos(extent.topLeft());

            float x = cos(angRad) * objX + -sin(angRad) * objY;
            float y = sin(angRad) * objX + cos(angRad) * objY;

            item->setPos(objX, -objY);
        }

        // auto grid = new BaseGridItem(QRectF(0.0f, 0.0f, zoneBounds.x(), zoneBounds.y()), 10 * objSeparation, 10);
        // grid->setRotation(-45.0);
        // editScene_->addItem(grid);

        QPen itemStroke{};
        for (auto &obj : project_->objects_) {
            QBrush itemFill(QColor::fromRgbF(0.0f, 0.0f, 1.0f, 0.2f));

            cubby_decoration_item *metaItem{};
            if (auto meta = hideoutMeta_->decoration_idx_by_hash.find(obj.hash_);
                meta != hideoutMeta_->decoration_idx_by_hash.end()) {
                metaItem = &hideoutMeta_->all_decorations[meta->second];
            }
            objectsModel_->appendRow(new QStandardItem(QString::fromStdString(obj.name_)));

            QRectF itemRect;
            QColor errorColor;
            auto parent = std::make_unique<DoodadGraphicsItem>();
            DoodadGraphicsItem *item = parent.get();
            parent->setFlag(QGraphicsItem::ItemIsSelectable, true);
            QRectF markerRect(-objSeparation, -objSeparation, objSeparation, objSeparation);
            new QGraphicsEllipseItem(markerRect, parent.get());
            if (metaItem) {
                auto mesh = metaItem->mesh(obj.var_);
                if (mesh) {
                    auto bbox = mesh->bounding_box();
                    QPointF c(bbox.c.x, -bbox.c.y);
                    QPointF e(bbox.e.x, bbox.e.y);
                    itemRect = QRectF(c - e / 2.0f, c + e / 2.0f);
                    auto child = std::make_unique<QGraphicsRectItem>(itemRect, item);
                    child->setPen(itemStroke);
                    child->setBrush(itemFill);
                    child.release();
                    editScene_->addItem(parent.release());
                } else {
                    if (metaItem->variant_count() == 0) {
                        LOG_F(WARNING, "Variant {} of no-variant item not available: {}", obj.var_, metaItem->id_);
                        errorColor = QColor::fromRgbF(1.0f, 1.0f, 0.0f, 0.5f);
                    } else {
                        auto path = metaItem->mesh_path(obj.var_);
                        size_t fileSize = 0;
                        if (path) {
                            fileSize = vfs_->open_file(poe::util::path(*path))->size();
                        }
                        LOG_F(WARNING, "Variant {} ({} found) not available: {} ({} bytes): {}", obj.var_,
                              metaItem->variant_count(), path ? *path : "<no file>", fileSize, metaItem->id_);
                        errorColor = QColor::fromRgbF(1.0f, 0.0f, 0.0f, 0.5f);
                    }
                }
            } else {
                LOG_F(WARNING, "No item available: hash {}", obj.hash_);
                errorColor = QColor::fromRgbF(0.0f, 1.0f, 0.0f, 0.5f);
            }
            if (parent) {
                float sizeX = 100.0f, sizeY = 100.0f;
                itemRect = QRectF(-sizeX / 2.0f, -sizeY / 2.0f, sizeX, sizeY);
                auto child = std::make_unique<QGraphicsEllipseItem>(itemRect, item);
                auto itemFill = QBrush(errorColor);
                child->setPen(itemStroke);
                child->setBrush(itemFill);
                child.release();
                editScene_->addItem(parent.release());
            }

            float const objSeparation = 250.0f / 23.0f;
            float objX = (float)obj.x_ * objSeparation;
            float objY = -(float)obj.y_ * objSeparation;
            float angRad = (float)-M_PI_4;
            float x = cos(angRad) * objX + -sin(angRad) * objY;
            float y = sin(angRad) * objX + cos(angRad) * objY;

            item->setPos(x, y);
            item->setRotation(-45.0 + -obj.rot_ / 65536.0 * 360.0f);
            viewMin.setX(std::min<qreal>(viewMin.x(), x));
            viewMin.setY(std::min<qreal>(viewMin.y(), y));
            viewMax.setX(std::max<qreal>(viewMax.x(), x));
            viewMax.setY(std::max<qreal>(viewMax.y(), y));
        }

        QPointF viewMid = (viewMin + viewMax) / 2.0f;
        editView->setScene(editScene_);
        editView->centerOn(viewMid);
        float viewZoom = 0.7f;
        editView->scale(viewZoom, viewZoom);

        // editView->fitInView(editScene_->itemsBoundingRect(), Qt::KeepAspectRatio);
    }
}

void MainWindow::on_actionExit_triggered() { close(); }

void MainWindow::on_actionTop_down_View_toggled(bool checked) { editView->setVisible(checked); }

void MainWindow::on_action3D_View_toggled(bool checked) { renderView->setVisible(checked); }

} // namespace cubby_qt