#pragma once

#include <main_window.ui.hpp>
#include <loguru/loguru.hpp>

#include <QtGui/QStandardItemModel>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFileDialog>

#include "cubby/hideout_meta.hpp"
#include "cubby/parse_cache.hpp"
#include "cubby/project.hpp"
#include "poe/format/dat.hpp"
#include "poe/io/vfs.hpp"

struct DatSet : std::enable_shared_from_this<DatSet> {
    explicit DatSet(std::shared_ptr<poe::io::vfs> vfs);

    std::shared_ptr<poe::format::dat::dat_table> base_item_types, environments, hideout_doodads, hideouts,
        item_visual_identity, pet, topologies, world_areas;

    std::future<std::shared_ptr<DatSet>> Load();

  private:
    void LoadTable(std::shared_ptr<poe::format::dat::dat_table> &table, poe::util::path const &path);

    std::shared_ptr<poe::io::vfs> vfs_;
    int issueCount_ = 0;
    std::vector<poe::util::path> bgPaths_;
    std::vector<std::future<void>> bgResults_;
};

namespace cubby_qt {
struct DecorationData {
    struct Metadata {
        float bbox[6];
    };

    std::map<uint32_t, Metadata> metadata;
};

struct ToolBase {
    virtual ~ToolBase() = default;
};

struct HideoutShape;

class MainWindow : public QMainWindow, private Ui::MainWindow {
    Q_OBJECT

  public:
    MainWindow();

  signals:
    void project_loaded(std::shared_ptr<cubby::project> project);

  private slots:
    void on_actionOpen_triggered();
    void on_actionExit_triggered();

    void on_actionTop_down_View_toggled(bool checked);
    void on_action3D_View_toggled(bool checked);

    void on_actionSelectRect_toggled(bool checked);
    void on_actionSelectFree_toggled(bool checked);

  private:
    std::shared_ptr<HideoutShape> LoadHideoutShape(uint16_t hideoutHash);
    void LoadHideout(QString const &fileName);

    std::shared_ptr<cubby::project> project_;
    QStandardItemModel *objectsModel_;
    std::shared_ptr<poe::io::vfs> vfs_;
    std::shared_future<std::shared_ptr<poe::io::vfs>> vfsFut_;
    std::shared_ptr<cubby::ParseCache> parseCache_;

    std::shared_ptr<cubby::HideoutMeta> hideoutMeta_;

    QGraphicsScene *editScene_ = nullptr;

    std::shared_ptr<ToolBase> currentTool_;
    QButtonGroup *decoButtonGroup_;
};
} // namespace cubby_qt