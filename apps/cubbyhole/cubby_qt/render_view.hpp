#pragma once

#include <QtCore/QTimer>
#include <QtWidgets/QFrame>

#include <pog/dx_context.hpp>
#include <pog/view_host.hpp>

namespace cubby_qt {
struct RenderView : QFrame {
    explicit RenderView(QWidget *parent);

    void Run();
    void Stop();

  protected:
    bool nativeEvent(QByteArray const &eventType, void *message, qintptr *result) override;

    QPaintEngine *paintEngine() const override { return nullptr; }
    void paintEvent(QPaintEvent *) override {}
    void resizeEvent(QResizeEvent *ev) override;

    void onFrame();

  private:
    pog::DxContext dx_;
    std::unique_ptr<pog::ViewHost> viewHost_;
    std::unique_ptr<pog::LayerTarget> viewRt_;
    glm::ivec2 fbSize_{};

    QTimer updateTimer_;
};
} // namespace cubby_qt