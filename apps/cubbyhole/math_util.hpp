#pragma once

#include <optional>

#include <glm/glm.hpp>
#include <imgui.h>
#include <d2d1.h>

#include <gsl/span>

inline glm::vec2 to_glm(D2D1_POINT_2F v) { return {v.x, v.y}; }
inline ImVec2 to_im(D2D1_POINT_2F v) { return {v.x, v.y}; }

inline D2D1_POINT_2F to_d2d(glm::vec2 v) { return {v.x, v.y}; }
inline ImVec2 to_im(glm::vec2 v) { return {v.x, v.y}; }

inline D2D1_POINT_2F to_d2d(ImVec2 v) { return {v.x, v.y}; }
inline glm::vec2 to_glm(ImVec2 v) { return {v.x, v.y}; }

inline ImVec4 to_im(glm::vec4 v) { return {v.x, v.y, v.z, v.w}; }

inline glm::vec4 to_glm(ImVec4 v) { return {v.x, v.y, v.z, v.w}; }

struct AABB2D {
    glm::vec2 c;
    glm::vec2 e;
};

AABB2D aabb_from_corners(glm::vec2 a, glm::vec2 b);

std::optional<AABB2D> aabb_from_points(std::span<glm::vec2 const> points);

struct OBB2D {
    glm::vec2 c;
    std::array<glm::vec2, 2> u;
    glm::vec2 e;
};

float sq_dist_point_obb(glm::vec2 p, OBB2D b);

OBB2D transform(D2D1::Matrix3x2F const &m, AABB2D bbox);
OBB2D transform(D2D1::Matrix3x2F const &m, OBB2D bbox);