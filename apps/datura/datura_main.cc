#include <loguru/loguru.hpp>

#include "ui_common.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <poe/format/dat.hpp>
#include <poe/io/vfs.hpp>
#include <poe/util/install_location.hpp>
#include <poe/util/memmem.hpp>
#include <poe/util/path.hpp>

#include <fmt/format.h>

#include <asio.hpp>

#include <nlohmann/json.hpp>

#include <algorithm>
#include <charconv>
#include <fstream>
#include <iostream>
#include <memory>
#include <regex>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <thread>
#include <unordered_set>
#include <vector>

#include <datura/dat_archive.hpp>
#include <datura/specification.hpp>

using namespace std::literals::string_view_literals;

std::filesystem::path get_program_data_dir() { return poe::util::data_local_root() / "zao/datura"; }

int main(int argc, char **argv) {
    loguru::init(argc, argv);
    asio::thread_pool pool{6};
    auto pool_wg = asio::make_work_guard(pool);

    auto prog_data_dir = get_program_data_dir();
    std::filesystem::create_directories(prog_data_dir);

    ui_common::ui_context ui("Datura", ui_common::ui_flags_use_default_fonts);
    std::map<std::string, std::string> shown_files_contents;
    std::map<std::string, bool> shown_files_stay_open;

    ImFont *sans_font{}, *mono_font{};
    {
        auto exe_dir = poe::util::executable_dir();
        if (exe_dir) {
            auto sans_file = *exe_dir / "Roboto-Regular.ttf";
            if (exists(sans_file)) {
                sans_font = ui.io_->Fonts->AddFontFromFileTTF(sans_file.string().c_str(), 18.0f * ui.dpi_scale_factor_);
                ui.io_->FontDefault = sans_font;
            }
            auto mono_file = *exe_dir / "RobotoMono-Regular.ttf";
            if (exists(mono_file)) {
                mono_font = ui.io_->Fonts->AddFontFromFileTTF(mono_file.string().c_str(), 18.0f * ui.dpi_scale_factor_);
            }
        }
    }

    std::shared_ptr<datura::DatArchive> archive = datura::DatArchive::from_file(prog_data_dir / "archive");

    bool show_demo = false;

    struct ImportPackTool {
        explicit ImportPackTool(datura::DatArchive &archive) : archive_(archive) {}

        void open() { open_ = true; }

        void run() {
            if (open_) {
                if (ImGui::Begin("Import pack", &open_)) {
                    ImGui::InputTextWithHint("Build ID (Steam)", "6454148", &build_id_,
                                             ImGuiInputTextFlags_AutoSelectAll | ImGuiInputTextFlags_CharsHexadecimal);
                    ImGui::InputTextWithHint("Tag ID (User-defined)", "3.13-preload", &tag_id_,
                                             ImGuiInputTextFlags_AutoSelectAll);
                    ImGui::InputText("Pack path", &pack_path_, ImGuiInputTextFlags_AutoSelectAll);
                    if (ImGui::Button("Import")) {
                        import();
                    }
                }
                ImGui::End();
            }
        }

      private:
        void import() {
            std::string_view p = pack_path_;
            if (p.starts_with('"')) {
                if (p.size() < 2 || !p.ends_with('"')) {
                    return;
                }
                p = p.substr(1, p.size() - 2);
            }
            std::filesystem::path path(p);
            if (!build_id_.empty() && archive_.has_build(build_id_)) {
                return;
            }
            if (!tag_id_.empty() && archive_.has_tag(tag_id_)) {
                return;
            }
            if (exists(path)) {
                try {
                    auto vfs = std::make_shared<poe::io::vfs>(path);
                    auto pack_id = archive_.import_pack(*vfs);
                    if (pack_id && !build_id_.empty()) {
                        archive_.set_pack_build(*pack_id, build_id_);
                    }
                    if (pack_id && !tag_id_.empty()) {
                        archive_.set_pack_tag(*pack_id, tag_id_);
                    }
                } catch (std::exception &e) {
                    LOG_F(ERROR, "{}", e.what());
                }
            }
        }

        datura::DatArchive &archive_;
        bool open_{};
        std::string build_id_;
        std::string tag_id_;
        std::string pack_path_;
    };

    ImportPackTool import_pack_tool(*archive);

    struct OpenPack {
        datura::NamedPack pack;
        datura::DatArchiveFileset files;
    };

    std::vector<OpenPack> open_packs;

    struct Variations {
        // Language, Extension
        using Key = std::tuple<std::string, std::string>;
        struct Value {
            datura::FileHash hash;
            std::vector<std::byte> contents;
            struct Shape {
                uint32_t row_count;
                uint32_t row_width;
            };
            Shape shape;
        };

        std::map<Key, Value> variant;
    };

    struct OpenDat {
        datura::NamedPack pack;
        std::string name;
        Variations vars;
        std::set<Variations::Key> shown_vars;

        bool show_spec{};
    };

    std::vector<OpenDat> open_dats;

    {
        nlohmann::ordered_json js;
        std::ifstream is(R"(C:\Code\poe\cubbyhole\repos\poe-cpp\libpoe\spec\stable.json)");
        try {
            if (is >> js) {
                datura::Specification::from_pypoe_json(js);
            }
        } catch (std::exception &e) {
            LOG_F(ERROR, "{}", e.what());
        }
    }

    while (ui.new_frame()) {
        if (ImGui::BeginMenuBar()) {
            if (ImGui::MenuItem("ImGui Demo")) {
                show_demo = true;
            }

            ImGui::EndMenuBar();
        }
        ImGui::End(); // For main window, do menu above this

        if (show_demo) {
            ImGui::ShowDemoWindow(&show_demo);
        }

        if (ImGui::Begin("Tools")) {
            if (ImGui::Button("Import pack")) {
                import_pack_tool.open();
            }
        }
        ImGui::End();

        import_pack_tool.run();

        if (ImGui::Begin("Packs")) {
            // ImGui::Text("Distinct hash count: %zu", archive->get_all_content_hashes().size());
            auto packs = archive->get_packs();
            for (auto const &pack : packs) {
                char const *build_text = pack.build_id ? pack.build_id->c_str() : "[unset]";
                char const *tag_text = pack.tag_id ? pack.tag_id->c_str() : "[unset]";
                std::string label =
                    fmt::format("Build: {}, tag: {}, ID: {{{}}}", build_text, tag_text, pack.pack_id.str());
                if (ImGui::Button(label.c_str())) {
                    if (auto op_I =
                            std::find_if(open_packs.begin(), open_packs.end(),
                                         [&pack](OpenPack const &cand) { return pack.pack_id == cand.pack.pack_id; });
                        op_I == open_packs.end()) {
                        auto files = archive->get_pack_files(pack.pack_id);
                        open_packs.push_back(OpenPack{
                            .pack = pack,
                            .files = *files,
                        });
                    }
                }
            }
        }
        ImGui::End();

        std::vector<xg::Guid> packs_to_close;
        for (auto &pack : open_packs) {
            bool pack_open = true;
            std::string win_id;
            {
                fmt::memory_buffer buf;
                if (pack.pack.build_id) {
                    format_to(fmt::appender(buf), "{}", *pack.pack.build_id);
                } else {
                    format_to(fmt::appender(buf), "{}", pack.pack.pack_id.str());
                }
                format_to(fmt::appender(buf), "##{}", pack.pack.pack_id.str());
                win_id = to_string(buf);
            }
            if (ImGui::Begin(win_id.c_str(), &pack_open)) {
                std::map<std::string, Variations> dats;
                for (auto const &file : pack.files.files) {
                    auto path = poe::util::path(file.filename);
                    std::string language;
                    if (path.components_.size() == 3) {
                        language = path.component(path.components_[1]);
                    } else {
                        language = "English";
                    }
                    auto leaf = path.leaf();
                    auto ext = path.extension();
                    auto name = std::string(leaf.substr(0, leaf.find_first_of('.')));
                    Variations::Key key(language, ext);
                    dats[name].variant[key] = Variations::Value{
                        .hash = file.filehash,
                    };
                }
                for (auto &[name, vars] : dats) {
                    if (ImGui::Button(name.c_str())) {
                        auto od_I = std::find_if(open_dats.begin(), open_dats.end(),
                                                 [name = std::string_view(name)](OpenDat const &v) { return name == v.name; });
                        if (od_I == open_dats.end()) {
                            for (auto &[key, var] : vars.variant) {
                                auto contents = *archive->get_file_contents(var.hash);
                                Variations::Value::Shape shape;
                                auto p = contents.data();
                                auto n = contents.size();
                                memcpy(&shape.row_count, p, sizeof(uint32_t));
                                static std::array<uint8_t, 8> separator = {0xBB, 0xBB, 0xBB, 0xBB,
                                                                           0xBB, 0xBB, 0xBB, 0xBB};
                                auto data_ptr = reinterpret_cast<std::byte const *>(
                                    poe::util::memmem(p + 4, n - 4, separator.data(), separator.size()));
                                size_t fixed_len = data_ptr - p - 4;
                                shape.row_width = (uint32_t)fixed_len / shape.row_count;
                                var.contents = std::move(contents);
                                var.shape = shape;
                            }
                            open_dats.push_back(OpenDat{
                                .pack = pack.pack,
                                .name = name,
                                .vars = std::move(vars),
                            });
                        }
                    }
                }
            }
            ImGui::End();
            if (!pack_open) {
                packs_to_close.push_back(pack.pack.pack_id);
            }
        }

        {
            auto I = open_packs.begin();
            for (auto &to_close : packs_to_close) {
                while (I->pack.pack_id != to_close) {
                    ++I;
                }
                I = open_packs.erase(I);
            }
        }

        std::vector<std::string> dats_to_close;
        for (auto &dat : open_dats) {
            bool dat_open = true;
            std::string win_id = fmt::format("{name}##view-dat-{name}", fmt::arg("name", dat.name));
            if (ImGui::Begin(win_id.c_str(), &dat_open, ImGuiWindowFlags_HorizontalScrollbar)) {
                if (ImGui::Button("Show spec")) {
                    dat.show_spec = true;
                }
                if (ImGui::ListBoxHeader("Variants")) {
                    for (auto &[key, var] : dat.vars.variant) {
                        bool selected = dat.shown_vars.contains(key);
                        if (ImGui::Selectable(
                                fmt::format("{}{}##show", std::get<0>(key).c_str(), std::get<1>(key).c_str()).c_str(),
                                &selected)) {
                            if (selected) {
                                dat.shown_vars.insert(key);
                            } else {
                                dat.shown_vars.erase(key);
                            }
                        }
                    }
                    ImGui::ListBoxFooter();
                }
                std::vector<std::string_view> extensions{".dat", ".dat64", ".datl", ".datl64"};
                std::map<std::string, std::vector<Variations::Value const *>> by_ext;
                std::map<std::string, std::vector<Variations::Value const *>> by_bitness;
                for (auto const &[key, var] : dat.vars.variant) {
                    auto [language, extension] = key;
                    bool is_32bit = extension == ".dat" || extension == ".datl";
                    auto bits_str = is_32bit ? "32"sv : "64"sv;
                    by_bitness[(std::string)bits_str].push_back(&var);
                    by_ext[extension].push_back(&var);

                    /*ImGui::Text("%s %s: %u rows, %zu width, %zu bytes", language.c_str(), extension.c_str(),
                                var.shape.row_count, var.shape.row_width, var.contents.size());*/
                }

                ImGui::PushFont(mono_font);
                for (auto const &[bits, vars] : by_bitness) {
                    auto first = vars[0];
                    auto shape = first->shape;
#if 0
                    std::vector<uint8_t> identical(shape.row_width, 1);
                    for (size_t col_byte_idx = 0; col_byte_idx < shape.row_width; ++col_byte_idx) {
                        for (size_t row_idx = 0; row_idx < shape.row_count; ++row_idx) {
                            bool byte_identical = true;
                            auto first_b = first->contents[4 + row_idx * shape.row_width + col_byte_idx];
                            for (auto var_I = vars.begin() + 1; var_I != vars.end(); ++var_I) {
                                auto b = (*var_I)->contents[4 + row_idx * shape.row_width + col_byte_idx];
                                byte_identical = byte_identical && (first_b == b);
                            }
                            identical[col_byte_idx] = byte_identical;
                        }
                    }
                    fmt::memory_buffer buf;
                    for (size_t i = 0; i < first->shape.row_width; ++i) {
                        format_to(fmt::appender(buf), " {}", identical[i] ? ".." : "!!");
                    }
                    ImGui::Text("%s", to_string(buf).c_str());
#endif

                    if (shape.row_count && shape.row_width) {
                        fmt::memory_buffer hdr_buf;
                        ImGuiListClipper clipper;
                        clipper.Begin((std::min)(10u, shape.row_count));
                        while (clipper.Step()) {
                            for (size_t row_idx = clipper.DisplayStart; row_idx < clipper.DisplayEnd; ++row_idx) {
                                fmt::memory_buffer hex_buf;
                                auto *p = first->contents.data() + 4 + row_idx * shape.row_width;
                                for (size_t byte_idx = 0; byte_idx < shape.row_width; ++byte_idx) {
                                    format_to(fmt::appender(hex_buf), " {:02X}", (uint8_t)p[byte_idx]);
                                }
                                ImGui::TextUnformatted(to_string(hex_buf).c_str());
                            }
                        }
                    }
                }
                ImGui::PopFont();
            }
            ImGui::End();

            if (dat.show_spec) {
                std::string win_id = fmt::format("{name} spec##view-dat-spec", fmt::arg("name", dat.name));
                if (ImGui::Begin(win_id.c_str(), &dat.show_spec)) {
                }
                ImGui::End();
            }

            if (!dat_open) {
                dats_to_close.push_back(dat.name);
            }
        }

        {
            for (auto I = open_dats.begin(); auto &to_close : dats_to_close) {
                while (I->name != to_close) {
                    ++I;
                }
                I = open_dats.erase(I);
            }
        }

        ImGui::Render();

        int win_w, win_h;
        glfwGetFramebufferSize(ui.win_, &win_w, &win_h);
        glViewport(0, 0, win_w, win_h);
        glClearColor(0.1f, 0.2f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(ui.win_);
    }

    pool_wg.reset();
    pool.join();

    return 0;
}
