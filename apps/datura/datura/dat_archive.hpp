#pragma once

#include <filesystem>
#include <memory>
#include <optional>
#include <string>
#include <string_view>

#include <poe/io/vfs.hpp>

#include <crossguid/guid.hpp>
#include <leveldb/db.h>
#include <sqlite3.h>

namespace datura {
using FileHash = poe::util::sha256_digest;

struct DatArchiveFile {
    xg::Guid pack_id;
    FileHash filehash;
    std::string filename;
};

struct DatArchiveFileset {
    std::vector<DatArchiveFile> files;
};

struct NamedPack {
    xg::Guid pack_id;
    std::optional<std::string> build_id;
    std::optional<std::string> tag_id;
};

struct DatArchive {
    static std::unique_ptr<DatArchive> from_file(std::filesystem::path db_path);
    DatArchive &operator=(DatArchive const &) = delete;
    DatArchive(DatArchive const &) = delete;

    std::optional<xg::Guid> import_pack(poe::io::vfs const &vfs);
    std::optional<xg::Guid> import_pack(sqlite3 *db, std::string_view manifest_id);
    void set_pack_build(xg::Guid pack_id, std::string_view build_id);
    void set_pack_tag(xg::Guid pack_id, std::string_view tag);
    std::optional<std::string> get_pack_build(xg::Guid id);
    std::optional<std::string> get_pack_tag(xg::Guid id);
    void declare_pack_order(xg::Guid pack_before, xg::Guid pack_after);

    bool has_build(std::string_view build_id) const;
    bool has_tag(std::string_view tag_id) const;
    std::vector<NamedPack> get_packs() const;
#if 0
    std::optional<std::string> get_first_build_id() const;
    std::optional<std::string> get_last_build_id() const;
    std::optional<std::string> get_previous_build_id(std::string_view build_id) const;
    std::optional<std::string> get_next_build_id(std::string_view build_id) const;
#endif
    std::optional<DatArchiveFileset> get_pack_files(xg::Guid pack_id) const;
    DatArchiveFileset get_file_history(std::string_view filename) const;

    std::vector<std::string> get_all_content_hashes() const;

    std::optional<std::vector<std::byte>> get_file_contents(FileHash content_hash) const;

  private:
    explicit DatArchive(std::filesystem::path db_path);

    std::unique_ptr<leveldb::Iterator> make_db_iter() const;

    std::unique_ptr<leveldb::DB> db_;
};
} // namespace datura