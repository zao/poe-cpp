#include <datura/dat_archive.hpp>

#include <poe/util/sha256.hpp>

#include <mutex>
#include <set>

namespace datura {
std::unique_ptr<DatArchive> DatArchive::from_file(std::filesystem::path db_path) {
    try {
        return std::unique_ptr<DatArchive>(new DatArchive(db_path));
    } catch (std::exception &) {
        return {};
    }
}

DatArchive::DatArchive(std::filesystem::path db_path) {
    leveldb::DB *db;
    leveldb::Options options;
    options.create_if_missing = true;
    leveldb::Status status = leveldb::DB::Open(options, db_path.string(), &db);
    if (db) {
        db_.reset(db);
    }
    if (!status.ok()) {
        throw std::runtime_error("Could not create/open DatArchive database");
    }
}

template <typename SizedByteData>
typename std::enable_if_t<sizeof(*std::declval<SizedByteData>().data()) == 1, leveldb::Slice>
to_slice(SizedByteData const &data) {
    return leveldb::Slice((char const *)data.data(), data.size());
}

std::string_view to_string_view(leveldb::Slice slice) { return std::string_view(slice.data(), slice.size()); }

using namespace std::string_view_literals;

static auto s_tag_pack_index = "pack-index;"sv;
static auto s_tag_file_content = "file-content;"sv;
static auto s_tag_pack_build = "pack-build;"sv;
static auto s_tag_build_pack = "build-pack;"sv;
static auto s_tag_pack_tag = "pack-tag;"sv;
static auto s_tag_tag_pack = "tag-pack;"sv;

std::optional<xg::Guid> DatArchive::import_pack(poe::io::vfs const &vfs) {
    auto new_id = xg::newGuid();
    std::string paths_index = fmt::format("{}{}", s_tag_pack_index, new_id.str());
    fmt::memory_buffer paths_buf;
    leveldb::ReadOptions iter_options;
    iter_options.fill_cache = false;
    std::unique_ptr<leveldb::Iterator> db_iter(db_->NewIterator(iter_options));
    bool success = true;
    vfs.enumerate_files([&](poe::util::path path) {
        auto ext = path.extension();
        if (ext == ".dat" || ext == ".dat64" || ext == ".datl" || ext == ".datl64") {
            auto file = vfs.open_file(path);
            if (file) {
                auto file_data = file->read_all();
                auto content_hash = poe::util::oneshot_sha256(file_data);
                auto hash_string = poe::util::digest_to_string(content_hash);
                format_to(fmt::appender(paths_buf), "{};{}\n", path.path_, hash_string);

                auto key_text = fmt::format("{}{}", s_tag_file_content, hash_string);
                db_iter->Seek(key_text);
                // Only insert if there's no matching content hash already
                if (!db_iter->Valid() || db_iter->key() != key_text) {
                    success = db_->Put({}, key_text, to_slice(file_data)).ok();
                }
            }
        }
        return success;
    });
    if (success) {
        auto const &index_key = paths_index;
        auto index_val = to_slice(paths_buf);
        db_->Put({}, index_key, index_val);
    }
    return success ? std::optional(new_id) : std::nullopt;
}

void DatArchive::set_pack_build(xg::Guid pack_id, std::string_view build_id) {
    std::string pack_id_str = pack_id.str();
    std::string pack_build_key = fmt::format("{}{}", s_tag_pack_build, pack_id_str);
    std::string build_pack_key = fmt::format("{}{}", s_tag_build_pack, build_id);
    db_->Put({}, pack_build_key, to_slice(build_id));
    db_->Put({}, build_pack_key, pack_id_str);
}

void DatArchive::set_pack_tag(xg::Guid pack_id, std::string_view tag_id) {
    std::string pack_id_str = pack_id.str();
    std::string pack_tag_key = fmt::format("{}{}", s_tag_pack_tag, pack_id_str);
    std::string tag_pack_key = fmt::format("{}{}", s_tag_tag_pack, tag_id);
    db_->Put({}, pack_tag_key, to_slice(tag_id));
    db_->Put({}, tag_pack_key, pack_id_str);
}

bool DatArchive::has_build(std::string_view build_id) const {
    std::unique_ptr<leveldb::Iterator> db_iter(db_->NewIterator({}));
    std::string index_key = fmt::format("{}{}", s_tag_build_pack, build_id);
    db_iter->Seek(index_key);
    return db_iter->Valid() && db_iter->key() == index_key;
}

bool DatArchive::has_tag(std::string_view tag_id) const {
    std::unique_ptr<leveldb::Iterator> db_iter(db_->NewIterator({}));
    std::string index_key = fmt::format("{}{}", s_tag_tag_pack, tag_id);
    db_iter->Seek(index_key);
    return db_iter->Valid() && db_iter->key() == index_key;
}

std::vector<NamedPack> DatArchive::get_packs() const {
    std::vector<NamedPack> packs;
    auto db_iter = make_db_iter();
    db_iter->Seek(to_slice(s_tag_pack_index));
    while (db_iter->Valid()) {
        auto key = to_string_view(db_iter->key());
        if (!key.starts_with(s_tag_pack_index)) {
            break;
        }
        std::string pack_id = std::string(key.substr(s_tag_pack_index.size()));
        NamedPack pack{
            .pack_id = xg::Guid(pack_id),
        };
        std::string build_id;
        std::string pack_build_key = fmt::format("{}{}", s_tag_pack_build, pack_id);
        if (db_->Get({}, pack_build_key, &build_id).ok()) {
            pack.build_id = build_id;
        }
        std::string tag_id;
        std::string pack_tag_key = fmt::format("{}{}", s_tag_pack_tag, pack_id);
        if (db_->Get({}, pack_tag_key, &tag_id).ok()) {
            pack.tag_id = tag_id;
        }
        packs.push_back(std::move(pack));
        db_iter->Next();
    }

    return packs;
}

std::optional<DatArchiveFileset> DatArchive::get_pack_files(xg::Guid pack_id) const {
    std::string pack_index;
    auto index_key = fmt::format("{}{}", s_tag_pack_index, pack_id.str());
    if (!db_->Get({}, index_key, &pack_index).ok()) {
        return {};
    }
    DatArchiveFileset fileset;
    std::istringstream iss(pack_index);
    std::string path, hash;
    while (std::getline(iss, path, ';') && std::getline(iss, hash)) {
        fileset.files.push_back(DatArchiveFile{
            .pack_id = pack_id,
            .filehash = poe::util::text_to_sha256_digest(hash),
            .filename = path,
        });
    }
    std::sort(fileset.files.begin(), fileset.files.end(),
              [](DatArchiveFile const &a, DatArchiveFile const &b) { return a.filename < b.filename; });
    return fileset;
}

std::vector<std::string> DatArchive::get_all_content_hashes() const {
    auto db_iter = make_db_iter();
    db_iter->Seek(to_slice(s_tag_file_content));
    std::set<std::string> hashes;
    while (db_iter->Valid()) {
        auto key = to_string_view(db_iter->key());
        if (!key.starts_with(s_tag_file_content)) {
            break;
        }
        hashes.insert(std::string(key.substr(s_tag_file_content.size())));
        db_iter->Next();
    }
    return std::vector(hashes.begin(), hashes.end());
}

std::optional<std::vector<std::byte>> DatArchive::get_file_contents(FileHash content_hash) const {
    auto db_iter = make_db_iter();
    auto hash_string = poe::util::digest_to_string(content_hash);
    auto key = fmt::format("{}{}", s_tag_file_content, hash_string);
    db_iter->Seek(key);
    if (db_iter->Valid() && db_iter->key() == key) {
        auto val = db_iter->value();
        auto p = (std::byte const *)val.data();
        return std::vector<std::byte>(p, p + val.size());
    }
    return {};
}

std::unique_ptr<leveldb::Iterator> DatArchive::make_db_iter() const {
    return std::unique_ptr<leveldb::Iterator>(db_->NewIterator({}));
}

} // namespace datura