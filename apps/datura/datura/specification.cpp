#include <datura/specification.hpp>
#include <loguru/loguru.hpp>

#include <map>

namespace datura {
using namespace std::string_view_literals;
using LookupValue = DatField::Type;

static std::string_view const s_key_ref_list = "ref|list|"sv;
static std::string_view const s_key_ref = "ref|"sv;

#define DATURA_DECLARE_TYPE(key, ty)                                                                                   \
    { key, DatField::Type::ty }
static std::map<std::string_view, LookupValue> const s_type_lookup = {
    DATURA_DECLARE_TYPE("bool"sv, Bool),
    DATURA_DECLARE_TYPE("byte"sv, Byte),
    DATURA_DECLARE_TYPE("float"sv, Float),
    DATURA_DECLARE_TYPE("int"sv, Int),
    DATURA_DECLARE_TYPE("long"sv, Long),
    DATURA_DECLARE_TYPE("ref|generic"sv, RefGeneric),
    DATURA_DECLARE_TYPE("ref|string"sv, RefString),
    DATURA_DECLARE_TYPE("short"sv, Short),
    DATURA_DECLARE_TYPE("uint"sv, UInt),
    DATURA_DECLARE_TYPE("ulong"sv, ULong),
};
#undef DATURA_DECLARE_TYPE

std::optional<Specification> Specification::from_pypoe_json(nlohmann::ordered_json value) {
    Specification spec;
    for (auto const &[filename, spec_js] : value.items()) {
        auto &file = spec.files_[filename];
        LOG_F(INFO, "Processing spec item \"{}\"", filename);
        for (auto const &[fieldname, field_js] : spec_js["fields"].items()) {
            DatField field;
            field.name = fieldname;

            std::string type_js = field_js["type"];
            std::string_view type = type_js;
            if (auto I = s_type_lookup.find(type); I != s_type_lookup.end()) {
                field.indirection = DatField::Indirection::None;
                field.type = I->second;
            } else {
                if (type.starts_with(s_key_ref_list)) {
                    field.indirection = DatField::Indirection::RefList;
                    if (auto J = s_type_lookup.find(type.substr(s_key_ref_list.size())); J != s_type_lookup.end()) {
                        field.type = J->second;
                    } else {
                        LOG_F(ERROR, "Could not match spec type \"{}\" for field \"{}\" in file \"{}\"", type,
                              fieldname, filename);
                        return {};
                    }
                } else if (type.starts_with(s_key_ref)) {
                    field.indirection = DatField::Indirection::RefList;
                    if (auto J = s_type_lookup.find(type.substr(s_key_ref.size())); J != s_type_lookup.end()) {
                        field.type = J->second;
                    } else {
                        LOG_F(ERROR, "Could not match spec type \"{}\" for field \"{}\" in file \"{}\"", type,
                              fieldname, filename);
                        return {};
                    }
                }
                else {
                    LOG_F(ERROR, "Could not match spec type \"{}\" for field \"{}\" in file \"{}\"", type,
                        fieldname, filename);
                    return {};
                }
            }

            file.fields.push_back(std::move(field));
        }
    }

    return spec;
}

std::optional<Specification> Specification::from_datura_json(nlohmann::json value) { return {}; }

nlohmann::json Specification::to_datura_json() const {
    nlohmann::json files;
    for (auto const &[filename, spec] : files_) {
        nlohmann::json fields;
        for (auto const &field : spec.fields) {
            fields.push_back({
                {"name", field.name},
                {"type", field.type},
                {"indirection", field.indirection},
            });
        }

        files.push_back({
            {"filename", filename},
            {"fields", fields},
        });
    }

    return {
        {"version", 1},
        {"files", files},
    };
}
} // namespace datura