#pragma once

#include <cstdint>
#include <optional>
#include <map>
#include <string>

#include <nlohmann/json.hpp>

namespace datura {
    struct DatField {
        enum class Indirection {
            None,
            Ref,
            RefList,
        };

        enum class Type {
            Bool,
            Byte,
            Float,
            Int,
            Long,
            RefGeneric,
            RefString,
            Short,
            UInt,
            ULong,
        };

        Indirection indirection;
        Type type;
        std::string name;
        std::optional<std::string> foreign_key;
    };

    struct DatSpec {
        std::vector<DatField> fields;
    };

    struct Specification {
        static std::optional<Specification> from_pypoe_json(nlohmann::ordered_json value);
        static std::optional<Specification> from_datura_json(nlohmann::json value);
        nlohmann::json to_datura_json() const;

    private:
        std::map<std::string, DatSpec> files_;
    };
}