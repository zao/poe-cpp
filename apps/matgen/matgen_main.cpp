#include <atlcomcli.h>
#include <d3dcompiler.h>

#include <absl/container/flat_hash_map.h>

#include <fmt/ostream.h>

#include <loguru/loguru.hpp>
#include <nlohmann/json.hpp>

#include <poe/format/mat.hpp>
#include <poe/io/vfs.hpp>

#include <fstream>
#include <regex>

#include <pog/ffx_library.hpp>
#include <pog/material.hpp>

using namespace std::chrono_literals;
using namespace std::string_view_literals;

namespace fxg_ns = poe::format::fxgraph;
namespace mat_ns = poe::format::mat;

std::string NodeName(poe::format::fxgraph::FxGraphKey &key) {
    return fmt::format("{}-{}-{}", key.type, key.index, key.stage.value_or("None"));
}

std::string NodeConnectorName(fxg_ns::FxGraphLink::Connector const &conn) {
    if (conn.swizzle) {
        return fmt::format("{variable}-{swizzle}", fmt::arg("variable", conn.variable),
                           fmt::arg("swizzle", conn.swizzle.value()));
    } else {
        return fmt::format("{variable}", fmt::arg("variable", conn.variable));
    }
}

std::string NodeConnectorLabel(fxg_ns::FxGraphLink::Connector const &conn) {
    auto name = NodeConnectorName(conn);
    if (conn.swizzle) {
        return fmt::format("<{name}> {variable}.{swizzle}", fmt::arg("name", name), fmt::arg("variable", conn.variable),
                           fmt::arg("swizzle", conn.swizzle.value()));
    } else {
        return fmt::format("<{name}> {variable}", fmt::arg("name", name), fmt::arg("variable", conn.variable));
    }
}

std::string EscapeQuotes(std::string s) {
    fmt::memory_buffer buf;
    for (auto ch : s) {
        if (ch != '"') {
            buf.append(&ch, &ch + 1);
        } else {
            buf.append(R"(\")"sv);
        }
    }
    return to_string(buf);
}

namespace ffx_ns = poe::format::ffx;

void VisualiseGraph(std::filesystem::path targetPath, pog::MaterialLibrary::CachedGraph const &g,
                    std::shared_ptr<pog::FFXLibrary> ffxLib, pog::MaterialLibrary const &matLib) {
    auto &graph = g.fxg;
    using namespace fmt::literals;
    std::error_code ec{};
    std::ofstream os(targetPath);
    os << R"(digraph g {
    graph [
        rankdir = "LR"
    ];
    node [
    ];
    edge [
    ];
)";
    using NodeIdx = size_t;
    struct NodeConnections {
        std::set<std::string> incoming;
        std::set<std::string> outgoing;
    };
    std::map<std::string, NodeConnections> connections;
    for (auto &link : graph->links) {
        auto srcKey = NodeName(link.src.key);
        auto dstKey = NodeName(link.dst.key);
        auto srcConn = NodeConnectorLabel(link.src);
        auto dstConn = NodeConnectorLabel(link.dst);
        connections[srcKey].outgoing.insert(srcConn);
        connections[dstKey].incoming.insert(dstConn);
    }

    auto &nodes = graph->nodes;
    std::vector<ffx_ns::Declarations const *> includes;
    std::vector<ffx_ns::MacroDeclaration const *> macros;
    std::set<std::string> cps;

    for (size_t nodeIdx = 0; nodeIdx < nodes.size(); ++nodeIdx) {
        auto &node = nodes[nodeIdx];
        auto name = NodeName(node.key);
        auto &conns = connections[name];

        std::optional<size_t> orderInStage;
        std::optional<size_t> derivedStage;
        for (auto &[stageIdx, nodes] : g.sortedNodesPerStage) {
            if (auto I = std::ranges::find(nodes, nodeIdx); I != nodes.end()) {
                orderInStage = std::distance(nodes.begin(), I);
                derivedStage = stageIdx;
                break;
            }
        }

        auto frag = ffxLib->LookupFragment(node.key.type);
        std::string fillColor = "white";
        std::optional<std::string> domain;
        if (frag) {
            for (auto &decl : frag->declarations_) {
                if (auto *kw = std::get_if<ffx_ns::KeywordDeclaration>(&decl)) {
                    if ("pixel"sv == kw->keyword) {
                        domain = "pixel";
                    } else if ("vertex"sv == kw->keyword) {
                        domain = "vertex";
                    }
                }
                if (auto *inc = std::get_if<ffx_ns::IncludeDeclaration>(&decl)) {
                    auto decls = ffxLib->LookupDeclarations(inc->target_);
                    includes.push_back(decls);
                }
                if (auto *macro = std::get_if<ffx_ns::MacroDeclaration>(&decl)) {
                    macros.push_back(macro);
                }
            }

            if (node.key.stage && matLib.LookupStage(node.key.stage.value()) != derivedStage) {
                fillColor = "lightpink";
            } else if (frag->HasKeyword("read")) {
                fillColor = "lightgreen";
            } else if (frag->HasKeyword("write")) {
                fillColor = "lightblue";
            }
        }

        auto label = fmt::format("{} #{} ({})", node.key.type, node.key.index, nodeIdx);
        if (node.key.stage) {
            label += fmt::format("|Stage: {}", node.key.stage.value());
        }
        if (orderInStage) {
            label += fmt::format("|Order: {}, {}", derivedStage.value(), orderInStage.value());
        }
        if (domain) {
            label += fmt::format("|Domain: {}", domain.value());
        }

        {
            if (node.customParameter) {
                label += fmt::format(R"(|CP: \"{}\")", node.customParameter.value());
                cps.insert(node.customParameter.value());
            }
            size_t paramIdx{};
            for (auto &param : node.parameters) {
                size_t subIdx{};
                for (auto &[key, val] : param.items()) {
                    label += fmt::format(R"(|P{}.{}: \"{}\" = {})", paramIdx, subIdx, key, EscapeQuotes(val.dump()));
                    ++subIdx;
                }
                ++paramIdx;
            }
        }

        auto inI = conns.incoming.begin();
        auto outI = conns.outgoing.begin();
        if (!conns.incoming.empty() || !conns.outgoing.empty()) {
            label += "|{{";
            std::string_view sep = ""sv;
            for (auto &in : conns.incoming) {
                label += fmt::format("{}{}", sep, in);
                sep = "|"sv;
            }
            label += "}|{";
            sep = ""sv;
            for (auto &out : conns.outgoing) {
                label += fmt::format("{}{}", sep, out);
                sep = "|"sv;
            }
            label += "}}";
            sep = ""sv;
#if 0
            while (true) {
                std::optional<std::string> left, right;
                if (inI != conns.incoming.end()) {
                    left = *inI;
                    ++inI;
                }
                if (outI != conns.outgoing.end()) {
                    right = *outI;
                    ++outI;
                }
                if (!left && !right) {
                    break;
                }
                if (left && right) {
                    label += fmt::format("| {{ {} | {} }}", *left, *right);
                } else if (left) {
                    label += fmt::format("| {{ {} | }}", *left);
                } else {
                    label += fmt::format("| {{ | {} }}", *right);
                }
            }
#endif
        }

        auto pos = glm::vec2(node.uiPosition) / 50.0f;
        fmt::print(os, R"(    "{name}" [
        label = "{label}"
        pos = "{x},{y}!"
        shape = "record"
        fillcolor = "{fillColor}"
        style = "filled"
    ];
)",
                   "name"_a = name, "label"_a = label, "x"_a = pos.x, "y"_a = pos.y, "fillColor"_a = fillColor);
    }

    for (auto &link : graph->links) {
        fmt::print(os, "    \"{}\":\"{}\" -> \"{}\":\"{}\";\n", NodeName(link.src.key), NodeConnectorName(link.src),
                   NodeName(link.dst.key), NodeConnectorName(link.dst));
    }

    {
        std::string label = "includes";
        for (size_t incIdx = 0; incIdx < includes.size(); ++incIdx) {
            auto &inc = includes[incIdx];
            label += fmt::format("|{}", inc->name_);
        }
        fmt::print(os, R"("includes" [label = "{}", shape="record"];
)",
                   label);
    }

    {
        std::string label = "macros";
        for (auto &macro : macros) {
            label += fmt::format("|{}", macro->name);
        }
        fmt::print(os, R"("macros" [label = "{}", shape="record"];
)",
                   label);
    }

    {
        std::string label = "custom params";
        for (auto &cp : cps) {
            label += fmt::format(R"(|{}\l)", cp);
        }
        fmt::print(os, R"("custom params" [label = "{}", shape="record"];
)",
                   label);
    }

    os << R"(}
)";
}

std::string ReplacesSlashes(std::string s) {
    for (auto &ch : s) {
        if (ch == '/') {
            ch = '@';
        }
    }
    return s;
}

void PrettyPrintToDisk(std::filesystem::path const &path, std::span<std::byte const> data) {
    std::ofstream os(path);
    auto text = poe::util::map_unicode_to_utf8_string(data);
    os << std::setw(4) << nlohmann::json::parse(*text);
}

int wmain(int argc, wchar_t **argv) {
    int build = 8077886;
    if (argc < 2) {
        return 1;
    }
    std::vector<std::string> matPaths;
    std::filesystem::path outRoot = argv[1];
    for (auto arg : std::span(argv + 2, argv + argc)) {
        matPaths.push_back(poe::util::to_string(reinterpret_cast<char16_t const *>(arg)));
    }

    auto collective = poe::io::file_collective_info{
        //.collective_root_ = "F:/Temp/poe/collective",
        .build = build,
    };
    auto vfs = std::make_shared<poe::io::vfs>(collective);
    if (!vfs) {
        throw std::runtime_error("could not open vfs");
    }

    auto ffxLib = std::make_shared<pog::FFXLibrary>(vfs);
    auto matLib = std::make_shared<pog::MaterialLibrary>(vfs, ffxLib);

    std::error_code ec{};
    std::filesystem::create_directories(outRoot, ec);
    for (auto &matPath : matPaths) {
        auto data = vfs->open_file(poe::util::path(matPath))->read_all();
        if (matPath.ends_with(".fxgraph")) {
            try {
                std::filesystem::path targetPath = outRoot / ReplacesSlashes(matPath);
                auto g = matLib->LoadGraph(poe::util::path(matPath));
                if (g) {
                    auto p = targetPath;
                    p.replace_extension(".dot");
                    VisualiseGraph(p, g.value(), ffxLib, *matLib);
                }
            } catch (std::exception &e) {
                LOG_F(ERROR, "{}: {}", matPath, e.what());
            }
            continue;
        }

        auto mt = matLib->MakeMaterialThingie(poe::util::path("Metadata/EngineGraphs/base.fxgraph"),
                                              poe::util::path(matPath));
        try {
            std::filesystem::path targetPath = outRoot / ReplacesSlashes(matPath);
            {
                auto p = targetPath;
                p.replace_extension(".mat.json");
                PrettyPrintToDisk(p, data);
            }

            auto mat = poe::format::mat::parse_mat(data);
            {
                auto p = targetPath;
                p.replace_extension(".defaultgraph.dot");
                auto g = matLib->LoadGraph(mat->defaultGraph);
                VisualiseGraph(p, g.value(), ffxLib, *matLib);
            }

            auto LookupGraphNodes = [&](poe::format::fxgraph::FxGraph const &graph) {
                using namespace fmt::literals;
                std::set<std::string> emittedFragBases;
                for (auto &node : graph.nodes) {
                    auto frag = ffxLib->LookupFragment(node.key.type);
                    if (frag) {
                        std::string domain;
                        std::map<std::string, std::vector<ffx_ns::VariableDeclaration const *>> nodeVars;
                        std::vector<ffx_ns::DeclUniformDeclaration const *> nodeUnis;
                        for (auto &decl : frag->declarations_) {
                            if (auto *keyword = std::get_if<ffx_ns::KeywordDeclaration>(&decl)) {
                                if (keyword->keyword == "vertex") {
                                    domain = "vertex";
                                } else if (keyword->keyword == "pixel") {
                                    domain = "pixel";
                                }
                            }
                            if (auto *var = std::get_if<ffx_ns::VariableDeclaration>(&decl)) {
                                nodeVars[var->kind].push_back(var);
                            }
                        }

                        if (!emittedFragBases.contains(frag->name_)) {
                            fmt::memory_buffer paramsBuf;
                            auto paramsSep = ""sv;
                            for (auto &uni : nodeUnis) {
                                fmt::format_to(fmt::appender(paramsBuf), "{}{} {}", paramsSep, uni->type, uni->name);
                                paramsSep = ", "sv;
                            }
                            for (auto &var : nodeVars["in"]) {
                                fmt::format_to(fmt::appender(paramsBuf), "{}in {} {}", paramsSep, var->type, var->name);
                                paramsSep = ", "sv;
                            }
                            for (auto &var : nodeVars["inout"]) {
                                fmt::format_to(fmt::appender(paramsBuf), "{}inout {} {}", paramsSep, var->type,
                                               var->name);
                                paramsSep = ", "sv;
                            }
                            for (auto &var : nodeVars["out"]) {
                                fmt::format_to(fmt::appender(paramsBuf), "{}out {} {}", paramsSep, var->type,
                                               var->name);
                                paramsSep = ", "sv;
                            }
                            std::string paramsStr = to_string(paramsBuf);
                            if (auto &nodeOuts = nodeVars["out"]; !nodeOuts.empty()) {
                                fmt::memory_buffer outputs;
                                for (auto &out : nodeOuts) {
                                    fmt::format_to(fmt::appender(outputs), "    {} {};\n", out->type, out->name);
                                }
                                fmt::print(std::cout, R"(struct GraphNode__{name} {{
{outputs}}};

)",
                                           fmt::arg("name", frag->name_), fmt::arg("outputs", to_string(outputs)));
                            }
                            fmt::print(std::cout, R"(void Code__{name}({params}) {{
{code}}}

)",
                                       "name"_a = frag->name_, "params"_a = paramsStr, "code"_a = frag->code_);
                            emittedFragBases.insert(frag->name_);
                        }

                        std::map<std::string, fmt::memory_buffer> varDefns;
                        for (auto &[kind, vars] : nodeVars) {
                            auto &defns = varDefns[kind];
                            for (auto &var : vars) {
                                fmt::format_to(fmt::appender(defns), "{type} {name};\n", "type"_a = var->type,
                                               "name"_a = var->name);
                            }
                        }

                        // Compose args sequence

                        std::string argsStr;

                        std::string transferIn, transferOut;
                        for (auto &out : nodeVars["out"]) {
                        }

                        auto inBlock = to_string(varDefns["in"]), outBlock = to_string(varDefns["out"]),
                             inoutBlock = to_string(varDefns["inout"]);

                        fmt::print(std::cout, R"(// Call to {name} {index}
Code__{name}({args});

)",
                                   "name"_a = node.key.type, "index"_a = node.key.index, "args"_a = argsStr);
                    } else {
                        fmt::print(std::cerr, "Fragment {} not found in FFX library\n", node.key.type);
                    }
                }
            };

            LookupGraphNodes(*mat->defaultGraph);

            for (auto &inst : mat->graphInstances) {
                auto fxgPath = inst.parent;
                auto data = vfs->open_file(poe::util::path(fxgPath))->read_all();

                std::filesystem::path targetPath = outRoot / ReplacesSlashes(fxgPath);
                {
                    auto p = targetPath;
                    p.replace_extension(".fxgraph.json");
                    PrettyPrintToDisk(p, data);
                }

                auto text = poe::util::map_unicode_to_utf8_string(data);
                auto js = nlohmann::json::parse(text.value());
                auto parentGraph = poe::format::fxgraph::parse_fxgraph_json(js);
                {
                    auto p = targetPath;
                    p.replace_extension(".fxgraph.dot");
                    VisualiseGraph(p, matLib->LoadGraph(parentGraph).value(), ffxLib, *matLib);
                }
                LookupGraphNodes(*parentGraph);
            }
        } catch (std::exception &e) {
            LOG_F(ERROR, "{}", e.what());
        }
    }

    return 0;
}
