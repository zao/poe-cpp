add_executable(nebula
    nebula_main.cc
    ImNodes.cpp
    ImNodes.h
    ImNodesEz.cpp
    ImNodesEz.h
)
GroupSourcesByFolder(nebula)
target_include_directories(nebula PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(nebula PRIVATE
    asio
    fmt::fmt
    gli
    loguru
    libpoe
    libpog
    ui_common
)

add_custom_command(TARGET nebula POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy_if_different
    $<TARGET_FILE:glfw> $<TARGET_FILE_DIR:nebula>
)

add_custom_command(TARGET nebula POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy_if_different
    $<TARGET_FILE_DIR:libpoe>/stable.json $<TARGET_FILE_DIR:nebula>
)

add_custom_command(TARGET nebula POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy_if_different
    "${CMAKE_SOURCE_DIR}/assets/Roboto-Regular.ttf" $<TARGET_FILE_DIR:nebula>
)

add_custom_command(TARGET nebula POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy_if_different
    "${CMAKE_SOURCE_DIR}/assets/RobotoMono-Regular.ttf" $<TARGET_FILE_DIR:nebula>
)