#pragma once

#include <pog/texture_library.hpp>
#include <pog/platform.hpp>

#include <atlbase.h>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <DirectXMath/DirectXMath.h>

#include <asio.hpp>
#include <nlohmann/json.hpp>

#include <poe/format/ffx.hpp>
#include <poe/util/graph.hpp>

#include <map>
#include <queue>
#include <set>
#include <variant>

struct Material {};

struct EffectGraph {};

struct EffectGraphInstance {};

struct MaterialInstance {
    using UniformValue =
        std::variant<float, DirectX::XMFLOAT2, DirectX::XMFLOAT3, DirectX::XMFLOAT4, CComPtr<ID3D11ShaderResourceView>>;
    std::shared_ptr<Material> mat_;
    std::vector<std::shared_ptr<EffectGraphInstance>> graph_instances_;
};
struct loaded_texture {
    nlohmann::json meta_;
    CComPtr<ID3D11Resource> res_;
    CComPtr<ID3D11ShaderResourceView> srv_;
};

struct material_impl {
    bool good() const { return signature_blob_ && vs_ && ps_; }
    std::vector<loaded_texture> textures_;
    CComPtr<ID3DBlob> signature_blob_;
    std::string shader_source_;
    CComPtr<ID3D11VertexShader> vs_;
    CComPtr<ID3D11PixelShader> ps_;
    std::string vs_log_;
    std::string ps_log_;
};

struct effect_graph {
    nlohmann::json js;
};

struct effect_graph_binding {
    std::map<std::string, poe::format::ffx::fragment const *> found_frags;
    std::set<std::string> missing_frags;
    size_t total_frags{};
};

struct mat_prober {
    mat_prober(asio::io_context &io_ctx, asio::thread_pool &offthread_ctx, std::shared_ptr<poe::io::vfs> vfs,
               pog::TextureLibrary &tex_lib)
        : io_(io_ctx), offthread_(offthread_ctx), vfs_(vfs), tex_lib_(tex_lib) {}

    void start() {
        worker_ = std::thread([&] {
            LOG_F(INFO, "mat worker starting");
            vfs_->enumerate_files([&](auto path) {
                if (path.extension() == ".mat") {
                    this->gather(path);
                }
            });

            if (bail_) {
                return;
            }
            this->finish();
            LOG_F(INFO, "mat worker done");
        });
    }

    void stop() {
        bail_ = true;
        worker_.join();
    }

    static std::string vertex_key(nlohmann::json const &js) {
        auto stage = js.contains("stage") ? js["stage"].get<std::string>() : "";
        return fmt::format("{}|{}|{}", std::string(js["type"]), (int)js["index"], stage);
    }

    std::shared_ptr<effect_graph> process_graph(nlohmann::json const &js, std::string_view path) {
        auto ret = std::make_shared<effect_graph>();
        ret->js = js;
        if (!js.contains("nodes") || !js.contains("links")) {
            return ret;
        }
        size_t const vmax = js["nodes"].size();
        size_t const emax = js["links"].size();
        // LOG_F(INFO, "{} : {}v {}e", path, vmax, emax);

        std::set<std::string> stages;
        std::map<std::string, size_t> vid_from_key;
        std::vector<std::set<std::string>> vstages(vmax);
        for (size_t i = 0; i < vmax; ++i) {
            auto const &node = js["nodes"][i];
            vid_from_key[vertex_key(node)] = i;
            if (auto I = node.find("stage"); I != node.end()) {
                stages.insert(stages.end(), *I);
                vstages[i].insert({*I});
            }
        }

        std::map<size_t, std::set<size_t>> edges;
        std::queue<size_t> wavefront;
        {
            std::vector<bool> sources(vmax, true);
            for (size_t i = 0; i < emax; ++i) {
                auto const &link = js["links"][i];
                auto from_key = vertex_key(link["src"]);
                auto to_key = vertex_key(link["dst"]);
                auto to_vid = vid_from_key[to_key];
                edges[vid_from_key[from_key]].insert({to_vid});
                sources[to_vid] = false;
            }
            for (size_t i = 0; i < vmax; ++i) {
                if (sources[i]) {
                    wavefront.push(i);
                }
            }
        }

        while (!wavefront.empty()) {
            auto vsrc = wavefront.front();
            wavefront.pop();
            for (auto vdst : edges[vsrc]) {
                auto &dst_vstages = vstages[vdst];
                bool any_new_dst_stage = false;
                for (auto &stage : vstages[vsrc]) {
                    any_new_dst_stage = any_new_dst_stage || dst_vstages.insert(stage).second;
                }
                if (any_new_dst_stage) {
                    wavefront.push(vdst);
                }
            }
        }

        std::vector<std::pair<size_t, size_t>> edge_pairs;
        for (auto const &[src, dsts] : edges) {
            for (auto dst : dsts) {
                edge_pairs.push_back({src, dst});
            }
        }
        auto sorted_vertices = poe::util::topologic_sort(edge_pairs);
        if (!sorted_vertices) {
            LOG_F(ERROR, "Could not topologically sort \"{}\"", path);
        }

        auto lk = std::lock_guard(mtx_);
        for (size_t i = 0; i < vmax; ++i) {
            auto const &dst = js["nodes"][i];
            if (dst.contains("stage")) {
                std::string stage = dst["stage"];
                for (auto const &s : vstages[i]) {
                    if (stage != s) {
                        stage_forward_deps_[s].insert(stage);
                    }
                }
            }
        }

        stages_.insert(stages.begin(), stages.end());
        return ret;
    }

    void gather(poe::util::path const &path) {
        using nlohmann::json;
        auto mat_data = vfs_->open_file(path)->read_all();
        auto mat_view = poe::util::to_u16string_view(mat_data);
        auto js = json::parse(mat_view->begin(), mat_view->end());

        if (graph_dedup_.contains(path.path_)) {
            return;
        }

        std::vector<std::string> graph_paths;
        std::list<std::promise<std::shared_ptr<effect_graph>>> process_promises;
        std::list<std::future<std::shared_ptr<effect_graph>>> processed_graphs;
        auto process_graph_offthread = [&](json js, std::string path) {
            graph_paths.push_back(path);
            graph_dedup_.insert(path);
            auto &p = process_promises.emplace_back();
            processed_graphs.push_back(p.get_future());
            asio::post(offthread_, [this, js, path, &p] {
                auto sp = this->process_graph(js, path);
                ++this->graphs_processed_;
                p.set_value(sp);
            });
        };

        process_graph_offthread(js["defaultgraph"], path.path_);
        for (auto const &g : js["graphinstances"]) {
            poe::util::path fxg_path(g["parent"]);
            if (graph_dedup_.contains(fxg_path.path_)) {
                continue;
            }
            auto fxg_data = vfs_->open_file(fxg_path)->read_all();
            auto fxg_view = poe::util::to_u16string_view(fxg_data);
            auto fxg_js = json::parse(fxg_view->begin(), fxg_view->end());
            process_graph_offthread(fxg_js, g["parent"]);
        }

        std::vector<std::pair<std::string, std::shared_ptr<effect_graph>>> out_graphs;
        size_t i = 0;
        for (auto &f : processed_graphs) {
            auto sp = f.get();
            auto path = graph_paths[i];
            out_graphs.push_back({path, sp});
            ++i;
        }
        {
            auto lk = std::lock_guard(mtx_);
            for (auto const &kv : out_graphs) {
                effect_graphs.insert(kv);
            }
        }
#if 0
        for (auto const& tex : js["textures"]) {
            if (!tex_infos_.contains(path.path_)) {
                tex_infos_.insert({ path.path_, tex });
            }
        }
#endif
    }

    bool finish() {
        for (auto const &[path, info] : tex_infos_) {
            if (bail_)
                return false;
            asio::post(io_, [this, path{path}, info{info}] {
                // Load texture in main thread due to D3D11 usage
                this->tex_lib_.load(info["filename"]);
            });
        }
        return true;
    }

    asio::io_context &io_;
    asio::thread_pool &offthread_;
    std::thread worker_;
    std::atomic<int> bail_ = 0;
    std::shared_ptr<poe::io::vfs> vfs_;
    pog::TextureLibrary &tex_lib_;

    std::atomic<size_t> graphs_processed_;

    mutable std::mutex dedup_mtx_;
    std::unordered_set<std::string> graph_dedup_;

    mutable std::mutex mtx_;
    std::set<std::string> stages_;
    std::map<std::string, std::set<std::string>> stage_forward_deps_;
    std::map<std::string, std::shared_ptr<effect_graph>> effect_graphs;

    std::unordered_map<std::string, nlohmann::json> tex_infos_;
};

inline std::shared_ptr<material_impl> translate_material(pog::DXBits const &dx, std::shared_ptr<poe::io::vfs> vfs,
                                                         pog::TextureLibrary &tex_lib, poe::util::path path) {
    using namespace std::string_view_literals;
    HRESULT hr = S_OK;
    (void)hr;

    using nlohmann::json;
    auto mat_data = vfs->open_file(path)->read_all();
    auto mat_view = poe::util::to_u16string_view(mat_data);
    auto js = json::parse(mat_view->begin(), mat_view->end());

    fmt::memory_buffer hlsl;
    hlsl.append(R"(cbuffer cb_frame : register(b0)
{
    float cb_frame_wall_time;
};
)"sv);
    std::vector<loaded_texture> textures;
    for (auto const &js_tex : js["textures"]) {
        size_t tex_id = textures.size();
        // LOG_F(WARNING, "{}", nlohmann::to_string(js_tex));
        std::string out_path;
        auto lt = tex_lib.load(js_tex["filename"]);
        loaded_texture t;
        t.meta_ = js_tex;
        t.res_ = lt->res_;
        t.srv_ = lt->srv_;
        textures.push_back(t);
        format_to(fmt::appender(hlsl), "Texture2D mat_tex_{} : register(t{});\n", tex_id, tex_id);
        format_to(fmt::appender(hlsl), "SamplerState mat_smp_{} : register(s{});\n", tex_id, tex_id);
    }
    auto ret = std::make_shared<material_impl>();
    ret->textures_ = std::move(textures);

    hlsl.append(R"(
struct vs_in {
    float3 position_clip : POSITION;
    float2 texcoord : TEXCOORD0;
};

struct vs_out {
    float4 position_clip : SV_POSITION;
    float2 texcoord : TEXCOORD0;
    float4 color0 : COLOR0;
};

struct VertexData {
	float2 uv;
	float4 position;
	float4 projected_position;
	float3 normal;
	float4 tangent;
	float3 binormal;
	float4 color;
};

void InitVertexData(inout VertexData vertex_data)
{
	vertex_data.uv = float2(0.f, 0.f);
	vertex_data.position = float4(0.f, 0.f, 0.f, 1.f);
	vertex_data.projected_position = float4(0.0f, 0.0f, 0.0f, 1.0f);
	vertex_data.normal = float3(0.f, 0.f, 1.f);
	vertex_data.tangent = float4(1.f, 0.f, 0.f, 1.f);
	vertex_data.binormal = float3(0.f, 1.f, 0.f);
	vertex_data.color = float4(0.f, 0.f, 0.f, 0.f);
}

struct SemanticsData {
    float4 world_pos; // TEXCOORD0
    float3 normal; // TEXCOORD5
    float3 tangent; // TEXCOORD6
    float3 binormal; // TEXCOORD7
    float2 uv1; // TEXCOORD1
    float4 uv2; // TEXCOORD2
    float4 uv3; // TEXCOORD3
    float4 uv4; // TEXCOORD4
    float4 uv8; // TEXCOORD8
    float4 uv9; // TEXCOORD9
    float4 color0; // COLOR0
    float4 color1; // COLOR1
    float seed; // COLOR2
};

void InitSemanticsData(inout SemanticsData semantics_data)
{
	semantics_data.world_pos = float4(0.f, 0.f, 0.f, 1.f);
	semantics_data.normal = float3(0.f, 0.f, 1.f);
	semantics_data.tangent = float3(1.f, 0.f, 0.f);
	semantics_data.binormal = float3(0.f, 1.f, 0.f);
	semantics_data.color0 = float4(1.f, 1.f, 1.f, 1.f);
	semantics_data.color1 = float4(0.f, 0.f, 0.f, 1.f);
	semantics_data.uv1 = float2(0.0f, 0.0f);
	semantics_data.uv2 = float4(0.f, 0.f, 0.f, 0.f);
	semantics_data.uv3 = float4(0.f, 0.f, 0.f, 0.f);
	semantics_data.uv4 = float4(0.f, 0.f, 0.f, 0.f);
	semantics_data.uv8 = float4(0.f, 0.f, 0.f, 0.f);
	semantics_data.uv9 = float4(0.f, 0.f, 0.f, 0.f);
	semantics_data.seed = 0.0f;
}

SemanticsData vs_main(vs_in input) {
    VertexData vertexData;
    InitVertexData(vertexData);
    vertexData.projected_position = float4(input.position_clip, 1.0);

    SemanticsData semanticsData;
    InitSemanticsData(semanticsData);
    semanticsData.world_pos = input.position;
    semanticsData.uv1 = input.texcoord;
    return semanticsData;
}

struct texturing_init {
    float2 InputUV_0;
    float2 UV_0;
};

void FromVertexColor(inout SemanticsData semanticsData, out float4 output) {
    output = semanticsData.color0;
}

float4 ps_main(SemanticsData semanticsData) : SV_TARGET {
    /* Art/Textures/Environment/Hideout/Void/void_hideoutc.mat - defaultgraph */
    texturing_init Texturing_Init;
    Texturing_Init.InputUV_0 = input.texcoord;
    Texturing_Init.UV_0 = Texturing_Init.InputUV_0;
   
    {
        float ConstantPixel_0 = 5.0;
        float Zero_0 = 0.0;
    
        float4 FromVertexColor_0 = input.color0;
        float4 SampleTexture_0 = mat_tex_0.Sample(mat_smp_0, Texturing_Init.InputUV_0);
        Texturing_Init.SpecularPower_0 = ConstantPixel_0;
        Texturing_Init.SpecularColor_0 = 0.0;
    }

    /* Metadata/Effects/Graphs/General/ProjectiveUV_Accurate.fxgraph */
    vec2 ProjectiveUvAccurate_0;
    ProjectiveUvAccurate(WorldTransform.InputVertexPosition_0, ProjectiveUvAccurate_0, float3(4.0, 2048.0, 2048.0));
    WorldTransform.VertexPosition_0 = WorldTransform.InputVertexPosition_0;

    WorldTransform.VertexUV_0 = ProjectiveUvAccurate_0.uv;

    /* Metadata/Effects/Graphs/General/CelestialMaterial.fxgraph */

    /* old */
    float pi = 3.14159;
    float x = input.texcoord.x * 7.0;
    float4 color;
    if (x < 1.0) {
        color = mat_tex_0.Sample(mat_smp_0, input.texcoord);
    } else if (x < 2.0) {
        color = mat_tex_1.Sample(mat_smp_1, input.texcoord);
    } else if (x < 3.0) {
        color = mat_tex_2.Sample(mat_smp_2, input.texcoord);
    } else if (x < 4.0) {
        color = mat_tex_3.Sample(mat_smp_3, input.texcoord);
    } else if (x < 5.0) {
        color = mat_tex_4.Sample(mat_smp_4, input.texcoord);
    } else if (x < 6.0) {
        color = mat_tex_5.Sample(mat_smp_5, input.texcoord);
    } else {
        color = mat_tex_6.Sample(mat_smp_6, input.texcoord);
    }
    return float4(color.rgb, 1.0);
}
)"sv);

    ret->shader_source_ = to_string(hlsl);

    UINT shader_flags1 = 0;
    UINT shader_flags2 = 0;

    CComPtr<ID3DBlob> vs_blob_, vs_log_;
    hr = D3DCompile(hlsl.data(), hlsl.size(), nullptr, nullptr, nullptr, "vs_main", "vs_5_0", shader_flags1,
                    shader_flags2, &vs_blob_, &vs_log_);
    ret->signature_blob_ = vs_blob_;

    if (vs_log_) {
        ret->vs_log_ = std::string((char const *)vs_log_->GetBufferPointer(), vs_log_->GetBufferSize());
        LOG_F(WARNING, "VS: {}", ret->vs_log_);
    }
    hr = dx.dev_->CreateVertexShader(vs_blob_->GetBufferPointer(), vs_blob_->GetBufferSize(), nullptr, &ret->vs_);

    CComPtr<ID3DBlob> ps_blob_, ps_log_;
    hr = D3DCompile(hlsl.data(), hlsl.size(), nullptr, nullptr, nullptr, "ps_main", "ps_5_0", shader_flags1,
                    shader_flags2, &ps_blob_, &ps_log_);
    ret->ps_ = ps_blob_;
    if (ps_log_) {
        ret->ps_log_ = std::string((char const *)ps_log_->GetBufferPointer(), ps_log_->GetBufferSize());
        LOG_F(WARNING, "PS: {}", ret->ps_log_);
    }
    hr = dx.dev_->CreatePixelShader(ps_blob_->GetBufferPointer(), ps_blob_->GetBufferSize(), nullptr, &ret->ps_);

    LOG_F(INFO, "{}", ret->shader_source_);

    return ret;
}