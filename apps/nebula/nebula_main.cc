#include <loguru/loguru.hpp>

#include "ui_common.hpp"
#include "material.hpp"

#include <d3d11.h>
#include <dxgi.h>
#include <d3dcompiler.h>

#include <DirectXMath/DirectXMath.h>
#include <DirectXTK/DDSTextureLoader.h>

#include <comdef.h>
#include <atlbase.h>
#include <atlcom.h>
#include <atltypes.h>

#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_stdlib.h>

#include <gli/gli.hpp>

#include <poe/util/graph.hpp>
#include <poe/util/hexdump.hpp>
#include <poe/util/parse_info.hpp>
#include <poe/util/path.hpp>
#include <poe/io/vfs.hpp>

#include <poe/format/dds.hpp>
#include <poe/format/ffx.hpp>

#include <fmt/format.h>

#include <asio.hpp>

#include <nlohmann/json.hpp>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <regex>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <string_view>
#include <thread>
#include <unordered_set>
#include <variant>
#include <vector>

#include <ImNodesEz.h>

#include <pog/ffx_library.hpp>
#include <pog/platform.hpp>
#include <pog/texture_library.hpp>

using namespace pog; // temporary while moving code

using namespace std::literals::string_view_literals;

struct View3D {
    DirectX::XMMATRIX proj_matrix_;
    DirectX::XMMATRIX view_matrix_;
};

struct QuadVertex {
    glm::vec3 pos_;
    glm::vec2 uv_;
};

CComPtr<ID3D11Buffer> build_quad(ID3D11Device *dev) {
    using Vertex = QuadVertex;
    HRESULT hr{S_OK};
    (void)hr;

    float side = 2.0f;
    float r = side / 2.0f;
    std::array<Vertex, 6> quad = {
        Vertex{{-r, -r, 0.0f}, {0.0f, 1.0f}}, // LL
        Vertex{{-r, +r, 0.0f}, {0.0f, 0.0f}}, // UL
        Vertex{{+r, +r, 0.0f}, {1.0f, 0.0f}}, // UR
        Vertex{{+r, +r, 0.0f}, {1.0f, 0.0f}}, // UR
        Vertex{{+r, -r, 0.0f}, {1.0f, 1.0f}}, // LR
        Vertex{{-r, -r, 0.0f}, {0.0f, 1.0f}}, // LL
    };
    CComPtr<ID3D11Buffer> quad_vb_;
    D3D11_BUFFER_DESC quad_desc{
        .ByteWidth = sizeof(quad),
        .Usage = D3D11_USAGE_DEFAULT,
        .BindFlags = D3D11_BIND_VERTEX_BUFFER,
    };
    D3D11_SUBRESOURCE_DATA quad_srd{.pSysMem = quad.data()};
    hr = dev->CreateBuffer(&quad_desc, &quad_srd, &quad_vb_);
    return quad_vb_;
}

struct EffectGraphWindow {
    EffectGraphWindow(std::string path, std::shared_ptr<effect_graph> graph) : path(path), graph(graph) {
        if (auto const &nodes = graph->js["nodes"]; nodes.is_array()) {
            size_t node_count = nodes.size();
            size_t node_idx = 0;
            auto find_node = [&](nlohmann::json key) -> size_t {
                for (size_t ni = 0; ni < node_count; ++ni) {
                    auto const &node = ui_nodes[ni];
                    if (node.type == key["type"] && node.index == key["index"] &&
                        node.stage ==
                            (key.contains("stage") ? std::optional((std::string)key["stage"]) : std::nullopt)) {
                        return ni;
                    }
                }
                return ~(size_t)0;
            };
            for (auto const &node : nodes) {
                canvas_node cn{
                    .type = node["type"],
                    .index = node["index"],
                    .pos = {node["ui_position"][0], node["ui_position"][1]},
                };
                if (node.contains("stage")) {
                    cn.stage = std::string(node["stage"]);
                }
                ui_nodes.push_back(cn);
                ++node_idx;
            }
            if (auto const &links = graph->js["links"]; links.is_array()) {
                for (auto const &link : links) {
                    auto format_slot = [](nlohmann::json const &key) {
                        fmt::memory_buffer buf;
                        format_to(fmt::appender(buf), "{}", key["variable"]);
                        if (key.contains("swizzle")) {
                            format_to(fmt::appender(buf), ".{}", key["swizzle"]);
                        }
                        return to_string(buf);
                    };
                    std::string src_slot_key = format_slot(link["src"]);
                    std::string dst_slot_key = format_slot(link["dst"]);
                    size_t src_idx = find_node(link["src"]);
                    size_t dst_idx = find_node(link["dst"]);
                    auto &outputs = ui_nodes[src_idx].outputs;
                    auto output_I = std::find_if(outputs.begin(), outputs.end(),
                                                 [&](auto const &a) { return src_slot_key == a.title; });
                    if (output_I == outputs.end()) {
                        auto &title = *titles.insert(src_slot_key).first;
                        outputs.push_back({
                            .title = title.c_str(),
                            .kind = 1,
                        });
                        output_I = outputs.end() - 1;
                    }
                    auto &inputs = ui_nodes[dst_idx].inputs;
                    auto input_I = std::find_if(inputs.begin(), inputs.end(),
                                                [&](auto const &a) { return dst_slot_key == a.title; });
                    if (input_I == inputs.end()) {
                        auto &title = *titles.insert(dst_slot_key).first;
                        inputs.push_back({
                            .title = title.c_str(),
                            .kind = 1,
                        });
                        input_I = inputs.end() - 1;
                    }
                    ui_connections.push_back({
                        .src_idx = src_idx,
                        .src_slot = (size_t)(output_I - outputs.begin()),
                        .dst_idx = dst_idx,
                        .dst_slot = (size_t)(input_I - inputs.begin()),
                    });
                }
            }
        }
    }

    std::string path;
    std::shared_ptr<effect_graph> graph;
    ImNodes::CanvasState canvas;
    std::set<std::string> titles;

    struct canvas_node {
        std::string type;
        size_t index;
        std::optional<std::string> stage;

        std::string title() const {
            fmt::memory_buffer buf;
            format_to(fmt::appender(buf), "{}", type);
            if (stage) {
                format_to(fmt::appender(buf), "|{}", *stage);
            }
            return to_string(buf);
        }

        ImVec2 pos{};
        bool selected{};
        std::vector<ImNodes::Ez::SlotInfo> inputs;
        std::vector<ImNodes::Ez::SlotInfo> outputs;
    };
    std::vector<canvas_node> ui_nodes;

    struct connection {
        size_t src_idx;
        size_t src_slot;
        size_t dst_idx;
        size_t dst_slot;
    };
    std::vector<connection> ui_connections;
};

struct ProgramPair {
    CComPtr<ID3DBlob> vs_blob_;
    CComPtr<ID3D11VertexShader> vs_;
    CComPtr<ID3D11PixelShader> ps_;
    std::optional<std::string> vs_log_, ps_log_;
};

std::array<ProgramPair, 2> build_pps(DXBits &dx) {
    using namespace std::string_view_literals;

    HRESULT hr{S_OK};
    (void)hr;

    std::array<ProgramPair, 2> progs;
    ProgramPair pp;

    auto shader_source = R"#(
SamplerState mat_smp_0 : register(s0);
Texture2D mat_tex_0 : register(t0);

cbuffer cb_frame : register(b0)
{
    float cb_frame_wall_time;
};

struct vs_in {
    float3 position_clip : POSITION;
    float2 texcoord : TEXCOORD0;
};

struct vs_out {
    float4 position_clip : SV_POSITION;
    float2 texcoord : TEXCOORD0;
};

vs_out vs_main(vs_in input) {
    vs_out output = (vs_out)0;
    output.position_clip = float4(input.position_clip, 1.0);
    output.texcoord = input.texcoord;
    return output;
}

float4 ps_main(vs_out input) : SV_TARGET {
    float pi = 3.14159;
    float x = fmod(cb_frame_wall_time, 1.0) * 3.0;
    x = input.texcoord.x * 3.0;
    float3 weight = {
        x >= 0.0 && x < 1.0,
        x >= 1.0 && x < 2.0,
        x >= 2.0 && x < 3.0,
    };
    float4 color = mat_tex_0.Sample(mat_smp_0, input.texcoord);
    float3 cw = color.rgb * weight * weight;
    float cwv = cw.r + cw.g + cw.b;
    return float4(cwv.xxx, 1.0);
    // return float4(input.texcoord, 0.3, 1.0);
}
)#"sv;
    CComPtr<ID3DBlob> vs_bytecode, vs_err;
    hr = D3DCompile(shader_source.data(), shader_source.size(), "dummy.hlsl", nullptr, nullptr, "vs_main", "vs_5_0", 0,
                    0, &vs_bytecode, &vs_err);
    if (vs_err) {
        auto err = std::string_view((char const *)vs_err->GetBufferPointer(), vs_err->GetBufferSize());
        pp.vs_log_ = std::string(err);
        LOG_F(ERROR, "{}", err);
    }
    pp.vs_blob_ = vs_bytecode;

    CComPtr<ID3DBlob> ps_bytecode, ps_err;
    hr = D3DCompile(shader_source.data(), shader_source.size(), "dummy.hlsl", nullptr, nullptr, "ps_main", "ps_5_0", 0,
                    0, &ps_bytecode, &ps_err);
    if (ps_err) {
        auto err = std::string_view((char const *)ps_err->GetBufferPointer(), ps_err->GetBufferSize());
        pp.ps_log_ = std::string(err);
        LOG_F(ERROR, "{}", err);
    }

    hr = dx.dev_->CreateVertexShader(vs_bytecode->GetBufferPointer(), vs_bytecode->GetBufferSize(), nullptr, &pp.vs_);

    hr = dx.dev_->CreatePixelShader(ps_bytecode->GetBufferPointer(), ps_bytecode->GetBufferSize(), nullptr, &pp.ps_);

    progs[1] = progs[0] = pp;
    return progs;
}

struct FFXWindow {
    using FFXPtr = std::shared_ptr<poe::format::ffx::parsed_ffx>;
    using FFXMap = std::map<std::string, FFXPtr>;
    explicit FFXWindow(WindowFrame const &frame, std::shared_ptr<FFXLibrary> ffx_lib)
        : frame_(frame), ffx_lib_(ffx_lib) {}

    void run() {
        if (ImGui::Begin("FFX")) {
            auto &parsed_ffx_files = ffx_lib_->parsed_ffx_files_;
            if (ImGui::BeginCombo("##ffx_file", ffx_choice_ ? ffx_choice_->c_str() : "<None>",
                                  ImGuiComboFlags_HeightLarge)) {
                for (auto &[name, _] : parsed_ffx_files) {
                    bool selected = ffx_choice_ == name;
                    if (ImGui::Selectable(name.c_str(), &selected)) {
                        ffx_choice_ = name;
                    }
                }
                ImGui::EndCombo();
            }
            if (ffx_choice_) {
                auto ffx = parsed_ffx_files.find(*ffx_choice_)->second;
                if (ImGui::TreeNode("##decl-ffx", "Declarations (%zu)", ffx->declarations_.size())) {
                    for (auto &[name, decl] : ffx->declarations_) {
                        ImGui::Text("DECLARATIONS %s", name.c_str());
                        for (auto const &pi : decl.declarations_) {
                            auto format_prelude_item =
                                [](poe::format::ffx::declaration::prelude_item const &pi) -> std::string {
                                // using prelude_item = std::variant<
                                //   include_declaration,
                                //   uniform_declaration,
                                //   define_declaration
                                // >;
                                if (auto incl = std::get_if<poe::format::ffx::include_declaration>(&pi)) {
                                    return fmt::format("include {}", incl->target_);
                                } else if (auto uniform = std::get_if<poe::format::ffx::uniform_declaration>(&pi)) {
                                    fmt::memory_buffer buf;
                                    format_to(fmt::appender(buf), "uniform {} {} {}", uniform->scope_, uniform->type_,
                                              uniform->name_);
                                    if (uniform->array_bounds_) {
                                        format_to(fmt::appender(buf), "{}", *uniform->array_bounds_);
                                    }
                                    if (uniform->comment_) {
                                        format_to(fmt::appender(buf), " {}", *uniform->comment_);
                                    }
                                    return to_string(buf);
                                } else if (auto define = std::get_if<poe::format::ffx::define_declaration>(&pi)) {
                                    return fmt::format("define {} {}", define->name_, define->value_);
                                } else {
                                    return "<unknown directive>";
                                }
                            };
                            ImGui::TextUnformatted(format_prelude_item(pi).c_str());
                        }
                        ImGui::PushID(&decl);
                        frame_.push_mono_font();
                        ImGui::InputTextMultiline("##code", &decl.code_, {-FLT_MIN, 0}, ImGuiInputTextFlags_ReadOnly);
                        ImGui::PopFont();
                        ImGui::PopID();
                    }
                    ImGui::TreePop();
                }
                if (ImGui::TreeNode("##frag-ffx", "Fragments (%zu)", ffx->fragments_.size())) {
                    for (auto &[name, frag] : ffx->fragments_) {
                        ImGui::Text("FRAGMENT %s", name.c_str());
                        for (auto d : frag.prelude_) {
                            ImGui::TextUnformatted(d.c_str());
                        }
                        if (frag.annotations_.size()) {
                            if (ImGui::TreeNode("##ann", "Annotations (%zu)", frag.annotations_.size())) {
                                for (auto &ann : frag.annotations_) {
                                    ImGui::TextUnformatted(ann.c_str());
                                }
                                ImGui::TreePop();
                            }
                        }
                        ImGui::PushID(&frag);
                        frame_.push_mono_font();
                        ImGui::InputTextMultiline("##code", &frag.code_, {-FLT_MIN, 0}, ImGuiInputTextFlags_ReadOnly);
                        ImGui::PopFont();
                        ImGui::PopID();
                    }
                    ImGui::TreePop();
                }
            }
        }
        ImGui::End();
    }

  private:
    WindowFrame const &frame_;
    std::optional<std::string> ffx_choice_;
    std::shared_ptr<FFXLibrary> ffx_lib_;
};

struct EffectGraphWindows {
    EffectGraphWindows(WindowFrame const &frame, mat_prober const &mp, std::shared_ptr<FFXLibrary> ffx_lib)
        : frame_(frame), mp_(mp), ffx_lib_(ffx_lib) {}

    void run() {
        {
            auto lk = std::lock_guard(mp_.mtx_);
            for (auto &[path, fxg] : mp_.effect_graphs) {
                if (path.find("CelestialMaterial") != path.npos) {
                    if (!effect_graphs_shown_.contains(path)) {
                        effect_graphs_shown_[path] = std::make_shared<EffectGraphWindow>(path, fxg);
                    }
                }
            }
        }

        if (ImGui::Begin("Effect graphs")) {
            ImGui::Checkbox("Hide empty graphs", &hide_empty_graphs_);
            ImGui::Checkbox("Hide good graphs", &hide_good_graphs_);
            ImGuiTableFlags table_flags = ImGuiTableFlags_RowBg | ImGuiTableFlags_Borders | ImGuiTableFlags_Resizable |
                                          ImGuiTableFlags_Hideable | ImGuiTableFlags_ScrollX | ImGuiTableFlags_ScrollY;
            if (ImGui::BeginTable("fxgraph-table", 5, table_flags)) {
                ImGui::TableSetupScrollFreeze(1, 1);
                ImGui::TableSetupColumn("Path");
                ImGui::TableSetupColumn("Open");
                ImGui::TableSetupColumn("Nodes");
                ImGui::TableSetupColumn("Links");
                ImGui::TableSetupColumn("Misc info");
                ImGui::TableHeadersRow();

                auto lk = std::lock_guard(mp_.mtx_);
                size_t i = 0;
                for (auto &[path, fxg] : mp_.effect_graphs) {
                    auto const &nodes = fxg->js["nodes"];
                    auto const &links = fxg->js["links"];
                    size_t node_count = nodes.is_array() ? nodes.size() : 0;
                    size_t link_count = links.is_array() ? links.size() : 0;
                    if (hide_empty_graphs_ && node_count == 0) {
                        continue;
                    }

                    auto &binding = effect_graph_bindings_[fxg.get()];
                    if (!binding) {
                        binding = effect_graph_binding{};
                        for (auto node : fxg->js["nodes"]) {
                            std::string node_type = node["type"];
                            auto frag = ffx_lib_->lookup_fragment(node_type);
                            ++binding->total_frags;
                            if (frag) {
                                binding->found_frags[node_type] = frag;
                            } else {
                                binding->missing_frags.insert(node_type);
                            }
                        }
                    }
                    if (hide_good_graphs_ && binding->missing_frags.empty()) {
                        continue;
                    }

                    ImGui::TableNextRow();
                    if (ImGui::TableNextColumn()) {
                        ImGui::Text("%s", path.c_str());
                    }
                    if (ImGui::TableNextColumn()) {
                        std::string button_label = fmt::format("{}##fxg-{}", "Open", i);
                        if (ImGui::Button(button_label.c_str())) {
                            auto &win = effect_graphs_shown_[path];
                            if (!win) {
                                win = std::make_shared<EffectGraphWindow>(path, fxg);
                            }
                        }
                    }
                    if (ImGui::TableNextColumn()) {
                        ImGui::Text("%zu", node_count);
                    }
                    if (ImGui::TableNextColumn()) {
                        ImGui::Text("%zu", link_count);
                    }
                    if (ImGui::TableNextColumn()) {
                        fmt::memory_buffer buf;
                        format_to(fmt::appender(buf), "{}/{}", binding->found_frags.size(), binding->total_frags);
                        for (auto f : binding->missing_frags) {
                            format_to(fmt::appender(buf), " {}", f);
                        }
                        ImGui::TextUnformatted(to_string(buf).c_str());
                    }
                    ++i;
                }
                ImGui::EndTable();
            }
        }
        ImGui::End();

        std::list<std::string> effect_graphs_closed;
        for (auto const &[path, fxg_win] : effect_graphs_shown_) {
            if (!fxg_win) {
                continue;
            }
            std::shared_ptr<effect_graph> fxg = fxg_win->graph;
            bool keep_open = true;
            if (ImGui::Begin(fxg_win->path.c_str(), &keep_open,
                             ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse)) {
#if 1
                ImNodes::BeginCanvas(&fxg_win->canvas);
                for (auto &node : fxg_win->ui_nodes) {
                    if (ImNodes::Ez::BeginNode(&node, node.title().c_str(), &node.pos, &node.selected)) {
                        ImNodes::Ez::InputSlots(node.inputs.data(), (int)node.inputs.size());
                        ImNodes::Ez::OutputSlots(node.outputs.data(), (int)node.outputs.size());
                        ImNodes::Ez::EndNode();
                    }
                }

                for (auto &conn : fxg_win->ui_connections) {
                    auto &src = fxg_win->ui_nodes[conn.src_idx];
                    auto &dst = fxg_win->ui_nodes[conn.dst_idx];
                    ImNodes::Connection(&dst, dst.inputs[conn.dst_slot].title, &src, src.outputs[conn.src_slot].title);
                }

                ImNodes::EndCanvas();
#else
                static ImNodes::CanvasState canvas;
                ImNodes::BeginCanvas(&canvas);

                struct Node {
                    ImVec2 pos{};
                    bool selected{};
                    ImNodes::Ez::SlotInfo inputs[1];
                    ImNodes::Ez::SlotInfo outputs[1];
                };

                static Node nodes[3] = {
                    {{50, 100}, false, {{"In", 1}}, {{"Out", 1}}},
                    {{250, 50}, false, {{"In", 1}}, {{"Out", 1}}},
                    {{250, 100}, false, {{"In", 1}}, {{"Out", 1}}},
                };

                for (Node &node : nodes) {
                    if (ImNodes::Ez::BeginNode(&node, "Node Title", &node.pos, &node.selected)) {
                        ImNodes::Ez::InputSlots(node.inputs, 1);
                        ImNodes::Ez::OutputSlots(node.outputs, 1);
                        ImNodes::Ez::EndNode();
                    }
                }

                ImNodes::Connection(&nodes[1], "In", &nodes[0], "Out");
                ImNodes::Connection(&nodes[2], "In", &nodes[0], "Out");

                ImNodes::EndCanvas();
#endif
            }
            ImGui::End();
            if (!keep_open) {
                effect_graphs_closed.push_back(path);
            }
        }
        for (auto const &fxg_path : effect_graphs_closed) {
            effect_graphs_shown_[fxg_path].reset();
        }
    }

    void debug() {
        auto &parsed_ffx_files = ffx_lib_->parsed_ffx_files_;
        size_t full_parses = 0;
        for (auto &[_, ptr] : parsed_ffx_files) {
            if (ptr) {
                full_parses += 1;
            }
        }
        ImGui::Text("Full FFX parses: %zu / %zu", full_parses, parsed_ffx_files.size());
        ImGui::Text("Graphs processed: %zu", mp_.graphs_processed_.load());
        for (auto const &[path, fxg_win] : effect_graphs_shown_) {
            if (!fxg_win) {
                continue;
            }
            auto fxg = fxg_win->graph;
            size_t fragments_found{};
            size_t fragments_total{};
            for (auto node : fxg->js["nodes"]) {
                auto frag = ffx_lib_->lookup_fragment(node["type"]);
                ++fragments_total;
                if (frag) {
                    ++fragments_found;
                }
            }
            ImGui::Text("Graph: %s - %zu/%zu fragments found.", path.c_str(), fragments_found, fragments_total);
            {
                fmt::memory_buffer buf;
                for (auto node : fxg->js["nodes"]) {
                    std::string node_type = node["type"];
                    auto frag = ffx_lib_->lookup_fragment(node_type);
                    if (frag) {
                        format_to(fmt::appender(buf), "// {} fragment\n{}\n", node_type, frag->code_);
                    } else {
                        format_to(fmt::appender(buf), "// {} fragment not found", node_type);
                    }
                }
                frame_.push_mono_font();
                ImGui::TextUnformatted(to_string(buf).c_str());
                ImGui::PopFont();
            }
        }
    }

  private:
    WindowFrame const &frame_;
    mat_prober const &mp_;
    std::shared_ptr<FFXLibrary> ffx_lib_;

    std::map<effect_graph const *, std::optional<effect_graph_binding>> effect_graph_bindings_;
    std::map<std::string, std::shared_ptr<EffectGraphWindow>> effect_graphs_shown_;
    bool hide_empty_graphs_ = true;
    bool hide_good_graphs_ = true;
};

struct TopologyProber {};

struct TopologyProberWindow {
    TopologyProberWindow(WindowFrame const &frame, TopologyProber const &tp) : frame_(frame), tp_(tp) {}

    WindowFrame const &frame_;
    TopologyProber const &tp_;
};

struct MatProberWindow {
    MatProberWindow(WindowFrame const &frame, mat_prober const &mp) : frame_(frame), mp_(mp) { (void)frame_; }

    void run() {
        if (ImGui::Begin("Input")) {
            {
                auto lk = std::lock_guard(mp_.mtx_);
                ImGui::TextUnformatted("Stage forward deps:");
                fmt::memory_buffer buf;
                for (auto const &[from, tos] : mp_.stage_forward_deps_) {
                    for (auto const &to : tos) {
                        format_to(fmt::appender(buf), "\"{}\" -> \"{}\";\n", from, to);
                    }
                }
                std::string stage_deps = to_string(buf);
                ImGui::InputTextMultiline("##stagedep", &stage_deps, {0, 0}, ImGuiInputTextFlags_ReadOnly);
                ImGui::TextUnformatted("Stages:");
                for (auto const &stage : mp_.stages_) {
                    ImGui::BulletText("%s", stage.c_str());
                }
            }
            std::shared_ptr<material_impl> void_mat;
            if (void_mat) {
                size_t i = 0;
                for (auto const &tex : void_mat->textures_) {
                    ImGui::TextUnformatted(nlohmann::to_string(tex.meta_).c_str());
                    CComPtr<ID3D11Texture2D> tp;
                    if (SUCCEEDED(tex.res_.QueryInterface(&tp))) {
                        D3D11_TEXTURE2D_DESC tex_desc;
                        tp->GetDesc(&tex_desc);
                        auto w = (float)tex_desc.Width;
                        auto h = (float)tex_desc.Height;
                        ImVec2 size{};
                        float side = 64.0f;
                        float max_extent;
                        if (w >= h) {
                            size = {side, side * h / w};
                            max_extent = w;
                        } else {
                            size = {side * w / h, side};
                            max_extent = h;
                        }
                        ImGui::Image((ImTextureID)tex.srv_.p, size, {0, 0}, {1, 1}, {1, 1, 1, 1},
                                     {0.5f, 0.5f, 0.5f, 1.0f});
                        if (ImGui::IsItemHovered()) {
                            ImGui::BeginTooltip();
                            ImVec2 big_size = {w, h};
                            if (max_extent > 768.0f) {
                                big_size.x *= 768.0f / max_extent;
                                big_size.y *= 768.0f / max_extent;
                            }
                            ImGui::Image((ImTextureID)tex.srv_.p, big_size, {0, 0}, {1, 1}, {1, 1, 1, 1},
                                         {0.5f, 0.5f, 0.5f, 1.0f});
                            ImGui::EndTooltip();
                        }
                    } else {
                        ImGui::TextUnformatted("Not a 2D texture.");
                    }
                }
                ++i;
            } else {
                ImGui::TextUnformatted("Load failed.");
            }
        }
        ImGui::End();
    }

  private:
    WindowFrame const &frame_;
    mat_prober const &mp_;
};

int main(int argc, char **argv) {
    loguru::init(argc, argv);
    ImGui_ImplWin32_EnableDpiAwareness();

    std::optional<std::filesystem::path> pack_path;

    for (int i = 1; i < argc; ++i) {
        if (argv[i] == "--pack"sv && i + 1 < argc) {
            ++i;
            pack_path = argv[i];
        }
    }

    if (!pack_path) {
        LOG_F(FATAL, "Required argument --pack not specified");
    }

    if (pack_path) {
        if (!exists(*pack_path)) {
            LOG_F(FATAL, "Pack path \"{}\" does not exist", pack_path->string());
        }
        if (!is_regular_file(*pack_path) && !is_symlink(*pack_path) && !is_directory(*pack_path)) {
            LOG_F(FATAL, "Pack path \"{}\" is not a file", pack_path->string());
        }
    }

    auto vfs = std::make_shared<poe::io::vfs>(*pack_path);

    auto ffx_lib = std::make_shared<pog::FFXLibrary>(vfs);
    std::optional<std::string> ffx_choice;

    pog::WindowFrame window_frame(u"Nebula", ImGuiConfigFlags_None);
    DXBits &dx = window_frame.dx();

    HRESULT hr = S_OK;
    (void)hr;

    View3D view_3d;

    asio::io_context io_ctx;
    auto io_work = asio::make_work_guard(io_ctx);
    asio::thread_pool offthread_pool(6);
    TextureLibrary tex_lib(vfs, dx);

    mat_prober mp(io_ctx, offthread_pool, vfs, tex_lib);
    // mp.start();

    std::shared_ptr<material_impl> void_mat;
    // void_mat = translate_material(dx, vfs, tex_lib,
    //                              poe::util::path("Art/Textures/Environment/Hideout/Void/void_hideoutc.mat"));

    auto quad_vb = build_quad(dx.dev_);

    bool show_demo = false;
    std::array<ProgramPair, 2> progs = build_pps(dx);

    std::array<D3D11_INPUT_ELEMENT_DESC, 2> quad_ied{
        D3D11_INPUT_ELEMENT_DESC{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(QuadVertex, pos_),
                                 D3D11_INPUT_PER_VERTEX_DATA, 0},
        D3D11_INPUT_ELEMENT_DESC{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, offsetof(QuadVertex, uv_),
                                 D3D11_INPUT_PER_VERTEX_DATA, 0},
    };
    CComPtr<ID3D11InputLayout> quad_il;
    if (void_mat) {
        gsl::span<std::byte const> quad_shader_sig((std::byte const *)void_mat->signature_blob_->GetBufferPointer(),
                                                   void_mat->signature_blob_->GetBufferSize());
        hr = dx.dev_->CreateInputLayout(quad_ied.data(), (UINT)quad_ied.size(), quad_shader_sig.data(),
                                        quad_shader_sig.size(), &quad_il);
    }

    std::shared_ptr<pog::RenderTarget> rt_3d;

    struct CBFrame {
        DirectX::XMMATRIX proj_matrix;
        DirectX::XMMATRIX view_matrix;
        float wall_time;
        float pad0[3];
    } cb_frame_cpu;

    CComPtr<ID3D11Buffer> cb_frame_gpu;
    {
        D3D11_BUFFER_DESC bd{
            .ByteWidth = sizeof(CBFrame),
            .Usage = D3D11_USAGE_DEFAULT,
            .BindFlags = D3D11_BIND_CONSTANT_BUFFER,
            .CPUAccessFlags = 0,
            .MiscFlags = 0,
            .StructureByteStride = 0,
        };
        hr = dx.dev_->CreateBuffer(&bd, nullptr, &cb_frame_gpu);
    }

    int shader_used = 0;

    uint64_t qpc_base, qpc_freq;
    QueryPerformanceCounter((LARGE_INTEGER *)&qpc_base);
    QueryPerformanceFrequency((LARGE_INTEGER *)&qpc_freq);

    FFXWindow ffx_window(window_frame, ffx_lib);
    EffectGraphWindows effect_graph_windows(window_frame, mp, ffx_lib);
    MatProberWindow mat_prober_window(window_frame, mp);

    /* Main loop */
    while (window_frame.new_frame()) {
        uint64_t qpc_count;
        QueryPerformanceCounter((LARGE_INTEGER *)&qpc_count);
        double wall_time = (qpc_count - qpc_base) / (double)qpc_freq;
        cb_frame_cpu.wall_time = (float)wall_time;

        window_frame.begin_window();
        if (ImGui::BeginMenuBar()) {
            if (ImGui::MenuItem("ImGui Demo")) {
                show_demo = true;
            }

            ImGui::EndMenuBar();
        }
        ImGui::End(); // For main window, do menu above this

        if (show_demo) {
            ImGui::ShowDemoWindow(&show_demo);
        }

        if (ImGui::Begin("Shader")) {
            if (void_mat) {
                auto &mat = *void_mat;
                window_frame.push_mono_font();
                ImGui::InputTextMultiline("##shadersource", &mat.shader_source_, ImVec2(-FLT_MIN, 400),
                                          ImGuiInputTextFlags_ReadOnly);
                ImGui::PopFont();

                ImGui::Text("Vertex shader log:");
                ImGui::Indent();
                ImGui::TextUnformatted(mat.vs_log_.c_str());
                ImGui::Unindent();

                ImGui::Text("Pixel shader log:");
                ImGui::Indent();
                ImGui::TextUnformatted(mat.ps_log_.c_str());
                ImGui::Unindent();
            }
        }
        ImGui::End();

        effect_graph_windows.run();

        mat_prober_window.run();

        if (ImGui::Begin("Debug")) {
            effect_graph_windows.debug();
        }
        ImGui::End();

        ffx_window.run();

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0, 0});
        if (ImGui::Begin("3D", nullptr, ImGuiWindowFlags_NoScrollbar)) {
            auto size = ImGui::GetContentRegionAvail();
            if (!rt_3d || !rt_3d->is_size(size)) {
                rt_3d = std::make_shared<pog::RenderTarget>(dx.dev_, size);
            }

            glm::vec4 clear_color{0.1f, 0.2f, 0.3f, 1.0f};
            dx.ctx_->ClearRenderTargetView(rt_3d->color_rtv_, &clear_color[0]);
            dx.ctx_->ClearDepthStencilView(rt_3d->depth_dsv_, D3D11_CLEAR_DEPTH, 1.0f, 0);

            D3D11_VIEWPORT viewport{.Width = size.x, .Height = size.y, .MaxDepth = 1.0f};
            dx.ctx_->RSSetViewports(1, &viewport);
            std::array<ID3D11RenderTargetView *, 1> rtvs{rt_3d->color_rtv_};
            dx.ctx_->OMSetRenderTargets((UINT)rtvs.size(), rtvs.data(), rt_3d->depth_dsv_);

            // Update view/proj matrices
            {
                DirectX::FXMVECTOR player_pos = {0.0f, 0.0f, 0.0f, 1.0f};
                DirectX::FXMVECTOR eye_offset = {-1000.0f, -1000.0f, -1000.0f, 0.0f};
                DirectX::FXMVECTOR eye_pos = DirectX::XMVectorAdd(player_pos, eye_offset);
                view_3d.proj_matrix_ = DirectX::XMMatrixPerspectiveLH(size.x, size.y, 0.1f, 5000.0f);
                view_3d.view_matrix_ = DirectX::XMMatrixLookAtLH(eye_pos, player_pos, DirectX::g_XMNegateZ);
                cb_frame_cpu.proj_matrix = view_3d.proj_matrix_;
                cb_frame_cpu.view_matrix = view_3d.view_matrix_;
            }

            dx.ctx_->UpdateSubresource(cb_frame_gpu, 0, nullptr, &cb_frame_cpu, 0, 0);

            if (void_mat) {
                dx.ctx_->IASetInputLayout(quad_il);
                dx.ctx_->IASetIndexBuffer(nullptr, DXGI_FORMAT_UNKNOWN, 0);

                ID3D11Buffer *vbs[] = {quad_vb};
                UINT strides[] = {sizeof(QuadVertex)};
                UINT offsets[] = {0};
                dx.ctx_->IASetVertexBuffers(0, 1, vbs, strides, offsets);

                if (ImGui::Begin("Debug")) {
                    ImGui::Text("%f x %f", size.x, size.y);
                    ImGui::RadioButton("Dummy shader", &shader_used, 0);
                    ImGui::RadioButton("Cool shader", &shader_used, 1);
                }
                ImGui::End();

                CComPtr<ID3D11SamplerState> ss;
                {
                    D3D11_SAMPLER_DESC samp_desc{
                        .Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR,
                        .AddressU = D3D11_TEXTURE_ADDRESS_WRAP,
                        .AddressV = D3D11_TEXTURE_ADDRESS_WRAP,
                        .AddressW = D3D11_TEXTURE_ADDRESS_CLAMP,
                        .MipLODBias = 0.0f,
                        .MaxAnisotropy = 1,
                        .ComparisonFunc = D3D11_COMPARISON_NEVER,
                        .BorderColor = {1.0f, 1.0f, 1.0f, 1.0f},
                        .MinLOD = -FLT_MAX,
                        .MaxLOD = +FLT_MAX,
                    };

                    hr = dx.dev_->CreateSamplerState(&samp_desc, &ss);
                }
                std::vector<ID3D11SamplerState *> sss(void_mat->textures_.size(), ss);
                dx.ctx_->PSSetSamplers(0, (UINT)sss.size(), sss.data());

                std::vector<ID3D11ShaderResourceView *> srvs;
                for (auto const &tex : void_mat->textures_) {
                    srvs.push_back(tex.srv_);
                };
                dx.ctx_->VSSetShaderResources(0, (UINT)srvs.size(), srvs.data());
                dx.ctx_->PSSetShaderResources(0, (UINT)srvs.size(), srvs.data());

                std::array<ID3D11Buffer *, 1> cbs{
                    cb_frame_gpu,
                };
                dx.ctx_->VSSetConstantBuffers(0, (UINT)cbs.size(), cbs.data());
                dx.ctx_->PSSetConstantBuffers(0, (UINT)cbs.size(), cbs.data());

                dx.ctx_->VSSetShader(void_mat->vs_, nullptr, 0);
                dx.ctx_->PSSetShader(void_mat->ps_, nullptr, 0);

                dx.ctx_->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
                dx.ctx_->Draw(6, 0);
            }

            dx.ctx_->OMSetRenderTargets(0, nullptr, nullptr);

            ImGui::Image((ImTextureID)rt_3d->color_srv_, size);
        }
        ImGui::End();
        ImGui::PopStyleVar();

        window_frame.render_and_present();
    }

    // mp.stop();

    offthread_pool.join();
    io_work.reset();

    return 0;
}